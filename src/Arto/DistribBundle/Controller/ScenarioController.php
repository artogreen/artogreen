<?php

namespace Arto\DistribBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Arto\DistribBundle\Entity\Scenario;
use Arto\DistribBundle\Entity\Flux;

class ScenarioController extends Controller
{
    public function saveAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $scenario = $em->getRepository('ArtoDistribBundle:Scenario')->find($this->getRequest()->get('scenario'));

        if ($scenario == null) {
            $scenario = new Scenario();
            $project = $em->getRepository('ArtoDistribBundle:Project')->find($this->getRequest()->get('project'));
            $scenario->setProject($project);
        }

        $scenario->setLabel($this->getRequest()->get('name'));
        $em->persist($scenario);
        $em->flush();

        return new Response($scenario->getId());
    }
    
    public function deleteAction(){
       $em = $this->getDoctrine()->getEntityManager();
       $request = $this->getRequest();
       
       $id = $request->get('id');
       
       $scenario = $em->getRepository('ArtoDistribBundle:Scenario')->findOneBy(
               array('id' => $id)
       );
       
       if($scenario != null){
           $em->remove($scenario);
           $em->flush();
       }
       
       $result = array(
           'scenarioId' => $id,
       );
       
       $json = json_encode($result);
       return new Response($json);
    }
    
    public function calculateTotalsAction(){
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        
        $id = $request->get('project');
        
        $project = $em->getRepository('ArtoDistribBundle:Project')->findOneBy(
            array('id' => $id));
        
        $scenarios = $project->getScenarios();
        
        $cost = 0;
        $impactCO2 = 0;
        $tonnage = 0;
        $percentMass = 0;
        $mass = 0;
        
        foreach($scenarios as $scenario){
            $fluxes = $scenario->getFluxes();
            foreach($fluxes as $flux){
               $cost = $cost + $flux->getTotalCost();
               $impactCO2 = $impactCO2 + $flux->getTotalImpact();
               $tonnage = $tonnage + $flux->getTotalTonnage();
               $percentMass = $percentMass + $flux->getTotalPercentMass();
               $mass = $mass + $flux->getTotalMass();
            }
            $scenario->setCost($cost);
            $scenario->setImpactCO2($impactCO2);
            $scenario->setTotalMass($mass);
            $em->persist($scenario);
            
            $cost = 0;
            $impactCO2 = 0;
            $tonnage = 0;
            $percentMass = 0;
            $mass = 0;
        }
        $em->flush();
        return new Response();
    }
    
    public function calculateTotalMassAction(){
       $em = $this->getDoctrine()->getEntityManager();
       $request = $this->getRequest();
       
       $id = $request->get('id');
       
       $scenario = $em->getRepository('ArtoDistribBundle:Scenario')->findOneBy(
               array('id' => $id)
       );
       
       $oldWeightPercentageLeft = $scenario->getWeightPercentageLeft();
       $oldWeightStatusLeft = $scenario->getWeightStatusLeft();
       
       $project = $scenario->getProject();
        
       $mass = 0;
       
       $fluxes = $scenario->getFluxes();
       foreach ($fluxes as $flux) {
            $names = array();
            $parts = $flux->getParts();
            foreach ($parts as $part) {
                $name = $part->getName();
                if(!in_array($name, $names)){
                    $percentFlux = $flux->getPercentage();
                    $pva = $project->getPva();
                    $nbreAnnuelParts = ($pva * $percentFlux)/100;

                    $quantity = $part->getQuantity();
                    $unitMass = $part->getUnitMass();

                    $totalMass = $quantity * $unitMass * $nbreAnnuelParts;
                    $partMass = $totalMass/1000000;
                    $partMass = round($partMass, 1);
                    
                    $mass = $mass + $partMass;
                    $part->setTotalMass($partMass);
                }else{
                    $part->setTotalMass(0);
                }
                
                array_push($names, $name);
                $em->persist($part);
            }
        }
              
        $scenario->setTotalMass($mass);
        $em->persist($scenario);
        $em->flush();
        
        $totalMass = number_format($mass,2,',',' ');
        
        $newWeightPercentageLeft = $scenario->getWeightPercentageLeft();
        $newWeightStatusLeft = $scenario->getWeightStatusLeft();

        $result = array(
           'id' => $id,
           'totalMass' => $totalMass,
           'oldWeightPercentageLeft' => $oldWeightPercentageLeft,
           'oldWeightStatusLeft' => $oldWeightStatusLeft,
           'newWeightPercentageLeft' => $newWeightPercentageLeft,
           'newWeightStatusLeft' => $newWeightStatusLeft,
       );
       
       $json = json_encode($result);
       return new Response($json);
    }
}
