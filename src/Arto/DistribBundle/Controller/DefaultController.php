<?php

namespace Arto\DistribBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Arto\GreenBundle\Controller\TokenAuthenticatedController;
use Arto\DistribBundle\Entity\Project;
use Arto\DistribBundle\Entity\Folder;
use Arto\DistribBundle\Entity\Country;
use Arto\DistribBundle\Entity\DistanceInter;
use Arto\DistribBundle\Entity\Scenario;
use Arto\DistribBundle\Entity\Flux;
use Arto\DistribBundle\Entity\Part;
use Arto\DistribBundle\Entity\DistanceLocal;
use Arto\DistribBundle\Entity\Test;

use Xlab\pChartBundle\pData;
use Xlab\pChartBundle\pDraw;
use Xlab\pChartBundle\pRadar;
use Xlab\pChartBundle\pImage;

use Xlab\DafuerJpgraphBundle\Graph;
use Xlab\DafuerJpgraphBundle\BarPlot;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

class DefaultController extends Controller implements TokenAuthenticatedController{
    private function dateToSQL($date) {
        $returnDate = null;

        $s=substr($date, 6, 4)
            ."-".substr($date, 3, 2)
            ."-".substr($date, 0, 2);

        try {
           $returnDate = new \DateTime($s);
        } catch (\Exception $e) {
            // Nothing to do
        }

        return $returnDate;
    }
    
    private function getCalculator()
    {
        return $this->get('arto.distrib.calculator');
    }

    public function indexAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $userId = $user->getId();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        $folderLetters = array();

        $projects = $em->getRepository('ArtoDistribBundle:Project')->findAllActive($user);
        
        //projectsWithoutFolder
        $projectsWF = $em->getRepository('ArtoDistribBundle:Project')->findBy(
                array('greenUser' => $userId, 'folder' => null), array('name' => 'ASC'));
        
        $folders = $em->getRepository('ArtoDistribBundle:Folder')->findBy(
                array('user' => $userId), array('name' => 'ASC'));
        
        foreach($folders as $folder){
            $folderName = $folder->getName();
            $firstLetter = strtoupper(substr($folderName,0,1));
            
            if(!in_array($firstLetter, $folderLetters)){
                array_push($folderLetters, $firstLetter);
            }
        }

        return $this->render('ArtoDistribBundle:Default:index.html.twig', array('projects' => $projects, 'user' => $userId, 'folders' => $folders,'projectsWF'=> $projectsWF, 'myLetter' => '', 'folderLetters' => $folderLetters));
    }

    public function createAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();

        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        $dossierId = $request->get('dossier');

        $project = new Project();
        $project->setGreenUser($user);
        $project->setName($request->get('name'));
        //$project->setPva(1);
        
        if($dossierId == ''){
            $project->setFolder(null);
            $em->persist($project);
        }else{
            $dossier = $em->getRepository('ArtoDistribBundle:Folder')->find($dossierId);

            $dossier->addProject($project); 
            $project->setFolder($dossier);

            $em->persist($dossier);
            $em->persist($project);
        }
        
        $em->persist($project);
        $em->flush();

        return $this->redirect($this->generateUrl('distrib_project_description', array('id' => $project->getId())));
    }
    
    public function deleteAction(){
        $user = $this->get('security.context')->getToken()->getUser();

        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        
        $id = $request->get('id');
        $special = $request->get('special');

        $project = $em->getRepository('ArtoDistribBundle:Project')->find($id);
        
        $isCorrectUser = $project->getGreenUser()->getId() == $user->getId();
        $isSpecial = isset($special) && $special != null;
        
        if ($isCorrectUser || $isSpecial) {
            $em->remove($project);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('distrib'));
    }
    
    public function renameAction(){
        $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        
        $id = $request->get('id');
        $newName = $request->get('name');

        $project = $em->getRepository('ArtoDistribBundle:Project')->find($id);
        
        $project->setName($newName);
        
        $em->persist($project);
        $em->flush();
               
        return $this->redirect($this->generateUrl('distrib'));  
    }
    
    public function generalAction(){
        return $this->render('ArtoDistribBundle:Default:general.html.twig');
    }

    public function descriptionAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        
        $id = $request->get('id');
        
        $project = $em->getRepository('ArtoDistribBundle:Project')->find($id);
        $operations = $em->getRepository('ArtoDistribBundle:Operation')->findBy(
                array('project' => $project->getId()));
        
        $scenarios = $em->getRepository('ArtoDistribBundle:Scenario')->findBy(
                array('project' => $project->getId()));
        
        $nbScenarios = count($scenarios);
        

        return $this->render('ArtoDistribBundle:Default:description.html.twig', array(
            'project' => $project,
            'operations' => $operations,
            'nbScenarios' => $nbScenarios
        ));
    }

    public function descriptionSaveAction($project)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        $project = $em->getRepository('ArtoDistribBundle:Project')->find($project);

        $project->setProductName($request->get('productName'));
        $weight = $request->get('weight');
        $weight = str_replace(',','.',$weight);
        
        $pva = $request->get('pva');
        $pilote = $request->get('pilote');
        $totalMass = ($weight * $pva)/1000000;
        
        $project->setPva($pva);
        $project->setPilote($pilote);
        $project->setWeight($weight);
        $project->setTotalMass($totalMass);
        
        $project->setObjectifs($request->get('objectifs'));
        $project->setDonnéesLogistiques($request->get('donnéesLogistiques'));
        
        $typeFlux = $request->get('typeFlux');
        if($typeFlux == "pieces"){
            $project->setTypeFlux("pieces");
        }else if($typeFlux == "produitFini"){
            $project->setTypeFlux("produitFini");
        }

        if ($request->get('pilote_date') != null) {
            $project->setPiloteDate($this->dateToSQL($request->get('pilote_date')));
        }

        $uploadedFile = $_FILES['file'];
        if(isset($uploadedFile['name'])) {
            if($uploadedFile['name'] != ''){
                move_uploaded_file($uploadedFile['tmp_name'], __DIR__.'/../../../../web/uploads/distrib/'.$uploadedFile['name']);
                $project->setPath($uploadedFile['name']);
            }
        }

        $em->persist($project);
        $em->flush();

        return $this->redirect($this->generateUrl('distrib_project_description', array('id' => $project->getId())));
    }
    
    public function bilanSaveAction($project){
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        $project = $em->getRepository('ArtoDistribBundle:Project')->find($project);
        
        $project->setCommentaires($request->get('commentaires'));
        
        $em->persist($project);
        $em->flush();
        
        return $this->redirect($this->generateUrl('distrib_project_bilan', array('id' => $project->getId())));
    }

    public function evaluationAction($id = null)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoDistribBundle:Project')->find($id);

        $countries = $em->getRepository('ArtoDistribBundle:Country')->findAll();
        $departments = $em->getRepository('ArtoDistribBundle:Department')->findAll();
        
        $this->recalculateAllMasses($id);

        $scenarios = $em->getRepository('ArtoDistribBundle:Scenario')->findBy(array('project' => $project->getId()));
        
        $nbScenarios = count($scenarios);
       
        return $this->render('ArtoDistribBundle:Default:evalCarbone.html.twig', array(
            'project' => $project,
            'countries' => $countries,
            'departments' => $departments,
            'scenarios' => $scenarios,
            'nbScenarios' => $nbScenarios,
        ));
    }

    public function modesAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        $modeTransport = null;
        $typeTransport = $request->get('typeTransport');
        $partId = $request->get('partId');

        $part = $em->getRepository('ArtoDistribBundle:Part')->find($partId);
        if ($part != null) {
            //return new Response('<html><body>Part NON null</body></html>');
            $modeTransport = $part->getModeTransport();
            if($typeTransport != $part->getTypeTransport()){
                //return new Response('<html><body>Type transport : '.$typeTransport.' != '.$part->getTypeTransport().'</body></html>');
                $modeTransport = null;
            }
            //return new Response('<html><body>Type transport identiques !</body></html>');
        }
        
        //return new Response('<html><body>Type transport : '.$typeTransport.' Mode transport : '.$modeTransport.'</body></html>');
        
        return $this->render('ArtoDistribBundle:Default:modesTransport.html.twig', array(
            'modeTransport' => $modeTransport,
            'typeTransport' => $typeTransport
        ));
    }
    
    public function logosAction(){
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        $modeTransport = null;
        $partId = $request->get('partId');
        $typeTransport = $request->get('typeTransport');
        
        $part = $em->getRepository('ArtoDistribBundle:Part')->find($partId);
        if ($part != null) {
            $modeTransport = $part->getModeTransport();
             if($typeTransport != $part->getTypeTransport()){
                $modeTransport = null;
            }
        }
        
        return $this->render('ArtoDistribBundle:Default:logosTransport.html.twig', array(
            'modeTransport' => $modeTransport,
        ));
    }

    public function addFluxAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $scenarioId = $this->getRequest()->get('scenarioId');

        echo($scenarioId);
        $scenario = $em->getRepository('ArtoDistribBundle:Scenario')->find($scenarioId);

        $flux = new Flux();
        $flux->setLabel("Nouveau flux");
        $flux->setScenario($scenario);
        $flux->setPercentage("50");
        $em->persist($flux);
        $em->flush();


        return new Response($flux->getId());
    }

    public function importAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $j = 1;

        if (($handle = fopen("/Users/nay/Clients/Artogreen/distance.csv", "r")) !== FALSE) {

            while (($row = fgetcsv($handle, 0, ";")) !== FALSE) {
                $dpt1 = $em->getRepository('ArtoDistribBundle:Department')->findOneByCode(trim($row[0]));

                for($i=1;$i<=95;$i++) {
                    $dpt2 = $em->getRepository('ArtoDistribBundle:Department')->findOneByCode($i);

                    if ($dpt1 != null && $dpt2 != null && $dpt1->getId() != $dpt2->getId()) {
                        $distance = $em->getRepository('ArtoDistribBundle:DistanceNational')->findOneBy(array(
                            'departure' => $dpt1->getId(),
                            'arrival' => $dpt2->getId(),
                        ));

                        if ($distance == null) {
                            $distance = new DistanceLocal();
                            $distance->setDeparture($dpt1);
                            $distance->setArrival($dpt2);

                            $km = trim($row[$i]);
                            if ($km != '') {
                                $distance->setKm($km);
                            }

                            $em->persist($distance);
                        }
                    }
                }

                $em->flush();

                $j++;
            }

            fclose($handle);
        }

        return new Response('finish');
    }

    public function bilanAction($id = null)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoDistribBundle:Project')->find($id);
        $projectId = $project->getId();

        $scenarios = $em->getRepository('ArtoDistribBundle:Scenario')->findBy(array('project' => $projectId));

        $emissionsCO2 = array();
        $costsCarburant = array();
        $names = array();

        foreach($scenarios as $scenario){
            //$scenario = new Scenario();
            $emissionsCO2[] = $scenario->getImpactCO2();
            $costsCarburant[] = number_format(($scenario->getCost()/1000),2,'.','');
            $names[] = $scenario->getLabel();
        }

        $font = __DIR__."/../../../Xlab/pChartBundle/Resources/fonts/calibri.ttf";

        $myData = new pData();
        $myData->addPoints($emissionsCO2,"Serie1");
        $myData->setSerieDescription("Serie1","Emissions CO2");
        $myData->setSerieOnAxis("Serie1",0);

        $myData->addPoints($names,"Absissa");
        $myData->setAbscissa("Absissa");

        $myData->setAxisPosition(0,AXIS_POSITION_LEFT);
        $myData->setAxisName(0,"");
        $myData->setAxisUnit(0,"");

        $myPicture = new pImage(700,230,$myData);
        $Settings = array("R"=>170, "G"=>183, "B"=>87, "Dash"=>1, "DashR"=>190, "DashG"=>203, "DashB"=>107);
        $myPicture->drawFilledRectangle(0,0,700,230,$Settings);

        $Settings = array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>220, "EndG"=>220, "EndB"=>220, "Alpha"=>50);
        $myPicture->drawGradientArea(0,0,700,230,DIRECTION_VERTICAL,$Settings);

        $myPicture->drawRectangle(0,0,699,229,array("R"=>0,"G"=>0,"B"=>0));

        $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>50,"G"=>50,"B"=>50,"Alpha"=>20));

        $myPicture->setFontProperties(array("FontName"=>$font,"FontSize"=>12));
        $TextSettings = array("Align"=>TEXT_ALIGN_MIDDLEMIDDLE
        , "R"=>3, "G"=>1, "B"=>8);
        $myPicture->drawText(350,25,"Emissions CO2 (T) en fonction des hypothèses retenues",$TextSettings);

        $myPicture->setShadow(FALSE);
        $myPicture->setGraphArea(50,50,675,190);
        $myPicture->setFontProperties(array("R"=>0,"G"=>0,"B"=>0,"FontName"=>$font,"FontSize"=>12));

        $Settings = array("Pos"=>SCALE_POS_LEFTRIGHT
        , "Mode"=>SCALE_MODE_START0
        , "LabelingMethod"=>LABELING_ALL
        , "GridR"=>255, "GridG"=>255, "GridB"=>255, "GridAlpha"=>50, "TickR"=>0, "TickG"=>0, "TickB"=>0, "TickAlpha"=>50, "LabelRotation"=>0, "CycleBackground"=>1, "DrawXLines"=>1, "DrawSubTicks"=>1, "SubTickR"=>255, "SubTickG"=>0, "SubTickB"=>0, "SubTickAlpha"=>50, "DrawYLines"=>ALL);
        $myPicture->drawScale($Settings);

        $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>50,"G"=>50,"B"=>50,"Alpha"=>10));

        $Config = array("DisplayValues"=>1, "AroundZero"=>1);
        $myPicture->drawBarChart($Config);

        $path = $this->get('kernel')->getRootDir() . '/../web';

        $myPictureName = $path . "/images/distrib/graphEmissionsCO2-".$projectId.".png";
        $myPicture->render($myPictureName);

        ///////////////////////////////////////////////
        //Graph2
        $myData2 = new pData();
        $myData2->addPoints($costsCarburant,"Serie1");
        $myData2->setSerieDescription("Serie1","Co�ts carburant");
        $myData2->setSerieOnAxis("Serie1",0);

        $myData2->addPoints($names,"Absissa");
        $myData2->setAbscissa("Absissa");

        $myData2->setAxisPosition(0,AXIS_POSITION_LEFT);
        $myData2->setAxisName(0,"");
        $myData2->setAxisUnit(0,"");

        $myPicture2 = new pImage(700,230,$myData2);
        $Settings2 = array("R"=>170, "G"=>183, "B"=>87, "Dash"=>1, "DashR"=>190, "DashG"=>203, "DashB"=>107);
        $myPicture2->drawFilledRectangle(0,0,700,230,$Settings2);

        $Settings2 = array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>220, "EndG"=>220, "EndB"=>220, "Alpha"=>50);
        $myPicture2->drawGradientArea(0,0,700,230,DIRECTION_VERTICAL,$Settings2);

        $myPicture2->drawRectangle(0,0,699,229,array("R"=>0,"G"=>0,"B"=>0));

        $myPicture2->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>50,"G"=>50,"B"=>50,"Alpha"=>20));

        $myPicture2->setFontProperties(array("FontName"=>$font,"FontSize"=>12));
        $TextSettings2 = array("Align"=>TEXT_ALIGN_MIDDLEMIDDLE
        , "R"=>3, "G"=>1, "B"=>8);
        $myPicture2->drawText(350,25,"Coût transport (k€) en fonction des hypothèses retenues",$TextSettings2);

        $myPicture2->setShadow(FALSE);
        $myPicture2->setGraphArea(50,50,675,190);
        $myPicture2->setFontProperties(array("R"=>0,"G"=>0,"B"=>0,"FontName"=>$font,"FontSize"=>12));

        $Settings2 = array("Pos"=>SCALE_POS_LEFTRIGHT
        , "Mode"=>SCALE_MODE_START0
        , "LabelingMethod"=>LABELING_ALL
        , "GridR"=>255, "GridG"=>255, "GridB"=>255, "GridAlpha"=>50, "TickR"=>0, "TickG"=>0, "TickB"=>0, "TickAlpha"=>50, "LabelRotation"=>0, "CycleBackground"=>1, "DrawXLines"=>1, "DrawSubTicks"=>1, "SubTickR"=>255, "SubTickG"=>0, "SubTickB"=>0, "SubTickAlpha"=>50, "DrawYLines"=>ALL);
        $myPicture2->drawScale($Settings);

        $myPicture2->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>50,"G"=>50,"B"=>50,"Alpha"=>10));

        $Config2 = array("DisplayValues"=>1, "AroundZero"=>1);
        $myPicture2->drawBarChart($Config2);

        $path = $this->get('kernel')->getRootDir() . '/../web';

        $myPictureName2 = $path . "/images/distrib/graphCoutsTransport-".$projectId.".png";
        $myPicture2->render($myPictureName2);

        $grapheCoutsTransport = "/images/distrib/graphCoutsTransport-".$projectId.".png";
        $grapheEmissionCO2 = "/images/distrib/graphEmissionsCO2-".$projectId.".png";

        return $this->render('ArtoDistribBundle:Default:bilanCarbone.html.twig', array(
            'project' => $project,
            'scenarios' => $scenarios,
            'grapheEmissionCO2' => $grapheEmissionCO2,
            'grapheCoutsTransport' => $grapheCoutsTransport,
            'scenariosCount' => count($scenarios)
        ));
    }
    
    public function findDestinationsDepartAction(){
        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        
        $destinationsM = null;
        $destinationsN = null;
        
        $typeTransport = $request->get('typeTransport');
        
        $partId = $request->get('partId');
        
        $part = $em->getRepository('ArtoDistribBundle:Part')->find($partId);

        if ($typeTransport == "M") {
            $destinationsM = $em->getRepository('ArtoDistribBundle:Country')->findAll();
        } else {
            $destinationsN = $em->getRepository('ArtoDistribBundle:Department')->findAll();
        }
        return $this->render('ArtoDistribBundle:Default:destinationsDepart.html.twig', array(
            'destinationsM' => $destinationsM,
            'destinationsN' => $destinationsN,
            'part' => $part
        ));
    }
    
    public function findDestinationsArriveeAction(){
        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        
        $destinationsM = null;
        $destinationsN = null;
        
        $typeTransport = $request->get('typeTransport');
        
        $partId = $request->get('partId');
        
        $part = $em->getRepository('ArtoDistribBundle:Part')->find($partId);

        if ($typeTransport == "M") {
            $destinationsM = $em->getRepository('ArtoDistribBundle:Country')->findAll();
        } else {
            $destinationsN = $em->getRepository('ArtoDistribBundle:Department')->findAll();
        }
        return $this->render('ArtoDistribBundle:Default:destinationsArrivee.html.twig', array(
            'destinationsM' => $destinationsM,
            'destinationsN' => $destinationsN,
            'part' => $part
        ));
    }
    
    public function distanceKnownAction(){
        $em = $this->getDoctrine()->getEntityManager();
          
        $distance = 0;
        
        $request = $this->getRequest();
        $departure = $request->get('depart');
        $arrival = $request->get('arrivee');
        $typeTransport = $request->get('typeTransport');
        $modeTransport = $request->get('modeTransport');
        $distanceKnown = "no";

        $part = $em->getRepository('ArtoDistribBundle:Part')->find($request->get('part'));
        if ($part != null) {
            $transport = $part->getModeTransport();
        }
        
        if($typeTransport == "M"){
            $journey = $em->getRepository('ArtoDistribBundle:DistanceInter')->findOneBy(
                    array('departure' => $departure, 'arrival' => $arrival));

            if($journey == null){
                $journey = $em->getRepository('ArtoDistribBundle:DistanceInter')->findOneBy(
                    array('departure' => $arrival , 'arrival' => $departure));
                }
                
            if ($journey != null) {
                if ($modeTransport == "air") {
                    if ($journey->getKmAir() != 0) {
                        $distanceKnown = "yes";
                        $distance = $journey->getKmAir();
                    }
                } elseif ($modeTransport == "road") {
                    if ($journey->getKmRoad() != 0) {
                        $distanceKnown = "yes";
                        $distance = $journey->getKmRoad();
                    }
                } elseif ($modeTransport == "water") {
                    if ($journey->getKmWater() != 0) {
                        $distanceKnown = "yes";
                        $distance = $journey->getKmWater();
                    }
                } elseif ($modeTransport == "rail") {
                    if ($journey->getKmRail() != 0) {
                        $distanceKnown = "yes";
                        $distance = $journey->getKmRail();
                    }
                }
            }
        }elseif($typeTransport == "N"){
           $journey = $em->getRepository('ArtoDistribBundle:DistanceNational')->findOneBy(
                    array('departure' => $departure, 'arrival' => $arrival));
           $distanceKnown = "yes";
           $distance = $journey->getKm();
        }
        
        $result = array(
            'connue' => $distanceKnown,
            'distance' => $distance
        );
        
        $json = json_encode($result);
        return new Response($json);
    }
    
    /**
     * Pour recalculer les masses des scénarios, flux et parts d'un projet
     */
    public function recalculateAllMasses($id){
        $em = $this->getDoctrine()->getEntityManager();
        $project = $em->getRepository('ArtoDistribBundle:Project')->find($id);
        $calculator = $this->getCalculator();
        
        $scenarios = $project->getScenarios();
        
        foreach($scenarios as $scenario){
            $scenarioMass = 0;
            $fluxes = $scenario->getFluxes();
            foreach ($fluxes as $flux) {
                $fluxId = $flux->getId();
                $parts = $flux->getParts();
                $cost = 0;
                $impactCO2 = 0;
                $tonnage = 0;
                $percentMass = 0;
                $mass = 0;
                foreach ($parts as $part) {
                    $calculator->recalculatePart($part);
                    $cost = $cost + $part->getPartCost();
                    $impactCO2 = $impactCO2 + $part->getImpactEvt();
                    $tonnage = $tonnage + $part->getTonnage();
                    $percentMass = $percentMass + $part->getPercentTotalMass();
                    $mass = $mass + $part->getTotalMass();
                    
                    $em->persist($part);
                }
                
                $flux->setTotalCost($cost);
                $flux->setTotalImpact($impactCO2);
                $flux->setTotalMass($mass);
                $flux->setTotalPercentMass($percentMass);
                $flux->setTotalTonnage($tonnage);
                $em->persist($flux);
                
                $scenarioMass = $scenarioMass + $flux->getTotalMass();
            }
            //En tonnes
            $scenarioMass = number_format(($scenarioMass),2,',',' ');
            $scenario->setTotalMass($scenarioMass);
            $em->persist($scenario);
        }
        $em->flush();
    }
    
    public function recalculateWeightPartsAction(){
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        
        $project = $em->getRepository('ArtoDistribBundle:Project')->find($request->get('project'));
        $weight = $request->get('weight');
        
        $calculator = $this->getCalculator();
        
        $totalMass = $calculator->recalculateWeightParts($project, $weight);
        
        $result = array(
            'totalMass' => $totalMass
        );
        
        $json = json_encode($result);
        return new Response($json);
    }
    
    public function recalculatePvaPartsAction(){
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        
        $project = $em->getRepository('ArtoDistribBundle:Project')->find($request->get('project')); 
        $pva = $request->get('pva');
        
        $calculator = $this->getCalculator();
        
        $totalMass = $calculator->recalculatePvaParts($project, $pva);
          
        $result = array(
            'totalMass' => $totalMass
        );
        
        $json = json_encode($result);
        return new Response($json);
    }
}
