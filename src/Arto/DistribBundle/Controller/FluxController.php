<?php

namespace Arto\DistribBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Arto\DistribBundle\Entity\Flux;
use Arto\DistribBundle\Entity\Part;

class FluxController extends Controller
{
    private function getCalculator()
    {
        return $this->get('arto.distrib.calculator');
    }
    
    public function saveAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        $result = null;
        
        $name = $request->get('name');
        $percentage = $request->get('percentage');
        
        if($name != '' && $percentage != ''){
            $flux = $em->getRepository('ArtoDistribBundle:Flux')->find($request->get('flux'));

            if ($flux == null) {
                $flux = new Flux();
                $scenario = $em->getRepository('ArtoDistribBundle:Scenario')->find($request->get('scenario'));
                $flux->setScenario($scenario);
            }
            
            $flux->setTotalCost(0);
            $flux->setTotalTonnage(0);
            $flux->setTotalPercentMass(0);
            $flux->setTotalMass(0);
            $flux->setTotalImpact(0);
            
            $flux->setLabel($name);
            $flux->setPercentage($percentage);
            $em->persist($flux);
            $em->flush();
            
            $id = $flux->getId();
            
            $result = array(
                'id' => $id
            );
        }
        
        $json = json_encode($result);
        return new Response($json);
    }
    
    public function deleteAction(){
       $em = $this->getDoctrine()->getEntityManager();
       $request = $this->getRequest();
       
       $id= $request->get('id');
       
       $flux = $em->getRepository('ArtoDistribBundle:Flux')->findOneBy(
               array('id' => $id)
       );
       
       if($flux != null){
           $fluxId = $flux->getId();
           
           $scenario = $flux->getScenario();
           $scenarioId = $scenario->getId();
           
           $cost = $flux->getTotalCost();
           $impactCO2 = $flux->getTotalImpact();
           $mass = $flux->getTotalMass();
           
           $scenario->setCost($scenario->getCost() - $cost);
           $scenario->setImpactCO2($scenario->getImpactCO2() - $impactCO2);
           $scenario->setTotalMass($scenario->getTotalMass() - $mass);
           
           $em->persist($scenario);
           $em->remove($flux);
           $em->flush();
       }
       
       $result = array(
           'fluxId' => $fluxId,
           'scenarioId' => $scenarioId,
           'scenarioTotalMass' => $scenario->getTotalMass(),
       );
       
       $json = json_encode($result);
       return new Response($json);
    }
    
    public function calculateTotalsAction(){
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        
        $calculator = $this->getCalculator();
        
        $flux = $em->getRepository('ArtoDistribBundle:Flux')->find($request->get('flux'));
        
        $parts = $flux->getParts();
        
        $cost = 0;
        $impactCO2 = 0;
        $tonnage = 0;
        $percentMass = 0;
        $mass = 0;
        
        $names = array();
        
        foreach($parts as $part){
            $calculator->recalculatePart($part);
            
            $name = $part->getName();
            
            $cost = $cost + $part->getPartCost();
            $impactCO2 = $impactCO2 + $part->getImpactEvt();
            
            if(!in_array($name, $names)){
                $tonnage = $tonnage + $part->getTonnage();
                $percentMass = $percentMass + $part->getPercentTotalMass();
                $mass = $mass + $part->getTotalMass();
            }
            
            array_push($names, $name);
        }
        
        $flux->setTotalCost($cost);
        $flux->setTotalImpact($impactCO2);
        $flux->setTotalMass($mass);
        $flux->setTotalPercentMass($percentMass);
        $flux->setTotalTonnage($tonnage);
        
        $em->persist($flux);
        $em->flush();
        
        $result = array(
            'id' => $flux->getId(),
            'cost' => $cost,
            'impactCO2' => round($impactCO2, 1),
            'tonnage' => $tonnage,
            'percentMass' => $percentMass,
            'mass' => $mass,
         );
        
        $json = json_encode($result);
        return new Response($json);
    }
}
