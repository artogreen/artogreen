<?php

namespace Arto\DistribBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Arto\DistribBundle\Entity\Flux;
use Arto\DistribBundle\Entity\Part;
use Arto\DistribBundle\Entity\Project;
use Arto\DistribBundle\Entity\Scenario;
use Arto\DistribBundle\Entity\DistanceLocal;
use Arto\DistribBundle\Entity\DistanceInterSaisie;
use Arto\DistribBundle\Entity\DistanceInter;
use Arto\DistribBundle\Entity\DistanceNational;


class PartController extends Controller
{
    private function getCalculator()
    {
        return $this->get('arto.distrib.calculator');
    }
    
    public function saveAction()
    {
        $calculator = $this->getCalculator();
        $part = null;
        $part_id = null;
        $request = $this->getRequest();
        $distanceInter = null;
        $distanceNational = null;
        $distanceLocal = null;
        $result = null;
        
        $em = $this->getDoctrine()->getEntityManager();
        
        $id = $request->get('part');
         
        $part = $em->getRepository('ArtoDistribBundle:Part')->findOneBy(
                array('id' => $id));
        
        $newPart = false;
        
        if ($part == null) {
                //echo("Part NULL !");
                $newPart = true;
                $part = new Part();
                $flux = $em->getRepository('ArtoDistribBundle:Flux')->find($request->get('flux'));
                $part->setFlux($flux);
                $depart = $request->get('depart');
                $arrivee = $request->get('arrivee');
                $typeTransport = $request->get('typeTransport');
                
                $quantity = $request->get('quantityPerProduct');
                $unitMass = $request->get('unitMass');
                $modeTransport = $request->get('modeTransport');
           
                $departL = $request->get('departL');
                $arriveeL = $request->get('arriveeL');
                
                $name = $request->get('name');
                $reference = $request->get('reference');
                
                $distanceUnknown = $request->get('distanceUnknown');
            }else{
                $partId = $part->getId();
                //echo("Part ID : ".$partId);
                
                $depart = $request->get('depart'.$partId);
                if($depart == null){
                    $depart = $request->get('departLoad');
                }
                
                $arrivee = $request->get('arrivee'.$partId);
                if($arrivee == null){
                    $arrivee = $request->get('arriveeLoad');
                }
                
                $typeTransport = $request->get('typeTransport'.$partId); 
                
                $quantity = $request->get('quantityPerProduct'.$partId);
                $unitMass = $request->get('unitMass'.$partId);
                $modeTransport = $request->get('modeTransport'.$partId);
           
                $departL = $request->get('departL'.$partId);
                $arriveeL = $request->get('arriveeL'.$partId);
                
                $name = $request->get('name'.$partId);
                $reference = $request->get('reference'.$partId);
                
                $distanceUnknown = $request->get('distanceUnknown'.$partId);
                               
            }

        if($quantity != null && $unitMass != null && $typeTransport != '' ) {
            //Si on sauvegarde, vidage des trucs en BDD existants
            $part->setDistanceLocal(null);
            $part->setDistanceInter(null);
            $part->setDistanceInterSaisie(null);
            $part->setDistanceNational(null);
            $part->setDistance(0);
            $part->setDistanceSaisie(0);
                
            $cost = 0;
            $emissionCO2 = 0;
            
            //Pré-calculs : %masse, %flux, nbre annuel de parts et tonnage
            $project_id = $request->get('project');
            $project = $em->getRepository('ArtoDistribBundle:Project')->find($project_id);
            $pva = $project->getPva();
            $flux = $em->getRepository('ArtoDistribBundle:Flux')->find($request->get('flux'));
            $percentFlux = $flux->getPercentage();
            $nbreAnnuelParts = ($pva * $percentFlux)/100;
            $partTotalMass = ($quantity * $unitMass * $nbreAnnuelParts)/1000000;
            
            $productMass = $project->getTotalMass();
            $percentMass = ($partTotalMass/$productMass)*100;
            $part->setPercentTotalMass($percentMass);
             
            $part->setTotalMass($partTotalMass);

            $tonnage = $partTotalMass;
            $part->setTonnage($tonnage);

            $part->setName($name);
            $part->setReference($reference);
            $part->setModeTransport($modeTransport);
            $part->setQuantity($quantity);
            $part->setUnitMass($unitMass);
            $part->setTypeTransport($typeTransport);

            if($typeTransport == "M"){
                if ($depart != '' && $arrivee != '') {
                    $distanceInter = $em->getRepository('ArtoDistribBundle:DistanceInter')->findOneBy(array(
                        'departure' => $depart,
                        'arrival' => $arrivee
                    ));
                    
                    if($distanceInter == null){
                        $distanceInter = $em->getRepository('ArtoDistribBundle:DistanceInter')->findOneBy(array(
                            'departure' => $arrivee,
                            'arrival' => $depart
                        ));
                    }
                    
                    $distanceToCheck = 0;
                    //Si qqc en BDD, on vérifie si la distance recherchée est 0 ou non
                    if($distanceInter != null){  
                        if($modeTransport == "air"){
                            $distanceToCheck = $distanceInter->getKmAir(); 
                        }elseif($modeTransport == "rail"){
                            $distanceToCheck = $distanceInter->getKmRail();  
                        }elseif($modeTransport == "water"){
                            $distanceToCheck = $distanceInter->getKmWater(); 
                        }else{
                            $distanceToCheck = $distanceInter->getKmRoad(); 
                        }   
                    }
                    
                    //Distance inter nulle ou pas de distance renseignée pour le mode choisi 
                    //-> on crée une distanceInterSaisie   
                    if($distanceInter == null || $distanceToCheck == 0){
                        $distanceInterSaisie = new DistanceInterSaisie();
                        //$distanceUnknown = $request->get('distanceUnknown');
                        $paysD = $em->getRepository('ArtoDistribBundle:Country')->findOneBy(
                                array('id' => $depart));
                        $paysA = $em->getRepository('ArtoDistribBundle:Country')->findOneBy(
                                array('id' => $arrivee));
                        $distanceInterSaisie->setDeparture($paysD);
                        $distanceInterSaisie->setArrival($paysA);
                        
                        if($modeTransport == "air"){
                           $distanceInterSaisie->setKmAir($distanceUnknown); 
                        }elseif($modeTransport == "rail"){
                           $distanceInterSaisie->setKmRail($distanceUnknown); 
                        }elseif($modeTransport == "water"){
                           $distanceInterSaisie->setKmWater($distanceUnknown);
                        }else{
                           $distanceInterSaisie->setKmRoad($distanceUnknown); 
                        }
                        
                        //$part->setTypeTransport($typeTransport);
                        $part->setDistance(0);
                        $part->setDistanceInterSaisie($distanceInterSaisie);
                        $part->setDistanceSaisie($distanceUnknown);
                        $em->persist($distanceInterSaisie);
                        
                        $calculator->calculatePartCostAndImpact($part, $distanceUnknown); 
                    }else{
                        //$part->setTypeTransport($typeTransport);
                        //Distance
                        $part->setDistanceInter($distanceInter);
                        
                        if($distanceInter != null){  
                            if($modeTransport == "air"){
                                $distance = $distanceInter->getKmAir(); 
                            }elseif($modeTransport == "rail"){
                                $distance = $distanceInter->getKmRail();  
                            }elseif($modeTransport == "water"){
                                $distance = $distanceInter->getKmWater(); 
                            }else{
                                $distance = $distanceInter->getKmRoad(); 
                            }   
                        }
                        
                        $part->setDistance($distance);  
                        $part->setDistanceSaisie(0);
                        
                        $calculator->calculatePartCostAndImpact($part, $distance);
                    }
                }
            }elseif($typeTransport == "N"){        
                if ($depart != '' && $arrivee != '') {
                    $distanceNational = $em->getRepository('ArtoDistribBundle:DistanceNational')->findOneBy(array(
                        'departure' => $depart,
                        'arrival' => $arrivee
                    ));

                    if ($distanceNational != null) {
                        //$part->setTypeTransport($typeTransport);
                        $part->setDistanceNational($distanceNational);
                        $distance = $distanceNational->getKm();
                        
                        $part->setDistance($distance);
                        $part->setDistanceSaisie(0);
                        
                        $calculator->calculatePartCostAndImpact($part, $distance);
                    }
                }  
             }elseif($typeTransport == "L"){
                 //New distance locale
                 $distanceLocal = new DistanceLocal();
                 $distanceLocal->setDeparture($departL);
                 $distanceLocal->setArrival($arriveeL);             
                 
                 //$distanceUnknown = $request->get('distanceUnknown');
                 $distanceLocal->setKm($distanceUnknown);
                 
                 $em->persist($distanceLocal);
                 
                 //$part->setTypeTransport($typeTransport);
                 $part->setDistance(0);
                 $part->setDistanceLocal($distanceLocal);
                 $part->setDistanceSaisie($distanceUnknown);
                 
                 $calculator->calculatePartCostAndImpact($part, $distanceUnknown);
             }  
            
            $em->persist($part);
            $em->flush();

            $part_id = $part->getId();
            
            $result = array(
                'id' => $part->getId(),
                'depart' => $depart,
                'arrivee' => $arrivee,
                'percentMass' => round($part->getPercentTotalMass(),1),
                'tonnage' => round($part->getTonnage(),1),
                'distance' => round($part->getDistance(),0),
                'distanceSaisie' => round($part->getDistanceSaisie(),0),
                'impactCO2' => round($part->getImpactEvt(),1)
            );
        }
        
        $json = json_encode($result);
        return new Response($json);
    }

    public function deleteAction(){
       $em = $this->getDoctrine()->getEntityManager();
       $request = $this->getRequest();
       
       $id = $request->get('project');
       $partId = $request->get('id');
       
       $part = $em->getRepository('ArtoDistribBundle:Part')->findOneBy(
               array('id' => $partId));
       
       $project = $em->getRepository('ArtoDistribBundle:Project')->findOneBy(
               array('id' => $id));

       if($part != null){
           $partId = $part->getId();
           $flux = $part->getFlux();
           $scenario = $flux->getScenario();
           
           $cost = $part->getPartCost();
           $tonnage = $part->getTonnage();
           $percentMass = $part->getPercentTotalMass();
           $impactCO2 = $part->getImpactEvt();
           $mass = $part->getTotalMass();
           
           $flux->setTotalCost($flux->getTotalCost() - $cost);
           $flux->setTotalTonnage($flux->getTotalTonnage() - $tonnage);
           $flux->setTotalPercentMass($flux->getTotalPercentMass() - $percentMass);
           $flux->setTotalImpact($flux->getTotalImpact() - $impactCO2);
           $flux->setTotalMass($flux->getTotalMass() - $mass);
           
           $scenario->setCost($scenario->getCost() - $cost);
           $scenario->setImpactCO2($scenario->getImpactCO2() - $impactCO2);
           $scenario->setTotalMass($scenario->getTotalMass() - $mass);
           
           $em->persist($flux);
           $em->persist($scenario);
           
           $em->remove($part);
           $em->flush();
       }
       
       $result = array(
           'partId' => $partId,
           'scenarioId' => $scenario->getId(),
           'fluxId' => $flux->getId(),
           'fluxPercentMass' => $flux->getTotalPercentMass(),
           'fluxTonnage' => $flux->getTotalTonnage(),
           'fluxImpactCO2' => $flux->getTotalImpact(),
           'fluxTotalMass' => $flux->getTotalMass(),
           'scenarioTotalMass' => $scenario->getTotalMass()
       );
       
       $json = json_encode($result);
       return new Response($json);

    }
}
