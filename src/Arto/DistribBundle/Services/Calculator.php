<?php

namespace Arto\DistribBundle\Services;

use Arto\DistribBundle\Entity\Part;
use Arto\DistribBundle\Entity\Flux;
use Arto\DistribBundle\Entity\Scenario;
use Arto\DistribBundle\Entity\Project;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Calculator {
    
    protected $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function recalculateWeightParts(Project $project, $weight ){
        $em = $this->em;
        
        $pva = $project->getPva();
        $totalMass = ($weight * $pva)/1000000;
        $project->setTotalMass($totalMass);
        
        $scenarios = $project->getScenarios();
        $percentMassFlux = 0;
        
        foreach($scenarios as $scenario){
            $fluxes = $scenario->getFluxes();
            foreach($fluxes as $flux){
                $parts = $flux->getParts();
                $percentFlux = $flux->getPercentage();
                $nbreAnnuelParts = ($pva * $percentFlux)/100;
                foreach($parts as $part){
                    $unitMass = $part->getUnitMass();
                    $qty = $part->getQuantity();
                    $partMass = ($qty * $unitMass * $nbreAnnuelParts)/1000000;
                    $percentMass = ($partMass/$totalMass)*100;
                    $part->setPercentTotalMass($percentMass);
                    $percentMassFlux = $percentMassFlux + $percentMass;
                    $em->persist($part);
                }
                $flux->setTotalPercentMass($percentMassFlux);
                $percentMassFlux = 0;
                $em->persist($flux);
            }
        }
        $em->persist($project);
        $em->flush();
        
        return $totalMass;   
    }
    
    public function recalculatePvaParts(Project $project, $pva){
        $em = $this->em;
        
        $weight = $project->getWeight();
        $totalMass = ($weight * $pva)/1000000;
        $project->setTotalMass($totalMass);
        
        $scenarios = $project->getScenarios();
        $nbreAnnuelParts = 0;
        $emissionCO2Flux = 0;
        $emissionCO2Scenario = 0;
        
        foreach($scenarios as $scenario){
            $fluxes = $scenario->getFluxes();
            foreach($fluxes as $flux){
                $parts = $flux->getParts();
                $nbreAnnuelParts = ($pva * $flux->getPercentage())/100;
                foreach($parts as $part){
                    $quantity = $part->getQuantity();
                    $unitMass = $part->getUnitMass();
                    $typeTransport = $part->getTypeTransport();
                    $modeTransport = $part->getModeTransport();
                    
                    $tonnage = ($quantity * $unitMass * $nbreAnnuelParts)/1000000;
                    
                    if($typeTransport == "M"){
                        $distance = $part->getDistance();
                        if($distance == 0){
                            $distanceSaisie = $part->getDistanceSaisie();
                            $distance = $distanceSaisie;
                        }
                        //Recalcul Impact CO2
                        //AIR
                        if($modeTransport == 'air'){
                            $facteurEmission = 1151.3;
                        //RAIL    
                        } elseif($modeTransport == 'rail'){
                            $facteurEmission = 12.1;
                        //WATER    
                        } elseif($modeTransport == 'water'){
                            $facteurEmission = 29.7;
                        //ROUTE        
                        } else{
                            $facteurEmission = 106.3;
                        }
                        $emissionCO2 = ($tonnage * $distance * $facteurEmission)/1000000;
                        $part->setImpactEvt($emissionCO2);
                    } elseif($typeTransport == "N"){
                        $distance = $part->getDistance();
                        //Recalcul Impact CO2
                        //RAIL    
                        if($modeTransport == 'rail'){
                           $facteurEmission = 12.1;
                        //ROUTE        
                        } else{
                           $facteurEmission = 106.3;
                        }
                        $emissionCO2 = ($tonnage * $distance * $facteurEmission)/1000000;
                        $part->setImpactEvt($emissionCO2);
                    } elseif($typeTransport == "L"){
                        $distance = $part->getDistanceSaisie();
                        //Calcul Côut carburant + Impact CO2
                        //AIR
                        if($modeTransport == 'air'){
                           $facteurEmission = 1151.3;
                        //RAIL    
                        } elseif($modeTransport == 'rail'){
                           $facteurEmission = 12.1;
                        //WATER    
                        } elseif($modeTransport == 'water'){
                           $facteurEmission = 29.7;
                        //ROUTE        
                        } else{
                           $facteurEmission = 106.3;
                        }
                        $emissionCO2 = ($tonnage * $distance * $facteurEmission)/1000000;
                        $part->setImpactEvt($emissionCO2);
                    }
                    
                    $emissionCO2Flux = $emissionCO2Flux + $emissionCO2;
                    $part->setTonnage($tonnage);
                    $em->persist($part);
                }  
                $flux->setTotalImpact($emissionCO2Flux);
                $emissionCO2Scenario = $emissionCO2Scenario + $flux->getTotalImpact();
                $nbreAnnuelParts = 0;
                $emissionCO2Flux = 0;
                $em->persist($flux);
            }
            $scenario->setImpactCO2($emissionCO2Scenario);
            $emissionCO2Scenario = 0;
            $em->persist($scenario);
        }
        $em->persist($project);
        $em->flush();
        
        return $totalMass;
    }
    
    public function recalculatePart(Part $part){
        $em = $this->em;
        
        $flux = $part->getFlux();
        $scenario = $flux->getScenario();
        $project = $scenario->getProject();
        
        $pva = $project->getPva();
        $weight = $project->getWeight();
        $totalMass = ($weight * $pva)/1000000;
        $nbreAnnuelParts = ($pva * $flux->getPercentage())/100;
        
        $quantity = $part->getQuantity();
        $unitMass = $part->getUnitMass();
        $typeTransport = $part->getTypeTransport();
        
        $partMass = ($quantity * $unitMass * $nbreAnnuelParts)/1000000;
        $partMass = round($partMass, 1);
        $percentMass = ($partMass/$totalMass)*100;
        $percentMass = round($percentMass, 1);
        
        $part->setTotalMass($partMass);
        $part->setPercentTotalMass($percentMass);
        
        $tonnage = ($quantity * $unitMass * $nbreAnnuelParts)/1000000;
        $tonnage = round($tonnage, 1);
        $part->setTonnage($tonnage);
        
        $distance = $part->getDistance();
        
        if ($typeTransport == 'M') {
            if ($distance == 0) {
                $distanceSaisie = $part->getDistanceSaisie();
                $distance = $distanceSaisie;
            }
        }
        
        if($typeTransport == 'L'){
            $distance = $part->getDistanceSaisie();
        }
        
        $this->calculatePartCostAndImpact($part, $distance);
        
        $em->persist($part);
        $em->flush();
    }
    
    public function calculatePartCostAndImpact(Part $part, $distance){
        $modeTransport = $part->getModeTransport();
        $tonnage = $part->getTonnage();
        
        //Calcul Côut carburant + Impact CO2
        //AIR
        if ($modeTransport == 'air') {
            if ($distance > 1000) {
                $cost = 0.25 * $distance * $tonnage;
            }else{
                $cost = 0.25 * $distance * $tonnage;
            }
            $facteurEmission = 1151.3;
        //RAIL    
        } elseif ($modeTransport == 'rail') {
            if ($distance < 500) {
                $cost = 0.19 * $distance * $tonnage;
            } elseif ($distance >= 500 and $distance < 1000) {
                $cost = 0.19 * $distance * $tonnage;
            } else {
                $cost = 0;
            }
            $facteurEmission = 12.1;
        //WATER    
        } elseif ($modeTransport == 'water') {
            if ($distance < 5000) {
                $cost = 0.038 * $distance * $tonnage;
            } elseif ($distance >= 5000 and $distance < 10000) {
                $cost = 0.025 * $distance * $tonnage;
            } elseif ($distance >= 10000) {
                $cost = 0.019 * $distance * $tonnage;
            }
            $facteurEmission = 29.7;
        //ROUTE        
        } else {
            if ($distance < 500) {
                $cost = 0.26 * $distance * $tonnage;
            } elseif ($distance >= 500 and $distance < 1000) {
                $cost = 0.19 * $distance * $tonnage;
            } else {
                $cost = 0.15 * $distance * $tonnage;
            }
            $facteurEmission = 106.3;
        }
        
        $part->setPartCost($cost);

        $emissionCO2 = ($tonnage * $distance * $facteurEmission) / 1000000;
        $emissionCO2 = round($emissionCO2, 3);
        $part->setImpactEvt($emissionCO2);
    }
   
}
