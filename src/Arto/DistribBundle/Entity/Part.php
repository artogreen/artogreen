<?php
namespace Arto\DistribBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="distrib_part")
 * @ORM\HasLifecycleCallbacks
 */
class Part
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=32, nullable=true)
     */
    private $name;
    
    /**
     * @ORM\Column(name="reference", type="string", length=32, nullable=true)
     */
    private $reference;
    
    /**
     * @ORM\Column(name="typeTransport", type="string", length=32, nullable=false)
     */
    private $typeTransport;

    /**
     * @ORM\Column(name="modeTransport", type="string", length=32, nullable=true)
     */
    private $modeTransport;

    /**
     * @ORM\Column(name="quantity", type="decimal", nullable=true, scale=2)
     */
    private $quantity;

    /**
     * @ORM\Column(name="unitMass", type="decimal", nullable=true, scale=2)
     */
    private $unitMass;

    /**
     * @ORM\Column(name="percentTotalMass", type="decimal", nullable=true, scale=3)
     */
    private $percentTotalMass;

    /**
     * @ORM\Column(name="tonnage", type="decimal", nullable=true, scale=3)
     */
    private $tonnage;

    /**
     * @ORM\Column(name="distance", type="integer", nullable=true)
     */
    private $distance;
    
    /**
     * @ORM\Column(name="distance_saisie", type="integer", nullable=true)
     */
    private $distanceSaisie;
    
    /**
     * @ORM\Column(name="impactEvt", type="decimal", nullable=true, scale=3, options={"default" = 0})
     */
    private $impactEvt;
    
    /**
     * @ORM\Column(name="cost", type="integer", nullable=true)
     */
    private $partCost;
    
    /**
     * @ORM\Column(name="totalMass", type="decimal", nullable=true, scale=1)
     */
    private $totalMass;
    
    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Flux
     *
     * @ORM\ManyToOne(targetEntity="Flux", inversedBy="parts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="flux_id", referencedColumnName="id")
     * })
     */
    private $flux;

    /**
     * @var DistanceInter
     *
     * @ORM\ManyToOne(targetEntity="DistanceInter")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="distance_inter_id", referencedColumnName="id")
     * })
     */
    private $distanceInter;
    
    /**
     * @var DistanceInterSaisie
     *
     * @ORM\ManyToOne(targetEntity="DistanceInterSaisie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="distance_inter_saisie_id", referencedColumnName="id")
     * })
     */
    private $distanceInterSaisie;

    /**
     * @var DistanceNational
     *
     * @ORM\ManyToOne(targetEntity="DistanceNational")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="distance_national_id", referencedColumnName="id")
     * })
     */
    private $distanceNational;
    
    /**
     * @var DistanceLocal
     * @ORM\ManyToOne(targetEntity="DistanceLocal")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="distance_local_id", referencedColumnName="id")
     * })
     * 
     */
    private $distanceLocal;


    public function __construct()
    {

    }

    public function __toString()
    {
        return (string)$this->getId();
    } 

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set reference
     *
     * @param string $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set typeTransport
     *
     * @param string $typeTransport
     */
    public function setTypeTransport($typeTransport)
    {
        $this->typeTransport = $typeTransport;
    }

    /**
     * Get typeTransport
     *
     * @return string 
     */
    public function getTypeTransport()
    {
        return $this->typeTransport;
    }

    /**
     * Set modeTransport
     *
     * @param string $modeTransport
     */
    public function setModeTransport($modeTransport)
    {
        $this->modeTransport = $modeTransport;
    }

    /**
     * Get modeTransport
     *
     * @return string 
     */
    public function getModeTransport()
    {
        return $this->modeTransport;
    }

    /**
     * Set quantity
     *
     * @param decimal $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * Get quantity
     *
     * @return decimal 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set unitMass
     *
     * @param decimal $unitMass
     */
    public function setUnitMass($unitMass)
    {
        $this->unitMass = $unitMass;
    }

    /**
     * Get unitMass
     *
     * @return decimal 
     */
    public function getUnitMass()
    {
        return $this->unitMass;
    }

    /**
     * Set totalMass
     *
     * @param decimal $totalMass
     */
    public function setTotalMass($totalMass)
    {
        $this->totalMass = $totalMass;
    }

    /**
     * Get totalMass
     *
     * @return decimal 
     */
    public function getTotalMass()
    {
        return $this->totalMass;
    }
    
    /**
     * Set percentTotalMass
     *
     * @param decimal $percentTotalMass
     */
    public function setPercentTotalMass($percentTotalMass)
    {
        $this->percentTotalMass = $percentTotalMass;
    }

    /**
     * Get percentTotalMass
     *
     * @return decimal 
     */
    public function getPercentTotalMass()
    {
        return $this->percentTotalMass;
    }

    /**
     * Set tonnage
     *
     * @param decimal $tonnage
     */
    public function setTonnage($tonnage)
    {
        $this->tonnage = $tonnage;
    }

    /**
     * Get tonnage
     *
     * @return decimal 
     */
    public function getTonnage()
    {
        return $this->tonnage;
    }

    /**
     * Set distance
     *
     * @param integer $distance
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;
    }

    /**
     * Get distance
     *
     * @return integer 
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * Set distanceSaisie
     *
     * @param integer $distanceSaisie
     */
    public function setDistanceSaisie($distanceSaisie)
    {
        $this->distanceSaisie = $distanceSaisie;
    }

    /**
     * Get distanceSaisie
     *
     * @return integer 
     */
    public function getDistanceSaisie()
    {
        return $this->distanceSaisie;
    }

    /**
     * Set impactEvt
     *
     * @param decimal $impactEvt
     */
    public function setImpactEvt($impactEvt)
    {
        $this->impactEvt = $impactEvt;
    }

    /**
     * Get impactEvt
     *
     * @return decimal 
     */
    public function getImpactEvt()
    {
        return $this->impactEvt;
    }

    /**
     * Set partCost
     *
     * @param integer $partCost
     */
    public function setPartCost($partCost)
    {
        $this->partCost = $partCost;
    }

    /**
     * Get partCost
     *
     * @return integer 
     */
    public function getPartCost()
    {
        return $this->partCost;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set flux
     *
     * @param Arto\DistribBundle\Entity\Flux $flux
     */
    public function setFlux(\Arto\DistribBundle\Entity\Flux $flux)
    {
        $this->flux = $flux;
    }

    /**
     * Get flux
     *
     * @return Arto\DistribBundle\Entity\Flux 
     */
    public function getFlux()
    {
        return $this->flux;
    }

    /**
     * Set distanceInter
     *
     * @param Arto\DistribBundle\Entity\DistanceInter $distanceInter
     */
    public function setDistanceInter(\Arto\DistribBundle\Entity\DistanceInter $distanceInter = null)
    {
        $this->distanceInter = $distanceInter;
    }

    /**
     * Get distanceInter
     *
     * @return Arto\DistribBundle\Entity\DistanceInter 
     */
    public function getDistanceInter()
    {
        return $this->distanceInter;
    }

    /**
     * Set distanceInterSaisie
     *
     * @param Arto\DistribBundle\Entity\DistanceInterSaisie $distanceInterSaisie
     */
    public function setDistanceInterSaisie(\Arto\DistribBundle\Entity\DistanceInterSaisie $distanceInterSaisie = null)
    {
        $this->distanceInterSaisie = $distanceInterSaisie;
    }

    /**
     * Get distanceInterSaisie
     *
     * @return Arto\DistribBundle\Entity\DistanceInterSaisie 
     */
    public function getDistanceInterSaisie()
    {
        return $this->distanceInterSaisie;
    }

    /**
     * Set distanceNational
     *
     * @param Arto\DistribBundle\Entity\DistanceNational $distanceNational
     */
    public function setDistanceNational(\Arto\DistribBundle\Entity\DistanceNational $distanceNational = null)
    {
        $this->distanceNational = $distanceNational;
    }

    /**
     * Get distanceNational
     *
     * @return Arto\DistribBundle\Entity\DistanceNational 
     */
    public function getDistanceNational()
    {
        return $this->distanceNational;
    }

    /**
     * Set distanceLocal
     *
     * @param Arto\DistribBundle\Entity\DistanceLocal $distanceLocal
     */
    public function setDistanceLocal(\Arto\DistribBundle\Entity\DistanceLocal $distanceLocal = null)
    {
        $this->distanceLocal = $distanceLocal;
    }

    /**
     * Get distanceLocal
     *
     * @return Arto\DistribBundle\Entity\DistanceLocal 
     */
    public function getDistanceLocal()
    {
        return $this->distanceLocal;
    }
}