<?php
namespace Arto\DistribBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="distrib_flux")
 */
class Flux
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="label", type="string", length=255, nullable=false)
     */
    private $label;

    /**
     * @ORM\Column(name="percentage", type="integer", nullable=false)
     */
    private $percentage;
    
    /**
     * @ORM\Column(name="totalPercentMass", type="decimal", scale=1, nullable=false)
     */
    private $totalPercentMass;
    
    /**
     * @ORM\Column(name="totalMass", type="decimal", scale=1, nullable=false)
     */
    private $totalMass;
    
    /**
     * @ORM\Column(name="totalTonnage", type="decimal", scale=1, nullable=false)
     */
    private $totalTonnage;
    
    /**
     * @ORM\Column(name="totalImpact", type="decimal", scale=1, nullable=false)
     */
    private $totalImpact;
    
    /**
     * @ORM\Column(name="totalCost", type="integer", nullable=false)
     */
    private $totalCost;

    /**
     * @var Scenario
     *
     * @ORM\ManyToOne(targetEntity="Scenario", inversedBy="fluxes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="scenario_id", referencedColumnName="id")
     * })
     */
    private $scenario;

    /**
     * @ORM\OneToMany(targetEntity="Part", mappedBy="flux", cascade={"remove", "persist"})
     */
    private $parts;


    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getLabel();
    }
    
     /**
     * Get parent parts
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getParentParts()
    {
        $parents = array();
        foreach($this->parts as $part)
        if ($part->getParent() == null) {
            $parents[] = $part;
        }

        return $parents;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set percentage
     *
     * @param integer $percentage
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;
    }

    /**
     * Get percentage
     *
     * @return integer 
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Set totalPercentMass
     *
     * @param integer $totalPercentMass
     */
    public function setTotalPercentMass($totalPercentMass)
    {
        $this->totalPercentMass = $totalPercentMass;
    }

    /**
     * Get totalMass
     *
     * @return integer 
     */
    public function getTotalMass()
    {
        return $this->totalMass;
    }
    
     /**
     * Set totalMass
     *
     * @param integer $totalPercentMass
     */
    public function setTotalMass($totalPercentMass)
    {
        $this->totalMass = $totalPercentMass;
    }

    /**
     * Get totalPercentMass
     *
     * @return integer 
     */
    public function getTotalPercentMass()
    {
        return $this->totalPercentMass;
    }

    /**
     * Set totalTonnage
     *
     * @param integer $totalTonnage
     */
    public function setTotalTonnage($totalTonnage)
    {
        $this->totalTonnage = $totalTonnage;
    }

    /**
     * Get totalTonnage
     *
     * @return integer 
     */
    public function getTotalTonnage()
    {
        return $this->totalTonnage;
    }

    /**
     * Set totalCost
     *
     * @param integer $totalCost
     */
    public function setTotalCost($totalCost)
    {
        $this->totalCost = $totalCost;
    }

    /**
     * Get totalCost
     *
     * @return integer 
     */
    public function getTotalCost()
    {
        return $this->totalCost;
    }

    /**
     * Set scenario
     *
     * @param Arto\DistribBundle\Entity\Scenario $scenario
     */
    public function setScenario(\Arto\DistribBundle\Entity\Scenario $scenario)
    {
        $this->scenario = $scenario;
    }

    /**
     * Get scenario
     *
     * @return Arto\DistribBundle\Entity\Scenario 
     */
    public function getScenario()
    {
        return $this->scenario;
    }

    /**
     * Add parts
     *
     * @param Arto\DistribBundle\Entity\Part $parts
     */
    public function addPart(\Arto\DistribBundle\Entity\Part $parts)
    {
        $this->parts[] = $parts;
    }

    /**
     * Get parts
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getParts()
    {
        return $this->parts;
    }

    /**
     * Set totalImpact
     *
     * @param integer $totalImpact
     */
    public function setTotalImpact($totalImpact)
    {
        $this->totalImpact = $totalImpact;
    }

    /**
     * Get totalImpact
     *
     * @return integer 
     */
    public function getTotalImpact()
    {
        return $this->totalImpact;
    }
}