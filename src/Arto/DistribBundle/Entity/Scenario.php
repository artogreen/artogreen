<?php
namespace Arto\DistribBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="distrib_scenario")
 */
class Scenario
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="label", type="string", length=255, nullable=false)
     */
    private $label;
    
    /**
     * @ORM\Column(name="impactCO2", type="decimal", scale=2, nullable="true")
     */
    private $impactCO2;
    
     /**
     * @ORM\Column(name="cost", type="decimal", scale=2, nullable="true")
     */
    private $cost;
    
    /**
     * @ORM\Column(name="totalMass", type="decimal", nullable="true", scale=1)
     */
    private $totalMass;
    

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;

     /**
     * @ORM\OneToMany(targetEntity="Flux", mappedBy="scenario", cascade={"remove"})
     */
    private $fluxes;


    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getLabel();
    }
    
     /**
     * Get weight
     *
     * @return decimal
     */
    public function getWeightPercentageLeft()
    {
        if ($this->getTotalMass() > 0) {
            return ($this->getTotalMass() / $this->getProject()->getTotalMass()) * 100;
        } else {
            return 0;
        }
    }
    
    /**
     * Get weight
     *
     * @return decimal
     */
    public function getWeightStatusLeft()
    {
        if ($this->getTotalMass() > 0) {
            $percentage = ($this->getTotalMass() / $this->getProject()->getTotalMass()) * 100;
            if ($percentage > 105) {
                return 'danger';
            } elseif ($percentage > 95) {
                return 'success';
            } elseif ($percentage > 0) {
                return 'warning';
            }
        } else {
            return 'info';
        }
    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set label
     *
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set project
     *
     * @param Arto\DistribBundle\Entity\Project $project
     */
    public function setProject(\Arto\DistribBundle\Entity\Project $project)
    {
        $this->project = $project;
    }

    /**
     * Get project
     *
     * @return Arto\DistribBundle\Entity\Project
     */
    public function getProject()
    {
        return $this->project;
    }


    /**
     * Add fluxes
     *
     * @param Arto\DistribBundle\Entity\Flux $fluxes
     */
    public function addFlux(\Arto\DistribBundle\Entity\Flux $fluxes)
    {
        $this->fluxes[] = $fluxes;
    }

    /**
     * Get fluxes
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getFluxes()
    {
        return $this->fluxes;
    }

    /**
     * Set impactCO2
     *
     * @param decimal $impactCO2
     */
    public function setImpactCO2($impactCO2)
    {
        $this->impactCO2 = $impactCO2;
    }

    /**
     * Get impactCO2
     *
     * @return decimal
     */
    public function getImpactCO2()
    {
        return $this->impactCO2;
    }

    /**
     * Set cost
     *
     * @param decimal $cost
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    /**
     * Get cost
     *
     * @return decimal 
     */
    public function getCost()
    {
        return $this->cost;
    }
    
    /**
     * Set totalMass
     *
     * @param decimal $totalMass
     */
    public function setTotalMass($totalMass)
    {
        $this->totalMass = $totalMass;
    }

    /**
     * Get totalMass
     *
     * @return decimal 
     */
    public function getTotalMass()
    {
        return $this->totalMass;
    }
}