<?php

namespace Arto\DistribBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="distrib_distance_inter_saisie")
 */
class DistanceInterSaisie
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="km_air", type="integer", nullable=true)
     */
    private $kmAir;

    /**
     * @ORM\Column(name="km_road", type="integer", nullable=true)
     */
    private $kmRoad;

    /**
     * @ORM\Column(name="km_water", type="integer", nullable=true)
     */
    private $kmWater;

    /**
     * @ORM\Column(name="km_rail", type="integer", nullable=true)
     */
    private $kmRail;

   /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="departure_id", referencedColumnName="id")
     * })
     */
    private $departure;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="arrival_id", referencedColumnName="id")
     * })
     */
    private $arrival;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set kmAir
     *
     * @param integer $kmAir
     */
    public function setKmAir($kmAir)
    {
        $this->kmAir = $kmAir;
    }

    /**
     * Get kmAir
     *
     * @return integer 
     */
    public function getKmAir()
    {
        return $this->kmAir;
    }

    /**
     * Set kmRoad
     *
     * @param integer $kmRoad
     */
    public function setKmRoad($kmRoad)
    {
        $this->kmRoad = $kmRoad;
    }

    /**
     * Get kmRoad
     *
     * @return integer 
     */
    public function getKmRoad()
    {
        return $this->kmRoad;
    }

    /**
     * Set kmWater
     *
     * @param integer $kmWater
     */
    public function setKmWater($kmWater)
    {
        $this->kmWater = $kmWater;
    }

    /**
     * Get kmWater
     *
     * @return integer 
     */
    public function getKmWater()
    {
        return $this->kmWater;
    }

    /**
     * Set kmRail
     *
     * @param integer $kmRail
     */
    public function setKmRail($kmRail)
    {
        $this->kmRail = $kmRail;
    }

    /**
     * Get kmRail
     *
     * @return integer 
     */
    public function getKmRail()
    {
        return $this->kmRail;
    }

    /**
     * Set departure
     *
     * @param Arto\DistribBundle\Entity\Country $departure
     */
    public function setDeparture(\Arto\DistribBundle\Entity\Country $departure)
    {
        $this->departure = $departure;
    }

    /**
     * Get departure
     *
     * @return Arto\DistribBundle\Entity\Country 
     */
    public function getDeparture()
    {
        return $this->departure;
    }

    /**
     * Set arrival
     *
     * @param Arto\DistribBundle\Entity\Country $arrival
     */
    public function setArrival(\Arto\DistribBundle\Entity\Country $arrival)
    {
        $this->arrival = $arrival;
    }

    /**
     * Get arrival
     *
     * @return Arto\DistribBundle\Entity\Country 
     */
    public function getArrival()
    {
        return $this->arrival;
    }
}