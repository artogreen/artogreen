<?php
namespace Arto\DistribBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="distrib_test")
 */
class Test
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="fluxId", type="integer", nullable=false)
     */
    private $fluxId;
    
    /**
     * @ORM\Column(name="impactCO2", type="decimal", scale=3, nullable=false)
     */
    private $impactCO2;


    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getFluxId();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fluxId
     *
     * @param string $fluxId
     */
    public function setFluxId($fluxId)
    {
        $this->fluxId = $fluxId;
    }

    /**
     * Get fluxId
     *
     * @return integer
     */
    public function getFluxId()
    {
        return $this->fluxId;
    }

    /**
     * Set impactCO2
     *
     * @param integer $impactCO2
     */
    public function setImpactCO2($impactCO2)
    {
        $this->impactCO2 = $impactCO2;
    }

    /**
     * Get impactCO2
     *
     * @return integer 
     */
    public function getImpactCO2()
    {
        return $this->impactCO2;
    }

}