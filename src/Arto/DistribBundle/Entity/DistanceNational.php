<?php
namespace Arto\DistribBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="distrib_distance_national")
 */
class DistanceNational
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="km", type="integer", nullable=false)
     */
    private $km;

    /**
     * @var Department
     *
     * @ORM\ManyToOne(targetEntity="Department")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="departure_id", referencedColumnName="id")
     * })
     */
    private $departure;

    /**
     * @var Department
     *
     * @ORM\ManyToOne(targetEntity="Department")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="arrival_id", referencedColumnName="id")
     * })
     */
    private $arrival;


    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getLabel();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set km
     *
     * @param integer $km
     */
    public function setKm($km)
    {
        $this->km = $km;
    }

    /**
     * Get km
     *
     * @return integer 
     */
    public function getKm()
    {
        return $this->km;
    }

    /**
     * Set departure
     *
     * @param Arto\DistribBundle\Entity\Department $departure
     */
    public function setDeparture(\Arto\DistribBundle\Entity\Department $departure)
    {
        $this->departure = $departure;
    }

    /**
     * Get departure
     *
     * @return Arto\DistribBundle\Entity\Department 
     */
    public function getDeparture()
    {
        return $this->departure;
    }

    /**
     * Set arrival
     *
     * @param Arto\DistribBundle\Entity\Department $arrival
     */
    public function setArrival(\Arto\DistribBundle\Entity\Department $arrival)
    {
        $this->arrival = $arrival;
    }

    /**
     * Get arrival
     *
     * @return Arto\DistribBundle\Entity\Department 
     */
    public function getArrival()
    {
        return $this->arrival;
    }
}