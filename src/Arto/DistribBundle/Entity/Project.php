<?php
namespace Arto\DistribBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Arto\DistribBundle\Repository\ProjectRepository")
 * @ORM\Table(name="distrib_project")
 * @ORM\HasLifecycleCallbacks
 */
class Project
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="uniq", type="string", length=32, nullable=false)
     */
    private $uniq;

    /**
     * @ORM\Column(name="name", type="string", length=32, nullable=false)
     * @Assert\NotBlank()
     */
    private $name;
    
    /**
     * @ORM\Column(name="productName", type="string", length=32, nullable=true)
     * @Assert\NotBlank()
     */
    private $productName;   

    /**
     * @ORM\Column(name="pilote", type="string", length=255, nullable=true)
     */
    private $pilote;

    /**
     * @ORM\Column(name="pilote_date", type="date", nullable=true)
     */
    private $piloteDate;

    /**
     * @ORM\Column(name="weight", type="decimal", nullable=true, precision=60, scale=2)
     */
    private $weight;
    
    /**
     * @ORM\Column(name="totalMass", type="decimal", nullable="true", precision =60, scale=2)
     */
    private $totalMass;
    
    /**
     * @ORM\Column(name="objectifs", type="string", length=1000, nullable=true)
     */
    private $objectifs;
    
    /**
     * @ORM\Column(name="donneesLogistiques", type="string", length=1000, nullable=true)
     */
    private $donnéesLogistiques;
    
    /**
     * @ORM\Column(name="commentaires", type="string", length=1000, nullable=true)
     */
    private $commentaires;

    /**
     * @ORM\Column(name="pva", type="integer", nullable=true)
     */
    private $pva;

    /**
     * @var string $path
     *
     * @ORM\Column(name="path", type="string", length=100, nullable=true)
     */
    private $path;

    /**
     * @Assert\File(maxSize="10000000")
     */
    public $file;

    /**
     * to get the image path (slug + extension)
     */
    private $image;

    /**
     * a property used temporarily while deleting
     */
    private $filenameForRemove;


    /**
     * @ORM\Column(type="boolean")
     */
    private $active;
    
    /**
     * @ORM\Column(name="typeFlux", type="string", length=255, nullable=true)
     */
    private $typeFlux;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
    
    /**
     * @var Folder
     *
     * @ORM\ManyToOne(targetEntity="Folder", inversedBy="projects")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="folder_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $folder;

     /**
     * @ORM\OneToMany(targetEntity="Scenario", mappedBy="project", cascade={"remove", "persist"})
     */
    private $scenarios;
    
    /**
     * @ORM\OneToMany(targetEntity="Operation", mappedBy="project", cascade={"remove", "persist"})
     */
    private $notes;

    /**
     * @var GreenUser
     *
     * @ORM\ManyToOne(targetEntity="Arto\GreenBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="green_user_id", referencedColumnName="id")
     * })
     */
    private $greenUser;


    public function __construct()
    {
        $this->uniq = md5( uniqid('artodistrib', true) );
        $this->active = true;
    }

    public function __toString()
    {
        return $this->getName();
    }



    /**
     * Methods for upload
     */

    public function getAbsolutePath($image)
    {
        return null === $image ? null : $this->getUploadRootDir().'/'.$image;
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/distrib/project';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            $this->path = $this->file->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        } else {
            $this->file->move($this->getUploadRootDir(), $this->getImage());
            unset($this->file);
        }
    }

    /**
     * @ORM\PreRemove()
     */
    public function storeFilenameForRemove()
    {
        $this->filenameForRemove = $this->getAbsolutePath($this->image);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($this->filenameForRemove) {
            unlink($this->filenameForRemove);
        }
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uniq
     *
     * @param string $uniq
     */
    public function setUniq($uniq)
    {
        $this->uniq = $uniq;
    }

    /**
     * Get uniq
     *
     * @return string 
     */
    public function getUniq()
    {
        return $this->uniq;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set productName
     *
     * @param string $productName
     */
    public function setProductName($productName)
    {
        $this->productName = $productName;
    }

    /**
     * Get productName
     *
     * @return string 
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * Set pilote
     *
     * @param string $pilote
     */
    public function setPilote($pilote)
    {
        $this->pilote = $pilote;
    }

    /**
     * Get pilote
     *
     * @return string 
     */
    public function getPilote()
    {
        return $this->pilote;
    }

    /**
     * Set piloteDate
     *
     * @param date $piloteDate
     */
    public function setPiloteDate($piloteDate)
    {
        $this->piloteDate = $piloteDate;
    }

    /**
     * Get piloteDate
     *
     * @return date 
     */
    public function getPiloteDate()
    {
        return $this->piloteDate;
    }

    /**
     * Set weight
     *
     * @param decimal $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * Get weight
     *
     * @return decimal 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set pva
     *
     * @param integer $pva
     */
    public function setPva($pva)
    {
        $this->pva = $pva;
    }

    /**
     * Get pva
     *
     * @return integer 
     */
    public function getPva()
    {
        return $this->pva;
    }

    /**
     * Set path
     *
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set active
     *
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add scenarios
     *
     * @param Arto\DistribBundle\Entity\Scenario $scenarios
     */
    public function addScenario(\Arto\DistribBundle\Entity\Scenario $scenarios)
    {
        $this->scenarios[] = $scenarios;
    }

    /**
     * Get scenarios
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getScenarios()
    {
        return $this->scenarios;
    }
    
    /**
     * Add notes
     * 
     * @param Arto\DistribBundle\Entity\Operation $notes
     */
    public function addNote(\Arto\DistribBundle\Entity\Operation $notes)
    {
        $this->notes[] = $notes;
    }
    
    /**
     * Get notes
     * 
     * @return Doctrine\Common\Collections\Collection
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set greenUser
     *
     * @param Arto\GreenBundle\Entity\User $greenUser
     */
    public function setGreenUser(\Arto\GreenBundle\Entity\User $greenUser)
    {
        $this->greenUser = $greenUser;
    }

    /**
     * Get greenUser
     *
     * @return Arto\GreenBundle\Entity\User 
     */
    public function getGreenUser()
    {
        return $this->greenUser;
    }

    /**
     * Set typeFlux
     *
     * @param string $typeFlux
     */
    public function setTypeFlux($typeFlux)
    {
        $this->typeFlux = $typeFlux;
    }

    /**
     * Get typeFlux
     *
     * @return string 
     */
    public function getTypeFlux()
    {
        return $this->typeFlux;
    }

    /**
     * Set objectifs
     *
     * @param string $objectifs
     */
    public function setObjectifs($objectifs)
    {
        $this->objectifs = $objectifs;
    }

    /**
     * Get objectifs
     *
     * @return string 
     */
    public function getObjectifs()
    {
        return $this->objectifs;
    }

    /**
     * Set donnéesLogistiques
     *
     * @param string $donnéesLogistiques
     */
    public function setDonnéesLogistiques($donnéesLogistiques)
    {
        $this->donnéesLogistiques = $donnéesLogistiques;
    }

    /**
     * Get donnéesLogistiques
     *
     * @return string 
     */
    public function getDonnéesLogistiques()
    {
        return $this->donnéesLogistiques;
    }

    /**
     * Set commentaires
     *
     * @param string $commentaires
     */
    public function setCommentaires($commentaires)
    {
        $this->commentaires = $commentaires;
    }

    /**
     * Get commentaires
     *
     * @return string 
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }
    
        /**
     * Set folder
     *
     * @param Arto\DistribBundle\Entity\Folder $folder
     */
    public function setFolder(\Arto\DistribBundle\Entity\Folder $folder = null)
    {
        $this->folder = $folder;
    }

    /**
     * Get folder
     *
     * @return Arto\DistribBundle\Entity\Folder 
     */
    public function getFolder()
    {
        return $this->folder;
    }
    
    /**
     * Set totalMass
     *
     * @param decimal $totalMass
     */
    public function setTotalMass($totalMass)
    {
        $this->totalMass = $totalMass;
    }

    /**
     * Get totalMass
     *
     * @return decimal 
     */
    public function getTotalMass()
    {
        return $this->totalMass;
    }
}