<?php

namespace Arto\PingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Arto\PingBundle\Entity\Test;


class DefaultController extends Controller
{
  

    public function testAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        return $this->render('PingBundle:Default:test.html.twig',array(
            'tests' => $tests
        ));
    }
 
    public function indexAction()
    {   
        $em = $this->getDoctrine()->getEntityManager();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        return $this->render('PingBundle:Default:index.html.twig',array(
            'tests' => $tests
        ));
     }  
     public function subindexAction()
    {   
        $em = $this->getDoctrine()->getEntityManager();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        return $this->render('PingBundle:Default:subindex.html.twig',array(
            'tests' => $tests
        ));
     }  
       public function compositionAction()
    {   
        $em = $this->getDoctrine()->getEntityManager();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        return $this->render('PingBundle:Default:composition.html.twig',array(
            'tests' => $tests
        ));
     } 
       public function fiche_descriptiveAction()
    {   
        $em = $this->getDoctrine()->getEntityManager();
        $Location = $em->getRepository('PingBundle:Ping_Location')->findAll();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        return $this->render('PingBundle:Default:fiche_descriptive.html.twig',array(
            'Locations' => $Location,
            'tests' => $tests
        ));
     } 
     
     public function contexteAction()
    {   
        $em = $this->getDoctrine()->getEntityManager();
        $Transport = $em->getRepository('PingBundle:Ping_Transport')->findAll();
         $tests = $em->getRepository('PingBundle:Test')->findAll();
        return $this->render('PingBundle:Default:contexte.html.twig',array(
            'Transports' => $Transport,
             'tests' => $tests
        ));
     } 
     
    
      public function voierieAction()
    {   
        $em = $this->getDoctrine()->getEntityManager();
        $Materiaux = $em->getRepository('PingBundle:Ping_Materiaux')->findAll();
         $tests = $em->getRepository('PingBundle:Test')->findAll();
        return $this->render('PingBundle:Default:voierie.html.twig',array(
            'tests' => $tests,
            'Materiauxs' => $Materiaux
        ));
     }  
      public function luminaireAction()
    {   
        $em = $this->getDoctrine()->getEntityManager();
        $SourceEnergie = $em->getRepository('PingBundle:Ping_SourceEnergie')->findAll();
         $tests = $em->getRepository('PingBundle:Test')->findAll();
        return $this->render('PingBundle:Default:luminaire.html.twig',array(
            'SourceEnergies' => $SourceEnergie,
             'tests' => $tests
        ));
     }  
      public function reseauAction()
    {   
        $em = $this->getDoctrine()->getEntityManager();
        $SourceEnergie = $em->getRepository('PingBundle:Ping_SourceEnergie')->findAll();
        return $this->render('PingBundle:Default:reseau.html.twig',array(
            'SourceEnergies' => $SourceEnergie
        ));
     }  
      public function terrassementAction()
    {   
        $em = $this->getDoctrine()->getEntityManager();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        return $this->render('PingBundle:Default:terrassement.html.twig',array(
            'tests' => $tests
        ));
     }  
      public function vegetationAction()
    {   
        $em = $this->getDoctrine()->getEntityManager();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        return $this->render('PingBundle:Default:vegetation.html.twig',array(
            'tests' => $tests
        ));
     }  
      public function gestion_eauxAction()
    {   
        $em = $this->getDoctrine()->getEntityManager();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        return $this->render('PingBundle:Default:gestion_eaux.html.twig',array(
            'tests' => $tests
        ));
     }  
      public function gestion_dechetsAction()
    {   
        $em = $this->getDoctrine()->getEntityManager();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        return $this->render('PingBundle:Default:gestion_dechets.html.twig',array(
            'tests' => $tests
        ));
     }  
     
     public function maisonAction()
    {   
        $em = $this->getDoctrine()->getEntityManager();
        $Materiaux = $em->getRepository('PingBundle:Ping_Materiaux')->findAll();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        $SystemEnergetique = $em->getRepository('PingBundle:Ping_Systemenergetique')->findAll();
        return $this->render('PingBundle:Default:maison.html.twig',array(
            'Materiauxs' => $Materiaux,
            'tests' => $tests,
            'SystemEnergetiques' => $SystemEnergetique
        ));
     }
     
         public function immeubleAction()
    {   
        $em = $this->getDoctrine()->getEntityManager();
        $Materiaux = $em->getRepository('PingBundle:Ping_Materiaux')->findAll();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        $SystemEnergetique = $em->getRepository('PingBundle:Ping_Systemenergetique')->findAll();
        return $this->render('PingBundle:Default:immeuble.html.twig',array(
            'Materiauxs' => $Materiaux,
            'tests' => $tests,
            'SystemEnergetiques' => $SystemEnergetique
        ));
     }
     
     public function createCompo1Action()
     {
         $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        
        $composition = new \ping\PingBundle\Entity\Ping_Composition();
        $composition->setColNom($request->get('col_nom'));
        $composition->setColPrenom($request->get('col_prenom'));
        $composition->setColAdd($request->get('col_add'));
        $composition->setColMail($request->get('col_mail'));

        $em->persist($composition);
       // $composition->save();
        $em->flush();

        return $this->redirect($this->generateUrl('ping_composition'));
     }
     
    public function gymnaseAction()
    {   
         $em = $this->getDoctrine()->getEntityManager();
        $Materiaux = $em->getRepository('PingBundle:Ping_Materiaux')->findAll();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        $SystemEnergetique = $em->getRepository('PingBundle:Ping_Systemenergetique')->findAll();
        return $this->render('PingBundle:Default:gymnase.html.twig',array(
            'Materiauxs' => $Materiaux,
            'tests' => $tests,
            'SystemEnergetiques' => $SystemEnergetique
        ));
        
     }  
     
     public function crecheAction()
    {   
          $em = $this->getDoctrine()->getEntityManager();
        $Materiaux = $em->getRepository('PingBundle:Ping_Materiaux')->findAll();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        $SystemEnergetique = $em->getRepository('PingBundle:Ping_Systemenergetique')->findAll();
        return $this->render('PingBundle:Default:creche.html.twig',array(
            'Materiauxs' => $Materiaux,
            'tests' => $tests,
            'SystemEnergetiques' => $SystemEnergetique
        ));
        
     }  
     
          public function ecole_primaireAction()
    {    $em = $this->getDoctrine()->getEntityManager();
        $Materiaux = $em->getRepository('PingBundle:Ping_Materiaux')->findAll();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        $SystemEnergetique = $em->getRepository('PingBundle:Ping_Systemenergetique')->findAll();
        return $this->render('PingBundle:Default:ecole_primaire.html.twig',array(
            'Materiauxs' => $Materiaux,
            'tests' => $tests,
            'SystemEnergetiques' => $SystemEnergetique
        ));
        
     } 
      public function collegeAction()
    {    $em = $this->getDoctrine()->getEntityManager();
        $Materiaux = $em->getRepository('PingBundle:Ping_Materiaux')->findAll();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        $SystemEnergetique = $em->getRepository('PingBundle:Ping_Systemenergetique')->findAll();
        return $this->render('PingBundle:Default:college.html.twig',array(
            'Materiauxs' => $Materiaux,
            'tests' => $tests,
            'SystemEnergetiques' => $SystemEnergetique
        ));
        
     }
      public function vegetationlyceeAction()
    {   
        $em = $this->getDoctrine()->getEntityManager();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        return $this->render('PingBundle:Default:lycee.html.twig',array(
            'tests' => $tests
        ));
     } 

      public function bureauAction()
    {    $em = $this->getDoctrine()->getEntityManager();
        $Materiaux = $em->getRepository('PingBundle:Ping_Materiaux')->findAll();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        $SystemEnergetique = $em->getRepository('PingBundle:Ping_Systemenergetique')->findAll();
        return $this->render('PingBundle:Default:bureau.html.twig',array(
            'Materiauxs' => $Materiaux,
            'tests' => $tests,
            'SystemEnergetiques' => $SystemEnergetique
        ));
        
     }
     public function commercesAction()
    {    $em = $this->getDoctrine()->getEntityManager();
        $Materiaux = $em->getRepository('PingBundle:Ping_Materiaux')->findAll();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        $SystemEnergetique = $em->getRepository('PingBundle:Ping_Systemenergetique')->findAll();
        return $this->render('PingBundle:Default:commerces.html.twig',array(
            'Materiauxs' => $Materiaux,
            'tests' => $tests,
            'SystemEnergetiques' => $SystemEnergetique
        ));
        
     }   
   public function centre_commercialesAction()
    {    $em = $this->getDoctrine()->getEntityManager();
        $Materiaux = $em->getRepository('PingBundle:Ping_Materiaux')->findAll();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        $SystemEnergetique = $em->getRepository('PingBundle:Ping_Systemenergetique')->findAll();
        return $this->render('PingBundle:Default:centre_commerciales.html.twig',array(
            'Materiauxs' => $Materiaux,
            'tests' => $tests,
            'SystemEnergetiques' => $SystemEnergetique
        ));
        
     }
     public function cinemaAction()
    {    $em = $this->getDoctrine()->getEntityManager();
        $Materiaux = $em->getRepository('PingBundle:Ping_Materiaux')->findAll();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        $SystemEnergetique = $em->getRepository('PingBundle:Ping_Systemenergetique')->findAll();
        return $this->render('PingBundle:Default:cinema.html.twig',array(
            'Materiauxs' => $Materiaux,
            'tests' => $tests,
            'SystemEnergetiques' => $SystemEnergetique
        ));
        
     }
     public function bibliothequeAction()
    {    $em = $this->getDoctrine()->getEntityManager();
        $Materiaux = $em->getRepository('PingBundle:Ping_Materiaux')->findAll();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        $SystemEnergetique = $em->getRepository('PingBundle:Ping_Systemenergetique')->findAll();
        return $this->render('PingBundle:Default:bibliotheque.html.twig',array(
            'Materiauxs' => $Materiaux,
            'tests' => $tests,
            'SystemEnergetiques' => $SystemEnergetique
        ));
        
     } 
     public function hopitalAction()
    {    $em = $this->getDoctrine()->getEntityManager();
        $Materiaux = $em->getRepository('PingBundle:Ping_Materiaux')->findAll();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        $SystemEnergetique = $em->getRepository('PingBundle:Ping_Systemenergetique')->findAll();
        return $this->render('PingBundle:Default:hopital.html.twig',array(
            'Materiauxs' => $Materiaux,
            'tests' => $tests,
            'SystemEnergetiques' => $SystemEnergetique
        ));
        
     }
     public function lyceeAction()
     {
         $em = $this->getDoctrine()->getEntityManager();
        $Materiaux = $em->getRepository('PingBundle:Ping_Materiaux')->findAll();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        $SystemEnergetique = $em->getRepository('PingBundle:Ping_Systemenergetique')->findAll();
        return $this->render('PingBundle:Default:hopital.html.twig',array(
            'Materiauxs' => $Materiaux,
            'tests' => $tests,
            'SystemEnergetiques' => $SystemEnergetique
        ));
     }
             

     public function local_de_recyclageAction()
    {    $em = $this->getDoctrine()->getEntityManager();
        $Materiaux = $em->getRepository('PingBundle:Ping_Materiaux')->findAll();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        $SystemEnergetique = $em->getRepository('PingBundle:Ping_Systemenergetique')->findAll();
        return $this->render('PingBundle:Default:local_de_recyclage.html.twig',array(
            'Materiauxs' => $Materiaux,
            'tests' => $tests,
            'SystemEnergetiques' => $SystemEnergetique
        ));
        
     }
     public function salle_polyvalenteAction()
    {    $em = $this->getDoctrine()->getEntityManager();
        $Materiaux = $em->getRepository('PingBundle:Ping_Materiaux')->findAll();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        $SystemEnergetique = $em->getRepository('PingBundle:Ping_Systemenergetique')->findAll();
        return $this->render('PingBundle:Default:salle_polyvalente.html.twig',array(
            'Materiauxs' => $Materiaux,
            'tests' => $tests,
            'SystemEnergetiques' => $SystemEnergetique
        ));
        
     }
     public function maison_retraiteAction()
    {    $em = $this->getDoctrine()->getEntityManager();
        $Materiaux = $em->getRepository('PingBundle:Ping_Materiaux')->findAll();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        $SystemEnergetique = $em->getRepository('PingBundle:Ping_Systemenergetique')->findAll();
        return $this->render('PingBundle:Default:maison_retraite.html.twig',array(
            'Materiauxs' => $Materiaux,
            'tests' => $tests,
            'SystemEnergetiques' => $SystemEnergetique
        ));
        
     }
     public function buanderieAction()
    {    $em = $this->getDoctrine()->getEntityManager();
        $Materiaux = $em->getRepository('PingBundle:Ping_Materiaux')->findAll();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        $SystemEnergetique = $em->getRepository('PingBundle:Ping_Systemenergetique')->findAll();
        return $this->render('PingBundle:Default:buanderie.html.twig',array(
            'Materiauxs' => $Materiaux,
            'tests' => $tests,
            'SystemEnergetiques' => $SystemEnergetique
        ));
        
     }
     public function parkingAction()
    {    $em = $this->getDoctrine()->getEntityManager();
        $Materiaux = $em->getRepository('PingBundle:Ping_Materiaux')->findAll();
        $tests = $em->getRepository('PingBundle:Test')->findAll();
        $SystemEnergetique = $em->getRepository('PingBundle:Ping_Systemenergetique')->findAll();
        return $this->render('PingBundle:Default:parking.html.twig',array(
            'Materiauxs' => $Materiaux,
            'tests' => $tests,
            'SystemEnergetiques' => $SystemEnergetique
        ));
        
     }  
     
        public function createVoierieAction()
     {  
         $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        $senario = new \ping\PingBundle\Entity\Ping_Senario();
        $senario->setVoierie( $request->get('no'));
        $senario->setMateriaux($request->get('materiaux_voi'));
        $senario->setQuantVoie($request->get('quant_voi'));
        echo( $senario->setMateriaux($request->get('materiaux_voi')));
        $em->persist($senario);
        //$composition->save();
        $em->flush();
        return $this->redirect($this->generateUrl('ping_luminaire'));
     }
     
     public function createLuminaireAction()
     {
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        $senario = new \ping\PingBundle\Entity\Ping_Senario();
        $em->getClassMetadata($senario);
        $senario->setLuminaire('source_lumi');
        $senario->setQuanLumi('quan_lumi');
        $senario->setPuissance('puissance_lumi');
        $em->persist($senario);
        $em->flush();
        return $this->redirect($this->generateUrl('ping_luminaire'));
     }
}    
     
     

