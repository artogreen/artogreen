<?php

namespace Arto\PingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="ping_projet")
 */
class Ping_Projet
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idPing_projet;

    /**
     * @ORM\Column(type="string", length="255", name="nom", nullable="true")
     * 
     * @var string $nom
     */
    private $nom;

    public function __construct() {
        
    }

    public function __toString() {
        return $this->getNom();
    }

    /**
     * Get idPing_sourceenergie
     *
     * @return integer 
     */
    public function getIdPingProjet() {
        return $this->idPing_projet;
    }

    /**
     * Get nom
     * 
     * @return  string
     */
    public function getNom() {
        return $this->nom;
    }

    public function setNom($nom) {
        $this->nom = $nom;
    }

}