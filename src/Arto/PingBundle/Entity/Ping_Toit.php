<?php

namespace Arto\PingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="ping_toit")
 */
class Ping_Toit {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer $idPing_toit
     */
    protected $idPing_toit;

    /**
     * @ORM\ManyToOne(targetEntity="Ping_Materiaux")
     * @ORM\JoinColumn(name="idPing_mater", referencedColumnName="idPing_mater")
     * 
     * @var Ping_Materiaux $ping_materiaux
     */
    protected $idPing_mater;

    public function __construct() {
        
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getIdPing_toit() {
        return $this->idPing_toit;
    }

    /**
     * Gets idPing_mater
     * 
     * @return a ping_materiqux object
     */
    public function getIdPing_mater() {
        return $this->idPing_mater;
    }

    /**
     * Sets idPing_mater
     * 
     * @param Ping_Materiaux $value ping_materiaux
     */
    public function setIdPing_Mater(Ping_Materiaux $value) {
        $this->idPing_mater = $value;
    }

    /**
     * Get idPing_toit
     *
     * @return integer 
     */
    public function getIdPingToit() {
        return $this->idPing_toit;
    }

    /**
     * Set idPing_mater
     *
     * @param ping\PingBundle\Entity\Ping_Materiaux $idPingMater
     */
    public function setIdPingMater(\ping\PingBundle\Entity\Ping_Materiaux $idPingMater) {
        $this->idPing_mater = $idPingMater;
    }

    /**
     * Get idPing_mater
     *
     * @return ping\PingBundle\Entity\Ping_Materiaux 
     */
    public function getIdPingMater() {
        return $this->idPing_mater;
    }

}