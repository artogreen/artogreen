<?php

namespace Arto\PingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="ping_systemenergetique")
 */
class Ping_SystemEnergetique {
    //put your code here
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer $idPing_sysenerg
     */
    protected $idPing_sysenerg;
    
    /**
     * @ORM\Column(type="string", name="type",nullable="true")
     * 
     * @var string $type
     */
    protected $type;
    
     /**
     * @ORM\Column(type="string", name="unite",nullable="true")
     * 
     * @var string $unite
     */
    protected $unite;
    
    /**
     * @ORM\Column(type="float", name="energieprimaire_mj",nullable="true")
     * 
     * @var float $energieprimaire_mj
     */
    protected $energieprimaire_mj;
    
     /**
     * @ORM\Column(type="float", name="nonrenouvable_mj",nullable="true")
     * 
     * @var float $nonrenouvable_mj
     */
    protected $nonrenouvable_mj;
    
    /**
     * @ORM\Column(type="float", name="kgeqco2_mj",nullable="true")
     * 
     * @var float $kgeqco2_mj
     */
    protected $kgeqco2_mj;
    
    /**
     * @ORM\Column(type="float", name="ubp",nullable="true")
     * 
     * @var float $ubp
     */
    protected $ubp;
    
    /**
     * @ORM\Column(type="float", name="energieprimaire_kwh",nullable="true")
     * 
     * @var float $energieprimaire_kwh
     */
    protected $energieprimaire_kwh;
    
    /**
     * @ORM\Column(type="float", name="nonrenouvable",nullable="true")
     * 
     * @var float $nonrenouvable
     */
    protected $nonrenouvable_kwh;
    
    /**
     * @ORM\Column(type="float", name="kgeqco2",nullable="true")
     * 
     * @var float $kgeqco2
     */
    protected $kgeqco2_kwh;
    
    
    
    public function __construct()
    {
        
    }


    

    /**
     * Get idPing_sysenerg
     *
     * @return integer 
     */
    public function getIdPingSysenerg()
    {
        return $this->idPing_sysenerg;
    }

    /**
     * Set type
     *
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set unite
     *
     * @param string $unite
     */
    public function setUnite($unite)
    {
        $this->unite = $unite;
    }

    /**
     * Get unite
     *
     * @return string 
     */
    public function getUnite()
    {
        return $this->unite;
    }

    /**
     * Set energieprimaire_mj
     *
     * @param float $energieprimaireMj
     */
    public function setEnergieprimaireMj($energieprimaireMj)
    {
        $this->energieprimaire_mj = $energieprimaireMj;
    }

    /**
     * Get energieprimaire_mj
     *
     * @return float 
     */
    public function getEnergieprimaireMj()
    {
        return $this->energieprimaire_mj;
    }

    /**
     * Set nonrenouvable_mj
     *
     * @param float $nonrenouvableMj
     */
    public function setNonrenouvableMj($nonrenouvableMj)
    {
        $this->nonrenouvable_mj = $nonrenouvableMj;
    }

    /**
     * Get nonrenouvable_mj
     *
     * @return float 
     */
    public function getNonrenouvableMj()
    {
        return $this->nonrenouvable_mj;
    }

    /**
     * Set kgeqco2_mj
     *
     * @param float $kgeqco2Mj
     */
    public function setKgeqco2Mj($kgeqco2Mj)
    {
        $this->kgeqco2_mj = $kgeqco2Mj;
    }

    /**
     * Get kgeqco2_mj
     *
     * @return float 
     */
    public function getKgeqco2Mj()
    {
        return $this->kgeqco2_mj;
    }

    /**
     * Set ubp
     *
     * @param float $ubp
     */
    public function setUbp($ubp)
    {
        $this->ubp = $ubp;
    }

    /**
     * Get ubp
     *
     * @return float 
     */
    public function getUbp()
    {
        return $this->ubp;
    }

    /**
     * Set energieprimaire_kwh
     *
     * @param float $energieprimaireKwh
     */
    public function setEnergieprimaireKwh($energieprimaireKwh)
    {
        $this->energieprimaire_kwh = $energieprimaireKwh;
    }

    /**
     * Get energieprimaire_kwh
     *
     * @return float 
     */
    public function getEnergieprimaireKwh()
    {
        return $this->energieprimaire_kwh;
    }

    /**
     * Set nonrenouvable_kwh
     *
     * @param float $nonrenouvableKwh
     */
    public function setNonrenouvableKwh($nonrenouvableKwh)
    {
        $this->nonrenouvable_kwh = $nonrenouvableKwh;
    }

    /**
     * Get nonrenouvable_kwh
     *
     * @return float 
     */
    public function getNonrenouvableKwh()
    {
        return $this->nonrenouvable_kwh;
    }

    /**
     * Set kgeqco2_kwh
     *
     * @param float $kgeqco2Kwh
     */
    public function setKgeqco2Kwh($kgeqco2Kwh)
    {
        $this->kgeqco2_kwh = $kgeqco2Kwh;
    }

    /**
     * Get kgeqco2_kwh
     *
     * @return float 
     */
    public function getKgeqco2Kwh()
    {
        return $this->kgeqco2_kwh;
    }
}