<?php

namespace Arto\PingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="ping_transport")
 */
class Ping_Transport {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer $idPing_transport
     */
    protected $idPing_transport;

    /**
     * @ORM\Column(type="string", name="moyen",nullable="true")
     * 
     * @var string $moyen
     */
    protected $moyen;

    /**
     * @ORM\Column(type="float", name="g_eq_c",nullable="true")
     * 
     * @var float $g_eq_c
     */
    protected $g_eq_c;

    /**
     * @ORM\Column(type="float", name="kg_eq_c",nullable="true")
     * 
     * @var float $kg_eq_c
     */
    protected $kg_eq_c;

    /**
     * @ORM\Column(type="float", name="kg_eq_co2",nullable="true")
     * 
     * @var float $kg_eq_co2
     */
    protected $kg_eq_co2;

    /**
     * @ORM\Column(type="string", name="unite",nullable="true")
     * 
     * @var string $unite
     */
    protected $unite;

    public function __construct() {
        
    }

    /**
     * Get idPing_transport
     *
     * @return integer 
     */
    public function getIdPingTransport() {
        return $this->idPing_transport;
    }

    /**
     * Set moyen
     *
     * @param string $moyen
     */
    public function setMoyen($moyen) {
        $this->moyen = $moyen;
    }

    /**
     * Get moyen
     *
     * @return string 
     */
    public function getMoyen() {
        return $this->moyen;
    }

    /**
     * Set g_eq_c
     *
     * @param float $gEqC
     */
    public function setGEqC($gEqC) {
        $this->g_eq_c = $gEqC;
    }

    /**
     * Get g_eq_c
     *
     * @return float 
     */
    public function getGEqC() {
        return $this->g_eq_c;
    }

    /**
     * Set kg_eq_c
     *
     * @param float $kgEqC
     */
    public function setKgEqC($kgEqC) {
        $this->kg_eq_c = $kgEqC;
    }

    /**
     * Get kg_eq_c
     *
     * @return float 
     */
    public function getKgEqC() {
        return $this->kg_eq_c;
    }

    /**
     * Set kg_eq_co2
     *
     * @param float $kgEqCo2
     */
    public function setKgEqCo2($kgEqCo2) {
        $this->kg_eq_co2 = $kgEqCo2;
    }

    /**
     * Get kg_eq_co2
     *
     * @return float 
     */
    public function getKgEqCo2() {
        return $this->kg_eq_co2;
    }

    /**
     * Set unite
     *
     * @param string $unite
     */
    public function setUnite($unite) {
        $this->unite = $unite;
    }

    /**
     * Get unite
     *
     * @return string 
     */
    public function getUnite() {
        return $this->unite;
    }

}