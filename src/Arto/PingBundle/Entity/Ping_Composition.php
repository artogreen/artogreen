<?php

namespace Arto\PingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="ping_composition")
 */
class Ping_Composition {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer $idPing_composition
     */
    protected $idPing_composition;

    /**
     * @ORM\Column(type="string", name="col_nom",nullable="true")
     * 
     * @var string $col_nom
     */
    protected $col_nom;

    /**
     * @ORM\Column(type="string", name="col_prenom",nullable="true")
     * 
     * @var string $col_prenom
     */
    protected $col_prenom;

    /**
     * @ORM\Column(type="string", name="col_add",nullable="true")
     * 
     * @var string $col_add
     */
    protected $col_add;

    /**
     * @ORM\Column(type="string", name="col_mail",nullable="true")
     * 
     * @var string $col_mail
     */
    protected $col_mail;

    /**
     * @ORM\Column(type="string", name="mo1_nom",nullable="true")
     * 
     * @var string $mo1_nom
     */
    protected $mo1_nom;

    /**
     * @ORM\Column(type="string", name="mo1_prenom",nullable="true")
     * 
     * @var string $mo1_prenom
     */
    protected $mo1_prenom;

    /**
     * @ORM\Column(type="string", name="mo1_add",nullable="true")
     * 
     * @var string $mo1_add
     */
    protected $mo1_add;

    /**
     * @ORM\Column(type="string", name="mo1_mail",nullable="true")
     * 
     * @var string $mo1_mail
     */
    protected $mo1_mail;

    /**
     * @ORM\Column(type="string", name="moe_nom",nullable="true")
     * 
     * @var string $moe_nom
     */
    protected $moe_nom;

    /**
     * @ORM\Column(type="string", name="moe_prenom",nullable="true")
     * 
     * @var string $moe_prenom
     */
    protected $moe_prenom;

    /**
     * @ORM\Column(type="string", name="moe_add",nullable="true")
     * 
     * @var string $moe_add
     */
    protected $moe_add;

    /**
     * @ORM\Column(type="string", name="moe_mail",nullable="true")
     * 
     * @var string $moe_mail
     */
    protected $moe_mail;

    /**
     * @ORM\Column(type="string", name="be1_nom",nullable="true")
     * 
     * @var string $be1_nom
     */
    protected $be1_nom;

    /**
     * @ORM\Column(type="string", name="be1_prenom",nullable="true")
     * 
     * @var string $be1_prenom
     */
    protected $be1_prenom;

    /**
     * @ORM\Column(type="string", name="be1_add",nullable="true")
     * 
     * @var string $be1_add
     */
    protected $be1_add;

    /**
     * @ORM\Column(type="string", name="be1_mail",nullable="true")
     * 
     * @var string $be1_mail
     */
    protected $be1_mail;

    /**
     * @ORM\Column(type="string", name="be2_nom",nullable="true")
     * 
     * @var string $be2_nom
     */
    protected $be2_nom;

    /**
     * @ORM\Column(type="string", name="be2_prenom",nullable="true")
     * 
     * @var string $be2_prenom
     */
    protected $be2_prenom;

    /**
     * @ORM\Column(type="string", name="be2_add",nullable="true")
     * 
     * @var string $be2_add
     */
    protected $be2_add;

    /**
     * @ORM\Column(type="string", name="be2_mail",nullable="true")
     * 
     * @var string $be2_mail
     */
    protected $be2_mail;

    /**
     * @ORM\Column(type="string", name="opc_nom",nullable="true")
     * 
     * @var string $opc_nom
     */
    protected $opc_nom;

    /**
     * @ORM\Column(type="string", name="opc_prenom",nullable="true")
     * 
     * @var string $opc_prenom
     */
    protected $opc_prenom;

    /**
     * @ORM\Column(type="string", name="opc_add",nullable="true")
     * 
     * @var string $opc_add
     */
    protected $opc_add;

    /**
     * @ORM\Column(type="string", name="opc_mail",nullable="true")
     * 
     * @var string $opc_mail
     */
    protected $opc_mail;

    /**
     * @ORM\Column(type="string", name="mo2_nom",nullable="true")
     * 
     * @var string $mo2_nom
     */
    protected $mo2_nom;

    /**
     * @ORM\Column(type="string", name="mo2_prenom",nullable="true")
     * 
     * @var string $mo2_prenom
     */
    protected $mo2_prenom;

    /**
     * @ORM\Column(type="string", name="mo2_add",nullable="true")
     * 
     * @var string $mo2_add
     */
    protected $mo2_add;

    /**
     * @ORM\Column(type="string", name="mo2_mail",nullable="true")
     * 
     * @var string $mo2_mail
     */
    protected $mo2_mail;

    public function __construct() {
        
    }

    /**
     * Get idPing_composition
     *
     * @return integer 
     */
    public function getIdPingComposition() {
        return $this->idPing_composition;
    }

    /**
     * Set col_nom
     *
     * @param string $colNom
     */
    public function setColNom($colNom) {
        $this->col_nom = $colNom;
    }

    /**
     * Get col_nom
     *
     * @return string 
     */
    public function getColNom() {
        return $this->col_nom;
    }

    /**
     * Set col_prenom
     *
     * @param string $colPrenom
     */
    public function setColPrenom($colPrenom) {
        $this->col_prenom = $colPrenom;
    }

    /**
     * Get col_prenom
     *
     * @return string 
     */
    public function getColPrenom() {
        return $this->col_prenom;
    }

    /**
     * Set col_add
     *
     * @param string $colAdd
     */
    public function setColAdd($colAdd) {
        $this->col_add = $colAdd;
    }

    /**
     * Get col_add
     *
     * @return string 
     */
    public function getColAdd() {
        return $this->col_add;
    }

    /**
     * Set col_mail
     *
     * @param string $colMail
     */
    public function setColMail($colMail) {
        $this->col_mail = $colMail;
    }

    /**
     * Get col_mail
     *
     * @return string 
     */
    public function getColMail() {
        return $this->col_mail;
    }

    /**
     * Set mo1_nom
     *
     * @param string $mo1Nom
     */
    public function setMo1Nom($mo1Nom) {
        $this->mo1_nom = $mo1Nom;
    }

    /**
     * Get mo1_nom
     *
     * @return string 
     */
    public function getMo1Nom() {
        return $this->mo1_nom;
    }

    /**
     * Set mo1_prenom
     *
     * @param string $mo1Prenom
     */
    public function setMo1Prenom($mo1Prenom) {
        $this->mo1_prenom = $mo1Prenom;
    }

    /**
     * Get mo1_prenom
     *
     * @return string 
     */
    public function getMo1Prenom() {
        return $this->mo1_prenom;
    }

    /**
     * Set mo1_add
     *
     * @param string $mo1Add
     */
    public function setMo1Add($mo1Add) {
        $this->mo1_add = $mo1Add;
    }

    /**
     * Get mo1_add
     *
     * @return string 
     */
    public function getMo1Add() {
        return $this->mo1_add;
    }

    /**
     * Set mo1_mail
     *
     * @param string $mo1Mail
     */
    public function setMo1Mail($mo1Mail) {
        $this->mo1_mail = $mo1Mail;
    }

    /**
     * Get mo1_mail
     *
     * @return string 
     */
    public function getMo1Mail() {
        return $this->mo1_mail;
    }

    /**
     * Set moe_nom
     *
     * @param string $moeNom
     */
    public function setMoeNom($moeNom) {
        $this->moe_nom = $moeNom;
    }

    /**
     * Get moe_nom
     *
     * @return string 
     */
    public function getMoeNom() {
        return $this->moe_nom;
    }

    /**
     * Set moe_prenom
     *
     * @param string $moePrenom
     */
    public function setMoePrenom($moePrenom) {
        $this->moe_prenom = $moePrenom;
    }

    /**
     * Get moe_prenom
     *
     * @return string 
     */
    public function getMoePrenom() {
        return $this->moe_prenom;
    }

    /**
     * Set moe_add
     *
     * @param string $moeAdd
     */
    public function setMoeAdd($moeAdd) {
        $this->moe_add = $moeAdd;
    }

    /**
     * Get moe_add
     *
     * @return string 
     */
    public function getMoeAdd() {
        return $this->moe_add;
    }

    /**
     * Set moe_mail
     *
     * @param string $moeMail
     */
    public function setMoeMail($moeMail) {
        $this->moe_mail = $moeMail;
    }

    /**
     * Get moe_mail
     *
     * @return string 
     */
    public function getMoeMail() {
        return $this->moe_mail;
    }

    /**
     * Set be1_nom
     *
     * @param string $be1Nom
     */
    public function setBe1Nom($be1Nom) {
        $this->be1_nom = $be1Nom;
    }

    /**
     * Get be1_nom
     *
     * @return string 
     */
    public function getBe1Nom() {
        return $this->be1_nom;
    }

    /**
     * Set be1_prenom
     *
     * @param string $be1Prenom
     */
    public function setBe1Prenom($be1Prenom) {
        $this->be1_prenom = $be1Prenom;
    }

    /**
     * Get be1_prenom
     *
     * @return string 
     */
    public function getBe1Prenom() {
        return $this->be1_prenom;
    }

    /**
     * Set be1_add
     *
     * @param string $be1Add
     */
    public function setBe1Add($be1Add) {
        $this->be1_add = $be1Add;
    }

    /**
     * Get be1_add
     *
     * @return string 
     */
    public function getBe1Add() {
        return $this->be1_add;
    }

    /**
     * Set be1_mail
     *
     * @param string $be1Mail
     */
    public function setBe1Mail($be1Mail) {
        $this->be1_mail = $be1Mail;
    }

    /**
     * Get be1_mail
     *
     * @return string 
     */
    public function getBe1Mail() {
        return $this->be1_mail;
    }

    /**
     * Set be2_nom
     *
     * @param string $be2Nom
     */
    public function setBe2Nom($be2Nom) {
        $this->be2_nom = $be2Nom;
    }

    /**
     * Get be2_nom
     *
     * @return string 
     */
    public function getBe2Nom() {
        return $this->be2_nom;
    }

    /**
     * Set be2_prenom
     *
     * @param string $be2Prenom
     */
    public function setBe2Prenom($be2Prenom) {
        $this->be2_prenom = $be2Prenom;
    }

    /**
     * Get be2_prenom
     *
     * @return string 
     */
    public function getBe2Prenom() {
        return $this->be2_prenom;
    }

    /**
     * Set be2_add
     *
     * @param string $be2Add
     */
    public function setBe2Add($be2Add) {
        $this->be2_add = $be2Add;
    }

    /**
     * Get be2_add
     *
     * @return string 
     */
    public function getBe2Add() {
        return $this->be2_add;
    }

    /**
     * Set be2_mail
     *
     * @param string $be2Mail
     */
    public function setBe2Mail($be2Mail) {
        $this->be2_mail = $be2Mail;
    }

    /**
     * Get be2_mail
     *
     * @return string 
     */
    public function getBe2Mail() {
        return $this->be2_mail;
    }

    /**
     * Set opc_nom
     *
     * @param string $opcNom
     */
    public function setOpcNom($opcNom) {
        $this->opc_nom = $opcNom;
    }

    /**
     * Get opc_nom
     *
     * @return string 
     */
    public function getOpcNom() {
        return $this->opc_nom;
    }

    /**
     * Set opc_prenom
     *
     * @param string $opcPrenom
     */
    public function setOpcPrenom($opcPrenom) {
        $this->opc_prenom = $opcPrenom;
    }

    /**
     * Get opc_prenom
     *
     * @return string 
     */
    public function getOpcPrenom() {
        return $this->opc_prenom;
    }

    /**
     * Set opc_add
     *
     * @param string $opcAdd
     */
    public function setOpcAdd($opcAdd) {
        $this->opc_add = $opcAdd;
    }

    /**
     * Get opc_add
     *
     * @return string 
     */
    public function getOpcAdd() {
        return $this->opc_add;
    }

    /**
     * Set opc_mail
     *
     * @param string $opcMail
     */
    public function setOpcMail($opcMail) {
        $this->opc_mail = $opcMail;
    }

    /**
     * Get opc_mail
     *
     * @return string 
     */
    public function getOpcMail() {
        return $this->opc_mail;
    }

    /**
     * Set mo2_nom
     *
     * @param string $mo2Nom
     */
    public function setMo2Nom($mo2Nom) {
        $this->mo2_nom = $mo2Nom;
    }

    /**
     * Get mo2_nom
     *
     * @return string 
     */
    public function getMo2Nom() {
        return $this->mo2_nom;
    }

    /**
     * Set mo2_prenom
     *
     * @param string $mo2Prenom
     */
    public function setMo2Prenom($mo2Prenom) {
        $this->mo2_prenom = $mo2Prenom;
    }

    /**
     * Get mo2_prenom
     *
     * @return string 
     */
    public function getMo2Prenom() {
        return $this->mo2_prenom;
    }

    /**
     * Set mo2_add
     *
     * @param string $mo2Add
     */
    public function setMo2Add($mo2Add) {
        $this->mo2_add = $mo2Add;
    }

    /**
     * Get mo2_add
     *
     * @return string 
     */
    public function getMo2Add() {
        return $this->mo2_add;
    }

    /**
     * Set mo2_mail
     *
     * @param string $mo2Mail
     */
    public function setMo2Mail($mo2Mail) {
        $this->mo2_mail = $mo2Mail;
    }

    /**
     * Get mo2_mail
     *
     * @return string 
     */
    public function getMo2Mail() {
        return $this->mo2_mail;
    }

}