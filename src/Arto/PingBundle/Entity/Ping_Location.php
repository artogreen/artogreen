<?php

namespace Arto\PingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="ping_location")
 */
class Ping_Location {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer $idPing_location
     */
    protected $idPing_location;

    /**
     * @ORM\Column(type="string", name="departement",nullable="true")
     * 
     * @var string $departement
     */
    protected $departement;

    /**
     * @ORM\Column(type="string", name="region",nullable="true")
     * 
     * @var integer $region
     */
    protected $region;

    /**
     * @ORM\Column(type="float", name="coeff_clima",nullable="true")
     * 
     * @var float $coeff_clima
     */
    protected $coeff_clima;

    public function __construct() {
        
    }

    /**
     * Get idPing_location
     *
     * @return integer 
     */
    public function getIdPingLocation() {
        return $this->idPing_location;
    }

    /**
     * Set departement
     *
     * @param string $departement
     */
    public function setDepartement($departement) {
        $this->departement = $departement;
    }

    /**
     * Get departement
     *
     * @return string 
     */
    public function getDepartement() {
        return $this->departement;
    }

    /**
     * Set region
     *
     * @param string $region
     */
    public function setRegion($region) {
        $this->region = $region;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion() {
        return $this->region;
    }

    /**
     * Set coeff_clima
     *
     * @param float $coeffClima
     */
    public function setCoeffClima($coeffClima) {
        $this->coeff_clima = $coeffClima;
    }

    /**
     * Get coeff_clima
     *
     * @return float 
     */
    public function getCoeffClima() {
        return $this->coeff_clima;
    }

}