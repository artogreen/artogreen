<?php
namespace Arto\PingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="ping_fenetre")
 */
class Ping_Fenetre {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer $idPing_fenetre
     */
    protected $idPing_fenetre;

    /**
     * @ORM\ManyToOne(targetEntity="Ping_Materiaux")
     * @ORM\JoinColumn(name="idPing_mater", referencedColumnName="idPing_mater")
     * 
     * @var Ping_Materiaux $ping_materiaux
     */
    protected $idPing_mater;

    public function __construct() {
        
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getIdPing_fenetre() {
        return $this->idPing_fenetre;
    }

    /**
     * Gets idPing_mater
     * 
     * @return a ping_materiqux object
     */
    public function getIdPing_mater() {
        return $this->idPing_mater;
    }

    /**
     * Sets idPing_mater
     * 
     * @param Ping_Materiaux $value ping_materiaux
     */
    public function setIdPing_Mater(Ping_Materiaux $value) {
        $this->idPing_mater = $value;
    }

    /**
     * Get idPing_fenetre
     *
     * @return integer 
     */
    public function getIdPingFenetre() {
        return $this->idPing_fenetre;
    }

    /**
     * Set idPing_mater
     *
     * @param ping\PingBundle\Entity\Ping_Materiaux $idPingMater
     */
    public function setIdPingMater(\ping\PingBundle\Entity\Ping_Materiaux $idPingMater) {
        $this->idPing_mater = $idPingMater;
    }

    /**
     * Get idPing_mater
     *
     * @return ping\PingBundle\Entity\Ping_Materiaux 
     */
    public function getIdPingMater() {
        return $this->idPing_mater;
    }

}