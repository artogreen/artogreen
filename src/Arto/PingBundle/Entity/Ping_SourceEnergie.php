<?php

namespace Arto\PingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="ping_sourceenergi")
 */
class Ping_SourceEnergie {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPing_sourceenerg;

    /**
     * @ORM\Column(type="string", length="255", name="source_energie", nullable="true")
     * 
     * @var string $source_energie
     */
    private $source_energie;

    /**
     * @ORM\Column(type="float", name="unite_par_kwh",nullable="true")
     * 
     * @var float $unite_par_kwh
     */
    protected $unite_par_kwh;

    /**
     * @ORM\Column(type="float", name="unite_par_tep",nullable="true")
     * 
     * @var float $unite_par_tep
     */
    protected $unite_par_tep;

    /**
     * @ORM\Column(type="float", name="unite_par_litre",nullable="true")
     * 
     * @var float $unite_par_litre
     */
    protected $unite_par_litre;

    /**
     * @ORM\Column(type="float", name="unite_par_tonne",nullable="true")
     * 
     * @var float $unite_par_tonne
     */
    protected $unite_par_tonne;

    /**
     * @ORM\Column(type="float", name="unite_cop",nullable="true")
     * 
     * @var float $unite_cop
     */
    protected $unite_cop;

    /**
     * @ORM\Column(type="float", name="coeff_energ", nullable="true")
     * 
     * @var float $coeff_energ
     */
    protected $coeff_energ;

    /**
     * @ORM\Column(type="float", name="kwh_ef", nullable="true")
     * 
     * @var float $kwh_ef
     */
    protected $kwh_ef;

    /**
     * @ORM\Column(type="float", name="kwh_ep", nullable="true")
     * 
     * @var float $kwh_ep
     */
    protected $kwh_ep;

    /**
     * @ORM\Column(type="float", name="kwh_so2",nullable="true")
     * 
     * @var float $kwh_so2
     */
    protected $kwh_so2;

    /**
     * @ORM\Column(type="float", name="kwh_nox",nullable="true")
     * 
     * @var float $kwh_nox
     */
    protected $kwh_nox;

    /**
     * @ORM\Column(type="float", name="kwh_ap",nullable="true")
     * 
     * @var float $kwh_ap
     */
    protected $kwh_ap;

    public function __construct() {
        
    }

    public function __toString() {
        return $this->getSource_energie();
    }

    /**
     * Get idPing_sourceenergie
     *
     * @return integer 
     */
    public function getIdPingSourceenerg() {
        return $this->idPing_sourceenerg;
    }

    /**
     * Set source_energie
     *
     * @param string $sourceEnergie
     */
    public function setSourceEnergie($sourceEnergie) {
        $this->source_energie = $sourceEnergie;
    }

    /**
     * Get source_energie
     *
     * @return string 
     */
    public function getSourceEnergie() {
        return $this->source_energie;
    }

    /**
     * Set unite_par_kwh
     *
     * @param float $uniteParKwh
     */
    public function setUniteParKwh($uniteParKwh) {
        $this->unite_par_kwh = $uniteParKwh;
    }

    /**
     * Get unite_par_kwh
     *
     * @return float
     */
    public function getUniteParKwh() {
        return $this->unite_par_kwh;
    }

    /**
     * Set unite_par_tep
     *
     * @param float $uniteParTep
     */
    public function setUniteParTep($uniteParTep) {
        $this->unite_par_tep = $uniteParTep;
    }

    /**
     * Get unite_par_tep
     *
     * @return float 
     */
    public function getUniteParTep() {
        return $this->unite_par_tep;
    }

    /**
     * Set unite_par_litre
     *
     * @param float $uniteParLitre
     */
    public function setUniteParLitre($uniteParLitre) {
        $this->unite_par_litre = $uniteParLitre;
    }

    /**
     * Get unite_par_litre
     *
     * @return float 
     */
    public function getUniteParLitre() {
        return $this->unite_par_litre;
    }

    /**
     * Set unite_par_tonne
     *
     * @param float $uniteParTonne
     */
    public function setUniteParTonne($uniteParTonne) {
        $this->unite_par_tonne = $uniteParTonne;
    }

    /**
     * Get unite_par_tonne
     *
     * @return float 
     */
    public function getUniteParTonne() {
        return $this->unite_par_tonne;
    }

    /**
     * Set unite_cop
     *
     * @param float $uniteCop
     */
    public function setUniteCop($uniteCop) {
        $this->unite_cop = $uniteCop;
    }

    /**
     * Get unite_cop
     *
     * @return float 
     */
    public function getUniteCop() {
        return $this->unite_cop;
    }

    /**
     * Set coeff_energ
     *
     * @param float $coeffEnerg
     */
    public function setCoeffEnerg($coeffEnerg) {
        $this->coeff_energ = $coeffEnerg;
    }

    /**
     * Get coeff_energ
     *
     * @return float
     */
    public function getCoeffEnerg() {
        return $this->coeff_energ;
    }

    /**
     * Set kwh_ef
     *
     * @param float $kwhEf
     */
    public function setKwhEf($kwhEf) {
        $this->kwh_ef = $kwhEf;
    }

    /**
     * Get kwh_ef
     *
     * @return float 
     */
    public function getKwhEf() {
        return $this->kwh_ef;
    }

    /**
     * Set kwh_ep
     *
     * @param float $kwhEp
     */
    public function setKwhEp($kwhEp) {
        $this->kwh_ep = $kwhEp;
    }

    /**
     * Get kwh_ep
     *
     * @return float
     */
    public function getKwhEp() {
        return $this->kwh_ep;
    }

    /**
     * Set kwh_so2
     *
     * @param float $kwhSo2
     */
    public function setKwhSo2($kwhSo2) {
        $this->kwh_so2 = $kwhSo2;
    }

    /**
     * Get kwh_so2
     *
     * @return float 
     */
    public function getKwhSo2() {
        return $this->kwh_so2;
    }

    /**
     * Set kwh_nox
     *
     * @param float $kwhNox
     */
    public function setKwhNox($kwhNox) {
        $this->kwh_nox = $kwhNox;
    }

    /**
     * Get kwh_nox
     *
     * @return float 
     */
    public function getKwhNox() {
        return $this->kwh_nox;
    }

    /**
     * Set kwh_ap
     *
     * @param float $kwhap
     */
    public function setKwhAp($kwhAp) {
        $this->kwh_ap = $kwhAp;
    }

    /**
     * Get kwh_ap
     *
     * @return float 
     */
    public function getKwhAp() {
        return $this->kwh_ap;
    }

}