<?php

namespace Arto\PingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * 
 * @ORM\Entity
 * @ORM\Table(name="ping_maison")
 */
class Ping_Maison {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer $idPing_mai
     */
    protected $idPing_mai;

    /**
     * @ORM\Column(type="integer", name="surface", nullable="true")
     * 
     * @var integer $surface
     */
    protected $surface;

    /**
     * @ORM\Column(type="integer", name="surfac_toit", nullable="true")
     * 
     * @var integer $surface_toit
     */
    protected $surface_toit;

    /**
     * @ORM\Column(type="integer", name="surface_sol", nullable="true")
     * 
     * @var integer $surface_sol
     */
    protected $surface_sol;

    /**
     * @ORM\Column(type="integer", name="qti_etage", nullable="true")
     * 
     * @var integer $qti_etage
     */
    protected $qti_etage;

    /**
     * @ORM\Column(type="integer", name="surface_jardin",nullable="true")
     * 
     * @var integer $surface_jardin
     */
    protected $surface_jardin;

    /**
     * @ORM\Column(type="integer", name="surface_sous_sol",nullable="true")
     * 
     * @var integer $surface_sous_sol
     */
    protected $surface_sous_sol;

    /**
     * @ORM\Column(type="integer", name="surface_parking",nullable="true")
     * 
     * @var integer $surface_parking
     */
    protected $surface_parking;

    /**
     * @ORM\ManyToOne(targetEntity="Ping_Fondation")
     * @ORM\JoinColumn(name="idPing_fond", referencedColumnName="idPing_fond")
     * 
     * @var Ping_Fondation $ping_fondation 
     */
    protected $idfond;

    /**
     * @ORM\ManyToOne(targetEntity="Ping_Ossature")
     * @ORM\JoinColumn(name="idPing_ossature", referencedColumnName="idPing_ossature")
     * 
     * @var Ping_Ossature $ping_ossature 
     */
    protected $idossat;

    /**
     * @ORM\ManyToOne(targetEntity="Ping_Fasade")
     * @ORM\JoinColumn(name="idPing_fasade", referencedColumnName="idPing_fasade")
     * 
     * @var Ping_Fasade $ping_fasade 
     */
    protected $idfasa;

    /**
     * @ORM\ManyToOne(targetEntity="Ping_Cloison")
     * @ORM\JoinColumn(name="idPing_cloison", referencedColumnName="idPing_cloison")
     * 
     * @var Ping_Cloison $ping_cloison 
     */
    protected $idclois;

    /**
     * @ORM\ManyToOne(targetEntity="Ping_SystemEnergetique")
     * @ORM\JoinColumn(name="idPing_sysenerg", referencedColumnName="idPing_sysenerg")
     * 
     * @var Ping_SystemEnergetique $ping_systemenergetique 
     */
    protected $idsys_energ;

    /**
     * @ORM\ManyToOne(targetEntity="Ping_Toit")
     * @ORM\JoinColumn(name="idPing_toit", referencedColumnName="idPing_toit")
     * 
     * @var Ping_Toit $ping_toit 
     */
    protected $idtoit;

    /**
     * @ORM\ManyToOne(targetEntity="Ping_Fenetre")
     * @ORM\JoinColumn(name="idPing_fenetre", referencedColumnName="idPing_fenetre")
     * 
     * @var Ping_Fenetre $ping_fenetre 
     */
    protected $id_fenetre;

    /**
     * @ORM\Column(type="integer", name="qti_fond",nullable="true")
     * 
     * @var integer $qti_fond
     */
    protected $qti_fond;

    /**
     * @ORM\Column(type="integer", name="qti_ossat",nullable="true")
     * 
     * @var integer $qti_ossat
     */
    protected $qti_ossat;

    /**
     * @ORM\Column(type="integer", name="qti_fasa",nullable="true")
     * 
     * @var integer $qti_fasa
     */
    protected $qti_fasa;

    /**
     * @ORM\Column(type="integer", name="qti_clois",nullable="true")
     * 
     * @var integer $qti_clois
     */
    protected $qti_clois;

    /**
     * @ORM\Column(type="integer", name="qti_sys_energ",nullable="true")
     * 
     * @var integer $qti_sys_energ
     */
    protected $qti_sys_energ;

    /**
     * @ORM\Column(type="integer", name="qti_fenet",nullable="true")
     * 
     * @var integer $qti_fenet
     */
    protected $qti_fenet;

    /**
     * @ORM\Column(type="integer", name="qti_voiture",nullable="true")
     * 
     * @var integer $qti_voiture
     */
    protected $qti_voiture;

    public function __construct() {
        
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getIdPing_mai() {
        return $this->idPing_mai;
    }

    /**
     * Gets surface
     * 
     * @return integer surface
     */
    public function getSurface() {
        return $this->surface;
    }

    /**
     * Sets surface
     * 
     * @param integer $value chauffage
     */
    public function setSurface($value) {
        $this->surface = $value;
    }

    /**
     * Gets surface_toit
     * 
     * @return integer surface_toit
     */
    public function getSurface_toit() {
        return $this->surface_toit;
    }

    /**
     * Sets surface_toit
     * 
     * @param integer $value surface_toit
     */
    public function setSurface_toit($value) {
        $this->surface_toit = $value;
    }

    /**
     * Gets surface_sol
     * 
     * @return integer surface_sol
     */
    public function getSurface_sol() {
        return $this->surface_sol;
    }

    /**
     * Sets surface_sol
     * 
     * @param integer $value surface_sol
     */
    public function setSurface_sol($value) {
        $this->surface_sol = $value;
    }

    /**
     * Gets qti_etage
     * 
     * @return integer qti_etage
     */
    public function getQti_etage() {
        return $this->qti_etage;
    }

    /**
     * Sets qti_etage
     * 
     * @param integer $value qti_etage
     */
    public function setQti_etage($value) {
        $this->qti_etage = $value;
    }

    /**
     * Gets surface_jardin
     * 
     * @return integer surface_jardin
     */
    public function getSurface_jardin() {
        return $this->surface_jardin;
    }

    /**
     * Sets surface_jardin
     * 
     * @param integer $value surface_jardin
     */
    public function setSurface_jardin($value) {
        $this->surface_jardin = $value;
    }

    /**
     * Gets surface_sous_sol
     * 
     * @return integer surface_sous_sol
     */
    public function getSurface_sous_sol() {
        return $this->surface_sous_sol;
    }

    /**
     * Sets surface_sous_sol
     * 
     * @param integer $value surface_sous_sol
     */
    public function setSurface_sous_sol($value) {
        $this->surface_sous_sol = $value;
    }

    /**
     * Gets surface_parking
     * 
     * @return integer surface_parking
     */
    public function getSurface_parking() {
        return $this->surface_parking;
    }

    /**
     * Sets surface_parking
     * 
     * @param integer $value surface_parking 
     */
    public function setSurface_parking($value) {
        $this->surface_parking = $value;
    }

    /**
     * Gets idfond
     * 
     * @return a ping_fondation object
     */
    public function getIdfond() {
        return $this->idfond;
    }

    /**
     * Sets idfond
     * 
     * @param Ping_Fondation $value ping_fondation
     */
    public function setIdfond(Ping_Fondation $value) {
        $this->idfond = $value;
    }

    /**
     * Gets idossat
     * 
     * @return a ping_ossature object
     */
    public function getIdossat() {
        return $this->idossat;
    }

    /**
     * Sets idossat
     * 
     * @param Ping_Ossature $value ping_ossature
     */
    public function setIdossat(Ping_Ossature $value) {
        $this->idossat = $value;
    }

    /**
     * Gets idfasa
     * 
     * @return a ping_fasade object
     */
    public function getIdfasa() {
        return $this->idfasa;
    }

    /**
     * Sets idfasa
     * 
     * @param Ping_Fasade $value ping_fasade
     */
    public function setIdfasa(Ping_Fasade $value) {
        $this->idfasa = $value;
    }

    /**
     * Gets idclois
     * 
     * @return a ping_cloison object
     */
    public function getIdclois() {
        return $this->idclois;
    }

    /**
     * Sets idclois
     * 
     * @param Ping_Cloison $value ping_cloison
     */
    public function setIdclois(Ping_Cloison $value) {
        $this->idclois = $value;
    }

    /**
     * Gets idsys_energ
     * 
     * @return a ping_systemenergetique object
     */
    public function getIdsys_energ() {
        return $this->idsys_energ;
    }

    /**
     * Sets idsys_energ
     * 
     * @param Ping_SystemEnergetique $value ping_systemenergetique
     */
    public function setIdsys_energ(Ping_SystemEnergetique $value) {
        $this->idsys_energ = $value;
    }

    /**
     * Gets idtoit
     * 
     * @return a ping_toit object
     */
    public function getIdtoit() {
        return $this->idtoit;
    }

    /**
     * Sets idtoit
     * 
     * @param Ping_Toit $value ping_toit
     */
    public function setIdtoit(Ping_Toit $value) {
        $this->idtoit = $value;
    }

    /**
     * Gets id_fenetre
     * 
     * @return a ping_fenetre object
     */
    public function getId_fenetre() {
        return $this->id_fenetre;
    }

    /**
     * Sets id_fenetre
     * 
     * @param Ping_Fenetre $value ping_fenetre
     */
    public function setId_fenetre(Ping_Fenetre $value) {
        $this->id_fenetre = $value;
    }

    /**
     * Gets qti_fond
     * 
     * @return integer qti_fond
     */
    public function getQti_fond() {
        return $this->qti_fond;
    }

    /**
     * Sets qti_fond
     * 
     * @param integer $value qti_fond
     */
    public function setQti_fond($value) {
        $this->qti_fond = $value;
    }

    /**
     * Gets qti_ossat
     * 
     * @return integer qti_ossat
     */
    public function getQti_ossat() {
        return $this->qti_ossat;
    }

    /**
     * Sets qti_ossat
     * 
     * @param integer $value qti_ossat
     */
    public function setQti_ossat($value) {
        $this->qti_ossat = $value;
    }

    /**
     * Gets qti_fasa
     * 
     * @return integer qti_fasa
     */
    public function getQti_fasa() {
        return $this->qti_fasa;
    }

    /**
     * Sets qti_fasa
     * 
     * @param integer $value qti_fasa
     */
    public function setQti_fasa($value) {
        $this->qti_fasa = $value;
    }

    /**
     * Gets qti_clois
     * 
     * @return integer qti_clois
     */
    public function getQti_clois() {
        return $this->qti_clois;
    }

    /**
     * Sets qti_clois
     * 
     * @param integer $value qti_clois
     */
    public function setQti_clois($value) {
        $this->qti_clois = $value;
    }

    /**
     * Gets qti_sys_energ
     * 
     * @return integer qti_sys_energ
     */
    public function getQti_sys_energ() {
        return $this->qti_sys_energ;
    }

    /**
     * Sets qti_sys_energ
     * 
     * @param integer $value qti_sys_energ
     */
    public function setQti_sys_energ($value) {
        $this->qti_sys_energ = $value;
    }

    /**
     * Gets qti_fenet
     * 
     * @return integer qti_fenet
     */
    public function getQti_fenet() {
        return $this->qti_fenet;
    }

    /**
     * Sets qti_fenet
     * 
     * @param integer $value qti_fenet
     */
    public function setQti_fenet($value) {
        $this->qti_fenet = $value;
    }

    /**
     * Gets qti_voiture
     * 
     * @return integer qti_voiture
     */
    public function getQti_voiture() {
        return $this->qti_voiture;
    }

    /**
     * Sets qti_voiture
     * 
     * @param integer $value qti_voiture
     */
    public function setQti_voiture($value) {
        $this->qti_voiture = $value;
    }

    /**
     * Get idPing_mai
     *
     * @return integer 
     */
    public function getIdPingMai() {
        return $this->idPing_mai;
    }

    /**
     * Set surface_toit
     *
     * @param integer $surfaceToit
     */
    public function setSurfaceToit($surfaceToit) {
        $this->surface_toit = $surfaceToit;
    }

    /**
     * Get surface_toit
     *
     * @return integer 
     */
    public function getSurfaceToit() {
        return $this->surface_toit;
    }

    /**
     * Set surface_sol
     *
     * @param integer $surfaceSol
     */
    public function setSurfaceSol($surfaceSol) {
        $this->surface_sol = $surfaceSol;
    }

    /**
     * Get surface_sol
     *
     * @return integer 
     */
    public function getSurfaceSol() {
        return $this->surface_sol;
    }

    /**
     * Set qti_etage
     *
     * @param integer $qtiEtage
     */
    public function setQtiEtage($qtiEtage) {
        $this->qti_etage = $qtiEtage;
    }

    /**
     * Get qti_etage
     *
     * @return integer 
     */
    public function getQtiEtage() {
        return $this->qti_etage;
    }

    /**
     * Set surface_jardin
     *
     * @param integer $surfaceJardin
     */
    public function setSurfaceJardin($surfaceJardin) {
        $this->surface_jardin = $surfaceJardin;
    }

    /**
     * Get surface_jardin
     *
     * @return integer 
     */
    public function getSurfaceJardin() {
        return $this->surface_jardin;
    }

    /**
     * Set surface_sous_sol
     *
     * @param integer $surfaceSousSol
     */
    public function setSurfaceSousSol($surfaceSousSol) {
        $this->surface_sous_sol = $surfaceSousSol;
    }

    /**
     * Get surface_sous_sol
     *
     * @return integer 
     */
    public function getSurfaceSousSol() {
        return $this->surface_sous_sol;
    }

    /**
     * Set surface_parking
     *
     * @param integer $surfaceParking
     */
    public function setSurfaceParking($surfaceParking) {
        $this->surface_parking = $surfaceParking;
    }

    /**
     * Get surface_parking
     *
     * @return integer 
     */
    public function getSurfaceParking() {
        return $this->surface_parking;
    }

    /**
     * Set qti_fond
     *
     * @param integer $qtiFond
     */
    public function setQtiFond($qtiFond) {
        $this->qti_fond = $qtiFond;
    }

    /**
     * Get qti_fond
     *
     * @return integer 
     */
    public function getQtiFond() {
        return $this->qti_fond;
    }

    /**
     * Set qti_ossat
     *
     * @param integer $qtiOssat
     */
    public function setQtiOssat($qtiOssat) {
        $this->qti_ossat = $qtiOssat;
    }

    /**
     * Get qti_ossat
     *
     * @return integer 
     */
    public function getQtiOssat() {
        return $this->qti_ossat;
    }

    /**
     * Set qti_fasa
     *
     * @param integer $qtiFasa
     */
    public function setQtiFasa($qtiFasa) {
        $this->qti_fasa = $qtiFasa;
    }

    /**
     * Get qti_fasa
     *
     * @return integer 
     */
    public function getQtiFasa() {
        return $this->qti_fasa;
    }

    /**
     * Set qti_clois
     *
     * @param integer $qtiClois
     */
    public function setQtiClois($qtiClois) {
        $this->qti_clois = $qtiClois;
    }

    /**
     * Get qti_clois
     *
     * @return integer 
     */
    public function getQtiClois() {
        return $this->qti_clois;
    }

    /**
     * Set qti_sys_energ
     *
     * @param integer $qtiSysEnerg
     */
    public function setQtiSysEnerg($qtiSysEnerg) {
        $this->qti_sys_energ = $qtiSysEnerg;
    }

    /**
     * Get qti_sys_energ
     *
     * @return integer 
     */
    public function getQtiSysEnerg() {
        return $this->qti_sys_energ;
    }

    /**
     * Set qti_fenet
     *
     * @param integer $qtiFenet
     */
    public function setQtiFenet($qtiFenet) {
        $this->qti_fenet = $qtiFenet;
    }

    /**
     * Get qti_fenet
     *
     * @return integer 
     */
    public function getQtiFenet() {
        return $this->qti_fenet;
    }

    /**
     * Set qti_voiture
     *
     * @param integer $qtiVoiture
     */
    public function setQtiVoiture($qtiVoiture) {
        $this->qti_voiture = $qtiVoiture;
    }

    /**
     * Get qti_voiture
     *
     * @return integer 
     */
    public function getQtiVoiture() {
        return $this->qti_voiture;
    }

    /**
     * Set idsys_energ
     *
     * @param ping\PingBundle\Entity\Ping_SystemEnergetique $idsysEnerg
     */
    public function setIdsysEnerg(\ping\PingBundle\Entity\Ping_SystemEnergetique $idsysEnerg) {
        $this->idsys_energ = $idsysEnerg;
    }

    /**
     * Get idsys_energ
     *
     * @return ping\PingBundle\Entity\Ping_SystemEnergetique 
     */
    public function getIdsysEnerg() {
        return $this->idsys_energ;
    }

    /**
     * Set id_fenetre
     *
     * @param ping\PingBundle\Entity\Ping_SystemEnergetique $idFenetre
     */
    public function setIdFenetre(\ping\PingBundle\Entity\Ping_SystemEnergetique $idFenetre) {
        $this->id_fenetre = $idFenetre;
    }

    /**
     * Get id_fenetre
     *
     * @return ping\PingBundle\Entity\Ping_SystemEnergetique 
     */
    public function getIdFenetre() {
        return $this->id_fenetre;
    }

}