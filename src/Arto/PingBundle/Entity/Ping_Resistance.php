<?php

namespace Arto\PingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="ping_resistance_thermique")
 */
class Ping_Resistance {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer $idPing_resistance
     */
    protected $idPing_resistance;

    /**
     * @ORM\Column(type="string", name="moyen",nullable="true")
     * 
     * @var string $moyen
     */
    protected $resist_therm_ref;

    /**
     * @ORM\Column(type="string", name="unite",nullable="true")
     * 
     * @var string $unite
     */
    protected $unite;

    /**
     * @ORM\Column(type="float", name="rt2005min",nullable="true")
     * 
     * @var float $rt2005min
     */
    protected $rt2005min;

    /**
     * @ORM\Column(type="float", name="rt2005max",nullable="true")
     * 
     * @var float $rt2005max
     */
    protected $rt2005max;

    /**
     * @ORM\Column(type="float", name="hpemin",nullable="true")
     * 
     * @var float $hpemin
     */
    protected $hpemin;

    /**
     * @ORM\Column(type="float", name="hpemax",nullable="true")
     * 
     * @var float $hpemax
     */
    protected $hpemax;

    /**
     * @ORM\Column(type="float", name="thpemin",nullable="true")
     * 
     * @var float $thpemin
     */
    protected $thpemin;

    /**
     * @ORM\Column(type="float", name="thpemax",nullable="true")
     * 
     * @var float $thpemax
     */
    protected $thpemax;

    /**
     * @ORM\Column(type="float", name="thperenovationmin",nullable="true")
     * 
     * @var float $thperenovationmin
     */
    protected $thperenovationmin;

    /**
     * @ORM\Column(type="float", name="thperenovationmax",nullable="true")
     * 
     * @var float $thperenovationmax
     */
    protected $thperenovationmax;

    /**
     * @ORM\Column(type="float", name="thpeenrmin",nullable="true")
     * 
     * @var float $thpeenrmin
     */
    protected $thpeenrmin;

    /**
     * @ORM\Column(type="float", name="thpeenrmax",nullable="true")
     * 
     * @var float $thpeenrmax
     */
    protected $thpeenrmax;

    /**
     * @ORM\Column(type="float", name="bbcmin",nullable="true")
     * 
     * @var float $bbcmin
     */
    protected $bbcmin;

    /**
     * @ORM\Column(type="float", name="bbcmax",nullable="true")
     * 
     * @var float $bbcmax
     */
    protected $bbcmax;

    /**
     * @ORM\Column(type="float", name="bbcrenovationmin",nullable="true")
     * 
     * @var float $bbcrenovationmin
     */
    protected $bbcrenovationmin;

    /**
     * @ORM\Column(type="float", name="bbcrenovationmax",nullable="true")
     * 
     * @var float $bbcrenovationmax
     */
    protected $bbcrenovationmax;

    /**
     * @ORM\Column(type="float", name="maison3litremin",nullable="true")
     * 
     * @var float $maison3litremin
     */
    protected $maison3litremin;

    /**
     * @ORM\Column(type="float", name="maison3litremax",nullable="true")
     * 
     * @var float $maison3litremax
     */
    protected $maison3litremax;

    /**
     * @ORM\Column(type="float", name="passivhausmin",nullable="true")
     * 
     * @var float $passivhausmin
     */
    protected $passivhausmin;

    /**
     * @ORM\Column(type="float", name="passivhausmax",nullable="true")
     * 
     * @var float $passivhausmax
     */
    protected $passivhausmax;

    /**
     * @ORM\Column(type="float", name="energyplusmin",nullable="true")
     * 
     * @var float $energyplusmin
     */
    protected $energplusmin;

    /**
     * @ORM\Column(type="float", name="energyplusmax",nullable="true")
     * 
     * @var float $energyplusmax
     */
    protected $energyplusmax;

    /**
     * @ORM\Column(type="float", name="minergiemin",nullable="true")
     * 
     * @var float $minergiemin
     */
    protected $minergiemin;

    /**
     * @ORM\Column(type="float", name="minergiemax",nullable="true")
     * 
     * @var float $minergiemax
     */
    protected $minergiemax;

    /**
     * @ORM\Column(type="float", name="minergiemoinsmin",nullable="true")
     * 
     * @var float $minergiemoinsmin
     */
    protected $minergiemoinsmin;

    /**
     * @ORM\Column(type="float", name="minergiemoinsmax",nullable="true")
     * 
     * @var float $minergiemoinsmax
     */
    protected $minergiemoinsmax;

    public function __construct() {
        
    }

    /**
     * Get idPing_resistance
     *
     * @return integer 
     */
    public function getIdPingResistance() {
        return $this->idPing_resistance;
    }

    /**
     * Set resist_therm_ref
     *
     * @param string $resistThermRef
     */
    public function setResistThermRef($resistThermRef) {
        $this->resist_therm_ref = $resistThermRef;
    }

    /**
     * Get resist_therm_ref
     *
     * @return string 
     */
    public function getResistThermRef() {
        return $this->resist_therm_ref;
    }

    /**
     * Set unite
     *
     * @param string $unite
     */
    public function setUnite($unite) {
        $this->unite = $unite;
    }

    /**
     * Get unite
     *
     * @return string 
     */
    public function getUnite() {
        return $this->unite;
    }

    /**
     * Set rt2005min
     *
     * @param float $rt2005min
     */
    public function setRt2005min($rt2005min) {
        $this->rt2005min = $rt2005min;
    }

    /**
     * Get rt2005min
     *
     * @return float 
     */
    public function getRt2005min() {
        return $this->rt2005min;
    }

    /**
     * Set rt2005max
     *
     * @param float $rt2005max
     */
    public function setRt2005max($rt2005max) {
        $this->rt2005max = $rt2005max;
    }

    /**
     * Get rt2005max
     *
     * @return float 
     */
    public function getRt2005max() {
        return $this->rt2005max;
    }

    /**
     * Set hpemin
     *
     * @param float $hpemin
     */
    public function setHpemin($hpemin) {
        $this->hpemin = $hpemin;
    }

    /**
     * Get hpemin
     *
     * @return float 
     */
    public function getHpemin() {
        return $this->hpemin;
    }

    /**
     * Set hpemax
     *
     * @param float $hpemax
     */
    public function setHpemax($hpemax) {
        $this->hpemax = $hpemax;
    }

    /**
     * Get hpemax
     *
     * @return float 
     */
    public function getHpemax() {
        return $this->hpemax;
    }

    /**
     * Set thpemin
     *
     * @param float $thpemin
     */
    public function setThpemin($thpemin) {
        $this->thpemin = $thpemin;
    }

    /**
     * Get thpemin
     *
     * @return float 
     */
    public function getThpemin() {
        return $this->thpemin;
    }

    /**
     * Set thpemax
     *
     * @param float $thpemax
     */
    public function setThpemax($thpemax) {
        $this->thpemax = $thpemax;
    }

    /**
     * Get thpemax
     *
     * @return float 
     */
    public function getThpemax() {
        return $this->thpemax;
    }

    /**
     * Set thperenovationmin
     *
     * @param float $thperenovationmin
     */
    public function setThperenovationmin($thperenovationmin) {
        $this->thperenovationmin = $thperenovationmin;
    }

    /**
     * Get thperenovationmin
     *
     * @return float 
     */
    public function getThperenovationmin() {
        return $this->thperenovationmin;
    }

    /**
     * Set thperenovationmax
     *
     * @param float $thperenovationmax
     */
    public function setThperenovationmax($thperenovationmax) {
        $this->thperenovationmax = $thperenovationmax;
    }

    /**
     * Get thperenovationmax
     *
     * @return float 
     */
    public function getThperenovationmax() {
        return $this->thperenovationmax;
    }

    /**
     * Set thpeenrmin
     *
     * @param float $thpeenrmin
     */
    public function setThpeenrmin($thpeenrmin) {
        $this->thpeenrmin = $thpeenrmin;
    }

    /**
     * Get thpeenrmin
     *
     * @return float 
     */
    public function getThpeenrmin() {
        return $this->thpeenrmin;
    }

    /**
     * Set thpeenrmax
     *
     * @param float $thpeenrmax
     */
    public function setThpeenrmax($thpeenrmax) {
        $this->thpeenrmax = $thpeenrmax;
    }

    /**
     * Get thpeenrmax
     *
     * @return float 
     */
    public function getThpeenrmax() {
        return $this->thpeenrmax;
    }

    /**
     * Set bbcmin
     *
     * @param float $bbcmin
     */
    public function setBbcmin($bbcmin) {
        $this->bbcmin = $bbcmin;
    }

    /**
     * Get bbcmin
     *
     * @return float 
     */
    public function getBbcmin() {
        return $this->bbcmin;
    }

    /**
     * Set bbcmax
     *
     * @param float $bbcmax
     */
    public function setBbcmax($bbcmax) {
        $this->bbcmax = $bbcmax;
    }

    /**
     * Get bbcmax
     *
     * @return float 
     */
    public function getBbcmax() {
        return $this->bbcmax;
    }

    /**
     * Set bbcrenovationmin
     *
     * @param float $bbcrenovationmin
     */
    public function setBbcrenovationmin($bbcrenovationmin) {
        $this->bbcrenovationmin = $bbcrenovationmin;
    }

    /**
     * Get bbcrenovationmin
     *
     * @return float 
     */
    public function getBbcrenovationmin() {
        return $this->bbcrenovationmin;
    }

    /**
     * Set bbcrenovationmax
     *
     * @param float $bbcrenovationmax
     */
    public function setBbcrenovationmax($bbcrenovationmax) {
        $this->bbcrenovationmax = $bbcrenovationmax;
    }

    /**
     * Get bbcrenovationmax
     *
     * @return float 
     */
    public function getBbcrenovationmax() {
        return $this->bbcrenovationmax;
    }

    /**
     * Set maison3litremin
     *
     * @param float $maison3litremin
     */
    public function setMaison3litremin($maison3litremin) {
        $this->maison3litremin = $maison3litremin;
    }

    /**
     * Get maison3litremin
     *
     * @return float 
     */
    public function getMaison3litremin() {
        return $this->maison3litremin;
    }

    /**
     * Set maison3litremax
     *
     * @param float $maison3litremax
     */
    public function setMaison3litremax($maison3litremax) {
        $this->maison3litremax = $maison3litremax;
    }

    /**
     * Get maison3litremax
     *
     * @return float 
     */
    public function getMaison3litremax() {
        return $this->maison3litremax;
    }

    /**
     * Set passivhausmin
     *
     * @param float $passivhausmin
     */
    public function setPassivhausmin($passivhausmin) {
        $this->passivhausmin = $passivhausmin;
    }

    /**
     * Get passivhausmin
     *
     * @return float 
     */
    public function getPassivhausmin() {
        return $this->passivhausmin;
    }

    /**
     * Set passivhausmax
     *
     * @param float $passivhausmax
     */
    public function setPassivhausmax($passivhausmax) {
        $this->passivhausmax = $passivhausmax;
    }

    /**
     * Get passivhausmax
     *
     * @return float 
     */
    public function getPassivhausmax() {
        return $this->passivhausmax;
    }

    /**
     * Set energplusmin
     *
     * @param float $energplusmin
     */
    public function setEnergplusmin($energplusmin) {
        $this->energplusmin = $energplusmin;
    }

    /**
     * Get energplusmin
     *
     * @return float 
     */
    public function getEnergplusmin() {
        return $this->energplusmin;
    }

    /**
     * Set energyplusmax
     *
     * @param float $energyplusmax
     */
    public function setEnergyplusmax($energyplusmax) {
        $this->energyplusmax = $energyplusmax;
    }

    /**
     * Get energyplusmax
     *
     * @return float 
     */
    public function getEnergyplusmax() {
        return $this->energyplusmax;
    }

    /**
     * Set minergiemin
     *
     * @param float $minergiemin
     */
    public function setMinergiemin($minergiemin) {
        $this->minergiemin = $minergiemin;
    }

    /**
     * Get minergiemin
     *
     * @return float 
     */
    public function getMinergiemin() {
        return $this->minergiemin;
    }

    /**
     * Set minergiemax
     *
     * @param float $minergiemax
     */
    public function setMinergiemax($minergiemax) {
        $this->minergiemax = $minergiemax;
    }

    /**
     * Get minergiemax
     *
     * @return float 
     */
    public function getMinergiemax() {
        return $this->minergiemax;
    }

    /**
     * Set minergiemoinsmin
     *
     * @param float $minergiemoinsmin
     */
    public function setMinergiemoinsmin($minergiemoinsmin) {
        $this->minergiemoinsmin = $minergiemoinsmin;
    }

    /**
     * Get minergiemoinsmin
     *
     * @return float 
     */
    public function getMinergiemoinsmin() {
        return $this->minergiemoinsmin;
    }

    /**
     * Set minergiemoinsmax
     *
     * @param float $minergiemoinsmax
     */
    public function setMinergiemoinsmax($minergiemoinsmax) {
        $this->minergiemoinsmax = $minergiemoinsmax;
    }

    /**
     * Get minergiemoinsmax
     *
     * @return float 
     */
    public function getMinergiemoinsmax() {
        return $this->minergiemoinsmax;
    }

}