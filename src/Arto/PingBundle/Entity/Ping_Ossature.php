<?php

namespace Arto\PingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="ping_ossature")
 */
class Ping_Ossature {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idPing_ossature;

    /**
     * @ORM\ManyToOne(targetEntity="Ping_Materiaux")
     * @ORM\JoinColumn(name="idPing_mater", referencedColumnName="idPing_mater")
     * 
     * @var Ping_Materiaux $ping_materiaux
     */
    protected $idPing_mater;

    public function __construct() {
        
    }

    public function __toString() {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set idPing_mater
     *
     * @param ping\PingBundle\Entity\Ping_Materiaux $idPingMater
     */
    public function setIdPingMater(\ping\PingBundle\Entity\Ping_Materiaux $idPingMater) {
        $this->idPing_mater = $idPingMater;
    }

    /**
     * Get idPing_mater
     *
     * @return ping\PingBundle\Entity\Ping_Materiaux 
     */
    public function getIdPingMater() {
        return $this->idPing_mater;
    }

    /**
     * Get idPing_Ossature
     *
     * @return integer 
     */
    public function getIdPingOssature() {
        return $this->idPing_Ossature;
    }

}