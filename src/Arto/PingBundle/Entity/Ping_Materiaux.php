<?php

namespace Arto\PingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="ping_materiaux")
 */
class Ping_Materiaux {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @var integer $idPing_mater
     */
    protected $idPing_mater;

    /**
     * @ORM\Column(type="string", length="255", name="type", nullable="true")
     * 
     * @var string $type
     */
    protected $type;

    /**
     * @ORM\Column(type="integer", name="densite_moy", nullable="true")
     * 
     * @var integer $densite_moy
     */
    protected $densite_moy;

    /**
     * @ORM\Column(type="integer", name="conducivite_termic", nullable="true")
     * 
     * @var integer $conducivite_termic
     */
    protected $conducivite_termic;

    /**
     * @ORM\Column(type="integer", name="chaleur_speci", nullable="true")
     * 
     * @var integer $chaleur_speci
     */
    protected $chaleur_speci;

    /**
     * @ORM\Column(type="integer", name="capacit_termic", nullable="true")
     * 
     * @var integer $capacit_termic
     */
    protected $capacit_termic;

    /**
     * @ORM\Column(type="integer", name="difusivite", nullable="true")
     * 
     * @var integer $difusivite
     */
    protected $difusivite;

    /**
     * @ORM\Column(type="integer", name="effusivite", nullable="true")
     * 
     * @var integer $effusivite
     */
    protected $effusivite;

    public function __construct() {
        
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getIdPing_mater() {
        return $this->idPing_mater;
    }

    /**
     * Gets type
     * 
     * @return string type
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Sets type
     * 
     * @param string $value type
     */
    public function setType($value) {
        $this->type = $value;
    }

    /**
     * Gets densite_moy
     * 
     * @return integer densite_moy
     */
    public function getDensite_moy() {
        return $this->densite_moy;
    }

    /**
     * Sets densite_moy
     * 
     * @param integer $value densite_moy
     */
    public function setDensite_moy($value) {
        $this->densite_moy = $value;
    }

    /**
     * Gets conducivite_termic
     * 
     * @return integer conducivite_termic
     */
    public function getConducivite_termic() {
        return $this->conducivite_termic;
    }

    /**
     * Sets conducivite_termic
     * 
     * @param integer $value conducivite_termic
     */
    public function setConducivite_termic($value) {
        $this->conducivite_termic = $value;
    }

    /**
     * Gets chaleur_speci
     * 
     * @return integer chaleur_speci
     */
    public function getChaleur_speci() {
        return $this->chaleur_speci;
    }

    /**
     * Sets chaleur_speci
     * 
     * @param integer $value chaleur_speci
     */
    public function setChaleur_speci($value) {
        $this->chaleur_speci = $value;
    }

    /**
     * Gets capacit_termic
     * 
     * @return integer capacit_termic
     */
    public function getCapacit_termic() {
        return $this->capacit_termic;
    }

    /**
     * Sets capacit_termic
     * 
     * @param integer $value capacit_termic
     */
    public function setCapacit_termic($value) {
        $this->capacit_termic = $value;
    }

    /**
     * Gets difusitivite
     * 
     * @return integer difusivite
     */
    public function getDifusivite() {
        return $this->difusivite;
    }

    /**
     * Sets difusivite
     * 
     * @param integer $value difusivite
     */
    public function setDifusivite($value) {
        $this->difusivite = $value;
    }

    /**
     * Gets effusivite
     * 
     * @return integer effusivite
     */
    public function getEffusivite() {
        return $this->effusivite;
    }

    /**
     * Sets effusivite
     * 
     * @param integer $value effusivite
     */
    public function setEffusivite($value) {
        $this->effusivite = $value;
    }

    /**
     * Get idPing_mater
     *
     * @return integer 
     */
    public function getIdPingMater() {
        return $this->idPing_mater;
    }

    /**
     * Set densite_moy
     *
     * @param integer $densiteMoy
     */
    public function setDensiteMoy($densiteMoy) {
        $this->densite_moy = $densiteMoy;
    }

    /**
     * Get densite_moy
     *
     * @return integer 
     */
    public function getDensiteMoy() {
        return $this->densite_moy;
    }

    /**
     * Set conducivite_termic
     *
     * @param integer $conduciviteTermic
     */
    public function setConduciviteTermic($conduciviteTermic) {
        $this->conducivite_termic = $conduciviteTermic;
    }

    /**
     * Get conducivite_termic
     *
     * @return integer 
     */
    public function getConduciviteTermic() {
        return $this->conducivite_termic;
    }

    /**
     * Set chaleur_speci
     *
     * @param integer $chaleurSpeci
     */
    public function setChaleurSpeci($chaleurSpeci) {
        $this->chaleur_speci = $chaleurSpeci;
    }

    /**
     * Get chaleur_speci
     *
     * @return integer 
     */
    public function getChaleurSpeci() {
        return $this->chaleur_speci;
    }

    /**
     * Set capacit_termic
     *
     * @param integer $capacitTermic
     */
    public function setCapacitTermic($capacitTermic) {
        $this->capacit_termic = $capacitTermic;
    }

    /**
     * Get capacit_termic
     *
     * @return integer 
     */
    public function getCapacitTermic() {
        return $this->capacit_termic;
    }

}