<?php

namespace Arto\PingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="ping_senario")
 */
class Ping_Senario {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idPing_senario;

    /**
     * @ORM\Column(type="string", length="255", name="nom", nullable="true")
     * 
     * @var string $nom
     */
    private $nom;

    /**
     * @ORM\ManyToOne(targetEntity="Ping_Projet")
     * @ORM\JoinColumn(name="idPing_projet", referencedColumnName="idPing_projet")
     * 
     * @var Ping_Projet $ping_projet
     */
    protected $idPing_projet;

    /**
     * @ORM\Column(type="string", length="255", name="voierie",nullable="true")
     * 
     * @var string $voierie
     */
    private $voierie;

    /**
     * @ORM\Column(type="string", length="255", name="materiaux",nullable="true")
     * 
     * @var string $materiaux
     */
    private $materiaux;

    /**
     * @ORM\Column(type="float", name="quant_voie",nullable="true")
     * 
     * @var float $quant_voie
     */
    private $quant_voie;

    /**
     * @ORM\Column(type="float", name="surface",nullable="true")
     * 
     * @var float $surface
     */
    private $surface;

    /**
     * @ORM\Column(type="string", length="255", name="luminaire",nullable="true")
     * 
     * @var string $luminaire
     */
    private $luminaire;

    /**
     * @ORM\Column(type="string", length="255", name="source",nullable="true")
     * 
     * @var string $source
     */
    private $source;

    /**
     * @ORM\Column(type="float", name="quant_lumi",nullable="true")
     * 
     * @var float $quant_lumi
     */
    private $quan_lumi;

    /**
     * @ORM\Column(type="float", name="puissance",nullable="true")
     * 
     * @var float $puissance
     */
    private $puissance;

    /**
     * @ORM\Column(type="float", name="longueur_eau",nullable="true")
     * 
     * @var float $longueur_eau
     */
    private $longueur_eau;

    /**
     * @ORM\Column(type="float", name="quant_eau_jour",nullable="true")
     * 
     * @var float $quant_eau_jour
     */
    private $quant_eau_jour;

    /**
     * @ORM\Column(type="float", name="quant_eau_sale",nullable="true")
     * 
     * @var float $quant_eau_sale
     */
    private $quant_eau_sale;

    /**
     * @ORM\Column(type="float", name="longueur_eau_sale",nullable="true")
     * 
     * @var float $longueur_eau_sale
     */
    private $longueur_eau_sale;

    /**
     * @ORM\Column(type="integer", name="nb_cab_tel",nullable="true")
     * 
     * @var integer $nb_cab_tel
     */
    private $nb_cab_tel;

    /**
     * @ORM\Column(type="integer", name="nb_ante",nullable="true")
     * 
     * @var integer $nb_ante
     */
    private $nb_ante;

    /**
     * @ORM\Column(type="float", name="surface_plafond",nullable="true")
     * 
     * @var float $surface_plafond
     */
    private $surface_plafond;

    /**
     * @ORM\Column(type="string", name="materiaux_chauf",nullable="true")
     * 
     * @var string $materiaux_chauf
     */
    private $materiaux_chauf;

    /**
     * @ORM\Column(type="float", name="lambda",nullable="true")
     * 
     * @var float $lambda
     */
    private $lambda;

    /**
     * @ORM\Column(type="float", name="epaisseur",nullable="true")
     * 
     * @var float $epaisseur
     */
    private $epaisseur;

    /**
     * @ORM\Column(type="float", name="resis_conduc",nullable="true")
     * 
     * @var float $resis_conduc
     */
    private $resis_conduc;

    /**
     * @ORM\Column(type="float", name="resis_convec",nullable="true")
     * 
     * @var float $resis_convec
     */
    private $resis_convec;

    /**
     * @ORM\Column(type="float", name="resis_total",nullable="true")
     * 
     * @var float $resis_total
     */
    private $resis_total;

    /**
     * @ORM\Column(type="float", name="deperdition",nullable="true")
     * 
     * @var float $deperdition
     */
    private $deperdition;

    /**
     * @ORM\Column(type="float", name="quant_chauf",nullable="true")
     * 
     * @var float $quant_chauf
     */
    private $quant_chauf;

    /**
     * @ORM\Column(type="string", name="type_fondation",nullable="true")
     * 
     * @var string $type_fondation
     */
    private $type_fondation;

    /**
     * @ORM\Column(type="string", name="etat_terras",nullable="true")
     * 
     * @var string $etat_terras
     */
    private $etat_terras;

    /**
     * @ORM\Column(type="float", name="surface_vert",nullable="true")
     * 
     * @var float $surface_vert
     */
    private $surface_vert;

    /**
     * @ORM\Column(type="integer", name="nb_arbre",nullable="true")
     * 
     * @var integer $nb_arbre
     */
    private $nb_arbre;

    /**
     * @ORM\Column(type="float", name="surface_pelouse",nullable="true")
     * 
     * @var float $surface_pelouse
     */
    private $surface_pelouse;

    /**
     * @ORM\Column(type="integer", name="nb_bassin",nullable="true")
     * 
     * @var integer $nb_bassin
     */
    private $nb_bassin;

    /**
     * @ORM\Column(type="float", name="surface_bassin",nullable="true")
     * 
     * @var float $surface_bassin
     */
    private $surface_bassin;

    /**
     * @ORM\Column(type="float", name="quant_eau_recol",nullable="true")
     * 
     * @var float $quant_eau_recol
     */
    private $quant_eau_recol;

    /**
     * @ORM\Column(type="float", name="quant_eau_trait",nullable="true")
     * 
     * @var float $quant_eau_trait
     */
    private $quant_eau_trait;

    /**
     * @ORM\Column(type="integer", name="nb_noue",nullable="true")
     * 
     * @var integer $nb_noue
     */
    private $nb_noue;

    /**
     * @ORM\Column(type="float", name="surface_noue",nullable="true")
     * 
     * @var float $surface_noue
     */
    private $surface_noue;

    /**
     * @ORM\Column(type="float", name="quant_eau_recol_noue",nullable="true")
     * 
     * @var float $quant_eau_recol_noue
     */
    private $quant_eau_recol_noue;

    /**
     * @ORM\Column(type="float", name="quant_eau_trait_noue",nullable="true")
     * 
     * @var float $quant_eau_trait_noue
     */
    private $quant_eau_trait_noue;

    public function __construct() {
        
    }

    /**
     * Get idPing_senario
     *
     * @return integer 
     */
    public function getIdPingSenario() {
        return $this->idPing_senario;
    }

    /**
     * Set nom
     *
     * @param string $nom
     */
    public function setNom($nom) {
        $this->nom = $nom;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Set voierie
     *
     * @param string $voierie
     */
    public function setVoierie($voierie) {
        $this->voierie = $voierie;
    }

    /**
     * Get voierie
     *
     * @return string 
     */
    public function getVoierie() {
        return $this->voierie;
    }

    /**
     * Set materiaux
     *
     * @param string $materiaux
     */
    public function setMateriaux($materiaux) {
        $this->materiaux = $materiaux;
    }

    /**
     * Get materiaux
     *
     * @return string 
     */
    public function getMateriaux() {
        return $this->materiaux;
    }

    /**
     * Set quant_voie
     *
     * @param float $quantVoie
     */
    public function setQuantVoie($quantVoie) {
        $this->quant_voie = $quantVoie;
    }

    /**
     * Get quant_voie
     *
     * @return float 
     */
    public function getQuantVoie() {
        return $this->quant_voie;
    }

    /**
     * Set surface
     *
     * @param float $surface
     */
    public function setSurface($surface) {
        $this->surface = $surface;
    }

    /**
     * Get surface
     *
     * @return float 
     */
    public function getSurface() {
        return $this->surface;
    }

    /**
     * Set luminaire
     *
     * @param string $luminaire
     */
    public function setLuminaire($luminaire) {
        $this->luminaire = $luminaire;
    }

    /**
     * Get luminaire
     *
     * @return string 
     */
    public function getLuminaire() {
        return $this->luminaire;
    }

    /**
     * Set source
     *
     * @param string $source
     */
    public function setSource($source) {
        $this->source = $source;
    }

    /**
     * Get source
     *
     * @return string 
     */
    public function getSource() {
        return $this->source;
    }

    /**
     * Set quan_lumi
     *
     * @param float $quanLumi
     */
    public function setQuanLumi($quanLumi) {
        $this->quan_lumi = $quanLumi;
    }

    /**
     * Get quan_lumi
     *
     * @return float 
     */
    public function getQuanLumi() {
        return $this->quan_lumi;
    }

    /**
     * Set puissance
     *
     * @param float $puissance
     */
    public function setPuissance($puissance) {
        $this->puissance = $puissance;
    }

    /**
     * Get puissance
     *
     * @return float 
     */
    public function getPuissance() {
        return $this->puissance;
    }

    /**
     * Set longueur_eau
     *
     * @param float $longueurEau
     */
    public function setLongueurEau($longueurEau) {
        $this->longueur_eau = $longueurEau;
    }

    /**
     * Get longueur_eau
     *
     * @return float 
     */
    public function getLongueurEau() {
        return $this->longueur_eau;
    }

    /**
     * Set quant_eau_jour
     *
     * @param float $quantEauJour
     */
    public function setQuantEauJour($quantEauJour) {
        $this->quant_eau_jour = $quantEauJour;
    }

    /**
     * Get quant_eau_jour
     *
     * @return float 
     */
    public function getQuantEauJour() {
        return $this->quant_eau_jour;
    }

    /**
     * Set quant_eau_sale
     *
     * @param float $quantEauSale
     */
    public function setQuantEauSale($quantEauSale) {
        $this->quant_eau_sale = $quantEauSale;
    }

    /**
     * Get quant_eau_sale
     *
     * @return float 
     */
    public function getQuantEauSale() {
        return $this->quant_eau_sale;
    }

    /**
     * Set longueur_eau_sale
     *
     * @param float $longueurEauSale
     */
    public function setLongueurEauSale($longueurEauSale) {
        $this->longueur_eau_sale = $longueurEauSale;
    }

    /**
     * Get longueur_eau_sale
     *
     * @return float 
     */
    public function getLongueurEauSale() {
        return $this->longueur_eau_sale;
    }

    /**
     * Set nb_cab_tel
     *
     * @param integer $nbCabTel
     */
    public function setNbCabTel($nbCabTel) {
        $this->nb_cab_tel = $nbCabTel;
    }

    /**
     * Get nb_cab_tel
     *
     * @return integer 
     */
    public function getNbCabTel() {
        return $this->nb_cab_tel;
    }

    /**
     * Set nb_ante
     *
     * @param integer $nbAnte
     */
    public function setNbAnte($nbAnte) {
        $this->nb_ante = $nbAnte;
    }

    /**
     * Get nb_ante
     *
     * @return integer 
     */
    public function getNbAnte() {
        return $this->nb_ante;
    }

    /**
     * Set surface_plafond
     *
     * @param float $surfacePlafond
     */
    public function setSurfacePlafond($surfacePlafond) {
        $this->surface_plafond = $surfacePlafond;
    }

    /**
     * Get surface_plafond
     *
     * @return float 
     */
    public function getSurfacePlafond() {
        return $this->surface_plafond;
    }

    /**
     * Set materiaux_chauf
     *
     * @param string $materiauxChauf
     */
    public function setMateriauxChauf($materiauxChauf) {
        $this->materiaux_chauf = $materiauxChauf;
    }

    /**
     * Get materiaux_chauf
     *
     * @return string 
     */
    public function getMateriauxChauf() {
        return $this->materiaux_chauf;
    }

    /**
     * Set lambda
     *
     * @param float $lambda
     */
    public function setLambda($lambda) {
        $this->lambda = $lambda;
    }

    /**
     * Get lambda
     *
     * @return float 
     */
    public function getLambda() {
        return $this->lambda;
    }

    /**
     * Set epaisseur
     *
     * @param float $epaisseur
     */
    public function setEpaisseur($epaisseur) {
        $this->epaisseur = $epaisseur;
    }

    /**
     * Get epaisseur
     *
     * @return float 
     */
    public function getEpaisseur() {
        return $this->epaisseur;
    }

    /**
     * Set resis_conduc
     *
     * @param float $resisConduc
     */
    public function setResisConduc($resisConduc) {
        $this->resis_conduc = $resisConduc;
    }

    /**
     * Get resis_conduc
     *
     * @return float 
     */
    public function getResisConduc() {
        return $this->resis_conduc;
    }

    /**
     * Set resis_convec
     *
     * @param float $resisConvec
     */
    public function setResisConvec($resisConvec) {
        $this->resis_convec = $resisConvec;
    }

    /**
     * Get resis_convec
     *
     * @return float 
     */
    public function getResisConvec() {
        return $this->resis_convec;
    }

    /**
     * Set resis_total
     *
     * @param float $resisTotal
     */
    public function setResisTotal($resisTotal) {
        $this->resis_total = $resisTotal;
    }

    /**
     * Get resis_total
     *
     * @return float 
     */
    public function getResisTotal() {
        return $this->resis_total;
    }

    /**
     * Set deperdition
     *
     * @param float $deperdition
     */
    public function setDeperdition($deperdition) {
        $this->deperdition = $deperdition;
    }

    /**
     * Get deperdition
     *
     * @return float 
     */
    public function getDeperdition() {
        return $this->deperdition;
    }

    /**
     * Set quant_chauf
     *
     * @param float $quantChauf
     */
    public function setQuantChauf($quantChauf) {
        $this->quant_chauf = $quantChauf;
    }

    /**
     * Get quant_chauf
     *
     * @return float 
     */
    public function getQuantChauf() {
        return $this->quant_chauf;
    }

    /**
     * Set type_fondation
     *
     * @param string $typeFondation
     */
    public function setTypeFondation($typeFondation) {
        $this->type_fondation = $typeFondation;
    }

    /**
     * Get type_fondation
     *
     * @return string 
     */
    public function getTypeFondation() {
        return $this->type_fondation;
    }

    /**
     * Set etat_terras
     *
     * @param string $etatTerras
     */
    public function setEtatTerras($etatTerras) {
        $this->etat_terras = $etatTerras;
    }

    /**
     * Get etat_terras
     *
     * @return string 
     */
    public function getEtatTerras() {
        return $this->etat_terras;
    }

    /**
     * Set surface_vert
     *
     * @param float $surfaceVert
     */
    public function setSurfaceVert($surfaceVert) {
        $this->surface_vert = $surfaceVert;
    }

    /**
     * Get surface_vert
     *
     * @return float 
     */
    public function getSurfaceVert() {
        return $this->surface_vert;
    }

    /**
     * Set nb_arbre
     *
     * @param integer $nbArbre
     */
    public function setNbArbre($nbArbre) {
        $this->nb_arbre = $nbArbre;
    }

    /**
     * Get nb_arbre
     *
     * @return integer 
     */
    public function getNbArbre() {
        return $this->nb_arbre;
    }

    /**
     * Set surface_pelouse
     *
     * @param float $surfacePelouse
     */
    public function setSurfacePelouse($surfacePelouse) {
        $this->surface_pelouse = $surfacePelouse;
    }

    /**
     * Get surface_pelouse
     *
     * @return float 
     */
    public function getSurfacePelouse() {
        return $this->surface_pelouse;
    }

    /**
     * Set nb_bassin
     *
     * @param integer $nbBassin
     */
    public function setNbBassin($nbBassin) {
        $this->nb_bassin = $nbBassin;
    }

    /**
     * Get nb_bassin
     *
     * @return integer 
     */
    public function getNbBassin() {
        return $this->nb_bassin;
    }

    /**
     * Set surface_bassin
     *
     * @param float $surfaceBassin
     */
    public function setSurfaceBassin($surfaceBassin) {
        $this->surface_bassin = $surfaceBassin;
    }

    /**
     * Get surface_bassin
     *
     * @return float 
     */
    public function getSurfaceBassin() {
        return $this->surface_bassin;
    }

    /**
     * Set quant_eau_recol
     *
     * @param float $quantEauRecol
     */
    public function setQuantEauRecol($quantEauRecol) {
        $this->quant_eau_recol = $quantEauRecol;
    }

    /**
     * Get quant_eau_recol
     *
     * @return float 
     */
    public function getQuantEauRecol() {
        return $this->quant_eau_recol;
    }

    /**
     * Set quant_eau_trait
     *
     * @param float $quantEauTrait
     */
    public function setQuantEauTrait($quantEauTrait) {
        $this->quant_eau_trait = $quantEauTrait;
    }

    /**
     * Get quant_eau_trait
     *
     * @return float 
     */
    public function getQuantEauTrait() {
        return $this->quant_eau_trait;
    }

    /**
     * Set nb_noue
     *
     * @param integer $nbNoue
     */
    public function setNbNoue($nbNoue) {
        $this->nb_noue = $nbNoue;
    }

    /**
     * Get nb_noue
     *
     * @return integer 
     */
    public function getNbNoue() {
        return $this->nb_noue;
    }

    /**
     * Set surface_noue
     *
     * @param float $surfaceNoue
     */
    public function setSurfaceNoue($surfaceNoue) {
        $this->surface_noue = $surfaceNoue;
    }

    /**
     * Get surface_noue
     *
     * @return float 
     */
    public function getSurfaceNoue() {
        return $this->surface_noue;
    }

    /**
     * Set quant_eau_recol_noue
     *
     * @param float $quantEauRecolNoue
     */
    public function setQuantEauRecolNoue($quantEauRecolNoue) {
        $this->quant_eau_recol_noue = $quantEauRecolNoue;
    }

    /**
     * Get quant_eau_recol_noue
     *
     * @return float 
     */
    public function getQuantEauRecolNoue() {
        return $this->quant_eau_recol_noue;
    }

    /**
     * Set quant_eau_trait_noue
     *
     * @param float $quantEauTraitNoue
     */
    public function setQuantEauTraitNoue($quantEauTraitNoue) {
        $this->quant_eau_trait_noue = $quantEauTraitNoue;
    }

    /**
     * Get quant_eau_trait_noue
     *
     * @return float 
     */
    public function getQuantEauTraitNoue() {
        return $this->quant_eau_trait_noue;
    }

    /**
     * Set idPing_projet
     *
     * @param ping\PingBundle\Entity\Ping_Projet $idPingProjet
     */
    public function setIdPingProjet(\ping\PingBundle\Entity\Ping_Projet $idPingProjet) {
        $this->idPing_projet = $idPingProjet;
    }

    /**
     * Get idPing_projet
     *
     * @return ping\PingBundle\Entity\Ping_Projet 
     */
    public function getIdPingProjet() {
        return $this->idPing_projet;
    }

}