<?php

namespace Arto\AcvBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Arto\AcvBundle\Entity\Base;
use Arto\AcvBundle\Entity\Project;
use Arto\AcvBundle\Entity\Assembly;
use Arto\AcvBundle\Entity\Family;
use Arto\AcvBundle\Entity\Type;
use Arto\AcvBundle\Entity\Usage;
use Arto\AcvBundle\Entity\UsagePart;

class UsageController extends Controller
{
    /**
     * Affiche la page "Utilisation" du projet
     * @param type $project le projet
     * @param type $id l'id du produit
     * @return La page "Utilisation" du projet
     */
    public function indexAction($project, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($id);

        $type = $em->getRepository('ArtoAcvBundle:Type')->findOneByName('process');
        $family = $em->getRepository('ArtoAcvBundle:Family')->findOneByName('énergie');

        $energies = $em->getRepository('ArtoAcvBundle:Base')->findBy(
            array('type' => $type->getId(),'family' => $family->getId(), 'isVisible' => 'oui'),
            array('name' => 'ASC')
        );

        $usage = $product->getUsage();

        $parts1 = array();
        $parts2 = array();

        if ($usage == null) {
            $usage = new Usage();
        } else {
            $parts1 = $em->getRepository('ArtoAcvBundle:UsagePart')->findBy(array(
                'category' => 1,
                'usage' => $product->getUsage()->getId()
            ));
            $parts2 = $em->getRepository('ArtoAcvBundle:UsagePart')->findBy(array(
                'category' => 2,
                'usage' => $product->getUsage()->getId()
            ));
        }
        
        return $this->render('ArtoAcvBundle:Default:usage.html.twig', array(
            'project' => $project,
            'product' => $product,
            'usage'   => $usage,
            'energies' => $energies,
            'parts1' => $parts1,
            'parts2' => $parts2,
        ));
    }

    /**
     * Affiche les consommables d'Utilisation
     * @param type $id l'id de l'UsagePart
     * @param type $category 1= consommable, 2= installation
     * @param type $key
     * @return type
     */
    public function partAction($id, $category, $key)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $part = $em->getRepository('ArtoAcvBundle:UsagePart')->find($id);

        $types = $em->getRepository('ArtoAcvBundle:Type')->findAll();
        $families = $em->getRepository('ArtoAcvBundle:Family')->findAll();

        $batteries = array();
        $bases = array();

        $batteries = $em->getRepository('ArtoAcvBundle:Base')->findBy(array(
            'type' => $part->getBase()->getType()->getId(),
            'family' => $part->getBase()->getFamily()->getId(),
        ), array('name' => 'ASC'));
        
        return $this->render('ArtoAcvBundle:Default:usage_part.html.twig', array(
            'types' => $types,
            'families' => $families,
            'part' => $part,
            'key' => $key,
            'batteries' => $batteries,
            'category' => $category
        ));
    }
    
    /**
     * Crée une nouvelle part
     * @param type $category 1= consommable, 2= installation
     * @param int $key
     * @return type
     */
    public function partNewAction($category, $key)
    {
        $key = 1000;
        $em = $this->getDoctrine()->getEntityManager();

        $types = $em->getRepository('ArtoAcvBundle:Type')->findAll();

        $batteries = null;
        if ($category == 1) {
            $query = $em->createQuery('SELECT b FROM ArtoAcvBundle:Base b WHERE b.name LIKE :term ORDER BY b.name ASC')
                        ->setParameter('term', 'batterie%');
            $batteries = $query->getResult();
        }

        return $this->render('ArtoAcvBundle:Default:usage_part_new.html.twig', array(
            'types' => $types,
            'category' => $category,
            'batteries' => $batteries,
            'key' => $key
        ));
    }
    
    /**
     * Sauvegarde les paramètres de la page "Utilisation"
     * @param type $project le projet
     * @param type $id l'id du produit
     * @return La page "Utilisation" avec les paramètres sauvegardés
     */
    public function saveAction($project, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();

        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        
        //$lifetime = $project->getLifetime();

        //$product = $request->get('product');
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($id);

        $usage = $product->getUsage();

        if ($usage == null) {
            $usage = new Usage();
            $usage->setProduct($product);
        }

        $usage->setEnergyType($request->get('energy_type'));
        
        //Replace , by .
        $onKw = $request->get('on_kw');
        $onKw = str_replace(',','.',$onKw);
        $usage->setOnKw($onKw);
        $usage->setOnPercent($request->get('on_percent'));
        
        $offKw = $request->get('off_kw');
        $offKw = str_replace(',','.',$offKw);
        $usage->setOffKw($offKw);
        $usage->setOffPercent($request->get('off_percent'));
        
        $standbyActiveKw = $request->get('standby_active_kw');
        $standbyActiveKw = str_replace(',','.',$standbyActiveKw);
        $usage->setStandByActiveKw($standbyActiveKw);
        $usage->setStandByActivePercent($request->get('standby_active_percent'));
        
        /*$standbyPassiveKw = $request->get('standby_passive_kw');
        $standbyPassiveKw = str_replace(',','.',$standbyPassiveKw);
        $usage->setStandByPassiveKw($standbyPassiveKw);
        $usage->setStandByPassivePercent($request->get('standby_passive_percent'));*/
        
        $lifetime = $request->get('lifetimeVal');
        $usage->setLifetime($lifetime);
        $project->setLifetime($lifetime);

        $usage->setContinuous($request->get('continuous'));

        $energy = $em->getRepository('ArtoAcvBundle:Base')->find($request->get('energy'));
        if ($energy != null) {
            $usage->setEnergy($energy);
        }

        $em->persist($usage);
        $em->persist($project);
        $em->flush();

        return $this->redirect($this->generateUrl('acv_usage', array(
            'project' => $project->getId(),
            'id' => $product->getId()
        )));
    }
    
    /**
     * Sauvegarde les parts de "Utilisation"
     * @param type $project le projet
     * @param type $id l'id du produit
     * @param type $category 1=consommables, 2= installation
     * @return type
     */
    public function partSaveAction($project, $id, $category)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();

        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($id);

        $usage = $product->getUsage();

        $parts = $request->get('part');

        foreach($parts as $key => $part) {

            if ($key != 1000 && $part['base'] != null && $part['quantity'] != null && $part['param1Value'] != null) {
                if (isset($part['id'])) {
                    $p = $em->getRepository('ArtoAcvBundle:UsagePart')->find($part['id']);
                } else {
                    $p = new UsagePart();
                    $p->setUsage($usage);
                }

                $p->setCategory($category);
                $p->setQuantity($part['quantity']);
                
                //Replace , by .
                $val = $part['param1Value'];
                $val = str_replace(',','.',$val);
                $p->setVal($val);

                $base = $em->getRepository('ArtoAcvBundle:Base')->find($part['base']);
                if ($base != null) {
                    $p->setBase($base);
                }

                $em->persist($p);
            }

        }

        $em->flush();

        return $this->redirect($this->generateUrl('acv_usage', array(
            'project' => $project->getId(),
            'id' => $product->getId()
        )));
    }
    
    /**
     * Supprime une part de "Utilisation"
     * @param type $project le projet
     * @param type $id l'id du produit
     * @param type $part la part
     * @return type
     */
    public function partDeleteAction($project, $id, $part)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($id);

        $p = $em->getRepository('ArtoAcvBundle:UsagePart')->find($part);
        $em->remove($p);

        $em->flush();

        return $this->redirect($this->generateUrl('acv_usage', array(
            'project' => $project->getId(),
            'id' => $product->getId()
        )));
    }


}
