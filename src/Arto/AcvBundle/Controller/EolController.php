<?php

namespace Arto\AcvBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Arto\AcvBundle\Entity\Base;
use Arto\AcvBundle\Entity\Project;
use Arto\AcvBundle\Entity\Product;
use Arto\AcvBundle\Entity\Assembly;
use Arto\AcvBundle\Entity\Family;
use Arto\AcvBundle\Entity\Type;
use Arto\AcvBundle\Entity\Eol;
use Arto\AcvBundle\Entity\Deee;
use Arto\AcvBundle\Entity\DeeePart;
use Arto\AcvBundle\Entity\EolPart;

class EolController extends Controller
{   
    /**
     * Pour appeler le service Calculator
     * @return une instance de Calculator
     */
    private function getCalculator()
    {
        return $this->get('arto.acv.calculator');
    }
    
    /**
     * Pour appeler le service Drawer
     * @return une instance de Drawer
     */
    private function getDrawer()
    {
        return $this->get('arto.acv.drawer');
    }
    
    /**
     * Affiche la page Fin de vie d'un projet
     * @param type $project le projet
     * @param type $id l'id du produit
     * @return Affiche la page Fin de vie du projet
     */
    public function indexAction($project, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $calculator = $this->getCalculator();
        
        $acvProject = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        
        $projectId = $acvProject->getId();
        
        $product = $em->getRepository('ArtoAcvBundle:Product')->findOneBy(
                array('id' => $id));

        $transport = $em->getRepository('ArtoAcvBundle:Family')->findOneByName('transport');
        //$transports = $em->getRepository('ArtoAcvBundle:Base')->findByFamily($transport->getId());
        $transports = $em->getRepository('ArtoAcvBundle:Base')->findBy(
                array('family' => $transport->getId(), 'isVisible' => 'oui'), array('name' => 'ASC'));
        
        $traitement = $em->getRepository('ArtoAcvBundle:Family')->findOneByName('fin de vie');
        //$traitements = $em->getRepository('ArtoAcvBundle:Base')->findByFamily($traitement->getId());
        $traitements = $em->getRepository('ArtoAcvBundle:Base')->findBy(
                array('family' => $traitement->getId(), 'isVisible' => 'oui'), array('name' => 'ASC'));

        $eol = new Eol();
        if ($product->getEol() != null) {
            $eol = $product->getEol();
        }
        
        $productId = $product->getId();
         
        $deees = $em->createQuery("SELECT d FROM ArtoAcvBundle:Deee d order by d.name")->getResult();
        
        $deeeParts = $em->getRepository('ArtoAcvBundle:DeeePart')->findBy(
                array('product' => $productId));
        
        $eolParts = $em->getRepository('ArtoAcvBundle:EolPart')->findBy(
                array('project' => $projectId));
        
        $this->calculateTauxRecyclabilité($acvProject, $id);
        
        $percentMetallic = $calculator->calculatePercentMetallic($product);
        $percentPlastic = $calculator->calculatePercentPlastic($product);
        $percentPackaging = $calculator->calculatePercentPackaging($product);
        $percentDiverse = 100 - ($percentMetallic + $percentPlastic + $percentPackaging);

        return $this->render('ArtoAcvBundle:Default:eol.html.twig', array(
            'project' => $acvProject,
            'product' => $product,
            'transports' => $transports,
            'traitements' => $traitements,
            'eol' => $eol,
            'deees' => $deees,
            'deeeParts' => $deeeParts,
            'eolParts' => $eolParts,
            'percentMetallic' => $percentMetallic,
            'percentPlastic' => $percentPlastic,
            'percentPackaging' => $percentPackaging,
            'percentDiverse' => $percentDiverse
        ));
    }

    /**
     * Sauvegarde la page Fin de vie d'un projet
     * @param type $project le projet
     * @param type $id l'id du produit
     * @return La page Fin de vie du projet sauvegardée
     */
    public function saveAction($project, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();

        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($id);

        $eol = $product->getEol();

        if ($eol == null) {
            $eol = new Eol();
            $eol->setProduct($product);
        }

        $eol->setImpactValorisation($request->get('impact_valorisation'));
        $eol->setImpactDecharge($request->get('impact_decharge'));
        $eol->setType($request->get('type'));
        
        $eolParts = $this->getRequest()->get('eol');
        
        foreach($eolParts as $eolPart){
            // We check if the label of the part is not empty
            if (isset($eolPart['label']) && $eolPart['label'] != '') {
                // Try Retreiving the object if the request id exist
                if (isset($eolPart['id']) && $eolPart['id'] != null) {
                    $p = $em->getRepository('ArtoAcvBundle:EolPart')->find($eolPart['id']);
                } else {
                    $p = new EolPart();
                }
                
                $p->setProject($project);
                $p->setLabel($eolPart['label']);
                
                if ($eolPart['traitement'] == "null") {
                    $p->setTraitement(null);
                    $p->setQuantity(null);
                    $p->setWeight(null);
                } else {
                    //Retrieve base to set the plasticPart base FK
                    $traitement = $em->getRepository('ArtoAcvBundle:Base')->find($eolPart['traitement']);
                    $p->setTraitement($traitement);

                    $p->setQuantity($eolPart['quantity']);
                    $weight = $eolPart['weight'];
                    $weight = str_replace(',', '.', $weight);
                    $p->setWeight($weight);
                }
                
                if ($eolPart['transport'] == "null") {
                    $p->setTransport(null);
                } else {
                    //Retrieve base to set the plasticPart base FK
                    $transport = $em->getRepository('ArtoAcvBundle:Base')->find($eolPart['transport']);
                    $p->setTransport($transport);

                    $p->setQuantity($eolPart['quantity']);
                    $weight = $eolPart['weight'];
                    $weight = str_replace(',', '.', $weight);
                    $p->setWeight($weight);
                    $p->setDistance($eolPart['distance']);
                }
                
                 $em->persist($p);
                   
            }
        }
        
        // We flush the all the modifications at once
        $em->flush();

        $transport = $em->getRepository('ArtoAcvBundle:Base')->find($request->get('collecte_transport'));
        if ($transport != null) {
            $eol->setCollecteTransport($transport);
            $eol->setCollecteDistance($request->get('collecte_distance'));
            $eol->setCollecteWeight($request->get('collecte_weight'));
        }

        $transport = $em->getRepository('ArtoAcvBundle:Base')->find($request->get('depollution_transport'));
        if ($transport != null) {
            $eol->setDepollutionTransport($transport);
            $eol->setDepollutionDistance($request->get('depollution_distance'));
            $eol->setDepollutionWeight($request->get('depollution_weight'));
        }

        $transport = $em->getRepository('ArtoAcvBundle:Base')->find($request->get('incineration_transport'));
        if ($transport != null) {
            $eol->setIncinerationTransport($transport);
            $eol->setIncinerationDistance($request->get('incineration_distance'));
            $eol->setIncinerationWeight($request->get('incineration_weight'));
        }

        $transport = $em->getRepository('ArtoAcvBundle:Base')->find($request->get('dechet_transport'));
        if ($transport != null) {
            $eol->setDechetTransport($transport);
            $eol->setDechetDistance($request->get('dechet_distance'));
            $eol->setDechetWeight($request->get('dechet_weight'));
        }

        $em->persist($eol);
        $em->flush();

        return $this->redirect($this->generateUrl('acv_eol', array(
            'project' => $project->getId(),
            'id' => $product->getId()
        )));
    }
    
    /**
     * Liste les DEEE dans la page Fin de vie
     * @return \Symfony\Component\HttpFoundation\Response la liste des DEEE remplie
     */
    public function deeeFindAction()
    {
         $em = $this->getDoctrine()->getEntityManager();
         $request = $this->getRequest();
         
         $id = $request->get('deeeId');
         
         $deee = $em->getRepository('ArtoAcvBundle:Deee')->findOneBy(
                 array('id' => $id));
         
         $result = array(
             'id' => $deee->getId(),
             'hazardous' => $deee->getIsHazardous()
         );
         
         $json = json_encode($result);
         return new Response($json);
    }
    
    /**
     * Sauvegarde les DEEE de la Fin de vie
     * @param type $project le projet
     * @param type $id l'id du produit
     * @return type la page Fin de vie avec les DEEE sauvegardés
     */
    public function saveDeeePartAction($project, $id)
    {
         $em = $this->getDoctrine()->getEntityManager();
         $request = $this->getRequest();
         
         $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
         $product = $em->getRepository('ArtoAcvBundle:Product')->find($id);
         
         $deeeParts = $this->getRequest()->get('deeePart');
         foreach ($deeeParts as $deeePart) {
            // Try Retreiving the object if the request id exist
            if (isset($deeePart['id']) && $deeePart['id'] != null) {
                $p = $em->getRepository('ArtoAcvBundle:DeeePart')->find($deeePart['id']);
            } else {
                $p = new DeeePart();
            }

            $p->setProduct($product);

            $deee = $em->getRepository('ArtoAcvBundle:Deee')->find($deeePart['deee']);
            $p->setDeee($deee);

            /* if(isset($deeePart['present'])){
              $p->setPresent("yes");
              }else{
              $p->setPresent("no");
              }

              $p->setWeight($deeePart['weight']); */
            $p->setPresent("no");
            $p->setWeight(0);

            $em->persist($p);
        }
        // We flush the all the modifications at once
        $em->flush();
        
        return $this->redirect($this->generateUrl('acv_eol', array(
            'project' => $project->getId(),
            'id' => $product->getId()
        )));
    }
    
    /**
     * Calcule le taux de recyclabilité du projet
     * @param type $project le projet
     * @param type $id l'id du produit
     */
    public function calculateTauxRecyclabilité($project, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $project = $em->getRepository('ArtoAcvBundle:Project')->findOneBy(
                array('id' => $project));
        $product = $em->getRepository('ArtoAcvBundle:Product')->findOneBy(
                array('id' => $id));
        
        $parts = array();
        $recyclableWeight = 0;
        $totalWeight = $product->getWeight();
        
        foreach($product->getAssemblies() as $assembly) {
            foreach($assembly->getParts() as $part) {
                if ($part->getParent() != null && $part->getMaterial() != null) {
                    
                    $material = $part->getMaterial();
                    $id = $material->getId();
                    
                    if($material->getUnit1() == "g"){
                        if($part->getParam1() != null){
                            $val = $part->getParam1();
                            $qty = $part->getParent()->getQuantity();
                            $weight = $val * $qty;
                        }
                    }else if($material->getUnit1() == "UN"){
                        if($material->getUnit2() == "g"){
                            if($part->getParam1() != null){
                                $val = $part->getParam1();
                                $qty = $part->getParent()->getQuantity();
                                $weight = $val * $qty * $material->getMultiplicateur();
                            }
                        }else if($material->getUnit2() == "kg"){
                            if($part->getParam1() != null){
                                $val = $part->getParam1();
                                $qty = $part->getParent()->getQuantity();
                                $weight = ($val * $qty * $material->getMultiplicateur())/1000;
                            }
                        }else{
                            if($part->getParam1() != null){
                                if($material->getUnit2() == ""){
                                    $val = $part->getParam1();
                                    $qty = $part->getParent()->getQuantity();
                                    $weight = $val * $qty * $material->getMultiplicateur();
                                }else{
                                    $val = $part->getParam1();
                                    $qty = $part->getParent()->getQuantity();
                                    $weight = $val * $qty * $material->getValParam2ByUN()* $material->getMultiplicateur();
                                }
                            }
                        }
                    }else{
                        if($part->getParam1() != null){
                            if($material->getUnit2() == ""){
                                $val = $part->getParam1();
                                $qty = $part->getParent()->getQuantity();
                                $weight = $val * $qty * $material->getMultiplicateur();
                            }else{
                                $val = $part->getParam1();
                                $qty = $part->getParent()->getQuantity();
                                $weight = $val * $qty * $material->getValParam2ByUN() * $material->getMultiplicateur();
                            }       
                        }
                    }

                    $recyclableWeight = $recyclableWeight + $weight * $material->getTauxRecyclabilite() * $assembly->getQuantity();
                }
            }
        }
        
        //DEEE
        /*foreach($product->getDeeeParts() as $deeePart){
            $weight = $deeePart->getWeight();
            $recyclableWeight = $recyclableWeight + $weight * $deeePart->getDeee()->getTauxRecyclabilité();
        }*/
        
        if($totalWeight == 0){
            $tauxRecyclabilite = 0;
        }else{
            $tauxRecyclabilite = ($recyclableWeight/$totalWeight)*100;
            $tauxRecyclabilite = round($tauxRecyclabilite, 0);
        }
        
        $product->setTauxRecyclabilité($tauxRecyclabilite);
        
        $em->persist($product);
        $em->flush();
    }
    
    /**
     * Supprime une Part
     * @param type $id l'id de l'EolPart à supprimer
     * @return la page Fin de vie du projet
     */
    public function deletePartAction($id){
        $em = $this->getDoctrine()->getEntityManager();
        
        //$request = $this->getRequest();
        
        $eolPart = $em->getRepository('ArtoAcvBundle:EolPart')->findOneBy(
                array('id' => $id));
        
        $eolPartProject = $eolPart->getProject();
        
        /*$project = $em->getRepository('ArtoAcvBundle:Project')->findOneBy(
                array('id' => $eolPartProject));*/
        
        $projectId = $eolPartProject->getId();
          
        $product = $em->getRepository('ArtoAcvBundle:Product')->findOneBy(
                array('project' => $projectId));
        
        $productId = $product->getId();
        
        $em->remove($eolPart);
        $em->flush();
        
        
        return $this->redirect($this->generateUrl('acv_eol', array('project' => $projectId, 'id' => $productId)));      
    }
   
   /**
    * 
    * @return type
    */ 
   public function testAction(){
       return $this->render('ArtoAcvBundle:Default:test.html.twig');
   }
   
   /**
    * Affiche le graphique de répartition des masses des matériaux du produit fini
    * @param type $project le projet
    * @param type $id l'id du produit
    * @return \Symfony\Component\HttpFoundation\Response le graphique
    */
   public function repartitionChartAction($project, $id){
        $em = $this->getDoctrine()->getEntityManager();
        $drawer = $this->getDrawer();
        $calculator = $this->getCalculator();

        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($id);

        $response = new Response();
        $response->headers->set('Content-Type', 'image/jpeg');
        
        $percentMetallic = $calculator->calculatePercentMetallic($product);
        $percentPlastic = $calculator->calculatePercentPlastic($product);
        $percentPackaging = $calculator->calculatePercentPackaging($product);
        $percentDiverse = 100 - ($percentMetallic + $percentPlastic + $percentPackaging);
        
        $repartitionChart = $drawer->drawRepartitionChart($percentMetallic, $percentPlastic, $percentPackaging, $percentDiverse);
        $response->setContent($repartitionChart);
        
        return $response;
    }
    
}
