<?php

namespace Arto\AcvBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Arto\AcvBundle\Entity\Base;
use Arto\AcvBundle\Entity\Project;
use Arto\AcvBundle\Entity\Assembly;
use Arto\AcvBundle\Entity\Family;
use Arto\AcvBundle\Entity\Type;
use Arto\AcvBundle\Entity\Delivery;
use Arto\AcvBundle\Entity\DeliveryPart;

class DeliveryController extends Controller
{   
    /**
     * Affiche la page Distribution d'un projet
     * @param type $project le projet en question
     * @return Affiche la page Distribution du projet
     */
    public function indexAction($project)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        $product = $em->getRepository('ArtoAcvBundle:Product')->findOneBy(
                array('project' => $project->getId()));

        $type = $em->getRepository('ArtoAcvBundle:Type')->findOneByName('process');
        $family = $em->getRepository('ArtoAcvBundle:Family')->findOneByName('énergie');

        $transport = $em->getRepository('ArtoAcvBundle:Family')->findOneByName('transport');
        //$transports = $em->getRepository('ArtoAcvBundle:Base')->findByFamily($transport->getId());
        $transports = $em->getRepository('ArtoAcvBundle:Base')->findBy(array('family' => $transport->getId(), 'isVisible' => 'oui'), array('name' => 'ASC'));

        $parts = null;
        $delivery = $product->getDelivery();

        if ($delivery != null) {
            $parts = $delivery->getParts();
        } else {
            $delivery = new Delivery();
        }

        return $this->render('ArtoAcvBundle:Default:delivery.html.twig', array(
            'project' => $project,
            'product' => $product,
            'transports' => $transports,
            'parts' => $parts,
            'delivery' => $delivery
        ));
    }
    
    /**
     * Sauvegarde les modifications dans la page Distribution d'un projet
     * @param type $id L'id du projet
     * @return La page Distribution du projet avec les modifications sauvegardées
     */
    public function saveAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();

        $project = $em->getRepository('ArtoAcvBundle:Project')->find($id);
        $product = $em->getRepository('ArtoAcvBundle:Product')->findOneBy(
                array('project' => $project->getId()));

        $delivery = $product->getDelivery();

        if ($delivery == null) {
            $delivery = new Delivery();
            $delivery->setProduct($product);
        }

        $delivery->setDetailed($request->get('detailed'));
        $delivery->setMode($request->get('mode'));

        if ($delivery->getDetailed() == 1) {
            $parts = $request->get('part');
            foreach ($parts as $p) {

                if ($p['transport'] != '') {

                    if (!isset($p['id'])) {
                        $part = new DeliveryPart();
                        $part->setDelivery($delivery);
                    } else {
                        $part =  $em->getRepository('ArtoAcvBundle:DeliveryPart')->find($p['id']);
                    }

                    $transport = $em->getRepository('ArtoAcvBundle:Base')->find($p['transport']);
                    $part->setTransport($transport);
                    $part->setDistance($p['distance']);

                    $em->persist($part);
                }
            }
        } elseif ($delivery->getDetailed() == 2) {
            foreach ($delivery->getParts() as $part) {
                $em->remove($part);
            }
        }

        $em->persist($delivery);
        $em->flush();

        return $this->redirect($this->generateUrl('acv_delivery', array(
            'project' => $project->getId(),
            'id' => $product->getId()
        )));
    }
    
    /**
     * Efface une part dans la page Distribution d'un projet
     * @param type $id L'id du projet
     * @param type $part La part à supprimer
     * @return Affiche la page Distribution du projet avec la part effacée
     */
    public function partDeleteAction($id, $part)
    {
        $em = $this->getDoctrine()->getEntityManager();
        
        $project = $em->getRepository('ArtoAcvBundle:Project')->find($id);
      
        $p = $em->getRepository('ArtoAcvBundle:DeliveryPart')->findOneBy(
                array('id' => $part));
        $em->remove($p);

        $em->flush();

        return $this->redirect($this->generateUrl('acv_delivery', array(
            'project' => $project->getId()
        )));
    }
}
