<?php

namespace Arto\AcvBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Arto\AcvBundle\Entity\Base;
use Arto\AcvBundle\Entity\Project;
use Arto\AcvBundle\Entity\Assembly;
use Arto\AcvBundle\Entity\Family;
use Arto\AcvBundle\Entity\Type;


class AssemblyController extends Controller
{
    /**
     * Action pour afficher les niveau 1 d'un produit
     * @param type $product le produit
     * @param type $assembly=null 
     * @return type
     */
    public function indexAction($product, $assembly = null)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($product);
        $assemblies = $product->getAssemblies();

        return $this->render('ArtoAcvBundle:Default:assembly.html.twig', array('product' => $product, 'assemblies' => $assemblies, 'assembly' => $assembly));
    }
    
    /**
     * Pour ajouter un niveau 1
     * @param type $product le produit
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addAction($product)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($product);

        $assembly = new Assembly();
        $assembly->setName($this->getRequest()->get('name'));
        $assembly->setQuantity(1);
        $assembly->setProduct($product);
        $em->persist($assembly);
        $em->flush();

        return new Response($assembly->getId());
    }

    /**
     * Pour sauvegarder le niveau 1 
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function saveAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $assembly = $em->getRepository('ArtoAcvBundle:Assembly')->find($this->getRequest()->get('assembly'));

        $assembly->setName($this->getRequest()->get('name'));
        $assembly->setQuantity($this->getRequest()->get('quantity'));
        $em->persist($assembly);
        $em->flush();

        return new Response($assembly->getId());
    }
    
    /**
     * Pour supprimer un niveau 1
     * @param type $assembly le niveau 1 à supprimer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction($assembly)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $assembly = $em->getRepository('ArtoAcvBundle:Assembly')->find($assembly);

        $em->remove($assembly);
        $em->flush();

        return new Response();
    }
}
