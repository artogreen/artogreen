<?php

namespace Arto\AcvBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Arto\AcvBundle\Entity\Base;
use Arto\AcvBundle\Entity\Project;
use Arto\AcvBundle\Entity\Product;
use Arto\AcvBundle\Entity\Assembly;
use Arto\AcvBundle\Entity\Family;
use Arto\AcvBundle\Entity\Type;
use Arto\AcvBundle\Entity\Part;


class PartController extends Controller
{
    /**
     * Affiche la liste des parts
     * @param type $project le projet
     * @param type $id l'id du produit
     * @return La liste des parts
     */
    public function indexAction($project, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($id);
        $assemblies = $em->getRepository('ArtoAcvBundle:Assembly')->findBy(array('product' => $product->getId()));
        //$allCodesMatieres = $em->getRepository('ArtoAcvBundle:Base')->findBy(array('isShown' => 'oui', 'type' => '2'));
        //$allCodesProcesses = $em->getRepository('ArtoAcvBundle:Base')->findBy(array('isShown' => 'oui', 'type' => '1'));
        
        
        // Find types for none process staff
        $types = array();
        $types_list = $em->getRepository('ArtoAcvBundle:Type')->findBy(array(), array('name' => 'ASC'));
        foreach($types_list as $type) {
            if ($type->getName() != 'process') {
                $types[] = $type;
            }
        }

        // Find process families
        $process_families = array();
        $type = $em->getRepository('ArtoAcvBundle:Type')->findOneByName('process');
        $processes = $em->getRepository('ArtoAcvBundle:Base')->findBy(
            array('type' => $type->getId(), 'isVisible' => 'oui'),
            array('name' => 'ASC')
        );

        foreach($processes as $p) {
            $family_id = $p->getFamily()->getId();
            if (!array_key_exists($family_id, $process_families)) {
                $process_families[$family_id] = $p->getFamily()->getName();
            }
        }

        $collator = new \Collator('fr_FR');
        $collator->asort($process_families);

        // Find transports
        $transport = $em->getRepository('ArtoAcvBundle:Family')->findOneByName('transport');
        $transports = $em->getRepository('ArtoAcvBundle:Base')->findBy(
            array('family' => $transport->getId(), 'isVisible' => 'oui'),
            array('name' => 'ASC')
        );
        
        //Find materials for density
        $materialsDensity = $em->getRepository('ArtoAcvBundle:Density')->findBy(
                array(),
                array('material' => 'ASC'));
        
        $additivesType = $em->getRepository('ArtoAcvBundle:Type')->findOneBy(
                array('name' => 'additif'));
        $additiveId = $additivesType->getId();
        
        //Plastic
        //Normal plastic
        $normalPlasticFamily = $em->getRepository('ArtoAcvBundle:Family')->findOneBy(
                array('name' => 'plastique'));
        $normalPlasticId = $normalPlasticFamily->getId();
         
        //Textile plastic
        $textilePlasticFamily = $em->getRepository('ArtoAcvBundle:Family')->findOneBy(
                array('name' => 'textile plastique'));
        $textilePlasticId = $textilePlasticFamily->getId();
        
        //Bio plastic
        $bioPlasticFamily = $em->getRepository('ArtoAcvBundle:Family')->findOneBy(
                array('name' => 'bio-plastique'));
        $bioPlasticId = $bioPlasticFamily->getId();
        
        //Autres
        $otherFamily = $em->getRepository('ArtoAcvBundle:Family')->findOneBy(
                array('name' => 'autres'));
        $otherId = $otherFamily->getId();
        
        $sql2 = "SELECT c FROM ArtoAcvBundle:Base c WHERE c.type=:type AND (c.family=:plasticId OR c.family=:textilePlasticId OR c.family=:bioPlasticId or c.family=:otherId) AND c.isVisible=:answerId ORDER BY c.name";
        $query2 = $em->createQuery($sql2)->setParameters(
                array('type' => $additiveId, 'plasticId' => $normalPlasticId, 'textilePlasticId' => $textilePlasticId, 'bioPlasticId' => $bioPlasticId, 'otherId' => $otherId, 'answerId' => 'oui'));
        
        $plasticAdditives = $query2->getResult();
        
        //Colorants
        $colorantFamily =  $em->getRepository('ArtoAcvBundle:Family')->findOneBy(
                array('name' => 'colorant'));
        $colorantId = $colorantFamily->getId();
        
        $sql3 = "SELECT c FROM ArtoAcvBundle:Base c WHERE c.type=:type AND c.family=:colorantId AND c.isVisible=:answerId ORDER BY c.name";
        $query3 = $em->createQuery($sql3)->setParameters(
                array('type' => $additiveId, 'colorantId' => $colorantId, 'answerId' => 'oui'));
        
        $plasticColorants = $query3->getResult();
        
        //Procédés finition
        $processesType = $em->getRepository('ArtoAcvBundle:Type')->findOneBy(
                array('name' => 'process'));
        $processesId = $processesType->getId();
        
        $finitionFamily = $em->getRepository('ArtoAcvBundle:Family')->findOneBy(
                array('name' => 'finition'));
        $finitionId = $finitionFamily->getId();
        
        $sql4 = "SELECT c FROM ArtoAcvBundle:Base c WHERE c.type=:type AND c.family=:normalPlasticId AND c.isVisible=:answerId ORDER BY c.name";
        $query4 = $em->createQuery($sql4)->setParameters(
                array('type' => $processesId, 'normalPlasticId' => $normalPlasticId, 'answerId' => 'oui'));
       
        $plasticProcesses = $query4->getResult();

        return $this->render('ArtoAcvBundle:Default:parts.html.twig', array(
            'project' => $project,
            'product' => $product,
            'assemblies' => $assemblies,
            'types' => $types,
            'process_families' => $process_families,
            'transports' => $transports,
            'materialsDensity' => $materialsDensity,
            'plasticAdditives' => $plasticAdditives,
            'plasticColorants' => $plasticColorants,
            'plasticProcesses' => $plasticProcesses
        ));
    }
    
    /**
     * Sauvegarde une part
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function saveAction()
    {
        $request = $this->getRequest();
        $assembly_id = $request->get('assembly');
        $em = $this->getDoctrine()->getEntityManager();
        $part = null;
        $child = null;

        $assembly = $em->getRepository('ArtoAcvBundle:Assembly')->find($assembly_id);

        if ($request->get('name') != null) {
            if ($request->get('parent_id') != null) {
                $part = $em->getRepository('ArtoAcvBundle:Part')->find($request->get('parent_id'));
                $part->setName($request->get('name'));
                $part->setQuantity($request->get('quantity'));
                $em->persist($part);
                $em->flush();
            } else {
                if ($request->get('material') != null) {
                    $part = new Part();
                    $part->setName($request->get('name'));
                    $part->setQuantity($request->get('quantity'));
                    $part->setAssembly($assembly);
                    $em->persist($part);
                    $em->flush();
                }
            }
        }

        if ($request->get('id') != null) {
            $child = $em->getRepository('ArtoAcvBundle:Part')->find($request->get('id'));
        } else {
            if ($part == null) {
                $part = $em->getRepository('ArtoAcvBundle:Part')->find($request->get('parent_id'));
            }
            $child = new Part();
            $child->setParent($part);
            $child->setAssembly($assembly);
        }
        
        $weight = 0;

        $material = $em->getRepository('ArtoAcvBundle:Base')->find($request->get('material'));
        if ($material != null){
            $child->setMaterial($material);
        
            //Replace , by .
            $param1 = $request->get('param1');
            $param2 = $request->get('param2');

            if($param1 != null){
                $param1 = str_replace(',','.',$param1);
            }

            if($param2 != null){
                $param2 = str_replace(',','.',$param2);
            }

            $child->setParam1($param1);
            $child->setParam2($param2);

            if ($material->getUnit1() == "g") {
                if ($param1 != null) {
                    $val = $param1;
                    $weight = $val;
                }
            } else if ($material->getUnit1() == "UN") {
                if ($material->getUnit2() == "g") {
                    if ($param1 != null) {
                        $val = $param1;
                        $weight = $val * $material->getMultiplicateur();
                    }
                } else if ($material->getUnit2() == "kg") {
                    if ($param1 != null) {
                        $val = $param1;
                        $weight = $val * $material->getMultiplicateur();
                    }
                } else {
                    if ($param1 != null) {
                        if ($material->getUnit2() == "") {
                            $val = $param1;
                            $weight = $val * $material->getMultiplicateur();
                        } else {
                            $val = $param1;
                            $weight = $val * $material->getValParam2ByUN() * $material->getMultiplicateur();
                        }
                    }
                }
            } else {
                if ($param1 != null) {
                    if ($material->getUnit2() == "") {
                        $val = $param1;
                        $weight = $val * $material->getMultiplicateur();
                    } else {
                        $val = $param1;
                        $weight = $val * $material->getValParam2ByUN() * $material->getMultiplicateur();
                    }
                }
            }
        }

        $process = $em->getRepository('ArtoAcvBundle:Base')->find($request->get('process'));
        if ($process != null){
            $child->setProcess($process);
        
            //Replace , by .
            $param3 = $request->get('param3');
            $param4 = $request->get('param4');


            if($param3 != null){
                $param3 = str_replace(',','.',$param3);     
            }

            if($param4 != null){
                $param4 = str_replace(',','.',$param4);
            }

            $child->setParam3($param3);
            $child->setParam4($param4);
        }

        $transport = $em->getRepository('ArtoAcvBundle:Base')->find($request->get('transport'));
        if ($transport != null){
            $child->setTransport($transport);
            $child->setDistance($request->get('distance'));
        }

        $em->persist($child);
        $em->flush();

        $result = array(
            'parent_id' => ($part != null) ? $part->getId() : $child->getParent()->getId(),
            'id' => $child->getId(),
            'weight' => $weight
        );

        $json = json_encode($result);
        $response = new Response($json);
        return $response;
    }
    
    /**
     * Supprime une part
     * @param type $id l'id de la part
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction($id)
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();

        $part = $em->getRepository('ArtoAcvBundle:Part')->find($id);

        $em->remove($part);
        $em->flush();

        return new Response();
    }
    
    /**
     * calcule la masse d'un matériau avec sa densité
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function calculMasseDensityAction(){
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();
        
        $materiauId = $request->get('materiauId');
        $volumeMateriau = $request->get('volumeMateriau');
        
        $materiau = $em->getRepository('ArtoAcvBundle:Density')->find($materiauId);
        
        $density = $materiau->getDensity();
        
        $masseMateriau = $volumeMateriau * $density;
        
        $result = array(
            'masseMateriau' => $masseMateriau
        );
        
        $json = json_encode($result);
        return new Response($json);    
    }
    
    /**
     * Vérifie le poids d'un matériau
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function checkWeightAction(){
        $request = $this->getRequest();
        
        $value = $request->get('weight');
        
        $result = array(
            'value' => $value
        );
        
        $json = json_encode($result);
        return new Response($json); 
    }

}
