<?php

namespace Arto\AcvBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Arto\AcvBundle\Entity\Base;
use Arto\AcvBundle\Entity\Project;
use Arto\AcvBundle\Entity\Assembly;
use Arto\AcvBundle\Entity\Family;
use Arto\AcvBundle\Entity\Product;
use Xlab\pChartBundle\pData;
use Xlab\pChartBundle\pDraw;
use Xlab\pChartBundle\pRadar;
use Xlab\pChartBundle\pImage;


class ComparisonController extends Controller
{   
    /**
     * Affiche la page de comparaison
     * @param type $project le projet dans lequel on se trouve
     * @param type $id  l'id du produit
     * @return type la page de comparaison
     */
    public function indexAction($project, $id)
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $userId = $user->getId();
        
        $comparedProject = null;
        $comparedFolder = null;
        
        $em = $this->getDoctrine()->getEntityManager();
        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($id);
        $projects = $em->getRepository('ArtoAcvBundle:Project')->findAllActive($user);
        
        $comparedProjectId = $project->getComparedProjectId();
        
        if($comparedProjectId != null && $comparedProjectId != ''){
            $comparedProject = $em->getRepository('ArtoAcvBundle:Project')->find($comparedProjectId);
            $comparedFolder = $comparedProject->getFolder();
        }
        

        return $this->render('ArtoAcvBundle:Default:comparison.html.twig', array(
            'projects' => $projects,
            'project'    => $project,
            'product'    => $product,
            'comparedProject' => $comparedProject,
            'comparedFolder' => $comparedFolder,
            'children'     => ($project->getParent() == null) ?  $project->getChilds() : $project->getParent()->getChilds()
        ));
    }
    
    /**
     * Action qui permet de comparer des projets
     * @return type La page avec les 2 projets comparés
     */
    public function compareProjectAction(){
        $user = $this->get('security.context')->getToken()->getUser();
        $userId = $user->getId();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();
        
        $projectId = $request->get('project');
        $project = $em->getRepository('ArtoAcvBundle:Project')->find($projectId);
        
        $productId = $request->get('product');
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($productId);
        
        $projects = $em->getRepository('ArtoAcvBundle:Project')->findAllActive($user);
        
        $comparedProjectId = $request->get('choixProjet');
        $comparedProject = $em->getRepository('ArtoAcvBundle:Project')->find($comparedProjectId);
        $comparedFolder = $comparedProject->getFolder();
        
        $project->setComparedProjectId($comparedProjectId);
        $em->persist($project);
        $em->flush();
        
        return $this->render('ArtoAcvBundle:Default:comparison.html.twig', array(
            'projects' => $projects,
            'project'    => $project,
            'product'    => $product,
            'comparedProject'     => $comparedProject,
            'comparedFolder' => $comparedFolder
        ));
    }
}
