<?php

namespace Arto\AcvBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Arto\AcvBundle\Entity\Base;
use Arto\AcvBundle\Entity\Project;
use Arto\AcvBundle\Entity\EolPart;
use Arto\AcvBundle\Entity\Assembly;
use Arto\AcvBundle\Entity\Family;
use Arto\AcvBundle\Entity\Product;
use Xlab\pChartBundle\pData;
use Xlab\pChartBundle\pDraw;
use Xlab\pChartBundle\pRadar;
use Xlab\pChartBundle\pImage;


class ResultController extends Controller
{
    /**
     * Pour appeler le service Calculator
     * @return Une instance de Calculator
     */
    private function getCalculator()
    {
        return $this->get('arto.acv.calculator');
    }
    
    /**
     * Pour appeler le service Drawer
     * @return Une instance de Drawer
     */
    private function getDrawer()
    {
        return $this->get('arto.acv.drawer');
    }
    
    /**
     * Affiche le bilan d'un projet
     * @param type $project le projet
     * @param type $id l'id du produit
     * @return Le bilan du projet
     */
    public function indexAction($project, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $calculator = $this->getCalculator();
        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($id);

        $categories = array('rmd', 'ed', 'wd', 'gw', 'od', 'at', 'poc', 'aa', 'wt', 'we', 'hwp');
        
        // Initialize array to 11 values set to 0
        $m = $calculator->calculateMaterial($product);
        $p = $calculator->calculateProcess($product);
        $a = $calculator->calculateManufacturing($product);
        $d = $calculator->calculateDelivery($product);
        $u = $calculator->calculateUsage($product);
        $energy = $calculator->calculateEnergy($product);
        $c = $calculator->calculateConsommables($product);
        $i = $calculator->calculateInstall($product);
        $e = $calculator->calculateEol($product);
        
        $t = $calculator->calculateTotalsForIndex($m,$p,$a,$d,$u,$energy,$c,$i,$e);
        $tRelative = $calculator->calculateTotalsRelativeForIndex($m,$p,$a,$d,$u,$energy,$c,$i,$e);

        return $this->render('ArtoAcvBundle:Default:result.html.twig', array(
            'project'    => $project,
            'product'    => $product,
            'categories' => $categories,
            'materials'  => $m,
            'processes'  => $p,
            'manufacturings'  => $a,
            'deliveries' => $d,
            'usages'     => $u,
            'installs'     => $i,
            'eols'     => $e,
            'totals'     => $t,
            'totalsRelative' => $tRelative,
            'consommables' => $c,
            'energy' => $energy
        ));
    }
    
    /**
     * Affiche le graphique du bilan du projet
     * @param type $project le projet
     * @param type $id l'id du Produit
     * @return \Symfony\Component\HttpFoundation\Response Affiche le graphique du bilan du projet
     */
    public function chart1Action($project, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        
        $drawer = $this->getDrawer();
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($id);

        $response = new Response();
        $response->headers->set('Content-Type', 'image/png');
        
        $chart1 = $drawer->drawChart1($product);
        $response->setContent($chart1);

        return $response;
    }
    
    /**
     * 
     * @param type $project Le projet
     * @param type $comparedProject Le projet comparé
     * @return \Symfony\Component\HttpFoundation\Response affiche le graphique de comparaison
     */
    public function comparisonChartAction($project, $comparedProject)
    {
        $em = $this->getDoctrine()->getEntityManager();
        
        $drawer = $this->getDrawer();
        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        $comparedProject = $em->getRepository('ArtoAcvBundle:Project')->find($comparedProject);

        $response = new Response();
        $response->headers->set('Content-Type', 'image/png');
        
        $comparisonChart = $drawer->drawComparisonChart($project, $comparedProject);
        $response->setContent($comparisonChart);
        return $response;

    }

    /**
     * @param type $project le projet 
     * @param type $comparedProject le projet comparé
     * @return type Le tableau de comparaison entre les 2 projets
     */
    public function comparisonTableAction($project, $comparedProject)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $calculator = $this->getCalculator();

        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        $comparedProject = $em->getRepository('ArtoAcvBundle:Project')->find($comparedProject);

        $base = $calculator->calculateAll($project->getFirstProduct());

        $results = array();
        
        $tab =  $calculator->calculateAll($comparedProject->getFirstProduct());

        for ($k = 0; $k <= 10; $k++) {
            if($base[$k] != 0){
                $tab[$k] = $tab[$k] / $base[$k] * 100;
                $results[$k] = round($tab[$k], 2);
            }
            else{
                $results[$k] = 0;
            }
        }
        
        $base = array_fill(0, 11, 100);

        return $this->render('ArtoAcvBundle:Default:comparison_table.html.twig', array(
            'project'    => $project,
            'comparedProject' => $comparedProject,
            'childs'     => $project->getChilds(),
            'results'    => $results
        ));

    }
    
    /**
     * Affiche le tableau "Coûts environnementaux pour une PVA de 1 produit durant 3 ans"
     * @param type $project le projet
     * @param type $comparedProject le projet comparé
     * @param type $pva la PVA
     * @return type Le tableau "Coûts environnementaux pour une PVA de 1 produit durant 3 ans"
     */
    public function comparisonTable2Action($project, $comparedProject, $pva)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $calculator = $this->getCalculator();

        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        $comparedProject = $em->getRepository('ArtoAcvBundle:Project')->find($comparedProject);

        $results = array();
        $euroCostsComparedProject = $calculator->calculateEuroCosts($comparedProject, $pva);
        $euroCostsProject = $calculator->calculateEuroCosts($project, $pva);
       
        $results[0] = round($euroCostsComparedProject['co2_euros'],0)*3;
        $results[1] = round($euroCostsComparedProject['water_euros'],0)*3;
        $results[2] = round($euroCostsComparedProject['power_euros'],0)*3;
        $results[3] = round($euroCostsComparedProject['recycle_euros'],0)*3;
        
        $results[4] = round($euroCostsProject['co2_euros'],0)*3;
        $results[5] = round($euroCostsProject['water_euros'],0)*3;
        $results[6] = round($euroCostsProject['power_euros'],0)*3;
        $results[7] = round($euroCostsProject['recycle_euros'],0)*3;

        return $this->render('ArtoAcvBundle:Default:comparison_table2.html.twig', array(
            'project'    => $project,
            'comparedProject' => $comparedProject,
            'childs'     => $project->getChilds(),
            'results'    => $results
        ));

    }
 
    /**
     * Affiche la page "Etiquette"
     * @param type $project le projet
     * @param type $id l'id du produit
     * @return \Symfony\Component\HttpFoundation\Response la page "Etiquette"
     */
    public function stickerAction($project, $id)
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();
        $calculator = $this->getCalculator();
        
        $siteArtogreen = $this->getRequest()->getScheme() . '://' . $this->getRequest()->getHttpHost() . $this->getRequest()->getBasePath();

        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($id);

        $params = $calculator->calculateStickerDatas($project, $product, $siteArtogreen);
        
        if ($request->get('pdf') == 1) {
            $html = $this->renderView('ArtoAcvBundle:Default:sticker_pdf.html.twig', $params);

            return new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
                200,
                array(
                    'Content-Type'          => 'application/pdf',
                    //'Content-Disposition'   => 'attachment; filename="ArtoACV_etiquette.pdf"'
                )
            );

        } else {
            return $this->render('ArtoAcvBundle:Default:sticker.html.twig', $params);
        }
    }
    
    /**
     * Vérifie que l'onglet Utilisation a été renseigné, donc qu'il existe une durée de vie
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function stickerCheckLifetimeAction(){
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        
        $projectId = $request->get('project');
        $productId = $request->get('product');
        
        $project = $em->getRepository('ArtoAcvBundle:Project')->findOneBy(
            array('id' => $projectId));
        
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($productId);
        
        $usage = $product->getUsage();
        
        $lifetime = 0;
        
        if($usage != null){
            $lifetime = $usage->getLifetime();
        }
        
        $result = array(
            'id' => $product->getId(),
            'lifetime' => $lifetime,
            'usage' => $usage
        );
        
        $json = json_encode($result);
        return new Response($json);  
    }
}