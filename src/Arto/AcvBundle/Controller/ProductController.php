<?php

namespace Arto\AcvBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Arto\AcvBundle\Entity\Base;
use Arto\AcvBundle\Entity\Project;
use Arto\AcvBundle\Entity\Assembly;
use Arto\AcvBundle\Entity\Family;
use Arto\AcvBundle\Entity\Type;


class ProductController extends Controller
{
    /**
     * Affiche la page d'un produit
     * @param type $project le projet
     * @param type $id l'id du produit
     * @return la page du produit
     */
    public function indexAction($project, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($id);

        return $this->render('ArtoAcvBundle:Default:product.html.twig', array('project' => $project, 'product' => $product));
    }

}
