<?php

namespace Arto\AcvBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Arto\GreenBundle\Controller\TokenAuthenticatedController;
use Arto\GreenBundle\Entity\User;
use Arto\GreenBundle\Entity\License;
//use Arto\GreenBundle\Entity\Product;
use Arto\AcvBundle\Entity\Base;
use Arto\AcvBundle\Entity\Project;
use Arto\AcvBundle\Entity\Product;
use Arto\AcvBundle\Entity\Assembly;
use Arto\AcvBundle\Entity\Family;
use Arto\AcvBundle\Entity\Type;
use Arto\AcvBundle\Entity\ProductPart;
use Arto\AcvBundle\Entity\Folder;
use Arto\AcvBundle\Entity\UserProductPart;
use Arto\AcvBundle\Entity\Part;
use Arto\AcvBundle\Entity\Log;
use Arto\GreenBundle\Entity\GreenLog;

use n3b\Bundle\Util\HttpFoundation\StreamResponse\StreamResponse,
    n3b\Bundle\Util\HttpFoundation\StreamResponse\FileWriter,
    n3b\Bundle\Util\HttpFoundation\StreamResponse\StreamWriterWrapper;


class DefaultController extends Controller implements TokenAuthenticatedController
{
    /**
     * Fonction pour passer une date PHP au format SQL
     * @param type $date une date PHP
     * @return \DateTime la date au format DateTime SQL
     */
    private function dateToSQL($date) {
        $returnDate = null;

        $s=substr($date, 6, 4)
            ."-".substr($date, 3, 2)
            ."-".substr($date, 0, 2);

        try {
           $returnDate = new \DateTime($s);
        } catch (\Exception $e) {
            // Nothing to do
        }

        return $returnDate;
    }
    
    /**
     * Affiche la page d'accueil ArtoAcv
     * @return type la page d'accueil ArtoAcv
     */
    public function indexAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $userId = $user->getId();
        
        $userName = $user->getUserName();
        
        if($userName != 'artogreen'){
            $this->logArtoAcv($user);
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        
        $folderLetters = array();

        $projects = $em->getRepository('ArtoAcvBundle:Project')->findAllActive($user);
        
        //projectsWithoutFolder
        $projectsWF = $em->getRepository('ArtoAcvBundle:Project')->findBy(
                array('greenUser' => $userId, 'folder' => null, 'parent' => null), array('name' => 'ASC'));
        
        $folders = $em->getRepository('ArtoAcvBundle:Folder')->findBy(
                array('user' => $userId), array('name' => 'ASC'));
        
        foreach ($folders as $folder) {
            $folderName = $folder->getName();
            $firstLetter = strtoupper(substr($folderName, 0, 1));

            if (!in_array($firstLetter, $folderLetters)) {
                array_push($folderLetters, $firstLetter);
            }
        }

        $templateImportExcelUrl = __DIR__.'/../../../../web/template_import_ArtoAcv.xlsm';

        return $this->render('ArtoAcvBundle:Default:index.html.twig', array('projects' => $projects, 'user' => $userId, 'folders' => $folders,'projectsWF'=> $projectsWF,'myLetter' => '', 'templateUrl' => $templateImportExcelUrl, 'folderLetters' => $folderLetters));
    }
    
    /**
     * Enregistre un log pour un utilisateur
     * @param type $user l'utilisateur dont on veut enregistrer le log
     */
    public function logArtoAcv($user){
        $em = $this->getDoctrine()->getEntityManager();
        
        $today = new \DateTime('NOW');
        $userName = $user->getUsername();
        $userPwd = $user->getPassword();
        $userEmail = $user->getEmail();
        
        $log = new Log();
        $log->setDate($today);
        $log->setUserName($userName);
        $log->setUserPwd($userPwd);
        $log->setUserEmail($userEmail);
        
        $greenLog = new GreenLog();
        $greenLog->setDate($today);
        $greenLog->setUserName($userName);
        $greenLog->setUserPwd($userPwd);
        $greenLog->setUserEmail($userEmail);
        $greenLog->setSoftware("ArtoAcv");
        
        $em->persist($log);
        $em->persist($greenLog);
        $em->flush();
    }
    
    /**
     * Fonction qui fait l'import ArtoAcv
     */
    public function importAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        if (($handle = fopen("/Users/nay/Clients/Artogreen/base.csv", "r")) !== FALSE) {

            while (($row = fgetcsv($handle, 0, ";")) !== FALSE) {
                $type = $em->getRepository('ArtoAcvBundle:Type')->findOneByName(trim($row[0]));
                if ($type == null) {
                    $type = new Type();
                    $type->setName(trim($row[0]));
                    $em->persist($type);
                    $em->flush();
                }

                $family = $em->getRepository('ArtoAcvBundle:Family')->findOneByName(trim($row[1]));
                if ($family == null) {
                    $family = new Family();
                    $family->setName(trim($row[1]));
                    $em->persist($family);
                    $em->flush();
                }
            }

            fclose($handle);
        }

        if (($handle = fopen("/Users/nay/Clients/Artogreen/base.csv", "r")) !== FALSE) {
            while (($row = fgetcsv($handle, 0, ";")) !== FALSE) {
                //$num = count($data);
                //echo "<p> $num champs à la ligne $row: <br /></p>\n";
                $base = $em->getRepository('ArtoAcvBundle:Base')->findOneByCode(trim($row[3]));
                if ($base == null) {
                    $base = new Base();
                }
                $type = $em->getRepository('ArtoAcvBundle:Type')->findOneByName(trim($row[0]));
                $base->setType($type);

                $family = $em->getRepository('ArtoAcvBundle:Family')->findOneByName(trim($row[1]));
                $base->setFamily($family);

                $base->setName($row[2]);
                $base->setCode($row[3]);
                $base->setUnit1($row[5]);
                $base->setUnit2($row[7]);
                $base->setUnitMass($row[8]);
                $base->setModuleCode($row[9]);
                $base->setDescription($row[11]);
                $base->setAa(str_replace(',', '.', $row[17]));
                $base->setAt(str_replace(',', '.', $row[18]));
                $base->setEd(str_replace(',', '.', $row[19]));
                $base->setGwp(str_replace(',', '.', $row[20]));
                $base->setHwp(str_replace(',', '.', $row[21]));
                $base->setOdp(str_replace(',', '.', $row[22]));
                $base->setPocp(str_replace(',', '.', $row[23]));
                $base->setRmd(str_replace(',', '.', $row[24]));
                $base->setWd(str_replace(',', '.', $row[25]));
                $base->setWe(str_replace(',', '.', $row[26]));
                $base->setWt(str_replace(',', '.', $row[27]));

                $em->persist($base);
                $em->flush();
            }
            fclose($handle);
        }

        exit();
    }
    
    /**
     * Pour aller à la page Indicateurs
     * @param type $project le projet dans lequel on se trouve
     * @return type la page Indicateurs
     */
    public function generalAction($project){
        $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);

        $products = $em->getRepository('ArtoAcvBundle:Product')->findBy(array('project' => $project->getId()));
        $product = $products[0];

        return $this->render('ArtoAcvBundle:Default:general.html.twig', array(
            'project' => $project,
            'product' => $product
        ));
    }
    
    /**
     * Pour aller à la page Glossaire
     * @return type la page Glossaire
     */
    public function termsAction(){
        return $this->render('ArtoAcvBundle:Default:terms.html.twig');
    }
    
    /**
     * Pour aller à la page Instructions
     * @param type $project le projet
     * @return type la page Instructions
     */
    public function instructionsAction($project){
        $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);

        $products = $em->getRepository('ArtoAcvBundle:Product')->findBy(array('project' => $project->getId()));
        $product = $products[0];

        return $this->render('ArtoAcvBundle:Default:instructions.html.twig', array(
            'project' => $project,
            'product' => $product
        ));
    }

    /**
     * Pour créer un nouveau projet ArtoAcv
     * @return type La page de description sur laquelle on arrive après avoir créé le projet ArtoAcv
     */
    public function createAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();

        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        $dossierId = $request->get('dossier');

        $project = new Project();
        $project->setGreenUser($user);
        $project->setName($request->get('name'));
        $project->setWeight(0);
        $project->setLifetime(0);

        $product = new Product();
        $product->setProject($project);
        $product->setName('Produit 1');
        $product->setTauxRecyclabilité(0);
        
        $em->persist($project);
        
        if($dossierId == ''){
            $project->setFolder(null);
            $em->persist($project);
        }else{
            $dossier = $em->getRepository('ArtoAcvBundle:Folder')->find($dossierId);

            $dossier->addProject($project); 
            $project->setFolder($dossier);

            $em->persist($dossier);
            $em->persist($project);
        }

        $em->persist($product);
        $em->flush();

        return $this->redirect($this->generateUrl('acv_project_description', array('id' => $project->getId())));
    }
    
    /**
     * Pour effacer un projet ArtoAcv 
     * @return type La page ou sont listés les projets ArtoAcv de l'utilisateur
     */ 
    public function deleteAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();

        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        $id = $request->get('id');
        $special = $request->get('special');

        $project = $em->getRepository('ArtoAcvBundle:Project')->find($id);
        
        $isCorrectUser = $project->getGreenUser()->getId() == $user->getId();
        $isSpecial = isset($special) && $special != null;

        if ($isCorrectUser || $isSpecial) {
            $em->remove($project);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('acv'));
    }
    
    /**
     * Pour renommer un projet ArtoAcv
     * @return type La page ou sont listés les projets ArtoAcv de l'utilisateur
     */
    public function renameAction(){
        $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        
        $id = $request->get('id');
        $newName = $request->get('name');

        $project = $em->getRepository('ArtoAcvBundle:Project')->find($id);
        
        $project->setName($newName);
        
        $em->persist($project);
        $em->flush();
               
        return $this->redirect($this->generateUrl('acv'));  
    }
    
    /**
     * Pour aller sur la page Description
     * @return type La page Description
     */
    public function descriptionAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        $id = $request->get('id');
        
        $project = $em->getRepository('ArtoAcvBundle:Project')->find($id);
        $operations = $em->getRepository('ArtoAcvBundle:Operation')->findBy(array('project' => $project->getId()));

        $products = $em->getRepository('ArtoAcvBundle:Product')->findBy(array('project' => $project->getId()));

        $product = $products[0];

        $transport = $em->getRepository('ArtoAcvBundle:Family')->findOneByName('transport');
        //$transports = $em->getRepository('ArtoAcvBundle:Base')->findByFamily($transport->getId());
         $transports = $em->getRepository('ArtoAcvBundle:Base')->findBy(array('family' => $transport->getId(), 'isVisible' => 'oui'));

        return $this->render('ArtoAcvBundle:Default:description.html.twig', array(
            'project' => $project,
            'operations' => $operations,
            'product' => $product,
            'transports' => $transports
        ));
    }
    
    /**
     * Pour sauvegarder les informations saisies sur la page Description, dans la section "Description du produit"
     * @return type La page Description, avec les informations saisies dans la section "Description du produit" enregistrées
     */
    public function descriptionSaveAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        $project = $request->get('project');
        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);

        $project->setRef($request->get('ref'));
        $project->setProjectName($request->get('projectName'));
        $project->setDescription($request->get('description'));
        $project->setDescriptionuf($request->get('descriptionuf'));
        $project->setCity($request->get('city'));
        $project->setPilote($request->get('pilote'));
        $project->setMassUnit($request->get('projectMassUnit'));
        /*$project->setLifetime($request->get('lifetime'));
        
        $energyConsumer = $request->get('energyConsumer');
        
        if($energyConsumer == 'yes'){
            $project->setEnergyConsumer(true);
        }elseif ($energyConsumer == 'no') {
            $project->setEnergyConsumer(false);
        }*/

        if ($request->get('pilote_date') != null) {
            $project->setPiloteDate($this->dateToSQL($request->get('pilote_date')));
        }

        $uploadedFile = $_FILES['file'];
        if (isset($uploadedFile['name'])) {
            if($uploadedFile['name'] != ''){
                move_uploaded_file($uploadedFile['tmp_name'], __DIR__.'/../../../../web/uploads/acv/'.$uploadedFile['name']);
                $project->setPath($uploadedFile['name']);
            }
        }

        $em->persist($project);
        $em->flush();

        return $this->redirect($this->generateUrl('acv_project_description', array('id' => $project->getId())));
    }
    
    /**
     * Pour sauvegarder les informations saisies sur la page Description, dans la section "Configuration des transports"
     * @return type La page Description, avec les informations saisies dans la section "Configuration des transports" enregistrées
     */
    public function transportSaveAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        $project = $request->get('project');
        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);

        $project->setTransportAppro($request->get('transport_appro'));

        $transportStock = $em->getRepository('ArtoAcvBundle:Base')->find($request->get('transport_stock'));
        $project->setTransportStock($transportStock);

        $project->setTransportStockKm($request->get('transport_stock_km'));

        $em->persist($project);
        $em->flush();

        return $this->redirect($this->generateUrl('acv_project_description', array('id' => $project->getId())));
    }
    
    /**
     * Pour sauvegarder les informations saisies dans la page Description section "Liste des produits constituant l'UF"
     * @return type la page Description avec les informations saisies dans la section "Liste des produits constituant l'UF" enregistrées
     */
    public function productSaveAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        $project = $request->get('project');
        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);

        $products = $em->getRepository('ArtoAcvBundle:Product')->findBy(array('project' => $project->getId()));
        $product = $products[0];

        $total_weight = 0;
        
        $massUnit = $project->getMassUnit();
        if($massUnit == 'g'){
            $mul = 1;
        }elseif ($massUnit == 'kg') {
            $mul = 1000;
        }elseif ($massUnit == 't') {
            $mul = 1000000;
        }

        foreach($product->getParts() as $part) {
            $id = $part->getId();
            $part->setType($request->get('type_'.$id));
            $part->setRef($request->get('ref_'.$id));
            $part->setName($request->get('name_'.$id));
            $part->setQuantity($request->get('quantity_'.$id));
            $weight = $request->get('weight_'.$id);
            $weight = str_replace(',','.',$weight);
            $part->setWeight($weight * $mul);
            $total_weight += ($part->getQuantity() * $part->getWeight());
            $em->persist($part);
        }

        if ($request->get('ref') != null && $request->get('name') != null && $request->get('weight') != null) {
            $part = new ProductPart();
            $part->setProduct($product);
            $part->setType($request->get('type'));
            $part->setRef($request->get('ref'));
            $part->setName($request->get('name'));
            $part->setQuantity($request->get('quantity'));
            $part->setWeight($request->get('weight'));
            $total_weight += ($part->getQuantity() * $part->getWeight());
            $em->persist($part);
        }
        
        $product->setWeight($total_weight);
        $em->persist($product);
        $em->flush();

        return $this->redirect($this->generateUrl('acv_project_description', array('id' => $project->getId())));
    }
    
    /**
     * Pour effacer une des parties des produits constituant l'UF
     * @return \Symfony\Component\HttpFoundation\Response La page Description avec la liste des produits constituant l'UF moins la partie supprimée
     */
    public function productDeleteAction(){
        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        $project = $request->get('project');
        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);

        $products = $em->getRepository('ArtoAcvBundle:Product')->findBy(array('project' => $project->getId()));
        $product = $products[0];
        
        $weight = $product->getWeight();
        
        foreach($product->getParts() as $part) {
            $id = $part->getId();
            if($id == $request->get('partId')){
                $partWeight = $part->getWeight();
                $newWeight = $weight - $partWeight;
                $product->setWeight($newWeight);
                $em->remove($part);
            }     
        }
        $em->persist($product);
        $em->flush();
        
        $result = array(
            'newWeight' => $newWeight
        );
        
        $json = json_encode($result);
        $response = new Response($json);
        return $response;
    }
    
    /**
     * 
     * @param type $project le projet
     * @param type $id le produit
     * @return Affiche le produit correspondant au projet
     */
    public function productAction($project, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($id);

        return $this->render('ArtoAcvBundle:Default:product.html.twig', array('project' => $project, 'product' => $product));
    }
    
    /**
     * Pour dupliquer un projet
     * @param type $id l'ID du projet à dupliquer
     * @return type la liste des projets ArtoAcv
     */
    public function duplicateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoAcvBundle:Project')->find($id);
        $projectId = $project->getId();

        $project_new = clone $project;
        //$project_new->setParent($project);
        $project_new->setName($project->getName().' - essai '.(count($project->getChilds())+1));

        //$products = array();
        foreach($project->getProducts() as $product) {
            $product_new = clone $product;
            $product_new->setProject($project_new);
            $em->persist($product_new);


            if (count($product->getParts()) > 0) {
                foreach($product->getParts() as $part) {
                    $product_part_new = clone $part;
                    $product_part_new->setProduct($product_new);
                    $em->persist($product_part_new);
                }
            }


            foreach($product->getAssemblies() as $assembly) {
                $assembly_new = clone $assembly;
                $assembly_new->setProduct($product_new);
                $em->persist($assembly_new);

                // Clone parent first
                foreach($assembly->getParts() as $part) {
                    if ($part->getParent() == null) {
                        $part_new = clone $part;
                        $part_new->setAssembly($assembly_new);
                        $em->persist($part_new);

                        // Clone childs
                        foreach($part->getChilds() as $child) {
                            $child_new = clone $child;
                            $child_new->setParent($part_new);
                            $child_new->setAssembly($assembly_new);
                            $em->persist($child_new);
                        }
                    }
                }
            }

            if ($product->getUsage() != null) {
                $usage = clone $product->getUsage();
                $usage->setProduct($product_new);
                $em->persist($usage);

                if (count($product->getUsage()->getParts()) > 0) {
                    foreach($product->getUsage()->getParts() as $part) {
                        $usage_part_new = clone $part;
                        $usage_part_new->setUsage($usage);
                        $em->persist($usage_part_new);
                    }
                }
            }

            if ($product->getDelivery() != null) {
                $delivery = clone $product->getDelivery();
                $delivery->setProduct($product_new);
                $em->persist($delivery);

                if (count($product->getDelivery()->getParts()) > 0) {
                    foreach($product->getDelivery()->getParts() as $part) {
                        $delivery_part_new = clone $part;
                        $delivery_part_new->setDelivery($delivery);
                        $em->persist($delivery_part_new);
                    }
                }
            }

            if ($product->getEol() != null) {
                $eol = clone $product->getEol();
                $eol->setProduct($product_new);
                $em->persist($eol);
                
                $eolParts = $em->getRepository('ArtoAcvBundle:EolPart')->findBy(
                    array('project' => $projectId));
                
                if (count($eolParts) > 0) {
                    foreach($eolParts as $part) {
                        $eol_part_new = clone $part;
                        $eol_part_new->setProject($project_new);
                        $em->persist($eol_part_new);
                    }
                }
            }
        }

        $em->persist($project_new);
        $em->flush();

        return $this->redirect($this->generateUrl('acv'));
    }
    
    /**
     * Sauvegarde la pva
     * @param type $project le projet
     * @param type $id le produit 
     * @return type affiche la page Etiquette d'ArtoAcv
     */
    public function pvaSaveAction($project, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        $project = $request->get('project');
        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($id);

        $project->setPva($request->get('pva'));

        $em->persist($project);
        $em->flush();

        return $this->redirect($this->generateUrl('acv_sticker', array(
            'project' => $project->getId(),
            'id' => $product->getId()
        )));
    }
     
    /**
     * 
     * @return typePour accéder à la gestion ArtoFlux
     */
    public function gestionArtoFluxAction(){
        $user = $this->get('security.context')->getToken()->getUser();
        $userId = $user->getId();

        $em = $this->getDoctrine()->getEntityManager();
        
        $artoFluxParts = $em->getRepository('ArtoGreenBundle:User')->findBy(
                array('acvUserId' => $userId));
        
        $artoFluxProjects = array();
        $artoFluxProducts = array();
        
        foreach($artoFluxParts as $artoFluxPart){
            $projects = $em->getRepository('ArtoFluxBundle:Project')->findBy(
                array('greenUser' => $artoFluxPart->getId()));
            
            $products = $em->getRepository('ArtoAcvBundle:UserProductPart')->findBy(
                array('fluxUserId' => $artoFluxPart->getId()));
            
            $artoFluxProjects[] = $projects;
            $artoFluxProducts[] = $products;
        }
        
        $acvUsers = $em->getRepository('ArtoAcvBundle:User')->findBy(
                array('greenUser' => $userId));
        
        return $this->render('ArtoAcvBundle:Default:gestionArtoFlux.html.twig', array('user' => $user, 'acvUsers' => $acvUsers, 'artoFluxParts' => $artoFluxParts, 'artoFluxProjects' => $artoFluxProjects, 'artoFluxProducts' => $artoFluxProducts));
    }
    
    /**
     * Pour sauvegarder des informations dans la page Gestion ArtoFlux
     */
    public function gestionArtoFluxSaveAction(){
        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        $userId = $request->get('userId');
        
        $user = $em->getRepository('ArtoGreenBundle:User')->findOneBy(
                array('id' => $userId));
        
        $acvLicense = $em->getRepository('ArtoGreenBundle:License')->findOneBy(
                array('user' => $userId, 'product' => '5'));
        
        $productArtoFlux = $em->getRepository('ArtoGreenBundle:Product')->findOneBy(
                array('name' => 'ArtoFlux'));
        
        $artoFluxParts = $this->getRequest()->get('artoFlux');
        
        $newUser = false;
        $forbiddenUsers = array();
        
        foreach($artoFluxParts as $artoFluxPart){
            $license = null;
            // We check if the label of the part is not empty
            if(isset($artoFluxPart['name']) && $artoFluxPart['name'] != ''){
                // Try Retreiving the object if the request id exist
                if (isset($artoFluxPart['id']) && $artoFluxPart['id'] != null) {
                    $p = $em->getRepository('ArtoGreenBundle:User')->find($artoFluxPart['id']);
                } else {
                    // We check if there is no user with same login
                    if(isset($artoFluxPart['username']) && $artoFluxPart['username'] != ''){
                         $checkUser = $em->getRepository('ArtoGreenBundle:User')->findOneBy(
                            array('username' => $artoFluxPart['username']));
                         if($checkUser != null){
                             $forbiddenUsers[] = $artoFluxPart['username'];
                             break;
                         }
                    }
                    $p = new User();
                    $newUser = true;
                    $license = new License();
                    $license->setCreatedAt($acvLicense->getCreatedAt());
                    $license->setEndAt($acvLicense->getEndAt());
                    $license->setDuration($acvLicense->getDuration());
                    $license->setQuantity($acvLicense->getQuantity());
                    $license->setProduct($productArtoFlux);
                }
                
                $p->setFirstname($artoFluxPart['name']);
                
                $p->setLastname('');
                
                $p->setMajority(true);
                
                $p->setAddress('');
                
                $p->setCity('');
                
                $p->setPostcode('');
                
                $p->setAccept1(true);
                
                $p->setAccept2(true);
                
                $p->setUsername($artoFluxPart['username']);
                
                $p->setPassword($artoFluxPart['password']);
                
                $p->setActive(true);
                
                $p->setAcvUserId($userId);
                
                $p->setNbProducts($artoFluxPart['nbProducts']);
                
                $nbProducts = $artoFluxPart['nbProducts'];
                
                $em->persist($p);
                
                if($newUser == true){
                    $em->flush();
                    $userJustCreated = $em->getRepository('ArtoGreenBundle:User')->findOneBy(
                            array('firstname' => $artoFluxPart['name'], 'username' => $artoFluxPart['username'], 'password' => $artoFluxPart['password']));
                    
                    $license->setUser($userJustCreated);
                    $em->persist($license);
                    
                    for($i = 0; $i < $nbProducts; $i++){
                        $indice = $i."";
                        $productName = $artoFluxPart['productName'.$indice];
                        $productReference = $artoFluxPart['productReference'.$indice];
                        
                        $userProductPart = new UserProductPart();
                        
                        $userProductPart->setName($productName);
                        $userProductPart->setReference($productReference);
                        $userProductPart->setFluxUserId($userJustCreated->getId());
                        $em->persist($userProductPart);
                        
                        $project = new \Arto\FluxBundle\Entity\Project();
                        
                        $project->setName($productName);
                        $project->setGreenUser($userJustCreated);
                        $project->setNumero(0);
                        $em->persist($project);
                        $em->flush();
                        
                        $product = new \Arto\FluxBundle\Entity\Product();
                        $product->setProject($project);
                        $product->setName($productName);
                        $product->setRef($productReference);
                        $em->persist($product);
                        $em->flush();
                    }
                    
                    $em->flush();
                    
                    
                    $newUser = false;
                }
            }     
        }
        
        $em->flush();
        
        $newArtoFluxParts = $em->getRepository('ArtoGreenBundle:User')->findBy(
                array('acvUserId' => $userId));
        
        $artoFluxProjects = array();
        $artoFluxProducts = array();
        
        foreach($newArtoFluxParts as $newArtoFluxPart){
            $projects = $em->getRepository('ArtoFluxBundle:Project')->findBy(
                array('greenUser' => $newArtoFluxPart->getId()));
            
            $products = $em->getRepository('ArtoAcvBundle:UserProductPart')->findBy(
                array('fluxUserId' => $newArtoFluxPart->getId()));
            
            $artoFluxProjects[] = $projects;
            $artoFluxProducts[] = $products;
                  
        }
        
        $acvUsers = $em->getRepository('ArtoAcvBundle:User')->findBy(
                array('greenUser' => $userId));
        
        return $this->render('ArtoAcvBundle:Default:gestionArtoFlux.html.twig', array('user' => $user, 'acvUsers' => $acvUsers, 'artoFluxParts' => $newArtoFluxParts, 'artoFluxProjects' => $artoFluxProjects, 'artoFluxProducts' => $artoFluxProducts, 'forbiddenUsers' => $forbiddenUsers));
    }
    
    /**
     * Pour supprimer un élément dans la page Gestion ArtoFlux
     */
    public function gestionArtoFluxDeleteAction(){
        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        $userId = $request->get('userId');
        
        $artoFluxPartId = $request->get('id');
        
        $artoFluxUser = $em->getRepository('ArtoGreenBundle:User')->findOneBy(
                array('id' => $artoFluxPartId));
        
        $artoFluxUserProjects = $em->getRepository('ArtoFluxBundle:Project')->findBy(
                array('greenUser' => $artoFluxUser->getId()));
        
        foreach($artoFluxUserProjects as $artoFluxUserProject){
            $em->remove($artoFluxUserProject);
            $em->flush();
        }
        
        $artoFluxUserLicenses = $em->getRepository('ArtoGreenBundle:License')->findBy(
                array('user' => $artoFluxUser->getId()));
        
        foreach($artoFluxUserLicenses as $artoFluxUserLicense){
           $artoFluxUserToken = $em->getRepository('ArtoGreenBundle:Token')->findOneBy(
                array('license' => $artoFluxUserLicense->getId()));

            $em->remove($artoFluxUserToken);
            $em->flush();  
        }
        
       
        foreach($artoFluxUserLicenses as $artoFluxUserLicense){
            $em->remove($artoFluxUserLicense);
            $em->flush();
        }
        
        $em->remove($artoFluxUser);
        $em->flush();
        
        $user = $em->getRepository('ArtoGreenBundle:User')->findOneBy(
                array('id' => $userId));
        
        $artoFluxParts = $em->getRepository('ArtoGreenBundle:User')->findBy(
                array('acvUserId' => $userId));
        
        $artoFluxProjects = array();
        $artoFluxProducts = array();
        
        foreach($artoFluxParts as $artoFluxPart){
            $projects = $em->getRepository('ArtoFluxBundle:Project')->findBy(
                array('greenUser' => $artoFluxPart->getId()));
            
            $products = $em->getRepository('ArtoAcvBundle:UserProductPart')->findBy(
                array('fluxUserId' => $artoFluxPart->getId()));
            
            $artoFluxProjects[] = $projects;
            $artoFluxProducts[] = $products;
        }
        
        $acvUsers = $em->getRepository('ArtoAcvBundle:User')->findBy(
                array('greenUser' => $userId));
        
        
        return $this->redirect($this->generateUrl('acv_gestion_artoFlux', array('user' => $user, 'acvUsers' => $acvUsers, 'artoFluxParts' => $artoFluxParts, 'artoFluxProjects' => $artoFluxProjects, 'artoFluxProducts' => $artoFluxProducts)));
    }
    
    /**
     * 
     * @return type
     */
    public function gestionAcvUserSaveAction(){
        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        $user = $this->get('security.context')->getToken()->getUser();
        
        $userId = $user->getId();
        
        $acvUserParts = $request->get('acvUser');
        
        foreach($acvUserParts as $acvUserPart){
            // We check if the label of the part is not empty
            if(isset($acvUserPart['firstname']) && $acvUserPart['firstname'] != ''){
                // Try Retreiving the object if the request id exist
                if (isset($acvUserPart['id']) && $acvUserPart['id'] != null) {
                    $p = $em->getRepository('ArtoAcvBundle:User')->find($acvUserPart['id']);
                }else{
                    $p = new \Arto\AcvBundle\Entity\User();
                }
                
                $p->setFirstname($acvUserPart['firstname']);
                $p->setLastname($acvUserPart['lastname']);
                $p->setCompany($acvUserPart['company']);
                $p->setEmail($acvUserPart['email']);
                $p->setTel($acvUserPart['tel']);
                $p->setRole($acvUserPart['role']);
                $p->setFournisseur($acvUserPart['fournisseur']);
                $p->setGreenUser($user);
                
                $em->persist($p);
            }
        }
        $em->flush();
        
        $artoFluxParts = $em->getRepository('ArtoGreenBundle:User')->findBy(
                array('acvUserId' => $userId));
        
        $artoFluxProjects = array();
        $artoFluxProducts = array();
        
        foreach($artoFluxParts as $artoFluxPart){
            $projects = $em->getRepository('ArtoFluxBundle:Project')->findBy(
                array('greenUser' => $artoFluxPart->getId()));
            
            $products = $em->getRepository('ArtoAcvBundle:UserProductPart')->findBy(
                array('fluxUserId' => $artoFluxPart->getId()));
            
            $artoFluxProjects[] = $projects;
            $artoFluxProducts[] = $products;
        }
        
        $acvUsers = $em->getRepository('ArtoAcvBundle:User')->findBy(
                array('greenUser' => $userId));
        
        return $this->render('ArtoAcvBundle:Default:gestionArtoFlux.html.twig', array('user' => $user, 'acvUsers' => $acvUsers, 'artoFluxParts' => $artoFluxParts, 'artoFluxProjects' => $artoFluxProjects, 'artoFluxProducts' => $artoFluxProducts));
    }
    
    /**
     * 
     */
    public function gestionAcvUserDeleteAction(){
        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        $user = $this->get('security.context')->getToken()->getUser();
        
        $userId = $user->getId();
        
        $acvUserId = $request->get('id');
        
        $acvUser = $em->getRepository('ArtoAcvBundle:User')->findOneBy(
                array('id' => $acvUserId));
        
        $em->remove($acvUser);
        $em->flush();
        
        $user = $em->getRepository('ArtoGreenBundle:User')->findOneBy(
                array('id' => $userId));
        
        $artoFluxParts = $em->getRepository('ArtoGreenBundle:User')->findBy(
                array('acvUserId' => $userId));
        
        $artoFluxProjects = array();
        $artoFluxProducts = array();
        
        foreach($artoFluxParts as $artoFluxPart){
            $projects = $em->getRepository('ArtoFluxBundle:Project')->findBy(
                array('greenUser' => $artoFluxPart->getId()));
            
            $products = $em->getRepository('ArtoAcvBundle:UserProductPart')->findBy(
                array('fluxUserId' => $artoFluxPart->getId()));
            
            $artoFluxProjects[] = $projects;
            $artoFluxProducts[] = $products;
        }
        
        $acvUsers = $em->getRepository('ArtoAcvBundle:User')->findBy(
                array('greenUser' => $userId));
        
        
        return $this->redirect($this->generateUrl('acv_gestion_artoFlux', array('user' => $user, 'acvUsers' => $acvUsers, 'artoFluxParts' => $artoFluxParts, 'artoFluxProjects' => $artoFluxProjects, 'artoFluxProducts' => $artoFluxProducts)));
        
    }
    
    public function downloadImportExcelFileAction($id = null){ 
        $fichier = 'template_import_ArtoAcv.xlsm';
        $path = __DIR__.'/../../../../web/';
        
        $response = new Response();
        $response->setStatusCode(200);
        $response->setContent(file_get_contents($path.$fichier));
        // modification du content-type pour forcer le téléchargement (sinon le navigateur internet essaie d'afficher le document)
        $response->headers->set('Content-Type', 'application/force-download'); 
        $response->headers->set('Content-disposition', 'filename='. $fichier);
 
        // prints the HTTP headers followed by the content
        $response->send();
        
        return $response;
    }
    
    /**
     * Fonction d'import Excel d'ArtoAcv
     */
    public function importExcelAction(){
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        $projectId = $request->get('choixProjet');
        
        $project = $em->getRepository('ArtoAcvBundle:Project')->find($projectId);
        
        $product = $em->getRepository('ArtoAcvBundle:Product')->findOneBy(
                array('project' => $project->getId()));
        
        $productId = $product->getId();
        
        // Pour enlever les niveaux 1 sans rien, quand on importe dans un projet qu'on vient de créer
        $assemblies = $em->getRepository('ArtoAcvBundle:Assembly')->findBy(array('product' => $product->getId()));
        
        foreach($assemblies as $assembly){
            //Si pas de nom
            if($assembly->getName() == null){
                $parts = $assembly->getParts();
                $nbParts = count($parts);
                //Si aucun niveau 2 non plus
                if($nbParts == 0){
                    $em->remove($assembly);
                    $em->flush();
                }
            }
        }
        
        $objPHPExcel = new \PHPExcel();
 
        $post = $_FILES['excel'];
        $fichier = basename($_FILES['excel']['name']);
        move_uploaded_file($post['tmp_name'], __DIR__.'/../../../../web/uploads/acv/'.$post['name']);
            
        $objPHPExcel = \PHPExcel_IOFactory::load(__DIR__.'/../../../../web/uploads/acv/'.$fichier);
        
        $objWorkSheet = $objPHPExcel->getSheetByName('Product Modeling');
        
        if($objWorkSheet->getCell('B5')->getValue() == 1){
            $objWorkSheet->getColumnDimension('K')->setVisible(true);
            $cell = 'B5';
            
            for($i=5;$i <= 1000; $i++){
                $cell = 'B'.$i;
                $cellValue = $objWorkSheet->getCell($cell)->getValue();
                if($cellValue == ''){
                    break;
                }
            }
            
            //lastCell, dernière cellule
            $lastCellLineNumber = $i;
            
            //Niveaux 1
            for($i=5;$i <= $lastCellLineNumber; $i++){
                $cellLvl = 'B'.$i;
                $cellName = 'E'.$i;
                $cellQty = 'G'.$i;
                $cellLvlValue = $objWorkSheet->getCell($cellLvl)->getValue();
                $cellNameValue = $objWorkSheet->getCell($cellName)->getValue();
                $cellQtyValue = $objWorkSheet->getCell($cellQty)->getValue();
                if($cellLvlValue == '1'){
                    $assembly = new Assembly();
                    $assembly->setName($cellNameValue);
                    $assembly->setQuantity($cellQtyValue);
                    $assembly->setProduct($product);
                    $em->persist($assembly);
                    $em->flush();
                }
            }
            
            $lvl1 = false;
            //Niveaux 2
            for($i=5;$i <= $lastCellLineNumber; $i++){
                $cellLvl = 'B'.$i;
                $cellName = 'E'.$i;
                $cellQty = 'G'.$i;
                $cellType = 'F'.$i;
                $cellLvlValue = $objWorkSheet->getCell($cellLvl)->getValue();
                $cellNameValue = $objWorkSheet->getCell($cellName)->getValue();
                $cellQtyValue = $objWorkSheet->getCell($cellQty)->getValue();
                $cellTypeValue = $objWorkSheet->getCell($cellType)->getValue();
                
                //Si niveau 1, lvl1 passe à true
                if($cellLvlValue == '1'){
                    $lvl1 = true;
                    $lvl1Assembly = $em->getRepository('ArtoAcvBundle:Assembly')->findOneBy(
                            array('name' => $cellNameValue, 'product' => $product->getId()));
                }else{
                    if($lvl1 == true){
                        if($cellLvlValue == '1'){
                            $lvl1 = false;
                        }else{
                            if($cellLvlValue == '2'){
                                $acvPart = new Part();
                                $acvPart->setName($cellNameValue);
                                $acvPart->setQuantity($cellQtyValue);
                                $acvPart->setAssembly($lvl1Assembly);
                                $em->persist($acvPart);
                                $em->flush();
                            }
                        }
                    }
                }   
            }
            
            $lvl2 = false;
            
            $lastMateriauPartMaterial = null;
            $lastMateriauPartParam1 = null;
            $lastMateriauPartParent = null;
            
            //Niveaux 3
            for($i=5;$i <= $lastCellLineNumber; $i++){
                $cellLvl = 'B'.$i;
                $cellName = 'E'.$i;
                $cellType = 'F'.$i;
                $cellParam1 = 'G'.$i;
                $cellParam2 = 'I'.$i;
                $cellCode = 'K'.$i;
                $cellLvlValue = $objWorkSheet->getCell($cellLvl)->getValue();
                $cellNameValue = $objWorkSheet->getCell($cellName)->getValue();
                $cellQtyValue = $objWorkSheet->getCell($cellQty)->getValue();
               
                $cellParam1Value = $objWorkSheet->getCell($cellParam1)->getValue();
                $cellParam2Value = $objWorkSheet->getCell($cellParam2)->getOldCalculatedValue();
                $cellCodeValue = $objWorkSheet->getCell($cellCode)->getOldCalculatedValue();
                
                if($cellLvlValue == '1'){
                    $lvl1Assembly = $em->getRepository('ArtoAcvBundle:Assembly')->findOneBy(
                            array('name' => $cellNameValue, 'product' => $product->getId()));
                }
                
                //Si niveau 2, lvl2 passe à true
                if($cellLvlValue == '2'){
                    $lvl2 = true;
                    $lvl2Part = $em->getRepository('ArtoAcvBundle:Part')->findOneBy(
                            array('name' => $cellNameValue, 'assembly' => $lvl1Assembly->getId()));
                }else{
                    if($lvl2 == true){
                        if($cellLvlValue == '2'){
                            $lvl2 = false;
                        }else{
                            if($cellLvlValue == '3'){
                                $familyTransport = $em->getRepository('ArtoAcvBundle:Family')->findOneBy(
                                        array('name' => 'transport'));
                                
                                $cellTypeValue = $objWorkSheet->getCell($cellType)->getOldCalculatedValue();
                                
                                $acvPart = new Part();
                                
                                $base = $em->getRepository('ArtoAcvBundle:Base')->findOneBy(
                                        array('code' => $cellCodeValue));
                                
                                if($cellTypeValue == 'M' || $cellTypeValue == 'C'){
                                    $acvPart = new Part();
                                    $lastMateriauPartMaterial = $base;
                                    $acvPart->setMaterial($base);
                                    $lastMateriauPartParam1 = $cellParam1Value;
                                    $acvPart->setParam1($cellParam1Value);
                                    
                                    if($cellParam2Value != 0){
                                        $acvPart->setParam2($cellParam2Value);
                                    }
                                    
                                    $lastMateriauPartParent = $lvl2Part;
                                    $acvPart->setParent($lvl2Part);
                                    $acvPart->setAssembly($lvl1Assembly);
                                }
                                
                                if($cellTypeValue == 'P'){
                                    $acvPart = $em->getRepository('ArtoAcvBundle:Part')->findOneBy(
                                            array('material' => $lastMateriauPartMaterial->getId(), 'param1' => $lastMateriauPartParam1, 'parent' => $lastMateriauPartParent->getId()));
                                    //Si transport
                                    if($base->getFamily() == $familyTransport){
                                        $acvPart->setTransport($base);
                                        $acvPart->setDistance($cellParam1Value);
                                    //Sinon    
                                    }else{
                                        $acvPart->setProcess($base);
                                        $acvPart->setParam3($cellParam1Value);
                                        if($cellParam2 != ''){
                                            $acvPart->setParam4($cellParam2Value);
                                        }
                                        $acvPart->setParent($lvl2Part);
                                    } 
                                    
                                    $acvPart->setAssembly($lvl1Assembly);
                                }
                                
                                $em->persist($acvPart);
                                $em->flush();
                            }
                        }
                    }
                }   
            }
            
            $user = $this->get('security.context')->getToken()->getUser();
            $userId = $user->getId();

            $projects = $em->getRepository('ArtoAcvBundle:Project')->findAllActive($user);

            //projectsWithoutFolder
            $projectsWF = $em->getRepository('ArtoAcvBundle:Project')->findBy(
                    array('greenUser' => $userId, 'folder' => null, 'parent' => null));

            $folders = $em->getRepository('ArtoAcvBundle:Folder')->findBy(
                    array('user' => $userId));
            
            $templateImportExcelUrl = __DIR__.'/../../../../web/template_import_ArtoAcv.xlsm';

            return $this->redirect($this->generateUrl('acv_parts', array('project' => $projectId, 'id' => $productId)));
        }else{
            return $this->render('ArtoAcvBundle:Default:index.html.twig', array('projects' => $projects, 'user' => $userId, 'folders' => $folders,'projectsWF'=> $projectsWF,'myLetter' => '', 'templateUrl' => $templateImportExcelUrl));
        } 
    }
    
    /**
     * Remplit l'aide pour les plastiques
     * @return type
     */
    public function fillPlasticWithHelpAction(){
        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        
        $totalWeightPlastic = $request->get('totalWeightPlastic');
        $projectName = $request->get('project');
        
        $project = $em->getRepository('ArtoAcvBundle:Project')->find($projectName);
        
        $products = $em->getRepository('ArtoAcvBundle:Product')->findBy(array('project' => $project->getId()));
        $product = $products[0];
        
        return $this->render('ArtoAcvBundle:Default:instructions.html.twig', array('totalWeightPlastic' => $totalWeightPlastic, 'project' => $project, 'product' => $product));
    }
    
    /**
     * Liste les aides concernant les matériaux  plastiques
     * @return type les aides concernant les matériaux plastiques
     */
    public function listPlasticHelpAction(){
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();
        
        $key1 = '';
        $key2 = '';
        
        $type = $request->get('type');
        
        //Plastic
        //Normal plastic
        $normalPlasticFamily = $em->getRepository('ArtoAcvBundle:Family')->findOneBy(
                array('name' => 'plastique'));
        $normalPlasticId = $normalPlasticFamily->getId();
        
        $materialsType = $em->getRepository('ArtoAcvBundle:Type')->findOneBy(
                array('name' => 'materiau'));
        $materialsId = $materialsType->getId();

        if($type == '1'){
            $key1 = '%ignifuge%';
            $key2 = '%fibre de verre%';
            $key3 = '%FV%';
            
            $sql = "SELECT c FROM ArtoAcvBundle:Base c WHERE c.type=:type AND c.family=:normalPlasticId AND c.isVisible=:answerId AND (c.name LIKE :key1 OR c.name LIKE :key2 OR c.name LIKE :key3) ORDER BY c.name";
            $query = $em->createQuery($sql)->setParameters(
                array('type' => $materialsId, 'normalPlasticId' => $normalPlasticId, 'answerId' => 'oui', 'key1' => $key1, 'key2' => $key2, 'key3' => $key3));
            
            $bases = $query->getResult();
        }elseif($type == '2'){
            $key1 = '%ignifuge%';
            $key2 = '%fibre de verre%';
            $key3 = '%FV%';
            
            $sql = "SELECT c FROM ArtoAcvBundle:Base c WHERE c.type=:type AND c.family=:normalPlasticId AND c.isVisible=:answerId AND c.name NOT LIKE :key1 AND c.name NOT LIKE :key2 AND c.name NOT LIKE :key3 ORDER BY c.name";
            $query = $em->createQuery($sql)->setParameters(
                array('type' => $materialsId, 'normalPlasticId' => $normalPlasticId, 'answerId' => 'oui', 'key1' => $key1, 'key2' => $key2, 'key3' => $key3));
            
            $bases = $query->getResult();
        }
        

        return $this->render('ArtoAcvBundle:Default:base.html.twig', array('bases' => $bases));
    }
}
