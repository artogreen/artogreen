<?php

namespace Arto\AcvBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Arto\GreenBundle\Controller\TokenAuthenticatedController;
use n3b\Bundle\Util\HttpFoundation\StreamResponse\StreamResponse,
    n3b\Bundle\Util\HttpFoundation\StreamResponse\FileWriter,
    n3b\Bundle\Util\HttpFoundation\StreamResponse\StreamWriterWrapper;


class ExportController extends Controller implements TokenAuthenticatedController
{
    /*
    public function indexAction($project, $id)
    {
        ini_set('memory_limit','6000M');

        $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($id);

        $excelService = $this->get('xls.service_xls2007');

        $file = $this->get('kernel')->getRootDir() . '/../file/test.xlsx';

        $xl_obj=$this->get('xls.load_xls2007');
        $my_xl_file = $xl_obj->load($file);

        //$obj = $this->get('xls.load_xls2007')->load($file)

        $excelService->setExcelObj($my_xl_file);

        $excelService->excelObj->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Hello')
                    ->setCellValue('B2', 'world!');


        $response = $excelService->getResponse();
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename=export.xlsx');

        // If you are using a https connection, you have to set those two headers for compatibility with IE <9
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        return $response;
    }
    */
    
    /**
     * Pour appeler le service Calculator
     * @return une instance de Calculator
     */
    private function getCalculator()
    {
        return $this->get('arto.acv.calculator');
    }
    
    /**
     * Crée un fichier Excel d'export d'un projet ArtoAcv
     * @param type $project le projet
     * @param type $id l'id du produit
     * @return Le fichier Excel d'export du projet
     */
    public function indexAction($project, $id)
    {
        $request = $this->getRequest();

        $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($id);

        $file = $this->get('kernel')->getRootDir() . '/../file/export.xlsx';


        $reader = \PHPExcel_IOFactory::createReader( 'Excel2007' );
        $excel = $reader->load( $file );

        $sheet = $excel->setActiveSheetIndex(0);

        // header
        $sheet->setCellValue('B5', $project->getRef())
              ->setCellValue('B6', $project->getName())
              ->setCellValue('H5', $project->getGreenUser()->getCompany())
              ->setCellValue('H6', $project->getPilote());

        if ($project->getPiloteDate() != null) {
            $sheet->setCellValue('H7', $project->getPiloteDate()->format('d/m/Y'));
        }

        ///////////////////////////////////////////////////////////////////////
        // project description
        ///////////////////////////////////////////////////////////////////////
        $sheet->setCellValue('B13', $project->getDescription())
              ->setCellValue('B15', $project->getCity())
        ;


        ///////////////////////////////////////////////////////////////////////
        // fu description
        ///////////////////////////////////////////////////////////////////////
        $sheet->setCellValue('B22', $project->getDescriptionuf());
        $cell = '26';
        $total = 0;
        foreach($product->getParts() as $part) {
            $sheet->setCellValue('A'.$cell, $part->getType());
            $sheet->setCellValue('B'.$cell, $part->getRef());
            $sheet->setCellValue('C'.$cell, $part->getName());
            $sheet->setCellValue('E'.$cell, $part->getQuantity());
            $sheet->setCellValue('G'.$cell, $part->getWeight());
            $subtotal = $part->getQuantity() * $part->getWeight();
            $sheet->setCellValue('H'.$cell, $subtotal);
            $total += $subtotal;
            $cell++;
        }
        $sheet->setCellValue('H46', $total);


        ///////////////////////////////////////////////////////////////////////
        // manufacturing
        ///////////////////////////////////////////////////////////////////////
        $sheet->setCellValue('B52', $total);
        $sheet->setCellValue('B54', $product->getWeightSet());
        $sheet->setCellValue('B56', round($product->getWeightPercentageLeft()).' %');


        ///////////////////////////////////////////////////////////////////////
        // distribution
        ///////////////////////////////////////////////////////////////////////
        $cell = '64';
        if ($product->getDelivery() != null && $product->getDelivery()->getDetailed() == 1) {
            foreach($product->getDelivery()->getParts() as $part) {
                $sheet->setCellValue('A'.$cell, $part->getTransport()->getName());
                $sheet->setCellValue('H'.$cell, $part->getDistance());
                $cell++;
            }
        }
        if ($product->getDelivery() != null && $product->getDelivery()->getDetailed() == 2) {
            $mode = $product->getDelivery()->getMode();
            if ($mode == 1) {
                $sheet->setCellValue('A64', 'Bateau');
                $sheet->setCellValue('H64', 19000);
                $sheet->setCellValue('A65', 'Camion');
                $sheet->setCellValue('H65', 1000);
            }
            if ($mode == 2) {
                $sheet->setCellValue('A64', 'Camion');
                $sheet->setCellValue('H64', 3500);
            }
            if ($mode == 3) {
                $sheet->setCellValue('A64', 'Camion');
                $sheet->setCellValue('H64', 1000);
            }
        }

        if ($project->getTransportAppro() == 0) {
            $sheet->setCellValue('B71', 'Transports détaillés');
        }
        if ($project->getTransportAppro() == 1) {
            $sheet->setCellValue('B71', 'Transport global => Mondial : 19000km (bateau) + 10000 (camion)');
        }
        if ($project->getTransportAppro() == 2) {
            $sheet->setCellValue('B71', 'Transport global => Intercontinental : 3500 km (camion)');
        }
        if ($project->getTransportAppro() == 3) {
            $sheet->setCellValue('B71', 'Transport global => Local: 1000km (camion)');
        }
        if ($project->getTransportStock() != null) {
            $sheet->setCellValue('A75', $project->getTransportStock()->getName());
            $sheet->setCellValue('H75', $project->getTransportStockKm());
        }

        ///////////////////////////////////////////////////////////////////////
        // use
        ///////////////////////////////////////////////////////////////////////
        $usage = $product->getUsage();
        if ($usage != null) {
            $sheet->setCellValue('B82', $usage->getLifetime());

            if ($usage->getEnergy() != null) {
                $sheet->setCellValue('B84', $usage->getEnergy()->getName());
            }

            $sheet->setCellValue('B87', $usage->getOnKw());
            $sheet->setCellValue('C87', $usage->getOnPercent());
            $sheet->setCellValue('B88', $usage->getStandbyActiveKw());
            $sheet->setCellValue('C88', $usage->getStandbyActivePercent());
            $sheet->setCellValue('B89', $usage->getStandbyPassiveKw());
            $sheet->setCellValue('C89', $usage->getStandbyPassivePercent());
            $sheet->setCellValue('B90', $usage->getOffKw());
            $sheet->setCellValue('C90', $usage->getOffPercent());

            $cell = '107';
            foreach($usage->getParts() as $part) {
                if ($part->getCategory() == 1) {
                    $sheet->setCellValue('A'.$cell, $part->getBase()->getName());
                    $sheet->setCellValue('B'.$cell, $part->getBase()->getModuleCode());
                    $sheet->setCellValue('C'.$cell, $part->getVal() * $part->getQuantity());
                    $sheet->setCellValue('D'.$cell, $part->getBase()->getUnit1());
                    $sheet->setCellValue('F'.$cell, $part->getBase()->getUnit1());
                    if ($request->get('artoadmin') == 1) {
                        $sheet->setCellValue('G'.$cell, $part->getBase()->getCode());
                    }
                    $cell++;
                }
            }

            $cell = '95';
            foreach($usage->getParts() as $part) {
                if ($part->getCategory() == 2) {
                    $sheet->setCellValue('A'.$cell, $part->getBase()->getName());
                    $sheet->setCellValue('B'.$cell, $part->getBase()->getModuleCode());
                    $sheet->setCellValue('C'.$cell, $part->getVal() * $part->getQuantity());
                    $sheet->setCellValue('D'.$cell, $part->getBase()->getUnit1());
                    $sheet->setCellValue('F'.$cell, $part->getBase()->getUnit1());
                    if ($request->get('artoadmin') == 1) {
                        $sheet->setCellValue('G'.$cell, $part->getBase()->getCode());
                    }
                    $cell++;
                }

            }
        }

        ///////////////////////////////////////////////////////////////////////
        // eol
        ///////////////////////////////////////////////////////////////////////
        $eol = $product->getEol();
        if ($eol != null) {
            if ($eol->getType() == 1) {
                $sheet->setCellValue('B122', 'Selon méthode des 1000 km par camion (CODDE-0324)');
                $sheet->setCellValue('B124', '1000');
            }
            if ($eol->getType() == 2) {
                $sheet->setCellValue('B122', 'Selon étude ECO\'DEEE');
                $sheet->setCellValue('B124', '0');
            }
            if ($eol->getType() == 3) {
                $sheet->setCellValue('B122', 'Mise en décharge (ELCD-0112) / Valorisation (ELCD-0264)');
                $sheet->setCellValue('B124', '0');
            }
        }




        ///////////////////////////////////////////////////////////////////////
        // parts
        ///////////////////////////////////////////////////////////////////////
        $sheet = $excel->setActiveSheetIndex(1);
        $cell = 10;

        $sheet->setCellValue('B5', $project->getRef())
              ->setCellValue('B6', $project->getName());

        $sheet->setCellValue('G5', $total);
        $sheet->setCellValue('G6', $product->getWeightSet());

        $assemblies = $em->getRepository('ArtoAcvBundle:Assembly')->findBy(array('product' => $product->getId()));
        if (count($assemblies) > 0) {
            foreach($assemblies as $assembly) {

                $cell++;
                $sheet->setCellValue('A'.$cell, 1);
                $sheet->setCellValue('B'.$cell, '...1');
                $sheet->setCellValue('C'.$cell, $assembly->getName());
                $sheet->setCellValue('D'.$cell, 'CX');
                $sheet->setCellValue('E'.$cell, $assembly->getQuantity());
                $sheet->setCellValue('F'.$cell, 'UN');

                foreach($assembly->getParts() as $part) {


                    if ($part->getParent() == null){
                        $cell++;
                        // get quantity
                        $quantity = $part->getQuantity();
                        
                        $sheet->setCellValue('A'.$cell, 2);
                        $sheet->setCellValue('B'.$cell, '...2');
                        
                        $name = $part->getName();
                        
                        $sheet->setCellValue('C'.$cell, $name);
                        $sheet->setCellValue('D'.$cell, 'PT');
                        $sheet->setCellValue('E'.$cell, $quantity);
                        $sheet->setCellValue('F'.$cell, 'UN');
                        $sheet->setCellValue('G'.$cell, '');
                        $sheet->setCellValue('H'.$cell, '');
                    }
                    
                    if ($part->getParent() != null) {
                        $child = $part;

                        // get base
                        if ($child->getMaterial() != null) {
                            $cell++;
                            // get quantity
                            $quantity = $part->getParent()->getQuantity();

                            $sheet->setCellValue('A'.$cell, 3);
                            $sheet->setCellValue('B'.$cell, '......3');

                            $base = $child->getMaterial();
                            $val1 = $part->getParam1();
                            
                            if($base->getUnit1() == 'g'){
                                $val2 = '';
                            }else if($base->getUnit1() == 'UN'){
                                if($base->getUnit2() == 'g'){
                                    $val2 = $base->getMultiplicateur();
                                }else if($base->getUnit2() == 'kg'){
                                    $val2 = $base->getMultiplicateur()/1000;
                                }else if($base->getUnit2() == ''){
                                    $val2 = '';
                                }else{
                                    $val2 = $base->getValParam2ByUN();
                                }
                            }else{
                                if($base->getUnit2() == ''){
                                    $val2 = '';
                                }else{
                                    $val2 = $base->getValParam2ByUN();
                                }
                            }
                            
                            $sheet->setCellValue('C'.$cell, $base->getName());
                            $sheet->setCellValue('D'.$cell, $base->getModuleCode());
                            $sheet->setCellValue('E'.$cell, $val1);
                            $sheet->setCellValue('F'.$cell, $base->getUnit1());
                            $sheet->setCellValue('G'.$cell, $val2);
                            $sheet->setCellValue('H'.$cell, $base->getUnit2());
                            if ($request->get('artoadmin') == 1) {
                                $sheet->setCellValue('I'.$cell, $base->getCode());
                            }
                        }

                        if ($child->getProcess() != null) {
                            $cell++;
                            // get quantity
                            $quantity = $part->getParent()->getQuantity();

                            $sheet->setCellValue('A'.$cell, 3);
                            $sheet->setCellValue('B'.$cell, '......3');

                            $base = $child->getProcess();
                            $val1 = $part->getParam3();
                            
                            if($base->getUnit2() == ''){
                                $val2 = '';
                            }else{
                                $val2 = $base->getValParam2ByUN();
                            }
                           
                            $sheet->setCellValue('C'.$cell, $base->getName());
                            $sheet->setCellValue('D'.$cell, $base->getModuleCode());
                            $sheet->setCellValue('E'.$cell, $val1);
                            $sheet->setCellValue('F'.$cell, $base->getUnit1());
                            $sheet->setCellValue('G'.$cell, $val2);
                            $sheet->setCellValue('H'.$cell, $base->getUnit2());
                            if ($request->get('artoadmin') == 1) {
                                $sheet->setCellValue('I'.$cell, $base->getCode());
                            }
                        }

                        if ($child->getTransport() != null) {
                            $cell++;
                            $base = $child->getTransport();
                            $sheet->setCellValue('A'.$cell, 3);
                            $sheet->setCellValue('B'.$cell, '......3');
                            $sheet->setCellValue('C'.$cell, $base->getName());
                            $sheet->setCellValue('D'.$cell, $base->getModuleCode());
                            $sheet->setCellValue('E'.$cell, $part->getDistance());
                            $sheet->setCellValue('F'.$cell, $base->getUnit1());
                            $sheet->setCellValue('G'.$cell, '');
                            $sheet->setCellValue('H'.$cell, $base->getUnit2());
                            if ($request->get('artoadmin') == 1) {
                                $sheet->setCellValue('I'.$cell, $base->getCode());
                            }
                        }
                    }
                }

            }
        }


        // Writer
        //$writer = \PHPExcel_IOFactory::createWriter( $excel, 'Excel2007' );
        $writer = new \PHPExcel_Writer_Excel5($excel);

        $filename = 'export_'.$project->getId().'.xls';

        header( 'Content-type: application/vnd.ms-excel; charset=utf-8' );
        header( 'Content-Disposition: attachment; filename='.$filename );

        return $writer->save( 'php://output' );
    }
    
    /**
     * Exporte en PDF un projet ArtoAcv
     * @param type $project le projet
     * @return \Symfony\Component\HttpFoundation\Response Le fichier PDF d'export du projet
     */
    public function exportPDFAction($project)
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();
        $calculator = $this->getCalculator();
        
        $project = $request->get('project');

        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        $product = $project->getFirstProduct();
        
        $projectId = $project->getId();
        $productId = $product->getId();
        
        $filename = 'export_'.$project->getName().'.pdf';
        
        $categories = array('rmd', 'ed', 'wd', 'gw', 'od', 'at', 'poc', 'aa', 'wt', 'we', 'hwp');
        
        // Initialize array to 11 values set to 0
        $m = $calculator->calculateMaterial($product);
        $p = $calculator->calculateProcess($product);
        $a = $calculator->calculateManufacturing($product);
        $d = $calculator->calculateDelivery($product);
        $u = $calculator->calculateUsage($product);
        $energy = $calculator->calculateEnergy($product);
        $c = $calculator->calculateConsommables($product);
        $i = $calculator->calculateInstall($product);
        $e = $calculator->calculateEol($product);
        
        $t = $calculator->calculateTotalsForIndex($m,$p,$a,$d,$u,$energy,$c,$i,$e);
        
        $paramsSticker = $calculator->calculateStickerDatas($project, $product);
        
        $siteArtogreen = $this->getRequest()->getScheme() . '://' . $this->getRequest()->getHttpHost() . $this->getRequest()->getBasePath();
        
        $urlResultGraph = $siteArtogreen."/acv/result/chart1/".$projectId."/".$productId;
        $urlLogoGraph = $siteArtogreen."/images/acv/logo.jpg";
        
        $urlSimulationGraph = $siteArtogreen."/acv/simulation_materials_chart/".$projectId."/".$productId;
        $urlSimulationIndicateursPic = $siteArtogreen."/images/acv/indicateurs.bmp";
        
        //Simulation Materials
        //% négatifs
        $percentages = $calculator->calculatePercentages('-');
        
        //% positifs
        $percentages2 = $calculator->calculatePercentages('+');
        
        $wastes = $calculator->calculateWastes($product);
        $totals = $calculator->calculateTotalsImpacts($product, $wastes);
        $parts = $calculator->calculatePartsForSimulation($product, false, false); 
        
        $comparedProjectId = $project->getComparedProjectId();
        
        $comparedProject = null;
        
        if($comparedProjectId != null && $comparedProjectId != ''){
            $comparedProject = $em->getRepository('ArtoAcvBundle:Project')->find($comparedProjectId);
        }
        
        $urlComparisonGraph = $siteArtogreen."/acv/comparison_chart/".$projectId."/".$comparedProjectId;
        
        $params = array(
            'project'    => $project,
            'product'    => $product,
            'categories' => $categories,
            'materials'  => $m,
            'processes'  => $p,
            'manufacturings'  => $a,
            'deliveries' => $d,
            'usages'     => $u,
            'installs'     => $i,
            'eols'     => $e,
            'totals'     => $t,
            'consommables' => $c,
            'energy' => $energy,
            'urlResultGraph' => $urlResultGraph,
            'urlLogoGraph' => $urlLogoGraph,
            //StickerParams
            'user' => $paramsSticker['user'],
            'co2' => $paramsSticker['co2'],
            'water' => $paramsSticker['water'],
            'power' => $paramsSticker['power'],
            'recycle' => $paramsSticker['recycle'],
            'co2_euros' => $paramsSticker['co2_euros'],
            'water_euros' => $paramsSticker['water_euros'],
            'power_euros' => $paramsSticker['power_euros'],
            'recycle_euros' => $paramsSticker['recycle_euros'],
            'pva' => $paramsSticker['pva'],
            //Simulation materials
            'percentages' => $percentages,
            'percentages2' => $percentages2,
            'parts' => $parts,
            'urlSimulationGraph' => $urlSimulationGraph,
            'urlSimulationIndicateursPic' => $urlSimulationIndicateursPic,
            //Comparaison
            'comparedProject' => $comparedProject,
            'urlComparisonGraph' => $urlComparisonGraph,
            'siteArtogreen' => $siteArtogreen
        );
        
        if ($request->get('pdf') == 1) {
            $html = $this->renderView('ArtoAcvBundle:Default:exportPDF_pdf.html.twig', $params);

            return new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
                200,
                array(
                    'Content-Type'          => 'application/pdf',
                    'Content-Disposition'   => 'attachment; filename='.$filename
                )
            );

        } else {
            return $this->render('ArtoAcvBundle:Default:sticker.html.twig', $params);
        }
    }

}
