<?php

namespace Arto\AcvBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Arto\AcvBundle\Entity\SimulationMaterial;
use Arto\AcvBundle\Entity\Product;
use Arto\AcvBundle\Entity\Assembly;
use Arto\AcvBundle\Entity\Part;
use Arto\AcvBundle\Entity\Project;


class SimulationController extends Controller
{
    /**
     * Pour appeler le service Calculator
     * @return Une instance de Calculator
     */
    private function getCalculator()
    {
        return $this->get('arto.acv.calculator');
    }
    
    /**
     * Pour appeler le service Drawer
     * @return Une instance de Drawer
     */
    private function getDrawer()
    {
        return $this->get('arto.acv.drawer');
    }
    
    /**
     * Affiche les parts de Simulation
     * @param type $project le projet
     * @param type $id l'id du produit
     * @return Les parts de simulation
     */
    public function partsAction($project, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $calculator = $this->getCalculator();
        
        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($id);
        $totalWeight = $product->getWeight();

        $percentages = array();
        for($i=1;$i<=20;$i++) {
            $percentages[] = array(
                'label' => '-'.$i*5,
                'value' => abs(($i*5/100)-1)
            );
        }
        
        //% positifs
        $percentages2 = array();
        for($i=20;$i>=1;$i--) {
            $percentages2[] = array(
                'label' => '+'.$i*5,
                'value' => abs(($i*5/100)+1)
            );
        }
        
        $totals = $calculator->calculateTotalsImpacts($product, false);
        $parts = $calculator->calculatePartsForSimulation($product, true, true);

        return $this->render('ArtoAcvBundle:Default:simulation_parts.html.twig', array(
            'project'     => $project,
            'product'     => $product,
            'percentages' => $percentages,
            'percentages2' => $percentages2,
            'parts'       => $parts,
            'weight'      => $totalWeight,
            'totals'      => $totals  
        ));
    }
    
    /**
     * Sauvegarde les parts de simulation
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function partsSaveAction()
    {
        $request = $this->getRequest();
        $part = $request->get('part');
        $field = $request->get('field');
        $val = $request->get('value');

        if ($val == '') $val = null;

        $new_val = '';

        if ($part != null && $field != null) {
            $em = $this->getDoctrine()->getEntityManager();
            $part = $em->getRepository('ArtoAcvBundle:Part')->find($part);
            
            //Si parent null c'est que la part a des enfants
            if($part->getParent() == null){
                $method = 'set'.ucfirst($field);
                $part->$method($val);
                
                $em->persist($part);
                $em->flush();
                
                $childs = $part->getChilds();
                foreach($childs as $child){
                     $method = 'set'.ucfirst($field);
                     $child->$method($val);
                     
                     $em->persist($child);
                     $em->flush();
                }
            }else{
                $method = 'set'.ucfirst($field);
                $part->$method($val);

                $em->persist($part);
                $em->flush();
                
            }
            
            $new_val = $part->getChildsWeight() * $val;
        }

        return new Response($new_val);
    }
    
    /**
     * Remet à zéro les parts de Simulation
     * @return \Symfony\Component\HttpFoundation\Response 
     */
    public function partsRazAction(){
         $request = $this->getRequest();
         $em = $this->getDoctrine()->getEntityManager();
         
         $productId = $request->get('product');
         $product = $em->getRepository('ArtoAcvBundle:Product')->find($productId);
         
         $assemblies = $em->getRepository('ArtoAcvBundle:Assembly')->findBy(
                 array('product' => $productId));
         
         foreach($assemblies as $assembly){
             $assemblyId = $assembly->getId();
             $parts = $em->getRepository('ArtoAcvBundle:Part')->findBy(
                 array('assembly' => $assemblyId));
             foreach($parts as $part){
                 $part->setPercentage1("1.00");
                 $part->setPercentage2("1.00");
                 $part->setPercentage3("1.00");
                 $em->persist($part);
             }
         }
         
         $em->flush();
         
         return new Response();      
    }

    /**
     * Sauvegarde les parts de Simulation des masses matières et composants
     * @return \Symfony\Component\HttpFoundation\Response 
     */
    public function materialsSaveAction()
    {
        $request = $this->getRequest();

        $field = $request->get('field');
        $val = $request->get('value');

        if ($val == '') $val = null;

        if ($field != null) {
            $em = $this->getDoctrine()->getEntityManager();

            $product = $em->getRepository('ArtoAcvBundle:Product')->find($request->get('product'));
            $material = $em->getRepository('ArtoAcvBundle:Base')->find($request->get('material'));

            $simulation = $em->getRepository('ArtoAcvBundle:SimulationMaterial')->findOneBy(array(
                'product'  => $product->getId(),
                'material' => $material->getId()
            ));

            if ($simulation == null) {
                $simulation = new SimulationMaterial();
                $simulation->setProduct($product);
            }

            $simulation->setMaterial($material);

            $method = 'set'.ucfirst($field);
            $simulation->$method($val);

            $em->persist($simulation);

            if ($simulation->getPercentage1() == null
                && $simulation->getPercentage2() == null
                && $simulation->getPercentage3() == null) {

                $em->remove($simulation);
            }

            $em->flush();
        }
        
       return new Response();
    }
    
    /**
     * Remet à zéro les parts de Simulation des masses matières et composants
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function materialsRazAction(){
         $request = $this->getRequest();
         $em = $this->getDoctrine()->getEntityManager();
         
         $productId = $request->get('product');
         $product = $em->getRepository('ArtoAcvBundle:Product')->find($productId);
         
         $materials = $em->getRepository('ArtoAcvBundle:Base')->findAll();
         
         foreach($materials as $material){
              $simulation = $em->getRepository('ArtoAcvBundle:SimulationMaterial')->findOneBy(array(
                'product'  => $product->getId(),
                'material' => $material->getId()
            ));
              
            if($simulation != null){
                $simulation->setPercentage1(1.00);
                $simulation->setPercentage2(1.00);
                $simulation->setPercentage3(1.00);
                $em->persist($simulation);
            }  
         }
         
         $em->flush();
         
         return new Response();     
    }
    
    /**
     * Met à zéro le % de variation d'une part de Simulation des masses matières et composants
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function materialsEraseAction(){
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();
        
        $productId = $request->get('product');
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($productId);
        
        $materialId = $request->get('material');
        $material = $em->getRepository('ArtoAcvBundle:Base')->find($materialId);
        
        $simulation = $em->getRepository('ArtoAcvBundle:SimulationMaterial')->findOneBy(array(
                'product'  => $product->getId(),
                'material' => $material->getId()   
        ));
        
        if($simulation != null){
                $simulation->setPercentage1(0.00);
                $em->persist($simulation);
        }
        
        $em->flush();
        
        return new Response(); 
    }
    
    /**
     * Annule la remise à zéro du % de variation d'une part de Simulation des masses matières et composants
     * @return \Symfony\Component\HttpFoundation\Response
     */
     public function materialsCancelEraseAction(){
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();
        
        $productId = $request->get('product');
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($productId);
        
        $materialId = $request->get('material');
        $material = $em->getRepository('ArtoAcvBundle:Base')->find($materialId);
        
        $simulation = $em->getRepository('ArtoAcvBundle:SimulationMaterial')->findOneBy(array(
                'product'  => $product->getId(),
                'material' => $material->getId()   
        ));
        
        if($simulation != null){
                $simulation->setPercentage1(1.00);
                $em->persist($simulation);
        }  
        
        $em->flush();
        
        return new Response(); 
    }
    
    /**
     * Affiche le radar de Simulation des masses des articles
     * @param type $project le projet
     * @param type $id l'id du produit
     * @return \Symfony\Component\HttpFoundation\Response Le radar de Simulation
     */
    public function simulationPartsChartAction($project, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $drawer = $this->getDrawer();

        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($id);

        $response = new Response();
        $response->headers->set('Content-Type', 'image/jpeg');

        $simulationChart = $drawer->drawSimulationChart($project,$product,'parts');
        $response->setContent($simulationChart);

        return $response;

    }
    
    /**
     * Affiche la page simulation des masses matières et composants
     * @param type $project le projet
     * @param type $id l'id du produit
     * @return La page simulation des masses matières et composants
     */
    public function simulationMaterialsAction($project, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $calculator = $this->getCalculator();
        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($id);
        
        //% négatifs
        $percentages = $calculator->calculatePercentages('-');
        
        //% positifs
        $percentages2 = $calculator->calculatePercentages('+');
        
        $totals = $calculator->calculateTotalsImpacts($product, false);
        $parts = $calculator->calculatePartsForSimulation($product, false, false);
        
        foreach ($product->getAssemblies() as $assembly) {
            foreach ($assembly->getParts() as $part) {
                $parent = $part->getParent();
                $material = $part->getMaterial();
                if ($parent != null && $material != null) {
                    $simulation = $em->getRepository('ArtoAcvBundle:SimulationMaterial')->findBy(array(
                        'product' => $product->getId(),
                        'material' => $material->getId()
                    ));
                    
                    if($simulation == null){
                        $newSimulation = new \Arto\AcvBundle\Entity\SimulationMaterial();
                        $newSimulation->setMaterial($part->getMaterial());
                        $newSimulation->setProduct($product);
                        $em->persist($newSimulation);
                    }
                }
            }
        }

        $em->flush();
            
        return $this->render('ArtoAcvBundle:Default:simulation_materials.html.twig', array(
            'project'     => $project,
            'product'     => $product,
            'percentages' => $percentages,
            'percentages2' => $percentages2,
            'parts'       => $parts,
            'totals'      => $totals 
        ));
    }
    
    /**
     * Affiche le radar de Simulation des masses matières et composants
     * @param type $project le projet
     * @param type $id l'id du produit
     * @return \Symfony\Component\HttpFoundation\Response Le radar de Simulation des masses matières et composants
     */
    public function simulationMaterialsChartAction($project, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $drawer = $this->getDrawer();

        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($id);

        $response = new Response();
        $response->headers->set('Content-Type', 'image/jpeg');
        
        $simulationChart = $drawer->drawSimulationChart($project,$product,'materials');
        $response->setContent($simulationChart);

        return $response;
    }
    
    /**
     * Affiche les pistes de réduction d'impact
     * @param type $project le projet
     * @param type $id l'id du produit
     * @return Les pistes de réduction d'impact
     */
    public function simulationPistesAction($project, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $calculator = $this->getCalculator();
        $project = $em->getRepository('ArtoAcvBundle:Project')->find($project);
        $product = $em->getRepository('ArtoAcvBundle:Product')->find($id);

        //% négatifs
        $percentages = $calculator->calculatePercentages('-');
        
        $totals = $calculator->calculateTotalsImpacts($product, true);
        $parts = $calculator->calculatePartsForSimulation($product, true, false); 
        
        return $this->render('ArtoAcvBundle:Default:simulation_pistes.html.twig', array(
            'project'     => $project,
            'product'     => $product,
            'percentages' => $percentages,
            'parts'       => $parts,
            'totals'      => $totals  
        ));
    }
}
