<?php

namespace Arto\AcvBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Arto\AcvBundle\Entity\Base;
use Arto\AcvBundle\Entity\Project;
use Arto\AcvBundle\Entity\Assembly;
use Arto\AcvBundle\Entity\Family;
use Arto\AcvBundle\Entity\Type;

class BaseController extends Controller
{   
    /**
     * Pour trouver toutes les familles de produits
     * @return la liste de toutes les familles de produits
     */
    public function familyAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();

        $bases = $em->getRepository('ArtoAcvBundle:Base')->findBy(
                array('type' => $request->get('type'), 'isVisible' => 'oui'),
                array('name' => 'ASC'));

        $families = array();
        foreach($bases as $base) {
            $family_id = $base->getFamily()->getId();
            
            if (!array_key_exists($family_id, $families)) {
                $families[$family_id] = $base->getFamily()->getName();
            }
        }

        $collator = new \Collator('fr_FR');
        $collator->asort($families);

        return $this->render('ArtoAcvBundle:Default:family.html.twig', array('families' => $families));
    }
    
    /**
     * Pour retourner tous les produits
     * @return la liste de tous les produits selon leur famille et type
     */
    public function listAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();
        
        $family = $request->get('family');
        $type = $request->get('type');

        $bases = $em->getRepository('ArtoAcvBundle:Base')->findBy(
            array('family' => $family,'type' => $type,'isVisible' => 'oui'),
            array('name' => 'ASC')
        );

        return $this->render('ArtoAcvBundle:Default:base.html.twig', array('bases' => $bases));
    }
    
    /**
     * Trouve les caractéristiques d'un produit à partir de son ID
     * @return \Symfony\Component\HttpFoundation\Response Met à jour la page des parts avec les caractéristiques du produit sélectionné
     */
    public function selectAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $id = $this->getRequest()->get('id');

        $base = $em->getRepository('ArtoAcvBundle:Base')->find($id);
        $type = substr($base->getType(), 0, 1);
        $valParam2ByUN = '';
        
        $typeId = $base->getType()->getId();
        $familyId = $base->getFamily()->getId();
        
        $typeName = $base->getType()->getName();
        $familyName = $base->getFamily()->getName();
        $path = $typeName.'/'.$familyName;

        $name = sprintf('<strong class="%s">%s</strong><a href="#" class="select-base" data-toggle="tooltip">%s</a>',
                strtolower($type),
                strtoupper($type),
                $base->getName()
            );
        
        if($base->getValParam2ByUN() != null){
            $valParam2ByUN = $base->getValParam2ByUN();
        }

        $result = array(
            'id' => $base->getId(),
            'name' => $name,
            'type' => $base->getType(),
            'unit1' => $base->getUnit1(),
            'unit2' => $base->getUnit2(),
            'unitmass' => $base->getUnitMass(),
            'multiplicateur' => round($base->getMultiplicateur(), 3),
            'valParam2ByUN' => $valParam2ByUN,
            'description' => $base->getDescription(),
            'typeId' => $typeId,
            'familyId' => $familyId,
            'path' => $path
        );

        $json = json_encode($result);
        $response = new Response($json);
        return $response;
    }
    
    /**
     * Liste les types de procédés
     * @return type La liste des types de procédés
     */
    public function processListAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();

        $type = $em->getRepository('ArtoAcvBundle:Type')->findOneByName('process');
        $processes = $em->getRepository('ArtoAcvBundle:Base')->findBy(
            array('family' => $request->get('family'),'type' => $type->getId(),'isVisible' => 'oui'),
            array('name' => 'ASC')
        );

        return $this->render('ArtoAcvBundle:Default:process.html.twig', array('processes' => $processes));
    }
    
    /**
     * Pour sélectionner un procédé
     * @return \Symfony\Component\HttpFoundation\Response Met à jour la page des parts avec le procédé sélectionné
     */
    public function processSelectAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $id = $this->getRequest()->get('id');

        $base = $em->getRepository('ArtoAcvBundle:Base')->find($id);
        $type = substr($base->getType(), 0, 1);
        $valParam2ByUN = '';
        
        $typeName = $base->getType()->getName();
        $familyName = $base->getFamily()->getName();
        $path = $typeName.'/'.$familyName;

        $name = sprintf('<strong class="%s">%s</strong><a href="#" class="select-process" data-toggle="tooltip">%s</a>',
                strtolower($type),
                strtoupper($type),
                $base->getName()
            );
        
        if($base->getValParam2ByUN() != null){
            $valParam2ByUN = $base->getValParam2ByUN();
        }

        $result = array(
            'id' => $base->getId(),
            'name' => $name,
            'type' => $base->getType(),
            'unit1' => $base->getUnit1(),
            'unit2' => $base->getUnit2(),
            'unitmass' => $base->getUnitMass(),
            'valParam2ByUN' => $valParam2ByUN,
            'path' => $path
        );

        $json = json_encode($result);
        $response = new Response($json);
        return $response;
    }
    
    /**
     * Recherche l'unité de transport
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function transportUnitAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();

        $unit = '';

        if ($request->get('base') != null) {
            $product = $em->getRepository('ArtoAcvBundle:Base')->find($request->get('base'));
            $unit = $product->getUnit1();
        }

        return new Response($unit);
    }


}
