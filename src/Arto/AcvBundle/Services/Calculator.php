<?php

namespace Arto\AcvBundle\Services;

use Arto\AcvBundle\Entity\Product;
use Arto\AcvBundle\Entity\ProductPart;
use Arto\AcvBundle\Entity\Project;
use Arto\AcvBundle\Entity\SimulationMaterial;
use Arto\AcvBundle\Entity\Part;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Calculator {
    
    protected $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    /**
     * Calcule les impacts des Matériaux
     * @param \Arto\AcvBundle\Entity\Product $product le produit
     * @param type $tab
     * @param type $percentage_field
     * @param type $simulationMaterials
     * @return type
     */
    public function calculateMaterial(Product $product,$tab = null, $percentage_field = null, $simulationMaterials = false) {
        //$em = $this->getDoctrine()->getEntityManager();
        $em = $this->em;
         
        // Initialize array to 11 values set to 0
        // Initialize array to 10 values set to 0
        if ($tab != null) {
            $m = $tab;
        } else {
            $m = array_fill(0, 11, 0);
        }

        foreach ($product->getAssemblies() as $assembly) {
            foreach ($assembly->getParts() as $part) {

                if ($part->getParent() != null) {
                    $quantity = $part->getParent()->getQuantity();

                    $base = $part->getMaterial();

                    if ($base != null) {

                        if ($part->getParam2() != null) {
                            $val = $part->getParam1() * $part->getParam2();
                        } else {
                            $val = $part->getParam1();
                        }

                        // Simulation for parts
                        if ($percentage_field != null && !$simulationMaterials) {
                            $percentage = $part->getParent()->$percentage_field();
                            if ($percentage != null) {
                                $val = $val * $percentage;
                            }
                        }

                        // Simulation for materials
                        if ($simulationMaterials) {
                            if ($part->getMaterial() != null) {
                                $simulation = $em->getRepository('ArtoAcvBundle:SimulationMaterial')->findOneBy(array(
                                    'product' => $product->getId(),
                                    'material' => $part->getMaterial()->getId()
                                ));

                                if ($simulation != null && $simulation->$percentage_field() != null) {
                                    $val = $val * $simulation->$percentage_field();
                                }
                            }
                        }

                        $mul = $assembly->getQuantity() * $quantity * $val;

                        $m[0] += $base->getRmd() * $mul;
                        $m[1] += $base->getEd() * $mul;
                        $m[2] += $base->getWd() * $mul;
                        $m[3] += $base->getGwp() * $mul;
                        $m[4] += $base->getOdp() * $mul;
                        $m[5] += $base->getAt() * $mul;
                        $m[6] += $base->getPocp() * $mul;
                        $m[7] += $base->getAa() * $mul;
                        $m[8] += $base->getWt() * $mul;
                        $m[9] += $base->getWe() * $mul;
                        $m[10] += $base->getHwp() * $mul;
                    }
                }
            }
        }
        
        return $m;
    }
    
    /**
     * Calcule les impacts des Procédés
     * @param \Arto\AcvBundle\Entity\Product $product
     * @param type $tab
     * @return type
     */
    public function calculateProcess(Product $product, $tab = null) {
        $em = $this->em;
        
        // Initialize array to 11 values set to 0
        if ($tab != null) {
            $m = $tab;
        } else {
            $m = array_fill(0, 11, 0);
        }

        foreach ($product->getAssemblies() as $assembly) {
            foreach ($assembly->getParts() as $part) {

                if ($part->getParent() != null) {
                    $quantity = $part->getParent()->getQuantity();

                    $base = $part->getProcess();

                    if ($base != null) {
                        if ($part->getParam4() != null) {
                            $val = $part->getParam3() * $part->getParam4();
                        } else {
                            $val = $part->getParam3();
                        }

                        $mul = $assembly->getQuantity() * $quantity * $val;

                        $m[0] += $base->getRmd() * $mul;
                        $m[1] += $base->getEd() * $mul;
                        $m[2] += $base->getWd() * $mul;
                        $m[3] += $base->getGwp() * $mul;
                        $m[4] += $base->getOdp() * $mul;
                        $m[5] += $base->getAt() * $mul;
                        $m[6] += $base->getPocp() * $mul;
                        $m[7] += $base->getAa() * $mul;
                        $m[8] += $base->getWt() * $mul;
                        $m[9] += $base->getWe() * $mul;
                        $m[10] += $base->getHwp() * $mul;
                    }

                    if ($product->getProject()->getTransportAppro() < 1) {
                        $base = $part->getTransport();

                        if ($base != null) {

                            $dist = $part->getDistance();
                            $res = $assembly->getQuantity() * $quantity * $dist;
                            $mul = $res;

                            $m[0] += $base->getRmd() * $mul;
                            $m[1] += $base->getEd() * $mul;
                            $m[2] += $base->getWd() * $mul;
                            $m[3] += $base->getGwp() * $mul;
                            $m[4] += $base->getOdp() * $mul;
                            $m[5] += $base->getAt() * $mul;
                            $m[6] += $base->getPocp() * $mul;
                            $m[7] += $base->getAa() * $mul;
                            $m[8] += $base->getWt() * $mul;
                            $m[9] += $base->getWe() * $mul;
                            $m[10] += $base->getHwp() * $mul;
                        }
                    }
                }
            }
        }

        $weight = $product->getWeight() / 1000;
        $appro = $product->getProject()->getTransportAppro();

        // Transport mondial : 19000km par bateau plus 1000km par camion> 32T
        if ($appro == 1) {

            $base = $em->getRepository('ArtoAcvBundle:Base')->findOneByCode('ELCD-0259');

            $mul = 19000 * $weight;

            $m[0] += $base->getRmd() * $mul;
            $m[1] += $base->getEd() * $mul;
            $m[2] += $base->getWd() * $mul;
            $m[3] += $base->getGwp() * $mul;
            $m[4] += $base->getOdp() * $mul;
            $m[5] += $base->getAt() * $mul;
            $m[6] += $base->getPocp() * $mul;
            $m[7] += $base->getAa() * $mul;
            $m[8] += $base->getWt() * $mul;
            $m[9] += $base->getWe() * $mul;
            $m[10] += $base->getHwp() * $mul;

            $base = $em->getRepository('ArtoAcvBundle:Base')->findOneByCode('ELCD-0122');
            $mul = 1000 * $weight;

            $m[0] += $base->getRmd() * $mul;
            $m[1] += $base->getEd() * $mul;
            $m[2] += $base->getWd() * $mul;
            $m[3] += $base->getGwp() * $mul;
            $m[4] += $base->getOdp() * $mul;
            $m[5] += $base->getAt() * $mul;
            $m[6] += $base->getPocp() * $mul;
            $m[7] += $base->getAa() * $mul;
            $m[8] += $base->getWt() * $mul;
            $m[9] += $base->getWe() * $mul;
            $m[10] += $base->getHwp() * $mul;
        }

        // Transport intracontinental : 3500km par camion> 32T
        if ($appro == 2) {

            $base = $em->getRepository('ArtoAcvBundle:Base')->findOneByCode('ELCD-0122');
            $mul = 3500 * $weight;

            $m[0] += $base->getRmd() * $mul;
            $m[1] += $base->getEd() * $mul;
            $m[2] += $base->getWd() * $mul;
            $m[3] += $base->getGwp() * $mul;
            $m[4] += $base->getOdp() * $mul;
            $m[5] += $base->getAt() * $mul;
            $m[6] += $base->getPocp() * $mul;
            $m[7] += $base->getAa() * $mul;
            $m[8] += $base->getWt() * $mul;
            $m[9] += $base->getWe() * $mul;
            $m[10] += $base->getHwp() * $mul;
        }

        // Transport local : 1000km par camion> 32T
        if ($appro == 3) {

            $base = $em->getRepository('ArtoAcvBundle:Base')->findOneByCode('ELCD-0122');
            $mul = 1000 * $weight;

            $m[0] += $base->getRmd() * $mul;
            $m[1] += $base->getEd() * $mul;
            $m[2] += $base->getWd() * $mul;
            $m[3] += $base->getGwp() * $mul;
            $m[4] += $base->getOdp() * $mul;
            $m[5] += $base->getAt() * $mul;
            $m[6] += $base->getPocp() * $mul;
            $m[7] += $base->getAa() * $mul;
            $m[8] += $base->getWt() * $mul;
            $m[9] += $base->getWe() * $mul;
            $m[10] += $base->getHwp() * $mul;
        }

        // Transport Stock
        if ($product->getProject()->getTransportStock() != null) {
            $base = $product->getProject()->getTransportStock();

            if ($base != null) {
                $mul = $weight * $product->getProject()->getTransportStockKm();

                $m[0] += $base->getRmd() * $mul;
                $m[1] += $base->getEd() * $mul;
                $m[2] += $base->getWd() * $mul;
                $m[3] += $base->getGwp() * $mul;
                $m[4] += $base->getOdp() * $mul;
                $m[5] += $base->getAt() * $mul;
                $m[6] += $base->getPocp() * $mul;
                $m[7] += $base->getAa() * $mul;
                $m[8] += $base->getWt() * $mul;
                $m[9] += $base->getWe() * $mul;
                $m[10] += $base->getHwp() * $mul;
            }
        }
        
        return $m;
    }
    
    /**
     * Calcule les déchets
     * @param \Arto\AcvBundle\Entity\Product $product
     * @return type
     */
    public function calculateWastes(Product $product){
        $m = array_fill(0, 11, 0);
        $em = $this->em;
       
        //Calcul des déchets
        $baseTransport =  $em->getRepository('ArtoAcvBundle:Base')->findOneByCode('ELCD-0122');
        
        //Electronique
        $weightElectronic = $this->calculateWeightElectronic($product);
        
        $base = $em->getRepository('ArtoAcvBundle:Base')->findOneByCode('ELCD-0108');
        $mul = 0.13 * $weightElectronic;
        $mulTransport = 0.13 * ($weightElectronic/1000) * 1000;
        
        $m[0] += $base->getRmd() * $mul;
        $m[1] += $base->getEd() * $mul;
        $m[2] += $base->getWd() * $mul;
        $m[3] += $base->getGwp() * $mul;
        $m[4] += $base->getOdp() * $mul;
        $m[5] += $base->getAt() * $mul;
        $m[6] += $base->getPocp() * $mul;
        $m[7] += $base->getAa() * $mul;
        $m[8] += $base->getWt() * $mul;
        $m[9] += $base->getWe() * $mul;
        $m[10] += $base->getHwp() * $mul;
        
        $m[0] += $baseTransport->getRmd() * $mulTransport;
        $m[1] += $baseTransport->getEd() * $mulTransport;
        $m[2] += $baseTransport->getWd() * $mulTransport;
        $m[3] += $baseTransport->getGwp() * $mulTransport;
        $m[4] += $baseTransport->getOdp() * $mulTransport;
        $m[5] += $baseTransport->getAt() * $mulTransport;
        $m[6] += $baseTransport->getPocp() * $mulTransport;
        $m[7] += $baseTransport->getAa() * $mulTransport;
        $m[8] += $baseTransport->getWt() * $mulTransport;
        $m[9] += $baseTransport->getWe() * $mulTransport;
        $m[10] += $baseTransport->getHwp() * $mulTransport;
        
        //Métallique
        $weightMetallic = $this->calculateWeightMetallic($product);
        
        $base = $em->getRepository('ArtoAcvBundle:Base')->findOneByCode('ELCD-0107');
        $mul = 0.32 * $weightMetallic;
        $mulTransport = 0.32 * ($weightMetallic/1000) * 1000;
        
        $m[0] += $base->getRmd() * $mul;
        $m[1] += $base->getEd() * $mul;
        $m[2] += $base->getWd() * $mul;
        $m[3] += $base->getGwp() * $mul;
        $m[4] += $base->getOdp() * $mul;
        $m[5] += $base->getAt() * $mul;
        $m[6] += $base->getPocp() * $mul;
        $m[7] += $base->getAa() * $mul;
        $m[8] += $base->getWt() * $mul;
        $m[9] += $base->getWe() * $mul;
        $m[10] += $base->getHwp() * $mul;
        
        $m[0] += $baseTransport->getRmd() * $mulTransport;
        $m[1] += $baseTransport->getEd() * $mulTransport;
        $m[2] += $baseTransport->getWd() * $mulTransport;
        $m[3] += $baseTransport->getGwp() * $mulTransport;
        $m[4] += $baseTransport->getOdp() * $mulTransport;
        $m[5] += $baseTransport->getAt() * $mulTransport;
        $m[6] += $baseTransport->getPocp() * $mulTransport;
        $m[7] += $baseTransport->getAa() * $mulTransport;
        $m[8] += $baseTransport->getWt() * $mulTransport;
        $m[9] += $baseTransport->getWe() * $mulTransport;
        $m[10] += $baseTransport->getHwp() * $mulTransport;
        
        
        //Plastique
        $weightPlastic = $this->calculateWeightPlastic($product);
        
        $base = $em->getRepository('ArtoAcvBundle:Base')->findOneByCode('ELCD-0114');
        $mul = 0.12 * $weightPlastic;
        $mulTransport = 0.12 * ($weightPlastic/1000) * 1000;
        
        $m[0] += $base->getRmd() * $mul;
        $m[1] += $base->getEd() * $mul;
        $m[2] += $base->getWd() * $mul;
        $m[3] += $base->getGwp() * $mul;
        $m[4] += $base->getOdp() * $mul;
        $m[5] += $base->getAt() * $mul;
        $m[6] += $base->getPocp() * $mul;
        $m[7] += $base->getAa() * $mul;
        $m[8] += $base->getWt() * $mul;
        $m[9] += $base->getWe() * $mul;
        $m[10] += $base->getHwp() * $mul;
        
        $m[0] += $baseTransport->getRmd() * $mulTransport;
        $m[1] += $baseTransport->getEd() * $mulTransport;
        $m[2] += $baseTransport->getWd() * $mulTransport;
        $m[3] += $baseTransport->getGwp() * $mulTransport;
        $m[4] += $baseTransport->getOdp() * $mulTransport;
        $m[5] += $baseTransport->getAt() * $mulTransport;
        $m[6] += $baseTransport->getPocp() * $mulTransport;
        $m[7] += $baseTransport->getAa() * $mulTransport;
        $m[8] += $baseTransport->getWt() * $mulTransport;
        $m[9] += $baseTransport->getWe() * $mulTransport;
        $m[10] += $baseTransport->getHwp() * $mulTransport;
        
        
        return $m;
        
    }
    
    /**
     * Calcule le poids des déchets
     * @param \Arto\AcvBundle\Entity\Product $product
     * @return type
     */
    public function calculateWeightWastes(Product $product){
        $weight = 0;
        
        //Electronique
        $weightElectronic = $this->calculateWeightElectronic($product);
        $weightWasteElectronic = 0.13 * $weightElectronic;
        
        //Metallique
        $weightMetallic = $this->calculateWeightMetallic($product);
        $weightWasteMetallic = 0.32 * $weightMetallic;
        
        //Plastique
        $weightPlastic = $this->calculateWeightPlastic($product);
        $weightWastePlastic = 0.12 * $weightPlastic;
        
        $weight = $weight + $weightWasteElectronic + $weightWasteMetallic + $weightWastePlastic;
        
        return $weight;
        
    }
    
    /**
     * Calcule les impacts à la Fabrication
     * @param \Arto\AcvBundle\Entity\Product $product
     * @param type $tab
     * @return type
     */
    public function calculateManufacturing(Product $product, $tab = null) {
        // Initialize array to 11 values set to 0
        if ($tab != null) {
            $m = $tab;
        } else {
            $m = array_fill(0, 11, 0);
        }

        $m1 = $this->calculateMaterial($product);
        $m2 = $this->calculateProcess($product);

        $m[0] = $m1[0] + $m2[0];
        $m[1] = $m1[1] + $m2[1];
        $m[2] = $m1[2] + $m2[2];
        $m[3] = $m1[3] + $m2[3];
        $m[4] = $m1[4] + $m2[4];
        $m[5] = $m1[5] + $m2[5];
        $m[6] = $m1[6] + $m2[6];
        $m[7] = $m1[7] + $m2[7];
        $m[8] = $m1[8] + $m2[8];
        $m[9] = $m1[9] + $m2[9];
        $m[10] = $m1[10] + $m2[10];

        return $m;
    }
    
    /**
     * Calcule les impacts à l'Utilisation
     * @param \Arto\AcvBundle\Entity\Product $product
     * @param type $tab
     * @return type
     */
    public function calculateUsage(Product $product, $tab = null) {
        // Initialize array to 11 values set to 0
        if ($tab != null) {
            $m = $tab;
        } else {
            $m = array_fill(0, 11, 0);
        }

        $usage = $product->getUsage();

        if ($usage != null) {
            if ($usage->getEnergyType() != 3) {
                $base = $usage->getEnergy();

                $power = $usage->getOnKw() * ($usage->getOnPercent() / 100);
                $power += $usage->getOffKw() * ($usage->getOffPercent() / 100);
                $power += $usage->getStandbyActiveKw() * ($usage->getStandbyActivePercent() / 100);
                //$power += $usage->getStandbyPassiveKw() * ($usage->getStandbyPassivePercent() / 100);

                $mul = $usage->getLifetime() * 365 * 24 * $power / 1000;

                $m[0] += $base->getRmd() * $mul;
                $m[1] += $base->getEd() * $mul;
                $m[2] += $base->getWd() * $mul;
                $m[3] += $base->getGwp() * $mul;
                $m[4] += $base->getOdp() * $mul;
                $m[5] += $base->getAt() * $mul;
                $m[6] += $base->getPocp() * $mul;
                $m[7] += $base->getAa() * $mul;
                $m[8] += $base->getWt() * $mul;
                $m[9] += $base->getWe() * $mul;
                $m[10] += $base->getHwp() * $mul;

                if (count($usage->getParts()) > 0) {
                    foreach ($usage->getParts() as $part) {
                        if ($part->getCategory() == 1) {
                            $base = $part->getBase();
                            $mul = $part->getQuantity() * $part->getVal();

                            $m[0] += $base->getRmd() * $mul;
                            $m[1] += $base->getEd() * $mul;
                            $m[2] += $base->getWd() * $mul;
                            $m[3] += $base->getGwp() * $mul;
                            $m[4] += $base->getOdp() * $mul;
                            $m[5] += $base->getAt() * $mul;
                            $m[6] += $base->getPocp() * $mul;
                            $m[7] += $base->getAa() * $mul;
                            $m[8] += $base->getWt() * $mul;
                            $m[9] += $base->getWe() * $mul;
                            $m[10] += $base->getHwp() * $mul;
                        }
                    }
                }
            }
            else{
                if (count($usage->getParts()) > 0) {
                    foreach ($usage->getParts() as $part) {
                        if ($part->getCategory() == 1) {
                            $base = $part->getBase();
                            $mul = $part->getQuantity() * $part->getVal();

                            $m[0] += $base->getRmd() * $mul;
                            $m[1] += $base->getEd() * $mul;
                            $m[2] += $base->getWd() * $mul;
                            $m[3] += $base->getGwp() * $mul;
                            $m[4] += $base->getOdp() * $mul;
                            $m[5] += $base->getAt() * $mul;
                            $m[6] += $base->getPocp() * $mul;
                            $m[7] += $base->getAa() * $mul;
                            $m[8] += $base->getWt() * $mul;
                            $m[9] += $base->getWe() * $mul;
                            $m[10] += $base->getHwp() * $mul;
                        }
                    }
                }
            }
        }

        return $m;
    }
    
    /**
     * Calcule les impacts à l'Utilisation
     * @param \Arto\AcvBundle\Entity\Product $product
     * @param type $tab
     * @return type
     */
    public function calculateEnergy(Product $product, $tab = null) {
        // Initialize array to 11 values set to 0
        if ($tab != null) {
            $m = $tab;
        } else {
            $m = array_fill(0, 11, 0);
        }

        $usage = $product->getUsage();

        if ($usage != null) {
            if ($usage->getEnergyType() != 3) {

                $base = $usage->getEnergy();

                $power = $usage->getOnKw() * ($usage->getOnPercent() / 100);
                $power += $usage->getOffKw() * ($usage->getOffPercent() / 100);
                $power += $usage->getStandbyActiveKw() * ($usage->getStandbyActivePercent() / 100);
                //$power += $usage->getStandbyPassiveKw() * ($usage->getStandbyPassivePercent() / 100);

                $mul = $usage->getLifetime() * 365 * 24 * $power / 1000;

                $m[0] += $base->getRmd() * $mul;
                $m[1] += $base->getEd() * $mul;
                $m[2] += $base->getWd() * $mul;
                $m[3] += $base->getGwp() * $mul;
                $m[4] += $base->getOdp() * $mul;
                $m[5] += $base->getAt() * $mul;
                $m[6] += $base->getPocp() * $mul;
                $m[7] += $base->getAa() * $mul;
                $m[8] += $base->getWt() * $mul;
                $m[9] += $base->getWe() * $mul;
                $m[10] += $base->getHwp() * $mul;
            }
        }

        return $m;
    }
    
    /**
     * Calcule l'impact des consommables
     * @param \Arto\AcvBundle\Entity\Product $product
     * @param type $tab
     * @return type
     */
    public function calculateConsommables(Product $product, $tab = null) {
        // Initialize array to 11 values set to 0
        if ($tab != null) {
            $m = $tab;
        } else {
            $m = array_fill(0, 11, 0);
        }

        $usage = $product->getUsage();

        if ($usage != null) {
            if ($usage->getEnergyType() != 3) {

                $base = $usage->getEnergy();

                if (count($usage->getParts()) > 0) {
                    foreach ($usage->getParts() as $part) {
                        if ($part->getCategory() == 1) {
                            $base = $part->getBase();
                            $mul = $part->getQuantity() * $part->getVal();

                            $m[0] += $base->getRmd() * $mul;
                            $m[1] += $base->getEd() * $mul;
                            $m[2] += $base->getWd() * $mul;
                            $m[3] += $base->getGwp() * $mul;
                            $m[4] += $base->getOdp() * $mul;
                            $m[5] += $base->getAt() * $mul;
                            $m[6] += $base->getPocp() * $mul;
                            $m[7] += $base->getAa() * $mul;
                            $m[8] += $base->getWt() * $mul;
                            $m[9] += $base->getWe() * $mul;
                            $m[10] += $base->getHwp() * $mul;
                        }
                    }
                }
            }else{
                if (count($usage->getParts()) > 0) {
                    foreach ($usage->getParts() as $part) {
                        if ($part->getCategory() == 1) {
                            $base = $part->getBase();
                            $mul = $part->getQuantity() * $part->getVal();

                            $m[0] += $base->getRmd() * $mul;
                            $m[1] += $base->getEd() * $mul;
                            $m[2] += $base->getWd() * $mul;
                            $m[3] += $base->getGwp() * $mul;
                            $m[4] += $base->getOdp() * $mul;
                            $m[5] += $base->getAt() * $mul;
                            $m[6] += $base->getPocp() * $mul;
                            $m[7] += $base->getAa() * $mul;
                            $m[8] += $base->getWt() * $mul;
                            $m[9] += $base->getWe() * $mul;
                            $m[10] += $base->getHwp() * $mul;
                        }
                    }
                }
            }
        }

        return $m;
    }
    
    /**
     * Calcule les impacts à l'Installation
     * @param \Arto\AcvBundle\Entity\Product $product
     * @param type $tab
     * @return type
     */
    public function calculateInstall(Product $product, $tab = null) {
        $em = $this->em;
        
        // Initialize array to 11 values set to 0
        if ($tab != null) {
            $m = $tab;
        } else {
            $m = array_fill(0, 11, 0);
        }
        
        $usage = $product->getUsage();
        $weightEmballage = $this->calculateWeightEmballage($product);
        
        $base = $em->getRepository('ArtoAcvBundle:Base')->findOneByCode('ELCD-0264');
        $mul = $weightEmballage;

        $m[0] += $base->getRmd() * $mul;
        $m[1] += $base->getEd() * $mul;
        $m[2] += $base->getWd() * $mul;
        $m[3] += $base->getGwp() * $mul;
        $m[4] += $base->getOdp() * $mul;
        $m[5] += $base->getAt() * $mul;
        $m[6] += $base->getPocp() * $mul;
        $m[7] += $base->getAa() * $mul;
        $m[8] += $base->getWt() * $mul;
        $m[9] += $base->getWe() * $mul;
        $m[10] += $base->getHwp() * $mul;
        
        $base = $em->getRepository('ArtoAcvBundle:Base')->findOneByCode('ELCD-0122');
        $mul =  $weightEmballage;

        $m[0] += $base->getRmd() * $mul;
        $m[1] += $base->getEd() * $mul;
        $m[2] += $base->getWd() * $mul;
        $m[3] += $base->getGwp() * $mul;
        $m[4] += $base->getOdp() * $mul;
        $m[5] += $base->getAt() * $mul;
        $m[6] += $base->getPocp() * $mul;
        $m[7] += $base->getAa() * $mul;
        $m[8] += $base->getWt() * $mul;
        $m[9] += $base->getWe() * $mul;
        $m[10] += $base->getHwp() * $mul;
   
        if ($usage != null) {
            if (count($usage->getParts()) > 0) {
                foreach ($usage->getParts() as $part) {
                    if ($part->getCategory() == 2) {
                        $base = $part->getBase();
                        $mul = $part->getQuantity() * $part->getVal();

                        $m[0] += $base->getRmd() * $mul;
                        $m[1] += $base->getEd() * $mul;
                        $m[2] += $base->getWd() * $mul;
                        $m[3] += $base->getGwp() * $mul;
                        $m[4] += $base->getOdp() * $mul;
                        $m[5] += $base->getAt() * $mul;
                        $m[6] += $base->getPocp() * $mul;
                        $m[7] += $base->getAa() * $mul;
                        $m[8] += $base->getWt() * $mul;
                        $m[9] += $base->getWe() * $mul;
                        $m[10] += $base->getHwp() * $mul;
                    }
                }
            }
        }
        
        //Calcul des déchets
        $wastes = $this->calculateWastes($product);
        
        $m[0] += $wastes[0];
        $m[1] += $wastes[1];
        $m[2] += $wastes[2];
        $m[3] += $wastes[3];
        $m[4] += $wastes[4];
        $m[5] += $wastes[5];
        $m[6] += $wastes[6];
        $m[7] += $wastes[7];
        $m[8] += $wastes[8];
        $m[9] += $wastes[9];
        $m[10] += $wastes[10];

        return $m;
    }

    /**
     * Calcule les impacts des transports
     * @param \Arto\AcvBundle\Entity\Product $product
     * @param type $tab
     * @return type
     */
    public function calculateDelivery(Product $product, $tab = null) {
        $em = $this->em;

        // Initialize array to 11 values set to 0
        if ($tab != null) {
            $m = $tab;
        } else {
            $m = array_fill(0, 11, 0);
        }

        $delivery = $product->getDelivery();

        if ($delivery != null) {

            $weight = $product->getWeight() / 1000;

            if ($delivery->getDetailed() == 1) {

                foreach ($delivery->getParts() as $part) {
                    $base = $part->getTransport();
                    $dist = $part->getDistance();
                    $mul = $dist * $weight;

                    $m[0] += $base->getRmd() * $mul;
                    $m[1] += $base->getEd() * $mul;
                    $m[2] += $base->getWd() * $mul;
                    $m[3] += $base->getGwp() * $mul;
                    $m[4] += $base->getOdp() * $mul;
                    $m[5] += $base->getAt() * $mul;
                    $m[6] += $base->getPocp() * $mul;
                    $m[7] += $base->getAa() * $mul;
                    $m[8] += $base->getWt() * $mul;
                    $m[9] += $base->getWe() * $mul;
                    $m[10] += $base->getHwp() * $mul;
                }
            }

            if ($delivery->getDetailed() == 2) {


                // Transport mondial : 19000km par bateau plus 1000km par camion> 32T
                if ($delivery->getMode() == 1) {

                    $base = $em->getRepository('ArtoAcvBundle:Base')->findOneByCode('ELCD-0259');
                    $mul = 19000 * $weight;

                    $m[0] += $base->getRmd() * $mul;
                    $m[1] += $base->getEd() * $mul;
                    $m[2] += $base->getWd() * $mul;
                    $m[3] += $base->getGwp() * $mul;
                    $m[4] += $base->getOdp() * $mul;
                    $m[5] += $base->getAt() * $mul;
                    $m[6] += $base->getPocp() * $mul;
                    $m[7] += $base->getAa() * $mul;
                    $m[8] += $base->getWt() * $mul;
                    $m[9] += $base->getWe() * $mul;
                    $m[10] += $base->getHwp() * $mul;

                    $base = $em->getRepository('ArtoAcvBundle:Base')->findOneByCode('ELCD-0122');
                    $mul = 1000 * $weight;

                    $m[0] += $base->getRmd() * $mul;
                    $m[1] += $base->getEd() * $mul;
                    $m[2] += $base->getWd() * $mul;
                    $m[3] += $base->getGwp() * $mul;
                    $m[4] += $base->getOdp() * $mul;
                    $m[5] += $base->getAt() * $mul;
                    $m[6] += $base->getPocp() * $mul;
                    $m[7] += $base->getAa() * $mul;
                    $m[8] += $base->getWt() * $mul;
                    $m[9] += $base->getWe() * $mul;
                    $m[10] += $base->getHwp() * $mul;
                }

                // Transport intracontinental : 3500km par camion> 32T
                if ($delivery->getMode() == 2) {

                    $base = $em->getRepository('ArtoAcvBundle:Base')->findOneByCode('ELCD-0122');
                    $mul = 3500 * $weight;
                    
                    $m[0] += $base->getRmd() * $mul;
                    $m[1] += $base->getEd() * $mul;
                    $m[2] += $base->getWd() * $mul;
                    $m[3] += $base->getGwp() * $mul;
                    $m[4] += $base->getOdp() * $mul;
                    $m[5] += $base->getAt() * $mul;
                    $m[6] += $base->getPocp() * $mul;
                    $m[7] += $base->getAa() * $mul;
                    $m[8] += $base->getWt() * $mul;
                    $m[9] += $base->getWe() * $mul;
                    $m[10] += $base->getHwp() * $mul;
                }

                // Transport local : 1000km par camion> 32T
                if ($delivery->getMode() == 3) {

                    $base = $em->getRepository('ArtoAcvBundle:Base')->findOneByCode('ELCD-0122');
                    $mul = 1000 * $weight;

                    $m[0] += $base->getRmd() * $mul;
                    $m[1] += $base->getEd() * $mul;
                    $m[2] += $base->getWd() * $mul;
                    $m[3] += $base->getGwp() * $mul;
                    $m[4] += $base->getOdp() * $mul;
                    $m[5] += $base->getAt() * $mul;
                    $m[6] += $base->getPocp() * $mul;
                    $m[7] += $base->getAa() * $mul;
                    $m[8] += $base->getWt() * $mul;
                    $m[9] += $base->getWe() * $mul;
                    $m[10] += $base->getHwp() * $mul;
                }
            }
        }

        return $m;
    }
    
    /**
     * Calcule le poids de l'emballage
     * @param \Arto\AcvBundle\Entity\Product $product
     * @return type
     */
    public function calculateWeightEmballage(Product $product){
         $weightEmballage = 0;
        
         foreach ($product->getParts() as $productPart) {
            $productPartType = $productPart->getType();
            if ($productPartType != "produitEmballage" && $productPartType != "produit") {
                $weightEmballage = $weightEmballage + $productPart->getWeight();
            }
         }
         
         return $weightEmballage;
    }

    /**
     * Calcule les impacts de la Fin de vie
     * @param \Arto\AcvBundle\Entity\Product $product
     * @param type $tab
     * @return type
     */
    public function calculateEol(Product $product, $tab = null) {
        $em = $this->em;

        $project = $product->getProject();
        $projectId = $project->getId();

        // Initialize array to 11 values set to 0
        if ($tab != null) {
            $m = $tab;
        } else {
            $m = array_fill(0, 11, 0);
        }

        $eol = $product->getEol();

        if ($eol != null) {

            if ($eol->getType() == 1) {

                $base = $em->getRepository('ArtoAcvBundle:Base')->findOneByCode('ELCD-0122');

                $weight = $product->getWeight() / 1000;
                $mul = 1000 * $weight;

                $m[0] += $base->getRmd() * $mul;
                $m[1] += $base->getEd() * $mul;
                $m[2] += $base->getWd() * $mul;
                $m[3] += $base->getGwp() * $mul;
                $m[4] += $base->getOdp() * $mul;
                $m[5] += $base->getAt() * $mul;
                $m[6] += $base->getPocp() * $mul;
                $m[7] += $base->getAa() * $mul;
                $m[8] += $base->getWt() * $mul;
                $m[9] += $base->getWe() * $mul;
                $m[10] += $base->getHwp() * $mul;
            }

            // ECO DEEE
            if ($eol->getType() == 2) {
                // TODO
            }

            if ($eol->getType() == 3) {
                $weightEmballage = $this->calculateWeightEmballage($product);
                $weight = $product->getWeight();
                $weightSansEmballage = $weight - $weightEmballage;
               

                // DECHETTERIE
                //if ($eol->getImpactDecharge() > 0) {

                    $base = $em->getRepository('ArtoAcvBundle:Base')->findOneByCode('ELCD-0112');
                    $mul = $weightSansEmballage * ($eol->getImpactDecharge() / 100);

                    $m[0] += $base->getRmd() * $mul;
                    $m[1] += $base->getEd() * $mul;
                    $m[2] += $base->getWd() * $mul;
                    $m[3] += $base->getGwp() * $mul;
                    $m[4] += $base->getOdp() * $mul;
                    $m[5] += $base->getAt() * $mul;
                    $m[6] += $base->getPocp() * $mul;
                    $m[7] += $base->getAa() * $mul;
                    $m[8] += $base->getWt() * $mul;
                    $m[9] += $base->getWe() * $mul;
                    $m[10] += $base->getHwp() * $mul;
                //}

                // VALORISATION
                //if ($eol->getImpactValorisation() > 0) {

                    $base = $em->getRepository('ArtoAcvBundle:Base')->findOneByCode('ELCD-0264');
                    $mul = $weightSansEmballage * ($eol->getImpactValorisation() / 100);

                    $m[0] += $base->getRmd() * $mul;
                    $m[1] += $base->getEd() * $mul;
                    $m[2] += $base->getWd() * $mul;
                    $m[3] += $base->getGwp() * $mul;
                    $m[4] += $base->getOdp() * $mul;
                    $m[5] += $base->getAt() * $mul;
                    $m[6] += $base->getPocp() * $mul;
                    $m[7] += $base->getAa() * $mul;
                    $m[8] += $base->getWt() * $mul;
                    $m[9] += $base->getWe() * $mul;
                    $m[10] += $base->getHwp() * $mul;
                //}
                    
                //1000 kms par camion produit sans emballage
                    $base = $em->getRepository('ArtoAcvBundle:Base')->findOneByCode('ELCD-0122');
                    $mul =  $weightSansEmballage;

                    $m[0] += $base->getRmd() * $mul;
                    $m[1] += $base->getEd() * $mul;
                    $m[2] += $base->getWd() * $mul;
                    $m[3] += $base->getGwp() * $mul;
                    $m[4] += $base->getOdp() * $mul;
                    $m[5] += $base->getAt() * $mul;
                    $m[6] += $base->getPocp() * $mul;
                    $m[7] += $base->getAa() * $mul;
                    $m[8] += $base->getWt() * $mul;
                    $m[9] += $base->getWe() * $mul;
                    $m[10] += $base->getHwp() * $mul;
            }

            if ($eol->getType() == 4) {
                //$weight = $product->getWeight();
                $eolParts = $em->getRepository('ArtoAcvBundle:EolPart')->findBy(
                        array('project' => $projectId));

                foreach ($eolParts as $eolPart) {
                    $weight = $eolPart->getWeight() * $eolPart->getQuantity();
                    $traitement = $eolPart->getTraitement();

                    $distance = $eolPart->getDistance();
                    $transport = $eolPart->getTransport();

                    $m[0] += $traitement->getRmd() * $weight;
                    $m[1] += $traitement->getEd() * $weight;
                    $m[2] += $traitement->getWd() * $weight;
                    $m[3] += $traitement->getGwp() * $weight;
                    $m[4] += $traitement->getOdp() * $weight;
                    $m[5] += $traitement->getAt() * $weight;
                    $m[6] += $traitement->getPocp() * $weight;
                    $m[7] += $traitement->getAa() * $weight;
                    $m[8] += $traitement->getWt() * $weight;
                    $m[9] += $traitement->getWe() * $weight;
                    $m[10] += $traitement->getHwp() * $weight;

                    $weight = $weight / 1000;

                    $mul = $distance * $weight;

                    //Si on a un transport, on calcule
                    if ($transport != null) {
                        $m[0] += $transport->getRmd() * $mul;
                        $m[1] += $transport->getEd() * $mul;
                        $m[2] += $transport->getWd() * $mul;
                        $m[3] += $transport->getGwp() * $mul;
                        $m[4] += $transport->getOdp() * $mul;
                        $m[5] += $transport->getAt() * $mul;
                        $m[6] += $transport->getPocp() * $mul;
                        $m[7] += $transport->getAa() * $mul;
                        $m[8] += $transport->getWt() * $mul;
                        $m[9] += $transport->getWe() * $mul;
                        $m[10] += $transport->getHwp() * $mul;
                    }
                }
            }
        }
        
        return $m;
    }

    /**
     * Calcule les impacts Totaux
     * @param \Arto\AcvBundle\Entity\Product $product
     * @return type
     */
    public function calculateAll(Product $product) { 
        $tab = $this->calculateMaterial($product);
        $tab = $this->calculateProcess($product, $tab);
        $tab = $this->calculateDelivery($product, $tab);
        $tab = $this->calculateUsage($product, $tab);
        $tab = $this->calculateInstall($product, $tab);
        $tab = $this->calculateEol($product, $tab);

        return $tab;
    }
    
    /**
     * Calcule les parts de Simulation->"Optimisation des masses des articles" pour tracer le radar
     * @param \Arto\AcvBundle\Entity\Product $product
     * @param type $field_key le numéro de la simulation (1, 2 ou 3)
     * @return type
     */
    public function calculateSimulationParts(Product $product, $field_key) {
        $field = 'getPercentage' . $field_key;
        $tab = $this->calculateMaterial($product, null, $field);

        return $tab;
    }
    
    /**
     * Calcule les parts de Simulation->"Optimisation masses matières & composants" pour tracer le radar
     * @param \Arto\AcvBundle\Entity\Product $product
     * @param type $field_key le numéro de la simulation (1, 2 ou 3)
     * @return type
     */
    public function calculateSimulationMaterials(Product $product, $field_key) {
        $field = 'getPercentage' . $field_key;
        $tab = $this->calculateMaterial($product, null, $field, true);

        return $tab;
    }
    
    /**
     * Calcule les totaux des impacts sur les 11 indicateurs
     * @param type $m
     * @param type $p
     * @param type $a
     * @param type $d
     * @param type $u
     * @param type $energy
     * @param type $c
     * @param type $i
     * @param type $e
     * @return type
     */
    public function calculateTotalsForIndex($m,$p,$a,$d,$u,$energy,$c,$i,$e){
        $t = array_fill(0, 11, 0);
        $t[0] = $m[0] + $p[0] + $d[0] + $u[0] + $i[0] + $e[0];
        $t[1] = $m[1] + $p[1] + $d[1] + $u[1] + $i[1] + $e[1];
        $t[2] = $m[2] + $p[2] + $d[2] + $u[2] + $i[2] + $e[2];
        $t[3] = $m[3] + $p[3] + $d[3] + $u[3] + $i[3] + $e[3];
        $t[4] = $m[4] + $p[4] + $d[4] + $u[4] + $i[4] + $e[4];
        $t[5] = $m[5] + $p[5] + $d[5] + $u[5] + $i[5] + $e[5];
        $t[6] = $m[6] + $p[6] + $d[6] + $u[6] + $i[6] + $e[6];
        $t[7] = $m[7] + $p[7] + $d[7] + $u[7] + $i[7] + $e[7];
        $t[8] = $m[8] + $p[8] + $d[8] + $u[8] + $i[8] + $e[8];
        $t[9] = $m[9] + $p[9] + $d[9] + $u[9] + $i[9] + $e[9];
        $t[10] = $m[10] + $p[10] + $d[10] + $u[10] + $i[10] + $e[10];
        
        return $t;
    }
    
    /**
     * Remplace les nombres négatifs par 0 dans un tableau
     * @param array $array
     * @return array
     */
    public function replaceNegativesByZero(array $array){
        $result = array(); 
        foreach ($array as $key => $value){
            if($value < 0){
               $result[$key] = 0;
            }else{
               $result[$key] = $array[$key];
            } 
        } 
        return $result; 
     }
    
     /**
      * Calcule les totaux des impacts en % sur les 11 indicateurs
      * @param type $m
      * @param type $p
      * @param type $a
      * @param type $d
      * @param type $u
      * @param type $energy
      * @param type $c
      * @param type $i
      * @param type $e
      * @return type
      */
     public function calculateTotalsRelativeForIndex($m,$p,$a,$d,$u,$energy,$c,$i,$e){
        $t = array_fill(0, 11, 0);
        $m = $this->replaceNegativesByZero($m);
        $p = $this->replaceNegativesByZero($p);
        $a = $this->replaceNegativesByZero($a);
        $d = $this->replaceNegativesByZero($d);
        $u = $this->replaceNegativesByZero($u);
        $energy = $this->replaceNegativesByZero($energy);
        $c = $this->replaceNegativesByZero($c);
        $i = $this->replaceNegativesByZero($i);
        $e = $this->replaceNegativesByZero($e);
        $t[0] = $m[0] + $p[0] + $d[0] + $u[0] + $i[0] + $e[0];
        $t[1] = $m[1] + $p[1] + $d[1] + $u[1] + $i[1] + $e[1];
        $t[2] = $m[2] + $p[2] + $d[2] + $u[2] + $i[2] + $e[2];
        $t[3] = $m[3] + $p[3] + $d[3] + $u[3] + $i[3] + $e[3];
        $t[4] = $m[4] + $p[4] + $d[4] + $u[4] + $i[4] + $e[4];
        $t[5] = $m[5] + $p[5] + $d[5] + $u[5] + $i[5] + $e[5];
        $t[6] = $m[6] + $p[6] + $d[6] + $u[6] + $i[6] + $e[6];
        $t[7] = $m[7] + $p[7] + $d[7] + $u[7] + $i[7] + $e[7];
        $t[8] = $m[8] + $p[8] + $d[8] + $u[8] + $i[8] + $e[8];
        $t[9] = $m[9] + $p[9] + $d[9] + $u[9] + $i[9] + $e[9];
        $t[10] = $m[10] + $p[10] + $d[10] + $u[10] + $i[10] + $e[10];
        
        return $t;
    }
    
    /**
     * Calcule les totaux d'impacts sur les Matériaux
     * @param \Arto\AcvBundle\Entity\Product $product
     * @return type
     */
    public function calculateMaterialTotalsImpact(Product $product){
        $total_impact = 0;
        $total_impact_RMD = 0;
        $total_impact_ED = 0;
        $total_impact_WD = 0;
        $total_impact_GWP = 0;
        $total_impact_ODP = 0;
        $total_impact_AT = 0;
        $total_impact_POCP = 0;
        $total_impact_AA = 0;
        $total_impact_WT = 0;
        $total_impact_WE = 0;
        $total_impact_HWP = 0;

        foreach ($product->getAssemblies() as $assembly) {
            foreach ($assembly->getParts() as $part) {
                if ($part->getParent() != null && $part->getMaterial() != null) {
                    $material = $part->getMaterial();
                    $id = $material->getId();

                    if ($part->getParam1() != null) {
                        $val = $part->getParam1();
                    }

                    $newVal = $assembly->getQuantity() * $part->getParent()->getQuantity() * $val;

                    $total_impact_RMD += $material->getRmd() * $newVal;
                    $total_impact_ED += $material->getEd() * $newVal;
                    $total_impact_WD += $material->getWd() * $newVal;
                    $total_impact_GWP += $material->getGwp() * $newVal;
                    $total_impact_ODP += $material->getOdp() * $newVal;
                    $total_impact_AT += $material->getAt() * $newVal;
                    $total_impact_POCP += $material->getPocp() * $newVal;
                    $total_impact_AA += $material->getAa() * $newVal;
                    $total_impact_WT += $material->getWt() * $newVal;
                    $total_impact_WE += $material->getWe() * $newVal;
                    $total_impact_HWP += $material->getHwp() * $newVal;
                }
            }
        }

        $totals = array();
        $totals['RMD'] = $total_impact_RMD;
        $totals['ED'] = $total_impact_ED;
        $totals['WD'] = $total_impact_WD;
        $totals['GWP'] = $total_impact_GWP;
        $totals['ODP'] = $total_impact_ODP;
        $totals['AT'] = $total_impact_AT;
        $totals['POCP'] = $total_impact_POCP;
        $totals['AA'] = $total_impact_AA;
        $totals['WT'] = $total_impact_WT;
        $totals['WE'] = $total_impact_WE;
        $totals['HWP'] = $total_impact_HWP;

        return $totals;
    }
    
    /**
     * Calcule les totaux des impacts
     * @param \Arto\AcvBundle\Entity\Product $product
     * @param type $wastes
     * @return type
     */
    public function calculateTotalsImpacts(Product $product, $wastes){
        $total_impact = 0;
        $total_impact_RMD = 0;
        $total_impact_ED = 0;
        $total_impact_WD = 0;
        $total_impact_GWP = 0;
        $total_impact_ODP = 0;
        $total_impact_AT = 0;
        $total_impact_POCP = 0;
        $total_impact_AA = 0;
        $total_impact_WT = 0;
        $total_impact_WE = 0;
        $total_impact_HWP = 0;
        
        foreach($product->getAssemblies() as $assembly) {
            foreach($assembly->getParts() as $part) {
                if ($part->getParent() != null && $part->getMaterial() != null) {     
                    $material = $part->getMaterial();
                    $id = $material->getId();
                    
                    if($part->getParam1() != null) {
                        $val = $part->getParam1();
                    }

                    $newVal = $assembly->getQuantity() * $part->getParent()->getQuantity() * $val;
                    
                    $total_impact_RMD += $material->getRmd() * $newVal;
                    $total_impact_ED += $material->getEd() * $newVal;
                    $total_impact_WD += $material->getWd() * $newVal;
                    $total_impact_GWP += $material->getGwp() * $newVal;
                    $total_impact_ODP += $material->getOdp() * $newVal;
                    $total_impact_AT += $material->getAt() * $newVal;
                    $total_impact_POCP += $material->getPocp() * $newVal;
                    $total_impact_AA += $material->getAa() * $newVal;
                    $total_impact_WT += $material->getWt() * $newVal;
                    $total_impact_WE += $material->getWe() * $newVal;
                    $total_impact_HWP += $material->getHwp() * $newVal;

                }
                
                if($part->getParent() != null && $part->getProcess() != null){
                    $process = $part->getProcess();
                    $id = $process->getId();
                    
                    if ($part->getParam4() != null) {
                        $val = $part->getParam3() * $part->getParam4();
                    } else {
                        $val = $part->getParam3();
                    }
		
                    $weight = $assembly->getQuantity() * $part->getParent()->getQuantity() * $val;
                    
                    $total_impact_RMD += $process->getRmd() * $weight;
                    $total_impact_ED += $process->getEd() * $weight;
                    $total_impact_WD += $process->getWd() * $weight;
                    $total_impact_GWP += $process->getGwp() * $weight;
                    $total_impact_ODP += $process->getOdp() * $weight;
                    $total_impact_AT += $process->getAt() * $weight;
                    $total_impact_POCP += $process->getPocp() * $weight;
                    $total_impact_AA += $process->getAa() * $weight;
                    $total_impact_WT += $process->getWt() * $weight;
                    $total_impact_WE += $process->getWe() * $weight;
                    $total_impact_HWP += $process->getHwp() * $weight;
                }

            }
        }
        
        if($wastes == true){
            $m = $this->calculateWastes($product);
            $total_impact_RMD += $m[0];
            $total_impact_ED += $m[1];
            $total_impact_WD += $m[2];
            $total_impact_GWP += $m[3];
            $total_impact_ODP += $m[4];
            $total_impact_AT += $m[5];
            $total_impact_POCP += $m[6];
            $total_impact_AA += $m[7];
            $total_impact_WT += $m[8];
            $total_impact_WE += $m[9];
            $total_impact_HWP += $m[10];
        }
        
        $totals = array();
        $totals['RMD'] = $total_impact_RMD;
        $totals['ED'] = $total_impact_ED;
        $totals['WD'] = $total_impact_WD;
        $totals['GWP'] = $total_impact_GWP;
        $totals['ODP'] = $total_impact_ODP;
        $totals['AT'] = $total_impact_AT;
        $totals['POCP'] = $total_impact_POCP;
        $totals['AA'] = $total_impact_AA;
        $totals['WT'] = $total_impact_WT;
        $totals['WE'] = $total_impact_WE;
        $totals['HWP'] = $total_impact_HWP;
        
        return $totals;
    }
    
    /**
     * Pour calculer le tableau parts pour les simulations
     * @param type $product Le Product
     * @param type $withProcesses Pour prendre en compte ou non les process
     * @param type $onParents Pour cas particulier de simulation_parts.html.twig
     */
    public function calculatePartsForSimulation($product, $withProcesses, $onParents){
         $parts = array();
         $totalWeight = $product->getWeight();
         $wastes = false;
        
         $total_impact = 0;
         
         $impact_RMD = 0;
         $impact_ED = 0;
         $impact_WD = 0;
         $impact_GWP = 0;
         $impact_ODP = 0;
         $impact_AT = 0;
         $impact_POCP = 0;
         $impact_AA = 0;
         $impact_WT = 0;
         $impact_WE = 0;
         $impact_HWP = 0;
         
         //Cas de simulationPistes -- Si dans pistes, on compte les déchets, sinon non
         if($withProcesses == false && $onParents == true){
             $wastes = true;
         }
         
         //Cas de simulationMaterials
         if($withProcesses == false && $onParents == false){
             $totals = $this->calculateMaterialTotalsImpact($product);
         }else{
             $totals = $this->calculateTotalsImpacts($product, $wastes);
         }
        
         foreach($product->getAssemblies() as $assembly) {
            foreach($assembly->getParts() as $part) {
                if ($part->getParent() != null && $part->getMaterial() != null) {
                    $material = $part->getMaterial();
                    $id = $material->getId();
                    $val = $part->getParam1();
                    
                    $weight = $this->calculateWeight($part);
                    $newVal = $assembly->getQuantity() * $part->getParent()->getQuantity() * $val;
                    
                    $simulation = $this->em->getRepository('ArtoAcvBundle:SimulationMaterial')->findOneBy(array(
                            'product'  => $product->getId(),
                            'material' => $id
                        ));
                     
                    if($onParents == true){
                       $id = $part->getParent()->getId();
                    }
                   
                    if (!array_key_exists($id, $parts)) {
                        $total_impact = 0;
                        $impact_RMD = 0;
                        $impact_ED = 0;
                        $impact_WD = 0;
                        $impact_GWP = 0;
                        $impact_ODP = 0;
                        $impact_AT = 0;
                        $impact_POCP = 0;
                        $impact_AA = 0;
                        $impact_WT = 0;
                        $impact_WE = 0;
                        $impact_HWP = 0;
                        
                        $parts[$id] = $this->createNewSimulationPart($part, 'material', $simulation, $weight, $withProcesses, $onParents);
                    } else { 
                        $parts[$id]['weight'] = $parts[$id]['weight'] + $weight;
                        $parts[$id]['weight'] = round($parts[$id]['weight'], 2);
                    }

                    $impact_RMD = $material->getRmd() * $newVal;
                    $impact_ED = $material->getEd() * $newVal;
                    $impact_WD = $material->getWd() * $newVal;
                    $impact_GWP = $material->getGwp() * $newVal;
                    $impact_ODP = $material->getOdp() * $newVal;
                    $impact_AT = $material->getAt() * $newVal;
                    $impact_POCP = $material->getPocp() * $newVal;
                    $impact_AA = $material->getAa() * $newVal;
                    $impact_WT = $material->getWt() * $newVal;
                    $impact_WE = $material->getWe() * $newVal;
                    $impact_HWP = $material->getHwp() * $newVal;

                    $parts[$id]['impactRMD'] += $impact_RMD;
                    $parts[$id]['impactED'] += $impact_ED;
                    $parts[$id]['impactWD'] += $impact_WD;
                    $parts[$id]['impactGWP'] += $impact_GWP;
                    $parts[$id]['impactODP'] += $impact_ODP;
                    $parts[$id]['impactAT'] += $impact_AT;
                    $parts[$id]['impactPOCP'] += $impact_POCP;
                    $parts[$id]['impactAA'] += $impact_AA;
                    $parts[$id]['impactWT'] += $impact_WT;
                    $parts[$id]['impactWE'] += $impact_WE;
                    $parts[$id]['impactHWP'] += $impact_HWP;

                }
                
                
                if($withProcesses == true){
                    if ($part->getParent() != null && $part->getProcess() != null) {
                        $process = $part->getProcess();
                        $id = $process->getId();
                        
                        if ($part->getParam4() != null) {
                            $val = $part->getParam3() * $part->getParam4();
                        } else {
                            $val = $part->getParam3();
                        }

                        $weight = $assembly->getQuantity() * $part->getParent()->getQuantity() * $val;
                       
                        if($onParents == true){
                            $id = $part->getParent()->getId(); 
                            $weight = 0;
                        }
                        
                        if (!array_key_exists($id, $parts)) {
                            $total_impact = 0;
                            $impact_RMD = 0;
                            $impact_ED = 0;
                            $impact_WD = 0;
                            $impact_GWP = 0;
                            $impact_ODP = 0;
                            $impact_AT = 0;
                            $impact_POCP = 0;
                            $impact_AA = 0;
                            $impact_WT = 0;
                            $impact_WE = 0;
                            $impact_HWP = 0;
                              
                            $parts[$id] = $this->createNewSimulationPart($part, 'process', $simulation, $weight, $withProcesses, $onParents);
                        } else {
                            $parts[$id]['weight'] = $parts[$id]['weight'] + $weight;
                            $parts[$id]['weight'] = round($parts[$id]['weight'], 2);
                        }
                        
                        
                        $impact_RMD = $process->getRmd() * $val;
                        $impact_ED = $process->getEd() * $val;
                        $impact_WD = $process->getWd() * $val;
                        $impact_GWP = $process->getGwp() * $val;
                        $impact_ODP = $process->getOdp() * $val;
                        $impact_AT = $process->getAt() * $val;
                        $impact_POCP = $process->getPocp() * $val;
                        $impact_AA = $process->getAa() * $val;
                        $impact_WT = $process->getWt() * $val;
                        $impact_WE = $process->getWe() * $val;
                        $impact_HWP = $process->getHwp() * $val;
                       
                        $parts[$id]['impactRMD'] += $impact_RMD;
                        $parts[$id]['impactED'] += $impact_ED;
                        $parts[$id]['impactWD'] += $impact_WD;
                        $parts[$id]['impactGWP'] += $impact_GWP;
                        $parts[$id]['impactODP'] += $impact_ODP;
                        $parts[$id]['impactAT'] += $impact_AT;
                        $parts[$id]['impactPOCP'] += $impact_POCP;
                        $parts[$id]['impactAA'] += $impact_AA;
                        $parts[$id]['impactWT'] += $impact_WT;
                        $parts[$id]['impactWE'] += $impact_WE;
                        $parts[$id]['impactHWP'] += $impact_HWP;
                    }
                }

            }
        }
        
        //Cas simulationPistes, on rajoute les déchets de fabrication
        if($withProcesses == true && $onParents == false){
            $weightWaste = $this->calculateWeightWastes($product);
            $m = $this->calculateWastes($product);
            $parts[0] = $this->createWasteSimulationPart($product,$m, $weightWaste);
        }
        
        $parts = $this->calculateImpactsPartsSimulation($parts, $totals, $totalWeight);
        
        if($parts != null){
            $parts = $this->sortTwoDimensionArrayByKey($parts,'impact');
        }
        return $parts;
    }
    
    /**
     * Calcile les impacts des parts des simulations
     * @param type $parts
     * @param type $totals
     * @param type $totalWeight
     * @return type
     */
    public function calculateImpactsPartsSimulation($parts, $totals, $totalWeight){
        foreach($parts as $id => $part) {
            if($parts[$id]['name'] != "Déchets de fabrication"){
                $percent = ($parts[$id]['weight'] / $totalWeight) * 100;
                $parts[$id]['percent'] = round($percent, 2);

                $percentRMD = 0;
                $percentED = 0;
                $percentWD = 0;
                $percentGWP = 0;
                $percentODP = 0;
                $percentAT = 0;
                $percentPOCP = 0;
                $percentAA = 0;
                $percentWT = 0;
                $percentWE = 0;
                $percentHWP = 0;


                if($totals['RMD'] != 0){
                    $percentRMD = ($parts[$id]['impactRMD']/$totals['RMD'])*100;
                }

                if($totals['ED'] != 0){
                    $percentED = ($parts[$id]['impactED']/$totals['ED'])*100;
                }

                if($totals['WD'] != 0){
                    $percentWD = ($parts[$id]['impactWD']/$totals['WD'])*100; 
                }

                if($totals['GWP'] != 0){
                    $percentGWP = ($parts[$id]['impactGWP']/$totals['GWP'])*100;
                }

                if($totals['ODP'] != 0){
                    $percentODP = ($parts[$id]['impactODP']/$totals['ODP'])*100;
                }

                if($totals['AT'] != 0){
                    $percentAT = ($parts[$id]['impactAT']/$totals['AT'])*100; 
                }

                if($totals['POCP'] != 0){
                    $percentPOCP = ($parts[$id]['impactPOCP']/$totals['POCP'])*100; 
                }

                if($totals['AA'] != 0){
                    $percentAA = ($parts[$id]['impactAA']/$totals['AA'])*100;
                }

                if($totals['WT'] != 0){
                    $percentWT = ($parts[$id]['impactWT']/$totals['WT'])*100;   
                }

                if($totals['WE'] != 0){
                    $percentWE = ($parts[$id]['impactWE']/$totals['WE'])*100;  
                }

                if($totals['HWP'] != 0){
                    $percentHWP = ($parts[$id]['impactHWP']/$totals['HWP'])*100;  
                }

                $percentImpact = ($percentRMD + $percentED + $percentWD + $percentGWP + $percentODP + $percentAT
                        + $percentPOCP + $percentAA + $percentWT + $percentWE + $percentHWP)/11;

                $parts[$id]['impact'] = round($percentImpact, 2);
                
            }
        }
        
        return $parts;
    }
    
    /**
     * Calcule le poids d'une Part
     * @param \Arto\AcvBundle\Entity\Part $part
     * @return type
     */
    public function calculateWeight(Part $part){
         if($part->getParent() != null && $part->getMaterial() != null) {
            $material = $part->getMaterial();
           
            if($material->getUnit1() == "g") {
                if($part->getParam1() != null) {
                    $val = $part->getParam1();
                    $weight = $val;
                }
            } else if($material->getUnit1() == "UN") {
                if($material->getUnit2() == "g") {
                    if($part->getParam1() != null) {
                        $val = $part->getParam1();
                        $weight = $val * $material->getMultiplicateur();
                    }
                } else if($material->getUnit2() == "kg") {
                    if($part->getParam1() != null) {
                        $val = $part->getParam1();
                        $weight = ($val * $material->getMultiplicateur()) / 1000;
                    }
                } else {
                    if($part->getParam1() != null) {
                        $val = $part->getParam1();
                        $weight = $val * $material->getMultiplicateur();
                    }
                }
            } else {
                if($part->getParam1() != null) {
                    $val = $part->getParam1();
                    $weight = $val * $material->getMultiplicateur();
                }
            }
            $weight = $weight * $part->getParent()->getQuantity();        
        }
        
        return $weight;
    }
    
    /**
     * Calcule le poids des matériaux métalliques
     * @param type $product
     * @return type
     */
    public function calculateWeightMetallic($product){
        $weightMetallic = 0;
        foreach($product->getAssemblies() as $assembly) {
            foreach($assembly->getParts() as $part) {
                $assemblyName = $assembly->getName();
                if ($part->getParent() != null && $part->getMaterial() != null) {
                    $material = $part->getMaterial();
                    $materialFamily = $material->getFamily()->getId();
                    if($materialFamily == 1 && ($assemblyName != "Emballage" || $assemblyName != "EMBALLAGE" || $assemblyName != "Conditionnement" || $assemblyName != "CONDITIONNEMENT")){
                        $weightMetallic = $weightMetallic + $this->calculateWeight($part);
                    }
                } 
            }
        }
        
        return $weightMetallic;
    }
    
    /**
     * Calcule le % de masse des matériaux métalliques
     * @param type $product
     * @return type
     */
    public function calculatePercentMetallic($product){
        $percentMetallic = 0;
        $totalWeight = $product->getWeight();
        $weightMetallic = $this->calculateWeightMetallic($product);
        
        $percentMetallic = ($weightMetallic / $totalWeight) * 100;
        $percentMetallic = round($percentMetallic);
        
        return $percentMetallic;
    }
    
    /**
     * Calcule le poids des matériaux plastiques
     * @param type $product
     * @return type
     */
    public function calculateWeightPlastic($product){
        $weightPlastic = 0;
        foreach($product->getAssemblies() as $assembly) {
            foreach($assembly->getParts() as $part) {
                $assemblyName = $assembly->getName();
                if ($part->getParent() != null && $part->getMaterial() != null) {
                    $material = $part->getMaterial();
                    $materialFamily = $material->getFamily()->getId();
                    if($materialFamily == 3 && ($assemblyName != "Emballage" || $assemblyName != "EMBALLAGE" || $assemblyName != "Conditionnement" || $assemblyName != "CONDITIONNEMENT")){
                        $weightPlastic = $weightPlastic + $this->calculateWeight($part);
                    }
                } 
            }
        }
        
        return $weightPlastic;
    }
    
    /**
     * Calcule le % de masse des matériaux plastiques
     * @param type $product
     * @return type
     */
    public function calculatePercentPlastic($product){
        $percentPlastic = 0;
        $totalWeight = $product->getWeight();
        $weightPlastic = $this->calculateWeightPlastic($product);
        
        $percentPlastic = ($weightPlastic / $totalWeight) * 100;
        $percentPlastic = round($percentPlastic);
        
        return $percentPlastic;
    }
    
    /**
     * Calcule le poids des emballages
     * @param type $product
     * @return type
     */
    public function calculateWeightPackaging($product){
        $weightPackaging = 0;
        foreach($product->getAssemblies() as $assembly) {
            $assemblyName = $assembly->getName();
            if($assemblyName == "Emballage" || $assemblyName == "EMBALLAGE" || $assemblyName == "Conditionnement" || $assemblyName == "CONDITIONNEMENT"){
                foreach($assembly->getParts() as $part) {
                    if ($part->getParent() != null && $part->getMaterial() != null) {
                        $weightPackaging = $weightPackaging + $this->calculateWeight($part);    
                    } 
                }
            }
        }
        
        return $weightPackaging;
    }
    
    /**
     * Calcule le % de masse des matériaux d'emballage
     * @param type $product
     * @return type
     */
    public function calculatePercentPackaging($product){
        $percentPackaging = 0;
        $totalWeight = $product->getWeight();
        $weightPackaging = $this->calculateWeightPackaging($product);
        
        $percentPackaging = ($weightPackaging / $totalWeight) * 100;
        $percentPackaging = round($percentPackaging);
        
        return $percentPackaging;
    }
    
    /**
     * Calcule le poids des composants électroniques
     * @param type $product
     * @return type
     */
    public function calculateWeightElectronic($product){
        $weightElectronic = 0;
        foreach($product->getAssemblies() as $assembly) {
            foreach($assembly->getParts() as $part) {
                if ($part->getParent() != null && $part->getMaterial() != null) {
                    $material = $part->getMaterial();
                    $materialFamily = $material->getFamily()->getId();
                    if($materialFamily == 6){
                        $weightElectronic = $weightElectronic + $this->calculateWeight($part);
                    }    
                } 
            }
        }
        
        return $weightElectronic;
    }
    
    
    
    /**
     * Crée une part de Simulation
     * @param \Arto\AcvBundle\Entity\Part $part
     * @param type $type
     * @param type $simulation
     * @param int $weight
     * @param type $withProcesses
     * @param type $onParents
     * @return type
     */
    public function createNewSimulationPart(Part $part, $type, $simulation, $weight, $withProcesses, $onParents){
        $theArray = array();
        if($type == 'material') {
            $material = $part->getMaterial();
            $id = $material->getId();
            $name = '';
            $materialName = $material->getName();
            
            if($onParents == true) {
                $id = $part->getParent()->getId();
                $name = $part->getParent()->getName();
               
                if($withProcesses == true) {
                    $materialName = $name;
                } else {
                    $materialName = $material->getName();
                }
            }

            $pistes = $material->getPistes();
            if($pistes == null) {
                $pistes = 'sans';
            }

            $theArray = array(
                'id' => $id,
                'simulation' => $simulation,
                'material' => $part->getMaterial(),
                'name' => $materialName,
                'weight' => round($weight, 2),
                'impact' => 0,
                'impactRMD' => 0,
                'impactED' => 0,
                'impactWD' => 0,
                'impactGWP' => 0,
                'impactODP' => 0,
                'impactAT' => 0,
                'impactPOCP' => 0,
                'impactAA' => 0,
                'impactWT' => 0,
                'impactWE' => 0,
                'impactHWP' => 0,
                'pistes' => $pistes,
                'type' => 'M',
                'percentage1' => $part->getPercentage1(),
                'percentage2' => $part->getPercentage2(),
                'percentage3' => $part->getPercentage3()
            );
        }elseif($type == 'process'){
            $process = $part->getProcess();
            $id = $process->getId();

            $name = '';
            $processName = '';

            if ($onParents == true) {
                $id = $part->getParent()->getId();
                $name = $part->getParent()->getName();
                $processName = $name;
                $weight = 0;
            } else {
                $processName = $process->getName();
            }
            
            $pistes = $process->getPistes();
            if ($pistes == null) {
                $pistes = 'sans';
            }

            $theArray = array(
                'id' => $id,
                'name' => $processName,
                'weight' => round($weight, 2),
                'impact' => 0,
                'impactRMD' => 0,
                'impactED' => 0,
                'impactWD' => 0,
                'impactGWP' => 0,
                'impactODP' => 0,
                'impactAT' => 0,
                'impactPOCP' => 0,
                'impactAA' => 0,
                'impactWT' => 0,
                'impactWE' => 0,
                'impactHWP' => 0,
                'pistes' => $pistes,
                'type' => 'P',
                'percentage1' => $part->getPercentage1(),
                'percentage2' => $part->getPercentage2(),
                'percentage3' => $part->getPercentage3()
            );
        }
        
        return $theArray;
    }
    
    /**
     * Crée la part des déchets de fabrication dans les simulations
     * @param type $product
     * @param type $m
     * @param type $weight
     * @return int
     */
    public function createWasteSimulationPart($product,$m, $weight){
        $totals = $this->calculateTotalsImpacts($product, true);

        $percentRMD = 0;
        $percentED = 0;
        $percentWD = 0;
        $percentGWP = 0;
        $percentODP = 0;
        $percentAT = 0;
        $percentPOCP = 0;
        $percentAA = 0;
        $percentWT = 0;
        $percentWE = 0;
        $percentHWP = 0;


        if ($totals['RMD'] != 0) {
            $percentRMD = ($m[0]/ $totals['RMD']) * 100;
        }

        if ($totals['ED'] != 0) {
            $percentED = ($m[1] / $totals['ED']) * 100;
        }

        if ($totals['WD'] != 0) {
            $percentWD = ($m[2] / $totals['WD']) * 100;
        }

        if ($totals['GWP'] != 0) {
            $percentGWP = ($m[3] / $totals['GWP']) * 100;
        }

        if ($totals['ODP'] != 0) {
            $percentODP = ($m[4]/ $totals['ODP']) * 100;
        }

        if ($totals['AT'] != 0) {
            $percentAT = ($m[5] / $totals['AT']) * 100;
        }

        if ($totals['POCP'] != 0) {
            $percentPOCP = ($m[6] / $totals['POCP']) * 100;
        }

        if ($totals['AA'] != 0) {
            $percentAA = ($m[7]/ $totals['AA']) * 100;
        }

        if ($totals['WT'] != 0) {
            $percentWT = ($m[8] / $totals['WT']) * 100;
        }

        if ($totals['WE'] != 0) {
            $percentWE = ($m[9] / $totals['WE']) * 100;
        }

        if ($totals['HWP'] != 0) {
            $percentHWP = ($m[10] / $totals['HWP']) * 100;
        }

        $percentImpact = ($percentRMD + $percentED + $percentWD + $percentGWP + $percentODP + $percentAT + $percentPOCP + $percentAA + $percentWT + $percentWE + $percentHWP) / 11;

        $percentImpact = round($percentImpact, 2);
        
        $theArray = array(
                'id' => 0,
                'name' => "Déchets de fabrication",
                'weight' => round($weight, 2),
                'impact' => $percentImpact,
                'impactRMD' => $m[0],
                'impactED' => $m[1],
                'impactWD' => $m[2],
                'impactGWP' => $m[3],
                'impactODP' => $m[4],
                'impactAT' => $m[5],
                'impactPOCP' => $m[6],
                'impactAA' => $m[7],
                'impactWT' => $m[8],
                'impactWE' => $m[9],
                'impactHWP' => $m[10],
                'pistes' => "Diminuer les déchets et les rebuts dus aux procédés de fabrication",
                'type' => 'P'
            );
        
        return $theArray;
    }
    
    /**
     * Trie un tableau à 2 dimensions
     * @param type $arr
     * @param type $arrKey
     * @param type $sortOrder
     * @return type
     */
    private function sortTwoDimensionArrayByKey($arr, $arrKey, $sortOrder=SORT_DESC)
    {
        foreach ($arr as $key => $row){
            $key_arr[$key] = $row[$arrKey];
        }

        array_multisort($key_arr, $sortOrder, $arr);

    return $arr;

    }
    
    /**
     * Calcule les valeurs dans l'étiquette
     * @param \Arto\AcvBundle\Entity\Project $project
     * @param \Arto\AcvBundle\Entity\Product $product
     * @param string $siteArtogreen
     * @return type
     */
    public function calculateStickerDatas(Project $project, Product $product, $siteArtogreen){
        $em = $this->em;
        $pva = 1;
       
        if ($project->getPva() != null){
            $pva = $project->getPva();
        }

        $results = $this->calculateAll($project->getFirstProduct());
        
        if($project->getEnergyConsumer() == true){
            $usage = $project->getFirstProduct()->getUsage();
            if($usage != null){
                 $lifetime = $usage->getLifetime();
            }else{
                 $lifetime = 1;
            }
        }else{
            $lifetime = $project->getLifetime();
        }

        $co2 = 0;
        $water = 0;
        $power = 0;
        $recycle = 0;
        
        $newCo2 = 0;
        $newWater = 0;
        $newPower = 0;
        $newRecycle = 0;
        
        $co2_euros = 0;
        $water_euros = 0;
        $power_euros = 0;
        $recycle_euros = 0;
        
        if($lifetime != 0){
            // calculé au coût de 30€ la tonne de CO2
            // pas de division par 1000, on a des kg et on affiche des kg
            $co2 = (isset($results[3]) && $results[3] > 0) ? round($results[3], 2) : 0;
            $newCo2 = ($co2 * $pva)/$lifetime;
            // on divise par 1000 car on a des kg et on calcule le prix par tonne !
            $co2_euros = round(($newCo2/1000) * 30 ,2);
            
            $co2Pva1000 = ($co2 * 1000)/$lifetime;
            $co2EurosPva1000 = round(($co2Pva1000/1000) * 30 ,2);

            // calculé au coût de 3,40€ le m3 d'eau (dm3 > m3 => /1000)
            $water = (isset($results[2]) && $results[2] > 0) ? round(($results[2] / 1000), 3) : 0;
            $newWater = ($water * $pva)/$lifetime;
            $water_euros = round($newWater * 3.40 ,2);
            
            $waterPva1000 = ($water * 1000)/$lifetime;
            $waterEurosPva1000 = round($waterPva1000 * 3.40 ,2);

            // calculé au coût de 0,12€ le kWh d'électricité EDF (Mj > kWh => *0.28)
            $power = (isset($results[1]) && $results[1] > 0) ? round($results[1] * 0.28, 2) : 0;
            $newPower = ($power * $pva)/$lifetime;
            $power_euros = round($newPower * 0.12, 2);
            
            $powerPva1000 = ($power * 1000)/$lifetime;
            $powerEurosPva1000 = round($powerPva1000 * 0.12, 2);

            // calculé au coût de recyclage de 1400€ / tonne de piles (kg > tonne /1000)
            $recycle = (isset($results[10]) && $results[10] > 0) ? round($results[10], 2) : 0;
            $newRecycle = ($recycle * $pva)/$lifetime;
            $recycle_euros = round($newRecycle * 1.4, 2);
            
            $recyclePva1000 = ($recycle * 1000)/$lifetime;
            $recycleEurosPva1000 = round($recyclePva1000 * 1.4, 2);
            
            $project->setCo2Pva1000($co2EurosPva1000);
            $project->setWaterPva1000($waterEurosPva1000);
            $project->setPowerPva1000($powerEurosPva1000);
            $project->setRecyclePva1000($recycleEurosPva1000);
            
            $em->persist($project);
            $em->flush();
                    
        }    

        $params = array(
            'user'       => $project->getGreenUser(),
            'project'    => $project,
            'product'    => $product,
            'co2'        => $newCo2,
            'water'      => $newWater,
            'power'      => $newPower,
            'recycle'    => $newRecycle,
            'co2_euros'        => $co2_euros,
            'water_euros'      => $water_euros,
            'power_euros'      => $power_euros,
            'recycle_euros'    => $recycle_euros,
            'pva'              => $pva,
            'siteArtogreen' => $siteArtogreen
        );
        
        return $params;
    }
    /**
     * Pour calculer les coûts en Euros pour une PVA donnée !
     * @param \Arto\AcvBundle\Entity\Project $project
     * @param type $pva
     * @return type 
     */
    public function calculateEuroCosts(Project $project, $pva){
        $em = $this->em;
        
        $results = $this->calculateAll($project->getFirstProduct());
        
        /*$usage = $project->getFirstProduct()->getUsage();
        if($usage != null){
             $lifetime = $usage->getLifetime();
        }else{
             $lifetime = 1;
        }*/
        
        $lifetime = $project->getLifetime();

        $co2 = 0;
        $water = 0;
        $power = 0;
        $recycle = 0;
        
        $newCo2 = 0;
        $newWater = 0;
        $newPower = 0;
        $newRecycle = 0;
        
        $co2_euros = 0;
        $water_euros = 0;
        $power_euros = 0;
        $recycle_euros = 0;
        
        if($lifetime != 0){
            // calculé au coût de 30€ la tonne de CO2
            // pas de division par 1000, on a des kg et on affiche des kg
            $co2 = (isset($results[3]) && $results[3] > 0) ? round($results[3], 2) : 0;
            $newCo2 = ($co2 * $pva)/$lifetime;
            // on divise par 1000 car on a des kg et on calcule le prix par tonne !
            $co2_euros = round(($newCo2/1000) * 30 ,2);
            
            // calculé au coût de 3,40€ le m3 d'eau (dm3 > m3 => /1000)
            $water = (isset($results[2]) && $results[2] > 0) ? round(($results[2] / 1000), 2) : 0;
            $newWater = ($water * $pva)/$lifetime;
            $water_euros = round($newWater * 3.40 ,2);
            
            // calculé au coût de 0,12€ le kWh d'électricité EDF (Mj > kWh => *0.28)
            $power = (isset($results[1]) && $results[1] > 0) ? round($results[1] * 0.28, 2) : 0;
            $newPower = ($power * $pva)/$lifetime;
            $power_euros = round($newPower * 0.12, 2);
            
            // calculé au coût de recyclage de 1400€ / tonne de piles (kg > tonne /1000)
            $recycle = (isset($results[10]) && $results[10] > 0) ? round($results[10], 2) : 0;
            $newRecycle = ($recycle * $pva)/$lifetime;
            $recycle_euros = round($newRecycle * 1.4, 2);
        }    

        $params = array(
            'co2_euros'        => $co2_euros,
            'water_euros'      => $water_euros,
            'power_euros'      => $power_euros,
            'recycle_euros'    => $recycle_euros
        );
        
        return $params;
    }
    
    /**
     * Calcule la liste des pourcentages + ou - de 0 à 100%
     * @param type $sign détermine si pourcentages + ou -
     */
    public function calculatePercentages($sign){
        $percentages = array();
        if($sign == '-'){
            for($i=1;$i<=20;$i++) {
                $percentages[] = array(
                    'label' => '-'.$i*5,
                    'value' => abs(($i*5/100)-1)
                );
            }
        }elseif($sign == '+'){
            for($i=20;$i>=1;$i--) {
                $percentages[] = array(
                    'label' => '+'.$i*5,
                    'value' => abs(($i*5/100)+1)
                );
            }
        }
        
        return $percentages;
    }    
}
