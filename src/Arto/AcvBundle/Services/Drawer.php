<?php

namespace Arto\AcvBundle\Services;

use Arto\AcvBundle\Entity\Product;
use Arto\AcvBundle\Entity\Project;
use Arto\AcvBundle\Services\Calculator;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Xlab\pChartBundle\pData;
use Xlab\pChartBundle\pDraw;
use Xlab\pChartBundle\pRadar;
use Xlab\pChartBundle\pImage;
use Xlab\pChartBundle\pPie;

class Drawer{
    protected $calculator;
    
    public function __construct(Calculator $calculator) {
        $this->calculator = $calculator;
    }
    
    /**
     * Remplace les nombres négatifs par 0 dans un tableau
     * @param array $array
     * @return array
     */
    public function replaceNegativesByZero(array $array){
        $result = array(); 
        foreach ($array as $key => $value){
            if($value < 0){
               $result[$key] = 0;
            }else{
               $result[$key] = $array[$key];
            } 
        } 
        return $result; 
    }
   
    /**
     * Trace le graphe de la page Bilan
     * @param \Arto\AcvBundle\Entity\Product $product
     * @return type
     */
    public function drawChart1(Product $product){
        $calculator = $this->calculator;
        
        $categories = array('RR','NRJ', 'CE', 'GES', 'DO', 'AI', 'OP', 'AA', 'EI', 'E', 'DD');
         
        $m = $calculator->calculateMaterial($product);
        $p = $calculator->calculateProcess($product);
        $d = $calculator->calculateDelivery($product);
        $u = $calculator->calculateUsage($product);
        $i = $calculator->calculateInstall($product);
        $e = $calculator->calculateEol($product);
        
        $m = $this->replaceNegativesByZero($m);
        $p = $this->replaceNegativesByZero($p);
        $d = $this->replaceNegativesByZero($d);
        $u = $this->replaceNegativesByZero($u);
        $i = $this->replaceNegativesByZero($i);
        $e = $this->replaceNegativesByZero($e);
        
        //Avant
        $t = array_fill(0, 11, 0);
        $t[0] = $m[0] + $p[0] + $d[0] + $u[0] + $i[0] + $e[0];
        $t[1] = $m[1] + $p[1] + $d[1] + $u[1] + $i[1] + $e[1];
        $t[2] = $m[2] + $p[2] + $d[2] + $u[2] + $i[2] + $e[2];
        $t[3] = $m[3] + $p[3] + $d[3] + $u[3] + $i[3] + $e[3];
        $t[4] = $m[4] + $p[4] + $d[4] + $u[4] + $i[4] + $e[4];
        $t[5] = $m[5] + $p[5] + $d[5] + $u[5] + $i[5] + $e[5];
        $t[6] = $m[6] + $p[6] + $d[6] + $u[6] + $i[6] + $e[6];
        $t[7] = $m[7] + $p[7] + $d[7] + $u[7] + $i[7] + $e[7];
        $t[8] = $m[8] + $p[8] + $d[8] + $u[8] + $i[8] + $e[8];
        $t[9] = $m[9] + $p[9] + $d[9] + $u[9] + $i[9] + $e[9];
        $t[10] = $m[10] + $p[10] + $d[10] + $u[10] + $i[10] + $e[10];
        
        for($k=0;$k<=10;$k++) {
            if($t[$k] != 0){
                if($t[$k] < 0){
                    $t[$k] = abs($t[$k]);
                }
                
                if($m[$k] < 0){
                    $m[$k] = 0;
                }else{
                    $m[$k] = $m[$k]/$t[$k] * 100;
                    $m[$k] = round($m[$k], 2);  
                }
                
                if($p[$k] < 0){
                    $p[$k] = 0;
                }else{
                    $p[$k] = $p[$k]/$t[$k] * 100;
                    $p[$k] = round($p[$k], 2);
                }
                
                if($d[$k] < 0){
                    $d[$k] = 0;
                }else{
                    $d[$k] = $d[$k]/$t[$k] * 100;
                    $d[$k] = round($d[$k], 2); 
                }

                if($u[$k] < 0){
                    $u[$k] = 0;
                }else{
                    $u[$k] = $u[$k]/$t[$k] * 100;
                    $u[$k] = round($u[$k], 2); 
                }
                
                if($i[$k] < 0){
                    $i[$k] = 0;
                }else{
                    $i[$k] = $i[$k]/$t[$k] * 100;
                    $i[$k] = round($i[$k], 2);
                }
                
                if($e[$k] < 0){
                    $e[$k] = 0;
                }else{
                    $e[$k] = $e[$k]/$t[$k] * 100;
                    $e[$k] = round($e[$k], 2);
                }
            }else{
                $m[$k] =0;
                $p[$k] =0;
                $d[$k] =0;
                $u[$k] =0;
                $i[$k] =0;
                $e[$k] =0;
            }    
        }
        
        $font = __DIR__ . "/../../../Xlab/pChartBundle/Resources/fonts/calibri.ttf";

        $data = new pData();
        $data->addPoints($m, "Matière première");
        $data->addPoints($p, "Procédé");
        $data->addPoints($d, "Distribution");
        $data->addPoints($i, "Installation");
        $data->addPoints($u, "Utilisation");
        $data->addPoints($e, "Fin de vie");
        //$data->setAxisName(0,"Temperatures");
        $data->addPoints($categories, "Labels");
        $data->setSerieDescription("Labels", "Indices");
        $data->setAbscissa("Labels");
        $data->setAxisUnit(0, "%");
        //$data->normalize(100);

        /* Create the pChart object */
        $chart = new pImage(870, 320, $data);

        /* Overlay some gradient areas */
        $settings = array("StartR" => 255, "StartG" => 255, "StartB" => 255, "EndR" => 220, "EndG" => 220, "EndB" => 220, "Alpha" => 50);
        $chart->drawGradientArea(0, 0, 870, 320, DIRECTION_VERTICAL, $settings);
        $chart->drawGradientArea(0, 0, 870, 0, DIRECTION_VERTICAL, array("StartR" => 0, "StartG" => 0, "StartB" => 0, "EndR" => 50, "EndG" => 50, "EndB" => 50, "Alpha" => 100));

        /* Add a border to the picture */
        $chart->drawRectangle(0, 0, 869, 319, array("R" => 204, "G" => 204, "B" => 204));

        $chart->setFontProperties(array("FontName" => $font, "FontSize" => 8));

        /* Draw the scale and the 1st chart */
        $chart->setGraphArea(40, 30, 830, 280);
        $chart->drawFilledRectangle(40, 30, 830, 280, array("R" => 255, "G" => 255, "B" => 255, "Surrounding" => -200, "Alpha" => 10));

        //$AxisBoundaries = array(0=>array("Min"=>0,"Max"=>10000));
        //$ScaleSettings  = array("Mode"=>SCALE_MODE_MANUAL,"ManualScale"=>$AxisBoundaries,"DrawSubTicks"=>TRUE,"DrawArrows"=>TRUE,"ArrowSize"=>6);
        //$chart->drawScale($ScaleSettings);
        //$axisBoundaries = array(0=>array("Min"=>0,"Max"=>100));
        $chart->drawScale(array("DrawSubTicks" => TRUE, "Mode" => SCALE_MODE_FLOATING));
        $chart->setShadow(TRUE, array("X" => 1, "Y" => 1, "R" => 0, "G" => 0, "B" => 0, "Alpha" => 10));
        $chart->setFontProperties(array("FontName" => $font, "FontSize" => 8));
        $chart->drawStackedBarChart(array("DisplayValues" => FALSE, "DisplayColor" => DISPLAY_AUTO));
        $chart->setShadow(FALSE);

        /* Write the chart legend */
        $chart->drawLegend(230, 305, array("Style" => LEGEND_NOBORDER, "Mode" => LEGEND_HORIZONTAL));

        /* Capture output and return the response */
        ob_start();
        $chart->autoOutput();
        
        return ob_get_clean();

    }
    
    /**
     * Trace le graphe de la page Comparaison
     * @param \Arto\AcvBundle\Entity\Project $project
     * @param \Arto\AcvBundle\Entity\Project $comparedProject
     * @return type
     */
    public function drawComparisonChart(Project $project, Project $comparedProject){
        $calculator = $this->calculator;
        
        $categories = array('RR','NRJ', 'CE', 'GES', 'DO', 'AI', 'OP', 'AA', 'EI', 'E', 'DD');

        $base = $calculator->calculateAll($project->getFirstProduct());

        $tab = $calculator->calculateAll($comparedProject->getFirstProduct());

        for ($k = 0; $k <= 10; $k++) {
            if($base[$k] != 0){
                $tab[$k] = $tab[$k] / $base[$k] * 100;
                $tab[$k] = round($tab[$k], 2);
            }else{
                $tab[$k] = 0;
            }
        }

        $base = array_fill(0, 11, 100);

        $font = __DIR__."/../../../Xlab/pChartBundle/Resources/fonts/calibri.ttf";

        $data = new pData();
        $data->addPoints($base, $project->getName());

        $data->addPoints($tab, $comparedProject->getName());

        /* Define the absissa serie */
        $data->addPoints($categories,"Labels");
        $data->setAbscissa("Labels");

        /* Create the pChart object */
        $chart = new pImage(800,250,$data);

        /* Overlay some gradient areas */
        $settings = array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>220, "EndG"=>220, "EndB"=>220, "Alpha"=>50);
        $chart->drawGradientArea(0,0,800,350,DIRECTION_VERTICAL,$settings);
        $chart->drawGradientArea(0,0,800,0,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>100));

        /* Add a border to the picture */
        $chart->drawRectangle(0,0,799,249,array("R"=>204,"G"=>204,"B"=>204));

        /* Set the default font properties */
        $chart->setFontProperties(array(
            "FontName"=>$font,
            "FontSize"=>9,
            "R"=>80,
            "G"=>80,
            "B"=>80
        ));

        /* Enable shadow computing */
        $chart->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));

        /* Create the pRadar object */
        $splitChart = new pRadar();

        /* Draw a radar chart */
        $chart->setGraphArea(20,20,780,230);
        $options = array(
            "Layout"=>RADAR_LAYOUT_STAR,
            "LabelPos"=>RADAR_LABELS_HORIZONTAL,
            "BackgroundGradient"=>array("StartR"=>255,"StartG"=>255,"StartB"=>255,"StartAlpha"=>30,"EndR"=>207,"EndG"=>227,"EndB"=>125,"EndAlpha"=>10),
            "FontName"=>$font,
            "FontSize"=>9,
            "LabelPadding"=>'8px',
            "AxisRotation" => '270'
        );

        $splitChart->drawRadar($chart,$data,$options);

        /* Write the chart legend */
        $chart->setFontProperties(array("FontName"=>$font,"FontSize"=>9));
        $chart->drawLegend(30,20,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_VERTICAL));

        /* Capture output and return the response */
        ob_start();
        $chart->autoOutput();
        
        return ob_get_clean();
    }
    
    /**
     * Trace le radar dans la page Simulation
     * @param \Arto\AcvBundle\Entity\Project $project
     * @param \Arto\AcvBundle\Entity\Product $product
     * @param type $type
     * @return type
     */
    public function drawSimulationChart(Project $project, Product $product, $type){
        $calculator = $this->calculator;
        
        $categories = array('RR','NRJ', 'CE', 'GES', 'DO', 'AI', 'OP', 'AA', 'EI', 'E', 'DD');
        
        $base = $calculator->calculateMaterial($product);
        
        if($type == 'materials'){
            $tabs[0] = $calculator->calculateSimulationMaterials($product, 1);
            $tabs[1] = $calculator->calculateSimulationMaterials($product, 2);
            $tabs[2] = $calculator->calculateSimulationMaterials($product, 3);
        }elseif($type == 'parts'){
            $tabs[0] = $calculator->calculateSimulationParts($product, 1);
            $tabs[1] = $calculator->calculateSimulationParts($product, 2);
            $tabs[2] = $calculator->calculateSimulationParts($product, 3);
        }
        
        foreach($tabs as $key => $tab) {
            for($k=0;$k<=10;$k++) {
                if($base[$k] != 0){
                    $tabs[$key][$k] = $tabs[$key][$k]/$base[$k] * 100;
                    $tabs[$key][$k] = round($tabs[$key][$k], 2);
                }else{
                    $tabs[$key][$k] = 100;
                }
            }
        }
        
        $base = array_fill(0, 11, 100);

        $font = __DIR__."/../../../Xlab/pChartBundle/Resources/fonts/calibri.ttf";

        $data = new pData();
        $data->addPoints($base, $project->getName());

        foreach($tabs as $key => $tab) {
            $data->addPoints($tab, 'Simulation '. ($key+1));
        }

        /* Define the absissa serie */
        $data->addPoints($categories,"Labels");
        $data->setAbscissa("Labels");

        /* Create the pChart object */
        $chart = new pImage(300,250,$data);

        /* Overlay some gradient areas */
        $settings = array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>220, "EndG"=>220, "EndB"=>220, "Alpha"=>50);
        $chart->drawGradientArea(0,0,300,350,DIRECTION_VERTICAL,$settings);
        $chart->drawGradientArea(0,0,300,0,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>100));

        /* Add a border to the picture */
        $chart->drawRectangle(0,0,299,249,array("R"=>204,"G"=>204,"B"=>204));

        /* Set the default font properties */
        $chart->setFontProperties(array(
            "FontName"=>$font,
            "FontSize"=>9,
            "R"=>80,
            "G"=>80,
            "B"=>80
        ));

        /* Enable shadow computing */
        $chart->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));

        /* Create the pRadar object */
        $splitChart = new pRadar();

        /* Draw a radar chart */
        $chart->setGraphArea(20,20,280,230);
        $options = array(
            "Layout"=>RADAR_LAYOUT_STAR,
            "LabelPos"=>RADAR_LABELS_HORIZONTAL,
            "BackgroundGradient"=>array("StartR"=>255,"StartG"=>255,"StartB"=>255,"StartAlpha"=>30,"EndR"=>207,"EndG"=>227,"EndB"=>125,"EndAlpha"=>10),
            "FontName"=>$font,
            "FontSize"=>9,
            "LabelPadding"=>'8px',
            "AxisRotation" => '270'
        );

        $splitChart->drawRadar($chart,$data,$options);

        /* Write the chart legend */
        //$chart->setFontProperties(array("FontName"=>$font,"FontSize"=>9));
        //$chart->drawLegend(30,20,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_VERTICAL));

        /* Capture output and return the response */
        ob_start();
        $chart->autoOutput();
        
        return ob_get_clean();
    }
    
    /**
     * Trace le camembert dans la page Fin de vie
     * @param type $percentMetallic
     * @param type $percentPlastic
     * @param type $percentPackaging
     * @param type $percentDiverse
     * @return type
     */
    public function drawRepartitionChart($percentMetallic, $percentPlastic, $percentPackaging, $percentDiverse){
        $font = __DIR__."/../../../Xlab/pChartBundle/Resources/fonts/calibri.ttf";
        
        /* Create and populate the pData object */
        $MyData = new pData();
        $MyData->addPoints(array($percentMetallic, $percentPlastic, $percentDiverse, $percentPackaging), "ScoreA");
        $MyData->setSerieDescription("ScoreA", "Application A");

        /* Define the absissa serie */
        $MyData->addPoints(array("% Materiaux métalliques", "% Matériaux plastiques", "% Divers matériaux", "% Matériaux d'emballage"), "Labels");
        $MyData->setAbscissa("Labels");

        /* Create the pChart object */
        $myPicture = new pImage(300, 260, $MyData, TRUE);

        /* Overlay with a gradient */
        $Settings = array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>220, "EndG"=>220, "EndB"=>220, "Alpha"=>50);
        $myPicture->drawGradientArea(0, 0, 300, 260, DIRECTION_VERTICAL, $Settings);
        $myPicture->drawGradientArea(0,0,300,0,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>100));

        /* Add a border to the picture */
        $myPicture->drawRectangle(0, 0, 299, 259, array("R" => 0, "G" => 0, "B" => 0));

        /* Write the picture title */
        $myPicture->setFontProperties(array("FontName" => $font, "FontSize" => 6));
        $myPicture->drawText(10, 13, "Répartition masses matériaux", array("R" => 255, "G" => 255, "B" => 255));

        /* Set the default font properties */
        $myPicture->setFontProperties(array("FontName" => $font, "FontSize" => 10, "R" => 80, "G" => 80, "B" => 80));

        /* Create the pPie object */
        $PieChart = new pPie($myPicture, $MyData);

        /* Draw an AA pie chart */
        $PieChart->draw2DPie(90, 140, array("SecondPass"=>FALSE));

        /* Write the legend box */
        $myPicture->setShadow(FALSE);
        $PieChart->drawPieLegend(140, 40, array("Alpha" => 20));

         /* Capture output and return the response */
        ob_start();
        $myPicture->autoOutput();
        
        return ob_get_clean();
    }

}
