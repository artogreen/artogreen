<?php
namespace Arto\AcvBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Arto\AcvBundle\Repository\BaseRepository")
 * @ORM\Table(name="acv_base")
 * @ORM\HasLifecycleCallbacks
 */
class Base
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(name="description", type="string", length=2000, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(name="code", type="string", length=20, nullable=false)
     */
    private $code;

    /**
     * @ORM\Column(name="unit1", type="string", length=10, nullable=true)
     */
    private $unit1;

    /**
     * @ORM\Column(name="unit2", type="string", length=10, nullable=true)
     */
    private $unit2;

    /**
     * @ORM\Column(name="unit_mass", type="string", length=50, nullable=true)
     */
    private $unitMass;

    /**
     * @ORM\Column(name="module_code", type="string", length=2, nullable=true)
     */
    private $moduleCode;

    /**
     * @ORM\Column(name="aa", type="decimal", nullable=true, precision=60, scale=30)
     */
    private $aa;

    /**
     * @ORM\Column(name="at", type="decimal", nullable=true, precision=60, scale=30)
     */
    private $at;

    /**
     * @ORM\Column(name="ed", type="decimal", nullable=true, precision=60, scale=30)
     */
    private $ed;

    /**
     * @ORM\Column(name="gwp", type="decimal", nullable=true, precision=60, scale=30)
     */
    private $gwp;

    /**
     * @ORM\Column(name="hwp", type="decimal", nullable=true, precision=60, scale=30)
     */
    private $hwp;

    /**
     * @ORM\Column(name="odp", type="decimal", nullable=true, precision=60, scale=30)
     */
    private $odp;

    /**
     * @ORM\Column(name="pocp", type="decimal", nullable=true, precision=60, scale=30)
     */
    private $pocp;

    /**
     * @ORM\Column(name="rr", type="decimal", nullable=true, precision=60, scale=30)
     */
    private $rmd;

    /**
     * @ORM\Column(name="wd", type="decimal", nullable=true, precision=60, scale=30)
     */
    private $wd;

    /**
     * @ORM\Column(name="we", type="decimal", nullable=true, precision=60, scale=30)
     */
    private $we;

    /**
     * @ORM\Column(name="wt", type="decimal", nullable=true, precision=60, scale=30)
     */
    private $wt;
    
    /**
     * @ORM\Column(name="pistes", type="text", nullable=true, length=1000)
     */
    private $pistes;
    
    /**
     * @ORM\Column(name="tauxRecyclabilité", type="decimal", nullable=true, precision=60, scale=2)
     */
    private $tauxRecyclabilite;
    
    /**
     * @ORM\Column(name="multiplicateur", type="decimal", nullable=true, precision=60, scale=4)
     */
    private $multiplicateur;
    
    /**
     * @ORM\Column(name="valParam2ByUN", type="decimal", nullable=true, precision=60, scale=3)
     */
    private $valParam2ByUN;
    
    /**
     * @ORM\Column(name="isVisible", type="string", length=6, nullable=true)
     */
    private $isVisible;
    
    /**
     * @ORM\Column(name="isShown", type="string", length=6, nullable=true)
     */
    private $isShown;


    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Type
     *
     * @ORM\ManyToOne(targetEntity="Type", inversedBy="bases")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     * })
     */
    private $type;

    /**
     * @var Family
     *
     * @ORM\ManyToOne(targetEntity="Family", inversedBy="bases")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="family_id", referencedColumnName="id")
     * })
     */
    private $family;



    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getName();
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set code
     *
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set unit1
     *
     * @param string $unit1
     */
    public function setUnit1($unit1)
    {
        $this->unit1 = $unit1;
    }

    /**
     * Get unit1
     *
     * @return string
     */
    public function getUnit1()
    {
        return $this->unit1;
    }

    /**
     * Set unit2
     *
     * @param string $unit2
     */
    public function setUnit2($unit2)
    {
        $this->unit2 = $unit2;
    }

    /**
     * Get unit2
     *
     * @return string
     */
    public function getUnit2()
    {
        return $this->unit2;
    }

    /**
     * Set aa
     *
     * @param decimal $aa
     */
    public function setAa($aa)
    {
        $this->aa = $aa;
    }

    /**
     * Get aa
     *
     * @return decimal
     */
    public function getAa()
    {
        return $this->aa;
    }

    /**
     * Set at
     *
     * @param decimal $at
     */
    public function setAt($at)
    {
        $this->at = $at;
    }

    /**
     * Get at
     *
     * @return decimal
     */
    public function getAt()
    {
        return $this->at;
    }

    /**
     * Set ed
     *
     * @param decimal $ed
     */
    public function setEd($ed)
    {
        $this->ed = $ed;
    }

    /**
     * Get ed
     *
     * @return decimal
     */
    public function getEd()
    {
        return $this->ed;
    }

    /**
     * Set gwp
     *
     * @param decimal $gwp
     */
    public function setGwp($gwp)
    {
        $this->gwp = $gwp;
    }

    /**
     * Get gwp
     *
     * @return decimal
     */
    public function getGwp()
    {
        return $this->gwp;
    }

    /**
     * Set hwp
     *
     * @param decimal $hwp
     */
    public function setHwp($hwp)
    {
        $this->hwp = $hwp;
    }

    /**
     * Get hwp
     *
     * @return decimal
     */
    public function getHwp()
    {
        return $this->hwp;
    }

    /**
     * Set odp
     *
     * @param decimal $odp
     */
    public function setOdp($odp)
    {
        $this->odp = $odp;
    }

    /**
     * Get odp
     *
     * @return decimal
     */
    public function getOdp()
    {
        return $this->odp;
    }

    /**
     * Set pocp
     *
     * @param decimal $pocp
     */
    public function setPocp($pocp)
    {
        $this->pocp = $pocp;
    }

    /**
     * Get pocp
     *
     * @return decimal
     */
    public function getPocp()
    {
        return $this->pocp;
    }

    /**
     * Set rmd
     *
     * @param decimal $rmd
     */
    public function setRmd($rmd)
    {
        $this->rmd = $rmd;
    }

    /**
     * Get rmd
     *
     * @return decimal
     */
    public function getRmd()
    {
        return $this->rmd;
    }

    /**
     * Set wd
     *
     * @param decimal $wd
     */
    public function setWd($wd)
    {
        $this->wd = $wd;
    }

    /**
     * Get wd
     *
     * @return decimal
     */
    public function getWd()
    {
        return $this->wd;
    }

    /**
     * Set we
     *
     * @param decimal $we
     */
    public function setWe($we)
    {
        $this->we = $we;
    }

    /**
     * Get we
     *
     * @return decimal
     */
    public function getWe()
    {
        return $this->we;
    }

    /**
     * Set wt
     *
     * @param decimal $wt
     */
    public function setWt($wt)
    {
        $this->wt = $wt;
    }

    /**
     * Get wt
     *
     * @return decimal
     */
    public function getWt()
    {
        return $this->wt;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set type
     *
     * @param Arto\AcvBundle\Entity\Type $type
     */
    public function setType(\Arto\AcvBundle\Entity\Type $type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return Arto\AcvBundle\Entity\Type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set family
     *
     * @param Arto\AcvBundle\Entity\Family $family
     */
    public function setFamily(\Arto\AcvBundle\Entity\Family $family)
    {
        $this->family = $family;
    }

    /**
     * Get family
     *
     * @return Arto\AcvBundle\Entity\Family
     */
    public function getFamily()
    {
        return $this->family;
    }

    /**
     * Set unitMass
     *
     * @param string $unitMass
     */
    public function setUnitMass($unitMass)
    {
        $this->unitMass = $unitMass;
    }

    /**
     * Get unitMass
     *
     * @return string
     */
    public function getUnitMass()
    {
        return $this->unitMass;
    }

    /**
     * Set moduleCode
     *
     * @param string $moduleCode
     */
    public function setModuleCode($moduleCode)
    {
        $this->moduleCode = $moduleCode;
    }

    /**
     * Get moduleCode
     *
     * @return string
     */
    public function getModuleCode()
    {
        return $this->moduleCode;
    }

    /**
     * Set pistes
     *
     * @param text $pistes
     */
    public function setPistes($pistes)
    {
        $this->pistes = $pistes;
    }

    /**
     * Get pistes
     *
     * @return text 
     */
    public function getPistes()
    {
        return $this->pistes;
    }

    /**
     * Set tauxRecyclabilite
     *
     * @param decimal $tauxRecyclabilite
     */
    public function setTauxRecyclabilite($tauxRecyclabilite)
    {
        $this->tauxRecyclabilite = $tauxRecyclabilite;
    }

    /**
     * Get tauxRecyclabilite
     *
     * @return decimal 
     */
    public function getTauxRecyclabilite()
    {
        return $this->tauxRecyclabilite;
    }

    /**
     * Set multiplicateur
     *
     * @param decimal $multiplicateur
     */
    public function setMultiplicateur($multiplicateur)
    {
        $this->multiplicateur = $multiplicateur;
    }

    /**
     * Get multiplicateur
     *
     * @return decimal 
     */
    public function getMultiplicateur()
    {
        return $this->multiplicateur;
    }

    /**
     * Set valParam2ByUN
     *
     * @param decimal $valParam2ByUN
     */
    public function setValParam2ByUN($valParam2ByUN)
    {
        $this->valParam2ByUN = $valParam2ByUN;
    }

    /**
     * Get valParam2ByUN
     *
     * @return decimal 
     */
    public function getValParam2ByUN()
    {
        return $this->valParam2ByUN;
    }

    /**
     * Set isVisible
     *
     * @param string $isVisible
     */
    public function setIsVisible($isVisible)
    {
        $this->isVisible = $isVisible;
    }

    /**
     * Get isVisible
     *
     * @return string 
     */
    public function getIsVisible()
    {
        return $this->isVisible;
    }
    
    /**
     * Set isShown
     *
     * @param string $isShown
     */
    public function setIsShown($isShown)
    {
        $this->isShown = $isShown;
    }

    /**
     * Get isShown
     *
     * @return string 
     */
    public function getIsShown()
    {
        return $this->isShown;
    }
}