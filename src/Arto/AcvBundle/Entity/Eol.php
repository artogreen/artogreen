<?php
namespace Arto\AcvBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="acv_eol")
 */
class Eol
{
    const TYPE_PCR = 1;
    const TYPE_ECO = 2;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="type", type="integer", nullable=false)
     */
    private $type;

    /**
     * @ORM\Column(name="collecte_distance", type="integer", nullable=true)
     */
    private $collecteDistance;

    /**
     * @ORM\Column(name="collecte_weight", type="integer", nullable=true)
     */
    private $collecteWeight;

    /**
     * @ORM\Column(name="depollution_distance", type="integer", nullable=true)
     */
    private $depollutionDistance;

    /**
     * @ORM\Column(name="depollution_weight", type="integer", nullable=true)
     */
    private $depollutionWeight;

    /**
     * @ORM\Column(name="incineration_distance", type="integer", nullable=true)
     */
    private $incinerationDistance;

    /**
     * @ORM\Column(name="incineration_weight", type="integer", nullable=true)
     */
    private $incinerationWeight;

    /**
     * @ORM\Column(name="dechet_distance", type="integer", nullable=true)
     */
    private $dechetDistance;

    /**
     * @ORM\Column(name="dechet_weight", type="integer", nullable=true)
     */
    private $dechetWeight;

    /**
     * @ORM\Column(name="impact_valorisation", type="integer", nullable=true)
     */
    private $impactValorisation;

    /**
     * @ORM\Column(name="impact_decharge", type="integer", nullable=true)
     */
    private $impactDecharge;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @ORM\OneToOne(targetEntity="Product", inversedBy="eol")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;

    /**
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Base")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="collect_transport_id", referencedColumnName="id")
     * })
     */
    private $collecteTransport;

    /**
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Base")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="depollution_transport_id", referencedColumnName="id")
     * })
     */
    private $depollutionTransport;

    /**
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Base")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="incineration_transport_id", referencedColumnName="id")
     * })
     */
    private $incinerationTransport;

    /**
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Base")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dechet_transport_id", referencedColumnName="id")
     * })
     */
    private $dechetTransport;


    public function __construct()
    {

    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set type
     *
     * @param integer $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set collecteDistance
     *
     * @param integer $collecteDistance
     */
    public function setCollecteDistance($collecteDistance)
    {
        $this->collecteDistance = $collecteDistance;
    }

    /**
     * Get collecteDistance
     *
     * @return integer
     */
    public function getCollecteDistance()
    {
        return $this->collecteDistance;
    }

    /**
     * Set collecteWeight
     *
     * @param integer $collecteWeight
     */
    public function setCollecteWeight($collecteWeight)
    {
        $this->collecteWeight = $collecteWeight;
    }

    /**
     * Get collecteWeight
     *
     * @return integer
     */
    public function getCollecteWeight()
    {
        return $this->collecteWeight;
    }

    /**
     * Set depollutionDistance
     *
     * @param integer $depollutionDistance
     */
    public function setDepollutionDistance($depollutionDistance)
    {
        $this->depollutionDistance = $depollutionDistance;
    }

    /**
     * Get depollutionDistance
     *
     * @return integer
     */
    public function getDepollutionDistance()
    {
        return $this->depollutionDistance;
    }

    /**
     * Set depollutionWeight
     *
     * @param integer $depollutionWeight
     */
    public function setDepollutionWeight($depollutionWeight)
    {
        $this->depollutionWeight = $depollutionWeight;
    }

    /**
     * Get depollutionWeight
     *
     * @return integer
     */
    public function getDepollutionWeight()
    {
        return $this->depollutionWeight;
    }

    /**
     * Set incinerationDistance
     *
     * @param integer $incinerationDistance
     */
    public function setIncinerationDistance($incinerationDistance)
    {
        $this->incinerationDistance = $incinerationDistance;
    }

    /**
     * Get incinerationDistance
     *
     * @return integer
     */
    public function getIncinerationDistance()
    {
        return $this->incinerationDistance;
    }

    /**
     * Set incinerationWeight
     *
     * @param integer $incinerationWeight
     */
    public function setIncinerationWeight($incinerationWeight)
    {
        $this->incinerationWeight = $incinerationWeight;
    }

    /**
     * Get incinerationWeight
     *
     * @return integer
     */
    public function getIncinerationWeight()
    {
        return $this->incinerationWeight;
    }

    /**
     * Set dechetDistance
     *
     * @param integer $dechetDistance
     */
    public function setDechetDistance($dechetDistance)
    {
        $this->dechetDistance = $dechetDistance;
    }

    /**
     * Get dechetDistance
     *
     * @return integer
     */
    public function getDechetDistance()
    {
        return $this->dechetDistance;
    }

    /**
     * Set dechetWeight
     *
     * @param integer $dechetWeight
     */
    public function setDechetWeight($dechetWeight)
    {
        $this->dechetWeight = $dechetWeight;
    }

    /**
     * Get dechetWeight
     *
     * @return integer
     */
    public function getDechetWeight()
    {
        return $this->dechetWeight;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set product
     *
     * @param Arto\AcvBundle\Entity\Product $product
     */
    public function setProduct(\Arto\AcvBundle\Entity\Product $product)
    {
        $this->product = $product;
    }

    /**
     * Get product
     *
     * @return Arto\AcvBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set collecteTransport
     *
     * @param Arto\AcvBundle\Entity\Base $collecteTransport
     */
    public function setCollecteTransport(\Arto\AcvBundle\Entity\Base $collecteTransport)
    {
        $this->collecteTransport = $collecteTransport;
    }

    /**
     * Get collecteTransport
     *
     * @return Arto\AcvBundle\Entity\Base
     */
    public function getCollecteTransport()
    {
        return $this->collecteTransport;
    }

    /**
     * Set depollutionTransport
     *
     * @param Arto\AcvBundle\Entity\Base $depollutionTransport
     */
    public function setDepollutionTransport(\Arto\AcvBundle\Entity\Base $depollutionTransport)
    {
        $this->depollutionTransport = $depollutionTransport;
    }

    /**
     * Get depollutionTransport
     *
     * @return Arto\AcvBundle\Entity\Base
     */
    public function getDepollutionTransport()
    {
        return $this->depollutionTransport;
    }

    /**
     * Set incinerationTransport
     *
     * @param Arto\AcvBundle\Entity\Base $incinerationTransport
     */
    public function setIncinerationTransport(\Arto\AcvBundle\Entity\Base $incinerationTransport)
    {
        $this->incinerationTransport = $incinerationTransport;
    }

    /**
     * Get incinerationTransport
     *
     * @return Arto\AcvBundle\Entity\Base
     */
    public function getIncinerationTransport()
    {
        return $this->incinerationTransport;
    }

    /**
     * Set dechetTransport
     *
     * @param Arto\AcvBundle\Entity\Base $dechetTransport
     */
    public function setDechetTransport(\Arto\AcvBundle\Entity\Base $dechetTransport)
    {
        $this->dechetTransport = $dechetTransport;
    }

    /**
     * Get dechetTransport
     *
     * @return Arto\AcvBundle\Entity\Base
     */
    public function getDechetTransport()
    {
        return $this->dechetTransport;
    }

    /**
     * Set impactValorisation
     *
     * @param integer $impactValorisation
     */
    public function setImpactValorisation($impactValorisation)
    {
        $this->impactValorisation = $impactValorisation;
    }

    /**
     * Get impactValorisation
     *
     * @return integer
     */
    public function getImpactValorisation()
    {
        return $this->impactValorisation;
    }

    /**
     * Set impactDecharge
     *
     * @param integer $impactDecharge
     */
    public function setImpactDecharge($impactDecharge)
    {
        $this->impactDecharge = $impactDecharge;
    }

    /**
     * Get impactDecharge
     *
     * @return integer
     */
    public function getImpactDecharge()
    {
        return $this->impactDecharge;
    }
}