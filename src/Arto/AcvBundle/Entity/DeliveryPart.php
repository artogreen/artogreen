<?php
namespace Arto\AcvBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="acv_delivery_part")
 */
class DeliveryPart
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="distance", type="integer", nullable=true)
     */
    private $distance;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Base")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="transport_id", referencedColumnName="id")
     * })
     */
    private $transport;

    /**
     * @var Delivery
     *
     * @ORM\ManyToOne(targetEntity="Delivery", inversedBy="parts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="delivery_id", referencedColumnName="id")
     * })
     */
    private $delivery;



    public function __construct()
    {

    }




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set distance
     *
     * @param integer $distance
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;
    }

    /**
     * Get distance
     *
     * @return integer
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set transport
     *
     * @param Arto\AcvBundle\Entity\Base $transport
     */
    public function setTransport(\Arto\AcvBundle\Entity\Base $transport)
    {
        $this->transport = $transport;
    }

    /**
     * Get transport
     *
     * @return Arto\AcvBundle\Entity\Base
     */
    public function getTransport()
    {
        return $this->transport;
    }

    /**
     * Set delivery
     *
     * @param Arto\AcvBundle\Entity\Delivery $delivery
     */
    public function setDelivery(\Arto\AcvBundle\Entity\Delivery $delivery)
    {
        $this->delivery = $delivery;
    }

    /**
     * Get delivery
     *
     * @return Arto\AcvBundle\Entity\Delivery
     */
    public function getDelivery()
    {
        return $this->delivery;
    }
}