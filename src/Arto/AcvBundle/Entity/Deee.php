<?php
namespace Arto\AcvBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="acv_deee")
 * @ORM\HasLifecycleCallbacks
 */
class Deee
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=130, nullable=false)
     */
    private $name;
    
    /**
     * @ORM\Column(name="isDeee", type="string", length=130, nullable=false)
     */
    private $isDeee;
    
    /**
     * @ORM\Column(name="isHazardous", type="string", length=130, nullable=false)
     */
    private $isHazardous;
       
    /**
     * @ORM\Column(name="tauxRecyclabilité", type="decimal", nullable=false, scale=2)
     */
    private $tauxRecyclabilité;
    
    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;


    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getName();
    }  

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isDeee
     *
     * @param string $isDeee
     */
    public function setIsDeee($isDeee)
    {
        $this->isDeee = $isDeee;
    }

    /**
     * Get isDeee
     *
     * @return string 
     */
    public function getIsDeee()
    {
        return $this->isDeee;
    }

    /**
     * Set isHazardous
     *
     * @param string $isHazardous
     */
    public function setIsHazardous($isHazardous)
    {
        $this->isHazardous = $isHazardous;
    }

    /**
     * Get isHazardous
     *
     * @return string 
     */
    public function getIsHazardous()
    {
        return $this->isHazardous;
    }

    /**
     * Set tauxRecyclabilité
     *
     * @param decimal $tauxRecyclabilité
     */
    public function setTauxRecyclabilité($tauxRecyclabilité)
    {
        $this->tauxRecyclabilité = $tauxRecyclabilité;
    }

    /**
     * Get tauxRecyclabilité
     *
     * @return decimal 
     */
    public function getTauxRecyclabilité()
    {
        return $this->tauxRecyclabilité;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}