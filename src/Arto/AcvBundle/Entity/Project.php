<?php
namespace Arto\AcvBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Arto\AcvBundle\Repository\ProjectRepository")
 * @ORM\Table(name="acv_project")
 * @ORM\HasLifecycleCallbacks
 */
class Project
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="uniq", type="string", length=32, nullable=false)
     */
    private $uniq;

    /**
     * @ORM\Column(name="ref", type="string", length=32, nullable=true)
     */
    private $ref;

    /**
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     */
    private $name;
    
    /**
     * @ORM\Column(name="projectName", type="string", length=100, nullable = true)
     */
    private $projectName;

    /**
     * @ORM\Column(name="description", type="string", length=600, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(name="descriptionuf", type="text", nullable=true)
     */
    private $descriptionuf;

    /**
     * @ORM\Column(name="pilote", type="string", length=255, nullable=true)
     */
    private $pilote;

    /**
     * @ORM\Column(name="pilote_date", type="date", nullable=true)
     */
    private $piloteDate;

    /**
     * @var string $path
     *
     * @ORM\Column(name="path", type="string", length=100, nullable=true)
     */
    private $path;

    /**
     * @Assert\File(maxSize="10000000")
     */
    public $file;

    /**
     * to get the image path (slug + extension)
     */
    private $image;

    /**
     * a property used temporarily while deleting
     */
    private $filenameForRemove;

    /**
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(name="weight", type="decimal", nullable=true, scale=2)
     */
    private $weight;
    
    /**
     * @ORM\Column(name="massUnit", type="string", length=2, nullable=true)
     */
    private $massUnit;

    /**
     * @ORM\Column(name="unit", type="string", length=2, nullable=true)
     */
    private $unit;

    /**
     * @ORM\Column(name="energy_consumer", type="boolean", nullable=true)
     */
    private $energyConsumer;

    /**
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @ORM\Column(name="transport_appro", type="integer", nullable=true)
     */
    private $transportAppro;

    /**
     * @ORM\Column(name="transport_stock_km", type="integer", nullable=true)
     */
    private $transportStockKm;

    /**
     * @ORM\Column(name="pva", type="integer", nullable=true)
     */
    private $pva;
    
    /**
     * @ORM\Column(name="lifetime", type="integer", nullable=true)
     */
    private $lifetime;
    
    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
    
    /**
     * @var Folder
     *
     * @ORM\ManyToOne(targetEntity="Folder", inversedBy="projects")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="folder_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $folder;
    
    /**
     * @ORM\OneToMany(targetEntity="Product", mappedBy="project", cascade={"remove", "persist"})
     */
    private $products;
    
    /**
     * @ORM\OneToMany(targetEntity="Operation", mappedBy="project", cascade={"remove", "persist"})
     */
    private $notes;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="childs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * })
     */
    private $parent;

    /**
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Base")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="transport_stock_id", referencedColumnName="id")
     * })
     */
    private $transportStock;

    /**
     * @ORM\OneToMany(targetEntity="Project", mappedBy="parent", cascade={"remove", "persist"})
     */
    private $childs;

    /**
     * @var GreenUser
     *
     * @ORM\ManyToOne(targetEntity="Arto\GreenBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="green_user_id", referencedColumnName="id")
     * })
     */
    private $greenUser;
    
    /**
     * @ORM\Column(name="co2Pva1000", type="decimal", nullable=true, precision=60, scale=2)
     */
    private $co2Pva1000;
    
    /**
     * @ORM\Column(name="waterPva1000", type="decimal", nullable=true, precision=60, scale=2)
     */
    private $waterPva1000;
    
    /**
     * @ORM\Column(name="powerPva1000", type="decimal", nullable=true, precision=60, scale=2)
     */
    private $powerPva1000;
    
    /**
     * @ORM\Column(name="recyclePva1000", type="decimal", nullable=true, precision=60, scale=2)
     */
    private $recyclePva1000;
    
    /**
     * @ORM\OneToMany(targetEntity="EolPart", mappedBy="project", cascade={"remove", "persist"})
     */
    private $eolParts;
    
    /**
     * @ORM\Column(name="comparedProject_id", type="integer", nullable=true)
     */
    private $comparedProjectId;


    public function __construct()
    {
        $this->uniq = md5( uniqid('artoacv', true) );
        //$this->energyConsumer = false;
        $this->pva = 1;
        $this->massUnit = 'g';
    }

    public function __toString()
    {
        return $this->getName();
    }

    /*
    public function __clone(){

        // Clone the relations Products
        if (count($this->getProducts()) > 0) {
            $clonedProducts = array();
            foreach($this->getProducts() as $product){
                $clonedProducts[] = clone $product;
            }

            // reinit the array of Values
            $this->products = new \Doctrine\Common\Collections\ArrayCollection();
            foreach($clonedProducts as $product) {
                $this->addProduct($product);
            }
        }
    }
    */



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Methods for upload
     */

    public function getAbsolutePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/project';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            $this->path = $this->file->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        $this->file->move($this->getUploadRootDir(), $this->getImage());

        unset($this->file);
    }

    /**
     * @ORM\PreRemove()
     */
    public function storeFilenameForRemove()
    {
        $this->filenameForRemove = $this->getAbsolutePath();
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($this->filenameForRemove) {
            unlink($this->filenameForRemove);
        }
    }

    /**
     * Set uniq
     *
     * @param string $uniq
     */
    public function setUniq($uniq)
    {
        $this->uniq = $uniq;
    }

    /**
     * Get uniq
     *
     * @return string
     */
    public function getUniq()
    {
        return $this->uniq;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set projectName
     *
     * @param string $projectName
     */
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;
    }

    /**
     * Get projectName
     *
     * @return string 
     */
    public function getProjectName()
    {
        return $this->projectName;
    }

    /**
     * Set weight
     *
     * @param decimal $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * Get weight
     *
     * @return decimal
     */
    public function getWeight()
    {
        return $this->weight;
    }
    
    /**
     * Set massUnit
     * 
     * @param string $massUnit
     */
    public function setMassUnit($massUnit)
    {
        $this->massUnit = $massUnit;
    }
    
    /**
     * Get massUnit
     * 
     * @return string
     */
    public function getMassUnit()
    {
        return $this->massUnit;
    }

    /**
     * Set unit
     *
     * @param string $unit
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
    }

    /**
     * Get unit
     *
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Set energyConsumer
     *
     * @param boolean $energyConsumer
     */
    public function setEnergyConsumer($energyConsumer)
    {
        $this->energyConsumer = $energyConsumer;
    }

    /**
     * Get energyConsumer
     *
     * @return boolean
     */
    public function getEnergyConsumer()
    {
        return $this->energyConsumer;
    }

    /**
     * Set country
     *
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set descriptionuf
     *
     * @param string $descriptionuf
     */
    public function setDescriptionuf($descriptionuf)
    {
        $this->descriptionuf = $descriptionuf;
    }

    /**
     * Get descriptionuf
     *
     * @return string
     */
    public function getDescriptionuf()
    {
        return $this->descriptionuf;
    }

    /**
     * Set pilote
     *
     * @param string $pilote
     */
    public function setPilote($pilote)
    {
        $this->pilote = $pilote;
    }

    /**
     * Get pilote
     *
     * @return string
     */
    public function getPilote()
    {
        return $this->pilote;
    }

    /**
     * Set piloteDate
     *
     * @param date $piloteDate
     */
    public function setPiloteDate($piloteDate)
    {
        $this->piloteDate = $piloteDate;
    }

    /**
     * Get piloteDate
     *
     * @return date
     */
    public function getPiloteDate()
    {
        return $this->piloteDate;
    }

    /**
     * Set path
     *
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set city
     *
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Add products
     *
     * @param Arto\AcvBundle\Entity\Product $products
     */
    public function addProduct(\Arto\AcvBundle\Entity\Product $products)
    {
        $this->products[] = $products;
    }

    /**
     * Get products
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Get first product
     *
     * @return Arto\AcvBundle\Entity\Product $product
     */
    public function getFirstProduct()
    {
        if (count($this->getProducts()) > 0) {
            $product = $this->getProducts();
            return $product[0];
        } else {
            return null;
        }

    }

    /**
     * Set ref
     *
     * @param string $ref
     */
    public function setRef($ref)
    {
        $this->ref = $ref;
    }

    /**
     * Get ref
     *
     * @return string
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * Set transportAppro
     *
     * @param integer $transportAppro
     */
    public function setTransportAppro($transportAppro)
    {
        $this->transportAppro = $transportAppro;
    }

    /**
     * Get transportAppro
     *
     * @return integer
     */
    public function getTransportAppro()
    {
        return $this->transportAppro;
    }

    /**
     * Set parent
     *
     * @param Arto\AcvBundle\Entity\Project $parent
     */
    public function setParent(\Arto\AcvBundle\Entity\Project $parent)
    {
        $this->parent = $parent;
    }

    /**
     * Get parent
     *
     * @return Arto\AcvBundle\Entity\Project
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add childs
     *
     * @param Arto\AcvBundle\Entity\Project $childs
     */
    public function addProject(\Arto\AcvBundle\Entity\Project $childs)
    {
        $this->childs[] = $childs;
    }

    /**
     * Get childs
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getChilds()
    {
        return $this->childs;
    }

    /**
     * Set pva
     *
     * @param integer $pva
     */
    public function setPva($pva)
    {
        $this->pva = $pva;
    }

    /**
     * Get pva
     *
     * @return integer
     */
    public function getPva()
    {
        return $this->pva;
    }
    
    /**
     * Set lifetime
     *
     * @param integer $lifetime
     */
    public function setLifetime($lifetime)
    {
        $this->lifetime = $lifetime;
    }

    /**
     * Get lifetime
     *
     * @return integer
     */
    public function getLifetime()
    {
        return $this->lifetime;
    }
    
    /**
     * Set transportStockKm
     *
     * @param integer $transportStockKm
     */
    public function setTransportStockKm($transportStockKm)
    {
        $this->transportStockKm = $transportStockKm;
    }

    /**
     * Get transportStockKm
     *
     * @return integer
     */
    public function getTransportStockKm()
    {
        return $this->transportStockKm;
    }

    /**
     * Set transportStock
     *
     * @param Arto\AcvBundle\Entity\Base $transportStock
     */
    public function setTransportStock(\Arto\AcvBundle\Entity\Base $transportStock = null)
    {
        $this->transportStock = $transportStock;
    }

    /**
     * Get transportStock
     *
     * @return Arto\AcvBundle\Entity\Base
     */
    public function getTransportStock()
    {
        return $this->transportStock;
    }

    /**
     * Set greenUser
     *
     * @param Arto\GreenBundle\Entity\User $greenUser
     */
    public function setGreenUser(\Arto\GreenBundle\Entity\User $greenUser)
    {
        $this->greenUser = $greenUser;
    }

    /**
     * Get greenUser
     *
     * @return Arto\GreenBundle\Entity\User 
     */
    public function getGreenUser()
    {
        return $this->greenUser;
    }

    /**
     * Set co2Pva1000
     *
     * @param decimal $co2Pva1000
     */
    public function setCo2Pva1000($co2Pva1000)
    {
        $this->co2Pva1000 = $co2Pva1000;
    }

    /**
     * Get co2Pva1000
     *
     * @return decimal 
     */
    public function getCo2Pva1000()
    {
        return $this->co2Pva1000;
    }

    /**
     * Set waterPva1000
     *
     * @param decimal $waterPva1000
     */
    public function setWaterPva1000($waterPva1000)
    {
        $this->waterPva1000 = $waterPva1000;
    }

    /**
     * Get waterPva1000
     *
     * @return decimal 
     */
    public function getWaterPva1000()
    {
        return $this->waterPva1000;
    }

    /**
     * Set powerPva1000
     *
     * @param decimal $powerPva1000
     */
    public function setPowerPva1000($powerPva1000)
    {
        $this->powerPva1000 = $powerPva1000;
    }

    /**
     * Get powerPva1000
     *
     * @return decimal 
     */
    public function getPowerPva1000()
    {
        return $this->powerPva1000;
    }

    /**
     * Set recyclePva1000
     *
     * @param decimal $recyclePva1000
     */
    public function setRecyclePva1000($recyclePva1000)
    {
        $this->recyclePva1000 = $recyclePva1000;
    }

    /**
     * Get recyclePva1000
     *
     * @return decimal 
     */
    public function getRecyclePva1000()
    {
        return $this->recyclePva1000;
    }

    /**
     * Set folder
     *
     * @param Arto\AcvBundle\Entity\Folder $folder
     */
    public function setFolder(\Arto\AcvBundle\Entity\Folder $folder = null)
    {
        $this->folder = $folder;
    }

    /**
     * Get folder
     *
     * @return Arto\AcvBundle\Entity\Folder 
     */
    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * Add notes
     *
     * @param Arto\AcvBundle\Entity\Operation $notes
     */
    public function addOperation(\Arto\AcvBundle\Entity\Operation $notes)
    {
        $this->notes[] = $notes;
    }

    /**
     * Get notes
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getNotes()
    {
        return $this->notes;
    }
    
    /**
     * Add eolParts
     *
     * @param Arto\AcvBundle\Entity\EolPart $eolParts
     */
    public function addEolPart(\Arto\AcvBundle\Entity\EolPart $eolParts)
    {
        $this->eolParts[] = $eolParts;
    }

    /**
     * Get eolParts
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getManufacturingPlastics()
    {
        return $this->eolParts;
    }
    
    /**
     * Set comparedProjectId
     *
     * @param integer $comparedProjectId
     */
    public function setComparedProjectId($comparedProjectId)
    {
        $this->comparedProjectId = $comparedProjectId;
    }

    /**
     * Get comparedProjectId
     *
     * @return integer
     */
    public function getComparedProjectId()
    {
        return $this->comparedProjectId;
    }
}