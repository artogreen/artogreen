<?php
namespace Arto\AcvBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="acv_product")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="ref", type="string", length=100, nullable=true)
     */
    private $ref;

    /**
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(name="weight", type="decimal", nullable=true, precision=38, scale=2)
     */
    private $weight;
    
    /**
     * @ORM\Column(name="tauxRecyclabilité", type="decimal", nullable=false, scale=2)
     */
    private $tauxRecyclabilité;


    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="products")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;

    /**
     * @ORM\OneToMany(targetEntity="Assembly", mappedBy="product", cascade={"remove"})
     */
    private $assemblies;

    /**
     * @ORM\OneToOne(targetEntity="Usage", mappedBy="product", cascade={"remove"})
     */
    private $usage;

    /**
     * @ORM\OneToOne(targetEntity="Delivery", mappedBy="product", cascade={"remove"})
     */
    private $delivery;

    /**
     * @ORM\OneToOne(targetEntity="Eol", mappedBy="product", cascade={"remove"})
     */
    private $eol;
    
    /**
     * @ORM\OneToMany(targetEntity="DeeePart", mappedBy="product", cascade={"remove"})
     */
    private $deeeParts;

    /**
     * @ORM\OneToMany(targetEntity="ProductPart", mappedBy="product", cascade={"remove"})
     */
    private $parts;

    /**
     * @ORM\OneToMany(targetEntity="SimulationMaterial", mappedBy="product", cascade={"remove"})
     */
    private $simulationMaterials;


    public function __construct()
    {
        $this->weight = 0;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set ref
     *
     * @param string $ref
     */
    public function setRef($ref)
    {
        $this->ref = $ref;
    }

    /**
     * Get ref
     *
     * @return string
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set weight
     *
     * @param decimal $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * Get weight
     *
     * @return decimal
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Get weight
     *
     * @return decimal
     */
    public function getWeightSet()
    {
        $total = 0;
        foreach($this->getAssemblies() as $a) {
            $assembly_quantity = $a->getQuantity();
            $i=0;
            foreach($a->getParts() as $part) {
                $i++;
                if ($part->getParent() != null && $part->getParent()->getQuantity() > 0) {
                    if ($part->getParam1() > 0) {
                        if($part->getMaterial() != null){
                            if($part->getMaterial()->getUnit1() == "g"){
                               $total += $assembly_quantity * $part->getParent()->getQuantity() * $part->getParam1(); 
                            }elseif($part->getMaterial()->getUnit1() == "UN"){
                                if($part->getMaterial()->getUnit2() == "g"){
                                    $total += $assembly_quantity * $part->getParent()->getQuantity() * $part->getParam1() * $part->getMaterial()->getMultiplicateur();
                                }elseif($part->getMaterial()->getUnit2() == "kg"){
                                    $total += $assembly_quantity * $part->getParent()->getQuantity() * $part->getParam1() * $part->getMaterial()->getMultiplicateur();
                                }else{
                                    $total += $assembly_quantity * $part->getParent()->getQuantity() * $part->getParam1() * $part->getMaterial()->getMultiplicateur(); 
                                } 
                            }else{
                                $total += $assembly_quantity * $part->getParent()->getQuantity() * $part->getParam1() * $part->getMaterial()->getMultiplicateur();
                            }
                        } 
                    }
                }
            }
        }
        return $total;
    }

    /**
     * Get weight
     *
     * @return decimal
     */
    public function getWeightPercentageLeft()
    {
        if ($this->getWeight() > 0) {
            return $this->getWeightSet() / $this->getWeight() * 100;
        } else {
            return 0;
        }
    }

    /**
     * Get weight
     *
     * @return decimal
     */
    public function getWeightStatusLeft()
    {
        if ($this->getWeight() > 0) {
            $percentage = $this->getWeightSet() / $this->getWeight() * 100;
            if ($percentage > 105) {
                return 'danger';
            } elseif ($percentage > 95) {
                return 'success';
            } elseif ($percentage > 0) {
                return 'warning';
            }
        } else {
            return 'info';
        }
    }


    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set project
     *
     * @param Arto\AcvBundle\Entity\Project $project
     */
    public function setProject(\Arto\AcvBundle\Entity\Project $project)
    {
        $this->project = $project;
    }

    /**
     * Get project
     *
     * @return Arto\AcvBundle\Entity\Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Add assemblies
     *
     * @param Arto\AcvBundle\Entity\Assembly $assemblies
     */
    public function addAssembly(\Arto\AcvBundle\Entity\Assembly $assemblies)
    {
        $this->assemblies[] = $assemblies;
    }

    /**
     * Get assemblies
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getAssemblies()
    {
        return $this->assemblies;
    }

    /**
     * Set usage
     *
     * @param Arto\AcvBundle\Entity\Usage $usage
     */
    public function setUsage(\Arto\AcvBundle\Entity\Usage $usage)
    {
        $this->usage = $usage;
    }

    /**
     * Get usage
     *
     * @return Arto\AcvBundle\Entity\Usage
     */
    public function getUsage()
    {
        return $this->usage;
    }

    /**
     * Set delivery
     *
     * @param Arto\AcvBundle\Entity\Delivery $delivery
     */
    public function setDelivery(\Arto\AcvBundle\Entity\Delivery $delivery)
    {
        $this->delivery = $delivery;
    }

    /**
     * Get delivery
     *
     * @return Arto\AcvBundle\Entity\Delivery
     */
    public function getDelivery()
    {
        return $this->delivery;
    }

    /**
     * Set eol
     *
     * @param Arto\AcvBundle\Entity\Eol $eol
     */
    public function setEol(\Arto\AcvBundle\Entity\Eol $eol)
    {
        $this->eol = $eol;
    }

    /**
     * Get eol
     *
     * @return Arto\AcvBundle\Entity\Eol
     */
    public function getEol()
    {
        return $this->eol;
    }

    /**
     * Add parts
     *
     * @param Arto\AcvBundle\Entity\ProductPart $parts
     */
    public function addProductPart(\Arto\AcvBundle\Entity\ProductPart $parts)
    {
        $this->parts[] = $parts;
    }

    /**
     * Get parts
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getParts()
    {
        return $this->parts;
    }

    /**
     * Add simulationMaterials
     *
     * @param Arto\AcvBundle\Entity\SimulationMaterial $simulationMaterials
     */
    public function addSimulationMaterial(\Arto\AcvBundle\Entity\SimulationMaterial $simulationMaterials)
    {
        $this->simulationMaterials[] = $simulationMaterials;
    }

    /**
     * Get simulationMaterials
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getSimulationMaterials()
    {
        return $this->simulationMaterials;
    }

    /**
     * Add deeeParts
     *
     * @param Arto\AcvBundle\Entity\DeeePart $deeeParts
     */
    public function addDeeePart(\Arto\AcvBundle\Entity\DeeePart $deeeParts)
    {
        $this->deeeParts[] = $deeeParts;
    }

    /**
     * Get deeeParts
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getDeeeParts()
    {
        return $this->deeeParts;
    }
    
    /**
     * Set tauxRecyclabilité
     *
     * @param decimal $tauxRecyclabilité
     */
    public function setTauxRecyclabilité($tauxRecyclabilité)
    {
        $this->tauxRecyclabilité = $tauxRecyclabilité;
    }

    /**
     * Get tauxRecyclabilité
     *
     * @return decimal 
     */
    public function getTauxRecyclabilité()
    {
        return $this->tauxRecyclabilité;
    }
}