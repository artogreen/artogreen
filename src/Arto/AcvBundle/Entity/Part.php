<?php
namespace Arto\AcvBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Arto\AcvBundle\Repository\PartRepository")
 * @ORM\Table(name="acv_part")
 * @ORM\HasLifecycleCallbacks
 */
class Part
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(name="quantity", type="decimal", nullable=true, scale=2)
     */
    private $quantity;

    /**
     * @ORM\Column(name="param1", type="decimal", nullable=true, scale=3)
     */
    private $param1;

    /**
     * @ORM\Column(name="param2", type="decimal", nullable=true, scale=3)
     */
    private $param2;

    /**
     * @ORM\Column(name="param3", type="decimal", nullable=true, scale=3)
     */
    private $param3;

    /**
     * @ORM\Column(name="param4", type="decimal", nullable=true, scale=3)
     */
    private $param4;

    /**
     * @ORM\Column(name="distance", type="decimal", nullable=true, scale=3)
     */
    private $distance;

    /**
     * @ORM\Column(name="percentage1", type="decimal", nullable=true, scale=2)
     */
    private $percentage1;

    /**
     * @ORM\Column(name="percentage2", type="decimal", nullable=true, scale=2)
     */
    private $percentage2;

    /**
     * @ORM\Column(name="percentage3", type="decimal", nullable=true, scale=2)
     */
    private $percentage3;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Assembly
     *
     * @ORM\ManyToOne(targetEntity="Assembly", inversedBy="parts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="assembly_id", referencedColumnName="id")
     * })
     */
    private $assembly;

    /**
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Base")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="material_id", referencedColumnName="id")
     * })
     */
    private $material;

    /**
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Base")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="process_id", referencedColumnName="id")
     * })
     */
    private $process;

    /**
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Base")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="transport_id", referencedColumnName="id")
     * })
     */
    private $transport;

    /**
     * @var Part
     *
     * @ORM\ManyToOne(targetEntity="Part", inversedBy="childs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * })
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Part", mappedBy="parent", cascade={"remove"})
     */
    private $childs;


    public function __construct()
    {

    }

    public function __toString()
    {
        return (string)$this->getName();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set param1
     *
     * @param integer $param1
     */
    public function setParam1($param1)
    {
        $this->param1 = $param1;
    }

    /**
     * Get param1
     *
     * @return integer
     */
    public function getParam1()
    {
        return $this->param1;
    }

    /**
     * Set param2
     *
     * @param integer $param2
     */
    public function setParam2($param2)
    {
        $this->param2 = $param2;
    }

    /**
     * Get param2
     *
     * @return integer
     */
    public function getParam2()
    {
        return $this->param2;
    }

    /**
     * Set param3
     *
     * @param integer $param3
     */
    public function setParam3($param3)
    {
        $this->param3 = $param3;
    }

    /**
     * Get param3
     *
     * @return integer
     */
    public function getParam3()
    {
        return $this->param3;
    }

    /**
     * Set param4
     *
     * @param integer $param4
     */
    public function setParam4($param4)
    {
        $this->param4 = $param4;
    }

    /**
     * Get param4
     *
     * @return integer
     */
    public function getParam4()
    {
        return $this->param4;
    }

    /**
     * Set distance
     *
     * @param integer $distance
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;
    }

    /**
     * Get distance
     *
     * @return integer
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set assembly
     *
     * @param Arto\AcvBundle\Entity\Assembly $assembly
     */
    public function setAssembly(\Arto\AcvBundle\Entity\Assembly $assembly)
    {
        $this->assembly = $assembly;
    }

    /**
     * Get assembly
     *
     * @return Arto\AcvBundle\Entity\Assembly
     */
    public function getAssembly()
    {
        return $this->assembly;
    }

    /**
     * Set material
     *
     * @param Arto\AcvBundle\Entity\Base $material
     */
    public function setMaterial(\Arto\AcvBundle\Entity\Base $material)
    {
        $this->material = $material;
    }

    /**
     * Get material
     *
     * @return Arto\AcvBundle\Entity\Base
     */
    public function getMaterial()
    {
        return $this->material;
    }

    /**
     * Set process
     *
     * @param Arto\AcvBundle\Entity\Base $process
     */
    public function setProcess(\Arto\AcvBundle\Entity\Base $process)
    {
        $this->process = $process;
    }

    /**
     * Get process
     *
     * @return Arto\AcvBundle\Entity\Base
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * Set transport
     *
     * @param Arto\AcvBundle\Entity\Base $transport
     */
    public function setTransport(\Arto\AcvBundle\Entity\Base $transport)
    {
        $this->transport = $transport;
    }

    /**
     * Get transport
     *
     * @return Arto\AcvBundle\Entity\Base
     */
    public function getTransport()
    {
        return $this->transport;
    }

    /**
     * Set parent
     *
     * @param Arto\AcvBundle\Entity\Part $parent
     */
    public function setParent(\Arto\AcvBundle\Entity\Part $parent)
    {
        $this->parent = $parent;
    }

    /**
     * Get parent
     *
     * @return Arto\AcvBundle\Entity\Part
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add childs
     *
     * @param Arto\AcvBundle\Entity\Part $childs
     */
    public function addPart(\Arto\AcvBundle\Entity\Part $childs)
    {
        $this->childs[] = $childs;
    }

    /**
     * Get childs
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getChilds()
    {
        return $this->childs;
    }

    /**
     * Get childs
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getChildsWeight()
    {
        $weight = 0;
        foreach($this->getChilds() as $child) {
            $weight += $this->getAssembly()->getQuantity() * $this->getQuantity() * $child->getParam1();
        }
        return $weight;
    }

    /**
     * Set percentage1
     *
     * @param integer $percentage1
     */
    public function setPercentage1($percentage1)
    {
        $this->percentage1 = $percentage1;
    }

    /**
     * Get percentage1
     *
     * @return integer
     */
    public function getPercentage1()
    {
        return $this->percentage1;
    }

    /**
     * Set percentage2
     *
     * @param integer $percentage2
     */
    public function setPercentage2($percentage2)
    {
        $this->percentage2 = $percentage2;
    }

    /**
     * Get percentage2
     *
     * @return integer
     */
    public function getPercentage2()
    {
        return $this->percentage2;
    }

    /**
     * Set percentage3
     *
     * @param integer $percentage3
     */
    public function setPercentage3($percentage3)
    {
        $this->percentage3 = $percentage3;
    }

    /**
     * Get percentage3
     *
     * @return integer
     */
    public function getPercentage3()
    {
        return $this->percentage3;
    }
}