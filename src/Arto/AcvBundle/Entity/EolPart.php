<?php
namespace Arto\AcvBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="acv_eol_part")
 */
class EolPart
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="label", type="string", length=100, nullable=false)
     */
    private $label;

    /**
     * Dénomination composant
     *
     * @ORM\Column(name="quantity", type="decimal", nullable=true, scale=2)
     */
    private $quantity;

    /**
     * Poids (g)
     *
     * @ORM\Column(name="weight", type="decimal", nullable=true, scale=2)
     */
    private $weight;

    /**
     * @ORM\Column(name="informations", type="string", length=255, nullable=true)
     */
    private $informations;

    /**
     * Distance de tranport, si il y a transport
     *
     * @ORM\Column(name="distance", type="integer", nullable=true)
     */
    private $distance;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="eolParts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;

    /**
     * Type de traitement en fin de vie
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Arto\AcvBundle\Entity\Base", inversedBy="traitement")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="traitement_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $traitement;

    /**
     * Types de transport
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Arto\AcvBundle\Entity\Base", inversedBy="transport")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="transport_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $transport;
    

    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getLabel();
    }

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set quantity
     *
     * @param decimal $quantity
     */
    public function setQuantity($quantity = null)
    {
        $this->quantity = $quantity;
    }

    /**
     * Get quantity
     *
     * @return decimal 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set weight
     *
     * @param decimal $weight
     */
    public function setWeight($weight = null)
    {
        $this->weight = $weight;
    }

    /**
     * Get weight
     *
     * @return decimal 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set informations
     *
     * @param string $informations
     */
    public function setInformations($informations = null)
    {
        $this->informations = $informations;
    }

    /**
     * Get informations
     *
     * @return string 
     */
    public function getInformations()
    {
        return $this->informations;
    }

    /**
     * Set distance
     *
     * @param integer $distance
     */
    public function setDistance($distance = null)
    {
        $this->distance = $distance;
    }

    /**
     * Get distance
     *
     * @return integer 
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set project
     *
     * @param Arto\AcvBundle\Entity\Project $project
     */
    public function setProject(\Arto\AcvBundle\Entity\Project $project)
    {
        $this->project = $project;
    }

    /**
     * Get project
     *
     * @return Arto\AcvBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }
    
    /**
     * Set traitement
     *
     * @param Arto\AcvBundle\Entity\Base $traitement
     */
    public function setTraitement(\Arto\AcvBundle\Entity\Base $traitement = null)
    {
        $this->traitement = $traitement;
    }

    /**
     * Get traitement
     *
     * @return Arto\AcvBundle\Entity\Base 
     */
    public function getTraitement()
    {
        return $this->traitement;
    }

    /**
     * Set transport
     *
     * @param Arto\AcvBundle\Entity\Base $transport
     */
    public function setTransport(\Arto\AcvBundle\Entity\Base $transport = null)
    {
        $this->transport = $transport;
    }

    /**
     * Get transport
     *
     * @return Arto\AcvBundle\Entity\Base 
     */
    public function getTransport()
    {
        return $this->transport;
    }

}