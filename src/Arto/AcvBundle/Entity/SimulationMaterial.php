<?php
namespace Arto\AcvBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="acv_simulation_material")
 * @ORM\HasLifecycleCallbacks
 */
class SimulationMaterial
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="percentage1", type="decimal", nullable=true, scale=2)
     */
    private $percentage1;

    /**
     * @ORM\Column(name="percentage2", type="decimal", nullable=true, scale=2)
     */
    private $percentage2;

    /**
     * @ORM\Column(name="percentage3", type="decimal", nullable=true, scale=2)
     */
    private $percentage3;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Base")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="material_id", referencedColumnName="id")
     * })
     */
    private $material;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="simulationMaterials")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    private $product;


    public function __construct()
    {

    }

    public function __toString()
    {
        return (string)$this->getName();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set percentage1
     *
     * @param decimal $percentage1
     */
    public function setPercentage1($percentage1)
    {
        $this->percentage1 = $percentage1;
    }

    /**
     * Get percentage1
     *
     * @return decimal 
     */
    public function getPercentage1()
    {
        return $this->percentage1;
    }

    /**
     * Set percentage2
     *
     * @param decimal $percentage2
     */
    public function setPercentage2($percentage2)
    {
        $this->percentage2 = $percentage2;
    }

    /**
     * Get percentage2
     *
     * @return decimal 
     */
    public function getPercentage2()
    {
        return $this->percentage2;
    }

    /**
     * Set percentage3
     *
     * @param decimal $percentage3
     */
    public function setPercentage3($percentage3)
    {
        $this->percentage3 = $percentage3;
    }

    /**
     * Get percentage3
     *
     * @return decimal 
     */
    public function getPercentage3()
    {
        return $this->percentage3;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set material
     *
     * @param Arto\AcvBundle\Entity\Base $material
     */
    public function setMaterial(\Arto\AcvBundle\Entity\Base $material)
    {
        $this->material = $material;
    }

    /**
     * Get material
     *
     * @return Arto\AcvBundle\Entity\Base 
     */
    public function getMaterial()
    {
        return $this->material;
    }

    /**
     * Set product
     *
     * @param Arto\AcvBundle\Entity\Product $product
     */
    public function setProduct(\Arto\AcvBundle\Entity\Product $product)
    {
        $this->product = $product;
    }

    /**
     * Get product
     *
     * @return Arto\AcvBundle\Entity\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }
}