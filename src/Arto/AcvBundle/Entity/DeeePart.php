<?php
namespace Arto\AcvBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="acv_deee_part")
 * @ORM\HasLifecycleCallbacks
 */
class DeeePart
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @ORM\Column(name="present", type="string", length=130, nullable=false)
     */
    private $present;
    
    /**
     * @ORM\Column(name="weight", type="decimal", nullable=true, scale=2)
     */
    private $weight;
    
    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

     /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="deeeParts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    private $product;
    
     /**
     * @ORM\OneToOne(targetEntity="Deee")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="deee_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $deee;


    public function __construct()
    {

    }

    public function __toString()
    {
        return (string)$this->getName();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set present
     *
     * @param string $present
     */
    public function setPresent($present)
    {
        $this->present = $present;
    }

    /**
     * Get present
     *
     * @return string 
     */
    public function getPresent()
    {
        return $this->present;
    }

    /**
     * Set weight
     *
     * @param decimal $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * Get weight
     *
     * @return decimal 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set product
     *
     * @param Arto\AcvBundle\Entity\Product $product
     */
    public function setProduct(\Arto\AcvBundle\Entity\Product $product)
    {
        $this->product = $product;
    }

    /**
     * Get product
     *
     * @return Arto\AcvBundle\Entity\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set deee
     *
     * @param Arto\AcvBundle\Entity\Deee $deee
     */
    public function setDeee(\Arto\AcvBundle\Entity\Deee $deee)
    {
        $this->deee = $deee;
    }

    /**
     * Get deee
     *
     * @return Arto\AcvBundle\Entity\Deee 
     */
    public function getDeee()
    {
        return $this->deee;
    }
}