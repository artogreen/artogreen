<?php
namespace Arto\DesignBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Arto\DesignBundle\Repository\ProjectRepository")
 * @ORM\Table(name="design_project")
 * @ORM\HasLifecycleCallbacks
 */
class Project
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="uniq", type="string", length=32, nullable=false)
     */
    private $uniq;

    /**
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     * @Assert\NotBlank()
     */
    private $name;
    
    /**
     * @ORM\Column(name="projectName", type="string", length=64, nullable = true)
     */
    private $projectName;

    /**
     * @ORM\Column(name="pilote", type="string", length=255, nullable=true)
     */
    private $pilote;

    /**
     * @ORM\Column(name="pilote_date", type="date", nullable=true)
     */
    private $piloteDate;

    /**
     * @var string $path
     *
     * @ORM\Column(name="path", type="string", length=100, nullable=true)
     */
    private $path;

    /**
     * @Assert\File(maxSize="10000000")
     */
    public $file;

    /**
     * to get the image path (slug + extension)
     */
    private $image;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(type="boolean")
     */
    private $base;

    /**
     * @ORM\Column(type="boolean")
     */
    private $energy;

    /**
     * @ORM\Column(name="energy_type", type="string", length=100, nullable=true)
     */
    private $energyType;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $rohs;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $deee;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $battery;

    /**
     * @ORM\Column(name="battery_type", type="string", length=100, nullable=true)
     */
    private $batteryType;

    /**
     * @ORM\Column(name="battery_extract", type="boolean", nullable=true)
     */
    private $batteryExtract;

    /**
     * @ORM\Column(name="reach", type="boolean", nullable=true)
     */
    private $reach;
    
    /**
     * @ORM\Column(name="power1", type="string", length=100, nullable=true)
     */
    private $power1;

    /**
     * @ORM\Column(name="power2", type="string", length=100, nullable=true)
     */
    private $power2;
    
    /**
     * @ORM\Column(name="typeEvaluation", type="string", length=100, nullable=true)
     */
    private $typeEvaluation;
    
    /**
     * @ORM\Column(name="firstVisit", type="boolean", nullable=true)
     */
    private $firstVisit;
    
    /**
     * @ORM\Column(name="resistance", type="integer", nullable=true)
     */
    private $resistance = 0;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
    
    /**
     * @ORM\Column(name="simulatedQeb", type="integer", nullable=true)
     */
    private $simulatedQeb;
    
    /**
     * @var Folder
     *
     * @ORM\ManyToOne(targetEntity="Folder", inversedBy="projects")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="folder_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $folder;

    /**
     * @ORM\OneToMany(targetEntity="Note", mappedBy="project", cascade={"remove"})
     */
    private $notes;

    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="project", cascade={"remove"})
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity="Operation", mappedBy="project", cascade={"remove"})
     */
    private $operations;
    
    /**
     * @ORM\OneToMany(targetEntity="Action", mappedBy="project", cascade={"remove"})
     */
    private $actions;
    
    /**
     * @ORM\OneToMany(targetEntity="AutoAction", mappedBy="project", cascade={"remove"})
     */
    private $autoActions;
    
    /**
     * @ORM\Column(name="descriptionuf", type="text", nullable=true)
     */
    private $descriptionuf;
    
    /**
     * @ORM\OneToOne(targetEntity="Project")
     * @ORM\JoinColumn(name="default_id", referencedColumnName="id")
     */
    private $default;
    
    /**
     * @var GreenUser
     *
     * @ORM\ManyToOne(targetEntity="Arto\GreenBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="green_user_id", referencedColumnName="id")
     * })
     */
    private $greenUser;
    
    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="childs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * })
     */
    private $parent;
    
    /**
     * @ORM\OneToMany(targetEntity="Project", mappedBy="parent", cascade={"remove", "persist"})
     */
    private $childs;


    public function __construct()
    {
        $this->uniq = md5( uniqid('artodesign', true) );
        $this->processed = false;
        $this->active = true;
        $this->energy = false;
        $this->firstVisit = true;
    }

    public function __toString()
    {
        return $this->getName();
    }
    
    /**
     * Methods for upload
     */

    public function getAbsolutePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/project';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            $this->path = $this->file->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        $this->file->move($this->getUploadRootDir(), $this->getImage());

        unset($this->file);
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uniq
     *
     * @param string $uniq
     */
    public function setUniq($uniq)
    {
        $this->uniq = $uniq;
    }

    /**
     * Get uniq
     *
     * @return string 
     */
    public function getUniq()
    {
        return $this->uniq;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set projectName
     *
     * @param string $projectName
     */
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;
    }

    /**
     * Get projectName
     *
     * @return string 
     */
    public function getProjectName()
    {
        return $this->projectName;
    }

    /**
     * Set pilote
     *
     * @param string $pilote
     */
    public function setPilote($pilote)
    {
        $this->pilote = $pilote;
    }

    /**
     * Get pilote
     *
     * @return string 
     */
    public function getPilote()
    {
        return $this->pilote;
    }

    /**
     * Set piloteDate
     *
     * @param date $piloteDate
     */
    public function setPiloteDate($piloteDate)
    {
        $this->piloteDate = $piloteDate;
    }

    /**
     * Get piloteDate
     *
     * @return date 
     */
    public function getPiloteDate()
    {
        return $this->piloteDate;
    }

    /**
     * Set path
     *
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set active
     *
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set base
     *
     * @param boolean $base
     */
    public function setBase($base)
    {
        $this->base = $base;
    }

    /**
     * Get base
     *
     * @return boolean 
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * Set energy
     *
     * @param boolean $energy
     */
    public function setEnergy($energy)
    {
        $this->energy = $energy;
    }

    /**
     * Get energy
     *
     * @return boolean 
     */
    public function getEnergy()
    {
        return $this->energy;
    }

    /**
     * Set energyType
     *
     * @param string $energyType
     */
    public function setEnergyType($energyType)
    {
        $this->energyType = $energyType;
    }

    /**
     * Get energyType
     *
     * @return string 
     */
    public function getEnergyType()
    {
        return $this->energyType;
    }

    /**
     * Set rohs
     *
     * @param boolean $rohs
     */
    public function setRohs($rohs)
    {
        $this->rohs = $rohs;
    }

    /**
     * Get rohs
     *
     * @return boolean 
     */
    public function getRohs()
    {
        return $this->rohs;
    }

    /**
     * Set deee
     *
     * @param boolean $deee
     */
    public function setDeee($deee)
    {
        $this->deee = $deee;
    }

    /**
     * Get deee
     *
     * @return boolean 
     */
    public function getDeee()
    {
        return $this->deee;
    }

    /**
     * Set battery
     *
     * @param boolean $battery
     */
    public function setBattery($battery)
    {
        $this->battery = $battery;
    }

    /**
     * Get battery
     *
     * @return boolean 
     */
    public function getBattery()
    {
        return $this->battery;
    }

    /**
     * Set batteryType
     *
     * @param string $batteryType
     */
    public function setBatteryType($batteryType)
    {
        $this->batteryType = $batteryType;
    }

    /**
     * Get batteryType
     *
     * @return string 
     */
    public function getBatteryType()
    {
        return $this->batteryType;
    }

    /**
     * Set batteryExtract
     *
     * @param boolean $batteryExtract
     */
    public function setBatteryExtract($batteryExtract)
    {
        $this->batteryExtract = $batteryExtract;
    }

    /**
     * Get batteryExtract
     *
     * @return boolean 
     */
    public function getBatteryExtract()
    {
        return $this->batteryExtract;
    }

    /**
     * Set reach
     *
     * @param boolean $reach
     */
    public function setReach($reach)
    {
        $this->reach = $reach;
    }

    /**
     * Get reach
     *
     * @return boolean 
     */
    public function getReach()
    {
        return $this->reach;
    }

    /**
     * Set power1
     *
     * @param string $power1
     */
    public function setPower1($power1)
    {
        $this->power1 = $power1;
    }

    /**
     * Get power1
     *
     * @return string 
     */
    public function getPower1()
    {
        return $this->power1;
    }

    /**
     * Set power2
     *
     * @param string $power2
     */
    public function setPower2($power2)
    {
        $this->power2 = $power2;
    }

    /**
     * Get power2
     *
     * @return string 
     */
    public function getPower2()
    {
        return $this->power2;
    }
    
     /**
     * Set typeEvaluation
     *
     * @param string $typeEvaluation
     */
    public function setTypeEvaluation($typeEvaluation)
    {
        $this->typeEvaluation = $typeEvaluation;
    }

    /**
     * Get typeEvaluation
     *
     * @return string 
     */
    public function getTypeEvaluation()
    {
        return $this->typeEvaluation;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set descriptionuf
     *
     * @param text $descriptionuf
     */
    public function setDescriptionuf($descriptionuf)
    {
        $this->descriptionuf = $descriptionuf;
    }

    /**
     * Get descriptionuf
     *
     * @return text 
     */
    public function getDescriptionuf()
    {
        return $this->descriptionuf;
    }

    /**
     * Add notes
     *
     * @param Arto\DesignBundle\Entity\Note $notes
     */
    public function addNote(\Arto\DesignBundle\Entity\Note $notes)
    {
        $this->notes[] = $notes;
    }

    /**
     * Get notes
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Add users
     *
     * @param Arto\DesignBundle\Entity\User $users
     */
    public function addUser(\Arto\DesignBundle\Entity\User $users)
    {
        $this->users[] = $users;
    }

    /**
     * Get users
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add operations
     *
     * @param Arto\DesignBundle\Entity\Operation $operations
     */
    public function addOperation(\Arto\DesignBundle\Entity\Operation $operations)
    {
        $this->operations[] = $operations;
    }

    /**
     * Get operations
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getOperations()
    {
        return $this->operations;
    }

    /**
     * Add actions
     *
     * @param Arto\DesignBundle\Entity\Action $actions
     */
    public function addAction(\Arto\DesignBundle\Entity\Action $actions)
    {
        $this->actions[] = $actions;
    }

    /**
     * Get actions
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getActions()
    {
        return $this->actions;
    }
    
    /**
     * Add autoActions
     *
     * @param Arto\DesignBundle\Entity\AutoAction $autoActions
     */
    public function addAutoAction(\Arto\DesignBundle\Entity\Action $autoActions)
    {
        $this->autoActions[] = $autoActions;
    }

    /**
     * Get autoActions
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getAutoActions()
    {
        return $this->autoActions;
    }

    /**
     * Set default
     *
     * @param Arto\DesignBundle\Entity\Project $default
     */
    public function setDefault(\Arto\DesignBundle\Entity\Project $default)
    {
        $this->default = $default;
    }

    /**
     * Get default
     *
     * @return Arto\DesignBundle\Entity\Project 
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * Set greenUser
     *
     * @param Arto\GreenBundle\Entity\User $greenUser
     */
    public function setGreenUser(\Arto\GreenBundle\Entity\User $greenUser)
    {
        $this->greenUser = $greenUser;
    }

    /**
     * Get greenUser
     *
     * @return Arto\GreenBundle\Entity\User 
     */
    public function getGreenUser()
    {
        return $this->greenUser;
    }
    
    /**
     * Set folder
     *
     * @param Arto\DesignBundle\Entity\Folder $folder
     */
    public function setFolder(\Arto\DesignBundle\Entity\Folder $folder = null)
    {
        $this->folder = $folder;
    }

    /**
     * Get folder
     *
     * @return Arto\DesignBundle\Entity\Folder 
     */
    public function getFolder()
    {
        return $this->folder;
    }
    
    /**
     * Set firstVisit
     * 
     * @param boolean
     */
    public function setFirstVisit($firstVisit)
    {
        $this->firstVisit = $firstVisit;
    }
    
    /**
     * Get firstVisit
     * 
     * @return boolean
     */
    public function getFirstVisit()
    {
        return $this->firstVisit;
    }
    
    /**
     * Set simulatedQeb
     * 
     * @param integer
     */
    public function setSimulatedQeb($simulatedQeb)
    {
        $this->simulatedQeb = $simulatedQeb;
    }
    
    /**
     * Get simulatedQeb
     * 
     * @return integer
     */
    public function getSimulatedQeb()
    {
        return $this->simulatedQeb;
    }
    
    /**
     * Set resistance
     * 
     * @param integer
     */
    public function setResistance($resistance)
    {
        $this->resistance = $resistance;
    }
    
    /**
     * Get resistance
     * 
     * @return integer
     */
    public function getResistance()
    {
        return $this->resistance;
    }
    
    /**
     * Set parent
     *
     * @param Arto\DesignBundle\Entity\Project $parent
     */
    public function setParent(\Arto\DesignBundle\Entity\Project $parent)
    {
        $this->parent = $parent;
    }

    /**
     * Get parent
     *
     * @return Arto\DesignBundle\Entity\Project
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add childs
     *
     * @param Arto\DesignBundle\Entity\Project $childs
     */
    public function addProject(\Arto\DesignBundle\Entity\Project $childs)
    {
        $this->childs[] = $childs;
    }

    /**
     * Get childs
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getChilds()
    {
        return $this->childs;
    }
}