<?php
namespace Arto\DesignBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="design_resistance_connexion")
 */
class ResistanceConnexion
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=32, nullable=true)
     * @Assert\NotBlank()
     */
    private $name;
    
    /**
     * @ORM\Column(name="withCoating", type="integer", nullable=false)
     * @Assert\NotBlank()
     */
    private $withCoating;
      
    /**
     * @ORM\Column(name="withoutCoating", type="integer", nullable=false)
     * @Assert\NotBlank()
     */
    private $withoutCoating;

    public function __construct()
    {
       
    }

    public function __toString()
    {
        return $this->getName();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set withCoating
     *
     * @param integer $withCoating
     */
    public function setWithCoating($withCoating)
    {
        $this->withCoating = $withCoating;
    }

    /**
     * Get withCoating
     *
     * @return integer
     */
    public function getWithCoating()
    {
        return $this->withCoating;
    }

    /**
     * Set withoutCoating
     *
     * @param integer $withoutCoating
     */
    public function setWithoutCoating($withoutCoating)
    {
        $this->withoutCoating = $withoutCoating;
    }

    /**
     * Get withoutCoating
     *
     * @return integer
     */
    public function getWithoutCoating()
    {
        return $this->withoutCoating;
    }
}