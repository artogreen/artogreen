<?php
namespace Arto\DesignBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="design_criteria")
 */
class Criteria
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="label", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     */
    private $label;

    /**
     * @ORM\Column(name="help", type="text", nullable=true)
     * @Assert\NotBlank()
     */
    private $help;
    
    /**
     * @ORM\Column(name="risk", type="string", length=300, nullable=true)
     */
    private $risk;
    
    /**
     * @ORM\Column(name="opportunity", type="string", length=300, nullable=true)
     */
    private $opportunity;
    
    /**
     * @ORM\Column(name="impact", type="string", length=300, nullable=true)
     */
    private $impact;
    
    /**
     * @var integer $weight
     *
     * @ORM\Column(name="weight", type="integer", nullable=false)
     */
    private $weight;
    
    /**
     * @ORM\Column(name="defaultAction", type="string", length=1000, nullable=false)
     */
    private $defaultAction;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="criterias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getLabel();
    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set label
     *
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set category
     *
     * @param Arto\DesignBundle\Entity\Category $category
     */
    public function setCategory(\Arto\DesignBundle\Entity\Category $category)
    {
        $this->category = $category;
    }

    /**
     * Get category
     *
     * @return Arto\DesignBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }


    /**
     * Set help
     *
     * @param text $help
     */
    public function setHelp($help)
    {
        $this->help = $help;
    }

    /**
     * Get help
     *
     * @return text 
     */
    public function getHelp()
    {
        return $this->help;
    }
    
    /**
     * Set risk
     *
     * @param string $risk
     */
    public function setRisk($risk)
    {
        $this->risk = $risk;
    }
    
    /**
     * Get risk
     * 
     * @return string $risk
     */
    public function getRisk()
    {
        return $this->risk;
    }
    
    /**
     * Set opportunity
     *
     * @param string $opportunity
     */
    public function setOpportunity($opportunity)
    {
        $this->opportunity = $opportunity;
    }
    
    /**
     * Get opportunity
     * 
     * @return string $opportunity
     */
    public function getOpportunity()
    {
        return $this->opportunity;
    }
    
    /**
     * Set impact
     * 
     * @param string $impact
     */
    public function setImpact($impact)
    {
        $this->impact = $impact;
    }
    
    /**
     * Get impact
     * 
     * @return string $impact
     */
    public function getImpact()
    {
        return $this->impact;
    }
    
    /**
     * Set defaultAction
     *
     * @param string $defaultAction
     */
    public function setDefaultAction($defaultAction)
    {
        $this->defaultAction = $defaultAction;
    }

    /**
     * Get defaultAction
     *
     * @return string
     */
    public function getDefaultAction()
    {
        return $this->defaultAction;
    }
    
}