<?php
namespace Arto\DesignBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="design_resistance_fil")
 */
class ResistanceFil
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=50, nullable=true)
     * @Assert\NotBlank()
     */
    private $name;
    
    /**
     * @ORM\Column(name="isFlexible", type="boolean", nullable=false)
     * @Assert\NotBlank()
     */
    private $isFlexible;
      
    /**
     * @ORM\Column(name="resistanceSolid", type="decimal", scale=1, nullable=false)
     * @Assert\NotBlank()
     */
    private $resistanceSolid;
    
    /**
     * @ORM\Column(name="resistanceFlexible", type="decimal", scale=1, nullable=true)
     */
    private $resistanceFlexible;

    public function __construct()
    {
        $this->quantity = 1;
    }

    public function __toString()
    {
        return $this->getName();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set isFlexible
     * 
     * @param boolean $isFlexible
     */
    public function setIsFlexible($isFlexible)
    {
        $this->isFlexible = $isFlexible;
    }
    
    /**
     * Get isFlexible
     *
     * @return boolean
     */
    public function getIsFlexible()
    {
        return $this->isFlexible;
    }

    /**
     * Set resistanceSolid
     *
     * @param decimal $resistanceSolid
     */
    public function setResistanceSolid($resistanceSolid)
    {
        $this->resistanceSolid = $resistanceSolid;
    }

    /**
     * Get resistanceSolid
     *
     * @return decimal
     */
    public function getResistanceSolid()
    {
        return $this->resistanceSolid;
    }

    /**
     * Set resistanceFlexible
     *
     * @param decimal $resistanceFlexible
     */
    public function setResistanceFlexible($resistanceFlexible)
    {
        $this->resistanceFlexible = $resistanceFlexible;
    }

    /**
     * Get resistanceFlexible
     *
     * @return decimal
     */
    public function getResistanceFlexible()
    {
        return $this->resistanceFlexible;
    }
}