<?php
namespace Arto\DesignBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="design_category")
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="label", type="string", length=32, nullable=false)
     * @Assert\NotBlank()
     */
    private $label;

    /**
     * @var integer $index
     *
     * @ORM\Column(name="idx", type="integer", nullable=false)
     */
    private $index;

    /**
     * @var integer $index
     *
     * @ORM\Column(name="eidx", type="integer", nullable=false)
     */
    private $eindex;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="Criteria", mappedBy="category")
     */
    private $criterias;

    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getLabel();
    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set label
     *
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add criterias
     *
     * @param Arto\DesignBundle\Entity\Criteria $criterias
     */
    public function addCriteria(\Arto\DesignBundle\Entity\Criteria $criterias)
    {
        $this->criterias[] = $criterias;
    }

    /**
     * Get criterias
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getCriterias()
    {
        return $this->criterias;
    }

    /**
     * Set index
     *
     * @param integer $index
     */
    public function setIndex($index)
    {
        $this->index = $index;
    }

    /**
     * Get index
     *
     * @return integer
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * Set eindex
     *
     * @param integer $eindex
     */
    public function setEindex($eindex)
    {
        $this->eindex = $eindex;
    }

    /**
     * Get eindex
     *
     * @return integer 
     */
    public function getEindex()
    {
        return $this->eindex;
    }
}