<?php
namespace Arto\DesignBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="design_folder")
 */
class Folder
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=32, nullable=true)
     * @Assert\NotBlank()
     */
    private $name;
    
    /**
     * @ORM\Column(name="isDeployed", type="string", length=32, nullable=false)
     * @Assert\NotBlank()
     */
    private $isDeployed;
      
    /**
     * @ORM\Column(name="code", type="string", length=32, nullable=false)
     * @Assert\NotBlank()
     */
    private $code;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
 
    /**
     * @ORM\OneToMany(targetEntity="Project", mappedBy="folder", cascade={"remove", "persist"})
     */
    private $projects;
    
    /**
     * @var user
     *
     * @ORM\ManyToOne(targetEntity="Arto\GreenBundle\Entity\User", inversedBy="designFolders")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


    public function __construct()
    {
        $this->quantity = 1;
    }

    public function __toString()
    {
        return $this->getName();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    
    /**
     * Remove all projects
     * 
     */
    public function removeAllProjects(){
        $this->projects = null;
    }
    
    /**
     * Add projects
     *
     * @param Arto\DesignBundle\Entity\Project $projects
     */
    public function addProject(\Arto\DesignBundle\Entity\Project $projects)
    {
        $this->projects[] = $projects;
    }

    /**
     * Get projects
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * Set user
     *
     * @param Arto\GreenBundle\Entity\User $user
     */
    public function setUser(\Arto\GreenBundle\Entity\User $user)
    {
        $this->user = $user;
    }

    /**
     * Get user
     *
     * @return Arto\GreenBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set isDeployed
     *
     * @param string $isDeployed
     */
    public function setIsDeployed($isDeployed)
    {
        $this->isDeployed = $isDeployed;
    }

    /**
     * Get isDeployed
     *
     * @return string 
     */
    public function getIsDeployed()
    {
        return $this->isDeployed;
    }

    /**
     * Set code
     *
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * Is Empty
     * 
     * @return boolean
     */
    public function isEmpty(){
        return (count($this->projects) == 0) ? true : false; 
    }
}