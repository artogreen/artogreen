<?php
namespace Arto\DesignBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Arto\DesignBundle\Repository\ProjectRepository")
 * @ORM\Table(name="design_autoAction")
 */
class AutoAction
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="pilote", type="string", length=255, nullable=true)
     */
    private $pilote;
    
    /**
     * @ORM\Column(name="accepted", type="boolean", nullable=false)
     */
    private $accepted;
    
    /**
     * @ORM\Column(name="deployed", type="boolean", nullable=true)
     */
    private $deployed;

    /**
     * @ORM\Column(name="pilote_date", type="date", nullable=true)
     */
    private $piloteDate;

    /**
     * @var Criteria
     *
     * @ORM\ManyToOne(targetEntity="Criteria", inversedBy="notes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="criteria_id", referencedColumnName="id")
     * })
     */
    private $criteria;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="autoActions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;


    public function __construct()
    {
    }

    public function __toString()
    {
        return $this->getPilote();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pilote
     *
     * @param string $pilote
     */
    public function setPilote($pilote)
    {
        $this->pilote = $pilote;
    }

    /**
     * Get pilote
     *
     * @return string
     */
    public function getPilote()
    {
        return $this->pilote;
    }

    /**
     * Set piloteDate
     *
     * @param date $piloteDate
     */
    public function setPiloteDate($piloteDate)
    {
        $this->piloteDate = $piloteDate;
    }

    /**
     * Get piloteDate
     *
     * @return date
     */
    public function getPiloteDate()
    {
        return $this->piloteDate;
    }

    /**
     * Set criteria
     *
     * @param Arto\DesignBundle\Entity\Criteria $criteria
     */
    public function setCriteria(\Arto\DesignBundle\Entity\Criteria $criteria)
    {
        $this->criteria = $criteria;
    }

    /**
     * Get criteria
     *
     * @return Arto\DesignBundle\Entity\Criteria
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * Set project
     *
     * @param Arto\DesignBundle\Entity\Project $project
     */
    public function setProject(\Arto\DesignBundle\Entity\Project $project)
    {
        $this->project = $project;
    }

    /**
     * Get project
     *
     * @return Arto\DesignBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }
    
    /**
     * Set accepted
     *
     * @param boolean $value
     */
    public function setAccepted($accepted)
    {
        $this->accepted = $accepted;
    }

    /**
     * Get value
     *
     * @return boolean
     */
    public function getAccepted()
    {
        return $this->accepted;
    }
    
    /**
     * Set deployed
     *
     * @param boolean $value
     */
    public function setDeployed($deployed)
    {
        $this->deployed = $deployed;
    }

    /**
     * Get value
     *
     * @return boolean
     */
    public function getDeployed()
    {
        return $this->deployed;
    }
}