<?php

namespace Arto\DesignBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Arto\DesignBundle\Entity\User;
use Arto\DesignBundle\Services\RequestHandler;

use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
   private function getRequestHandler()
    {
        return $this->get('arto.design.requestHandler');
    } 

   public function saveAction($id)
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();
        $project = $em->getRepository('ArtoDesignBundle:Project')->find($id);
        $users = $request->get('user');
        
        $u = new User();
        
        foreach($users as $user){
               // We check if the name of the part is not empty
               if (isset($user['lastname']) && $user['lastname'] != null) {

                   // Try Retreiving the object if the request id exist
                   if (isset($user['id']) && $user['id'] != null) {
                       $u = $em->getRepository('ArtoDesignBundle:User')->find($user['id']);
                   } else {
                       $u = new User();
                   }
                   
                   $u->setProject($project);
                   $u->setCompany($user['company']);
                   $u->setFirstname($user['firstname']);
                   $u->setLastname($user['lastname']);
                   
                   $em->persist($u); 
             }          
        }
        
        // We flush the all the modifications at once
        $em->flush();
        
        return $this->redirect($this->generateUrl('design_project_description', array('id' => $id)));
    }
    
    public function deleteAction(){
        $em = $this->getDoctrine()->getEntityManager();
        $requestHandler = $this->getRequestHandler();
        $categories = $requestHandler->getCategorys();

        $userId =$this->getRequest()->get('userId');
        $user = $em->getRepository('ArtoDesignBundle:User')->find($userId);
        
        $projectId = $user->getProject()->getId();
        $project = $em->getRepository('ArtoDesignBundle:Project')->find($projectId);

        $em->remove($user);
        $em->flush();

        return $this->redirect($this->generateUrl('design_project_description', array('id' => $project->getId(), 'categories' => $categories)));
    }

}
