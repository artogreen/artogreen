<?php

namespace Arto\DesignBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Arto\GreenBundle\Controller\TokenAuthenticatedController;
use Arto\DesignBundle\Entity\Note;
use Arto\DesignBundle\Entity\Project;
use Arto\DesignBundle\Entity\Folder;
use Arto\DesignBundle\Entity\Criteria;
use Arto\DesignBundle\Entity\Category;
use Arto\DesignBundle\Entity\Action;
use Arto\DesignBundle\Entity\AutoAction;
use Arto\DesignBundle\Entity\Log;
use Arto\GreenBundle\Entity\GreenLog;
use Arto\DesignBundle\Services\RequestHandler;
use Arto\DesignBundle\Services\Calculator;
use Arto\DesignBundle\Services\Drawer;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller implements TokenAuthenticatedController
{

    private function dateToSQL($date) {
        $returnDate = null;

        $s=substr($date, 6, 4)
            ."-".substr($date, 3, 2)
            ."-".substr($date, 0, 2);

        try {
           $returnDate = new \DateTime($s);
        } catch (\Exception $e) {
            // Nothing to do
        }

        return $returnDate;
    }
    
    private function getRequestHandler()
    {
        return $this->get('arto.design.requestHandler');
    }
    
    private function getCalculator()
    {
        return $this->get('arto.design.calculator');
    }
    
    private function getDrawer()
    {
        return $this->get('arto.design.drawer');
    }

    public function generalAction()
    {
        return $this->render('ArtoDesignBundle:Default:general.html.twig');
    }
    
    public function instructionsAction($id){
        $requestHandler = $this->getRequestHandler();
        $project = $requestHandler->getProject($id);
        $categories = $requestHandler->getCategorys();

        return $this->render('ArtoDesignBundle:Default:instructions.html.twig', array('project' => $project,'categories' => $categories));
    }

    public function termsAction()
    {
        return $this->render('ArtoDesignBundle:Default:terms.html.twig');
    }

    public function indexAction()
    {
        $requestHandler = $this->getRequestHandler();
        $user = $this->get('security.context')->getToken()->getUser();
        
        $userName = $user->getUserName();
        
        if($userName != 'artogreen'){
            $this->logArtoDesign($user);
        }
        
        $userId = $user->getId();
        $categories = $requestHandler->getCategorys();
        
        $folderLetters = array();

        $projects = $requestHandler->findProjects($user);
        $projectsWF = $requestHandler->findProjectsWF($userId);
        
        $folders = $requestHandler->findFolders($userId);
        
        foreach($folders as $folder){
            $folderName = $folder->getName();
            $firstLetter = strtoupper(substr($folderName,0,1));
            
            if(!in_array($firstLetter, $folderLetters)){
                array_push($folderLetters, $firstLetter);
            }
        }
        
        return $this->render('ArtoDesignBundle:Default:index.html.twig', array(
            'projects' => $projects,
            'projectsWF' => $projectsWF,
            'folders' => $folders,
            'myLetter' => '',
            'folderLetters' => $folderLetters,
            'categories' => $categories    
        ));
    }
    
    public function logArtoDesign($user){
        $em = $this->getDoctrine()->getEntityManager();
        
        $today = new \DateTime('NOW');
        $userName = $user->getUsername();
        $userPwd = $user->getPassword();
        $userEmail = $user->getEmail();
        
        $log = new Log();
        $log->setDate($today);
        $log->setUserName($userName);
        $log->setUserPwd($userPwd);
        $log->setUserEmail($userEmail);
        
        $greenLog = new GreenLog();
        $greenLog->setDate($today);
        $greenLog->setUserName($userName);
        $greenLog->setUserPwd($userPwd);
        $greenLog->setUserEmail($userEmail);
        $greenLog->setSoftware("ArtoDesign");
        
        $em->persist($log);
        $em->persist($greenLog);
        $em->flush();
    }

    public function homeAction()
    {
        $requestHandler = $this->getRequestHandler();
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        
        $id = $request->get('id');
        $project = $em->getRepository('ArtoDesignBundle:Project')->find($id);
        $categories = $requestHandler->getCategorys();
        
        return $this->render('ArtoDesignBundle:Default:home.html.twig', array('project' => $project, 'categories' => $categories));
    }

    public function createAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $requestHandler = $this->getRequestHandler();
        $request = $this->getRequest();
        $categories = $requestHandler->getCategorys();

        $typeEvaluation = $request->get('typeEvaluation');
        $energy = $request->get('energy');
        $name = $request->get('name');
        $dossierId = $request->get('dossier');
        
        $project = $requestHandler->createProject($name, $user, $typeEvaluation, $energy, $dossierId);
       
        return $this->redirect($this->generateUrl('design_project_home', array('id' => $project->getId(), 'categories' => $categories)));
    }
    
    public function renameAction(){
        $requestHandler = $this->getRequestHandler(); 
        $request = $this->getRequest();
        
        $id = $request->get('id');
        $newName = $request->get('name');
        
        $requestHandler->renameProject($id, $newName);
       
        return $this->redirect($this->generateUrl('design'));  
    }
    
    public function deleteAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $requestHandler = $this->getRequestHandler();
        
        $request = $this->getRequest();
        $id = $request->get('id');
        $special = $request->get('special');
        
        $requestHandler->deleteProject($id, $user, $special);
        
        return $this->redirect($this->generateUrl('design'));
    }
    
    public function showAction($id)
    {
        $requestHandler = $this->getRequestHandler();
        $calculator = $this->getCalculator();
        
        $project = $requestHandler->getProject($id);
        $criterias = $requestHandler->getCriterias();
        $projectDefault = $project->getDefault();
        $actions = array();
        $categories = $requestHandler->getCategorys();

        foreach($project->getNotes() as $helpnote) {
            if ($helpnote->getAction() != null) {
                if (!isset($actions[$helpnote->getCriteria()->getId()])) {
                    $actions[$helpnote->getCriteria()->getId()] = array();
                }
                $actions[$helpnote->getCriteria()->getId()][] = $helpnote->getAction();
            }
        }
        
        $qeb = $calculator->calculateQeb($project, false);
        $qeb_default = $calculator->calculateQeb($projectDefault, false);
        
        $highWeightCriterias = $requestHandler->getHighCriterias($project);
        
        $pilote_actions = $requestHandler->findAllActions($project);

        return $this->render('ArtoDesignBundle:Default:show.html.twig', array(
            'project' => $project,
            'project_default' => $project->getDefault(),
            'criterias' => $criterias,
            'qeb' => round($qeb),
            'qeb_default' => round($qeb_default),
            'actions' => $actions,
            'pilote_actions' => $pilote_actions,
            'categories' => $categories,
            'highCriterias' => $highWeightCriterias
        ));
    }
    
    public function showInteractiveAction($id)
    {
        $requestHandler = $this->getRequestHandler();
        $calculator = $this->getCalculator();
        $request = $this->getRequest();
        
        $project = $requestHandler->getProject($id);
        $criterias = $requestHandler->getCriterias();
        $projectDefault = $project->getDefault();
        $categories = $requestHandler->getCategorys();
        $actions = array();
        

        foreach($criterias as $criteria){
            $note = $requestHandler->getNote($project, $criteria);
            
            $noteValue = $note->getValue();
            
            //Si critère faible on crée une AutoAction
            if($noteValue == false){
                $autoActionFound = $requestHandler->findAutoAction($project, $criteria);
                
                if($autoActionFound == null){
                    $requestHandler->createAutoAction($project, $criteria);
                } 
            }
            //Si critère fort et qu'il existe une AutoAction on la supprime
            else{
                $autoActionFound = $requestHandler->findAutoAction($project, $criteria);
                
                if($autoActionFound != null){
                    $requestHandler->deleteAutoAction($autoActionFound);
                }  
            }     
        }
        
        $qeb = $calculator->calculateQeb($project, true);
        $qeb_default = $calculator->calculateQeb($projectDefault, false);
        
        $autoActions = $requestHandler->findAllAutoActions($project);

        return $this->render('ArtoDesignBundle:Default:showInteractif.html.twig', array(
            'project' => $project,
            'project_default' => $project->getDefault(),
            'categories' => $categories,
            'qeb' => round($qeb),
            'qeb_default' => round($qeb_default),
            'actions' => $actions,
            'autoActions' => $autoActions
        ));
    }

    public function hpeAction($id)
    {
        $requestHandler = $this->getRequestHandler();
        $calculator = $this->getCalculator();
        
        $project = $requestHandler->getProject($id);
        $criterias = $requestHandler->getCriterias();
        $categories = $requestHandler->getCategorys();
        
        $question1 = $calculator->isQuestion1($project);
        $question2 = $calculator->isQuestion2($project);        
        $question3 = $calculator->isQuestion3($project);
        $question4 = $calculator->isQuestion4($project);
        $question5 = $calculator->isQuestion5($project);
        $question6 = $calculator->isQuestion6($project);
        $question7 = $calculator->isQuestion7($project);
        
        return $this->render('ArtoDesignBundle:Default:hpe.html.twig', array(
            'project' => $project,
            'criterias' => $criterias,
            'question1' => $question1,
            'question2' => $question2,
            'question3' => $question3,
            'question4' => $question4,
            'question5' => $question5,
            'question6' => $question6,
            'question7' => $question7,
            'categories' => $categories
        ));
    }

    public function descriptionAction($id = null)
    {
        $requestHandler = $this->getRequestHandler();

        $defaut = $this->getRequest()->get('default');

        $project = $requestHandler->getProject($id);
        $users = $requestHandler->getUsersByProject($project);
        $operations = $requestHandler->getOperationsByProject($project);
        $categories = $requestHandler->getCategorys();
        $typologie = $requestHandler->getTypologie($project);
        $objetEtude = $requestHandler->getObjetEtude($project);

        $project_default = $project->getDefault();

        if ($defaut == null) {
            $tpl = 'ArtoDesignBundle:Default:description.html.twig';
        } else {
            $tpl = 'ArtoDesignBundle:Default:description_default.html.twig';
        }
        return $this->render($tpl, array(
            'project' => $project,
            'project_default' => $project_default,
            'users' => $users,
            'operations' => $operations,
            'categories' => $categories,
            'typologie' => $typologie,
            'objetEtude' => $objetEtude
        ));
    }

    public function descriptionGeneralitesSaveAction()
    {
        $requestHandler = $this->getRequestHandler();
        $request = $this->getRequest();
        
        $project = $request->get('project');
        $default = $request->get('default');
        $projectName = $request->get('projectName');
        $descriptionUF = $request->get('descriptionuf');
        $uploadedFile = $_FILES['file'];
        $categories = $requestHandler->getCategorys();
        
        $projectBase = $requestHandler->getProject($project);
        $projectBaseId = $projectBase->getId();
        
        $requestHandler->saveGeneralites($projectBase, $default, $projectName, $descriptionUF, $uploadedFile);

        return $this->redirect($this->generateUrl('design_project_description', array('id' => $projectBaseId, 'default' => $default, 'categories' => $categories)));
    }

    public function descriptionPiloteSaveAction(){
        $requestHandler = $this->getRequestHandler();
        $request = $this->getRequest();
        
        $project = $request->get('project');
        $default = $request->get('default');
        $pilote = $request->get('pilote');
        $piloteDate = $request->get('pilote_date');  
        $categories = $requestHandler->getCategorys();
        
        $projectBase = $requestHandler->getProject($project);
        $projectBaseId = $projectBase->getId();

        $requestHandler->savePilote($projectBase, $default, $pilote, $piloteDate);

        return $this->redirect($this->generateUrl('design_project_description', array('id' => $projectBaseId, 'default' => $default, 'categories' => $categories)));
    }

    public function descriptionReglementationSaveAction(){
        $requestHandler = $this->getRequestHandler();
        $request = $this->getRequest();
        
        $project = $request->get('project');
        $default = $request->get('default');
        $rohs = $request->get('rohs');
        $reach = $request->get('reach');
        $deee = $request->get('deee');
        $battery = $request->get('battery');
        $batteryExtract = $request->get('battery_extract');
        $categories = $requestHandler->getCategorys();
        
        $projectBase = $requestHandler->getProject($project);
        $projectBaseId = $projectBase->getId();

        $requestHandler->saveReglementation($projectBase, $default, $rohs, $reach, $deee, $battery, $batteryExtract);

        return $this->redirect($this->generateUrl('design_project_description', array('id' => $projectBaseId, 'default' => $default, 'categories' => $categories)));
    }
    
    public function descriptionTypeEvaluationSaveAction(){
        $requestHandler = $this->getRequestHandler();
        $request = $this->getRequest();
        
        $project = $request->get('project');
        $default = $request->get('default');
        $typeEvaluation = $request->get('typeEvaluation');
        $categories = $requestHandler->getCategorys();
        
        $projectBase = $requestHandler->getProject($project);
        $projectBaseId = $projectBase->getId();
        
        $requestHandler->saveTypeEvaluation($projectBase, $default, $typeEvaluation);

        return $this->redirect($this->generateUrl('design_project_description', array('id' => $projectBaseId, 'default' => $default, 'categories' => $categories)));
    }

    public function descriptionConsommationSaveAction(){
        $requestHandler = $this->getRequestHandler();
        $request = $this->getRequest();
        
        $project = $request->get('project');
        $default = $request->get('default');
        $power1 = $request->get('power1');
        $power2 = $request->get('power2');
        $energy = $request->get('energy');
        $energyType = $request->get('energy_type');
        
        $categories = $requestHandler->getCategorys();
        $projectBase = $requestHandler->getProject($project);
        $projectBaseId = $projectBase->getId();

        $requestHandler->saveConsommation($project, $default, $power1, $power2, $energy, $energyType);

        return $this->redirect($this->generateUrl('design_project_description', array('id' => $projectBaseId, 'default' => $default, 'categories' => $categories)));
    }
    
    public function descriptionReferenceSaveAction(){
        $requestHandler = $this->getRequestHandler();
        $request = $this->getRequest();

        $project = $request->get('project');
        $default = $request->get('default');
        $projectDefaultName = $request->get('projectDefaultName');
        $uploadedFile = $_FILES['fileDefault'];
        $categories = $requestHandler->getCategorys();
        
        $projectBase = $requestHandler->getProject($project);
        $projectBaseId = $projectBase->getId();

        $requestHandler->saveReference($project, $default, $projectDefaultName, $uploadedFile);
        
        return $this->redirect($this->generateUrl('design_project_description', array('id' => $projectBaseId, 'default' => null, 'categories' => $categories)));
    }


    public function categoryAction($id, $category){
        $em = $this->getDoctrine()->getEntityManager();
        $requestHandler = $this->getRequestHandler();
        
        $project = $requestHandler->getProject($id);
        $category = $requestHandler->getCategory($category);
        $categories = $requestHandler->getCategorys();
        $connexions = $em->getRepository('ArtoDesignBundle:ResistanceConnexion')->findAll();

        $notes = $requestHandler->getArrayNotesByCriterias($project);
        $actions = $requestHandler->getArrayActionsByCriterias($project);
        $comments = $requestHandler->getArrayCommentsByCriterias($project);
        $comments_default = $requestHandler->getArrayCommentsDefaultByCriterias($project);
        $notes_default = $requestHandler->getArrayNotesDefaultByCriterias($project);
        
        $proofs = $requestHandler->checkPreuves($project, $category);
        
        return $this->render('ArtoDesignBundle:Default:category.html.twig', array(
            'project' => $project,
            'category' => $category,
            'notes' => $notes,
            'notes_default' => $notes_default,
            'actions' => $actions,
            'comments' => $comments,
            'comments_default' => $comments_default,
            'categories' => $categories,
            'connexions' => $connexions,
            'proofs' => $proofs
        ));
    }

    public function categorySaveAction($id, $category)
    {
        $requestHandler = $this->getRequestHandler();
        $request = $this->getRequest();
        
        $project = $requestHandler->getProject($id);
        $category = $requestHandler->getCategory($category);
        $requestHandler->saveCategory($project, $category, $request);
        $categories = $requestHandler->getCategorys();
        
        $projectId = $project->getId();
        $categoryId = $category->getId();
        
        return $this->redirect($this->generateUrl('design_project_category', array('id' => $projectId, 'category' => $categoryId, 'categories' => $categories)));
    }

    public function actionSaveAction($id, $project)
    {
        $requestHandler = $this->getRequestHandler();
        $request = $this->getRequest();

        $project = $requestHandler->getProject($project);
        $criteria = $requestHandler->getCriteria($id);
        $action = $request->get('action');
        
        $requestHandler->saveAction($project, $criteria, $action);
        
        exit();
    }

    public function commentSaveAction($id, $project)
    {
        $requestHandler = $this->getRequestHandler();
        $request = $this->getRequest();
        
        $project = $requestHandler->getProject($project);
        $criteria = $requestHandler->getCriteria($id);
        $comment = $request->get('comment');
        $commentDefault = $request->get('comment_default');

        $requestHandler->saveComment($project, $criteria, $comment, $commentDefault);
        
        exit();
    }

    public function chartAction($id, $simulated)
    {
        $drawer = $this->getDrawer();
        
        $response = new Response();
        $response->headers->set('Content-Type', 'image/jpeg');
        
        $chart1 = $drawer->drawChart1($id, $simulated);
        $response->setContent($chart1);
        
        //$response = $drawer->drawChart1($id, $simulated);
                
        return $response;
    }

    public function summuaryCategoryAction($id, $simulated)
    {
        $requestHandler = $this->getRequestHandler();
        $calculator = $this->getCalculator();
        
        $categories = $requestHandler->getCategorys();
        $criterias = $requestHandler->getCriterias();

        /* Calculate Main Project */
        $project1 = $requestHandler->getProject($id);
        $result_percentage = $calculator->calculateCategoriesProfile($project1, $categories, $criterias, $simulated);

        /* Calculate Default Project */
        $project2 = $project1->getDefault();
        $result_default = $calculator->calculateCategoriesProfile($project2, $categories, $criterias, $simulated);

        $result = array();
        if (count($result_percentage) == 5 && count($result_default) == 5) {
            $i=0;
            foreach($categories as $category){
                $i++;
                $result[$category->getLabel()] = ($result_percentage[$i] >= $result_default[$i]) ? 'green' : 'red';
            }
        }

        return $this->render('ArtoDesignBundle:Default:summuary_category.html.twig', array(
            'result' => $result,
            'categories' => $categories
        ));
    }

    public function chart2Action($id, $simulated)
    {
        $drawer = $this->getDrawer();
        $response = $drawer->drawChart2($id, $simulated);
                
        return $response;
    }

    public function summuaryProfileAction($id, $simulated)
    {
        $requestHandler = $this->getRequestHandler();
        $calculator = $this->getCalculator();

        $categories = $requestHandler->getCategorys();
        $weights = $requestHandler->getWeights();
        $calculs = $requestHandler->getCalculs();

        $project1 = $requestHandler->getProject($id);
        $notes1 = $requestHandler->getNotes($project1);
        $result_percentage = $calculator->calculateWeightProfile($project1, $categories, $weights, $weights, $calculs, $notes1, $simulated);

        $project2 = $project1->getDefault();
        $notes2 = $requestHandler->getNotes($project2);
        $result_default = $calculator->calculateWeightProfile($project2, $categories, $weights, $weights, $calculs, $notes2, $simulated);

        $result = array();
        if (count($result_percentage) == 5 && count($result_default) == 5) {
            $result['Sûr'] = ($result_percentage[1] >= $result_default[1]) ? 'green' : 'red';
            $result['Léger'] = ($result_percentage[2] >= $result_default[2]) ? 'green' : 'red';
            $result['Sobre'] = ($result_percentage[3] >= $result_default[3]) ? 'green' : 'red';
            $result['Durable'] = ($result_percentage[4] >= $result_default[4]) ? 'green' : 'red';
            $result['Economique'] = ($result_percentage[5] >= $result_default[5]) ? 'green' : 'red';
        }

        return $this->render('ArtoDesignBundle:Default:summuary_profile.html.twig', array(
            'result' => $result,
            'categories' => $categories
        ));
    }

    public function searchShowAction($id)
    {
        $requestHandler = $this->getRequestHandler();
        
        $project = $requestHandler->getProject($id);
        $categories = $requestHandler->getCategorys();
        
        return $this->render('ArtoDesignBundle:Default:search.html.twig', array(
            'project' => $project,
            'categories' => $categories
        ));
    }

    public function searchAction($id)
    {
        $requestHandler = $this->getRequestHandler();
        $request = $this->getRequest();
        
        $project = $requestHandler->getProject($id);
        $categories = $requestHandler->getCategorys();

        $searchKey = $request->get('key');
        $results = $requestHandler->searchCriteria($searchKey);
        
        return $this->render('ArtoDesignBundle:Default:_searchResult.html.twig',array(
            'results' => $results,
            'project' => $project,
            'categories' => $categories
        ));
    }

    public function planActionSaveAction($id)
    {
        $requestHandler = $this->getRequestHandler();
        $request = $this->getRequest();

        $project = $requestHandler->getProject($id);
        
        $requestHandler->savePlanAction($project, $request);
        
        return $this->showAction($project->getId());
    }
    
    public function bilanInteractifSaveAction($id)
    {
        $requestHandler = $this->getRequestHandler();
        $calculator = $this->getCalculator();
        $request = $this->getRequest();
       
        $project = $requestHandler->getProject($id);
        
        $requestHandler->saveBilanInteractif($project, $request);
        $simulatedQeb = $calculator->calculateQeb($project, true);
        $simulatedQeb = round($simulatedQeb);
        $requestHandler->setSimulatedQeb($id,$simulatedQeb);
        
        return $this->showInteractiveAction($project->getId());
    }
    
    public function visitAction()
    {
        $requestHandler = $this->getRequestHandler();
        $request = $this->getRequest();
        
        $projectId = $request->get('projectId');
        
        $result = $requestHandler->visitAmeliorations($projectId);
        
        $json = json_encode($result);
        $response = new Response($json);
        return $response;
    }
    
    public function unVisitAction()
    {
        $requestHandler = $this->getRequestHandler();
        $request = $this->getRequest();
        
        $projectId = $request->get('projectId');
        
        $project = $requestHandler->unVisitAmeliorations($projectId);
        
        return $this->showInteractiveAction($projectId);
    }
    
    public function hideAutoActionsAction()
    {
        $requestHandler = $this->getRequestHandler();
        $request = $this->getRequest();
         
        $categoryId = $request->get('categoryId');
        $projectId = $request->get('projectId');
        
        $result = $requestHandler->hideAutoActions($projectId, $categoryId);
        
        $json = json_encode($result);
        $response = new Response($json);
        
        return $response;
    }
    
    public function resistanceStep1CalculateAction(){
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        
        $projectId = $request->get('projectId');
        $id = $request->get('connexionId');
        $withoutCVal = $request->get('withoutCVal');
        $withCVal = $request->get('withCVal');
        
        if($withoutCVal == ''){
            $withoutCVal = 0;
        }
        
        if($withCVal == ''){
            $withCVal = 0;
        }
        
        $project = $em->getRepository('ArtoDesignBundle:Project')->find($projectId);
        
        $connexion = $em->getRepository('ArtoDesignBundle:ResistanceConnexion')->findOneBy(
                array('id' => $id));
        
        if($id != 1){
            $resistance = $project->getResistance() + $withoutCVal * $connexion->getWithoutCoating() + $withCVal * $connexion->getWithCoating();
        }else{
            $resistance = $withoutCVal * $connexion->getWithoutCoating() + $withCVal * $connexion->getWithCoating();
        }
        
        
        $project->setResistance($resistance);
        
        $em->persist($project);
        $em->flush();
        
        exit();
    }
    
    public function resistanceStep2CalculateAction(){
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        
        $projectId = $request->get('projectId');
        $nbConducteurs = $request->get('nbConducteurs');
        $sectionConducteur = $request->get('sectionConducteur');
        $longueurConducteur = $request->get('longueurConducteur');
        $resistanceLineique = $request->get('resistanceLineique');
        
        $project = $em->getRepository('ArtoDesignBundle:Project')->find($projectId);
        
        $resistance = $project->getResistance();
        
        $resistanceStep2 = $resistanceLineique/$sectionConducteur * $longueurConducteur * 10 * $nbConducteurs;
        
        $resistance = $resistance + $resistanceStep2;
        
        $project->setResistance($resistance);
        
        $em->persist($project);
        $em->flush();
        
        exit();
    }
    
    public function getProjectResistanceAction(){
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        
        $projectId = $request->get('projectId');
        
        $project = $em->getRepository('ArtoDesignBundle:Project')->find($projectId);
        
        $resistance = $project->getResistance();
        
        $result = array(
            'resistance' => $resistance
        );
        
        $json = json_encode($result);
        $response = new Response($json);
        return $response;
    }
    
    public function resetResistanceAction(){
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        
        $projectId = $request->get('projectId');
        
        $project = $em->getRepository('ArtoDesignBundle:Project')->find($projectId);
        
        $project->setResistance(0);
        
        $em->persist($project);
        $em->flush();
        
        exit();
    }
    
    public function getConducteursAction(){
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        
        $rigid = $request->get('rigid');
        $options = '';
        
        if($rigid == 'true'){
            $conducteurs = $em->getRepository('ArtoDesignBundle:ResistanceFil')->findAll();
        }else{
             $conducteurs = $em->getRepository('ArtoDesignBundle:ResistanceFil')->findBy(
                array('isFlexible' => true));
        }
         
        foreach($conducteurs as $conducteur){
            if($rigid == 'true'){
                $options = $options.'<option value="'.$conducteur->getResistanceSolid().'">'.$conducteur->getName().'</option>';
            }elseif($rigid == 'false'){
                $options = $options.'<option value="'.$conducteur->getResistanceFlexible().'">'.$conducteur->getName().'</option>';
            }
        }
        
        $result = array(
            'options' => $options
        );
        
        $json = json_encode($result);
        $response = new Response($json);
        return $response;
    }
    
    public function downloadPowerExcelFileAction(){ 
        $fichier = 'Feuille de calcul de puissance.xls';
        $path = __DIR__.'/../../../../web/';
        
        $response = new Response();
        $response->setStatusCode(200);
        $response->setContent(file_get_contents($path.$fichier));
        // modification du content-type pour forcer le téléchargement (sinon le navigateur internet essaie d'afficher le document)
        $response->headers->set('Content-Type', 'application/force-download'); 
        $response->headers->set('Content-disposition', 'filename='. $fichier);
 
        // prints the HTTP headers followed by the content
        $response->send();
        
        return $response;
    }
    
    public function downloadHelpPonderationFileAction(){
        $fichier = 'Aide a la ponderation.xls';
        $path = __DIR__.'/../../../../web/';
        
        $response = new Response();
        $response->setStatusCode(200);
        $response->setContent(file_get_contents($path.$fichier));
        // modification du content-type pour forcer le téléchargement (sinon le navigateur internet essaie d'afficher le document)
        $response->headers->set('Content-Type', 'application/force-download'); 
        $response->headers->set('Content-disposition', 'filename='. $fichier);
 
        // prints the HTTP headers followed by the content
        $response->send();
        
        return $response;
    }
    
    public function downloadEcoCalculateurImpactTransportsFileAction(){
        $fichier = 'Eco calculateur impact transports.xls';
        $path = __DIR__.'/../../../../web/';
        
        $response = new Response();
        $response->setStatusCode(200);
        $response->setContent(file_get_contents($path.$fichier));
        // modification du content-type pour forcer le téléchargement (sinon le navigateur internet essaie d'afficher le document)
        $response->headers->set('Content-Type', 'application/force-download'); 
        $response->headers->set('Content-disposition', 'filename='. $fichier);
 
        // prints the HTTP headers followed by the content
        $response->send();
        
        return $response;
    }
    
    public function downloadCriteria4FileAction(){
        $fichier = 'Biosourcé VS Biodégradable VS Compostable.docx';
         
        $path = __DIR__.'/../../../../web/';
        
        $response = new Response();
        $response->setStatusCode(200);
        $response->setContent(file_get_contents($path.$fichier));
        // modification du content-type pour forcer le téléchargement (sinon le navigateur internet essaie d'afficher le document)
        $response->headers->set('Content-Type', 'application/force-download'); 
        $response->headers->set('Content-disposition', 'filename='. $fichier);
 
        // prints the HTTP headers followed by the content
        $response->send();
        
        return $response;  
    }
    
    public function downloadCriteria6File1Action(){
        $fichier = "L'économie circulaire.docx";
         
        $path = __DIR__.'/../../../../web/';
        
        $response = new Response();
        $response->setStatusCode(200);
        $response->setContent(file_get_contents($path.$fichier));
        // modification du content-type pour forcer le téléchargement (sinon le navigateur internet essaie d'afficher le document)
        $response->headers->set('Content-Type', 'application/force-download'); 
        $response->headers->set('Content-disposition', 'filename='. $fichier);
 
        // prints the HTTP headers followed by the content
        $response->send();
        
        return $response;  
    }
    
    public function downloadCriteria6File2Action(){
        $fichier = "Les matières plastiques recyclées.docx";
         
        $path = __DIR__.'/../../../../web/';
        
        $response = new Response();
        $response->setStatusCode(200);
        $response->setContent(file_get_contents($path.$fichier));
        // modification du content-type pour forcer le téléchargement (sinon le navigateur internet essaie d'afficher le document)
        $response->headers->set('Content-Type', 'application/force-download'); 
        $response->headers->set('Content-disposition', 'filename='. $fichier);
 
        // prints the HTTP headers followed by the content
        $response->send();
        
        return $response;  
    }
    
    
    
    public function duplicateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        
        $project = $em->getRepository('ArtoDesignBundle:Project')->find($id);
        $projectDefault = $project->getDefault();
        $user = $this->get('security.context')->getToken()->getUser();
        
        $projectDefault_new = $this->duplicateDefaultProject($projectDefault);
        $project_new = clone $project;
        
        $project_new->setGreenUser($user);
        $project_new->setName($project->getName().' - essai '.(count($project->getChilds())+1));
        $project_new->setProjectName($project->getProjectName());
        $project_new->setEnergy($project->getEnergy());
        $project_new->setBase(1);
        $project_new->setDefault($projectDefault_new);
        
         //Un dossier ?
        $folder = $project->getFolder();
        //Si il y a un dossier
        if($folder != null){
            //On retrouve le nom du dossier
            $folderName = $folder->getName();
            
            //Recherche du dossier
            $folderCloned = $em->getRepository('ArtoDesignBundle:Folder')->findOneBy(
                    array('name' => $folderName, 'user' => $user->getId()));
            
            //Attribution du dossier au projet cloné
            $project_new->setFolder($folderCloned);
        }

        $em->persist($project_new);  
        
        foreach($project->getNotes() as $note){
            $note_new = clone $note;
            $note_new->setProject($project_new);
            $em->persist($note_new);
            //$project_new->addNote($note_new);
        }
        
        foreach($projectDefault->getNotes() as $noteDefault){
            $noteDefault_new = clone $noteDefault;
            $noteDefault_new->setProject($projectDefault_new);
            $em->persist($noteDefault_new);
            //$project_new->addNote($note_new);
        }
        
        foreach($project->getActions() as $action){
            $action_new = clone $action;
            $action_new->setProject($project_new);
            $em->persist($action_new);
            //$project_new->addAction($action_new);
        }
        
        foreach($projectDefault->getActions() as $actionDefault){
            $actionDefault_new = clone $actionDefault;
            $actionDefault_new->setProject($projectDefault_new);
            $em->persist($actionDefault_new);
            //$project_new->addAction($action_new);
        }
        
        foreach($project->getOperations() as $operation){
            $operation_new = clone $operation;
            $operation_new->setProject($project_new);
            $em->persist($operation_new);
        }
        
        foreach($project->getUsers() as $projectUser){
            $user_new = clone $projectUser;
            $user_new->setProject($project_new);
            $em->persist($user_new);
        }
        
        $em->flush();
        
        return $this->redirect($this->generateUrl('design'));
    }
    
    public function duplicateDefaultProject($project){
        $em = $this->getDoctrine()->getEntityManager();
        
        $user = $this->get('security.context')->getToken()->getUser();
        
        //Gathering projectDefault datas
        $projectDefaultName = $project->getName();
        $projectDefaultPilote = $project->getPilote();
        $projectDefaultPiloteDate = $project->getPiloteDate();
        $projectDefaultPath = $project->getPath();
        $projectDefaultActive = $project->getActive();
        $projectDefaultCreatedAt = $project->getCreatedAt();
        $projectDefaultUpdatedAt = $project->getUpdatedAt();
        
        //Creating new ProjectDefault
        $projectDefault_new = new Project();

        $projectDefaultEnergy = $project->getEnergy();
        $projectDefaultEnergyType = $project->getEnergyType();
        $projectDefaultRohs = $project->getRohs();
        $projectDefaultDeee = $project->getDeee();
        $projectDefaultBattery = $project->getBattery();
        $projectDefaultBatteryType = $project->getBatteryType();
        $projectDefaultBatteryExtract = $project->getBatteryExtract();
        $projectDefaultReach = $project->getReach();
        $projectDefaultPower1 = $project->getPower1();
        $projectDefaultPower2 = $project->getPower2();
        $projectDefaultTypeEvaluation = $project->getTypeEvaluation();
        $projectDefaultDescriptionuf = $project->getDescriptionuf();

        //Setting other datas as equal as original ProjectDefault
        $projectDefault_new->setEnergy($projectDefaultEnergy);
        $projectDefault_new->setEnergyType($projectDefaultEnergyType);
        $projectDefault_new->setRohs($projectDefaultRohs);
        $projectDefault_new->setDeee($projectDefaultDeee);
        $projectDefault_new->setBattery($projectDefaultBattery);
        $projectDefault_new->setBatteryType($projectDefaultBatteryType);
        $projectDefault_new->setBatteryExtract($projectDefaultBatteryExtract);
        $projectDefault_new->setReach($projectDefaultReach);
        $projectDefault_new->setPower1($projectDefaultPower1);
        $projectDefault_new->setPower2($projectDefaultPower2);
        $projectDefault_new->setTypeEvaluation($projectDefaultTypeEvaluation);
        $projectDefault_new->setDescriptionuf($projectDefaultDescriptionuf);

        //Changing some datas for clone
        $projectDefault_new->setGreenUser($user);
        $projectDefault_new->setName($projectDefaultName);
        //$projectDefault_new->setEnergy($projectDefault->getEnergy());
        $projectDefault_new->setBase(0);
        
        //Setting other datas as equal as original ProjectDefault
        $projectDefault_new->setPilote($projectDefaultPilote);
        $projectDefault_new->setPiloteDate($projectDefaultPiloteDate);
        $projectDefault_new->setPath($projectDefaultPath);
        $projectDefault_new->setActive($projectDefaultActive);
        $projectDefault_new->setCreatedAt($projectDefaultCreatedAt);
        $projectDefault_new->setUpdatedAt($projectDefaultUpdatedAt);
        
        $em->persist($projectDefault_new);
         
        return $projectDefault_new;
    }
    
}