<?php

namespace Arto\DesignBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Arto\DesignBundle\Entity\Operation;
use Arto\DesignBundle\Services\RequestHandler;

use Symfony\Component\HttpFoundation\Response;

class OperationController extends Controller
{
    private function getRequestHandler()
    {
        return $this->get('arto.design.requestHandler');
    }

    public function saveAction($id)
    {
        $request = $this->getRequest();
        $requestHandler = $this->getRequestHandler();
        $categories = $requestHandler->getCategorys();
        $em = $this->getDoctrine()->getEntityManager();
        $project = $em->getRepository('ArtoDesignBundle:Project')->find($id);
        $operations = $em->getRepository('ArtoDesignBundle:Operation')->findBy(array('project' => $project->getId()));

        foreach($operations as $operation) {
            $date = $request->get('operation_date_'.$operation->getId());
            $date = explode('/', $date);
            echo $date[2].'-'.$date[1].'-'.$date[0];
            $date = new \DateTime($date[2].'-'.$date[1].'-'.$date[0]);
            $operation->setDateAction($date);
            $operation->setDescription($request->get('operation_description_'.$operation->getId()));
            $em->persist($operation);
        }

        if ($request->get('operation_date') != null && $request->get('operation_description') != null) {
            $operation = new Operation();
            $operation->setProject($project);
            $date = $request->get('operation_date');
            $date = explode('/', $date);
            $date = new \DateTime($date[2].'-'.$date[1].'-'.$date[0]);
            $operation->setDateAction($date);
            $operation->setDescription($request->get('operation_description'));
            $em->persist($operation);
        }
        $em->flush();

        return $this->redirect($this->generateUrl('design_project_description', array('id' => $id, 'categories' => $categories)));
    }

}
