<?php

namespace Arto\DesignBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Arto\GreenBundle\Controller\TokenAuthenticatedController;
use Arto\DesignBundle\Services\RequestHandler;
use Arto\DesignBundle\Services\Calculator;
use n3b\Bundle\Util\HttpFoundation\StreamResponse\StreamResponse,
    n3b\Bundle\Util\HttpFoundation\StreamResponse\FileWriter,
    n3b\Bundle\Util\HttpFoundation\StreamResponse\StreamWriterWrapper;


class ExportController extends Controller implements TokenAuthenticatedController
{ 
    private function getRequestHandler()
    {
        return $this->get('arto.design.requestHandler');
    }
    
    private function getCalculator()
    {
        return $this->get('arto.design.calculator');
    }
    
    public function exportPDFAction($project)
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();
        $requestHandler = $this->getRequestHandler();
        $calculator = $this->getCalculator();
        $siteArtogreen = $this->getRequest()->getScheme() . '://' . $this->getRequest()->getHttpHost() . $this->getRequest()->getBasePath();
        
        $project = $request->get('project');

        $project = $em->getRepository('ArtoDesignBundle:Project')->find($project);
        $projectDefault = $project->getDefault();
        $urlLogoGraph = $siteArtogreen."/images/acv/logo.jpg";
        $projectPath = $project->getPath();
        $projectDefaultPath = $projectDefault->getPath();
        
        if($projectPath != ""){
            $urlPath = $siteArtogreen."/media/cache/standard/uploads/design/".$projectPath;
        }else{
            $urlPath = $siteArtogreen."/images/default.png";
        }
        
        if($projectDefaultPath != ""){
            $urlDefaultPath = $siteArtogreen."/media/cache/standard/uploads/design/".$projectDefaultPath;
        }else{
            $urlDefaultPath = $siteArtogreen."/images/default.png";
        }
        
        $categories = $requestHandler->getCategorys();
        $criterias = $requestHandler->getCriterias();
        $weights = $requestHandler->getWeights();
        $calculs = $requestHandler->getCalculs();
        
        $notes = $requestHandler->getArrayNotesByCriterias($project);
        $notesDefault = $requestHandler->getArrayNotesDefaultByCriterias($project);
        
        $typologie = $requestHandler->getTypologie($project);
        $objetEtude = $requestHandler->getObjetEtude($project);
        
        $actions = array();
        
        foreach($project->getNotes() as $helpnote) {
            if ($helpnote->getAction() != null) {
                if (!isset($actions[$helpnote->getCriteria()->getId()])) {
                    $actions[$helpnote->getCriteria()->getId()] = array();
                }
                $actions[$helpnote->getCriteria()->getId()][] = $helpnote->getAction();
            }
        }
        
        $qeb = $calculator->calculateQeb($project, false);
        $qeb = round($qeb);
        
        $qeb_default = $calculator->calculateQeb($projectDefault, false);
        $qeb_default = round($qeb_default);
        
        $highWeightCriterias = $requestHandler->getHighCriterias($project);
        
        $pilote_actions = $requestHandler->findAllActions($project);
        
        $projectId = $project->getId();
        
        $filename = 'export_'.$project->getName().'.pdf';
        
        $urlChart1 = $siteArtogreen."/design/project/chart/".$projectId."/false";
        $urlChart2 = $siteArtogreen."/design/project/chart2/".$projectId."/false";
        
        $percentCategories = $calculator->calculateCategoriesProfile($project, $categories, $criterias, false);
        $percentCategoriesDefault = $calculator->calculateCategoriesProfile($projectDefault, $categories, $criterias, false);
        
        $notesForWeightProfiles = $requestHandler->getNotes($project);
        $notesDefaultForWeightProfiles = $requestHandler->getNotes($projectDefault);
        
        $percentIndicateurs = $calculator->calculateWeightProfile($project, $categories, $weights, $weights, $calculs, $notesForWeightProfiles, false);
        $percentIndicateursDefault = $calculator->calculateWeightProfile($projectDefault, $categories, $weights, $weights, $calculs, $notesDefaultForWeightProfiles, false);
        
        $params = array(
            'project'    => $project,
            'projectDefault' => $projectDefault,
            'projectId' => $projectId,
            'typologie' => $typologie,
            'objetEtude' => $objetEtude,
            'urlLogoGraph' => $urlLogoGraph,
            'urlPath'=> $urlPath,
            'urlDefaultPath' => $urlDefaultPath,
            'categories' => $categories,
            'criterias' => $criterias,
            'notes' => $notes,
            'notes_default' => $notesDefault,
            'actions' => $actions,
            'pilote_actions' => $pilote_actions,
            'qeb' => $qeb,
            'qeb_default' => $qeb_default,
            'highCriterias' => $highWeightCriterias,
            'urlChart1' => $urlChart1,
            'urlChart2' => $urlChart2,
            'percentCategories' => $percentCategories,
            'percentCategoriesDefault' => $percentCategoriesDefault,
            'percentIndicateurs' => $percentIndicateurs,
            'percentIndicateursDefault' => $percentIndicateursDefault,
            'siteArtogreen' => $siteArtogreen
            );
        
        if ($request->get('pdf') == 1) {
            $html = $this->renderView('ArtoDesignBundle:Default:exportPDF_pdf.html.twig', $params);

            return new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
                200,
                array(
                    'Content-Type'          => 'application/pdf',
                    'Content-Disposition'   => 'attachment; filename='.$filename
                )
            );

        } else {
            return $this->render('ArtoDesignBundle:Default:exportPDF_pdf.html.twig', $params);
        }
    }
}

