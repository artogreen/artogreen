<?php

namespace Arto\DesignBundle\Services;

use Arto\DesignBundle\Entity\Product;
use Arto\DesignBundle\Entity\Project;
use Arto\DesignBundle\Entity\Folder;
use Arto\DesignBundle\Entity\Note;
use Arto\DesignBundle\Entity\AutoAction;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FileUploader {
    
    public function uploadFile($project, $file){
        if (isset($file) && isset($file['tmp_name'])) {
            if($file['name'] != ''){
                move_uploaded_file($file['tmp_name'], __DIR__.'/../../../../web/uploads/design/'.$file['name']);
                $project->setPath($file['name']);
            } 
        }
    }
    
}