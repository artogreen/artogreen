<?php

namespace Arto\DesignBundle\Services;

use Arto\DesignBundle\Entity\Product;
use Arto\DesignBundle\Entity\Project;
use Arto\DesignBundle\Entity\Folder;
use Arto\DesignBundle\Entity\Note;
use Arto\DesignBundle\Services\RequestHandler;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Calculator {
    protected $requestHandler;
    
    public function __construct(RequestHandler $requestHandler) {
        $this->requestHandler = $requestHandler;
    }
    
    public function calculateQeb($project, $simulated){
        $qeb = 0;
        if (!$project->getEnergy()) {
            foreach($project->getNotes() as $note) {
                if($simulated == true){
                    $qeb += (($note->getSimulatedValue()*$note->getCriteria()->getWeight())*($note->getCriteria()->getCategory()->getIndex()/200));
                }else{
                    $qeb += (($note->getValue()*$note->getCriteria()->getWeight())*($note->getCriteria()->getCategory()->getIndex()/200));
                }
            }
        } else {
            foreach($project->getNotes() as $note) {
                if($simulated == true){
                    $qeb += (($note->getSimulatedValue()*$note->getCriteria()->getWeight())*($note->getCriteria()->getCategory()->getEindex()/200));
                }else{
                    $qeb += (($note->getValue()*$note->getCriteria()->getWeight())*($note->getCriteria()->getCategory()->getEindex()/200));
                }    
            }
        }
        
        return $qeb;
    }
    
    public function isQuestion1($project){
        $requestHandler = $this->requestHandler;
        $question1 = false;
        $noteQuestion1 = $requestHandler->getNote($project, 1);
        
        if ($noteQuestion1 != null && $noteQuestion1->getValue()) {
            $question1 = true;
        }
        
        return $question1;
    }
    
    public function isQuestion2($project){
        $requestHandler = $this->requestHandler;
        $question2 = false;
         
        $criteria2Value = $requestHandler->getCriteriaValue($project, 2);
        $criteria6Value = $requestHandler->getCriteriaValue($project, 6);
        $criteria11Value = $requestHandler->getCriteriaValue($project, 11);
        $criteria12Value = $requestHandler->getCriteriaValue($project, 12);
        $criteria15Value = $requestHandler->getCriteriaValue($project, 15);
        $criteria24Value = $requestHandler->getCriteriaValue($project, 24);
        $criteria26Value = $requestHandler->getCriteriaValue($project, 26);
        $criteria27Value = $requestHandler->getCriteriaValue($project, 27);
        $criteria36Value = $requestHandler->getCriteriaValue($project, 36);

        $criteriaQuestion2 = $criteria2Value + $criteria6Value + $criteria15Value +
                $criteria11Value + $criteria12Value + $criteria24Value +
                $criteria26Value + $criteria27Value + $criteria36Value;

        if ($criteriaQuestion2 >= 6 && $criteria36Value == 1) {
            $question2 = true;
        }
         
        return $question2;
    }
    
    public function isQuestion3($project){
        $requestHandler = $this->requestHandler;
        $question3 = false;
        $noteQuestion3 = $requestHandler->getNote($project, 48);
        
        if ($noteQuestion3 != null && $noteQuestion3->getValue()) {
            $question3 = true;
        }
        
        return $question3;
    }
    
    public function isQuestion4($project){
        $requestHandler = $this->requestHandler;
        $question4 = false;
         
        $criteria13Value = $requestHandler->getCriteriaValue($project, 13);
        //$criteria14Value = $requestHandler->getCriteriaValue($project, 14);
        //$criteria15Value = $requestHandler->getCriteriaValue($project, 15);
        //$criteria21Value = $requestHandler->getCriteriaValue($project, 21);
        
        $criteriaQuestion4 = $criteria13Value;
        
        if ($criteriaQuestion4 == 1) {
            $question4 = true;
        } 
        
        return $question4;
    }
    
    public function isQuestion5($project){
        $requestHandler = $this->requestHandler;
        $question5 = false;
        
        $criteria32Value = $requestHandler->getCriteriaValue($project, 32);
        $criteria33Value = $requestHandler->getCriteriaValue($project, 33);
        $criteria34Value = $requestHandler->getCriteriaValue($project, 34);
        $criteria40Value = $requestHandler->getCriteriaValue($project, 40);
        
        $criteriaQuestion5 = $criteria32Value + $criteria33Value + $criteria34Value +
                $criteria40Value;
        
        if($criteriaQuestion5 == 4){
            $question5 = true;
        }
        
        return $question5;
    }
    
    public function isQuestion6($project){
        $requestHandler = $this->requestHandler;
        $question6 = false;
        
        $noteQuestion6 = $requestHandler->getNote($project, 50);
        
        if ($noteQuestion6 != null && $noteQuestion6->getValue()) {
            $question6 = true;
        }
        
        return $question6;
    }
    
    public function isQuestion7($project){
        $question7 = false;
        $projectDefault = $project->getDefault();
        
        $qeb = round($this->calculateQeb($project, false));
        $qebDefault = round($this->calculateQeb($projectDefault, true));
        $ecart = $qeb - $qebDefault;
        
        if ($qeb >= 12 && $ecart >= 1){
            $question7 = true;
        }
        
        return $question7;
    }
    
    /**
     *  Calculate the pourcentage for each categories
     *
     * @param type $project
     * @param type $categories
     * @param type $criterias
     * @param type $simulated
     * @return type
     */
    public function calculateCategoriesProfile($project, $categories, $criterias, $simulated) {

        $weight_total = 0;
        foreach($criterias as $criteria) {
            $weight_total += $criteria->getWeight();
        }

        $index = array();
        $coef = array();
        foreach($categories as $category) {
            $cat = $category->getId();
            $index[$cat] = $category->getIndex(); 
            $coef[$cat] = $index[$cat]/$weight_total;
        }

        $results = array();
        $results_weight = array();
        foreach($project->getNotes() as $note) {
            $cat =  $note->getCriteria()->getCategory()->getId();
            if($simulated == 'true'){
                if($project->getDefault() != null){
                    $results[$cat][] = $note->getSimulatedValue() * $note->getCriteria()->getWeight() * $coef[$cat];  
                }else{
                    $results[$cat][] = $note->getValue() * $note->getCriteria()->getWeight() * $coef[$cat];
                }
            }else{
                $results[$cat][] = $note->getValue() * $note->getCriteria()->getWeight() * $coef[$cat];  
            }
            
            $results_weight[$cat][] = $note->getCriteria()->getWeight() * $coef[$cat];
        }

        $total = array();
        $total_weight = array();
        $result_percentage = array();
        foreach($categories as $category) {
            $cat = $category->getId();
            if (isset($results[$cat])) {
                $total[$cat] = array_sum($results[$cat]);
                $total_weight[$cat] = array_sum($results_weight[$cat]);
                $result_percentage[$cat] = ($total[$cat]/$total_weight[$cat])*100;
            } else {
                $result_percentage[$cat] = 0;
            }
        }
        
        /* only use for debugging */
        
        /*
            //print_r($index);
            //print_r($coef);
            //print_r($weight_total);
            echo("Results[] : ");
            print_r($results);
            echo("\n");
            echo("Result_percentage[] : ");
            print_r($result_percentage);
            echo("\n");
            exit();
            */
        return $result_percentage;
    }
    
    /**
     * Calculate the weight for each categories
     *
     * @param type $project
     * @param type $categories
     * @param type $weights
     * @param type $weights
     * @param type $calculs
     * @param type $notes
     * @param type $simulated
     * @return type
     */
    public function calculateWeightProfile($project, $categories, $weights, $weights, $calculs, $notes, $simulated) {

        $calcul_totals = array();
        $calcul_weights = array();
        foreach($calculs as $calcul) {
            $criteria_id = $calcul->getCriteria()->getId();

            if (!isset($calcul_weights[$criteria_id])) {
                $calcul_weights[$criteria_id] = array();
            }

            $calcul_weights[$criteria_id][$calcul->getWeight()->getId()] = $calcul->getValue();

            if (!isset($calcul_totals[$criteria_id])) {
                $calcul_totals[$criteria_id] = $calcul->getValue();
            } else {
                $calcul_totals[$criteria_id] += $calcul->getValue();
            }
        }

        $notes_array = array();
        foreach($notes as $note) {
            $criteria_id = $note->getCriteria()->getId();
            if($simulated == 'true'){
                if($project->getDefault() != null){
                    $notes_array[$criteria_id] = $note->getSimulatedValue();
                }else{
                    $notes_array[$criteria_id] = $note->getValue();
                }
                
            }else{
                $notes_array[$criteria_id] = $note->getValue();
            }
            
        }

        $result = array();
        $results_total = array();
        foreach($categories as $category) {
            $category_id = $category->getId();

            foreach($category->getCriterias() as $criteria) {
                $criteria_id = $criteria->getId();
                if (isset($calcul_totals[$criteria_id])) {

                    foreach($weights as $weight) {
                        $weight_id = $weight->getId();
                        // check if the note exsit and is greater than 0
                        if (isset($notes_array[$criteria_id]) && $notes_array[$criteria_id]) {
                            $result[$category_id][$criteria_id][$weight_id] = (($calcul_weights[$criteria_id][$weight_id] * $criteria->getWeight()) / $calcul_totals[$criteria_id]);
                        } else {
                            $result[$category_id][$criteria_id][$weight_id] = 0;
                        }

                        if (!isset($results_total[$category_id][$weight_id])) {
                                $results_total[$category_id][$weight_id] = $result[$category_id][$criteria_id][$weight_id];
                        } else {
                                $results_total[$category_id][$weight_id] += $result[$category_id][$criteria_id][$weight_id];
                        }
                    }

                }
            }
        }


        $weights_total = array();
        foreach($results_total as $result_total) {
            foreach($weights as $weight) {
                $weight_id = $weight->getId();
                if (!isset($weights_total[$weight_id])) {
                    $weights_total[$weight_id] = $result_total[$weight_id];
                } else {
                    $weights_total[$weight_id] += $result_total[$weight_id];
                }
            }
        }

        $results_final = array();
        $weights_label = array();
        foreach($weights as $weight) {
            $weights_label[] = $weight->getLabel();
            $weight_id = $weight->getId();
            $rf = round(($weights_total[$weight_id] / $weight->getIndex()) * 100);
            $results_final[$weight_id] = ($rf <= 100) ? $rf : 100;
        }
        
        /* only use for debugging */
        /*
        if ($request->get('debug') == 1) {
            print_r($results_total);
            print_r($results_final);
            exit();
        }*/

        return $results_final;
    }
}
