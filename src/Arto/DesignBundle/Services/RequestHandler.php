<?php

namespace Arto\DesignBundle\Services;

use Arto\DesignBundle\Entity\Product;
use Arto\DesignBundle\Entity\Project;
use Arto\DesignBundle\Entity\Folder;
use Arto\DesignBundle\Entity\Note;
use Arto\DesignBundle\Entity\AutoAction;
use Arto\DesignBundle\Entity\Action;
use Arto\DesignBundle\Services\FileUploader;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RequestHandler {
    
    protected $em;
    protected $fileUploader;
    
    public function __construct(EntityManager $em, FileUploader $fileUploader) {
        $this->em = $em;
        $this->fileUploader = $fileUploader;     
    }
    
    private function dateToSQL($date) {
        $returnDate = null;

        $s = substr($date, 6, 4)
            ."-".substr($date, 3, 2)
            ."-".substr($date, 0, 2);

        try {
           $returnDate = new \DateTime($s);
        } catch (\Exception $e) {
            // Nothing to do
        }

        return $returnDate;
    }
    
    public function getWeights(){
        $em = $this->em;
        
        $weights = $em->getRepository('ArtoDesignBundle:Weight')->findAll();
        
        return $weights;
    }
    
    public function getCalculs(){
        $em = $this->em;
        
        $calculs = $em->getRepository('ArtoDesignBundle:Calcul')->findAll();
        
        return $calculs;
    }

    public function getCriteria($id){
        $em = $this->em;
        
        $criteria = $em->getRepository('ArtoDesignBundle:Criteria')->find($id);
        
        return $criteria;
    }
    
    public function getCriteriaValue($project, $criteria){
        $note = $this->getNote($project, $criteria);
        $noteValue = $note->getValue();
        
        return $noteValue;
    }
     
    public function getCriteriasByCategory($categoryId){
        $em = $this->em;
        
        $criterias = $em->getRepository('ArtoDesignBundle:Criteria')->findBy(
                array('category' => $categoryId));
        
        return $criterias;
    }
    
    public function getCriterias(){
        $em = $this->em;
        
        $criterias = $em->getRepository('ArtoDesignBundle:Criteria')->findAll();
        
        return $criterias;
    }
    
    public function getProject($id){
        $em = $this->em;
        
        $project = $em->getRepository('ArtoDesignBundle:Project')->find($id);
        
        return $project;
    }
    
    public function getCategory($id){
        $em = $this->em;
        
        $category = $em->getRepository('ArtoDesignBundle:Category')->find($id);
        
        return $category;
    }
    
    public function findProjectsWF($userId){
        $em = $this->em;
        
        $sql = "SELECT c FROM ArtoDesignBundle:Project c WHERE c.greenUser=:greenUser AND c.folder IS NULL AND c.default IS NOT NULL ORDER BY c.name";
        $query = $em->createQuery($sql)->setParameters(
                array("greenUser" => $userId));
        
        $projectsWF = $query->getResult();
        
        return $projectsWF;
    }
    
    public function findProjects($user){
        $em = $this->em;
        
        $projects = $em->getRepository('ArtoDesignBundle:Project')->findAllActive($user);
        
        return $projects;
    }
    
    public function findFolders($userId){
        $em  = $this->em;
        
        $folders = $em->getRepository('ArtoDesignBundle:Folder')->findBy(
                array('user' => $userId), array('name' => 'ASC'));
        
        return $folders;
    }
    
    public function getCategorys(){
        $em = $this->em;
        
        $categorys = $em->getRepository('ArtoDesignBundle:Category')->findAll();
        
        return $categorys;
    }
    
    public function createNote($project, $criteria){
        $em = $this->em;
        
        $note = new Note();
        $note->setProject($project);
        $note->setCriteria($criteria);
        $note->setValue(false);
        $note->setSimulatedValue(false);
        
        $em->persist($note);
        $em->flush();
    }
    
    public function createSpecialNote($project, $criteria){
        $em = $this->em;
        
        $note = new Note();
        $note->setProject($project);
        $note->setSimulatedValue(false);
        $note->setCriteria($criteria);
        $criteriaId = $criteria->getId();
        
        switch($criteriaId){
            case 1:
                $note->setValue(true);
                break;
            case 2:
                $note->setValue(true);
                break;
            case 3:
                $note->setValue(true);
                break;
            case 5:
                $note->setValue(true);
                break;
            case 7:
                $note->setValue(true);
                break;
            case 8:
                $note->setValue(true);
                break;
            case 11:
                $note->setValue(true);
                break;
            case 13:
                $note->setValue(true);
                break;
            case 14:
                $note->setValue(true);
                break;
            case 16:
                $note->setValue(true);
                break;
            case 22:
                $note->setValue(true);
                break;
            case 25:
                $note->setValue(true);
                break;
            case 31:
                $note->setValue(true);
                break;
            case 33:
                $note->setValue(true);
                break;
            case 36:
                $note->setValue(true);
                break;
            case 38:
                $note->setValue(true);
                break;
            case 43:
                $note->setValue(true);
                break;
            case 44:
                $note->setValue(true);
                break;
            case 47:
                $note->setValue(true);
                break;
            case 48:
                $note->setValue(true);
                break;
            default:
                $note->setValue(false);
                break;
        }
        
        $em->persist($note);
        $em->flush();
    }
    
    public function createShortNotes($project,$criteria){
        $em = $this->em;
        
        $criteriaId = $criteria->getId();
        
        $note = new Note();
        $note->setProject($project);
        $note->setSimulatedValue(false);
        $note->setCriteria($criteria);

        switch($criteriaId){
            case 2:
                $note->setValue(true);
                break;
            case 4:
                $note->setValue(true);
                break;
            case 9:
                $note->setValue(true);
                break;
            case 10:
                $note->setValue(true);
                break;
            case 12:
                $note->setValue(true);
                break;
            case 13:
                $note->setValue(true);
                break;
            case 14:
                $note->setValue(true);
                break;
            case 17:
                $note->setValue(true);
                break;
            case 18:
                $note->setValue(true);
                break;
            case 21:
                $note->setValue(true);
                break;
            case 23:
                $note->setValue(true);
                break;
            case 24:
                $note->setValue(true);
                break;
            case 25:
                $note->setValue(true);
                break;
            case 27:
                $note->setValue(true);
                break;
            case 28:
                $note->setValue(true);
                break;
            case 29:
                $note->setValue(true);
                break;
            case 32:
                $note->setValue(true);
                break;
            case 33:
                $note->setValue(true);
                break;
            case 34:
                $note->setValue(true);
                break;
            case 35:
                $note->setValue(true);
                break;
            case 38:
                $note->setValue(true);
                break;
            case 39:
                $note->setValue(true);
                break;
            case 40:
                $note->setValue(true);
                break;
            case 41:
                $note->setValue(true);
                break;
            case 42:
                $note->setValue(true);
                break;
            case 45:
                $note->setValue(true);
                break;
            case 46:
                $note->setValue(true);
                break;
            case 48:
                $note->setValue(true);
                break;
            case 49:
                $note->setValue(true);
                break;
            case 50:
                $note->setValue(true);
                break;
            default:
                $note->setValue(false);
                break;
        }
        
        $em->persist($note);
        $em->flush();
    }
    
    public function getNote($project, $criteria){
        $em = $this->em;
        
        if(is_int($criteria)){
            $criteriaId = $criteria;
        }else{
            $criteriaId = $criteria->getId();
        }
        
        if(is_int($project)){
           $projectId = $project;
        }else{
           $projectId = $project->getId();
        }
        
        $note = $em->getRepository('ArtoDesignBundle:Note')->findOneBy(
                    array('project' => $projectId, 'criteria' => $criteriaId));
        
        return $note;
    }
    
    public function createProject($name, $user, $typeEvaluation, $energy, $dossierId){
        $em = $this->em;
        $fileUploader = $this->fileUploader;
        
        $project_default = new Project();
        $project_default->setGreenUser($user);
        if($typeEvaluation == 'service'){
            $project_default->setName('Produit cible');
            $project_default->setPath("cible.jpg");
        }else{
            $project_default->setName('');
        }
        $project_default->setProjectName('');
        //$project_default->setEnergy($energy);
        $project_default->setBase(0);

        $project = new Project();
        $project->setGreenUser($user);
        $project->setName($name);
        $project->setProjectName('');
        $project->setEnergy($energy);
        $project->setBase(1);
        $project->setTypeEvaluation($typeEvaluation);
        $project->setDefault($project_default);

        $em->persist($project_default);
        $em->persist($project);
        
        $categorys = $this->getCategorys();
        
        foreach($categorys as $category){
            $categoryId = $category->getId();
            $criterias = $this->getCriteriasByCategory($categoryId);
            
            foreach($criterias as $criteria){
                if($typeEvaluation == "service"){
                    $this->createNote($project, $criteria);
                    $this->createSpecialNote($project_default, $criteria);
                }else if($typeEvaluation == "short"){
                    $this->createShortNotes($project, $criteria);
                    $this->createShortNotes($project_default, $criteria);
                }
                else{
                    $this->createNote($project, $criteria);
                    $this->createNote($project_default, $criteria);
                }
            }
        }
        
        
        
        if(isset($dossierId) && $dossierId !=''){
            $dossier = $em->getRepository('ArtoDesignBundle:Folder')->find($dossierId);

            $dossier->addProject($project); 
            $project->setFolder($dossier);

            $em->persist($dossier);
            $em->persist($project);
        }else{
            $project->setFolder(null);
            $em->persist($project);
        }
        
        $em->flush();
        
        return $project;
    }
    
    public function renameProject($projectId, $newName){
        $em = $this->em;
        
        $project = $this->getProject($projectId);
        
        $project->setName($newName);
        
        $em->persist($project);
        $em->flush();
    }
    
    public function deleteProject($projectId, $user, $special){
        $em = $this->em;
        
        $project = $this->getProject($projectId);
        $projectDefault = $project->getDefault();
        
        $isCorrectUser = $project->getGreenUser()->getId() == $user->getId();
        $isSpecial = isset($special) && $special != null;
        
        if ($isCorrectUser ||$isSpecial) {
            $em->remove($project);
           
            if($projectDefault != null){
                $em->remove($projectDefault);
            }
            
            $em->flush();
        }
    }
    
    public function findAutoAction($project, $criteria){
        $em = $this->em;
        
        $criteriaId = $criteria->getId();
        $projectId = $project->getId();
        
        $autoAction = $em->getRepository('ArtoDesignBundle:AutoAction')->findOneBy(
                    array('project' => $projectId, 'criteria' => $criteriaId));
        
        return $autoAction;
    }
    
     public function findAction($project, $criteria){
        $em = $this->em;
        
        $criteriaId = $criteria->getId();
        $projectId = $project->getId();
        
        $action = $em->getRepository('ArtoDesignBundle:Action')->findOneBy(
                    array('project' => $projectId, 'criteria' => $criteriaId));
        
        return $action;
    }
    public function createAutoAction($project, $criteria){
        $em = $this->em;
        
        $autoAction = new AutoAction();
        $autoAction->setProject($project);
        $autoAction->setCriteria($criteria);
        $autoAction->setAccepted(false);
        $autoAction->setDeployed(false);

        $em->persist($autoAction);
        $em->flush();
    }
    
    public function deleteAutoAction($autoAction){
        $em = $this->em;
        
        $em->remove($autoAction);
        $em->flush();
    }
    
    public function findAllAutoActions($project){
        $em = $this->em;
        
        $projectId = $project->getId();
        
        $autoActions = $em->getRepository('ArtoDesignBundle:AutoAction')->findBy(array('project' => $projectId));
        
        return $autoActions;
    }
    
    public function findAllActions($project){
        $em = $this->em;
        
        $projectId = $project->getId();
        
        $actions = $em->getRepository('ArtoDesignBundle:Action')->findBy(array('project' => $projectId));
        
        return $actions;
        
    }
    
    public function getUsersByProject($project){
        $em = $this->em;
        
        $projectId = $project->getId();
        
        $users = $em->getRepository('ArtoDesignBundle:User')->findBy(array('project' => $projectId));
        
        return $users;
    }
    
    public function getOperationsByProject($project){
        $em = $this->em;
        
        $projectId = $project->getId();
        
        $operations = $em->getRepository('ArtoDesignBundle:Operation')->findBy(array('project' => $projectId));
        
        return $operations;
    }
    
    public function saveGeneralites($project, $default, $projectName, $descriptionUF, $uploadedFile){
        $em = $this->em;
        $fileUploader = $this->fileUploader;
        
        if ($default != null) {
            $project = $project->getDefault();
        }
        
        $project->setProjectName($projectName);
        $project->setDescriptionuf($descriptionUF);
        $fileUploader->uploadFile($project, $uploadedFile);
        
        $em->persist($project);
        $em->flush();
    }
    
    public function savePilote($project, $default, $pilote, $piloteDate){
        $em = $this->em;
        
        if ($default != null) {
            $project = $project->getDefault();
        }

        $project->setPilote($pilote);

        if ($piloteDate != null) {
            $piloteDateToSQL = $this->dateToSQL($piloteDate);
            $project->setPiloteDate($piloteDateToSQL);
        }
        
        $em->persist($project);
        $em->flush();
    }
    
    public function saveReglementation($project, $default, $rohs, $reach, $deee, $battery, $batteryExtract){
        $em = $this->em;
        
        if ($default != null) {
            $project = $project->getDefault();
        }

        $project->setRohs($rohs);
        $project->setReach($reach);
        $project->setDeee($deee);
        $project->setBattery($battery);
        $project->setBatteryExtract($batteryExtract);

        $em->persist($project);
        $em->flush();
    }
    
    public function saveTypeEvaluation($project, $default, $typeEvaluation){
        $em = $this->em;
        
        if ($default != null) {
            $project = $project->getDefault();
        }
        
        $project->setTypeEvaluation($typeEvaluation);

        $em->persist($project);
        $em->flush();
    }
    
    public function saveConsommation($project, $default, $power1, $power2, $energy, $energyType){
        $em = $this->em;
        
        if ($default != null) {
            $project = $project->getDefault();
        } 
        
        $power1 = str_replace(",",".",$power1);
        $power2 = str_replace(",", ".",$power2);

        $project->setEnergy($energy);
        $project->setEnergyType($energyType);
        $project->setPower1($power1);
        $project->setPower2($power2);

        $em->persist($project);
        $em->flush();
    }
    
    public function saveReference($project, $default, $projectDefaultName, $uploadedFile){
        $em = $this->em; 
        $fileUploader = $this->fileUploader;
        
        $realProject = $this->getProject($project);
        
        if ($default != null) {
            $theProject = $realProject->getDefault();
        }
        
        $theProject->setName($projectDefaultName);
        $fileUploader->uploadFile($theProject, $uploadedFile);
        
        $em->persist($theProject);
        $em->flush();
    }
    
    public function getNotes($project){
        $em = $this->em;
        
        $projectId = $project->getId();
        
        $notes = $em->getRepository('ArtoDesignBundle:Note')->findBy(array('project' => $projectId));
        
        return $notes;
    }
    
    public function getArrayNotesByCriterias($project){
        $notes = array();
        
        foreach($project->getNotes() as $note) {
            $criteria = $note->getCriteria();
            $criteriaId = $criteria->getId();
            $notes[$criteriaId] = $note->getValue();
        }
        
        return $notes;
    }
    
    public function getArrayActionsByCriterias($project){
        $actions = array();
        
        foreach($project->getNotes() as $note) {
            $criteria = $note->getCriteria();
            $criteriaId = $criteria->getId();
            $actions[$criteriaId] = $note->getAction();
        }
        
        return $actions;
    }
    
    public function getArrayCommentsByCriterias($project){
        $comments = array();
        
        foreach($project->getNotes() as $note) {
            $criteria = $note->getCriteria();
            $criteriaId = $criteria->getId();
            $comments[$criteriaId] = $note->getComment();
        }
        
        return $comments;
    }
    
    public function getArrayCommentsDefaultByCriterias($project){
        $comments_default = array();
        
        foreach($project->getNotes() as $note) {
            $criteria = $note->getCriteria();
            $criteriaId = $criteria->getId();
            $comments_default[$criteriaId] = $note->getCommentDefault();
        }
        
        return $comments_default;
    }
    
    public function getArrayNotesDefaultByCriterias($project){
        $notes_default = array();
        
        foreach($project->getDefault()->getNotes() as $note) {
            $notes_default[$note->getCriteria()->getId()] = $note->getValue();
        }
        
        return $notes_default; 
    }
    
    /**
     * Les criteres de poids >= 7 qui sont côtés Faible
     * @param type $project
     */
    public function getHighCriterias($project){
        $em = $this->em;
        $projectId = $project->getId();
         
        $notes = $em->getRepository('ArtoDesignBundle:Note')->findBy(
                array( 'project' => $projectId, 'value' => false));
        
        $results = array();
        
        foreach($notes as $note){
            $criteria = $note->getCriteria();
            if($criteria->getWeight() >= 10){
                array_push($results, $criteria);
            }
        }
        
        return $results;
    }
    
    public function saveCategory($project, $category, $request){
        $em = $this->em;
        
        $criterias = $category->getCriterias();
        $projectId = $project->getId();
        $projectDefault = $project->getDefault();
        $projectDefaultId = $projectDefault->getId();
        
        foreach($criterias as $criteria)
        {
            $criteriaId = $criteria->getId();
            $c = $request->get('criteria_'.$criteriaId, 0);
            $cd = $request->get('criteria_default_'.$criteriaId, 0);

            $note = $this->getNote($projectId, $criteriaId);
            $note_default = $this->getNote($projectDefaultId, $criteriaId);

            if ($note == null) {
                $note = new Note();
                $note->setProject($project);
                $note->setCriteria($criteria);
            }

            if ($note_default == null) {
                $note_default = new Note();
                $note_default->setProject($project->getDefault());
                $note_default->setCriteria($criteria);
            }

            $note->setValue($c);
            $note_default->setValue($cd);
            $em->persist($note);
            $em->persist($note_default);
        }
        
        $em->flush();
    }
    
    public function saveAction($project, $criteria, $action){
        $em = $this->em;
        
        $note = $this->getNote($project, $criteria);

        if ($note == null) {
            $note = new Note();
            $note->setProject($project);
            $note->setCriteria($criteria);
        }

        $note->setAction($action);
        
        $em->persist($note);
        $em->flush();
    }
    
    public function saveComment($project, $criteria, $comment, $commentDefault){
        $em = $this->em;
        
        $note = $this->getNote($project, $criteria);

        if ($note == null) {
            $note = new Note();
            $note->setProject($project);
            $note->setCriteria($criteria);
        }

        $note->setComment($comment);
        $note->setCommentDefault($commentDefault);

        $em->persist($note);
        $em->flush();
    }
    
    public function searchCriteria($key){
        $em = $this->em;
        
        $newSearchKey = '%'.$key.'%';
        
        $sql = "SELECT c FROM ArtoDesignBundle:Criteria c WHERE c.label LIKE :key";
        $query = $em->createQuery($sql)->setParameter('key', $newSearchKey);

        $results = $query->getResult();
        
        return $results;
    }
    
    public function savePlanAction($project, $request){
        $em = $this->em;
        
        $notes = $this->getNotes($project);
        
        foreach($notes as $note) {
            if($note->getAction() != null){
                $criteria = $note->getCriteria();
                
                $actionFound = $this->findAction($project, $criteria);
                
                if(!$actionFound){
                    $action = new Action();
                    
                    $piloteName = $request->get('pilote_new');
                    $piloteDate = $this->dateToSQL($request->get('pilote_date_new'));

                    $action->setProject($project);
                    $action->setCriteria($criteria);
                    $action->setPilote($piloteName);
                    $action->setPiloteDate($piloteDate);

                    $em->persist($action);     
                }else{
                    $actionFoundId = $actionFound->getId();
                    
                    $piloteName = $request->get('pilote_'.$actionFoundId);
                    $piloteDate = $this->dateToSQL($request->get('pilote_date_'.$actionFoundId));
                    
                    $actionFound->setPilote($piloteName);
                    $actionFound->setPiloteDate($piloteDate);
                    
                    $em->persist($actionFound);
                }
            }
        }  
        
        $em->flush();
    }
    
    public function saveBilanInteractif($project, $request)
    {
        $em = $this->em;
        
        $autoActions = $this->findAllAutoActions($project);
        
        foreach($autoActions as $autoAction) {
           $criteria = $autoAction->getCriteria();
           $criteriaId = $criteria->getId();
           
           $autoActionFound = $this->findAutoAction($project, $criteria);
           $note = $this->getNote($project, $criteria);
          
           if(!$autoActionFound){
               $autoActionNew = new AutoAction();
               
               $accepted = $request->get('criteria_change_accepted_'.$criteriaId);
               
               $note->setSimulatedValue($accepted);
               
               $autoActionNew->setProject($project);
               $autoActionNew->setCriteria($criteria);
               $autoActionNew->setAccepted($accepted);
               
               $em->persist($note);
               $em->persist($autoActionNew);
           }else{
               $autoActionAccepted = $request->get('criteria_change_accepted_'.$criteriaId);
               
               $note->setSimulatedValue($autoActionAccepted);
               
               $autoActionFound->setAccepted($autoActionAccepted);
               
               $em->persist($note);
               $em->persist($autoActionFound);
           }
        }
               
        $em->flush(); 
    }
    
    public function visitAmeliorations($projectId){
        $em = $this->em;
        
        $project = $this->getProject($projectId);
        $firstVisit = $project->getFirstVisit();
        
        $notes = $project->getNotes();
        
        foreach($notes as $note){
            $noteValue = $note->getValue();
            $note->setSimulatedValue($noteValue);
            $em->persist($note);
        }
        
        $em->flush();
        
        $result = array(
            'firstVisit' => $firstVisit
        );
        
        return $result;
    }
    
    public function unVisitAmeliorations($projectId){
        $em = $this->em;
        $project = $this->getProject($projectId);
        
        $project->setFirstVisit(false);
        
        $em->persist($project);
        $em->flush();
        
        return $project;
    }
    
    public function hideAutoActions($projectId, $categoryId){
        $em = $this->em;
        
        $criterias = $this->getCriteriasByCategory($categoryId);
        $project = $this->getProject($projectId);
        $isDeployed = false;
        
        //Pour chaque critère
        foreach($criterias as $criteria){
            $autoAction = $this->findAutoAction($project, $criteria);
            
            //Si une autoAction a été définie
            if($autoAction != null){
               //Si visible on cache 
               if($autoAction->getDeployed() == true){
                    $isDeployed = false;
                    $autoAction->setDeployed(false);
               }
               //Si caché on rend visible
               else{
                    $isDeployed = true;
                    $autoAction->setDeployed(true);
               }
            
               $em->persist($autoAction); 
            }
        }
        
        $em->flush();
        
        $result = array(
            'isDeployed' => $isDeployed
        );
        
        return $result;
    }
    
    public function setSimulatedQeb($projectId, $simulatedQeb){
        $em = $this->em;
        
        $project = $this->getProject($projectId);
        
        $project->setSimulatedQeb($simulatedQeb);
        
        $em->persist($project);
        $em->flush();
    }
    
    public function getTypologie($project){
        $em = $this->em;
        
        $result = '';
        
        if($project->getEnergy()){
            $result = "Produit/Service à usage énérgétique";
        }else{
            $result = "Produit/Service à usage non énérgétique";
        }
        
        return $result;
    }
    
    public function getObjetEtude($project) {
        $result = '';

        $typeEvaluation = $project->getTypeEvaluation();

        switch ($typeEvaluation) {
            case "product":
                $result = "Evaluer l'éco-conception d'un produit (version longue - tps 2h30), consiste à comparer un produit nouveau par rapport"
                    . " à un autre produit existant, concurrent ou un autre concept sur 50 critères ou aspects environnementaux et sociaux"
                    . " puis déboucher sur un produit nouveau éco-conçu de manière éco-responsable." . PHP_EOL;
                break;
            case "short":
                $result = "Evaluer l'éco-conception d'un produit (version courte - tps : 1h), consiste à comparer un produit nouveau par rapport"
                    . " à un autre produit existant, concurrent ou un autre concept sur 20 critères ou aspects environnementaux significatifs puis"
                    . " déboucher sur un produit nouveau éco-conçu. (les 30 questions restantes respectent les critères demandés)." . PHP_EOL;
                break;
            case "service":
                $result = "Evaluer un produit existant par rapport au produit cible. L'évaluation est effectuée selon les Aspects Environnementaux Significatifs du produit dits Hotspots à fort impact du produit sur l'environnement."
                    . " dont les aspects environnementaux et sociaux sont connus et évalués puis déboucher sur l'éco-industrialisation du produit"
                    . " existant évalué." . PHP_EOL . "L’évaluation « FORT » ou « FAIBLE » affichée à chaque étape, dans la colonne « Produit cible » ne doit pas être modifiée.";
                break;
            case "achat":
                $result = "Evaluer un produit acheté, consiste à comparer un produit acheté A par rapport à un autre produit existant ou acheté B puis"
                    . " déboucher sur l'évaluation de la durabilité du produit acheté A." . PHP_EOL;
                break;
        }

        return $result;
    }
    
    public function checkPreuves($project, $category){
        $em = $this->em;
        
        $result = true;
        
        $projectDefault = $project->getDefault();
        $projectDefaultId = $projectDefault->getId();
        $projectId = $project->getId();
        $categoryId = $category->getId();
        
        
        $criterias = $this->getCriteriasByCategory($categoryId);
        foreach($criterias as $criteria){
            //Pour chaque critère on prend les 2 notes : normale et default
            $criteriaId = $criteria->getId();
            
            $note = $em->getRepository('ArtoDesignBundle:Note')->findOneBy(
                    array('project' => $projectId, 'criteria' => $criteriaId));
            
            $noteDefault = $em->getRepository('ArtoDesignBundle:Note')->findOneBy(
                    array('project' => $projectDefaultId, 'criteria' => $criteriaId));
            
            //Si une des deux notes est à Fort, on vérifie si les 2 commentaires sont vides
            if($note->getValue() == true || $noteDefault->getValue() == true){
                //Si les 2 commentaires sont vides -> on a une coche non remplie donc à remplir
                if($note->getComment() == '' && $note->getCommentDefault() == ''){
                    $result = false;
                }
            }
        }
        
        return $result;
    }

}
