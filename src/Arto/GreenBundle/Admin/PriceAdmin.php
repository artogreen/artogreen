<?php

/*
 * This file is part of the Sonata package.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arto\GreenBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Knplabs\Bundle\MenuBundle\MenuItem;

class PriceAdmin extends Admin
{
    protected $baseRouteName = 'admin_green_price';

    protected $maxPerPage = 50;

    public function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('product', null, array('label' => 'Produit'))
            ->add('company', null, array('label' => 'Capital'))
            ->add('station', null, array('label' => 'Nombre de poste'))
            ->add('license', null, array('label' => 'Licence'))
            ->add('price', null, array('label' => 'Prix'))
            ->add('createdAt', 'date')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )))
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->with('General')
                ->add('product', null, array('label' => 'Produit'))
                ->add('company', null, array('label' => 'Capital'))
                ->add('station', null, array('label' => 'Nombre de poste'))
                ->add('license', null, array('label' => 'Licence'))
                ->add('price', null, array('label' => 'Prix'))
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid
            ->add('product', null, array('label' => 'Produit'))
            ->add('company', null, array('label' => 'Capital'))
            ->add('station', null, array('label' => 'Nombre de poste'))
            ->add('license', null, array('label' => 'Licence'))
        ;
    }
}