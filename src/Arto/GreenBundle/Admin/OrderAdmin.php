<?php

/*
 * This file is part of the Sonata package.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arto\GreenBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

use Knplabs\Bundle\MenuBundle\MenuItem;

class OrderAdmin extends Admin
{
    protected $baseRouteName = 'admin_order';

    protected $maxPerPage = 50;

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('order');
        $collection->add('send');
    }

    public function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('createdAt', 'date')
            ->addIdentifier('ref')
            ->add('company', null, array('label' => 'Société'))
            ->add('price', null, array('label' => 'Prix Total HT'))
            ->add('status', null, array('label' => 'Status'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'delete' => array(),
                )))
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General')
                ->add('ref')
                ->add('status', 'sonata_type_model', array('label' => 'Etat','required' => false), array('edit' => 'standard'))
            ->end()
            ->with('Client')
                ->add('user', 'sonata_type_model', array('label' => 'Client', 'required' => false), array('edit' => 'standard'))
                ->add('email')
                ->add('address', null, array('label' => 'Adresse'))
                ->add('city', null, array('label' => 'Ville'))
                ->add('postcode', null, array('label' => 'Code postal'))
            ->end()
            ->with('Details')
                ->add('lines', 'sonata_type_collection', array(), array('edit' => 'inline', 'inline' => 'table'))
                ->add('price', null, array('label' => 'Prix total'))
            ->end();

    }


    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid
            ->add('id')
            ->add('ref')
            ->add('status')
        ;
    }
}