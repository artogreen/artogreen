<?php

/*
 * This file is part of the Sonata package.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arto\GreenBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

use Knplabs\Bundle\MenuBundle\MenuItem;

class OrderLineAdmin extends Admin
{
    protected $baseRouteName = 'admin_order_line';

    protected $maxPerPage = 50;

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('order');
        $collection->add('send');
    }

    public function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('product', null, array('label' => 'Produit'))
            ->add('training', null, array('label' => 'Formation'))
            ->add('quantity', null, array('label' => 'Quantité'))
            ->add('price', null, array('label' => 'Prix'))
            ->add('duration', null, array('label' => 'temps'))
            ->add('price', null, array('label' => 'Prix Total HT'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'delete' => array(),
                )))
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('product', null, array('label' => 'Produit', 'required' => false))
            ->add('training', null, array('label' => 'Formation', 'required' => false))
            ->add('quantity', null, array('label' => 'Quantité'))
            ->add('price', null, array('label' => 'Prix'))
            ->add('duration', null, array('label' => 'temps'))
            ->add('price', null, array('label' => 'Prix Total HT'))
        ;

    }


    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid
            ->add('product')
        ;
    }
}