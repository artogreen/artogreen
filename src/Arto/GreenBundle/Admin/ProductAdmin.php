<?php

/*
 * This file is part of the Sonata package.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arto\GreenBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Knplabs\Bundle\MenuBundle\MenuItem;

class ProductAdmin extends Admin
{
    protected $baseRouteName = 'admin_green_product';

    protected $maxPerPage = 50;

    public function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('name', null, array('label' => 'Nom'))
            ->add('category', null, array('label' => 'Catégorie'))
            ->add('pack', null, array('label' => 'Est un pack'))
            ->add('createdAt', 'date')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )))
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->with('General')
                ->add('category', 'sonata_type_model', array('label' => 'Categorie'), array('edit' => 'standard'))
                ->add('name', null, array('label' => 'Nom'))
                ->add('info', null, array('label' => 'Info'))
                ->add('intro', 'ckeditor', array('config_name' => 'arto', 'label' => 'Introduction'))
                ->add('description', 'ckeditor', array('config_name' => 'arto', 'label' => 'Description'))
                ->add('pack', null, array('label' => 'Est un pack ?', 'required' => false))
            ->with('General')
                ->add('similars', 'sonata_type_model', array('expanded' => true, 'label' => 'Produits similaires ou produits du pack'))
            ->end()
            ->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid
            ->add('category')
        ;
    }

}