<?php

/*
 * This file is part of the Sonata package.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arto\GreenBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Knplabs\Bundle\MenuBundle\MenuItem;

class TrainingAdmin extends Admin
{
    protected $baseRouteName = 'admin_green_training';

    protected $maxPerPage = 50;

    public function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('label1', null, array('label' => 'Titre 1'))
            ->addIdentifier('product', null, array('label' => 'Produit'))
            ->add('company', null, array('label' => 'Capital'))
            ->add('price1', null, array('label' => 'Prix 1'))
            ->add('price2', null, array('label' => 'Prix 2'))
            ->add('createdAt', 'date')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )))
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->with('General')
                ->add('product', null, array('label' => 'Produit'))
                ->add('company', null, array('label' => 'Capital'))
                ->add('label1', null, array('label' => 'Titre 1'))
                ->add('price1', null, array('label' => 'Prix 1'))
                ->add('label2', null, array('label' => 'Titre 2'))
                ->add('price2', null, array('label' => 'Prix 2'))
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid
            ->add('product', null, array('label' => 'Produit'))
            ->add('company', null, array('label' => 'Capital'))
        ;
    }
}