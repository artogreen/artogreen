<?php

namespace Arto\GreenBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class UserAdmin extends Admin
{

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('username')
            ->add('email')
            ->add('firstname')
            ->add('lastname')
            ->add('tel')
            ->add('active')
        ;

        /*
        if ($this->isGranted('ROLE_ALLOWED_TO_SWITCH')) {
            $listMapper
                ->add('impersonating', 'string', array('template' => 'SonataUserBundle:Admin:Field/impersonating.html.twig'))
            ;
        }
        */
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper
            ->add('username')
            ->add('active')
            ->add('email')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Profil')
                ->add('firstname', null, array('required' => false, 'label' => 'Prénom'))
                ->add('lastname', null, array('required' => false, 'label' => 'Nom'))
                ->add('address', null, array('required' => false, 'label' => 'Adresse'))
                ->add('postcode', null, array('required' => false, 'label' => 'Code postal'))
                ->add('city', null, array('required' => false, 'label' => 'Ville'))
                ->add('tel', null, array('required' => false, 'label' => 'Téléphone'))
            ->end()
            ->with('Société')
                ->add('siret', null, array('required' => false, 'label' => 'SIRET'))
                ->add('company', null, array('required' => false, 'label' => 'Société'))
            ->end()
            ->with('Authentification')
                ->add('username')
                ->add('email')
                ->add('password', 'text')
                ->add('active', null, array('required' => false))
            ->end()

        ;

        /*
        if (!$this->getSubject()->hasRole('ROLE_SUPER_ADMIN')) {
            $formMapper->with('Management')
                ->add('roles', 'sonata_security_roles', array(
                    'expanded' => true,
                    'multiple' => true,
                    'required' => false
                ))
                ->add('locked', null, array('required' => false))
                ->add('expired', null, array('required' => false))
                ->add('enabled', null, array('required' => false))
                ->add('credentialsExpired', null, array('required' => false))
            ->end();
        }
        */

    }

}
