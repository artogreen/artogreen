<?php

/*
 * This file is part of the Sonata package.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Arto\GreenBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Knplabs\Bundle\MenuBundle\MenuItem;

class ContactAdmin extends Admin
{
    protected $baseRouteName = 'admin_contact';

    protected $maxPerPage = 50;

    public function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('email')
            ->add('firstname')
            ->add('lastname')
            ->add('subject')
            ->add('followedBy')
            ->add('processed')
            ->add('createdAt', 'date')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )))
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->with('General')
            ->add('subject', 'sonata_type_model', array(), array('edit' => 'standard'))
            ->add('firstname', null, array('label' => 'Prénom'))
            ->add('lastname', null, array('label' => 'Nom'))
            ->add('email')
            ->add('company', null, array('label' => 'Société'))
            ->add('website', null, array('label' => 'Site web'))
            ->add('tel', null, array('label' => 'Téléphone'))
            ->add('message', 'textarea')
            ->end()
            ->with('Details')
            ->add('followedBy', null, array('label' => 'Suivi par'))
            ->add('comment', 'textarea', array('label' => 'Commentaire'))
            ->add('processed', null, array('label' => 'Traité ?'))
            ->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid
            ->add('subject')
            ->add('email')
            ->add('followedBy')
            ->add('processed');
        ;
    }

}