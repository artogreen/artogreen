<?php

namespace Arto\GreenBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Arto\GreenBundle\Controller\TokenAuthenticatedController;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Arto\GreenBundle\Entity\Token;

class TokenListener
{
    protected $em;
    protected $securityContext;

    public function __construct(EntityManager $em, SecurityContextInterface $securityContext)
    {
        $this->em = $em;
        $this->securityContext = $securityContext;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        $accessGranted = true;

        /*
         * $controller passed can be either a class or a Closure. This is not usual in Symfony2 but it may happen.
         * If it is a class, it comes in array format
         */
        if (!is_array($controller)) {
            return;
        }

        $request = $event->getRequest();

        $route  = $request->attributes->get('_route');

        if ($route == '_internal') {
            return;
        }

        if ($route == 'user_clear') {
            $clearToken = $event->getRequest()->getSession()->get('auth_token');

            if ($clearToken != null) {
                $clearTokens = $this->em->getRepository('ArtoGreenBundle:Token')->findBy(array(
                    'token' => $clearToken
                ));

                if (count($clearTokens) > 0) {
                    foreach($clearTokens as $clearToken) {
                        $this->em->remove($clearToken);
                        $this->em->flush();
                    }
                }
            }
        }

        if ($controller[0] instanceof TokenAuthenticatedController) {


            // Matched route


            $route_tab = explode('_', $route);
            $product_path = null;
            if (count($route_tab) > 0 && isset($route_tab[0])) {
                $product_path = $route_tab[0];
            }

            $user = $this->securityContext->getToken()->getUser();

            $product =  $this->em->getRepository('ArtoGreenBundle:Product')->findOneByPath($product_path);

            if ($user != null && $product != null) {

                // Check if the user have a license for the requested product
                $license = $this->em->getRepository('ArtoGreenBundle:License')->findActiveForProduct($user, $product);

                if ($license == null) {

                    $accessGranted = false;

                } else {

                    $this->em->getRepository('ArtoGreenBundle:Token')->deleteTimeout($user);

                    // Get the token in db for the current token in session and the license to use
                    $currentToken = $event->getRequest()->getSession()->get('auth_token');

                    $token = $this->em->getRepository('ArtoGreenBundle:Token')->findOneBy(array(
                        'token' => $currentToken,
                        'license' => $license->getId()
                    ));

                    if ($token != null) {
                        // Access is granted, we set the token in session
                        $event->getRequest()->getSession()->set('auth_token', $token->getToken());

                    } else {

                        // Check if there is still some token available compared to the number of license
                        $activeTokens = $this->em->getRepository('ArtoGreenBundle:Token')->findBy(array(
                            'license' => $license->getId()
                        ));

                        if ((count($activeTokens) + 1) > $license->getQuantity()) {

                            $accessGranted = false;

                        } else {

                            // Find token that maybe used for an other license
                            $tokens = $this->em->getRepository('ArtoGreenBundle:Token')->findBy(array(
                                'token' => $currentToken
                            ));

                            // Removed tokens used for previous licenses
                            foreach($tokens as $token) {
                                if ($token->getLicense()->getId() != $license->getId()) {
                                    $this->em->remove($token);
                                }
                            }
                            $this->em->flush();

                            // Create new token for requested license
                            $newTokenStr = substr(strtoupper(md5( uniqid('arto', true) )), 0, 128 );

                            $newToken = new Token();
                            $newToken->setToken($newTokenStr);
                            $newToken->setLicense($license);

                            $this->em->persist($newToken);
                            $this->em->flush();

                            // Access is granted, we set the new token in session
                            $event->getRequest()->getSession()->set('auth_token', $newToken->getToken());
                        }
                    }
                }
            } else {
                $accessGranted = false;
            }
        }

        if (!$accessGranted) {
            throw new HttpException(200, null, null, array('Location' => '/no-more-license'));
        }
    }

    /*
    public function onKernelResponse(FilterResponseEvent $event)
    {
        // check to see if onKernelController marked this as a token "auth'ed" request
        if (!$token = $event->getRequest()->getSession()->get('auth_token')) {
            return;
        }

        $response = $event->getResponse();

        // create a hash and set it as a response header
        $hash = $token;
        $response->headers->set('X-CONTENT-HASH', $hash);
    }
    */

}