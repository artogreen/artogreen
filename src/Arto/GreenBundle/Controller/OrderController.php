<?php

namespace Arto\GreenBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Arto\GreenBundle\Entity\Order;
use Arto\GreenBundle\Entity\OrderLine;
use Symfony\Component\HttpFoundation\Response;


class OrderController extends Controller
{
    public function basketAction()
    {
        $params = array();

        $user = $this->get('security.context')->getToken()->getUser();

        $em = $this->getDoctrine()->getEntityManager();
        $status = $em->getRepository('ArtoGreenBundle:Status')->findOneByCode('ORDER_CREATED');
        $order = $em->getRepository('ArtoGreenBundle:Order')->findOneBy(array(
            'status' => $status->getId(),
            'user' => $user->getId()
        ));

        if ($order != null) {
            $total = 0;
            foreach($order->getLines() as $line) {
                $total += $line->getPrice();
            }
            $order->setPrice($total);
            $em->persist($order);
            $em->flush();

            // SYSTEM PAY
            $key = "3223191566931454";
            // Param init
            $params["vads_site_id"] = 57143914;
            $montant_en_euro = 9.99 ;
            $params["vads_amount"] = 100*($order->getPrice()*1.196) ; // in cents
            $params["vads_currency"] = "978"; // norme ISO 4217
            $params["vads_ctx_mode"] = "TEST";
            $params["vads_page_action"] = "PAYMENT";
            $params["vads_action_mode"] = "INTERACTIVE"; // card input by payment plateforme
            $params["vads_payment_config"]= "SINGLE";
            $params["vads_version"] = "V2";
            $ts = time();
            $params["vads_trans_date"] = gmdate("YmdHis", $ts);
            $params["vads_trans_id"] = str_pad($order->getId(), 6, "0", STR_PAD_LEFT);

            $params["vads_validation_mode"] = 0;

            $params["vads_url_success"] = $this->generateUrl('green_payment_success', array(), true);
            $params["vads_url_referral"] = $this->generateUrl('green_payment_success', array(), true);
            $params["vads_url_refused"] = $this->generateUrl('green_payment_success', array(), true);

            $params["vads_url_error"] = $this->generateUrl('green_payment_error', array(), true);
            $params["vads_url_cancel"] = $this->generateUrl('green_payment_cancel', array(), true);

            // Automated return
            $params["vads_redirect_success_timeout"] = 0;
            $params["vads_redirect_success_message"] = "Vous allez être redirigé vers ArtoGREEN";
            $params["vads_redirect_error_timeout"] = 0;
            $params["vads_redirect_error_message"] = "Vous allez être redirigé vers ArtoGREEN";
            $params["vads_return_mode"] = 'GET';

            // Generate signature
            ksort($params) ;
            $contenu_signature = "" ;
            foreach ($params as $nom => $valeur)
            {
                   $contenu_signature .= $valeur. "+" ;
            }
            $contenu_signature .= $key; // On ajoute le certificat a la fin
            $params["signature"] = sha1($contenu_signature);
        }


        return $this->render('ArtoGreenBundle:Order:basket.html.twig', array('order' => $order, 'params' => $params));
    }

    public function basketAddAction() {
        $user = $this->get('security.context')->getToken()->getUser();
        $request = $this->getRequest();

        $product_id = $request->get('product');

        if ($user != null && $product_id != null) {

            $em = $this->getDoctrine()->getEntityManager();
            $status = $em->getRepository('ArtoGreenBundle:Status')->findOneByCode('ORDER_CREATED');
            $order = $em->getRepository('ArtoGreenBundle:Order')->findOneBy(array(
                'status' => $status->getId(),
                'user' => $user->getId()
            ));

            if ($order == null) {
                $order = new Order();
                $order->setUser($user);
                $order->setStatus($status);
                $order->setCompany($user->getCompany());
                $order->setEmail($user->getEmail());
                $order->setAddress($user->getAddress());
                $order->setCity($user->getCity());
                $order->setPostcode($user->getPostcode());
                $order->setTel($user->getTel());
            } else {

                $lines = $em->getRepository('ArtoGreenBundle:OrderLine')->findBy(array(
                    'product' => $product_id
                ));

                foreach($lines as $line) {
                    $em->remove($line);
                }

                $em->flush();
            }

            $product = $em->getRepository('ArtoGreenBundle:Product')->find($product_id);

            $price = $em->getRepository('ArtoGreenBundle:Price')->findOneBy(array(
                'product' => $product->getId(),
                'license' => $request->get('license'),
                'station' => $request->get('station'),
            ));

            $line = new OrderLine();
            $line->setOrder($order);
            $line->setProduct($product);
            $line->setQuantity($price->getStation());
            $line->setDuration($price->getLicense());
            $line->setPrice($price->getPrice());

            $em->persist($line);

            if ($product->getPack()) {
                foreach($product->getSimilars() as $similar) {
                    $line = new OrderLine();
                    $line->setOrder($order);
                    $line->setProduct($similar);
                    $line->setQuantity($price->getStation());
                    $line->setDuration($price->getLicense());
                    $line->setPrice(0);

                    $em->persist($line);
                }

            }

            if ($request->get('training1')) {

                $training = $em->getRepository('ArtoGreenBundle:Training')->findOneBy(array(
                    'product' => $request->get('product'),
                    'company' => 1
                ));

                $tline1 = new OrderLine();
                $tline1->setOrder($order);
                $tline1->setProduct($product);
                $tline1->setTraining($training);
                $tline1->setTrainingType(1);
                $tline1->setQuantity(1);
                $tline1->setPrice($training->getPrice1());

                $em->persist($tline1);
            }
            if ($request->get('training2')) {

                $training = $em->getRepository('ArtoGreenBundle:Training')->findOneBy(array(
                    'product' => $request->get('product'),
                    'company' => 1
                ));

                $tline2 = new OrderLine();
                $tline2->setOrder($order);
                $tline2->setProduct($product);
                $tline2->setTraining($training);
                $tline2->setTrainingType(2);
                $tline2->setQuantity(1);
                $tline2->setPrice($training->getPrice2());

                $em->persist($tline2);
            }

            $em->persist($order);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('green_basket'));
    }

    public function basketRemoveAction() {
        $request = $this->getRequest();
        $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getEntityManager();

        $line = $em->getRepository('ArtoGreenBundle:OrderLine')->find($request->get('id'));

        if ($line != null && $line->getOrder()->getUser()->getId() == $user->getId()) {
            $em->remove($line);

            if ($line->getProduct()->getPack()) {
                foreach($line->getProduct()->getSimilars() as $similar) {
                    $sline = $em->getRepository('ArtoGreenBundle:OrderLine')->findOneBy(array(
                        'order' => $line->getOrder()->getId(),
                        'product' => $similar->getId()
                    ));
                    $em->remove($sline);
                }
            }

            $em->flush();
        }

        return $this->redirect($this->generateUrl('green_basket'));
    }

    public function basketDeleteAction() {
        $request = $this->getRequest();
        $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getEntityManager();

        $order = $em->getRepository('ArtoGreenBundle:Order')->find($request->get('id'));

        if ($order != null && $order->getUser()->getId() == $user->getId()) {
            $em->remove($order);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('green_basket'));
    }

    public function invoiceAction($uniq)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $status = $em->getRepository('ArtoGreenBundle:Status')->findOneByCode('PAYMENT_ACCEPTED');

        $invoice = $em->getRepository('ArtoGreenBundle:Order')->findOneBy(array(
            'uniq' => $uniq,
            'status' => $status->getId(),
            'user' => $user->getId()
        ));

        if ($invoice != null) {
            $html = $this->renderView('ArtoGreenBundle:Order:invoice.html.twig', array('invoice' => $invoice));

            return new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
                200,
                array(
                    'Content-Type'          => 'application/pdf',
                    //'Content-Disposition'   => 'attachment; filename="ArtoACV_etiquette.pdf"'
                )
            );
        }
    }
}
