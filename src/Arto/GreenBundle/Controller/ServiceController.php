<?php

namespace Arto\GreenBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class ServiceController extends Controller
{

    public function indexAction($slug)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $section = $em->getRepository('ArtoGreenBundle:ServiceSection')->findOneBySlug($slug);

        return $this->render('ArtoGreenBundle:Service:index.html.twig', array(
            'section' => $section
        ));
    }

    public function pageAction($slug)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $page = $em->getRepository('ArtoGreenBundle:ServicePage')->findOneBySlug($slug);

        return $this->render('ArtoGreenBundle:Service:page.html.twig', array(
            'page' => $page
        ));
    }
}
