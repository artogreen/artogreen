<?php

namespace Arto\GreenBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Arto\GreenBundle\Bank\Signature;
use Arto\GreenBundle\Entity\License;


class PaymentController extends Controller
{

    public function paymentAction() {

        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        $uniq = $request->get('uniq');
        $rmethod = $request->get('payment_method');

        $order = $em->getRepository('ArtoGreenBundle:Order')->findOneByUniq($uniq);
        $method = $em->getRepository('ArtoGreenBundle:Payment')->findOneByCode($rmethod);

        if ($order != null && $method != null) {

            if ($method->getCode() == 'CHECK') {

                $status = $em->getRepository('ArtoGreenBundle:Status')->findOneByCode('PAYMENT_IN_PROCESS');
                $order->setStatus($status);
                $order->setPayment($method);
                $em->persist($order);
                $em->flush();

                return $this->render('ArtoGreenBundle:Order:accept_bycheck.html.twig', array(
                    'order' => $order
                ));
            }

            if ($method->getCode()  == 'CB') {

                $status = $em->getRepository('ArtoGreenBundle:Status')->findOneByCode('PAYMENT_IN_PROCESS');
                $order->setStatus($status);
                $em->persist($order);
                $em->flush();

                return $this->render('ArtoGreenBundle:Order:payment.html.twig', array(
                    'order' => $order,
                    'params' => $params
                ));
            }
        }

        return $this->redirect($this->generateUrl('green_basket'));
    }



    public function successAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        $params = $request->query->all();

        // Get Order
        $order = $em->getRepository('ArtoGreenBundle:Order')->find($params['vads_trans_id']);

        // Certificat
        $key = "3223191566931454";

        if (Signature::check($params, $key) == 'true') {

            // Control payment result
            if($params['vads_result'] == "00") {
                switch ($params['vads_auth_mode']) {
                    case "FULL":
                        //echo "---------------------------------------------------------------------------------------------<br/>";
                        //echo "Paiement accepté pour le montant total de la transaction. <br/>";
                        //echo "vads_result=00 et vads_auth_mode=FULL  ( consulter la documentation pour plus d'information ).<br/>";
                        //echo "---------------------------------------------------------------------------------------------<br/>";
                        $code = 'ACCEPTED';
                        break;
                    case "MARK":
                        //echo "------------------------------------------------------------------------------------------------------------------------------<br/>";
                        //echo "Paiement accepté liée à une empreinte de la transaction  ( remise en banque >6 jours ). <br/>";
                        //echo "vads_result=00 et vads_auth_mode=MARK  ( consulter la documentation pour plus d'information ).<br/>";
                        //echo "ATTENTION le Paiement pourra être refusé ultérieurement lors de l'autorisation pour le montant total de la transaction. <br/>";
                        //echo "-------------------------------------------------------------------------------------------------------------------------------<br/>";
                        $code = 'ACCEPTED';
                    break;

                }

                $status = $em->getRepository('ArtoGreenBundle:Status')->findOneByCode('PAYMENT_ACCEPTED');

                $this->processPayment($order);

            } else {
                $code = 'REFUSED';
                $status = $em->getRepository('ArtoGreenBundle:Status')->findOneByCode('PAYMENT_REJECTED');
            }
        } else {
            $code = 'ERROR';
            $status = $em->getRepository('ArtoGreenBundle:Status')->findOneByCode('PAYMENT_ERROR_API');
        }

        $order->setStatus($status);
        $order->setPayment($em->getRepository('ArtoGreenBundle:Payment')->findOneByCode('CB'));
        $em->persist($order);
        $em->flush();

        return $this->render('ArtoGreenBundle:Order:success.html.twig', array('order' => $order, 'code' => $code));

    }

    public function errorAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        $params = $request->query->all();

        // Get Order
        $order = $em->getRepository('ArtoGreenBundle:Order')->find($params['vads_trans_id']);

        // Certificat
        $key = "3223191566931454";

        if (Signature::check($params, $key) == 'true') {
            $status = $em->getRepository('ArtoGreenBundle:Status')->findOneByCode('PAYMENT_ERROR_API');
            $order->setStatus($status);
            $em->persist($order);
            $em->flush();
        }

        return $this->render('ArtoGreenBundle:Order:error.html.twig', array('order' => $order));
    }

    public function cancelAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        $params = $request->query->all();

        // Get Order
        $order = $em->getRepository('ArtoGreenBundle:Order')->find($params['vads_trans_id']);

        // Certificat
        $key = "3223191566931454";

        if (Signature::check($params, $key) == 'true') {
            $status = $em->getRepository('ArtoGreenBundle:Status')->findOneByCode('PAYMENT_CANCEL');
            $order->setStatus($status);
            $em->persist($order);
            $em->flush();
        }

        return $this->render('ArtoGreenBundle:Order:cancel.html.twig', array('order' => $order));
    }

    private function processPayment($order)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $user = $this->get('security.context')->getToken()->getUser();

        if ($order != null) {

            foreach($order->getLines() as $line) {
                // Create license only for potential products (not for training or packs)
                if ($line->getProduct() != null
                    && $line->getTraining() == null
                    && $line->getDuration() > 0
                    && !$line->getProduct()->getPack()) {

                    $license = $em->getRepository('ArtoGreenBundle:License')->findOneBy(array(
                        'user' => $user->getId(),
                        'product' => $line->getProduct()->getId()
                    ));

                    if ($license == null) {
                        $license = new License();
                        $license->setUser($user);
                        $license->setProduct($line->getProduct());
                        $license->setQuantity($line->getQuantity());
                        $license->setDuration($line->getDuration());
                    } else {
                        $license->setQuantity($license->getQuantity() + $line->getQuantity());
                        $license->setDuration($line->getDuration());
                    }

                    $today = new \DateTime();
                    $end = $today->add(new \DateInterval('P'.$license->getDuration().'M'));
                    $license->setEndAt($end);

                    $em->persist($license);
                    $em->flush();
                }
            }

            $message = \Swift_Message::newInstance(null, null, 'text/html')
                    ->setSubject('Facture N°'.$order->getRef().' ArtoGREEN')
                    ->setFrom(array('no-reply@artogreen.com' => 'ArtoGREEN'))
                    ->setTo($user->getEmail())
                    ->setBody($this->renderView('ArtoGreenBundle:Mail:invoice.html.twig', array('user' => $user, 'order' => $order)));
                $this->get('mailer')->send($message);

        }

        //return $this->render($tpl, array('order' => $order, 'result' => $result));
    }
}
