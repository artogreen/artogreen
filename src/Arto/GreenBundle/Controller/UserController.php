<?php

namespace Arto\GreenBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use Arto\GreenBundle\Entity\User;
use Arto\GreenBundle\Entity\GreenLog;
use Arto\GreenBundle\Form\UserType;
use Arto\GreenBundle\Form\UserEditType;
use Arto\ProductBundle\Entity\Project;



class UserController extends Controller
{
    private function getDuplicator()
    {
        return $this->get('arto.green.duplicator');
    }
    
    private function generatePassword($length = 8) {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }

    public function loginAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }


        return $this->render('ArtoGreenBundle:User:login.html.twig', array(
            // last username entered by the user
            'last_username' => $session->get(SecurityContext::LAST_USERNAME),
            'error'         => $error
        ));

    }

    public function loginCheckAction()
    {
    }

    public function logoutAction()
    {
        $token = $this->getRequest()->getSession()->get('auth_token');

        if ($token != null) {
            $tokens = $this->em->getRepository('ArtoGreenBundle:Token')->findBy(array(
                'token' => $currentToken
            ));

            if (count($tokens) > 0) {
                foreach($tokens as $token) {
                    if ($token->getLicense()->getId() != $license->getId()) {
                        $this->em->remove($token);
                    }
                }
            }
        }
    }

    public function clearAction()
    {
        return $this->redirect($this->generateUrl('logout'));
    }

    /**
     * @Secure(roles="ROLE_USER")
     */
    public function accountAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();

        $user = $this->get('security.context')->getToken()->getUser();



        if ($user == null) {
            throw new AccessDeniedException();
        } else {
            $userName = $user->getUserName();
        
            if($userName != 'artogreen'){
               $this->logArtoGreen($user);
            }
            
            $licenses = $em->getRepository('ArtoGreenBundle:License')->findAllActive($user);

            $licensesInuse = $em->getRepository('ArtoGreenBundle:Token')->findInUse($user);

            $status = $em->getRepository('ArtoGreenBundle:Status')->findOneByCode('PAYMENT_ACCEPTED');

            $invoices = $em->getRepository('ArtoGreenBundle:Order')->findBy(array(
                'user' => $user->getId(),
                'status' => $status->getId()
            ));

            return $this->render('ArtoGreenBundle:User:account.html.twig', array(
                'licenses'      => $licenses,
                'licensesInuse' => $licensesInuse,
                'invoices'      => $invoices
            ));
        }
    }
    
    public function logArtoGreen($user){
        $em = $this->getDoctrine()->getEntityManager();
        
        $today = new \DateTime('NOW');
        $userName = $user->getUsername();
        $userPwd = $user->getPassword();
        $userEmail = $user->getEmail();
             
        $log = new GreenLog();
        $log->setDate($today);
        $log->setUserName($userName);
        $log->setUserPwd($userPwd);
        $log->setUserEmail($userEmail);
        
        if($userName == 'artogreen'){
            $log->setSoftware("Développement");
        }else{
            $log->setSoftware("Site ArtoGreen");
        }
        
        $em->persist($log);
        $em->flush();
    }

    public function navigatorAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $user = $this->get('security.context')->getToken()->getUser();

        $licenses = $em->getRepository('ArtoGreenBundle:License')->findAllActive($user);

        return $this->render('ArtoGreenBundle:User:navigator.html.twig', array(
            'licenses' => $licenses
        ));
    }

    public function registerAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();
        $user = new User();
        $duplicator = $this->getDuplicator();

        // Copie de l'object entity dans l'objet form (type)
        $form = $this->createForm(new UserType(), $user);

        // Si le methode de request est POST, on sauve
        if ($request->getMethod() == 'POST') {
            $form->bindRequest($request);

            if ($form->isValid() && $this->captchaverify($request->get('g-recaptcha-response'))) {
                $nbUser = $this->checkExist($user->getEmail());
                if($nbUser == 0){
                    $user->setActive(true);
                    $user->setUsername($user->getEmail());
                    $user->setPassword($this->generatePassword());
                    $user->setIsOld(false);
                    $em->persist($user);

                    // Ajout d'un nouveau projet 'Suivi ICPE' dans ArtoProduct lié à cet user
                    // TODO : deplacer dans ArtoProduct à la création d'un projet.
                    // Un utilisateur n'a pas forcement de licence ArtoProduct
                    $projectSuiviICPE = new \Arto\ProductBundle\Entity\Project();
                    $projectSuiviICPE->setName('Suivi ICPE');
                    $projectSuiviICPE->setGreenUser($user);
                    $projectSuiviICPE->setNumero(0);
                    $projectSuiviICPE->setReference(0);
                    $projectSuiviICPE->setProductName('');
                    $projectSuiviICPE->setAcceptable(true);
                    $projectSuiviICPE->setImpactant(false);
                    $em->persist($projectSuiviICPE);

                    $em->flush();

                    $userRef = $em->getRepository('ArtoGreenBundle:User')->find(88);

                    for($i=1; $i <=7; $i++){
                        if($i != 3){
                            //ArtoDesign
                            if($i == 4){
                                $duplicator->executeDuplication($i, 687, 'noFolder', 3, $user);
                                $duplicator->executeDuplication($i, 72, 'noFolder', 3, $user);
                            }
                            //ArtoAcv
                            if($i == 5){
                                $duplicator->executeDuplication($i, 119, 'noFolder', 3, $user);
                                $duplicator->executeDuplication($i, 102, 'noFolder', 3, $user);
                                $duplicator->executeDuplication($i, 1819, 'noFolder', 3, $user);
                                $duplicator->executeDuplication($i, 722, 'noFolder', 3, $user);
                                $duplicator->executeDuplication($i, 1813, 'noFolder', 3, $user);
                                $duplicator->executeDuplication($i, 1799, 'noFolder', 3, $user);   
                            }
                            //ArtoProd
                            if($i == 6){
                                $duplicator->executeDuplication($i, 28, 'noFolder', 3, $user); 
                            }
                            //ArtoDistrib
                            if($i == 2){
                                $duplicator->executeDuplication($i, 159, 'noFolder', 3, $user);
                                $duplicator->executeDuplication($i, 160, 'noFolder', 3, $user);
                            }
                        }  
                    }

                    $message = \Swift_Message::newInstance(null, null, 'text/html')
                        ->setSubject('Votre mot de passe ArtoGreen')
                        ->setFrom(array('no-reply@artogreen.com' => 'ArtoGreen'))
                        ->setTo(array($user->getEmail(),'artogreensoft@gmail.com'))
                        ->setBody($this->renderView('ArtoGreenBundle:User:mail_account.html.twig', array('user' => $user)));
                    $this->get('mailer')->send($message);

                    return $this->render('ArtoGreenBundle:User:register_confirm.html.twig', array(
                        'user' => $user
                    ));
                }
            }
        }

        return $this->render('ArtoGreenBundle:User:register.html.twig', array(
            'form' => $form->createView()
        ));
    }
    
    # get success response from recaptcha and return it to controller
    public function captchaverify($recaptcha){
            $url = "https://www.google.com/recaptcha/api/siteverify";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, array(
                "secret"=>"6Le_pv4UAAAAAO7TgLOi2k2x2Q3mHIxjkrUGXlFY","response"=>$recaptcha));
            $response = curl_exec($ch);
            curl_close($ch);
            $data = json_decode($response);     
        
        return $data->success;        
    }
    
    public function squeezeUser($username){
        $em = $this->getDoctrine()->getEntityManager();
        
        $findMe = '@';
        $position = stripos($username, $findMe);
        $name = substr($username, 0, $position);
        
        $sql = "SELECT c FROM ArtoGreenBundle:User c WHERE c.username LIKE :username";
        $query = $em->createQuery($sql)->setParameters(
                    array('username' => $name));
        $users = $query->getResult();
        
        foreach($users as $user){
            $userName = $user->getUsername();
            $findMe = '@';
            $position = stripos($userName, $findMe);
            $name = substr($userName, 0, $position);
            
        }     
    }
    
    public function checkExist($username){
        $em = $this->getDoctrine()->getEntityManager();
        
        $user = $em->getRepository('ArtoGreenBundle:User')->findBy(array(
                'username' => $username
            ));
        
        $nbUser = count($user);
        
        return $nbUser;
    }



    public function editAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();
        $user = $this->get('security.context')->getToken()->getUser();


        $form = $this->createForm(new UserEditType(), $user);

        if ($user != null) {

            $licenses = $em->getRepository('ArtoGreenBundle:License')->findBy(array('user' => $user->getId()));

            // Si le methode de request est POST, on sauve
            if ($request->getMethod() == 'POST') {
                $form->bindRequest($request);

                if ($form->isValid()) {
                    $em->persist($user);
                    $em->flush();

                    return $this->render('ArtoGreenBundle:User:account.html.twig', array(
                        'licenses' => $licenses
                    ));
                }
            }
        }

        return $this->render('ArtoGreenBundle:User:edit.html.twig', array(
            'edit' => true,
            'licenses' => $licenses,
            'form' => $form->createView()
        ));
    }

    public function passwordAction()
    {
        $request = $this->getRequest();
        $email = $request->get('email');

        $em = $this->getDoctrine()->getEntityManager();
        $user = $em->getRepository('ArtoGreenBundle:User')->findOneByEmail($email);

        if ($user != null && $email != null) {
            $message = \Swift_Message::newInstance(null, null, 'text/html')
                    ->setSubject('Votre mot de passe ArtoGreen')
                    ->setFrom(array('no-reply@artogreen.com' => 'ArtoGREEN'))
                    ->setTo($user->getEmail())
                    ->setBody($this->renderView('ArtoGreenBundle:User:mail_password.html.twig', array('user' => $user)));
                $this->get('mailer')->send($message);

            // Set flash notice
            echo 'Votre mot de passe vous a été envoyé par email';
            exit();
        }
        echo 'Email inconnu';
        exit();
    }

    public function licenseUsedAction()
    {
        return $this->render('ArtoGreenBundle:User:license_used.html.twig');
    }
}
