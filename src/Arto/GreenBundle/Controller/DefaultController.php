<?php

namespace Arto\GreenBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Arto\AcvBundle\Entity\Project;
use Arto\AcvBundle\Entity\Folder;
use Arto\AcvBundle\Entity\Log;
use Arto\GreenBundle\Entity\GreenLog;
use Doctrine\Common\Collections\ArrayCollection;
use n3b\Bundle\Util\HttpFoundation\StreamResponse\StreamResponse,
    n3b\Bundle\Util\HttpFoundation\StreamResponse\FileWriter,
    n3b\Bundle\Util\HttpFoundation\StreamResponse\StreamWriterWrapper;


class DefaultController extends Controller
{
    private function getDuplicator()
    {
        return $this->get('arto.green.duplicator');
    }
    
    private function getRequestHandler()
    {
        return $this->get('arto.green.requestHandler');
    }

    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $sections = $em->getRepository('ArtoGreenBundle:ServiceSection')->findAll();

        $categories1 = $em->getRepository('ArtoGreenBundle:ServiceCategory')->findBy(array('section' => $sections[0]));
        $categories2 = $em->getRepository('ArtoGreenBundle:ServiceCategory')->findBy(array('section' => $sections[1]));

        return $this->render('ArtoGreenBundle:Default:index.html.twig', array(
            'sections' => $sections,
            'categories1' => $categories1,
            'categories2' => $categories2
        ));
    }

    public function subnavServiceAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $sections = $em->getRepository('ArtoGreenBundle:ServiceSection')->findAll();

        $section = $sections[0];
        $id = $section->getId();
        $categories1 = $em->getRepository('ArtoGreenBundle:ServiceCategory')->findBy(array('section' => $id));

        $section = $sections[1];
        $id = $section->getId();
        $categories2 = $em->getRepository('ArtoGreenBundle:ServiceCategory')->findBy(array('section' => $id));

        return $this->render('ArtoGreenBundle:Default:_subnav_service.html.twig', array(
            'categories1' => $categories1,
            'categories2' => $categories2
        ));
    }

    public function subnavHomeAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $pages = $em->getRepository('ArtoGreenBundle:Page')->findAll();

        return $this->render('ArtoGreenBundle:Default:_subnav_home.html.twig', array(
            'pages' => $pages
        ));
    }

    public function faqAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $faqs = $em->getRepository('ArtoGreenBundle:Faq')->findAll();

        return $this->render('ArtoGreenBundle:Default:faq.html.twig', array(
            'faqs' => $faqs
        ));
    }
    
    public function cgvAction(){
        return $this->render('ArtoGreenBundle:Default:cgv.html.twig');
    }
    
    public function cguAction(){
        return $this->render('ArtoGreenBundle:Default:cgu.html.twig');
    }
    
    public function copyProjectAction(){
        $requestHandler = $this->getRequestHandler();
        
        $users = $requestHandler->findAllUsers();
        $softwares = $requestHandler->findAllSoftwares();
        $userLetters = $requestHandler->findUserLetters();
           
        return $this->render('ArtoGreenBundle:Default:copyProject.html.twig', array(
            'users' => $users,
            'softwares' => $softwares,
            'userLetters' => $userLetters
        ));          
    }
    
    public function codeFoldersAction(){
        $requestHandler = $this->getRequestHandler();
        
        $users = $requestHandler->findAllUsers();
        $softwares = $requestHandler->findAllSoftwares();
        $userLetters = $requestHandler->findUserLetters();
        
        return $this->render('ArtoGreenBundle:Default:codeFolders.html.twig', array(
            'users' => $users,
            'softwares' => $softwares,
            'userLetters' => $userLetters
        ));          
    }
    
    public function myCodeFoldersAction(){
        $requestHandler = $this->getRequestHandler();
        
        $users = $requestHandler->findAllUsers();
        $user = $this->get('security.context')->getToken()->getUser();
        $userId = $user->getId();
        
        $licenses = $requestHandler->findLicensesForUser($userId);
        $licenses = new ArrayCollection($licenses);
         
        $iterator = $licenses->getIterator();
        $iterator->uasort(function ($a, $b) {
            return ($a->getProduct()->getName() < $b->getProduct()->getName()) ? -1 : 1;
        });
         
        $licenses = new ArrayCollection(iterator_to_array($iterator));
        
        return $this->render('ArtoGreenBundle:Default:myCodeFolders.html.twig', array(
            'users' => $users,
            'softwares' => $licenses
        ));          
    }
    
    public function checkAccessKeyAction(){
        $request = $this->getRequest();
        $password = $request->get('password');
        
        if($password == 'artogreen'){
            $requestHandler = $this->getRequestHandler();
        
            $users = $requestHandler->findAllUsers();
            $user = $this->get('security.context')->getToken()->getUser();
            $userId = $user->getId();

            $licenses = $requestHandler->findLicensesForUser($userId);
            $licenses = new ArrayCollection($licenses);

            $iterator = $licenses->getIterator();
            $iterator->uasort(function ($a, $b) {
                return ($a->getProduct()->getName() < $b->getProduct()->getName()) ? -1 : 1;
            });

            $licenses = new ArrayCollection(iterator_to_array($iterator));
            return $this->render('ArtoGreenBundle:Default:myCodeFolders.html.twig', array(
                'users' => $users,
                'softwares' => $licenses
            ));         
        }else{
            $user = $this->get('security.context')->getToken()->getUser();
            $em = $this->getDoctrine()->getEntityManager();
        
            $licenses = $em->getRepository('ArtoGreenBundle:License')->findAllActive($user);

            $licensesInuse = $em->getRepository('ArtoGreenBundle:Token')->findInUse($user);

            $status = $em->getRepository('ArtoGreenBundle:Status')->findOneByCode('PAYMENT_ACCEPTED');

            $invoices = $em->getRepository('ArtoGreenBundle:Order')->findBy(array(
                'user' => $user->getId(),
                 'status' => $status->getId()
             ));
            
             return $this->render('ArtoGreenBundle:User:account.html.twig', array(
                'licenses'      => $licenses,
                'licensesInuse' => $licensesInuse,
                'invoices'      => $invoices
             )); 
        }
    }
    
    public function getUsersByLetterAction(){
         $request = $this->getRequest();
         $requestHandler = $this->getRequestHandler();
         
         $letter = $request->get('letter');
         
         $result = '<option selected="selected" value="">- Choisir -</option>';
         
         $users = $requestHandler->findUsersByLetter($letter);
         
         foreach($users as $user){
             $name = $user->getUsername();
             $id = $user->getId();
             $result = $result.'<option value="'.$id.'">'.$name.'</option>';
         }
         
         $json = json_encode($result);
         $response = new Response($json);
         return $response;        
    }
    
    public function userGetSoftwaresAction(){
         $request = $this->getRequest();
         $requestHandler = $this->getRequestHandler();
      
         $user = $request->get('user');
         
         $result = '<option selected="selected" value="">- Choisir -</option>';
         
         $licenses = $requestHandler->findLicensesForUser($user);
         $licenses = new ArrayCollection($licenses);
         
         $iterator = $licenses->getIterator();
         $iterator->uasort(function ($a, $b) {
            return ($a->getProduct()->getName() < $b->getProduct()->getName()) ? -1 : 1;
         });
         
         $licenses = new ArrayCollection(iterator_to_array($iterator));
         
         foreach($licenses as $license){
             $name = $license->getProduct()->getName();
             $id = $license->getProduct()->getId();
             $result = $result.'<option value="'.$id.'">'.$name.'</option>';
         }
          
         $json = json_encode($result);
         $response = new Response($json);
         return $response;  
    }
    
    public function getUserCodeAction(){
        $request = $this->getRequest();
        $requestHandler = $this->getRequestHandler();
         
        $user = $request->get('user');
         
        $result = $requestHandler->findCodeForUser($user);
         
        $json = json_encode($result);
        $response = new Response($json);
        return $response;
    }
    
    public function renewAction(){
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();
        
        $userId = $request->get('user');
        $endDate = $request->get('endDate');
        
        $user = $em->getRepository('ArtoGreenBundle:User')->find($userId);
        
        
        return $this->redirect($this->generateUrl('green_gestion_licences'));
    }
    
    public function userFindTargetUsersAction(){
        $requestHandler = $this->getRequestHandler();
           
        $result = '<option selected="selected" value="">- Choisir -</option>';
        
        $users = $requestHandler->findAllUsers();
        
        foreach ($users as $user) {
            $userName = $user->getUsername();
            $userId = $user->getId();
            $result = $result . '<option value="' . $userId . '">' . $userName . '</option>';
        }

        $json = json_encode($result);
        $response = new Response($json);
        return $response;  
    }
    
    public function userGetProjectsAction(){
        $request = $this->getRequest();
        $requestHandler = $this->getRequestHandler();
        
        $user = $request->get('user');
        $softwareId = $request->get('software');
        $folderId = $request->get('folder');
        
        $result = '<option selected="selected" value="">- Choisir -</option>';
        
        $softwareName = $requestHandler->getSoftwareName($softwareId);
          
        $projects = $requestHandler->findProjectsByFolder($user, $softwareName, $folderId);
         
        if($projects != null){
           foreach($projects as $project){
             $name = $project->getName();
             $id = $project->getId();
             $result = $result.'<option value="'.$id.'">'.$name.'</option>';
           } 
        }
        
        $json = json_encode($result);
        $response = new Response($json);
        return $response;  
    }
    
    public function userGetFoldersAction(){
        $request = $this->getRequest();      
        $requestHandler = $this->getRequestHandler();
        
        $user = $request->get('user');
        $softwareId = $request->get('software');
        $search = $request->get('search');
        
        $folders = null;
        
        $result = '<option selected="selected" value="choose">- Choisir -</option>';
       
        $softwareName = $requestHandler->getSoftwareName($softwareId);
        
        //Si pas dans page codeFolders.html.twig, lister tous les dossiers
        if($search == 'no'){
            $result = $result.'<option value="noFolder">Sans dossier</option>';
            $folders = $requestHandler->findFoldersByUser($user, $softwareName); 
        }
        //Sinon, lister les dossiers codés
        else{
            $folders = $requestHandler->findFoldersByUser($user, $softwareName, "yes");
        }
          
        if($folders != null){
           foreach($folders as $folder){
             $name = $folder->getName();
             $id = $folder->getId();
             $result = $result.'<option value="'.$id.'">'.$name.'</option>';
           } 
        }
        
         $json = json_encode($result);
         $response = new Response($json);
         return $response; 
    }
    
    public function folderGetCodeAction(){
        $request = $this->getRequest();
        $requestHandler = $this->getRequestHandler();
        
        $folderId = $request->get('folder');
        $softwareId = $request->get('software');
        
        $softwareName = $requestHandler->getSoftwareName($softwareId);
        $code = $requestHandler->findCodeForFolder($folderId, $softwareName); 
        
        $result = '<label name="project[codes]" class="codes" style="font-weight : bold;">'.$code.'</label>';
        
        $json = json_encode($result);
        $response = new Response($json);
        return $response;  
    }
    
    public function copyProjectsAction() {
        $em = $this->getDoctrine()->getEntityManager();
        $duplicator = $this->getDuplicator();
        $request = $this->getRequest();

        $projects = $request->get('projects');

        foreach ($projects as $theProject) {
            if (isset($theProject['targetUser']) && $theProject['targetUser'] != '') {
                $projectId = $theProject['targetProject'];
                $sourceUser = $theProject['user'];
                $targetUser = $em->getRepository('ArtoGreenBundle:User')->find(
                        $theProject['targetUser']);

                $folderId = $theProject['folder'];
                $softwareId = $theProject['software'];
                
                $duplicator->executeDuplication($softwareId, $projectId, $folderId, $sourceUser, $targetUser);
            }
        }

        return $this->redirect($this->generateUrl('green_copy_project'));
    }
    
    public function gestionLicencesAction(){
        $requestHandler = $this->getRequestHandler();
        
        $users = $requestHandler->findAllUsers();
        $softwares = $requestHandler->findAllSoftwares();
        $userLetters = $requestHandler->findUserLetters();
        
        return $this->render('ArtoGreenBundle:Default:gestionLicences.html.twig', array(
            'users' => $users,
            'softwares' => $softwares,
            'userLetters' => $userLetters    
        ));    
    }
    
   public function userGetLicenceAction(){
        $request = $this->getRequest();
        $requestHandler = $this->getRequestHandler();
        
        $user = $request->get('user');
        $softwareId = $request->get('software');
        
        $licence = $requestHandler->findLicenseForUser($user, $softwareId);
         
        $licenceEndDate = $licence->getEndAt();
        $licenceEndDateFormatted = $licenceEndDate->format('d/m/Y');
        
        $json = json_encode($licenceEndDateFormatted);
        $response = new Response($json);
        return $response;  
    }
    
    public function gestionBaseArtoAcvAction(){
         $em = $this->getDoctrine()->getEntityManager();
         $requestHandler = $this->getRequestHandler();
        
         $users = $requestHandler->findAllUsers();
         
         $types = $em->getRepository('ArtoAcvBundle:Type')->findAll();
         
         return $this->render('ArtoGreenBundle:Default:gestionBaseArtoAcv.html.twig', array(
            'users' => $users,
            'types' => $types
        ));      
    }
    
    public function findIfActiveAction(){
        $requestHandler = $this->getRequestHandler();
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        
        $user = $request->get('user');
        $active = "Actif";
        
        $licences = $requestHandler->findLicensesForUser($user);
        
        $now = new \DateTime('NOW');
         
        foreach($licences as $licence) {
            $licenceEndDate = $licence->getEndAt();
            if ($licenceEndDate < $now) {
                $active = "Inactif";
                break;
            }
        }

        $json = json_encode($active);
        $response = new Response($json);
        return $response;       
    }
    
    public function updateLicencesAction(){
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();

        $projects = $request->get('projects');
        
        foreach($projects as $theProject){
            if(isset($theProject['user']) && $theProject['user'] != '') {
                if(isset($theProject['licenceEndDate']) && $theProject['licenceEndDate'] != '') {
                    //$user = $em->getRepository('ArtoGreenBundle:User')->find($theProject['user']);
                    $licenceEndDate = $theProject['licenceEndDate'];
                    $day = substr($licenceEndDate,0,2);
                    $month = substr($licenceEndDate,3,2);
                    $year = substr($licenceEndDate,6,4);
                    $realLicenceEndDate = new \DateTime($year.'/'.$month.'/'.$day);
                    //$realLicenceEndDate = \DateTime::createFromFormat('dd/mm/YYYY', $licenceEndDate);
                    if(isset($theProject['software']) && $theProject['software'] != ''){
                        //$softwareId = $theProject['software'];
                        $licence = $em->getRepository('ArtoGreenBundle:License')->findOneBy(
                                array('user' => $theProject['user'], 'product' => $theProject['software']));
                        
                        $licence->setEndAt($realLicenceEndDate);
                        $em->persist($licence);
                        $em->flush();
                    }else{
                        $licences = $em->getRepository('ArtoGreenBundle:License')->findBy(
                                array('user' => $theProject['user']));
                        
                        foreach($licences as $licence){
                           $licence->setEndAt($realLicenceEndDate);
                           $em->persist($licence);
                           $em->flush(); 
                        }
                    }
                }
            }
        }
        
        return $this->redirect($this->generateUrl('green_gestion_licences'));
    }
    
    public function getFamiliesAction(){
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        
        //$type = $request->get('type');
        
        $familiesContent = '<option selected="selected" value="">- Choisir -</option>';
        
        $bases = $em->getRepository('ArtoAcvBundle:Base')->findBy(
                array('type' => $request->get('type'), 'isVisible' => 'oui'),
                array('name' => 'ASC'));

        $families = array();
        foreach($bases as $base) {
            $familyId = $base->getFamily()->getId();
            $familyName = $base->getFamily()->getName();
            
            if (!array_key_exists($familyId, $families)) {
                $families[$familyId] = $familyName;
                $familiesContent = $familiesContent.'<option value="'.$familyId.'">'.$familyName.'</option>';
            }
        }
        
        $json = json_encode($familiesContent);
        $response = new Response($json);
        return $response;       
    }
    
    public function getModulesAction(){
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        
        $type = $request->get('type');
        $family = $request->get('family');
        
        $modulesContent = '<option selected="selected" value="">- Choisir -</option>';
        
        $modules = $em->getRepository('ArtoAcvBundle:Base')->findBy(
                                array('type' => $type, 'family' => $family));
        
        foreach($modules as $module){
            $modulesContent = $modulesContent.'<option value="'.$module->getId().'">'.$module->getName().'</option>';
        }
        
        $json = json_encode($modulesContent);
        $response = new Response($json);
        return $response;       
    }
    
    public function getBaseAction(){
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        
        $moduleId = $request->get('module');
        
         $query = $em
                 ->createQuery("SELECT e FROM ArtoAcvBundle:Base e WHERE e.id = :id")
                 ->setParameter('id', $moduleId);
 
        $module = $query->getArrayResult();
        
        
        $json = json_encode($module);
        $response = new Response($json);
        return $response;       
    }
    
    public function falseIndexAction(){
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        
        $userId = $request->get('user');
        $softwareId = $request->get('software');
        
        $softwareName = $this->findSoftwareName($softwareId);
        $repositoryProjectName = $softwareName.'Bundle:Project';
        $repositoryFolderName = $softwareName.'Bundle:Folder';
        $pathName = 'ArtoGreenBundle:Default:falseIndex'.$softwareName.'.html.twig';
        
        $user = $em->getRepository('ArtoGreenBundle:User')->find($userId);
        
        $folderLetters = array();

        $projects = $em->getRepository($repositoryProjectName)->findAllActive($user);
        if($softwareName == 'ArtoDesign' || $softwareName == 'ArtoArchi'){
            $sql = "SELECT c FROM ".$softwareName."Bundle:Project c WHERE c.greenUser=:greenUser AND c.folder IS NULL AND c.default IS NOT NULL ORDER BY c.name";
            $query = $em->createQuery($sql)->setParameters(
                array("greenUser" => $userId));
        
            $projectsWF = $query->getResult();
        }elseif ($softwareName == 'ArtoAcv') {
            $projectsWF = $em->getRepository($repositoryProjectName)->findBy(
                array('greenUser' => $userId, 'folder' => null, 'parent' => null), array('name' => 'ASC'));
        }else{
            $projectsWF = $em->getRepository($repositoryProjectName)->findBy(
                array('greenUser' => $userId, 'folder' => null), array('name' => 'ASC'));
        }
        
        $folders = $em->getRepository($repositoryFolderName)->findBy(
                array('user' => $userId), array('name' => 'ASC'));
        
        foreach ($folders as $folder) {
            $folderName = $folder->getName();
            $firstLetter = strtoupper(substr($folderName, 0, 1));

            if (!in_array($firstLetter, $folderLetters)) {
                array_push($folderLetters, $firstLetter);
            }
        }

        //$templateImportExcelUrl = __DIR__.'/../../../../web/template_import_ArtoAcv.xlsm';
        
        
        return $this->render($pathName, array(
            'projects' => $projects,
            'user' => $userId,
            'folders' => $folders,
            'projectsWF'=> $projectsWF,
            'myLetter' => '',
            'folderLetters' => $folderLetters));
    }
    
    public function razUserAction(){
        $requestHandler = $this->getRequestHandler();
        
        $users = $requestHandler->findAllUsers();
        $softwares = $requestHandler->findAllSoftwares();
        $userLetters = $requestHandler->findUserLetters();
           
        return $this->render('ArtoGreenBundle:Default:emptyUserSoftware.html.twig', array(
            'users' => $users,
            'softwares' => $softwares,
            'userLetters' => $userLetters
        ));
    }   
    
    public function razSoftwareAction(){
        $request = $this->getRequest();
        $requestHandler = $this->getRequestHandler();
        
        $projects = $request->get('projects');
        $users = $requestHandler->findAllUsers();
        $softwares = $requestHandler->findAllSoftwares();
        $userLetters = $requestHandler->findUserLetters();
        
        foreach($projects as $theProject) {
            if(isset($theProject['firstLetterUser']) && $theProject['firstLetterUser'] != '') {
               $user = $theProject['user'];
               $softwareId = $theProject['software'];
        
               $softwareName = $this->findSoftwareName($softwareId);
               $folders = $requestHandler->findFoldersByUser($user, $softwareName);
        
               foreach($folders as $folder){
                    $folderId = $folder->getId();
                    $requestHandler->deleteFolder($folderId, $softwareName);
               }
               
               $requestHandler->deleteProjects($user, $softwareName);
            }
        }
        
        return $this->render('ArtoGreenBundle:Default:emptyUserSoftware.html.twig', array(
            'users' => $users,
            'softwares' => $softwares,
            'userLetters' => $userLetters
        ));
    }
    
    public function findSoftwareName($softwareId){
        $softwareName = '';
        //ArtoUrba
        if($softwareId == 1){
            $softwareName = 'ArtoArchi';
        }//ArtoDesign
        elseif($softwareId == 4) {
            $softwareName = 'ArtoDesign';
        }//ArtoAcv
        elseif($softwareId == 5) {
            $softwareName = 'ArtoAcv';
        }//ArtoFlux
        elseif($softwareId == 7){
            $softwareName = 'ArtoFlux';
        }//ArtoDistrib
        elseif($softwareId == 2){
            $softwareName = 'ArtoDistrib';
        }//ArtoProduct
        elseif($softwareId == 6){
            $softwareName = 'ArtoProduct';
        }
        
        return $softwareName;
    }
      
    public function checkLogArtoAcvAction(){
        return $this->render('ArtoGreenBundle:Default:logsArtoAcv.html.twig'); 
    }
    
    public function checkLogArtoGreenAction(){
        return $this->render('ArtoGreenBundle:Default:logsArtoGreen.html.twig'); 
    }
    
    public function sameDay(\DateTime $date1, \DateTime $date2){
        $isSame = false;
        
        $date1Format = $date1->format("d/m/Y");
        $date2Format = $date2->format("d/m/Y");
        
        if($date1Format == $date2Format){
            $isSame = true;
        }
        
        return $isSame;
    }
    
    public function downloadLogArtoAcvAction(){
        $em = $this->getDoctrine()->getEntityManager();
        
        $logs = $em->getRepository('ArtoDesignBundle:Log')->findAll();
        
        foreach($logs as $log){
            //$logDate = $log->getDate();
            $logUserName = $log->getUserName();
            $logUserPwd = $log->getUserPwd();
            
            $myUser = $em->getRepository('ArtoGreenBundle:User')->findOneBy(
                    array('username' => $logUserName, 'password' => $logUserPwd));
            
            $log->setUserEmail($myUser->getEmail());
            $em->persist($log);
        }
        
        $em->flush();
        
        return $this->render('ArtoGreenBundle:Default:logsArtoAcv.html.twig'); 
    }
    
    public function downloadLogArtoGreenAction(){
        $em = $this->getDoctrine()->getEntityManager();
               
        $logs = $em->getRepository('ArtoGreenBundle:GreenLog')->findAll();
        $today = date("d-m-Y");
          
        $fileName = "Log ArtoGreen ".$today.".xlsx";
        
        $excel = new \PHPExcel();
        
        $sheet = $excel->setActiveSheetIndex(0);
        $sheet->setTitle('Logs ArtoGreen', false);
        
        $sheet->setCellValue('B3', "Logiciel")
              ->setCellValue('C3', "Date d'accès")
              ->setCellValue('D3', "Login")
              ->setCellValue('E3', "Password");
        
        $sheet->getStyle('B3')->getAlignment()->setHorizontal(
            \PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C3')->getAlignment()->setHorizontal(
            \PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D3')->getAlignment()->setHorizontal(
            \PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E3')->getAlignment()->setHorizontal(
            \PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        $sheet->getStyle('B3:E3')->getFont()->setBold(true);
        $sheet->getStyle('B3:E3')->getFill()->getStartColor()->setRGB('008000');
        
        $nbCell = 4;
        
        $phpColor = new \PHPExcel_Style_Color();
        $phpColor->setRGB('FF0000');  

        
        $logDateRef = new \DateTime('2016-11-05');
        $date2018 = "2017-09-01";
        
        foreach($logs as $log){
            $logDate = $log->getDate();
            $logDateString = $logDate->format('Y-m-d');
            if(strtotime($logDateString) >= strtotime($date2018) ){
                $logSoftware = $log->getSoftware();
                $logDate = $log->getDate();

                $logDateHour = $logDate->format("H:i");
                $logDateRefS = $logDateRef->format("d/m/Y");


                $logUserName = $log->getUserName();
                $logUserPwd = $log->getUserPwd();

                if($this->sameDay($logDateRef, $logDate)){
                    if($logDateRefS == '05/11/2016'){
                        $sheet->setCellValue('B'.$nbCell, $logDateRefS);
                        $sheet->getStyle('B'.$nbCell)->getFont()->setBold(true);
                        $sheet->getStyle('B'.$nbCell)->getFont()->setColor($phpColor);

                        $nbCell++;
                    }

                    $sheet->setCellValue('B'.$nbCell, $logSoftware)
                          ->setCellValue('C'.$nbCell, $logDateHour)
                          ->setCellValue('D'.$nbCell, $logUserName)
                          ->setCellValue('E'.$nbCell, $logUserPwd);
                }else{
                    $logDateRef = $logDate;
                    $logDateRefS = $logDateRef->format("d/m/Y");
                    $sheet->setCellValue('B'.$nbCell, $logDateRefS);
                    $sheet->getStyle('B'.$nbCell)->getFont()->setBold(true);
                    $sheet->getStyle('B'.$nbCell)->getFont()->setColor($phpColor);

                    $nbCell++;

                    $sheet->setCellValue('B'.$nbCell, $logSoftware)
                          ->setCellValue('C'.$nbCell, $logDateHour)
                          ->setCellValue('D'.$nbCell, $logUserName)
                          ->setCellValue('E'.$nbCell, $logUserPwd);
                }

                $nbCell++;
                }
        }
        
        $BStyle = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        
        $lastCell = $nbCell -1;
        
        $sheet->getStyle('B3:E'.$lastCell)->applyFromArray($BStyle);
        
        foreach (range('B', 'G') as $columnID) {
            $sheet->getColumnDimension($columnID)
                  ->setAutoSize(true);
        }

        // Writer
        $writer = \PHPExcel_IOFactory::createWriter( $excel, 'Excel2007' );

        header( 'Content-type: application/vnd.ms-excel; charset=utf-8' );
        header( 'Content-Disposition: attachment; filename='.$fileName );

        return $writer->save( 'php://output' );
    }
    
    public function deleteUserAction(){
        $requestHandler = $this->getRequestHandler();
        
        $users = $requestHandler->findAllUsers();
        $userLetters = $requestHandler->findUserLetters();
           
        return $this->render('ArtoGreenBundle:Default:deleteUser.html.twig', array(
            'users' => $users,
            'userLetters' => $userLetters
        ));          
    }
    
    public function eraseUserAction(){
         $em = $this->getDoctrine()->getEntityManager();
         $request = $this->getRequest();
         $requestHandler = $this->getRequestHandler();
         
         $users = $requestHandler->findAllUsers();
         $userLetters = $requestHandler->findUserLetters();
         
         $projects = $request->get('projects');
        
         foreach($projects as $theProject) {
            if(isset($theProject['user']) && $theProject['user'] != '') {
               $user = $theProject['user'];
               $requestHandler->deleteProjects($user, 'ArtoAcv');
               $requestHandler->deleteProjects($user, 'ArtoProduct');
               $requestHandler->deleteProjects($user, 'ArtoArchi');
               $requestHandler->deleteProjects($user, 'ArtoDistrib');
               $requestHandler->deleteProjects($user, 'ArtoFlux');
               $requestHandler->deleteProjects($user, 'ArtoDesign');
               
               $requestHandler->deleteLicenses($user);
               
               $greenUser = $em->getRepository('ArtoGreenBundle:User')->find($user);
               
               $em->remove($greenUser);
               $em->flush();
               
            }
         }
         
         return $this->render('ArtoGreenBundle:Default:deleteUser.html.twig', array(
            'users' => $users,
            'userLetters' => $userLetters
        ));       
    }
}
