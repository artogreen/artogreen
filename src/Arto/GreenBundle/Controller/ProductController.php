<?php

namespace Arto\GreenBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{

    public function indexAction($slug)
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();

        $user = null;
        if ($request->getSession()->get('user') != null) {
            $user = $em->getRepository('ArtoGreenBundle:User')->findOneByUniq($request->getSession()->get('user'));
        }

        $product = $em->getRepository('ArtoGreenBundle:Product')->findOneBySlug($slug);

        $training = $em->getRepository('ArtoGreenBundle:Training')->findOneBy(array(
            'product' => $product->getId(),
            'company' => 1
        ));

        $pack = $em->getRepository('ArtoGreenBundle:Product')->findOneBy(array(
            'category' => $product->getCategory()->getId(),
            'pack' => true
        ));

        $training_pack = $em->getRepository('ArtoGreenBundle:Training')->findOneBy(array(
            'product' => $pack->getId(),
            'company' => 1
        ));

        return $this->render('ArtoGreenBundle:Product:index.html.twig', array(
            'product' => $product,
            'training' => $training,
            'training_pack' => $training_pack,
            'pack'    => $pack,
            'user'    => $user
        ));
    }

    public function descriptionAction()
    {
        return $this->render('ArtoGreenBundle:Product:description.html.twig');
    }

    public function priceSelectAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();
        $training1 = 0;
        $training2 = 0;

        $results = array(
            'total' => 0,
            'training1' => 0,
            'training2' => 0
        );

        $price = $em->getRepository('ArtoGreenBundle:Price')->findOneBy(array(
            'product' => $request->get('product'),
            'license' => $request->get('license'),
            'station' => $request->get('station'),
        ));

        if ($request->get('training1') == 1) {
            $training = $em->getRepository('ArtoGreenBundle:Training')->findOneBy(array(
                'product' => $request->get('product'),
                'company' => 1
            ));
            $results['training1'] = $training->getPrice1();
        }

        if ($request->get('training2') == 1) {
            $training = $em->getRepository('ArtoGreenBundle:Training')->findOneBy(array(
                'product' => $request->get('product'),
                'company' => 1
            ));
            $results['training2'] = $training->getPrice2();
        }

        if ($price != null) {
            $results['total'] = $price->getPrice() + $results['training1'] + $results['training2'];
        }

        return new Response(json_encode($results));
    }

}
