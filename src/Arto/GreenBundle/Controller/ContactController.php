<?php

namespace Arto\GreenBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Arto\GreenBundle\Entity\Contact;
use Arto\GreenBundle\Form\ContactType;

class ContactController extends Controller {

    /**
     * Action to load contact form and if request is post to save contact in db
     *
     * @param string $subject
     * @return
     */
    public function indexAction() {
        // Voir pour gerer le formulaire de contact http://symfony.com/doc/current/book/forms.html
        $contact = new Contact();
        $form = $this->createForm(new ContactType(), $contact);

        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $form->bindRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getEntityManager();
                $contact->setProcessed(false);
                $em->persist($contact);
                $em->flush();

                $message = \Swift_Message::newInstance(null, null, 'text/html')
                    ->setSubject('[ArtoGREEN] Demande de contact')
                    ->setFrom($contact->getEmail())
                        ->setTo(array('artogreensoft@gmail.com'))
                    ->setBody($this->renderView('ArtoGreenBundle:Mail:contact.html.twig', array('contact' => $contact)));
                $this->get('mailer')->send($message);

                return $this->redirect($this->generateUrl('green_contact_confirm'));
            }
        }

        return $this->render('ArtoGreenBundle:Contact:index.html.twig',
                array('form' => $form->createView(),));
    }

    public function confirmAction() {
        return $this->render('ArtoGreenBundle:Contact:confirm.html.twig');
    }

}
