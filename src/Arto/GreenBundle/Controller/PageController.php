<?php

namespace Arto\GreenBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class PageController extends Controller
{

    public function indexAction($slug)
    {
        return $this->render('ArtoGreenBundle:Page:index.html.twig');
    }

    public function pageAction($slug)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $page = $em->getRepository('ArtoGreenBundle:Page')->findOneBySlug($slug);

        return $this->render('ArtoGreenBundle:Page:page.html.twig', array(
            'page' => $page
        ));
    }
}
