<?php

namespace Arto\GreenBundle\Bank;


class Signature
{

    // Get the signature
    public static function get($field,$key) {

        ksort($field); // tri des paramétres par ordre alphabétique
        $contenu_signature = "";
        foreach ($field as $nom => $valeur)
        {
            if(substr($nom,0,5) == 'vads_') {
            $contenu_signature .= $valeur."+";
            }
        }
        $contenu_signature .= $key; // On ajoute le certificat à la fin de la chaîne.
        $signature = sha1($contenu_signature);
        return($signature);

    }

    // Control signatrue
    public static function check($field,$key) {
        $result='false';

        $signature=self::get($field,$key);

        if(isset($field['signature']) && ($signature == $field['signature']))
        {
            $result='true';

        }
        return ($result);
    }
}