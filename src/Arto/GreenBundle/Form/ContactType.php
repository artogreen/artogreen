<?php

namespace Arto\GreenBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('subject', null, array('label' => 'Sujet'))
            ->add('firstname', null, array('label' => 'Prénom', 'required' => true))
            ->add('lastname', null, array('label' => 'Nom', 'required' => true))
            ->add('email', null, array('label' => 'E-mail', 'required' => true))
            ->add('tel', null, array('label' => 'Téléphone', 'required' => false))
            ->add('company', null, array('label' => 'Société', 'required' => false))
            ->add('website', null, array('label' => 'Site web', 'required' => false))
            ->add('message', null, array('label' => 'Message', 'required' => true))
        ;
    }

    public function getName()
    {
        return 'artogreen_contactbundle_contacttype';
    }

     public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'Arto\GreenBundle\Entity\Contact',
        );
    }
}
