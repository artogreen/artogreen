<?php

namespace Arto\GreenBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class UserType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('firstname', null, array('required' => true))
            ->add('lastname', null, array('required' => true))
            ->add('majority', null, array('required' => true))
            ->add('address', null, array('required' => true))
            ->add('city', null, array('required' => true))
            ->add('postcode', null, array('required' => true))
            ->add('company', null, array('required' => true))
            ->add('activity', null, array('required' => true))
            ->add('siret', null, array('required' => true))
            ->add('email', null, array('required' => true))
            ->add('password', null, array('required' => true))    
            ->add('tel', null, array('required' => true))
            ->add('capital', null, array('required' => true))
            ->add('optin', null, array('required' => true))
            ->add('accept1', null, array('required' => true))
            ->add('accept2', null, array('required' => true))
        ;
    }

    public function getName()
    {
        return 'usertype';
    }
}
