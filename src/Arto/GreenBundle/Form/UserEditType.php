<?php

namespace Arto\GreenBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class UserEditType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('firstname', null, array('required' => true))
            ->add('lastname', null, array('required' => true))
            ->add('address', null, array('required' => true))
            ->add('city', null, array('required' => true))
            ->add('postcode', null, array('required' => true))
            ->add('company', null, array('required' => true))
            ->add('activity', null, array('required' => true))
            ->add('siret' , null, array('required' => true))    
            ->add('tel', null, array('required' => true))
        ;
    }

    public function getName()
    {
        return 'useredittype';
    }
}
