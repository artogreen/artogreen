<?php
namespace Arto\GreenBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="green_training")
 */
class Training
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="company", type="integer", nullable=true)
     */
    private $company;

    /**
     * @ORM\Column(name="price1", type="integer", nullable=true)
     */
    private $price1;

    /**
     * @ORM\Column(name="price2", type="integer", nullable=true)
     */
    private $price2;

    /**
     * @ORM\Column(name="label1", type="string", length=255, nullable=true)
     */
    private $label1;

    /**
     * @ORM\Column(name="label2", type="string", length=255, nullable=true)
     */
    private $label2;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="prices")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    private $product;


    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getLabel1();
    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set company
     *
     * @param integer $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * Get company
     *
     * @return integer
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set price1
     *
     * @param integer $price1
     */
    public function setPrice1($price1)
    {
        $this->price1 = $price1;
    }

    /**
     * Get price1
     *
     * @return integer
     */
    public function getPrice1()
    {
        return $this->price1;
    }

    /**
     * Set price2
     *
     * @param integer $price2
     */
    public function setPrice2($price2)
    {
        $this->price2 = $price2;
    }

    /**
     * Get price2
     *
     * @return integer
     */
    public function getPrice2()
    {
        return $this->price2;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set product
     *
     * @param Arto\GreenBundle\Entity\Product $product
     */
    public function setProduct(\Arto\GreenBundle\Entity\Product $product)
    {
        $this->product = $product;
    }

    /**
     * Get product
     *
     * @return Arto\GreenBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set label1
     *
     * @param string $label1
     */
    public function setLabel1($label1)
    {
        $this->label1 = $label1;
    }

    /**
     * Get label1
     *
     * @return string
     */
    public function getLabel1()
    {
        return $this->label1;
    }

    /**
     * Set label2
     *
     * @param string $label2
     */
    public function setLabel2($label2)
    {
        $this->label2 = $label2;
    }

    /**
     * Get label2
     *
     * @return string
     */
    public function getLabel2()
    {
        return $this->label2;
    }
}