<?php

namespace Arto\GreenBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Arto\GreenBundle\Entity\User
 *
 * @ORM\Table(name="green_user")
 * @ORM\Entity(repositoryClass="Arto\GreenBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks
 */
class User implements UserInterface
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $uniq
     *
     * @ORM\Column(name="uniq", type="string", length=32, nullable=false)
     */
    private $uniq;

    /**
     * @var string $firstname
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="firstname", type="string", length=100, nullable=false)
     */
    private $firstname;

    /**
     * @var string $lastname
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="lastname", type="string", length=100, nullable=false)
     */
    private $lastname;

    /**
     * @var boolean $majority
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="majority", type="boolean", nullable=false)
     */
    private $majority;

    /**
     * @var string $address
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="address", type="text", nullable=false)
     */
    private $address;

    /**
     * @var string $city
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="city", type="string", length=100, nullable=false)
     */
    private $city;

    /**
     * @var string $postcode
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="postcode", type="string", length=10, nullable=false)
     */
    private $postcode;

    /**
     * @var string $company
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="company", type="string", length=250, nullable=true)
     */
    private $company;

    /**
     * @var string $capital
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="capital", type="integer", nullable=true)
     */
    private $capital;

    /**
     * @var string $siret
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="siret", type="string", length=20, nullable=true)
     */
    private $siret;

    /**
     * @var string $tel
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="tel", type="string", length=20, nullable=true)
     */
    private $tel;

    /**
     * @var string $username
     *
     * @ORM\Column(name="username", type="string", length=100, nullable=false)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $salt;

    /**
     * @var string $password
     *
     * @ORM\Column(name="password", type="string", length=20, nullable=true)
     */
    private $password;

    /**
     * @var string $email
     *
     * @Assert\NotBlank()
     * @Assert\Email()
     * @ORM\Column(name="email", type="string", length=250, nullable=true)
     */
    private $email;

    /**
     * @var boolean $active
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var boolean $optin
     *
     * @ORM\Column(name="optin", type="boolean", nullable=true)
     */
    private $optin;

    /**
     * @var boolean $accept1
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="accept1", type="boolean", nullable=false)
     */
    private $accept1;

    /**
     * @var boolean $accept2
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="accept2", type="boolean", nullable=false)
     */
    private $accept2;
    
    /**
     * @var string acvUserId
     * 
     * @ORM\Column(name="acvUser_id", type="string", length=20, nullable=true)
     */
    private $acvUserId;
    
    /**
     * @var integer nbProducts
     * 
     * @ORM\Column(name="nbProducts", type="integer", nullable=true)
     */
    private $nbProducts;
    
    /**
     * @var boolean isOld
     * 
     * @ORM\Column(name="isOld", type="boolean", nullable=true)
     */
    private $isOld;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Activity
     *
     * @ORM\ManyToOne(targetEntity="Activity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="activity_id", referencedColumnName="id")
     * })
     */
    private $activity;

    /**
     * @ORM\OneToMany(targetEntity="License", mappedBy="user", cascade={"remove"})
     */
    private $licenses;
    
    /**
     * @ORM\OneToMany(targetEntity="Arto\AcvBundle\Entity\Folder", mappedBy="user", cascade={"remove", "persist"})
     */
    private $acvFolders;
    
    /**
     * @ORM\OneToMany(targetEntity="Arto\ArchiBundle\Entity\Folder", mappedBy="user", cascade={"remove", "persist"})
     */
    private $archiFolders;
    
    /**
     * @ORM\OneToMany(targetEntity="Arto\DesignBundle\Entity\Folder", mappedBy="user", cascade={"remove", "persist"})
     */
    private $designFolders;
    
    /**
     * @ORM\OneToMany(targetEntity="Arto\DistribBundle\Entity\Folder", mappedBy="user", cascade={"remove", "persist"})
     */
    private $distribFolders;
    
    /**
     * @ORM\OneToMany(targetEntity="Arto\ProductBundle\Entity\Folder", mappedBy="user", cascade={"remove", "persist"})
     */
    private $productFolders;
    
    /**
     * @ORM\OneToMany(targetEntity="Arto\FluxBundle\Entity\Folder", mappedBy="user", cascade={"remove", "persist"})
     */
    private $fluxFolders;
    
    public function __construct()
    {
        $this->uniq = substr(strtoupper(md5( uniqid('arto', true) )), 0, 32 );
        $this->active = false;
        $this->salt = md5(uniqid(null, true));
        $this->accept1 = false;
        $this->accept2 = false;
    }

    public function __sleep()
    {
        return array('id');
    }

    /*public function __toString()
    {
        return $this->firstname.' '.$this->lastname;
    }*/

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set firstname
     *
     * @param string $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set address
     *
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set company
     *
     * @param string $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set capital
     *
     * @param integer $capital
     */
    public function setCapital($capital)
    {
        $this->capital = $capital;
    }

    /**
     * Get capital
     *
     * @return integer
     */
    public function getCapital()
    {
        return $this->capital;
    }

    /**
     * Set siret
     *
     * @param string $siret
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;
    }

    /**
     * Get siret
     *
     * @return string
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * Set tel
     *
     * @param string $tel
     */
    public function setTel($tel)
    {
        $this->tel = $tel;
    }

    /**
     * Get tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set username
     *
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set active
     *
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set activity
     *
     * @param Arto\GreenBundle\Entity\Activity $activity
     */
    public function setActivity(\Arto\GreenBundle\Entity\Activity $activity)
    {
        $this->activity = $activity;
    }

    /**
     * Get activity
     *
     * @return Arto\GreenBundle\Entity\Activity
     */
    public function getActivity()
    {
        return $this->activity;
    }
    
    /**
     * Set acvUserId
     *
     * @param string $acvUserId
     */
    public function setAcvUserId($acvUserId = null)
    {
        $this->acvUserId = $acvUserId;
    }

    /**
     * Get acvUserId
     *
     * @return string
     */
    public function getAcvUserId()
    {
        return $this->acvUserId;
    }
    
    
    /**
     * Set nbProducts
     *
     * @param integer $nbProducts
     */
    public function setNbProducts($nbProducts = null)
    {
        $this->nbProducts = $nbProducts;
    }

    /**
     * Get nbProducts
     *
     * @return integer
     */
    public function getNbProducts()
    {
        return $this->nbProducts;
    }
    

    /**
     * Add licenses
     *
     * @param Arto\GreenBundle\Entity\License $licenses
     */
    public function addLicense(\Arto\GreenBundle\Entity\License $licenses)
    {
        $this->licenses[] = $licenses;
    }

    /**
     * Get licenses
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getLicenses()
    {
        return $this->licenses;
    }

    /**
     * Set uniq
     *
     * @param string $uniq
     */
    public function setUniq($uniq)
    {
        $this->uniq = $uniq;
    }

    /**
     * Get uniq
     *
     * @return string
     */
    public function getUniq()
    {
        return $this->uniq;
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        if ($this->getUsername() == 'artogreen') {
            return array('ROLE_ADMIN');
        } else {
            return array('ROLE_USER');
        }
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
    }

    /**
     * @inheritDoc
     */
    public function equals(UserInterface $user)
    {
        return $this->username === $user->getUsername();
    }

    /**
     * Set salt
     *
     * @param string $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->setSalt('');
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function postUpdate()
    {
        $this->setSalt('');
    }

    /**
     * Set optin
     *
     * @param boolean $optin
     */
    public function setOptin($optin)
    {
        $this->optin = $optin;
    }

    /**
     * Get optin
     *
     * @return boolean
     */
    public function getOptin()
    {
        return $this->optin;
    }

    /**
     * Set accept1
     *
     * @param boolean $accept1
     */
    public function setAccept1($accept1)
    {
        $this->accept1 = $accept1;
    }

    /**
     * Get accept1
     *
     * @return boolean
     */
    public function getAccept1()
    {
        return $this->accept1;
    }

    /**
     * Set accept2
     *
     * @param boolean $accept2
     */
    public function setAccept2($accept2)
    {
        $this->accept2 = $accept2;
    }

    /**
     * Get accept2
     *
     * @return boolean
     */
    public function getAccept2()
    {
        return $this->accept2;
    }

    /**
     * Set majority
     *
     * @param boolean $majority
     */
    public function setMajority($majority)
    {
        $this->majority = $majority;
    }

    /**
     * Get majority
     *
     * @return boolean
     */
    public function getMajority()
    {
        return $this->majority;
    }

    /**
     * Add acvFolders
     *
     * @param Arto\AcvBundle\Entity\Folder $acvFolders
     */
    public function addFolder(\Arto\AcvBundle\Entity\Folder $acvFolders)
    {
        $this->acvFolders[] = $acvFolders;
    }

    /**
     * Get acvFolders
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getAcvFolders()
    {
        return $this->acvFolders;
    }

    /**
     * Get archiFolders
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getArchiFolders()
    {
        return $this->archiFolders;
    }

    /**
     * Get designFolders
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getDesignFolders()
    {
        return $this->designFolders;
    }

    /**
     * Get distribFolders
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getDistribFolders()
    {
        return $this->distribFolders;
    }

    /**
     * Get productFolders
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getProductFolders()
    {
        return $this->productFolders;
    }

    /**
     * Get fluxFolders
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getFluxFolders()
    {
        return $this->fluxFolders;
    }
    
    /**
     * Get isOld
     * 
     * @return boolean
     */
    public function getIsOld()
    {
        return $this->isOld;
    }
    
    /**
     * Set isOld
     * 
     * @param boolean $isOld
     */
    public function setIsOld($isOld)
    {
        $this->isOld = $isOld;
    }
}