<?php
namespace Arto\GreenBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="green_product")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(name="intro", type="text", nullable=true)
     */
    private $intro;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(name="info", type="string", length=100, nullable=false)
     */
    private $info;

    /**
     * @var boolean $pack
     *
     * @ORM\Column(name="pack", type="boolean", nullable=false)
     */
    private $pack;

    /**
     * @ORM\Column(name="path", type="string", length=20, nullable=false)
     */
    private $path;

    /**
     * @var string $slug
     *
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(name="slug", type="string", length=100, unique=true)
     */
    private $slug;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="products")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @ORM\ManyToMany(targetEntity="Product", mappedBy="similars")
     */
    private $similarsWithMe;

    /**
     * @ORM\ManyToMany(targetEntity="Product", inversedBy="similarsWithMe")
     * @ORM\JoinTable(name="green_similars",
     *      joinColumns={@ORM\JoinColumn(name="product1_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="product2_id", referencedColumnName="id")}
     *      )
     */
    private $similars;

    /**
     * @ORM\OneToMany(targetEntity="Price", mappedBy="product", cascade={"remove"})
     */
    private $prices;


    public function __construct()
    {
        $this->pack = false;
    }

    public function __toString()
    {
        if ($this->getCategory() != null) {
            return $this->getCategory()->getName().' - '.$this->getName();
        } else {
            return $this->getName();
        }

    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set intro
     *
     * @param text $intro
     */
    public function setIntro($intro)
    {
        $this->intro = $intro;
    }

    /**
     * Get intro
     *
     * @return text
     */
    public function getIntro()
    {
        return $this->intro;
    }

    /**
     * Set description
     *
     * @param text $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return text
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set slug
     *
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set category
     *
     * @param Arto\GreenBundle\Entity\Category $category
     */
    public function setCategory(\Arto\GreenBundle\Entity\Category $category)
    {
        $this->category = $category;
    }

    /**
     * Get category
     *
     * @return Arto\GreenBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add similars
     *
     * @param Arto\GreenBundle\Entity\Product $similars
     */
    public function addProduct(\Arto\GreenBundle\Entity\Product $similars)
    {
        $this->similars[] = $similars;
    }

    /**
     * Get similarsWithMe
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getSimilarsWithMe()
    {
        return $this->similarsWithMe;
    }

    /**
     * Get similars
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getSimilars()
    {
        return $this->similars;
    }

    /**
     * Set info
     *
     * @param string $info
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    /**
     * Get info
     *
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Add prices
     *
     * @param Arto\GreenBundle\Entity\Price $prices
     */
    public function addPrice(\Arto\GreenBundle\Entity\Price $prices)
    {
        $this->prices[] = $prices;
    }

    /**
     * Get prices
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getPrices()
    {
        return $this->prices;
    }

    /**
     * Set pack
     *
     * @param boolean $pack
     */
    public function setPack($pack)
    {
        $this->pack = $pack;
    }

    /**
     * Get pack
     *
     * @return boolean
     */
    public function getPack()
    {
        return $this->pack;
    }

    /**
     * Set path
     *
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
}