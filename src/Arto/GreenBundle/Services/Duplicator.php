<?php

namespace Arto\GreenBundle\Services;

use Arto\AcvBundle\Entity\Product;
use Arto\AcvBundle\Entity\Project;
use Arto\AcvBundle\Entity\Folder;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Duplicator {
    
    protected $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function findSoftwareBundle($softwareId){
        $softwareBundle = '';
        //ArtoUrba
        if($softwareId == 1){
            $softwareBundle = 'ArtoArchiBundle';
        }//ArtoDesign
        elseif($softwareId == 4) {
            $softwareBundle = 'ArtoDesignBundle';
        }//ArtoAcv
        elseif($softwareId == 5) {
            $softwareBundle = 'ArtoAcvBundle';
        }//ArtoFlux
        elseif($softwareId == 7){
            $softwareBundle = 'ArtoFluxBundle';
        }//ArtoDistrib
        elseif($softwareId == 2){
            $softwareBundle = 'ArtoDistribBundle';
        }//ArtoProduct
        elseif($softwareId == 6){
            $softwareBundle = 'ArtoProductBundle';
        }
        
        return $softwareBundle;
    }
    
    public function duplicateFolder($softwareBundle, $folder, $user){
        //ArtoUrba
        if($softwareBundle == 'ArtoArchiBundle'){
            $newFolder = new \Arto\ArchiBundle\Entity\Folder();
        }//ArtoDesign
        elseif($softwareBundle == 'ArtoDesignBundle'){
            $newFolder = new \Arto\DesignBundle\Entity\Folder();
        }//ArtoAcv
        elseif($softwareBundle == 'ArtoAcvBundle'){
            $newFolder = new \Arto\AcvBundle\Entity\Folder();
        }//ArtoFlux
        elseif($softwareBundle == 'ArtoFluxBundle'){
            $newFolder = new \Arto\FluxBundle\Entity\Folder();
        }//ArtoDistrib
        elseif($softwareBundle == 'ArtoDistribBundle'){
            $newFolder = new \Arto\DistribBundle\Entity\Folder();
        }//ArtoProduct
        elseif($softwareBundle == 'ArtoProductBundle'){
            $newFolder = new \Arto\ProductBundle\Entity\Folder();
        }
        
        $em = $this->em;

        $userId = $user->getId();
        $folderName = $folder->getName();
        $repositoryFolderName = $softwareBundle.':Folder';
        
        //On cherche si le dossier a déja été copié
        $folder_new = $em->getRepository($repositoryFolderName)->findOneBy(
                array('name' => $folderName, 'user' => $userId));

        //Si il n'a pas été copié, on le copie
        if (!$folder_new) {
            $newFolder->setCode('');
            $newFolder->setIsDeployed('oui');
            $newFolder->setName($folderName);
            $newFolder->setUser($user);
            $em->persist($newFolder);
            $em->flush();
        } else {
            //Si il a déja été copié, on ne fait rien     
        } 
    }
    
    public function duplicateProject($softwareBundle, $project, $user){
        //ArtoUrba
        if($softwareBundle == 'ArtoArchiBundle'){
            $projectDefault = $project->getDefault();
            $this->duplicateArchiProject($project, $user, $projectDefault);
        }//ArtoDesign
        elseif($softwareBundle == 'ArtoDesignBundle'){
            $projectDefault = $project->getDefault();
            $this->duplicateDesignProject($project, $user, $projectDefault);
        }//ArtoAcv
        elseif($softwareBundle == 'ArtoAcvBundle'){
            $this->duplicateAcvProject($project, $user);
        }//ArtoFlux
        elseif($softwareBundle == 'ArtoFluxBundle'){
            $this->duplicateFluxProject($project, $user);
        }//ArtoDistrib
        elseif($softwareBundle == 'ArtoDistribBundle'){
            $this->duplicateDistribProject($project, $user);
        }//ArtoProduct
        elseif($softwareBundle == 'ArtoProductBundle'){
            $this->duplicateProductProject($project, $user);
        }
    }
    
    public function executeDuplication($softwareId, $projectId, $folderId, $sourceUser, $targetUser){
        $softwareBundle = $this->findSoftwareBundle($softwareId);
        
        //Dossier : OK + Projet : -Choisir-
        if ($projectId == '' && $folderId != 'choose' && $folderId != 'noFolder') {
            $this->duplicateCase1($softwareBundle, $projectId, $folderId, $sourceUser, $targetUser);
        }//Dossier : Sans dossier
        elseif ($folderId == 'noFolder') {
            $this->duplicateCase2($softwareBundle, $projectId, $folderId, $sourceUser, $targetUser);
        }//Dossier : - Choisir- + Projet : -Choisir-
        elseif ($folderId == 'choose') {
            $this->duplicateCase3($softwareBundle, $projectId, $folderId, $sourceUser, $targetUser);
        }//Autres cas
        else {
            $this->duplicateCase4($softwareBundle, $projectId, $folderId, $sourceUser, $targetUser);
        }
    }
        
    //Dossier : OK + Projet : -Choisir-
    public function duplicateCase1($softwareBundle, $projectId, $folderId, $sourceUser, $targetUser){
        $em = $this->em;
        
        $repositoryFolderName = $softwareBundle.':Folder';
        $repositoryProjectName = $softwareBundle.':Project';
        
        $folder = $em->getRepository($repositoryFolderName)->findOneBy(
                array('id' => $folderId));

        $this->duplicateFolder($softwareBundle, $folder, $targetUser);
        
        $projects = $em->getRepository($repositoryProjectName)->findBy(
                array('greenUser' => $sourceUser, 'folder' => $folderId), array('name' => 'ASC'));
       
        foreach ($projects as $project) {
            $this->duplicateProject($softwareBundle, $project, $targetUser);
        }
    }
    
    //Dossier : Sans dossier
    public function duplicateCase2($softwareBundle, $projectId, $folderId, $sourceUser, $targetUser){
        $em = $this->em;
        
        $repositoryProjectName = $softwareBundle.':Project';
        
        //Projet : -Choisir-
        if ($projectId == '') {
            if($softwareBundle == 'ArtoArchiBundle' || $softwareBundle == 'ArtoDesignBundle'){
                $sql1 = 'SELECT c FROM ';
                $sql2 = ' c WHERE c.greenUser=:greenUser AND c.folder IS NULL AND c.default IS NOT NULL ORDER BY c.name';
                $sql = $sql1.$repositoryProjectName.$sql2;
                
                $query = $em->createQuery($sql)->setParameters(
                    array("greenUser" => $sourceUser));
                $projects = $query->getResult();
            }else{
                $projects = $em->getRepository($repositoryProjectName)->findBy(
                    array('greenUser' => $sourceUser, 'folder' => null), array('name' => 'ASC'));    
            }

            foreach ($projects as $project) {
                $this->duplicateProject($softwareBundle, $project, $targetUser);
            }
        }
        //Projet != -Choisir-
        else {
            $project = $em->getRepository($repositoryProjectName)->findOneBy(
                    array('id' => $projectId));

            $this->duplicateProject($softwareBundle, $project, $targetUser);
        }
    }
    
    //Dossier : - Choisir- + Projet : -Choisir
    public function duplicateCase3($softwareBundle, $projectId, $folderId, $sourceUser, $targetUser){
        $em = $this->em;
        
        $repositoryFolderName = $softwareBundle.':Folder';
        $repositoryProjectName = $softwareBundle.':Project';
        
        $folders = $em->getRepository($repositoryFolderName)->findBy(
                array('user' => $sourceUser), array('name' => 'ASC'));

        foreach ($folders as $folder) {
            $this->duplicateFolder($softwareBundle, $folder, $targetUser);
        }
        
        if($softwareBundle == 'ArtoArchiBundle' || $softwareBundle == 'ArtoDesignBundle'){
            $sql1 = 'SELECT c FROM ';
            $sql2 = ' c WHERE c.greenUser=:greenUser AND c.default IS NOT NULL ORDER BY c.name';
            $sql = $sql1.$repositoryProjectName.$sql2;
            
            $query = $em->createQuery($sql)->setParameters(
                array("greenUser" => $sourceUser));
            $projects = $query->getResult();
        }else{
            $sql1 = 'SELECT c FROM ';
            $sql2 = ' c WHERE c.greenUser=:greenUser ORDER BY c.name';
            $sql = $sql1.$repositoryProjectName.$sql2;
            
            $query = $em->createQuery($sql)->setParameters(
                array("greenUser" => $sourceUser));
            $projects = $query->getResult();
            //$projects = $em->getRepository($repositoryProjectName)->findBy(
                //array('greenUser' => $sourceUser), array('name' => 'ASC'));
        }

        foreach ($projects as $project) {
            $this->duplicateProject($softwareBundle, $project, $targetUser);
        }
    }
    
    //Autres cas
    public function duplicateCase4($softwareBundle, $projectId, $folderId, $sourceUser, $targetUser){
        $em = $this->em;
        
        $repositoryFolderName = $softwareBundle.':Folder';
        $repositoryProjectName = $softwareBundle.':Project';
         
        $project = $em->getRepository($repositoryProjectName)->findOneBy(
                array('id' => $projectId));

        //Si le projet à dupliquer est dans un dossier, on cherche d'abord à dupliquer le dossier
        $folder = $em->getRepository($repositoryFolderName)->findOneBy(
                array('id' => $folderId));

        if ($folder != null) {
            $this->duplicateFolder($softwareBundle, $folder, $targetUser);
        }

        //Ensuite, on duplique le projet
        $this->duplicateProject($softwareBundle, $project, $targetUser);
    }
     
    /**
     * Duplique un projet ArtoProduct chez un autre utilisateur
     * @param type $project le projet à dupliquer
     * @param type $user l'utilisateur chez qui copier
     */
    public function duplicateProductProject($project, $user){
        $em = $this->em;

        $userId = $user->getId();
        $project_new = clone $project;
        $project_new->setName($project->getName());
        
        if($project->getName() == 'Suivi ICPE'){
            $existingICPE = $em->getRepository('ArtoProductBundle:Project')->findOneBy(
                    array('name' => 'Suivi ICPE', 'greenUser' => $userId));
            
            $existingICPEActivites =  $em->getRepository('ArtoProductBundle:Activite')->findBy(
               array('project' => $existingICPE->getId()));
            
            foreach($existingICPEActivites as $existingICPEActivite){
                $em->remove($existingICPEActivite);
            }
            
            $em->remove($existingICPE);
            $em->flush();
            
            $activites = $em->getRepository('ArtoProductBundle:Activite')->findBy(
               array('project' => $project->getId()));
            foreach($activites as $activite){
                $activite_new = clone $activite;
                $activite_new->setProject($project_new);
                $em->persist($activite_new);
            }
        }else{
            $parts = $em->getRepository('ArtoProductBundle:Part')->findBy(
                    array('project' => $project->getId()));
            if($parts != null){
                foreach($parts as $part){
                    $part_new = clone $part;
                    $part_new->setProject($project_new);
                    $em->persist($part_new);
                }
            }
            
            $evalICPE = $em->getRepository('ArtoProductBundle:EvaluationIcpe')->findOneBy(
                array('project' => $project->getId()));
           
            if($evalICPE != null){
                $evalICPE_new = clone $evalICPE;
                $evalICPE_new->setProject($project_new);
                $em->persist($evalICPE_new);
            }
            
            $impactsICPE = $em->getRepository('ArtoProductBundle:ImpactIcpe')->findBy(
                array('project' => $project->getId()));
            
            if($impactsICPE != null){
                foreach($impactsICPE as $impactICPE){
                    $impactICPE_new = clone $impactICPE;
                    $impactICPE_new->setProject($project_new);
                    $em->persist($impactICPE_new);
                }    
            }
        }
        
        $project_new->setGreenUser($user);
        
        $em->persist($project_new);
        $em->flush();
    }
    
    /**
     * Duplique un projet ArtoDistrib chez un autre utilisateur
     * @param type $project le projet à dupliquer
     * @param type $user l'utilisateur chez qui copier
     */
    public function duplicateDistribProject($project, $user){
        $em = $this->em;
        
        $project_new = clone $project;
        $project_new->setName($project->getName());
        
        $scenarios = $project->getScenarios();
        
        if(count($scenarios) > 0){
            foreach($scenarios as $scenario){
                $scenario_new = clone $scenario;
                $scenario_new->setProject($project_new);
                $em->persist($scenario_new);
                
                $fluxes = $scenario->getFluxes();
                foreach($fluxes as $flux){
                    $flux_new = clone $flux;
                    $flux_new->setScenario($scenario_new);
                    $em->persist($flux_new);
                    
                    $parts = $flux->getParts();
                    foreach($parts as $part){
                        $part_new = clone $part;
                        $part_new->setFlux($flux_new);
                        $em->persist($part_new);
                    }
                }
            }
        }
        
        //Un dossier ?
        $folder = $project->getFolder();
        //Si il y a un dossier
        if($folder != null){
            //On retrouve le nom du dossier
            $folderName = $folder->getName();
            
            //Recherche du dossier
            $folderCloned = $em->getRepository('ArtoDistribBundle:Folder')->findOneBy(
                    array('name' => $folderName, 'user' => $user->getId()));
            
            //Attribution du dossier au projet cloné
            $project_new->setFolder($folderCloned);
        }

        $project_new->setGreenUser($user);
        
        $em->persist($project_new);
        $em->flush();
    }
    
    /**
     * Duplique un projet ArtoFlux chez un autre utilisateur
     * @param type $project le projet à dupliquer
     * @param type $user l'utilisateur chez qui copier
     */
    public function duplicateFluxProject($project, $user){
        $em = $this->em;
        
        $project_new = clone $project;
        $project_new->setName($project->getName());
        $em->persist($project_new);
        
        $product = $project->getProduct();
        $product_new = new \Arto\FluxBundle\Entity\Product();
        $product_new->setRef($product->getRef());
        $product_new->setName($product->getName());
        $product_new->setWeight($product->getWeight());
        $product_new->setWeightElectronic($product->getWeightElectronic());
        $product_new->setWeightElectronicSummed($product->getWeightElectronicSummed());
        $product_new->setProductLine($product->getProductLine());
        $product_new->setFunctionalUnit($product->getFunctionalUnit());
        $product_new->setCreatedAt($product->getCreatedAt());
        $product_new->setUpdatedAt($product->getUpdatedAt());
        $product_new->setRohs($product->getRohs());
        $product_new->setRohsDetail($product->getRohsDetail());
        $product_new->setDeee($product->getDeee());
        $product_new->setBattery($product->getBattery());
        $product_new->setBatteryExtract($product->getBatteryExtract());
        $product_new->setReach($product->getReach());
        $product_new->setReachDetail($product->getReachDetail());
        $product_new->setProductStandard($product->getProductStandard());
        $product_new->setProductStandardDetail($product->getProductStandardDetail());
        $product_new->setProject($project_new);
        
        $em->persist($product_new);
        
        
        if (count($product->getParts()) > 0) {
            foreach ($product->getParts() as $part) {
                $product_part_new = new \Arto\FluxBundle\Entity\ProductPart();
                $product_part_new->setType($part->getType());
                $product_part_new->setRef($part->getRef());
                $product_part_new->setName($part->getName());
                $product_part_new->setQuantity($part->getQuantity());
                $product_part_new->setWeight($part->getWeight());
                $product_part_new->setCreatedAt($part->getCreatedAt());
                $product_part_new->setUpdatedAt($part->getUpdatedAt());
                $product_part_new->setProduct($product_new);
                $em->persist($product_part_new);
            }
        }
        
        if ($product->getUsage() != null) {
            $usage = clone $product->getUsage();
            $usage->setProduct($product_new);
            $em->persist($usage);

            if (count($product->getUsage()->getParts()) > 0) {
                foreach ($product->getUsage()->getParts() as $part) {
                    $usage_part_new = clone $part;
                    $usage_part_new->setUsage($usage);
                    $em->persist($usage_part_new);
                }
            }
        }

        if ($product->getDelivery() != null) {
            $delivery = clone $product->getDelivery();
            $delivery->setProduct($product_new);
            $em->persist($delivery);

            if (count($product->getDelivery()->getParts()) > 0) {
                foreach ($product->getDelivery()->getParts() as $part) {
                    $delivery_part_new = clone $part;
                    $delivery_part_new->setDelivery($delivery);
                    $em->persist($delivery_part_new);
                }
            }
        }
        
        foreach($project->getManufacturingPlastics() as $mp){
            $mp_new = clone $mp;
            $mp_new->setProject($project_new);
            $project_new->addManufacturingPlastic($mp_new);
            $em->persist($project_new);
        }
        
        foreach($project->getManufacturingMetallics() as $mm){
            $mm_new = clone $mm;
            $mm_new->setProject($project_new);
            $project_new->addManufacturingMetallic($mm_new);
            $em->persist($project_new);
        }
        
        foreach($project->getElectronicPcbParts() as $ePcb){
            $ePcb_new = clone $ePcb;
            $ePcb_new->setProject($project_new);
            $project_new->addElectronicPcbPart($ePcb_new);
            $em->persist($project_new);
        }
        
        foreach($project->getElectronicComponentParts() as $ec){
            $ec_new = clone $ec;
            $ec_new->setProject($project_new);
            $project_new->addElectronicComponentPart($ec_new);
            $em->persist($project_new);
        }
        
        //Un dossier ?
        $folder = $project->getFolder();
        //Si il y a un dossier
        if($folder != null){
            //On retrouve le nom du dossier
            $folderName = $folder->getName();
            
            //Recherche du dossier
            $folderCloned = $em->getRepository('ArtoFluxBundle:Folder')->findOneBy(
                    array('name' => $folderName, 'user' => $user->getId()));
            
            //Attribution du dossier au projet cloné
            $project_new->setFolder($folderCloned);
        }

        $project_new->setGreenUser($user);
        if($user->getAcvUserId() == null){
            $userId = $user->getId();
            $user->setAcvUserId($userId);
            $em->persist($user);
        }
        
        $em->persist($project_new);
        $em->flush();
    }
    
    /**
     * Duplique un projet ArtoAcv chez un autre utilisateur
     * @param type $project le projet à dupliquer
     * @param type $user l'utilisateur chez qui copier
     */
    public function duplicateAcvProject($project, $user){
        $em = $this->em;
        
        $project_new = clone $project;
        $project_new->setName($project->getName());
        
        //$products = array();
        foreach ($project->getProducts() as $product) {
            $product_new = clone $product;
            $product_new->setProject($project_new);
            $em->persist($product_new);


            if (count($product->getParts()) > 0) {
                foreach ($product->getParts() as $part) {
                    $product_part_new = clone $part;
                    $product_part_new->setProduct($product_new);
                    $em->persist($product_part_new);
                }
            }


            foreach ($product->getAssemblies() as $assembly) {
                $assembly_new = clone $assembly;
                $assembly_new->setProduct($product_new);
                $em->persist($assembly_new);

                // Clone parent first
                foreach ($assembly->getParts() as $part) {
                    if ($part->getParent() == null) {
                        $part_new = clone $part;
                        $part_new->setAssembly($assembly_new);
                        $em->persist($part_new);

                        // Clone childs
                        foreach ($part->getChilds() as $child) {
                            $child_new = clone $child;
                            $child_new->setParent($part_new);
                            $child_new->setAssembly($assembly_new);
                            $em->persist($child_new);
                        }
                    }
                }
            }

            if ($product->getUsage() != null) {
                $usage = clone $product->getUsage();
                $usage->setProduct($product_new);
                $em->persist($usage);

                if (count($product->getUsage()->getParts()) > 0) {
                    foreach ($product->getUsage()->getParts() as $part) {
                        $usage_part_new = clone $part;
                        $usage_part_new->setUsage($usage);
                        $em->persist($usage_part_new);
                    }
                }
            }

            if ($product->getDelivery() != null) {
                $delivery = clone $product->getDelivery();
                $delivery->setProduct($product_new);
                $em->persist($delivery);

                if (count($product->getDelivery()->getParts()) > 0) {
                    foreach ($product->getDelivery()->getParts() as $part) {
                        $delivery_part_new = clone $part;
                        $delivery_part_new->setDelivery($delivery);
                        $em->persist($delivery_part_new);
                    }
                }
            }

            if ($product->getEol() != null) {
                $eol = clone $product->getEol();
                $eol->setProduct($product_new);
                $em->persist($eol);
            }
        }
        
        //Un dossier ?
        $folder = $project->getFolder();
        //Si il y a un dossier
        if($folder != null){
            //On retrouve le nom du dossier
            $folderName = $folder->getName();
            
            //Recherche du dossier
            $folderCloned = $em->getRepository('ArtoAcvBundle:Folder')->findOneBy(
                    array('name' => $folderName, 'user' => $user->getId()));
            
            //Attribution du dossier au projet cloné
            $project_new->setFolder($folderCloned);
        }

        $project_new->setGreenUser($user);

        $em->persist($project_new);
        $em->flush();
    }
    
     public function duplicateDefaultProject($project, $user, $softwareName){
        $em = $this->em;
        
        //Gathering projectDefault datas
        $projectDefaultName = $project->getName();
        $projectDefaultPilote = $project->getPilote();
        $projectDefaultPiloteDate = $project->getPiloteDate();
        $projectDefaultPath = $project->getPath();
        $projectDefaultActive = $project->getActive();
        $projectDefaultCreatedAt = $project->getCreatedAt();
        $projectDefaultUpdatedAt = $project->getUpdatedAt();
        
        if($softwareName == 'design'){
            //Creating new ProjectDefault
            $projectDefault_new = new \Arto\DesignBundle\Entity\Project();
            
            $projectDefaultEnergy = $project->getEnergy();
            $projectDefaultEnergyType = $project->getEnergyType();
            $projectDefaultRohs = $project->getRohs();
            $projectDefaultDeee = $project->getDeee();
            $projectDefaultBattery = $project->getBattery();
            $projectDefaultBatteryType = $project->getBatteryType();
            $projectDefaultBatteryExtract = $project->getBatteryExtract();
            $projectDefaultReach = $project->getReach();
            $projectDefaultPower1 = $project->getPower1();
            $projectDefaultPower2 = $project->getPower2();
            $projectDefaultTypeEvaluation = $project->getTypeEvaluation();
            $projectDefaultDescriptionuf = $project->getDescriptionuf();
            
            //Setting other datas as equal as original ProjectDefault
            $projectDefault_new->setEnergy($projectDefaultEnergy);
            $projectDefault_new->setEnergyType($projectDefaultEnergyType);
            $projectDefault_new->setRohs($projectDefaultRohs);
            $projectDefault_new->setDeee($projectDefaultDeee);
            $projectDefault_new->setBattery($projectDefaultBattery);
            $projectDefault_new->setBatteryType($projectDefaultBatteryType);
            $projectDefault_new->setBatteryExtract($projectDefaultBatteryExtract);
            $projectDefault_new->setReach($projectDefaultReach);
            $projectDefault_new->setPower1($projectDefaultPower1);
            $projectDefault_new->setPower2($projectDefaultPower2);
            $projectDefault_new->setTypeEvaluation($projectDefaultTypeEvaluation);
            $projectDefault_new->setDescriptionuf($projectDefaultDescriptionuf);
        }elseif($softwareName == 'urba'){
            //Creating new ProjectDefault
            $projectDefault_new = new \Arto\ArchiBundle\Entity\Project();
            
            $projectDefaultProjectName = $project->getProjectName();
            $projectDefaultAddress1 = $project->getAddress1();
            $projectDefaultAddress2 = $project->getAddress2();
            $projectDefaultCity = $project->getCity();
            $projectDefaultSurface = $project->getSurface();
            $projectDefaultPopulation = $project->getPopulation();
            $projectDefaultHousing = $project->getHousing();
            $projectDefaultOther = $project->getOther();
            
            //Setting other datas as equal as original ProjectDefault
            $projectDefault_new->setProjectName($projectDefaultProjectName);
            $projectDefault_new->setAddress1($projectDefaultAddress1);
            $projectDefault_new->setAddress2($projectDefaultAddress2);
            $projectDefault_new->setCity($projectDefaultCity);
            $projectDefault_new->setSurface($projectDefaultSurface);
            $projectDefault_new->setPopulation($projectDefaultPopulation);
            $projectDefault_new->setHousing($projectDefaultHousing);
            $projectDefault_new->setOther($projectDefaultOther);
        }

        //Changing some datas for clone
        $projectDefault_new->setGreenUser($user);
        $projectDefault_new->setName($projectDefaultName);
        //$projectDefault_new->setEnergy($projectDefault->getEnergy());
        $projectDefault_new->setBase(0);
        
        //Setting other datas as equal as original ProjectDefault
        $projectDefault_new->setPilote($projectDefaultPilote);
        $projectDefault_new->setPiloteDate($projectDefaultPiloteDate);
        $projectDefault_new->setPath($projectDefaultPath);
        $projectDefault_new->setActive($projectDefaultActive);
        $projectDefault_new->setCreatedAt($projectDefaultCreatedAt);
        $projectDefault_new->setUpdatedAt($projectDefaultUpdatedAt);
        
        $em->persist($projectDefault_new);
         
        return $projectDefault_new;
    }
      
    public function duplicateDesignProject($project, $user, $projectDefault){
        $em = $this->em;
        
        $projectDefault_new = $this->duplicateDefaultProject($projectDefault, $user, 'design');
        $project_new = clone $project;
        $project_new->setGreenUser($user);
        $project_new->setName($project->getName());
        $project_new->setProjectName($project->getProjectName());
        $project_new->setEnergy($project->getEnergy());
        $project_new->setBase(1);
        $project_new->setDefault($projectDefault_new);
        
         //Un dossier ?
        $folder = $project->getFolder();
        //Si il y a un dossier
        if($folder != null){
            //On retrouve le nom du dossier
            $folderName = $folder->getName();
            
            //Recherche du dossier
            $folderCloned = $em->getRepository('ArtoDesignBundle:Folder')->findOneBy(
                    array('name' => $folderName, 'user' => $user->getId()));
            
            //Attribution du dossier au projet cloné
            $project_new->setFolder($folderCloned);
        }

        $em->persist($project_new);  
        
        foreach($project->getNotes() as $note){
            $note_new = clone $note;
            $note_new->setProject($project_new);
            $em->persist($note_new);
            //$project_new->addNote($note_new);
        }
        
        foreach($projectDefault->getNotes() as $noteDefault){
            $noteDefault_new = clone $noteDefault;
            $noteDefault_new->setProject($projectDefault_new);
            $em->persist($noteDefault_new);
            //$project_new->addNote($note_new);
        }
        
        foreach($project->getActions() as $action){
            $action_new = clone $action;
            $action_new->setProject($project_new);
            $em->persist($action_new);
            //$project_new->addAction($action_new);
        }
        
        foreach($projectDefault->getActions() as $actionDefault){
            $actionDefault_new = clone $actionDefault;
            $actionDefault_new->setProject($projectDefault_new);
            $em->persist($actionDefault_new);
            //$project_new->addAction($action_new);
        }
        
        foreach($project->getOperations() as $operation){
            $operation_new = clone $operation;
            $operation_new->setProject($project_new);
            $em->persist($operation_new);
        }
        
        foreach($project->getUsers() as $projectUser){
            $user_new = clone $projectUser;
            $user_new->setProject($project_new);
            $em->persist($user_new);
        }
        
        $em->flush();
    }
    
    public function duplicateArchiProject($project, $user, $projectDefault){
        $em = $this->em;
        
        $projectDefault_new = $this->duplicateDefaultProject($projectDefault, $user, 'urba');
        $project_new = clone $project;
        
        $project_new->setGreenUser($user);
        $project_new->setName($project->getName());
        $project_new->setProjectName($project->getProjectName());
        //$project_new->setEnergy($project->getEnergy());
        $project_new->setBase(1);
        $project_new->setDefault($projectDefault_new);
        
         //Un dossier ?
        $folder = $project->getFolder();
        //Si il y a un dossier
        if($folder != null){
            //On retrouve le nom du dossier
            $folderName = $folder->getName();
            
            //Recherche du dossier
            $folderCloned = $em->getRepository('ArtoArchiBundle:Folder')->findOneBy(
                    array('name' => $folderName, 'user' => $user->getId()));
            
            //Attribution du dossier au projet cloné
            $project_new->setFolder($folderCloned);
        }

        $em->persist($project_new);  
        
        foreach($project->getNotes() as $note){
            $note_new = clone $note;
            $note_new->setProject($project_new);
            $em->persist($note_new);
            //$project_new->addNote($note_new);
        }
        
        foreach($projectDefault->getNotes() as $noteDefault){
            $noteDefault_new = clone $noteDefault;
            $noteDefault_new->setProject($projectDefault_new);
            $em->persist($noteDefault_new);
            //$project_new->addNote($note_new);
        }
        
        foreach($project->getHelpNotes() as $helpNote){
            $helpNote_new = clone $helpNote;
            $helpNote_new->setProject($project_new);
            $em->persist($helpNote_new);
        }
        
        foreach($projectDefault->getHelpNotes() as $helpNoteDefault){
            $helpNoteDefault_new = clone $helpNoteDefault;
            $helpNoteDefault_new->setProject($projectDefault_new);
            $em->persist($helpNoteDefault_new);   
        }
        
        foreach($project->getActions() as $action){
            $action_new = clone $action;
            $action_new->setProject($project_new);
            $em->persist($action_new);
            //$project_new->addAction($action_new);
        }
        
        foreach($projectDefault->getActions() as $actionDefault){
            $actionDefault_new = clone $actionDefault;
            $actionDefault_new->setProject($projectDefault_new);
            $em->persist($actionDefault_new);
            //$project_new->addAction($action_new);
        }
        
        foreach($project->getOperations() as $operation){
            $operation_new = clone $operation;
            $operation_new->setProject($project_new);
            $em->persist($operation_new);
        }
        
        foreach($project->getUsers() as $projectUser){
            $user_new = clone $projectUser;
            $user_new->setProject($project_new);
            $em->persist($user_new);
        }
        
        $em->flush();
    }
}
