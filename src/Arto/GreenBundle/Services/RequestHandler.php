<?php

namespace Arto\GreenBundle\Services;

use Arto\AcvBundle\Entity\Product;
use Arto\AcvBundle\Entity\Project;
use Arto\AcvBundle\Entity\Folder;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RequestHandler {
    
    protected $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function getSoftwareName($softwareId){
        $em = $this->em;
        
        $software = $em->getRepository('ArtoGreenBundle:Product')->find($softwareId);
        $softwareName = $software->getName();
        
        //Si nom = ArtoUrba => ArtoArchi
        if($softwareName == 'ArtoUrba'){
            $softwareName = 'ArtoArchi';
        }
        //Si nom = ArtoACV => ArtoAcv
        elseif($softwareName == 'ArtoACV'){
            $softwareName = 'ArtoAcv';
        }
        //Si nom = ArtoProd => ArtoProduct
        elseif($softwareName == 'ArtoProd'){
            $softwareName = 'ArtoProduct';
        }
        
        
        return $softwareName;
    }
     
    public function findFoldersByUser($userId, $softwareName, $withCode = 'no'){
        $em = $this->em;
        
        $sqlSelect = "SELECT c FROM ".$softwareName."Bundle:Folder c";
        $sqlWhereUser = " WHERE c.user=:greenUser";
        $sqlWithCode = " AND c.code <> :code";
        $sqlOrderBy = " ORDER BY c.name";
        
        if($withCode == "yes"){
            $sql = $sqlSelect.$sqlWhereUser.$sqlWithCode.$sqlOrderBy;
            $query = $em->createQuery($sql)->setParameters(
                    array("greenUser" => $userId, "code" => ""));
        }else{
            $sql = $sqlSelect.$sqlWhereUser.$sqlOrderBy;
            $query = $em->createQuery($sql)->setParameters(
                    array("greenUser" => $userId));
        }
        
        $folders = $query->getResult();
        
        return $folders;
    }
    
    
    public function findProjectsByFolder($userId, $softwareName, $folderId){
        $em = $this->em;
        
        $sqlSelect = "SELECT c FROM ".$softwareName."Bundle:Project c";
        $sqlWhereUser = " WHERE c.greenUser=:greenUser";
        
        if($folderId != 0){
            $sqlHasFolder = " AND c.folder=:folder";
        }else{
            $sqlHasFolder = " AND c.folder IS NULL";
        }
       
        $sqlOrderBy = " ORDER BY c.name";
        
        if($softwareName == 'ArtoArchi' || $softwareName == 'ArtoDesign'){
            $sqlDefaultNotNull = " AND c.default IS NOT NULL";
            $sql = $sqlSelect.$sqlWhereUser.$sqlHasFolder.$sqlDefaultNotNull.$sqlOrderBy;
        }else{
            $sql = $sqlSelect.$sqlWhereUser.$sqlHasFolder.$sqlOrderBy;
        }
        
        if ($folderId != 0) {
            $query = $em->createQuery($sql)->setParameters(
                    array("greenUser" => $userId, "folder" => $folderId));
        }else {
            $query = $em->createQuery($sql)->setParameters(
                    array("greenUser" => $userId));
        }
        
        $projects = $query->getResult();
        
        return $projects;
    }
    
    public function findLicensesForUser($user){
        $em = $this->em;
        
        $licenses = $em->getRepository('ArtoGreenBundle:License')->findBy(
                 array('user' => $user));
        
        return $licenses;
    }
    
    public function findLicenseForUser($user, $softwareId){
        $em = $this->em;
        
        $license = $em->getRepository('ArtoGreenBundle:License')->findOneBy(
                array('user' => $user, 'product' => $softwareId));
        
        return $license;
        
    }
    
    public function findCodeForUser($user){
        $em = $this->em;
        
        $greenUser = $em->getRepository('ArtoGreenBundle:User')->find($user);
        $userCode = $greenUser->getPassword();
        
        return $userCode;
    }
    
    public function findCodeForFolder($folderId, $softwareName){
        $em = $this->em;
        
        $repositoryName = $softwareName."Bundle:Folder";
        
        $folder = $em->getRepository($repositoryName)->find($folderId);
        $code = $folder->getCode();
        
        return $code;
    }
    
    public function findAllUsers(){
        $em = $this->em;
        
        $sql = "SELECT c FROM ArtoGreenBundle:User c WHERE c.isOld=:old ORDER BY c.username";
        
        $query = $em->createQuery($sql)->setParameters(
                    array("old" => false));
        $users = $query->getResult();
        
        return $users;
    }
    
    public function findAllSoftwares(){
        $em = $this->em;
        
        $softwares = $em->getRepository('ArtoGreenBundle:Product')->findAll();
        
        return $softwares;
    }
    
    public function findUserLetters() {
        $users = $this->findAllUsers();
        $userLetters = array();
        
        foreach ($users as $user) {
            $userName = $user->getUsername();
            $firstLetter = strtoupper(substr($userName, 0, 1));

            if(!in_array($firstLetter, $userLetters)) {
                array_push($userLetters, $firstLetter);
            }
        }
        
        return $userLetters;
    }
    
    public function findUsersByLetter($letter) {
        $users = $this->findAllUsers();
        $correctUsers = array();
        
        foreach($users as $user){
            $userName = $user->getUsername();
            $firstLetter = strtoupper(substr($userName, 0, 1));
            
            if($firstLetter == $letter){
                if(!in_array($user, $correctUsers)) {
                    array_push($correctUsers, $user);
                }
            }
        }
        
        return $correctUsers;
    }
    
    public function deleteFolder($folderId, $softwareName){
        $em = $this->em;
          
        $repositoryName = $softwareName.'Bundle:Folder';
        
        $folder = $em->getRepository($repositoryName)->find($folderId);
        
        $em->remove($folder);
        $em->flush();
    }
    
    public function deleteProjects($user, $softwareName){
         $em = $this->em;
         
         $repositoryName = $softwareName."Bundle:Project";
         
         if($softwareName == 'ArtoDesign' || $softwareName == 'ArtoArchi'){
            $projects = $em->getRepository($repositoryName)->findBy(
                    array('greenUser' => $user));
            
            /*
            foreach($projects as $project){
                $project->setDefault($projectDesign);
            }
            
            $em->flush();*/
            
            foreach($projects as $project){
                $em->remove($project);
            }
              
         }else{
            $projects = $em->getRepository($repositoryName)->findBy(
                    array('greenUser' => $user));

            foreach($projects as $project){
                $em->remove($project);
            }
         }
         
         $em->flush();
    }
    
    public function deleteLicenses($user){
        $em = $this->em;
        
        $licenses = $em->getRepository('ArtoGreenBundle!License')->findBy(
                array('user' => $user));
        
        foreach($licenses as $license){
            $em->remove($license);
        }
        
        $em->flush();
    }
}