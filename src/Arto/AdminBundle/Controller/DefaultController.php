<?php

namespace Arto\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DefaultController extends Controller
{

    public function indexAction()
    {
        return $this->render('ArtoAdminBundle:Default:index.html.twig');
    }

    public function updateAction()
    {
        passthru('cd '.__DIR__.'/../../../../; svnupdate; app/console doctrine:schema:update --force; app/console cache:clear --env=prod', $result);
        print_r($result);
        exit();
    }
}
