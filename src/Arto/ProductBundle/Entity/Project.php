<?php
namespace Arto\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Arto\ProductBundle\Repository\ProjectRepository")
 * @ORM\Table(name="product_project")
 * @ORM\HasLifecycleCallbacks
 */
class Project
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="uniq", type="string", length=32, nullable=false)
     */
    private $uniq;

    /**
     * @ORM\Column(name="name", type="string", length=32, nullable=false)
     * @Assert\NotBlank()
     */
    private $name;
    
    /**
     * @ORM\Column(name="projectName", type="string", length=64, nullable = true)
     */
    private $projectName;

    /**
     *@ORM\Column(name="numero", type="integer")
     *@Assert\NotBlank()
     */
    private $numero;

    /**
     *@ORM\Column(name="productName", type="string")
     *@Assert\NotBlank()
     */
    private $productName;

    /**
     * @ORM\Column(name="reference", type="integer")
     * @Assert\NotBlank()
     */
    private $reference;

    /**
     * @ORM\Column(name="pilote", type="string", length=255, nullable=true)
     */
    private $pilote;

    /**
     * @ORM\Column(name="pilote_date", type="date", nullable=true)
     */
    private $piloteDate;

    /**
     * @var string $path
     *
     * @ORM\Column(name="path", type="string", length=100, nullable=true)
     */
    private $path;

    /**
     * @Assert\File(maxSize="10000000")
     */
    public $file;

    /**
     * to get the image path (slug + extension)
     */
    private $image;

    /**
     * a property used temporarily while deleting
     */
    private $filenameForRemove;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $acceptable;
    
    /**
     * @ORM\Column(type="boolean")
     */
    private $impactant;
    
    /**
     * @ORM\Column(name="commentaires", type="string", length=5000, nullable=true)
     */
    private $commentaires;
    
    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
    
    /**
     * @var Folder
     *
     * @ORM\ManyToOne(targetEntity="Folder", inversedBy="projects")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="folder_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $folder;

    /**
     * @ORM\OneToMany(targetEntity="Part", mappedBy="project", cascade={"remove"})
     */
    private $parts;

    /**
     * @ORM\OneToMany(targetEntity="ImpactIcpe", mappedBy="project", cascade={"remove"})
     */
    private $impacts;

    /**
     * @var GreenUser
     *
     * @ORM\ManyToOne(targetEntity="Arto\GreenBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="green_user_id", referencedColumnName="id")
     * })
     */
    private $greenUser;
    
     /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="project", cascade={"remove"})
     */
    private $users;
    
    /**
     * @ORM\OneToMany(targetEntity="EvaluationIcpe", mappedBy="project", cascade={"remove"})
     */
    private $evaluationsIcpe;


    public function __construct()
    {
        $this->uniq = md5( uniqid('artoproduct', true) );
        $this->active = true;
    }

    public function __toString()
    {
        return $this->getName();
    }



    /**
     * Methods for upload
     */

    public function getAbsolutePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/product/project';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            $this->path = $this->file->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        $this->file->move($this->getUploadRootDir(), $this->getImage());

        unset($this->file);
    }

    /**
     * @ORM\PreRemove()
     */
    public function storeFilenameForRemove()
    {
        $this->filenameForRemove = $this->getAbsolutePath();
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($this->filenameForRemove) {
            unlink($this->filenameForRemove);
        }
    }

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uniq
     *
     * @param string $uniq
     */
    public function setUniq($uniq)
    {
        $this->uniq = $uniq;
    }

    /**
     * Get uniq
     *
     * @return string 
     */
    public function getUniq()
    {
        return $this->uniq;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set projectName
     *
     * @param string $projectName
     */
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;
    }

    /**
     * Get projectName
     *
     * @return string 
     */
    public function getProjectName()
    {
        return $this->projectName;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * Get numero
     *
     * @return integer 
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set productName
     *
     * @param string $productName
     */
    public function setProductName($productName)
    {
        $this->productName = $productName;
    }

    /**
     * Get productName
     *
     * @return string 
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * Set reference
     *
     * @param integer $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * Get reference
     *
     * @return integer 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set pilote
     *
     * @param string $pilote
     */
    public function setPilote($pilote)
    {
        $this->pilote = $pilote;
    }

    /**
     * Get pilote
     *
     * @return string 
     */
    public function getPilote()
    {
        return $this->pilote;
    }

    /**
     * Set piloteDate
     *
     * @param date $piloteDate
     */
    public function setPiloteDate($piloteDate)
    {
        $this->piloteDate = $piloteDate;
    }

    /**
     * Get piloteDate
     *
     * @return date 
     */
    public function getPiloteDate()
    {
        return $this->piloteDate;
    }

    /**
     * Set path
     *
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set active
     *
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set acceptable
     *
     * @param boolean $acceptable
     */
    public function setAcceptable($acceptable)
    {
        $this->acceptable = $acceptable;
    }

    /**
     * Get acceptable
     *
     * @return boolean 
     */
    public function getAcceptable()
    {
        return $this->acceptable;
    }

    /**
     * Set impactant
     *
     * @param boolean $impactant
     */
    public function setImpactant($impactant)
    {
        $this->impactant = $impactant;
    }

    /**
     * Get impactant
     *
     * @return boolean 
     */
    public function getImpactant()
    {
        return $this->impactant;
    }

    /**
     * Set commentaires
     *
     * @param string $commentaires
     */
    public function setCommentaires($commentaires)
    {
        $this->commentaires = $commentaires;
    }

    /**
     * Get commentaires
     *
     * @return string 
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add parts
     *
     * @param Arto\ProductBundle\Entity\Part $parts
     */
    public function addPart(\Arto\ProductBundle\Entity\Part $parts)
    {
        $this->parts[] = $parts;
    }

    /**
     * Get parts
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getParts()
    {
        return $this->parts;
    }

    /**
     * Add impacts
     *
     * @param Arto\ProductBundle\Entity\ImpactIcpe $impacts
     */
    public function addImpactIcpe(\Arto\ProductBundle\Entity\ImpactIcpe $impacts)
    {
        $this->impacts[] = $impacts;
    }

    /**
     * Get impacts
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getImpacts()
    {
        return $this->impacts;
    }

    /**
     * Set greenUser
     *
     * @param Arto\GreenBundle\Entity\User $greenUser
     */
    public function setGreenUser(\Arto\GreenBundle\Entity\User $greenUser)
    {
        $this->greenUser = $greenUser;
    }

    /**
     * Get greenUser
     *
     * @return Arto\GreenBundle\Entity\User 
     */
    public function getGreenUser()
    {
        return $this->greenUser;
    }

    /**
     * Add users
     *
     * @param Arto\ProductBundle\Entity\User $users
     */
    public function addUser(\Arto\ProductBundle\Entity\User $users)
    {
        $this->users[] = $users;
    }

    /**
     * Get users
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add evaluationsIcpe
     *
     * @param Arto\ProductBundle\Entity\EvaluationIcpe $evaluationsIcpe
     */
    public function addEvaluationIcpe(\Arto\ProductBundle\Entity\EvaluationIcpe $evaluationsIcpe)
    {
        $this->evaluationsIcpe[] = $evaluationsIcpe;
    }

    /**
     * Get evaluationsIcpe
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getEvaluationsIcpe()
    {
        return $this->evaluationsIcpe;
    }
    
     /**
     * Set folder
     *
     * @param Arto\ProductBundle\Entity\Folder $folder
     */
    public function setFolder(\Arto\ProductBundle\Entity\Folder $folder = null)
    {
        $this->folder = $folder;
    }

    /**
     * Get folder
     *
     * @return Arto\ProductBundle\Entity\Folder 
     */
    public function getFolder()
    {
        return $this->folder;
    }
}