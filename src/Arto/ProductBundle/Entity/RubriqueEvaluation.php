<?php
namespace Arto\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_rubriques_evaluation")
 * @ORM\HasLifecycleCallbacks
 */
class RubriqueEvaluation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var Rubrique
     *
     * @ORM\ManyToOne(targetEntity="Rubrique")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="rubrique_id", referencedColumnName="id")
     * })
     */
    private $rubrique;
    
    /**
     * @var EvaluationIcpe
     *
     * @ORM\ManyToOne(targetEntity="EvaluationIcpe")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="evaluationIcpe_id", referencedColumnName="id")
     * })
     */
    private $evaluationIcpe;
    
    /**
     * @ORM\Column(name="value", type="string", nullable=true)
     * @Assert\NotBlank()
     */
    private $value;
    
     /**
     * @ORM\Column(name="commentaire", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     */
    private $commentaire;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
   
    
    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
   
    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }  

    /**
     * Set value
     *
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;
    }

    /**
     * Get commentaire
     *
     * @return string 
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * Set rubrique
     *
     * @param Arto\ProductBundle\Entity\Rubrique $rubrique
     */
    public function setRubrique(\Arto\ProductBundle\Entity\Rubrique $rubrique)
    {
        $this->rubrique = $rubrique;
    }

    /**
     * Get rubrique
     *
     * @return Arto\ProductBundle\Entity\Rubrique 
     */
    public function getRubrique()
    {
        return $this->rubrique;
    }

    /**
     * Set evaluationIcpe
     *
     * @param Arto\ProductBundle\Entity\EvaluationIcpe $evaluationIcpe
     */
    public function setEvaluationIcpe(\Arto\ProductBundle\Entity\EvaluationIcpe $evaluationIcpe)
    {
        $this->evaluationIcpe = $evaluationIcpe;
    }

    /**
     * Get evaluationIcpe
     *
     * @return Arto\ProductBundle\Entity\EvaluationIcpe 
     */
    public function getEvaluationIcpe()
    {
        return $this->evaluationIcpe;
    }

    /**
     * Set typeEvaluation
     *
     * @param string $typeEvaluation
     */
    public function setTypeEvaluation($typeEvaluation)
    {
        $this->typeEvaluation = $typeEvaluation;
    }

    /**
     * Get typeEvaluation
     *
     * @return string 
     */
    public function getTypeEvaluation()
    {
        return $this->typeEvaluation;
    }
}