<?php
namespace Arto\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_situation_reelle")
 * @ORM\HasLifecycleCallbacks
 */
class SituationReelle
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="volume_activity", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     */
    private $volumeActivite;
    
    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
    
     /**
     * @var Regime
     *
     * @ORM\ManyToOne(targetEntity="Regime")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="regime_id", referencedColumnName="id")
     * })
     */
    private $regime;
    
    /**
     * @var Rubrique
     *
     * @ORM\ManyToOne(targetEntity="Rubrique")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="rubrique_id", referencedColumnName="id")
     * })
     */
    private $rubrique;
    
    public function __construct()
    {

    }

    public function __toString()
    {
        return "";
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
        
    /**
     * Set volumeActivite
     *
     * @param string $volumeActivite
     */
    public function setVolumeActivite($volumeActivite)
    {
        $this->volumeActivite = $volumeActivite;
    }

    /**
     * Get volumeActivite
     *
     * @return string 
     */
    public function getVolumeActivite()
    {
        return $this->volumeActivite;
    }

    /**
     * Set regime
     *
     * @param Arto\ProductBundle\Entity\Regime $regime
     */
    public function setRegime(\Arto\ProductBundle\Entity\Regime $regime)
    {
        $this->regime = $regime;
    }

    /**
     * Get regime
     *
     * @return Arto\ProductBundle\Entity\Regime 
     */
    public function getRegime()
    {
        return $this->regime;
    }


    /**
     * Set rubrique
     *
     * @param Arto\ProductBundle\Entity\Rubrique $rubrique
     */
    public function setRubrique(\Arto\ProductBundle\Entity\Rubrique $rubrique)
    {
        $this->rubrique = $rubrique;
    }

    /**
     * Get rubrique
     *
     * @return Arto\ProductBundle\Entity\Rubrique 
     */
    public function getRubrique()
    {
        return $this->rubrique;
    }
}