<?php
namespace Arto\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_family")
 * @ORM\HasLifecycleCallbacks
 */
class Family
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(name="impact", type="text", nullable=true)
     */
    private $impact;

    /**
     * @ORM\Column(name="usage_terms", type="text", nullable=true)
     */
    private $usageTerms;

    /**
     * @ORM\Column(name="rank1", type="string", length=20, nullable=false)
     * @Assert\NotBlank()
     */
    private $rank1;

    /**
     * @ORM\Column(name="rank2", type="string", length=20, nullable=false)
     * @Assert\NotBlank()
     */
    private $rank2;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Process
     *
     * @ORM\ManyToOne(targetEntity="Process", inversedBy="parts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="process_id", referencedColumnName="id")
     * })
     */
    private $process;
    
     /**
     * @ORM\Column(name="impactTotal", type="text", nullable=true)
     */
    private $totalImpact;
            


    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function getRankStyle($rank)
    {
        if ($rank == 'Acceptable') {
            return "rank-acceptable";
        }
        if ($rank == 'Non Acceptable') {
            return "rank-nonacceptable";
        }
        if ($rank == 'Propre') {
            return "rank-clean";
        }
    }

    public function getRank1Style()
    {
        return $this->getRankStyle($this->rank1);
    }

    public function getRank2Style()
    {
        return $this->getRankStyle($this->rank2);
    }



    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set impact
     *
     * @param text $impact
     */
    public function setImpact($impact)
    {
        $this->impact = $impact;
    }

    /**
     * Get impact
     *
     * @return text
     */
    public function getImpact()
    {
        return $this->impact;
    }

    /**
     * Set rank1
     *
     * @param string $rank1
     */
    public function setRank1($rank1)
    {
        $this->rank1 = $rank1;
    }

    /**
     * Get rank1
     *
     * @return string
     */
    public function getRank1()
    {
        return $this->rank1;
    }

    /**
     * Set rank2
     *
     * @param string $rank2
     */
    public function setRank2($rank2)
    {
        $this->rank2 = $rank2;
    }

    /**
     * Get rank2
     *
     * @return string
     */
    public function getRank2()
    {
        return $this->rank2;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set process
     *
     * @param Arto\ProductBundle\Entity\Process $process
     */
    public function setProcess(\Arto\ProductBundle\Entity\Process $process)
    {
        $this->process = $process;
    }

    /**
     * Get process
     *
     * @return Arto\ProductBundle\Entity\Process
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * Set usageTerms
     *
     * @param text $usageTerms
     */
    public function setUsageTerms($usageTerms)
    {
        $this->usageTerms = $usageTerms;
    }

    /**
     * Get usageTerms
     *
     * @return text
     */
    public function getUsageTerms()
    {
        return $this->usageTerms;
    }

    /**
     * Set totalImpact
     *
     * @param text $totalImpact
     */
    public function setTotalImpact($totalImpact)
    {
        $this->totalImpact = $totalImpact;
    }

    /**
     * Get totalImpact
     *
     * @return text 
     */
    public function getTotalImpact()
    {
        return $this->totalImpact;
    }
}