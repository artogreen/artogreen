<?php
namespace Arto\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_rubriques")
 * @ORM\HasLifecycleCallbacks
 */
class Rubrique
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(name="activites", type="text", nullable=false)
     */
    private $activites;
    
    /**
     * @ORM\Column(name="seuil_autorisation_servitudes", type="text", nullable=true)
     */
    private $seuilAutorisationServitudes;
    
    /**
     * @ORM\Column(name="seuil_autorisation", type="text", nullable=true)
     */
    private $seuilAutorisation;
    
    /**
     * @ORM\Column(name="seuil_enregistrement", type="text", nullable=true)
     */
    private $seuilEnregistrement;
    
    /**
     * @ORM\Column(name="seuil_declaration_controle", type="text", nullable=true)
     */
    private $seuilDeclarationControle;
    
    /**
     * @ORM\Column(name="seuil_declaration", type="text", nullable=true)
     */
    private $seuilDeclaration;
 
    /**
     * @ORM\Column(name="unit", type="text", nullable=true)
     */
    private $unit;
    
    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
   
    
    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

   
    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    

    /**
     * Set activites
     *
     * @param text $activites
     */
    public function setActivites($activites)
    {
        $this->activites = $activites;
    }

    /**
     * Get activites
     *
     * @return text 
     */
    public function getActivites()
    {
        return $this->activites;
    }

    /**
     * Set seuilDeclaration
     *
     * @param text $seuilDeclaration
     */
    public function setSeuilDeclaration($seuilDeclaration)
    {
        $this->seuilDeclaration = $seuilDeclaration;
    }

    /**
     * Get seuilDeclaration
     *
     * @return text 
     */
    public function getSeuilDeclaration()
    {
        return $this->seuilDeclaration;
    }

    /**
     * Set seuilEnregistrement
     *
     * @param text $seuilEnregistrement
     */
    public function setSeuilEnregistrement($seuilEnregistrement)
    {
        $this->seuilEnregistrement = $seuilEnregistrement;
    }

    /**
     * Get seuilEnregistrement
     *
     * @return text 
     */
    public function getSeuilEnregistrement()
    {
        return $this->seuilEnregistrement;
    }

    /**
     * Set seuilAutorisation
     *
     * @param text $seuilAutorisation
     */
    public function setSeuilAutorisation($seuilAutorisation)
    {
        $this->seuilAutorisation = $seuilAutorisation;
    }

    /**
     * Get seuilAutorisation
     *
     * @return text 
     */
    public function getSeuilAutorisation()
    {
        return $this->seuilAutorisation;
    }

    /**
     * Set seuilAutorisationServitudes
     *
     * @param text $seuilAutorisationServitudes
     */
    public function setSeuilAutorisationServitudes($seuilAutorisationServitudes)
    {
        $this->seuilAutorisationServitudes = $seuilAutorisationServitudes;
    }

    /**
     * Get seuilAutorisationServitudes
     *
     * @return text 
     */
    public function getSeuilAutorisationServitudes()
    {
        return $this->seuilAutorisationServitudes;
    }

    /**
     * Set unit
     *
     * @param text $unit
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
    }

    /**
     * Get unit
     *
     * @return text 
     */
    public function getUnit()
    {
        return $this->unit;
    }
    

    /**
     * Set seuilDeclarationControle
     *
     * @param text $seuilDeclarationControle
     */
    public function setSeuilDeclarationControle($seuilDeclarationControle)
    {
        $this->seuilDeclarationControle = $seuilDeclarationControle;
    }

    /**
     * Get seuilDeclarationControle
     *
     * @return text 
     */
    public function getSeuilDeclarationControle()
    {
        return $this->seuilDeclarationControle;
    }
}