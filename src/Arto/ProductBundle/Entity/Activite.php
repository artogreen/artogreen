<?php
namespace Arto\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_activite")
 * @ORM\HasLifecycleCallbacks
 */
class Activite
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     */
    private $name;
   
    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
    
     /**
     * @var SituationAdministrative
     *
     * @ORM\ManyToOne(targetEntity="SituationAdministrative")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="situation_administrative_id", referencedColumnName="id")
     * })
     */
    private $situationAdministrative;
    
    /**
     * @var SituationReelle
     *
     * @ORM\ManyToOne(targetEntity="SituationReelle")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="situation_reelle_id", referencedColumnName="id")
     * })
     */
    private $situationReelle;
    
    /**
     * @var Rubrique
     *
     * @ORM\ManyToOne(targetEntity="Rubrique")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="rubrique_id", referencedColumnName="id")
     * })
     */
    private $rubrique;
    
     /**
     * @var Conformite
     *
     * @ORM\ManyToOne(targetEntity="Conformite")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="conformite_id", referencedColumnName="id")
     * })
     */
    private $conformite;
    
     /**
     * @ORM\Column(name="commentaire", type="string", length=255, nullable=true)
     */
    private $commentaire;
    
     /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;

    
    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

   
    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    

    /**
     * Set situationAdministrative
     *
     * @param Arto\ProductBundle\Entity\SituationAdministrative $situationAdministrative
     */
    public function setSituationAdministrative(\Arto\ProductBundle\Entity\SituationAdministrative $situationAdministrative)
    {
        $this->situationAdministrative = $situationAdministrative;
    }

    /**
     * Get situationAdministrative
     *
     * @return Arto\ProductBundle\Entity\SituationAdministrative 
     */
    public function getSituationAdministrative()
    {
        return $this->situationAdministrative;
    }

    /**
     * Set situationReelle
     *
     * @param Arto\ProductBundle\Entity\SituationReelle $situationReelle
     */
    public function setSituationReelle(\Arto\ProductBundle\Entity\SituationReelle $situationReelle)
    {
        $this->situationReelle = $situationReelle;
    }

    /**
     * Get situationReelle
     *
     * @return Arto\ProductBundle\Entity\SituationReelle 
     */
    public function getSituationReelle()
    {
        return $this->situationReelle;
    }

    /**
     * Set rubrique
     *
     * @param Arto\ProductBundle\Entity\Rubrique $rubrique
     */
    public function setRubrique(\Arto\ProductBundle\Entity\Rubrique $rubrique)
    {
        $this->rubrique = $rubrique;
    }

    /**
     * Get rubrique
     *
     * @return Arto\ProductBundle\Entity\Rubrique 
     */
    public function getRubrique()
    {
        return $this->rubrique;
    }

    /**
     * Set project
     *
     * @param Arto\ProductBundle\Entity\Project $project
     */
    public function setProject(\Arto\ProductBundle\Entity\Project $project)
    {
        $this->project = $project;
    }

    /**
     * Get project
     *
     * @return Arto\ProductBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;
    }

    /**
     * Get commentaire
     *
     * @return string 
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * Set conformite
     *
     * @param Arto\ProductBundle\Entity\Conformite $conformite
     */
    public function setConformite(\Arto\ProductBundle\Entity\Conformite $conformite)
    {
        $this->conformite = $conformite;
    }

    /**
     * Get conformite
     *
     * @return Arto\ProductBundle\Entity\Conformite 
     */
    public function getConformite()
    {
        return $this->conformite;
    }
}