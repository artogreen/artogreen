<?php
namespace Arto\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_evaluation_icpe")
 * @ORM\HasLifecycleCallbacks
 */
class EvaluationIcpe
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="nom_correspondant", type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $nomCorrespondant;
    
    /**
     * @var datetime $dateReponse
     *
     * @ORM\Column(name="date_reponse", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $dateReponse;
    
    /**
     * @ORM\Column(name="impact", type="integer")
     */
    private $impact;
    
    /**
     * @ORM\Column(name="typeEvaluation", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     */
    private $typeEvaluation;
    
    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;  
     
    /**
     * @var RubriqueEvaluation
     *
     * @ORM\ManyToOne(targetEntity="RubriqueEvaluation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="rubriqueEvaluation_id", referencedColumnName="id")
     * })
     */
    private $rubriqueEvaluation;
    
     /**
     * @ORM\Column(name="commentaire", type="string", length=10000, nullable=true)
     * @Assert\NotBlank()
     */
    private $commentaire;
    
     /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="evaluationsIcpe")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;

    
    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

   
    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set project
     *
     * @param Arto\ProductBundle\Entity\Project $project
     */
    public function setProject(\Arto\ProductBundle\Entity\Project $project)
    {
        $this->project = $project;
    }

    /**
     * Get project
     *
     * @return Arto\ProductBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;
    }

    /**
     * Get commentaire
     *
     * @return string 
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * Set nomCorrespondant
     *
     * @param string $nomCorrespondant
     */
    public function setNomCorrespondant($nomCorrespondant)
    {
        $this->nomCorrespondant = $nomCorrespondant;
    }

    /**
     * Get nomCorrespondant
     *
     * @return string 
     */
    public function getNomCorrespondant()
    {
        return $this->nomCorrespondant;
    }

    /**
     * Set dateReponse
     *
     * @param datetime $dateReponse
     */
    public function setDateReponse($dateReponse)
    {
        $this->dateReponse = $dateReponse;
    }

    /**
     * Get dateReponse
     *
     * @return datetime 
     */
    public function getDateReponse()
    {
        return $this->dateReponse;
    }

    /**
     * Set rubriqueEvaluation
     *
     * @param Arto\ProductBundle\Entity\RubriqueEvaluation $rubriqueEvaluation
     */
    public function setRubriqueEvaluation(\Arto\ProductBundle\Entity\RubriqueEvaluation $rubriqueEvaluation)
    {
        $this->rubriqueEvaluation = $rubriqueEvaluation;
    }

    /**
     * Get rubriqueEvaluation
     *
     * @return Arto\ProductBundle\Entity\RubriqueEvaluation 
     */
    public function getRubriqueEvaluation()
    {
        return $this->rubriqueEvaluation;
    }

    /**
     * Set impact
     *
     * @param boolean $impact
     */
    public function setImpact($impact)
    {
        $this->impact = $impact;
    }

    /**
     * Get impact
     *
     * @return boolean 
     */
    public function getImpact()
    {
        return $this->impact;
    }

    /**
     * Set typeEvaluation
     *
     * @param string $typeEvaluation
     */
    public function setTypeEvaluation($typeEvaluation)
    {
        $this->typeEvaluation = $typeEvaluation;
    }

    /**
     * Get typeEvaluation
     *
     * @return string 
     */
    public function getTypeEvaluation()
    {
        return $this->typeEvaluation;
    }
}