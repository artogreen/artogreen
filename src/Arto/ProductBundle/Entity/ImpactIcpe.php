<?php
namespace Arto\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_impact_icpe")
 * @ORM\HasLifecycleCallbacks
 */
class ImpactIcpe
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="answer", type="integer")
     */
    private $answer;
    
    /**
     * @ORM\Column(name="curr_qty", type="string", nullable=true)
     * @Assert\NotBlank()
     */
    private $currQty;
    
    /**
     * @ORM\Column(name="new_qty", type="string", nullable=true)
     * @Assert\NotBlank()
     */
    private $newQty;
    
    /**
     * @ORM\Column(name="supp_qty", type="string", nullable=true)
     * @Assert\NotBlank()
     */
    private $suppQty;
    
    /**
     * @ORM\Column(name="description", type="string", length="10000",nullable=true)
     * @Assert\NotBlank()
     */
    private $description;
    
    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
    
    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;

    /**
     * @var ProcessIcpe
     *
     * @ORM\ManyToOne(targetEntity="ProcessIcpe")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="processicpe_id", referencedColumnName="id")
     * })
     */
    private $processIcpe;
    
    /**
     * @var StockageIcpe
     *
     * @ORM\ManyToOne(targetEntity="StockageIcpe")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="stockageicpe_id", referencedColumnName="id")
     * })
     */
    private $stockageIcpe;


    public function __construct()
    {
        
    }

    public function __toString()
    {
        return $this->getName();
    }

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set answer
     *
     * @param integer $answer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    }

    /**
     * Get answer
     *
     * @return integer 
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set currQty
     *
     * @param string $currQty
     */
    public function setCurrQty($currQty)
    {
        $this->currQty = $currQty;
    }

    /**
     * Get currQty
     *
     * @return string 
     */
    public function getCurrQty()
    {
        return $this->currQty;
    }

    /**
     * Set newQty
     *
     * @param string $newQty
     */
    public function setNewQty($newQty)
    {
        $this->newQty = $newQty;
    }

    /**
     * Get newQty
     *
     * @return string 
     */
    public function getNewQty()
    {
        return $this->newQty;
    }

    /**
     * Set suppQty
     *
     * @param string $suppQty
     */
    public function setSuppQty($suppQty)
    {
        $this->suppQty = $suppQty;
    }

    /**
     * Get suppQty
     *
     * @return string 
     */
    public function getSuppQty()
    {
        return $this->suppQty;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set project
     *
     * @param Arto\ProductBundle\Entity\Project $project
     */
    public function setProject(\Arto\ProductBundle\Entity\Project $project)
    {
        $this->project = $project;
    }

    /**
     * Get project
     *
     * @return Arto\ProductBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set processIcpe
     *
     * @param Arto\ProductBundle\Entity\ProcessIcpe $processIcpe
     */
    public function setProcessIcpe(\Arto\ProductBundle\Entity\ProcessIcpe $processIcpe)
    {
        $this->processIcpe = $processIcpe;
    }

    /**
     * Get processIcpe
     *
     * @return Arto\ProductBundle\Entity\ProcessIcpe 
     */
    public function getProcessIcpe()
    {
        return $this->processIcpe;
    }

    /**
     * Set stockageIcpe
     *
     * @param Arto\ProductBundle\Entity\StockageIcpe $stockageIcpe
     */
    public function setStockageIcpe(\Arto\ProductBundle\Entity\StockageIcpe $stockageIcpe)
    {
        $this->stockageIcpe = $stockageIcpe;
    }

    /**
     * Get stockageIcpe
     *
     * @return Arto\ProductBundle\Entity\StockageIcpe 
     */
    public function getStockageIcpe()
    {
        return $this->stockageIcpe;
    }

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}