<?php
namespace Arto\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_activite_flux")
 * @ORM\HasLifecycleCallbacks
 */
class ActiviteFlux
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="atelier_name", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     */
    private $atelier_name;
    
    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
    
     /**
     * @var Family
     *
     * @ORM\ManyToOne(targetEntity="Family")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="family_id", referencedColumnName="id")
     * })
     */
    private $family;
    
    /**
     * @ORM\Column(name="num_rubriqueIcpe", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     */
    private $numRubriqueIcpe;
    
    /**
     * @ORM\Column(name="designation_rubriqueIcpe", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     */
    private $designationRubriqueIcpe;
    
     /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;

    
    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getName();
    }

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set atelier_name
     *
     * @param string $atelierName
     */
    public function setAtelierName($atelierName)
    {
        $this->atelier_name = $atelierName;
    }

    /**
     * Get atelier_name
     *
     * @return string 
     */
    public function getAtelierName()
    {
        return $this->atelier_name;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set numRubriqueIcpe
     *
     * @param string $numRubriqueIcpe
     */
    public function setNumRubriqueIcpe($numRubriqueIcpe)
    {
        $this->numRubriqueIcpe = $numRubriqueIcpe;
    }

    /**
     * Get numRubriqueIcpe
     *
     * @return string 
     */
    public function getNumRubriqueIcpe()
    {
        return $this->numRubriqueIcpe;
    }

    /**
     * Set designationRubriqueIcpe
     *
     * @param string $designationRubriqueIcpe
     */
    public function setDesignationRubriqueIcpe($designationRubriqueIcpe)
    {
        $this->designationRubriqueIcpe = $designationRubriqueIcpe;
    }

    /**
     * Get designationRubriqueIcpe
     *
     * @return string 
     */
    public function getDesignationRubriqueIcpe()
    {
        return $this->designationRubriqueIcpe;
    }

    /**
     * Set family
     *
     * @param Arto\ProductBundle\Entity\Family $family
     */
    public function setFamily(\Arto\ProductBundle\Entity\Family $family)
    {
        $this->family = $family;
    }

    /**
     * Get family
     *
     * @return Arto\ProductBundle\Entity\Family 
     */
    public function getFamily()
    {
        return $this->family;
    }

    /**
     * Set project
     *
     * @param Arto\ProductBundle\Entity\Project $project
     */
    public function setProject(\Arto\ProductBundle\Entity\Project $project)
    {
        $this->project = $project;
    }

    /**
     * Get project
     *
     * @return Arto\ProductBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }
}