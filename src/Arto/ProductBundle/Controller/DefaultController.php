<?php

namespace Arto\ProductBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Arto\GreenBundle\Controller\TokenAuthenticatedController;
use Arto\ProductBundle\Entity\Project;
use Arto\ProductBundle\Entity\Process;
use Arto\ProductBundle\Entity\Family;
use Arto\ProductBundle\Entity\Part;
use Arto\ProductBundle\Entity\ImpactIcpe;
use Arto\ProductBundle\Entity\StockageIcpe;
use Arto\ProductBundle\Entity\Rubrique;
use Arto\ProductBundle\Entity\Regime;
use Arto\ProductBundle\Entity\Conformite;
use Arto\ProductBundle\Entity\Activite;
use Arto\ProductBundle\Entity\EvaluationIcpe;
use Arto\ProductBundle\Entity\RubriqueEvaluation;
use Arto\ProductBundle\Entity\SituationAdministrative;
use Arto\ProductBundle\Entity\SituationReelle;
use Arto\ProductBundle\Entity\User;

use Xlab\pChartBundle\pData;
use Xlab\pChartBundle\pDraw;
use Xlab\pChartBundle\pRadar;
use Xlab\pChartBundle\pImage;

class DefaultController extends Controller implements TokenAuthenticatedController
{
    private function dateToSQL($date) {
        $returnDate = null;

        $s=substr($date, 6, 4)
            ."-".substr($date, 3, 2)
            ."-".substr($date, 0, 2);

        try {
           $returnDate = new \DateTime($s);
        } catch (\Exception $e) {
            // Nothing to do
        }

        return $returnDate;
    }

    public function generalAction()
    {
        return $this->render('ArtoProductBundle:Default:general.html.twig');
    }

    public function termsAction()
    {
        return $this->render('ArtoProductBundle:Default:terms.html.twig');
    }
    
    public function methodeAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $project = $em->getRepository('ArtoProductBundle:Project')->find($id);
        
        return $this->render('ArtoProductBundle:Default:methode.html.twig', array('project' => $project));
    }
    
    public function syntheseAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $project = $em->getRepository('ArtoProductBundle:Project')->find($id);
        
        return $this->render('ArtoProductBundle:Default:synthese.html.twig', array('project' => $project));
    }
    
    public function syntheseSaveAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        $project_id = $request->get('project');
        $project = $em->getRepository('ArtoProductBundle:Project')->find($project_id);
        
        $commentaires = $request->get('commentaires');
        $project->setCommentaires($commentaires);
        
        $em->persist($project);
        $em->flush();
        
        return $this->redirect($this->generateUrl('product_synthese', array(
            'id' => $project->getId())
        ));
    }

    public function indexAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $userId = $user->getId();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        $folderLetters = array();

        $projects = $em->getRepository('ArtoProductBundle:Project')->findAllActive($user);
        
        //projectsWithoutFolder
        $projectsWF = $em->getRepository('ArtoProductBundle:Project')->findBy(
                array('greenUser' => $userId, 'folder' => null), array('name' => 'ASC'));
        
        $folders = $em->getRepository('ArtoProductBundle:Folder')->findBy(
                array('user' => $userId), array('name' => 'ASC'));
        
        foreach($folders as $folder){
            $folderName = $folder->getName();
            $firstLetter = strtoupper(substr($folderName,0,1));
            
            if(!in_array($firstLetter, $folderLetters)){
                array_push($folderLetters, $firstLetter);
            }
        }


        return $this->render('ArtoProductBundle:Default:index.html.twig', array('projects' => $projects, 'user' => $userId, 'folders' => $folders,'projectsWF'=> $projectsWF, 'myLetter' => '', 'folderLetters' => $folderLetters));
    }

    public function createAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();

        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        $dossierId = $request->get('dossier');

        $project = new Project();
        $project->setGreenUser($user);
        $project->setName($request->get('name'));
        $project->setNumero(0);
        $project->setReference(0);
        $project->setProductName('');
        $project->setAcceptable(true);
        $project->setImpactant(false);
        
        if($dossierId == ''){
            $project->setFolder(null);
            $em->persist($project);
        }else{
            $dossier = $em->getRepository('ArtoProductBundle:Folder')->find($dossierId);

            $dossier->addProject($project); 
            $project->setFolder($dossier);

            $em->persist($dossier);
            $em->persist($project);
        }

        $em->persist($project);
        $em->flush();

        return $this->redirect($this->generateUrl('product_project_description', array('id' => $project->getId())));
    }
    
    public function renameAction(){
        $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        
        $id = $request->get('id');
        $newName = $request->get('name');

        $project = $em->getRepository('ArtoProductBundle:Project')->find($id);
        
        $project->setName($newName);
        
        $em->persist($project);
        $em->flush();
               
        return $this->redirect($this->generateUrl('product'));  
    }
            

    public function deleteAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();

        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        $id = $request->get('id');
        $special = $request->get('special');

        $project = $em->getRepository('ArtoProductBundle:Project')->find($id);

        $isCorrectUser = $project->getGreenUser()->getId() == $user->getId();
        $isSpecial = isset($special) && $special != null;

        if ($isCorrectUser || $isSpecial) {
            $em->remove($project);
            $em->flush();
        }
        return $this->redirect($this->generateUrl('product'));
    }

    public function descriptionAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        $id = $request->get('id');

        $project = $em->getRepository('ArtoProductBundle:Project')->find($id);

        $projectName = $project->getName();
        $users = $em->getRepository('ArtoProductBundle:User')->findBy(array('project' => $project->getId()));
        
        $operations = $em->getRepository('ArtoProductBundle:Operation')->findBy(
                array('project' => $project->getId()));

        if($projectName !="Suivi ICPE"){
            return $this->render('ArtoProductBundle:Default:description.html.twig', array(
                'project' => $project,
                'users' => $users,
                'operations' => $operations
            ));
        }else{
            $rubriques = $em->getRepository('ArtoProductBundle:Rubrique')->findAll();
            $regimes = $em->getRepository('ArtoProductBundle:Regime')->findAll();
            $conformites = $em->getRepository('ArtoProductBundle:Conformite')->findAll();
           
            $projectSuiviIcpe = $em->getRepository('ArtoProductBundle:Project')->findOneBy(
               array('name' => 'Suivi ICPE', 'greenUser' => $project->getGreenUser()->getId()));
            $activites = $em->getRepository('ArtoProductBundle:Activite')->findBy(
               array('project' => $projectSuiviIcpe->getId()));

            return $this->render('ArtoProductBundle:Default:tableauSuivi.html.twig', array(
                'project' => $project,
                'activites' => $activites,
                'rubriques' => $rubriques,
                'regimes' => $regimes,
                'conformites' => $conformites
            ));
        }
    }

    public function descriptionSaveAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        $project_id = $request->get('project');
        $project = $em->getRepository('ArtoProductBundle:Project')->find($project_id);

        $project->setProjectName($request->get('projectName'));
        $project->setPilote($request->get('pilote'));
        $project->setNumero($request->get('numero'));
        $project->setProductName($request->get('productName'));

        if ($request->get('pilote_date') != null) {
            $project->setPiloteDate($this->dateToSQL($request->get('pilote_date')));
        }

        $uploadedFile = $_FILES['file'];
        if (isset($uploadedFile['name'])) {
            if($uploadedFile['name'] != ''){
                move_uploaded_file($uploadedFile['tmp_name'], __DIR__.'/../../../../web/uploads/product/'.$uploadedFile['name']);
                $project->setPath($uploadedFile['name']);
            }
        }

        $em->persist($project);
        $em->flush();

        return $this->redirect($this->generateUrl('product_project_description', array(
            'id' => $project->getId())
        ));
    }

    public function userSaveAction($id)
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();
        $project = $em->getRepository('ArtoProductBundle:Project')->find($id);
        $users = $request->get('user');
        
        foreach($users as $user){
            // We check if the name of the part is not empty
               if (isset($user['lastname']) && $user['lastname'] != null) {

                   // Try Retreiving the object if the request id exist
                   if (isset($user['id']) && $user['id'] != null) {
                       $u = $em->getRepository('ArtoProductBundle:User')->find($user['id']);
                   } else {
                       $u = new User();
                   }
                   
                   $u->setProject($project);
                   $u->setRole($user['role']);
                   $u->setCompany($user['company']);
                   $u->setFirstname($user['firstname']);
                   $u->setLastname($user['lastname']);
                   $u->setEmail($user['email']);
                   $u->setTel($user['tel']);
                   
                   $em->persist($u);
                   
             }          
        }
        
        // We flush the all the modifications at once
        $em->flush();
        
        return $this->redirect($this->generateUrl('product_project_description', array('id' => $id)));
    }
    
    public function userDeleteAction(){
        $em = $this->getDoctrine()->getEntityManager();

        $userName =$this->getRequest()->get('nomUser');
        $user = $em->getRepository('ArtoProductBundle:User')->findOneBy(
                array('lastname' => $userName));

        $projectId = $user->getProject()->getId();
        $project = $em->getRepository('ArtoProductBundle:Project')->find($projectId);

        $em->remove($user);
        $em->flush();

        return $this->redirect($this->generateUrl('product_project_description', array('id' => $project->getId())));
    }


    public function scenarioAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoProductBundle:Project')->find($id);
        $parts = $em->getRepository('ArtoProductBundle:Part')->findBy(array('project' => $project->getId()));
        $processes = $em->getRepository('ArtoProductBundle:Process')->findBy(array(), array('name' => "ASC"));
        $families = $em->getRepository('ArtoProductBundle:Family')->findBy(array(), array('name' => "ASC"));
        
        $project->setAcceptable(true);
        $result = 'Acceptable';
        foreach($parts as $part) {
            if ($part->getFamily()->getRank2() == 'Non Acceptable') {
                $result = 'Non Acceptable';
                $project->setAcceptable(false);
                break;
            }
        }
        
        $em->persist($project);
        $em->flush();
        

        return $this->render('ArtoProductBundle:Default:scenario.html.twig', array(
            'project' => $project,
            'processes' => $processes,
            'families' => $families,
            'parts' => $parts,
            'result' => $result
        ));
    }

    public function familiesAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $process = $this->getRequest()->get('process');
        $familyId = $this->getRequest()->get('familyId');

        $families = $em->getRepository('ArtoProductBundle:Family')->findBy(
                array('process' => $process),
                array('name' => "ASC"));

        return $this->render('ArtoProductBundle:Default:families.html.twig', array(
            'families' => $families,
            'familyId' => $familyId
        ));
    }

    public function familyDetailsAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $id = $this->getRequest()->get('id');

        $family = $em->getRepository('ArtoProductBundle:Family')->find($id);

        $result = array(
            'impact' => nl2br($family->getImpact()),
            'usageterms' => nl2br($family->getUsageTerms()),
            'rank1' => '<span align="center" class="'.$family->getRank1Style().'">'.$family->getRank1().'</span>',
            'rank2' => '<span align="center" class="'.$family->getRank2Style().'">'.$family->getRank2().'</span>',
            'familyId'=> $family->getId(),
            'familyImpact'=> $family->getTotalImpact(),
            'familyName'=> $family->getName(),
            'familyProcessName'=>$family->getProcess()->getName()
        );

        $json = json_encode($result);
        $response = new Response($json);
        return $response;
    }

    public function impactsICPEAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoProductBundle:Project')->find($id);
        $processesICPE = $em->getRepository('ArtoProductBundle:ProcessIcpe')->findAll();
        $stockagesICPE = $em->getRepository('ArtoProductBundle:StockageIcpe')->findAll();
        $nbProcess = count($processesICPE);
        $nbStockages = count($stockagesICPE);
        $evaluationICPE = $em->getRepository('ArtoProductBundle:EvaluationIcpe')->findOneBy(
                array('project' => $project->getId()));

        if($evaluationICPE == null){
            $evaluationICPE = new EvaluationIcpe();
        }

        $impactsICPE = $em->getRepository('ArtoProductBundle:ImpactIcpe')->findBy(
                array('project' => $project->getId()));

        $nbImpactsICPE = count($impactsICPE);
        $trouve = 0;

        $rubriques = $em->getRepository('ArtoProductBundle:Rubrique')->findAll();

         return $this->render('ArtoProductBundle:Default:impactsICPE.html.twig', array(
            'project' => $project,
            'processes' => $processesICPE,
            'nbProcess' => $nbProcess,
            'stockages' => $stockagesICPE,
            'nbStockages' => $nbStockages,
            'impacts' => $impactsICPE,
            'nbImpacts' => $nbImpactsICPE,
            'trouve' => $trouve,
            'rubriques' => $rubriques,
            'evaluationIcpe' => $evaluationICPE
        ));

    }


    public function partsSaveAction($project)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoProductBundle:Project')->find($project);

        // First we update the existing lines
        $parts = $this->getRequest()->get('part');

        foreach($parts as $part) {
            // We check if the name of the part is not empty
            if (isset($part['name']) && $part['name'] != null) {

                // Try Retreiving the object if the request id exist
                if (isset($part['id']) && $part['id'] != null) {
                    $p = $em->getRepository('ArtoProductBundle:Part')->find($part['id']);
                } else {
                    $p = new Part();
                }

                $p->setProject($project);

                // Retreive family to set the part family FK
                $family = $em->getRepository('ArtoProductBundle:Family')->find($part['family']);
                $p->setFamily($family);

                $p->setName($part['name']);
                $p->setSitename($part['site']);
                $p->setCountry($part['country']);

                $em->persist($p);
            }
        }

        // We flus the all the modifications at once
        $em->flush();

        return $this->redirect($this->generateUrl('product_scenario', array('id' => $project->getId())));
    }

    public function impactsICPESaveAction($project)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoProductBundle:Project')->find($project);
        $processes = $em->getRepository('ArtoProductBundle:ProcessIcpe')->findAll();
        $stockages = $em->getRepository('ArtoProductBundle:StockageIcpe')->findall();


        $typeOperation = $this->getRequest()->get('typeOperation');
        //Refonte
        if($typeOperation == 1){
            $impactsProcess = $this->getRequest()->get('impactsProcessRefonte');
            $impactsStockage = $this->getRequest()->get('impactsStockageRefonte');
            $eval = $this->getRequest()->get('evalRefonte');

            //PROCESS
            foreach($processes as $key => $process){
                // Recuperation des données entrées
                $answerProcess= $impactsProcess['answer_Process'.$process->getId()];
                if($process->getId() != 4){
                    $currQtyProcess= $impactsProcess['curr_qty_Process'.$process->getId()];
                    $newQtyProcess= $impactsProcess['new_qty_Process'.$process->getId()];
                    $suppQtyProcess= null;
                }else{
                    $descriptionProcess = $impactsProcess['description_Process'.$process->getId()];
                }

                if(isset($impactsProcess['id_Process'.$process->getId()]) && $impactsProcess['id_Process'.$process->getId()]!=null){
                    $impactICPE = $em->getRepository('ArtoProductBundle:ImpactIcpe')->find($impactsProcess['id_Process'.$process->getId()]);
                }
                else{
                    $impactICPE = new ImpactIcpe();
                }

                // Creation d'un nouvel ImpactIcpe qu'on set avec les données
                if($process->getId() == 4){
                    $impactICPE->setDescription($descriptionProcess);
                    $impactICPE->setProject($project);
                    $impactICPE->setProcessIcpe($process);
                    $impactICPE->setCurrQty(0);
                    $impactICPE->setNewQty(0);
                    $impactICPE->setSuppQty(0);
                }else{
                    $impactICPE->setProject($project);
                    $impactICPE->setProcessIcpe($process);
                    $impactICPE->setCurrQty($currQtyProcess);
                    $impactICPE->setNewQty($newQtyProcess);
                    $impactICPE->setSuppQty($suppQtyProcess);
                }
                
                echo("answer Process ".$process->getId()." : ".$answerProcess."\n");
                if($answerProcess=="yes"){
                   $impactICPE->setAnswer(1);
                } else{
                   $impactICPE->setAnswer(0);
                }
                $em->persist($impactICPE);
          }

          //STOCKAGE
          foreach($stockages as $key => $stockage){
                // Recuperation des données entrées
                $answerStockage= $impactsStockage['answer_Stockage'.$stockage->getId()];
                $currQtyStockage= $impactsStockage['curr_qty_Stockage'.$stockage->getId()];
                $newQtyStockage= $impactsStockage['new_qty_Stockage'.$stockage->getId()];
                $suppQtyStockage= null;


                if(isset($impactsStockage['id_Stockage'.$stockage->getId()]) && $impactsStockage['id_Stockage'.$stockage->getId()]!=null){
                    $impactICPE = $em->getRepository('ArtoProductBundle:ImpactIcpe')->find($impactsStockage['id_Stockage'.$stockage->getId()]);
                }
                else{
                    $impactICPE = new ImpactIcpe();
                }

                // Creation d'un nouvel ImpactIcpe qu'on set avec les données
                //$impactICPE = new ImpactIcpe();
                $impactICPE->setDescription('');
                $impactICPE->setProject($project);
                $impactICPE->setStockageIcpe($stockage);
                $impactICPE->setCurrQty($currQtyStockage);
                $impactICPE->setNewQty($newQtyStockage);
                $impactICPE->setSuppQty($suppQtyStockage);
                
                if($answerStockage=="yes"){
                   $impactICPE->setAnswer(1);
                } else{
                   $impactICPE->setAnswer(0);
                }
                $em->persist($impactICPE);
            }

            //EVALUATION
            $nomCorrespondant = $eval['nomCorrespondant'];
            $dateReponse = $this->dateToSQL($eval['dateReponse']);
            $impact = $eval['impactYesNo'];
            $commentaire = $eval['commentaire'];


            if(isset($eval['idEval']) && $eval['idEval']!=null){
                $evaluationICPE = $em->getRepository('ArtoProductBundle:EvaluationIcpe')->find($eval['idEval']);
            }
            else{
                $evaluationICPE = new EvaluationIcpe();
            }

            $evaluationICPE->setProject($project);
            $evaluationICPE->setNomCorrespondant($nomCorrespondant);
            $evaluationICPE->setDateReponse($dateReponse);
            $evaluationICPE->setCommentaire($commentaire);
            $evaluationICPE->setTypeEvaluation('Refonte');

            if($impact=="yes"){
                $evaluationICPE->setImpact(1);
                $project->setImpactant(true);
            } else{
                $evaluationICPE->setImpact(0);
                $project->setImpactant(false);
            }

            $em->persist($evaluationICPE);

        }
        
        //Supplément
        else{
            $impactsProcess = $this->getRequest()->get('impactsProcessSupplement');
            $impactsStockage = $this->getRequest()->get('impactsStockageSupplement');
            $eval = $this->getRequest()->get('evalSupplement');

            //PROCESS
            foreach($processes as $key => $process){

                echo("Process ID : ".$process->getId());
                // Recuperation des données entrées
                $answerProcess= $impactsProcess['answer_Process'.$process->getId()];
                if($process->getId() != 4){
                    $currQtyProcess= null;
                    $newQtyProcess= null;
                    $suppQtyProcess= $impactsProcess['supp_qty_Process'.$process->getId()];;
                }else{
                    $descriptionProcess = $impactsProcess['description_Process'.$process->getId()];
                }
                
               

                if(isset($impactsProcess['id_Process'.$process->getId()]) && $impactsProcess['id_Process'.$process->getId()]!=null){
                    $impactICPE = $em->getRepository('ArtoProductBundle:ImpactIcpe')->find($impactsProcess['id_Process'.$process->getId()]);
                }
                else{
                    $impactICPE = new ImpactIcpe();
                }

                // Creation d'un nouvel ImpactIcpe qu'on set avec les données
                //$impactICPE = new ImpactIcpe();
                if($process->getId() == 4){
                    $impactICPE->setDescription($descriptionProcess);
                    $impactICPE->setProject($project);
                    $impactICPE->setProcessIcpe($process);
                    $impactICPE->setCurrQty(0);
                    $impactICPE->setNewQty(0);
                    $impactICPE->setSuppQty(0);       
                }else{
                    $impactICPE->setProject($project);
                    $impactICPE->setProcessIcpe($process);
                    $impactICPE->setCurrQty($currQtyProcess);
                    $impactICPE->setNewQty($newQtyProcess);
                    $impactICPE->setSuppQty($suppQtyProcess);
                }

                echo("answer Process ".$process->getId()." : ".$answerProcess."\n");
                if($answerProcess=="yes"){
                   $impactICPE->setAnswer(1);
                } else{
                   $impactICPE->setAnswer(0);
                }
                $em->persist($impactICPE);
          }

          //STOCKAGE
          foreach($stockages as $key => $stockage){
                // Recuperation des données entrées
                $answerStockage= $impactsStockage['answer_Stockage'.$stockage->getId()];
                
                if($answerStockage == 'yes'){
                    $suppQtyStockage= $impactsStockage['supp_qty_Stockage'.$stockage->getId()];
                }
                else{
                    $suppQtyStockage = null;
                }
                
                $currQtyStockage= null;
                $newQtyStockage= null;
                


                if(isset($impactsStockage['id_Stockage'.$stockage->getId()]) && $impactsStockage['id_Stockage'.$stockage->getId()]!=null){
                    $impactICPE = $em->getRepository('ArtoProductBundle:ImpactIcpe')->find($impactsStockage['id_Stockage'.$stockage->getId()]);
                }
                else{
                    $impactICPE = new ImpactIcpe();
                }

                // Creation d'un nouvel ImpactIcpe qu'on set avec les données
                //$impactICPE = new ImpactIcpe();
                $impactICPE->setDescription('');
                $impactICPE->setProject($project);
                $impactICPE->setStockageIcpe($stockage);
                $impactICPE->setCurrQty($currQtyStockage);
                $impactICPE->setNewQty($newQtyStockage);
                $impactICPE->setSuppQty($suppQtyStockage);
                
                if($answerStockage == "yes"){
                   $impactICPE->setAnswer(1);
                } else{
                   $impactICPE->setAnswer(0);
                }
                $em->persist($impactICPE);
            }

            //EVALUATION
            $nomCorrespondant = $eval['nomCorrespondant'];
            $dateReponse = $this->dateToSQL($eval['dateReponse']);
            $impact = $eval['impactYesNo'];
            $commentaire = $eval['commentaire'];


            if(isset($eval['idEval']) && $eval['idEval']!=null){
                $evaluationICPE = $em->getRepository('ArtoProductBundle:EvaluationIcpe')->find($eval['idEval']);
            }
            else{
                $evaluationICPE = new EvaluationIcpe();
            }

            $evaluationICPE->setProject($project);
            $evaluationICPE->setNomCorrespondant($nomCorrespondant);
            $evaluationICPE->setDateReponse($dateReponse);
            $evaluationICPE->setCommentaire($commentaire);
            $evaluationICPE->setTypeEvaluation('Supplement');


            if($impact=="yes"){
                $evaluationICPE->setImpact(1);
                $project->setImpactant(true);
            } else{
                $evaluationICPE->setImpact(0);
                $project->setImpactant(false);
            }

            $em->persist($evaluationICPE);

        }
        
        $em->persist($project);

        // We flush the all the modifications at once
        $em->flush();

        return $this->redirect($this->generateUrl('product_impactsICPE', array('id' => $project->getId())));
    }

    public function tableauSuiviAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoProductBundle:Project')->find($id);
        $rubriques = $em->getRepository('ArtoProductBundle:Rubrique')->findAll();
        $regimes = $em->getRepository('ArtoProductBundle:Regime')->findAll();
        $conformites = $em->getRepository('ArtoProductBundle:Conformite')->findAll();
        
        $projectSuiviIcpe = $em->getRepository('ArtoProductBundle:Project')->findOneBy(
               array('name' => 'Suivi ICPE', 'greenUser' => $project->getGreenUser()->getId()));
        $activites = $em->getRepository('ArtoProductBundle:Activite')->findBy(
               array('project' => $projectSuiviIcpe->getId()));
        //$activites = $em->getRepository('ArtoProductBundle:Activite')->findAll();
        
         return $this->render('ArtoProductBundle:Default:tableauSuivi.html.twig', array(
            'project' => $project,
            'activites' => $activites,
            'rubriques' => $rubriques,
            'regimes' => $regimes,
            'conformites' => $conformites
        ));

    }

    public function tableauSuiviSaveAction($project){

         $em = $this->getDoctrine()->getEntityManager();
         echo('Project : '.$project);
         if($project != ''){
            $project = $em->getRepository('ArtoProductBundle:Project')->find($project);
            $user = $project->getGreenUser();

            // First we update the existing lines
            $activites = $this->getRequest()->get('activite');

             foreach($activites as $activite) {
               // We check if the name of the part is not empty
               if (isset($activite['name']) && $activite['name'] != null) {

                   // Try Retreiving the object if the request id exist
                   if (isset($activite['id']) && $activite['id'] != null) {
                       $a = $em->getRepository('ArtoProductBundle:Activite')->find($activite['id']);
                   } else {
                       $a = new Activite();
                   }

                   // Retreive rubrique to set the activity rubrique FK
                   $rubrique = $em->getRepository('ArtoProductBundle:Rubrique')->find($activite['rubriqueName']);
                   $conformite = $em->getRepository('ArtoProductBundle:Conformite')->findOneBy(
                           array('name' => $activite['conformite']));

                   $a->setRubrique($rubrique);
                   $a->setConformite($conformite);
                   $a->setName($activite['name']);

                   $volumeSituationAdmin = $activite['situationAdminVolume'];
                   $volumeSituationReelle = $activite['situationReelleVolume'];

                   $situationAdmin = $em->getRepository('ArtoProductBundle:SituationAdministrative')->findOneBy(
                           array('volumeActivite' => $volumeSituationAdmin, 'rubrique' => $rubrique->getId()));

                   $a->setSituationAdministrative($situationAdmin);

                   $situationReelle = $em->getRepository('ArtoProductBundle:SituationReelle')->findOneBy(
                           array('volumeActivite' => $volumeSituationReelle, 'rubrique' => $rubrique->getId()));

                   $a->setSituationReelle($situationReelle);

                   $projectSuiviIcpe = $em->getRepository('ArtoProductBundle:Project')->findOneBy(
                           array('name' => 'Suivi ICPE', 'greenUser' => $project->getGreenUser()->getId()));

                   $a->setProject($projectSuiviIcpe);

                   $em->persist($a);
               }
           }

           // We flush the all the modifications at once
           $em->flush();

             return $this->redirect($this->generateUrl('product_tableauSuivi', array('id' => $project->getId())));
         }else{
             return $this->redirect($this->generateUrl('product'));
         }  
    }

    public function rubriqueDetailsAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $id = $this->getRequest()->get('id');

        $rubrique = $em->getRepository('ArtoProductBundle:Rubrique')->find($id);

        $result = array(
            'activites' => $rubrique->getActivites(),
            'seuilD' => $rubrique->getSeuilDeclaration(),
            'seuilDC' => $rubrique->getSeuilDeclarationControle(),
            'seuilE' => $rubrique->getSeuilEnregistrement(),
            'seuilA' => $rubrique->getSeuilAutorisation(),
            'seuilAS' => $rubrique->getSeuilAutorisationServitudes(),
            'unit' => $rubrique->getUnit()
        );

        $json = json_encode($result);
        $response = new Response($json);
        return $response;
    }

    public function calculateSeuilAdminAction(){
        $em = $this->getDoctrine()->getEntityManager();

        $id = $this->getRequest()->get('id');

        $rubrique = $em->getRepository('ArtoProductBundle:Rubrique')->find($id);
        $seuil = '';
        
        $seuilD = $rubrique->getSeuilDeclaration();
        $seuilDC = $rubrique->getSeuilDeclarationControle();
        $seuilE = $rubrique->getSeuilEnregistrement();
        $seuilA = $rubrique->getSeuilAutorisation();
        $seuilAS = $rubrique->getSeuilAutorisationServitudes();
        
        $seuils = array();
        $noms = array();
        
        if($seuilD !=0){
           array_push($seuils, $seuilD);
           array_push($noms, 'D');
        }
        
        if($seuilDC !=0){
           array_push($seuils, $seuilDC); 
           array_push($noms, 'DC');
        }
        
        if($seuilE !=0){
           array_push($seuils, $seuilE);
           array_push($noms, 'E');
        }
        
        if($seuilA !=0){
           array_push($seuils, $seuilA); 
           array_push($noms, 'A');
        }
        
        if($seuilAS !=0){
           array_push($seuils, $seuilAS);
           array_push($noms, 'AS');
        }

        $volume = $this->getRequest()->get('volume');
        
        if(count($seuils) == 1){
            if($volume <= $seuils[0]){
                $seuil = "Non classe";
            }
            else{
                $seuil = $noms[0];
            }
        }elseif(count($seuils) == 2){
            if($volume <= $seuils[0]){
                $seuil = "Non classe";
            }elseif($volume > $seuils[0] && $volume < $seuils[1]){
                $seuil = $noms[0];
            }else{
                $seuil = $noms[1];
            }   
        }else{
            if($volume <= $seuils[0]){
                $seuil = "Non classe";
            }elseif($volume > $seuils[0] && $volume < $seuils[1]){
                $seuil = $noms[0];
            }else{
                if($volume < $seuils[2]){
                    $seuil = $noms[1];
                }else{
                    $seuil = $noms[2];
                }
            }
        }
        
       $regimeSituationAdmin = $em->getRepository('ArtoProductBundle:Regime')->findOneBy(
                    array('name' => $seuil));
            
       $existingSituationAdmin = $em->getRepository('ArtoProductBundle:SituationAdministrative')->findOneBy(
                    array('regime' => $regimeSituationAdmin, 'volumeActivite' => $volume));
                      
       if($existingSituationAdmin == null){
            $situationAdmin = new SituationAdministrative();
            $situationAdmin->setRegime($regimeSituationAdmin);
            $situationAdmin->setVolumeActivite($volume);
            $situationAdmin->setRubrique($rubrique);
            $em->persist($situationAdmin);
            $em->flush();    
       }        
        
       $result = array(
            'seuil' => $seuil
        );
       $json = json_encode($result);
       $response = new Response($json);
       return $response;
    }

    public function calculateSeuilReelAction(){
        $em = $this->getDoctrine()->getEntityManager();

        $id = $this->getRequest()->get('id');

        $rubrique = $em->getRepository('ArtoProductBundle:Rubrique')->find($id);
        $seuil = '';
        
        $seuilD = $rubrique->getSeuilDeclaration();
        $seuilDC = $rubrique->getSeuilDeclarationControle();
        $seuilE = $rubrique->getSeuilEnregistrement();
        $seuilA = $rubrique->getSeuilAutorisation();
        $seuilAS = $rubrique->getSeuilAutorisationServitudes();
        
        $seuils = array();
        $noms = array();
        
        if($seuilD !=0){
           array_push($seuils, $seuilD);
           array_push($noms, 'D');
        }
        
        if($seuilDC !=0){
           array_push($seuils, $seuilDC); 
           array_push($noms, 'DC');
        }
        
        if($seuilE !=0){
           array_push($seuils, $seuilE);
           array_push($noms, 'E');
        }
        
        if($seuilA !=0){
           array_push($seuils, $seuilA); 
           array_push($noms, 'A');
        }
        
        if($seuilAS !=0){
           array_push($seuils, $seuilAS);
           array_push($noms, 'AS');
        }

        $volume = $this->getRequest()->get('volume');
        
         if(count($seuils) == 1){
            if($volume <= $seuils[0]){
                $seuil = "Non classe";
            }
            else{
                $seuil = $noms[0];
            }
        }elseif(count($seuils) == 2){
            if($volume <= $seuils[0]){
                $seuil = "Non classe";
            }elseif($volume > $seuils[0] && $volume < $seuils[1]){
                $seuil = $noms[0];
            }else{
                $seuil = $noms[1];
            }   
        }else{
            if($volume <= $seuils[0]){
                $seuil = "Non classe";
            }elseif($volume > $seuils[0] && $volume < $seuils[1]){
                $seuil = $noms[0];
            }else{
                if($volume < $seuils[2]){
                    $seuil = $noms[1];
                }else{
                    $seuil = $noms[2];
                }
            }
        }

        $regimeSituationReelle = $em->getRepository('ArtoProductBundle:Regime')->findOneBy(
                    array('name' => $seuil));
            
        $existingSituationReelle = $em->getRepository('ArtoProductBundle:SituationReelle')->findOneBy(
                    array('regime' => $regimeSituationReelle, 'volumeActivite' => $volume));
        
        if($existingSituationReelle == null){
                $situationReelle = new SituationReelle();
                $situationReelle->setRegime($regimeSituationReelle);
                $situationReelle->setVolumeActivite($volume);
                $situationReelle->setRubrique($rubrique);
                $em->persist($situationReelle);
                $em->flush();
            }   
        
        $result = array(
            'seuil' => $seuil
        );
        $json = json_encode($result);
        $response = new Response($json);
        return $response;
    }

    public function activiteDeleteAction(){
        $em = $this->getDoctrine()->getEntityManager();

        $activiteName =$this->getRequest()->get('nomActivite');
        $activite = $em->getRepository('ArtoProductBundle:Activite')->findOneBy(
                array('name' => $activiteName));

        $projectId = $activite->getProject()->getId();
        $project = $em->getRepository('ArtoProductBundle:Project')->find($projectId);

        $em->remove($activite);
        $em->flush();

        return $this->redirect($this->generateUrl('product_tableauSuivi', array('id' => $project->getId())));
    }

    public function grapheProcedesAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoProductBundle:Project')->find($id);

        $processes = $em->getRepository('ArtoProductBundle:Process')->findBy(array(), array('name' => "ASC"));
        $families = $em->getRepository('ArtoProductBundle:Family')->findBy(array(), array('name' => "ASC"));

        return $this->render('ArtoProductBundle:Default:graphesEvaluationProcedes.html.twig', array(
            'project'=> $project,
            'processes' => $processes,
            'families' => $families
        ));
    }

    public function partDeleteAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $partId =$this->getRequest()->get('idPart');
        echo($partId);

        $part = $em->getRepository('ArtoProductBundle:Part')->findOneBy(
                array('id' => $partId));

        echo(count($part));

        $projectId = $part->getProject()->getId();
        $project = $em->getRepository('ArtoProductBundle:Project')->find($projectId);

        $em->remove($part);
        $em->flush();

        return $this->redirect($this->generateUrl('product_scenario', array('id' => $project->getId())));
    }

    public function graphesIcpeAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoProductBundle:Project')->find($id);

        $projectId = $project->getId();

        $activites = $em->getRepository('ArtoProductBundle:Activite')->findBy(
                array('project' => $projectId ));


        $graphes = array();

        foreach($activites as $activite){
            $activiteId = $activite->getId();
            $rubrique = $activite->getRubrique();

            $rubriqueSeuilA = $rubrique->getSeuilAutorisation();
            $rubriqueSeuilD = $rubrique->getSeuilDeclaration();

            $rubriqueLibelle = $rubrique->getActivites();
            $rubriqueNomenclature = $rubrique->getName();

            $volumeActivite = $activite->getSituationReelle()->getVolumeActivite();

            $abscissAxisName = $rubriqueLibelle."\n"."              "."R ".$rubriqueNomenclature;

            $abscissAxisMax = "";
            if($rubriqueSeuilA != null)
                {
                    $abscissAxisMax = $rubriqueSeuilA;
                }
            else
                {
                    $abscissAxisMax = $rubriqueSeuilD;
                }

            $myData = new pData();
            $myData->addPoints(array($volumeActivite,$abscissAxisMax),"Serie1");
            //$myData->setSerieDescription("Serie1","Valeur");
            $myData->setSerieOnAxis("Serie1",0);

            $myData->addPoints(array($abscissAxisName),"Abscissa");
            $myData->setPalette("Abscissa",array("R"=>210,"G"=>123,"B"=>'170','Alpha'=>60));
            $myData->setAbscissa("Abscissa");

            $myData->setAxisPosition(0,AXIS_POSITION_LEFT);
            $myData->setAxisName(0,"");
            $myData->setAxisUnit(0,"");

            $myPicture = new pImage(200,400,$myData);
            $myPicture->drawRectangle(0,0,199,399,array("R"=>0,"G"=>0,"B"=>0));

            $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>50,"G"=>50,"B"=>50,"Alpha"=>20));

            $myPicture->setFontProperties(array("FontName"=>"fonts/PTS55F-webfont.ttf","FontSize"=>18));
            $TextSettings = array("Align"=>TEXT_ALIGN_MIDDLEMIDDLE
            , "R"=>255, "G"=>255, "B"=>255);
            $myPicture->drawText(100,25,"",$TextSettings);

            $myPicture->setShadow(FALSE);
            $myPicture->setGraphArea(50,50,175,365);
            $myPicture->setFontProperties(array("R"=>0,"G"=>0,"B"=>0,"FontName"=>"fonts/PTS55F-webfont.ttf","FontSize"=>8));

            $Settings = array("Pos"=>SCALE_POS_LEFTRIGHT
            , "Mode"=>SCALE_MODE_ADDALL_START0
            , "LabelingMethod"=>LABELING_ALL
            , "GridR"=>255, "GridG"=>255, "GridB"=>255, "GridAlpha"=>50, "TickR"=>0, "TickG"=>0, "TickB"=>0, "TickAlpha"=>50, "LabelRotation"=>0, "CycleBackground"=>1, "DrawXLines"=>1, "DrawSubTicks"=>1, "SubTickR"=>255, "SubTickG"=>0, "SubTickB"=>0, "SubTickAlpha"=>50, "DrawYLines"=>ALL);
            $myPicture->drawScale($Settings);

            $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>50,"G"=>50,"B"=>50,"Alpha"=>10));

            $Config = array("DisplayValues"=>1, "AroundZero"=>1);
            $myPicture->drawBarChart($Config);

            $Config = array("FontR"=>0, "FontG"=>0, "FontB"=>0, "FontName"=>"fonts/PTS55F-webfont.ttf", "FontSize"=>6, "Margin"=>6, "Alpha"=>30, "BoxSize"=>5, "Style"=>LEGEND_NOBORDER
            , "Mode"=>LEGEND_HORIZONTAL
            );
            $myPicture->drawLegend(145,16,$Config);

            $path = $this->get('kernel')->getRootDir() . '/../web';

            $myPictureName = $path . "/images/product/graph".$activiteId.".png";
            $myPicture->render($myPictureName);

            $graphes[$activiteId] = "/images/product/graph".$activiteId.".png";

        }

       return $this->render('ArtoProductBundle:Default:graphesIcpe.html.twig', array(
            'project'=> $project,
            'graphes'=> $graphes
        ));
    }

    public function inventaireFluxAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoProductBundle:Project')->find($id);
        $parts = $em->getRepository('ArtoProductBundle:Part')->findBy(array('project' => $project->getId()));
        $processes = $em->getRepository('ArtoProductBundle:Process')->findBy(array(), array('name' => "ASC"));
        $families = $em->getRepository('ArtoProductBundle:Family')->findBy(array(), array('name' => "ASC"));

         return $this->render('ArtoProductBundle:Default:inventaireFlux.html.twig', array(
            'project' => $project,
            'processes' => $processes,
            'families' => $families,
            'parts' => $parts
        ));
    }

    public function aspectsEnvtFluxAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $partName =$this->getRequest()->get('nomPart');
        echo($partName);

        $part = $em->getRepository('ArtoProductBundle:Part')->findOneBy(
                array('name' => $partName));

        echo(count($part));

        $projectId = $part->getProject()->getId();
        $project = $em->getRepository('ArtoProductBundle:Project')->find($projectId);

        $em->remove($part);
        $em->flush();

        return $this->redirect($this->generateUrl('product_scenario', array('id' => $project->getId())));
    }

}

