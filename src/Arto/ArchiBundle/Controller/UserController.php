<?php

namespace Arto\ArchiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Arto\ArchiBundle\Entity\User;

use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{

    public function saveAction($id)
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();
        $project = $em->getRepository('ArtoArchiBundle:Project')->find($id);
        $users = $request->get('user');
        
        foreach($users as $user){
            // We check if the name of the part is not empty
               if (isset($user['lastname']) && $user['lastname'] != null) {

                   // Try Retreiving the object if the request id exist
                   if (isset($user['id']) && $user['id'] != null) {
                       $u = $em->getRepository('ArtoArchiBundle:User')->find($user['id']);
                   } else {
                       $u = new User();
                   }
                   
                   $u->setProject($project);
                   $u->setCompany($user['company']);
                   $u->setFirstname($user['firstname']);
                   $u->setLastname($user['lastname']);
                   
                   $em->persist($u);
                   
             }          
        }
        
        // We flush the all the modifications at once
        $em->flush();
        
        return $this->redirect($this->generateUrl('archi_project_description', array('id' => $id)));
    }
    
     public function deleteAction(){
        $em = $this->getDoctrine()->getEntityManager();

        $userId =$this->getRequest()->get('userId');
        $user = $em->getRepository('ArtoArchiBundle:User')->find($userId);

        $projectId = $user->getProject()->getId();
        $project = $em->getRepository('ArtoArchiBundle:Project')->find($projectId);

        $em->remove($user);
        $em->flush();

        return $this->redirect($this->generateUrl('archi_project_description', array('id' => $project->getId())));
    }

}
