<?php

namespace Arto\ArchiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Arto\GreenBundle\Controller\TokenAuthenticatedController;
use Arto\ArchiBundle\Entity\Note;
use Arto\ArchiBundle\Entity\Project;
use Arto\ArchiBundle\Entity\Folder;
use Arto\ArchiBundle\Entity\HelpNote;
use Arto\ArchiBundle\Entity\Action;
use Arto\ArchiBundle\Entity\Criteria;
use Xlab\pChartBundle\pData;
use Xlab\pChartBundle\pDraw;
use Xlab\pChartBundle\pRadar;
use Xlab\pChartBundle\pImage;
use Arto\ArchiBundle\Services\RequestHandler;
use Arto\ArchiBundle\Services\Calculator;
use Arto\ArchiBundle\Services\Drawer;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller implements TokenAuthenticatedController
{
    private function dateToSQL($date) {
        $returnDate = null;

        $s=substr($date, 6, 4)
            ."-".substr($date, 3, 2)
            ."-".substr($date, 0, 2);

        try {
           $returnDate = new \DateTime($s);
        } catch (\Exception $e) {
            // Nothing to do
        }

        return $returnDate;
    }
    
    private function getRequestHandler()
    {
        return $this->get('arto.urba.requestHandler');
    }
    
    private function getCalculator()
    {
        return $this->get('arto.urba.calculator');
    }
    
    private function getDrawer()
    {
        return $this->get('arto.urba.drawer');
    }

    public function generalAction()
    {
        return $this->render('ArtoArchiBundle:Default:general.html.twig');
    }
    
    public function instructionsAction($id){
        $requestHandler = $this->getRequestHandler();
        $project = $requestHandler->getProject($id);

        return $this->render('ArtoArchiBundle:Default:instructions.html.twig', array('project' => $project));
    }

    public function termsAction()
    {
        return $this->render('ArtoArchiBundle:Default:terms.html.twig');
    }

    public function indexAction()
    {
        $requestHandler = $this->getRequestHandler();
        $user = $this->get('security.context')->getToken()->getUser();
        $userId = $user->getId();
        
        $folderLetters = array();

        $projects = $requestHandler->findProjects($user);
        $projectsWF = $requestHandler->findProjectsWF($userId);
        
        $folders = $requestHandler->findFolders($userId);
        
        foreach ($folders as $folder) {
            $folderName = $folder->getName();
            $firstLetter = strtoupper(substr($folderName, 0, 1));

            if (!in_array($firstLetter, $folderLetters)) {
                array_push($folderLetters, $firstLetter);
            }
        }

        return $this->render('ArtoArchiBundle:Default:index.html.twig', array(
            'projects' => $projects,
            'projectsWF' => $projectsWF,
            'folders' => $folders,
            'myLetter' => '',
            'folderLetters' => $folderLetters
        ));
    }

    public function homeAction()
    {
        $requestHandler = $this->getRequestHandler();
        $request = $this->getRequest();
        
        $id = $request->get('id');
        $project = $requestHandler->getProject($id);

        return $this->render('ArtoArchiBundle:Default:home.html.twig', array('project' => $project));
    }

    public function createAction()
    {
        $requestHandler = $this->getRequestHandler();
        $user = $this->get('security.context')->getToken()->getUser();

        $request = $this->getRequest();
        $city = $request->get('city');
        $name = $request->get('name');
        $dossierId = $request->get('dossier');
        
        $project = $requestHandler->createProject($name, $user, $city, $dossierId);

        return $this->redirect($this->generateUrl('archi_project_home', array('id' => $project->getId())));
    }

    public function deleteAction()
    {
        $requestHandler = $this->getRequestHandler();
        $user = $this->get('security.context')->getToken()->getUser();

        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        $id = $request->get('id');
        $special = $request->get('special');

        $requestHandler->deleteProject($id, $user, $special);
        
        return $this->redirect($this->generateUrl('archi'));
    }
    
    public function renameAction(){
        $requestHandler = $this->getRequestHandler();
        $request = $this->getRequest();
        
        $id = $request->get('id');
        $newName = $request->get('name');

        $requestHandler->renameProject($id, $newName);
               
        return $this->redirect($this->generateUrl('archi'));  
    }

    public function showAction($id)
    {
        $requestHandler = $this->getRequestHandler();
        $calculator = $this->getCalculator();
        
        $project = $requestHandler->getProject($id);
        $projectDefault = $project->getDefault();
        $criterias = $requestHandler->getCriterias();
        $actions = array();

        foreach($project->getHelpNotes() as $helpnote) {
            if ($helpnote->getAction() != null) {
                if (!isset($actions[$helpnote->getHelp()->getCriteria()->getId()])) {
                    $actions[$helpnote->getHelp()->getCriteria()->getId()] = array();
                }
                $actions[$helpnote->getHelp()->getCriteria()->getId()][] = $helpnote->getAction();
            }
        }

        $qeb = $calculator->calculateQeb($project);
        $qeb_default = $calculator->calculateQeb($projectDefault);
        
        $pilote_actions = $requestHandler->findAllActions($project);

        return $this->render('ArtoArchiBundle:Default:show.html.twig', array(
            'project' => $project,
            'project_default' => $project->getDefault(),
            'criterias' => $criterias,
            'qeb' => round($qeb),
            'qeb_default' => round($qeb_default),
            'actions' => $actions,
            'pilote_actions' => $pilote_actions
        ));
    }

    public function descriptionAction($id = null)
    {
        $requestHandler = $this->getRequestHandler();
        $request = $this->getRequest();

        $defaut = $request->get('default');

        $project = $requestHandler->getProject($id);
        $users = $requestHandler->getUsersByProject($project);
        $operations = $requestHandler->getOperationsByProject($project);

        $project_default = $project->getDefault();

        if ($defaut == null) {
            $tpl = 'ArtoArchiBundle:Default:description.html.twig';
        } else {
            $tpl = 'ArtoArchiBundle:Default:description_default.html.twig';
        }
        return $this->render($tpl, array(
            'project' => $project,
            'project_default' => $project_default,
            'users' => $users,
            'operations' => $operations
        ));
    }

    public function descriptionSaveAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        
        $requestHandler = $this->getRequestHandler();
        $request = $this->getRequest();
        $project = $request->get('project');
        $project_base = $em->getRepository('ArtoArchiBundle:Project')->find($project);

        $default = $request->get('default');
        $surface = $request->get('surface');
        $surface = str_replace(',','.',$surface);
        $projectName = $request->get('projectName');
        $address1 = $request->get('address1');
        $city = $request->get('city');
        $population = $request->get('population');
        $housing = $request->get('housing');
        $other = $request->get('other');
        $pilote = $request->get('pilote');
        $piloteDate = $request->get('pilote_date');
        $uploadedFile = $_FILES['file'];

        $requestHandler->saveDescription($project, $default, $surface, $projectName, $address1, $city, $population, $housing, $other, $pilote, $piloteDate, $uploadedFile);

        return $this->redirect($this->generateUrl('archi_project_description', array(
            'id' => $project_base->getId(),
            'default' => $default
        )));
    }

    public function categoryAction($id, $category)
    {
        $requestHandler = $this->getRequestHandler();
        $project = $requestHandler->getProject($id);
        $category = $requestHandler->getCategory($category);

        $notes = $requestHandler->getArrayNotesByCriterias($project);
        $notes_default = $requestHandler->getArrayNotesDefaultByCriterias($project);

        $help_notes = $requestHandler->getArrayHelpNotes($project);
        $help_actions = $requestHandler->getArrayHelpActions($project);
        $help_comments = $requestHandler->getArrayHelpComments($project);
        $help_comments_default = $requestHandler->getArrayHelpCommentsDefault($project);
        $help_notes_default = $requestHandler->getArrayHelpNotesDefault($project);
        
        $proofs = $requestHandler->checkPreuves($project);
      
        return $this->render('ArtoArchiBundle:Default:category.html.twig', array(
            'project' => $project,
            'category' => $category,
            'notes' => $notes,
            'notes_default' => $notes_default,
            'help_notes' => $help_notes,
            'help_actions' => $help_actions,
            'help_comments' => $help_comments,
            'help_comments_default' => $help_comments_default,
            'help_default_notes' => $help_notes_default,
            //'help_default_actions' => $help_default_actions,
            //'help_default_comments' => $help_default_comments
            'proofs' => $proofs
                
        ));
    }

    public function categorySaveAction($id, $category)
    {
        $requestHandler = $this->getRequestHandler();
        $em = $this->getDoctrine()->getEntityManager();
        $project = $requestHandler->getProject($id);
        $category = $em->getRepository('ArtoArchiBundle:Category')->find($category);

        $request = $this->getRequest();

        $requestHandler->saveCategory($project, $category, $request);
        
        return $this->redirect($this->generateUrl('archi_project_category', array(
            'id' => $project->getId(),
            'category' => $category->getId()
        )));
    }

    public function actionSaveAction($id, $project)
    {
        $requestHandler = $this->getRequestHandler();
        
        $request = $this->getRequest();
        $defaut = $request->get('default');
        $action = $request->get('action');

        $requestHandler->saveAction($project, $id, $action);
        
        exit();
    }

    public function commentSaveAction($id, $project)
    {
        $requestHandler = $this->getRequestHandler();
        $request = $this->getRequest();
        
        $defaut = $request->get('default');
        $comment = $request->get('comment');
        $commentDefault = $request->get('comment_default');

        $requestHandler->saveComment($project, $id, $comment, $commentDefault);
        exit();
    }

    public function chartAction($id)
    {
        $drawer = $this->getDrawer();
        $response = $drawer->drawChart1($id);
        
        return $response;
    }

    public function summuaryCategoryAction($id)
    {
        $requestHandler = $this->getRequestHandler();
        $calculator = $this->getCalculator();
        $em = $this->getDoctrine()->getEntityManager();

        $categories = $requestHandler->getCategorys();
        $criterias = $requestHandler->getCriterias();

        /* Calculate Main Project */
        $project1 = $requestHandler->getProject($id);
        $result_percentage = $calculator->calculateCategoriesProfile($project1, $categories, $criterias);


        /* Calculate Default Project */
        $project2 = $project1->getDefault();
        $result_default = $calculator->calculateCategoriesProfile($project2, $categories, $criterias);

        $result = array();
        if (count($result_percentage) == 4 && count($result_default) == 4) {
            $result['Environnement'] = ($result_percentage[1] >= $result_default[1]) ? 'green' : 'red';
            $result['Social'] = ($result_percentage[2] >= $result_default[2]) ? 'green' : 'red';
            $result['Economique'] = ($result_percentage[3] >= $result_default[3]) ? 'green' : 'red';
            $result['Transversal'] = ($result_percentage[4] >= $result_default[4]) ? 'green' : 'red';
        }

        return $this->render('ArtoArchiBundle:Default:summuary_category.html.twig', array(
            'result' => $result
        ));
    }

    public function chart2Action($id)
    {
        $drawer = $this->getDrawer();
        $response = $drawer->drawChart2($id);
        
        return $response;
    }

    public function summuaryProfileAction($id)
    {
        $requestHandler = $this->getRequestHandler();
        $calculator = $this->getCalculator();
        
        $categories = $requestHandler->getCategorys();
        $weights = $requestHandler->getWeights();
        $calculs = $requestHandler->getCalculs();

        $project1 = $requestHandler->getProject($id);
        $notes = $requestHandler->getNotes($project1);
        $result_percentage = $calculator->calculateWeightProfile($project1, $categories, $weights, $weights, $calculs, $notes);

        $project2 = $project1->getDefault();
        $notes = $requestHandler->getNotes($project2);
        $result_default = $calculator->calculateWeightProfile($project2, $categories, $weights, $weights, $calculs, $notes);

        $result = array();
        if (count($result_percentage) == 5 && count($result_default) == 5) {
            $result['Sain et confortable'] = ($result_percentage[1] >= $result_default[1]) ? 'green' : 'red';
            $result['Intégré'] = ($result_percentage[2] >= $result_default[2]) ? 'green' : 'red';
            $result['Sobre'] = ($result_percentage[3] >= $result_default[3]) ? 'green' : 'red';
            $result['Durable'] = ($result_percentage[4] >= $result_default[4]) ? 'green' : 'red';
            $result['Economique'] = ($result_percentage[4] >= $result_default[4]) ? 'green' : 'red';
        }

        return $this->render('ArtoArchiBundle:Default:summuary_profile.html.twig', array(
            'result' => $result
        ));
    }

    public function searchShowAction($id){
        $requestHandler = $this->getRequestHandler();
        
        $project = $requestHandler->getProject($id);
        return $this->render('ArtoArchiBundle:Default:search.html.twig', array(
            'project' => $project
        ));
    }

    public function searchAction($id){
        $requestHandler = $this->getRequestHandler();
        $request = $this->getRequest();

        $project = $requestHandler->getProject($id);

        $searchKey = $request->get('key');
        $results = $requestHandler->searchCriteria($searchKey);
        
        return $this->render('ArtoArchiBundle:Default:_searchResult.html.twig',array(
            'results' => $results,
            'project' => $project
        ));
    }
    
    public function planActionSaveAction($id){
        $requestHandler = $this->getRequestHandler();
        $request = $this->getRequest();

        $project = $requestHandler->getProject($id);
        
        $requestHandler->savePlanAction($project, $request);
        
        return $this->showAction($project->getId());
    }
    
    public function checkNotesAction(){
        $requestHandler = $this->getRequestHandler();
        $request = $this->getRequest(); 
        $em = $this->getDoctrine()->getEntityManager();
        
        $projectId = $request->get('project');
        $project = $requestHandler->getProject($projectId);
        
        $notesEmpty = true;
        $notes = $em->getRepository('ArtoArchiBundle:Note')->findBy(
                array('project' => $projectId));
        
        if($notes != null){
            $notesEmpty = false;
        }
        
        $result = array(
            'notesEmpty' => $notesEmpty,
            'notes' => $notes
        );
        
        $json = json_encode($result);
        return new Response($json);
        
    }
    
    public function duplicateAction($id){
        $em = $this->getDoctrine()->getEntityManager();
        
        $project = $em->getRepository('ArtoArchiBundle:Project')->find($id);
        $projectDefault = $project->getDefault();
        $user = $this->get('security.context')->getToken()->getUser();
        
        $projectDefault_new = $this->duplicateDefaultProject($projectDefault);
        $project_new = clone $project;
        
        $project_new->setGreenUser($user);
        $project_new->setName($project->getName());
        $project_new->setProjectName($project->getProjectName());
        //$project_new->setEnergy($project->getEnergy());
        $project_new->setBase(1);
        $project_new->setDefault($projectDefault_new);
        
         //Un dossier ?
        $folder = $project->getFolder();
        //Si il y a un dossier
        if($folder != null){
            //On retrouve le nom du dossier
            $folderName = $folder->getName();
            
            //Recherche du dossier
            $folderCloned = $em->getRepository('ArtoArchiBundle:Folder')->findOneBy(
                    array('name' => $folderName, 'user' => $user->getId()));
            
            //Attribution du dossier au projet cloné
            $project_new->setFolder($folderCloned);
        }

        $em->persist($project_new);  
        
        foreach($project->getNotes() as $note){
            $note_new = clone $note;
            $note_new->setProject($project_new);
            $em->persist($note_new);
            //$project_new->addNote($note_new);
        }
        
        foreach($projectDefault->getNotes() as $noteDefault){
            $noteDefault_new = clone $noteDefault;
            $noteDefault_new->setProject($projectDefault_new);
            $em->persist($noteDefault_new);
            //$project_new->addNote($note_new);
        }
        
        foreach($project->getHelpNotes() as $helpNote){
            $helpNote_new = clone $helpNote;
            $helpNote_new->setProject($project_new);
            $em->persist($helpNote_new);
        }
        
        foreach($projectDefault->getHelpNotes() as $helpNoteDefault){
            $helpNoteDefault_new = clone $helpNoteDefault;
            $helpNoteDefault_new->setProject($projectDefault_new);
            $em->persist($helpNoteDefault_new);   
        }
        
        foreach($project->getActions() as $action){
            $action_new = clone $action;
            $action_new->setProject($project_new);
            $em->persist($action_new);
            //$project_new->addAction($action_new);
        }
        
        foreach($projectDefault->getActions() as $actionDefault){
            $actionDefault_new = clone $actionDefault;
            $actionDefault_new->setProject($projectDefault_new);
            $em->persist($actionDefault_new);
            //$project_new->addAction($action_new);
        }
        
        foreach($project->getOperations() as $operation){
            $operation_new = clone $operation;
            $operation_new->setProject($project_new);
            $em->persist($operation_new);
        }
        
        foreach($project->getUsers() as $projectUser){
            $user_new = clone $projectUser;
            $user_new->setProject($project_new);
            $em->persist($user_new);
        }
        
        $em->flush();
        
        return $this->redirect($this->generateUrl('archi'));
    }
    
    public function duplicateDefaultProject($project){
        $em = $this->getDoctrine()->getEntityManager();
        $user = $this->get('security.context')->getToken()->getUser();
        
        //Gathering projectDefault datas
        $projectDefaultName = $project->getName();
        $projectDefaultPilote = $project->getPilote();
        $projectDefaultPiloteDate = $project->getPiloteDate();
        $projectDefaultPath = $project->getPath();
        $projectDefaultActive = $project->getActive();
        $projectDefaultCreatedAt = $project->getCreatedAt();
        $projectDefaultUpdatedAt = $project->getUpdatedAt();
        
        //Creating new ProjectDefault
        $projectDefault_new = new \Arto\ArchiBundle\Entity\Project();

        $projectDefaultProjectName = $project->getProjectName();
        $projectDefaultAddress1 = $project->getAddress1();
        $projectDefaultAddress2 = $project->getAddress2();
        $projectDefaultCity = $project->getCity();
        $projectDefaultSurface = $project->getSurface();
        $projectDefaultPopulation = $project->getPopulation();
        $projectDefaultHousing = $project->getHousing();
        $projectDefaultOther = $project->getOther();

        //Setting other datas as equal as original ProjectDefault
        $projectDefault_new->setProjectName($projectDefaultProjectName);
        $projectDefault_new->setAddress1($projectDefaultAddress1);
        $projectDefault_new->setAddress2($projectDefaultAddress2);
        $projectDefault_new->setCity($projectDefaultCity);
        $projectDefault_new->setSurface($projectDefaultSurface);
        $projectDefault_new->setPopulation($projectDefaultPopulation);
        $projectDefault_new->setHousing($projectDefaultHousing);
        $projectDefault_new->setOther($projectDefaultOther);

        //Changing some datas for clone
        $projectDefault_new->setGreenUser($user);
        $projectDefault_new->setName($projectDefaultName);
        //$projectDefault_new->setEnergy($projectDefault->getEnergy());
        $projectDefault_new->setBase(0);
        
        //Setting other datas as equal as original ProjectDefault
        $projectDefault_new->setPilote($projectDefaultPilote);
        $projectDefault_new->setPiloteDate($projectDefaultPiloteDate);
        $projectDefault_new->setPath($projectDefaultPath);
        $projectDefault_new->setActive($projectDefaultActive);
        $projectDefault_new->setCreatedAt($projectDefaultCreatedAt);
        $projectDefault_new->setUpdatedAt($projectDefaultUpdatedAt);
        
        $em->persist($projectDefault_new);
         
        return $projectDefault_new;
    }
}
