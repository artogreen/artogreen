<?php
namespace Arto\ArchiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="archi_criteria")
 */
class Criteria
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="label", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     */
    private $label;

    /**
     * @var integer $weight
     *
     * @ORM\Column(name="weight", type="integer", nullable=false)
     */
    private $weight;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="criterias")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="Help", mappedBy="criteria", cascade={"remove"})
     */
    private $helps;

    /**
     * @ORM\OneToMany(targetEntity="Note", mappedBy="criteria", cascade={"remove"})
     */
    private $notes;

    /**
     * @ORM\OneToMany(targetEntity="Action", mappedBy="criteria", cascade={"remove"})
     */
    private $actions;

    /**
     * @ORM\OneToMany(targetEntity="Calcul", mappedBy="criteria", cascade={"remove"})
     */
    private $calculs;

    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getLabel();
    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set label
     *
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set category
     *
     * @param Arto\ArchiBundle\Entity\Category $category
     */
    public function setCategory(\Arto\ArchiBundle\Entity\Category $category)
    {
        $this->category = $category;
    }

    /**
     * Get category
     *
     * @return Arto\ArchiBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Add helps
     *
     * @param Arto\ArchiBundle\Entity\Help $helps
     */
    public function addHelp(\Arto\ArchiBundle\Entity\Help $helps)
    {
        $this->helps[] = $helps;
    }

    /**
     * Get helps
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getHelps()
    {
        return $this->helps;
    }

    /**
     * Add notes
     *
     * @param Arto\ArchiBundle\Entity\Note $notes
     */
    public function addNote(\Arto\ArchiBundle\Entity\Note $notes)
    {
        $this->notes[] = $notes;
    }

    /**
     * Get notes
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Add actions
     *
     * @param Arto\ArchiBundle\Entity\Action $actions
     */
    public function addAction(\Arto\ArchiBundle\Entity\Action $actions)
    {
        $this->actions[] = $actions;
    }

    /**
     * Get actions
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * Add calculs
     *
     * @param Arto\ArchiBundle\Entity\Calcul $calculs
     */
    public function addCalcul(\Arto\ArchiBundle\Entity\Calcul $calculs)
    {
        $this->calculs[] = $calculs;
    }

    /**
     * Get calculs
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getCalculs()
    {
        return $this->calculs;
    }
}