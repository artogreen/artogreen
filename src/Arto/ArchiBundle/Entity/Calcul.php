<?php
namespace Arto\ArchiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Arto\ArchiBundle\Repository\CalculRepository")
 * @ORM\Table(name="archi_calcul")
 */
class Calcul
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $value;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Criteria
     *
     * @ORM\ManyToOne(targetEntity="Criteria", inversedBy="notes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="criteria_id", referencedColumnName="id")
     * })
     */
    private $criteria;

    /**
     * @var Weight
     *
     * @ORM\ManyToOne(targetEntity="Weight", inversedBy="notes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="weight_id", referencedColumnName="id")
     * })
     */
    private $weight;

    public function __construct()
    {
        $this->value = false;
    }

    public function __toString()
    {
        return $this->getNote();
    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set value
     *
     * @param boolean $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Get value
     *
     * @return boolean
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set criteria
     *
     * @param Arto\ArchiBundle\Entity\Criteria $criteria
     */
    public function setCriteria(\Arto\ArchiBundle\Entity\Criteria $criteria)
    {
        $this->criteria = $criteria;
    }

    /**
     * Get criteria
     *
     * @return Arto\ArchiBundle\Entity\Criteria
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * Set weight
     *
     * @param Arto\ArchiBundle\Entity\Weight $weight
     */
    public function setWeight(\Arto\ArchiBundle\Entity\Weight $weight)
    {
        $this->weight = $weight;
    }

    /**
     * Get weight
     *
     * @return Arto\ArchiBundle\Entity\Weight
     */
    public function getWeight()
    {
        return $this->weight;
    }
}