<?php
namespace Arto\ArchiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Arto\ArchiBundle\Repository\ProjectRepository")
 * @ORM\Table(name="archi_action")
 */
class Action
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="pilote", type="string", length=255, nullable=true)
     */
    private $pilote;

    /**
     * @ORM\Column(name="pilote_date", type="date", nullable=true)
     */
    private $piloteDate;

    /**
     * @var Criteria
     *
     * @ORM\ManyToOne(targetEntity="Criteria", inversedBy="notes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="criteria_id", referencedColumnName="id")
     * })
     */
    private $criteria;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="actions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;


    public function __construct()
    {
    }

    public function __toString()
    {
        return $this->getPilote();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pilote
     *
     * @param string $pilote
     */
    public function setPilote($pilote)
    {
        $this->pilote = $pilote;
    }

    /**
     * Get pilote
     *
     * @return string
     */
    public function getPilote()
    {
        return $this->pilote;
    }

    /**
     * Set piloteDate
     *
     * @param date $piloteDate
     */
    public function setPiloteDate($piloteDate)
    {
        $this->piloteDate = $piloteDate;
    }

    /**
     * Get piloteDate
     *
     * @return date
     */
    public function getPiloteDate()
    {
        return $this->piloteDate;
    }

    /**
     * Set criteria
     *
     * @param Arto\ArchiBundle\Entity\Criteria $criteria
     */
    public function setCriteria(\Arto\ArchiBundle\Entity\Criteria $criteria)
    {
        $this->criteria = $criteria;
    }

    /**
     * Get criteria
     *
     * @return Arto\DesignBundle\Entity\Criteria
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * Set project
     *
     * @param Arto\ArchiBundle\Entity\Project $project
     */
    public function setProject(\Arto\ArchiBundle\Entity\Project $project)
    {
        $this->project = $project;
    }

    /**
     * Get project
     *
     * @return Arto\ArchiBundle\Entity\Project
     */
    public function getProject()
    {
        return $this->project;
    }
}