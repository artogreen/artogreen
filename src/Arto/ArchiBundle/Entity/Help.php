<?php
namespace Arto\ArchiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="archi_help")
 */
class Help
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="description", type="text", nullable=false)
     * @Assert\NotBlank()
     */
    private $label;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Criteria
     *
     * @ORM\ManyToOne(targetEntity="Criteria", inversedBy="helps")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="criteria_id", referencedColumnName="id")
     * })
     */
    private $criteria;

    /**
     * @ORM\OneToMany(targetEntity="HelpNote", mappedBy="help", cascade={"remove"})
     */
    private $helpNotes;

    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getTitle();
    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set label
     *
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getTitle()
    {
        return substr(strip_tags($this->label), 0, 200);
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set category
     *
     * @param Arto\ArchiBundle\Entity\Category $category
     */
    public function setCategory(\Arto\ArchiBundle\Entity\Category $category)
    {
        $this->category = $category;
    }

    /**
     * Get category
     *
     * @return Arto\ArchiBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set criteria
     *
     * @param Arto\ArchiBundle\Entity\Criteria $criteria
     */
    public function setCriteria(\Arto\ArchiBundle\Entity\Criteria $criteria)
    {
        $this->criteria = $criteria;
    }

    /**
     * Get criteria
     *
     * @return Arto\ArchiBundle\Entity\Criteria
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * Add helpNotes
     *
     * @param Arto\ArchiBundle\Entity\HelpNote $helpNotes
     */
    public function addHelpNote(\Arto\ArchiBundle\Entity\HelpNote $helpNotes)
    {
        $this->helpNotes[] = $helpNotes;
    }

    /**
     * Get helpNotes
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getHelpNotes()
    {
        return $this->helpNotes;
    }
}