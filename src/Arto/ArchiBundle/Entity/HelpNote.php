<?php
namespace Arto\ArchiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="archi_help_note")
 */
class HelpNote
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="value", type="decimal", nullable=true, scale=1)
     */
    private $value;

    /**
     * @ORM\Column(name="action", type="text", nullable=true)
     */
    private $action;

    /**
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(name="comment_default", type="text", nullable=true)
     */
    private $commentDefault;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="notes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;

    /**
     * @var Help
     *
     * @ORM\ManyToOne(targetEntity="Help", inversedBy="helpNotes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="help_id", referencedColumnName="id")
     * })
     */
    private $help;

    public function __construct()
    {
    }

    public function __toString()
    {
        return $this->getValue();
    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set value
     *
     * @param boolean $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Get value
     *
     * @return boolean
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set project
     *
     * @param Arto\ArchiBundle\Entity\Project $project
     */
    public function setProject(\Arto\ArchiBundle\Entity\Project $project)
    {
        $this->project = $project;
    }

    /**
     * Get project
     *
     * @return Arto\ArchiBundle\Entity\Project
     */
    public function getProject()
    {
        return $this->project;
    }


    /**
     * Set help
     *
     * @param Arto\ArchiBundle\Entity\Help $help
     */
    public function setHelp(\Arto\ArchiBundle\Entity\Help $help)
    {
        $this->help = $help;
    }

    /**
     * Get help
     *
     * @return Arto\ArchiBundle\Entity\Help
     */
    public function getHelp()
    {
        return $this->help;
    }

    /**
     * Set action
     *
     * @param text $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * Get action
     *
     * @return text
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set comment
     *
     * @param text $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * Get comment
     *
     * @return text
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set commentDefault
     *
     * @param text $commentDefault
     */
    public function setCommentDefault($commentDefault)
    {
        $this->commentDefault = $commentDefault;
    }

    /**
     * Get commentDefault
     *
     * @return text
     */
    public function getCommentDefault()
    {
        return $this->commentDefault;
    }
}