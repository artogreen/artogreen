<?php
namespace Arto\ArchiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Arto\ArchiBundle\Repository\ProjectRepository")
 * @ORM\Table(name="archi_project")
 * @ORM\HasLifecycleCallbacks
 */
class Project
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="uniq", type="string", length=32, nullable=false)
     */
    private $uniq;

    /**
     * @ORM\Column(name="name", type="string", length=32, nullable=false)
     * @Assert\NotBlank()
     */
    private $name;
    
    /**
     * @ORM\Column(name="projectName", type="string", length=64, nullable = true)
     */
    private $projectName;

    /**
     * @ORM\Column(name="pilote", type="string", length=255, nullable=true)
     */
    private $pilote;

    /**
     * @ORM\Column(name="pilote_date", type="date", nullable=true)
     */
    private $piloteDate;

    /**
     * @ORM\Column(name="address1", type="string", length=255, nullable=true)
     */
    private $address1;

    /**
     * @ORM\Column(name="address2", type="string", length=255, nullable=true)
     */
    private $address2;

    /**
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(name="surface", type="string", length=255, nullable=true)
     */
    private $surface;

    /**
     * @ORM\Column(name="population", type="string", length=255, nullable=true)
     */
    private $population;

    /**
     * @ORM\Column(name="housing", type="string", length=255, nullable=true)
     */
    private $housing;

    /**
     * @ORM\Column(name="other", type="string", length=255, nullable=true)
     */
    private $other;

    /**
     * @var string $path
     *
     * @ORM\Column(name="path", type="string", length=100, nullable=true)
     */
    private $path;

    /**
     * @Assert\File(maxSize="10000000")
     */
    public $file;

    /**
     * to get the image path (slug + extension)
     */
    private $image;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(type="boolean")
     */
    private $base;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
    
    /**
     * @var Folder
     *
     * @ORM\ManyToOne(targetEntity="Folder", inversedBy="projects")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="folder_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $folder;

    /**
     * @ORM\OneToMany(targetEntity="Note", mappedBy="project", cascade={"remove"})
     */
    private $notes;

    /**
     * @ORM\OneToMany(targetEntity="HelpNote", mappedBy="project", cascade={"remove"})
     */
    private $helpNotes;

    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="project", cascade={"remove"})
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity="Operation", mappedBy="project", cascade={"remove"})
     */
    private $operations;
    
    /**
     * @ORM\OneToMany(targetEntity="Action", mappedBy="project", cascade={"remove"})
     */
    private $actions;

    /**
     * @ORM\OneToOne(targetEntity="Project")
     * @ORM\JoinColumn(name="default_id", referencedColumnName="id")
     */
    private $default;

    /**
     * @var GreenUser
     *
     * @ORM\ManyToOne(targetEntity="Arto\GreenBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="green_user_id", referencedColumnName="id")
     * })
     */
    private $greenUser;
    
    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="childs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * })
     */
    private $parent;
    
    /**
     * @ORM\OneToMany(targetEntity="Project", mappedBy="parent", cascade={"remove", "persist"})
     */
    private $childs;


    public function __construct()
    {
        $this->uniq = md5( uniqid('artoarchi', true) );
        $this->processed = false;
        $this->active = true;
    }

    public function __toString()
    {
        return $this->getName();
    }



    /**
     * Methods for upload
     */

    public function getAbsolutePath()
    {
        return null === $this->image ? null : $this->getUploadRootDir().'/'.$this->image;
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/archi/project';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            $this->path = $this->file->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        $this->file->move($this->getUploadRootDir(), $this->getImage());

        unset($this->file);
    }
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uniq
     *
     * @param string $uniq
     */
    public function setUniq($uniq)
    {
        $this->uniq = $uniq;
    }

    /**
     * Get uniq
     *
     * @return string 
     */
    public function getUniq()
    {
        return $this->uniq;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set projectName
     *
     * @param string $projectName
     */
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;
    }

    /**
     * Get projectName
     *
     * @return string 
     */
    public function getProjectName()
    {
        return $this->projectName;
    }


    /**
     * Set pilote
     *
     * @param string $pilote
     */
    public function setPilote($pilote)
    {
        $this->pilote = $pilote;
    }

    /**
     * Get pilote
     *
     * @return string 
     */
    public function getPilote()
    {
        return $this->pilote;
    }

    /**
     * Set piloteDate
     *
     * @param date $piloteDate
     */
    public function setPiloteDate($piloteDate)
    {
        $this->piloteDate = $piloteDate;
    }

    /**
     * Get piloteDate
     *
     * @return date 
     */
    public function getPiloteDate()
    {
        return $this->piloteDate;
    }

    /**
     * Set address1
     *
     * @param string $address1
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;
    }

    /**
     * Get address1
     *
     * @return string 
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set address2
     *
     * @param string $address2
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;
    }

    /**
     * Get address2
     *
     * @return string 
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set city
     *
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set surface
     *
     * @param string $surface
     */
    public function setSurface($surface)
    {
        $this->surface = $surface;
    }

    /**
     * Get surface
     *
     * @return string 
     */
    public function getSurface()
    {
        return $this->surface;
    }

    /**
     * Set population
     *
     * @param string $population
     */
    public function setPopulation($population)
    {
        $this->population = $population;
    }

    /**
     * Get population
     *
     * @return string 
     */
    public function getPopulation()
    {
        return $this->population;
    }

    /**
     * Set housing
     *
     * @param string $housing
     */
    public function setHousing($housing)
    {
        $this->housing = $housing;
    }

    /**
     * Get housing
     *
     * @return string 
     */
    public function getHousing()
    {
        return $this->housing;
    }

    /**
     * Set other
     *
     * @param string $other
     */
    public function setOther($other)
    {
        $this->other = $other;
    }

    /**
     * Get other
     *
     * @return string 
     */
    public function getOther()
    {
        return $this->other;
    }

    /**
     * Set path
     *
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set active
     *
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set base
     *
     * @param boolean $base
     */
    public function setBase($base)
    {
        $this->base = $base;
    }

    /**
     * Get base
     *
     * @return boolean 
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add notes
     *
     * @param Arto\ArchiBundle\Entity\Note $notes
     */
    public function addNote(\Arto\ArchiBundle\Entity\Note $notes)
    {
        $this->notes[] = $notes;
    }

    /**
     * Get notes
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Add helpNotes
     *
     * @param Arto\ArchiBundle\Entity\HelpNote $helpNotes
     */
    public function addHelpNote(\Arto\ArchiBundle\Entity\HelpNote $helpNotes)
    {
        $this->helpNotes[] = $helpNotes;
    }

    /**
     * Get helpNotes
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getHelpNotes()
    {
        return $this->helpNotes;
    }

    /**
     * Add users
     *
     * @param Arto\ArchiBundle\Entity\User $users
     */
    public function addUser(\Arto\ArchiBundle\Entity\User $users)
    {
        $this->users[] = $users;
    }

    /**
     * Get users
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add operations
     *
     * @param Arto\ArchiBundle\Entity\Operation $operations
     */
    public function addOperation(\Arto\ArchiBundle\Entity\Operation $operations)
    {
        $this->operations[] = $operations;
    }

    /**
     * Get operations
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getOperations()
    {
        return $this->operations;
    }

    /**
     * Add actions
     *
     * @param Arto\ArchiBundle\Entity\Action $actions
     */
    public function addAction(\Arto\ArchiBundle\Entity\Action $actions)
    {
        $this->actions[] = $actions;
    }

    /**
     * Get actions
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * Set default
     *
     * @param Arto\ArchiBundle\Entity\Project $default
     */
    public function setDefault(\Arto\ArchiBundle\Entity\Project $default)
    {
        $this->default = $default;
    }

    /**
     * Get default
     *
     * @return Arto\ArchiBundle\Entity\Project 
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * Set greenUser
     *
     * @param Arto\GreenBundle\Entity\User $greenUser
     */
    public function setGreenUser(\Arto\GreenBundle\Entity\User $greenUser)
    {
        $this->greenUser = $greenUser;
    }

    /**
     * Get greenUser
     *
     * @return Arto\GreenBundle\Entity\User 
     */
    public function getGreenUser()
    {
        return $this->greenUser;
    }
    
    /**
     * Set folder
     *
     * @param Arto\ArchiBundle\Entity\Folder $folder
     */
    public function setFolder(\Arto\ArchiBundle\Entity\Folder $folder = null)
    {
        $this->folder = $folder;
    }

    /**
     * Get folder
     *
     * @return Arto\ArchiBundle\Entity\Folder 
     */
    public function getFolder()
    {
        return $this->folder;
    }
    
     /**
     * Set parent
     *
     * @param Arto\ArchiBundle\Entity\Project $parent
     */
    public function setParent(\Arto\ArchiBundle\Entity\Project $parent)
    {
        $this->parent = $parent;
    }

    /**
     * Get parent
     *
     * @return Arto\ArchiBundle\Entity\Project
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add childs
     *
     * @param Arto\ArchiBundle\Entity\Project $childs
     */
    public function addProject(\Arto\ArchiBundle\Entity\Project $childs)
    {
        $this->childs[] = $childs;
    }

    /**
     * Get childs
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getChilds()
    {
        return $this->childs;
    }
}