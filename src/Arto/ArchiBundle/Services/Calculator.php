<?php

namespace Arto\ArchiBundle\Services;

use Arto\ArchiBundle\Entity\Project;
use Arto\ArchiBundle\Entity\Folder;
use Arto\ArchiBundle\Entity\Note;
use Arto\ArchiBundle\Services\RequestHandler;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Calculator {
    protected $requestHandler;
    
    public function __construct(RequestHandler $requestHandler) {
        $this->requestHandler = $requestHandler;
    }
    
    public function calculateQeb($project){
        $qeb = 0;
        
        foreach($project->getNotes() as $note) {
            $qeb += (($note->getValue()*$note->getCriteria()->getWeight())*($note->getCriteria()->getCategory()->getIndex()/200));
        }
        
        return $qeb;
    }
    
    /**
     *  Calculate the pourcentage for each categories
     *
     * @param type $project
     * @param type $categories
     * @param type $criterias
     * @return type
     */
    public function calculateCategoriesProfile($project, $categories, $criterias) {

        $weight_total = 0;
        foreach($criterias as $criteria) {
            $weight_total += $criteria->getWeight();
        }

        $index = array();
        $coef = array();
        foreach($categories as $category) {
            $cat = $category->getId();
            $index[$cat] = $category->getIndex();
            $coef[$cat] = $index[$cat]/$weight_total;
        }

        $results = array();
        $results_weight = array();
        foreach($project->getNotes() as $note) {
            $cat =  $note->getCriteria()->getCategory()->getId();
            $val = $note->getValue();
            
            $results[$cat][] = $note->getValue() * $note->getCriteria()->getWeight() * $coef[$cat];
            $results_weight[$cat][] = $note->getCriteria()->getWeight() * $coef[$cat];
        }

        $total = array();
        $total_weight = array();
        $result_percentage = array();
        foreach($categories as $category) {
            $cat = $category->getId();
            $total[$cat] = array_sum($results[$cat]);
            $total_weight[$cat] = array_sum($results_weight[$cat]);
            $result_percentage[$cat] = ($total[$cat]/$total_weight[$cat])*100;
        }

        return $result_percentage;
    }
    
     /**
     * Calculate the weight for each categories
     *
     * @param type $project
     * @param type $categories
     * @param type $weights
     * @param type $weights
     * @param type $calculs
     * @param type $notes
     * @return type
     */
    public function calculateWeightProfile($project, $categories, $weights, $weights, $calculs, $notes) {

        $calcul_totals = array();
        $calcul_weights = array();
        foreach($calculs as $calcul) {
            $criteria_id = $calcul->getCriteria()->getId();

            if (!isset($calcul_weights[$criteria_id])) {
                $calcul_weights[$criteria_id] = array();
            }

            $calcul_weights[$criteria_id][$calcul->getWeight()->getId()] = $calcul->getValue();

            if (!isset($calcul_totals[$criteria_id])) {
                $calcul_totals[$criteria_id] = $calcul->getValue();
            } else {
                $calcul_totals[$criteria_id] += $calcul->getValue();
            }
        }

        $notes_array = array();
        foreach($notes as $note) {
            $criteria_id = $note->getCriteria()->getId();
            $notes_array[$criteria_id] = $note->getValue();
        }

        $result = array();
        $results_total = array();
        foreach($categories as $category) {
            $category_id = $category->getId();

            foreach($category->getCriterias() as $criteria) {
                $criteria_id = $criteria->getId();
                if (isset($calcul_totals[$criteria_id])) {

                    foreach($weights as $weight) {
                        $weight_id = $weight->getId();
                        // check if the note is greater than 0
                        if ($notes_array[$criteria_id]) {
                            $result[$category_id][$criteria_id][$weight_id] = (($calcul_weights[$criteria_id][$weight_id] * $criteria->getWeight()) / $calcul_totals[$criteria_id]);
                        } else {
                            $result[$category_id][$criteria_id][$weight_id] = 0;
                        }

                        if (!isset($results_total[$category_id][$weight_id])) {
                                $results_total[$category_id][$weight_id] = $result[$category_id][$criteria_id][$weight_id];
                        } else {
                                $results_total[$category_id][$weight_id] += $result[$category_id][$criteria_id][$weight_id];
                        }
                    }

                }
            }
        }


        $weights_total = array();
        foreach($results_total as $result_total) {
            foreach($weights as $weight) {
                $weight_id = $weight->getId();
                if (!isset($weights_total[$weight_id])) {
                    $weights_total[$weight_id] = $result_total[$weight_id];
                } else {
                    $weights_total[$weight_id] += $result_total[$weight_id];
                }
            }
        }

        $results_final = array();
        $weights_label = array();
        foreach($weights as $weight) {
            $weights_label[] = $weight->getLabel();
            $weight_id = $weight->getId();
            $results_final[$weight_id] = round(($weights_total[$weight_id] / $weight->getIndex()) * 100);
        }

        return $results_final;
    }
}
