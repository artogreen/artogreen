<?php

namespace Arto\ArchiBundle\Services;

use Arto\ArchiBundle\Entity\Project;
use Arto\ArchiBundle\Entity\Folder;
use Arto\ArchiBundle\Entity\Note;
use Arto\ArchiBundle\Services\Calculator;
use Arto\ArchiBundle\Services\RequestHandler;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Xlab\pChartBundle\pData;
use Xlab\pChartBundle\pDraw;
use Xlab\pChartBundle\pRadar;
use Xlab\pChartBundle\pImage;

class Drawer {
    protected $calculator;
    protected $requestHandler;
    
    public function __construct(Calculator $calculator, RequestHandler $requestHandler) {
        $this->calculator = $calculator;
        $this->requestHandler = $requestHandler;
    }
    
    public function drawChart1($projectId){
        $calculator = $this->calculator;
        $requestHandler = $this->requestHandler;
        
        $categories = $requestHandler->getCategorys();
        $criterias = $requestHandler->getCriterias();

        $categories_array = array();
        foreach($categories as $category) {
            $categories_array[] = $category->getLabel();
        }

        /* Calculate Main Project */
        $project1 = $requestHandler->getProject($projectId);
        $result_percentage = $calculator->calculateCategoriesProfile($project1, $categories, $criterias);

        /* Calculate Default Project */
        $project2 = $project1->getDefault();
        $result_default = $calculator->calculateCategoriesProfile($project2, $categories, $criterias);

        $response = new Response();
        $response->headers->set('Content-Type', 'image/png');

        /* Chart */
        /* chart font */
        $font = __DIR__."/../../../Xlab/pChartBundle/Resources/fonts/calibri.ttf";

        /* pChart stuff goes here */
        $data = new pData();
        $data->addPoints($result_percentage, "ScoreA");
        $data->addPoints($result_default,"ScoreB");
        $data->setSerieDescription("ScoreA",$project1->getName());
        $data->setSerieDescription("ScoreB",$project2->getName());
        $data->setPalette("ScoreA",array("R"=>1,"G"=>143,"B"=>189));
        $data->setPalette("ScoreB",array("R"=>255,"G"=>153,"B"=>0));

        /* Define the absissa serie */
        $data->addPoints($categories_array,"Labels");
        $data->setAbscissa("Labels");

        /* Create the pChart object */
        $chart = new pImage(400,350,$data);

        /* Overlay some gradient areas */
        $settings = array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>220, "EndG"=>220, "EndB"=>220, "Alpha"=>50);
        $chart->drawGradientArea(0,0,400,350,DIRECTION_VERTICAL,$settings);
        $chart->drawGradientArea(0,0,400,0,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>100));

        /* Add a border to the picture */
        $chart->drawRectangle(0,0,399,349,array("R"=>204,"G"=>204,"B"=>204));

        /* Set the default font properties */
        $chart->setFontProperties(array(
            "FontName"=>$font,
            "FontSize"=>9,
            "R"=>80,
            "G"=>80,
            "B"=>80
        ));

        /* Enable shadow computing */
        $chart->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));

        /* Create the pRadar object */
        $splitChart = new pRadar();

        /* Draw a radar chart */
        $chart->setGraphArea(50,25,350,300);
        $options = array(
            "Layout"=>RADAR_LAYOUT_STAR,
            "LabelPos"=>RADAR_LABELS_HORIZONTAL,
            "BackgroundGradient"=>array("StartR"=>255,"StartG"=>255,"StartB"=>255,"StartAlpha"=>30,"EndR"=>207,"EndG"=>227,"EndB"=>125,"EndAlpha"=>10),
            "FontName"=>$font,
            "FontSize"=>9,
            "LabelPadding"=>'8px',
            "AxisRotation" => '270'
        );

        $splitChart->drawRadar($chart,$data,$options);

        /* Write the chart legend */
        $chart->setFontProperties(array("FontName"=>$font,"FontSize"=>9));
        $chart->drawLegend(15,310,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_VERTICAL));

        /* Capture output and return the response */
        ob_start();
        $chart->autoOutput();
        $response->setContent(ob_get_clean());

        return $response;
    }
    
    public function drawChart2($projectId)
    {
        $calculator = $this->calculator;
        $requestHandler = $this->requestHandler;
        
        $response = new Response();
        $categories = $requestHandler->getCategorys();
        $weights = $requestHandler->getWeights();
        $calculs = $requestHandler->getCalculs();

        $weights_label = array();
        foreach($weights as $weight) {
            $weights_label[] = $weight->getLabel();
        }

        $project1 = $requestHandler->getProject($projectId);
        $notes = $requestHandler->getNotes($project1);
        $results_final = $calculator->calculateWeightProfile($project1, $categories, $weights, $weights, $calculs, $notes);

        $project2 = $project1->getDefault();
        $notesDefault = $requestHandler->getNotes($project2);
        $result_default = $calculator->calculateWeightProfile($project2, $categories, $weights, $weights, $calculs, $notesDefault);

        /* chart font */
        $font = __DIR__."/../../../Xlab/pChartBundle/Resources/fonts/calibri.ttf";

        /* pChart stuff goes here */
        $data = new pData();
        $data->addPoints($results_final, "ScoreA");
        $data->addPoints($result_default,"ScoreB");
        $data->setSerieDescription("ScoreA",$project1->getName());
        $data->setSerieDescription("ScoreB",$project2->getName());
        $data->setPalette("ScoreA",array("R"=>1,"G"=>143,"B"=>189));
        $data->setPalette("ScoreB",array("R"=>255,"G"=>153,"B"=>0));

        /* Define the absissa serie */
        $data->addPoints($weights_label,"Labels");
        $data->setAbscissa("Labels");

        /* Create the pChart object */
        $chart = new pImage(400,350,$data);

        /* Overlay some gradient areas */
        $settings = array("StartR"=>255, "StartG"=>255, "StartB"=>255, "EndR"=>220, "EndG"=>220, "EndB"=>220, "Alpha"=>50);
        $chart->drawGradientArea(0,0,400,350,DIRECTION_VERTICAL,$settings);
        $chart->drawGradientArea(0,0,400,0,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>100));

        /* Add a border to the picture */
        $chart->drawRectangle(0,0,399,349,array("R"=>204,"G"=>204,"B"=>204));

        /* Set the default font properties */
        $chart->setFontProperties(array(
            "FontName"=>$font,
            "FontSize"=>9,
            "R"=>80,
            "G"=>80,
            "B"=>80
        ));

        /* Enable shadow computing */
        $chart->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>10));

        /* Create the pRadar object */
        $splitChart = new pRadar();

        /* Draw a radar chart */
        $chart->setGraphArea(50,25,350,300);
        $options = array(
            "Layout"=>RADAR_LAYOUT_STAR,
            "LabelPos"=>RADAR_LABELS_HORIZONTAL,
            "BackgroundGradient"=>array("StartR"=>255,"StartG"=>255,"StartB"=>255,"StartAlpha"=>30,"EndR"=>207,"EndG"=>227,"EndB"=>125,"EndAlpha"=>10),
            "FontName"=>$font,
            "FontSize"=>9,
            "LabelPadding"=>'8px',
            "AxisRotation" => '270'
        );

        $splitChart->drawRadar($chart,$data,$options);

        /* Write the chart legend */
        $chart->setFontProperties(array("FontName"=>$font,"FontSize"=>9));
        $chart->drawLegend(15,310,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_VERTICAL));

        /* Capture output and return the response */
        ob_start();
        $chart->autoOutput();
        $response->setContent(ob_get_clean());

        return $response;
 
    }
    
    
}

