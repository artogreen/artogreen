<?php

namespace Arto\ArchiBundle\Services;

use Arto\ArchiBundle\Entity\Project;
use Arto\ArchiBundle\Entity\Folder;
use Arto\ArchiBundle\Entity\Note;
use Arto\ArchiBundle\Entity\HelpNote;
use Arto\ArchiBundle\Entity\Action;
use Arto\ArchiBundle\Services\FileUploader;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RequestHandler {
    
    protected $em;
    protected $fileUploader;
    
    public function __construct(EntityManager $em, FileUploader $fileUploader) {
        $this->em = $em;
        $this->fileUploader = $fileUploader;     
    }
    
    private function dateToSQL($date) {
        $returnDate = null;

        $s = substr($date, 6, 4)
            ."-".substr($date, 3, 2)
            ."-".substr($date, 0, 2);

        try {
           $returnDate = new \DateTime($s);
        } catch (\Exception $e) {
            // Nothing to do
        }

        return $returnDate;
    }
    
    public function getWeights(){
        $em = $this->em;
        
        $weights = $em->getRepository('ArtoArchiBundle:Weight')->findAll();
        
        return $weights;
    }
    
    public function getCalculs(){
        $em = $this->em;
        
        $calculs = $em->getRepository('ArtoArchiBundle:Calcul')->findAll();
        
        return $calculs;
    }

    public function getCriteria($id){
        $em = $this->em;
        
        $criteria = $em->getRepository('ArtoArchiBundle:Criteria')->find($id);
        
        return $criteria;
    }
    
    public function getCriteriaValue($project, $criteria){
        $note = $this->getNote($project, $criteria);
        $noteValue = $note->getValue();
        
        return $noteValue;
    }
     
    public function getCriteriasByCategory($categoryId){
        $em = $this->em;
        
        $criterias = $em->getRepository('ArtoArchiBundle:Criteria')->findBy(
                array('category' => $categoryId));
        
        return $criterias;
    }
    
    public function getCriterias(){
        $em = $this->em;
        
        $criterias = $em->getRepository('ArtoArchiBundle:Criteria')->findAll();
        
        return $criterias;
    }
    
    public function getProject($id){
        $em = $this->em;
        
        $project = $em->getRepository('ArtoArchiBundle:Project')->find($id);
        
        return $project;
    }
    
    public function getCategory($id){
        $em = $this->em;
        
        $category = $em->getRepository('ArtoArchiBundle:Category')->find($id);
        
        return $category;
    }
    
    public function findProjectsWF($userId){
        $em = $this->em;
        
        $sql = "SELECT c FROM ArtoArchiBundle:Project c WHERE c.greenUser=:greenUser AND c.folder IS NULL AND c.default IS NOT NULL ORDER BY c.name";
        $query = $em->createQuery($sql)->setParameters(
                array("greenUser" => $userId));
        
        
        $projectsWF = $query->getResult();
        
        return $projectsWF;
    }
    
    public function findProjects($user){
        $em = $this->em;
        
        $projects = $em->getRepository('ArtoArchiBundle:Project')->findAllActive($user);
        
        return $projects;
    }
    
    public function findFolders($userId){
        $em  = $this->em;
        
        $folders = $em->getRepository('ArtoArchiBundle:Folder')->findBy(
                array('user' => $userId), array('name' => 'ASC'));
        
        return $folders;
    }
    
    public function getCategorys(){
        $em = $this->em;
        
        $categorys = $em->getRepository('ArtoArchiBundle:Category')->findAll();
        
        return $categorys;
    }
    
    public function createNote($project, $criteria){
        $em = $this->em;
        
        $note = new Note();
        $note->setProject($project);
        $note->setCriteria($criteria);
        $note->setValue(false);
        //$note->setSimulatedValue(false);
        
        $em->persist($note);
        $em->flush();
    }
    
    public function createSpecialNote($project, $criteria){
        $em = $this->em;
        
        $note = new Note();
        $note->setProject($project);
        //$note->setSimulatedValue(false);
        $note->setCriteria($criteria);
        $criteriaId = $criteria->getId();
        
        switch($criteriaId){
            case 1:
                $note->setValue(true);
                break;
            case 2:
                $note->setValue(true);
                break;
            case 5:
                $note->setValue(true);
                break;
            case 7:
                $note->setValue(true);
                break;
            case 8:
                $note->setValue(true);
                break;
            case 11:
                $note->setValue(true);
                break;
            case 13:
                $note->setValue(true);
                break;
            case 14:
                $note->setValue(true);
                break;
            case 16:
                $note->setValue(true);
                break;
            case 22:
                $note->setValue(true);
                break;
            case 24:
                $note->setValue(true);
                break;
            case 25:
                $note->setValue(true);
                break;
            case 27:
                $note->setValue(true);
                break;
            case 31:
                $note->setValue(true);
                break;
            case 32:
                $note->setValue(true);
                break;
            case 33:
                $note->setValue(true);
                break;
            case 36:
                $note->setValue(true);
                break;
            case 38:
                $note->setValue(true);
                break;
            case 40:
                $note->setValue(true);
                break;
            case 43:
                $note->setValue(true);
                break;
            case 44:
                $note->setValue(true);
                break;
            case 47:
                $note->setValue(true);
                break;
            case 48:
                $note->setValue(true);
                break;
            default:
                $note->setValue(false);
                break;
        }
        
        $em->persist($note);
        $em->flush();
    }
    
    public function getNote($project, $criteria){
        $em = $this->em;
        
        if(is_int($criteria)){
            $criteriaId = $criteria;
        }else{
            $criteriaId = $criteria->getId();
        }
        
        if(is_int($project)){
           $projectId = $project;
        }else{
           $projectId = $project->getId();
        }
        
        $note = $em->getRepository('ArtoArchiBundle:Note')->findOneBy(
                    array('project' => $projectId, 'criteria' => $criteriaId));
        
        return $note;
    }
    
    public function createProject($name, $user, $city, $dossierId){
        $em = $this->em;
        
        $project_default = new Project();
        $project_default->setGreenUser($user);
        $project_default->setName('Projet de référence');
        $project_default->setBase(0);

        $project = new Project();
        $project->setGreenUser($user);
        $project->setName($name);
        $project->setCity($city);
        $project->setBase(1);
        $project->setDefault($project_default);
        
        if($dossierId == ''){
            $project->setFolder(null);
            $em->persist($project);
        }else{
            $dossier = $em->getRepository('ArtoArchiBundle:Folder')->find($dossierId);

            $dossier->addProject($project); 
            $project->setFolder($dossier);

            $em->persist($dossier);
            $em->persist($project);
        }

        $em->persist($project_default);
        $em->persist($project);
        $em->flush();
        
        $criterias = $this->getCriterias();
        
        foreach($criterias as $criteria){
            $this->createNote($project, $criteria);
            $this->createNote($project_default, $criteria);
        }
        
        return $project;
    }
    
    public function renameProject($projectId, $newName){
        $em = $this->em;
        
        $project = $this->getProject($projectId);
        
        $project->setName($newName);
        
        $em->persist($project);
        $em->flush();
    }
    
    public function deleteProject($projectId, $user, $special){
        $em = $this->em;
        
        $project = $this->getProject($projectId);
        $projectDefault = $project->getDefault();
        
        $isCorrectUser = $project->getGreenUser()->getId() == $user->getId();
        $isSpecial = isset($special) && $special != null;
        
        if ($isCorrectUser ||$isSpecial) {
            $em->remove($project);
           
            if($projectDefault != null){
                $em->remove($projectDefault);
            }
            
            $em->flush();
        }
    }
    
    public function findActions($project, $criteria){
        $em = $this->em;
        
        $criteriaId = $criteria->getId();
        $projectId = $project->getId();
        
        $action = $em->getRepository('ArtoArchiBundle:Action')->findBy(
                    array('project' => $projectId, 'criteria' => $criteriaId));
        
        return $action;
    }
    
    public function findAllActions($project){
        $em = $this->em;
        
        $projectId = $project->getId();
        
        $actions = $em->getRepository('ArtoArchiBundle:Action')->findBy(array('project' => $projectId));
        
        return $actions;
        
    }
    
    public function getUsersByProject($project){
        $em = $this->em;
        
        $projectId = $project->getId();
        
        $users = $em->getRepository('ArtoArchiBundle:User')->findBy(array('project' => $projectId));
        
        return $users;
    }
    
    public function getOperationsByProject($project){
        $em = $this->em;
        
        $projectId = $project->getId();
        
        $operations = $em->getRepository('ArtoArchiBundle:Operation')->findBy(array('project' => $projectId));
        
        return $operations;
    }
    
    public function saveDescription($project, $default, $surface, $projectName, $address1, $city, $population, $housing, $other, $pilote, $piloteDate, $uploadedFile){
        $em = $this->em;
        $fileUploader = $this->fileUploader;
        
        $project_base = $em->getRepository('ArtoArchiBundle:Project')->find($project);
        if ($default != null) {
            $project = $project_base->getDefault();
            $project->setName($projectName);
        } else {
            $project = $project_base;
            $project->setProjectName($projectName);
        }
        
        //$project->setProjectName($request->get('projectName'));
        $project->setAddress1($address1);
        //$project->setAddress2($request->get('address2'));
        $project->setCity($city);
        
        $project->setSurface($surface);
        $project->setPopulation($population);
        $project->setHousing($housing);
        $project->setOther($other);
        $project->setPilote($pilote);

        if ($piloteDate != null) {
            $project->setPiloteDate($this->dateToSQL($piloteDate));
        }

        $fileUploader->uploadFile($project, $uploadedFile);

        $em->persist($project);
        $em->flush();
    }
    
    public function getNotes($project){
        $em = $this->em;
        
        $projectId = $project->getId();
        
        $notes = $em->getRepository('ArtoArchiBundle:Note')->findBy(array('project' => $projectId));
        
        return $notes;
    }
    
    public function getArrayNotesByCriterias($project){
        $notes = array();
        
        foreach($project->getNotes() as $note) {
            $criteria = $note->getCriteria();
            $criteriaId = $criteria->getId();
            $notes[$criteriaId] = $note->getValue();
        }
        
        return $notes;
    }
    
    public function getArrayActionsByCriterias($project){
        $actions = array();
        
        foreach($project->getNotes() as $note) {
            $criteria = $note->getCriteria();
            $criteriaId = $criteria->getId();
            $actions[$criteriaId] = $note->getAction();
        }
        
        return $actions;
    }
    
    public function getArrayCommentsByCriterias($project){
        $comments = array();
        
        foreach($project->getNotes() as $note) {
            $criteria = $note->getCriteria();
            $criteriaId = $criteria->getId();
            $comments[$criteriaId] = $note->getComment();
        }
        
        return $comments;
    }
    
    public function getArrayCommentsDefaultByCriterias($project){
        $comments_default = array();
        
        foreach($project->getNotes() as $note) {
            $criteria = $note->getCriteria();
            $criteriaId = $criteria->getId();
            $comments_default[$criteriaId] = $note->getCommentDefault();
        }
        
        return $comments_default;
    }
    
    public function getArrayNotesDefaultByCriterias($project){
        $notes_default = array();
        
        foreach($project->getDefault()->getNotes() as $note) {
            $notes_default[$note->getCriteria()->getId()] = $note->getValue();
        }
        
        return $notes_default; 
    }
    
    public function getArrayHelpNotes($project){
        $help_notes = array();
        
        foreach($project->getHelpNotes() as $note) {
            $help_notes[$note->getHelp()->getId()] = $note->getValue();
        }
        
        return $help_notes;
    }
    
    public function getArrayHelpActions($project){
        $help_actions = array();
        
        foreach($project->getHelpNotes() as $note) {
            $help_actions[$note->getHelp()->getId()] = $note->getAction();
        }
        
        return $help_actions;
    }
    
    public function getArrayHelpComments($project){
        $help_comments = array();
        
        foreach($project->getHelpNotes() as $note) {
            $help_comments[$note->getHelp()->getId()] = $note->getComment();
        }
        
        return $help_comments;
    }
    
    public function getArrayHelpCommentsDefault($project){
        $help_comments_default = array();
        
        foreach($project->getHelpNotes() as $note) {
            $help_comments_default[$note->getHelp()->getId()] = $note->getCommentDefault();
        }
        
        return $help_comments_default;
    }
    
    public function getArrayHelpNotesDefault($project){
        $help_notes_default = array();
        $projectDefault = $project->getDefault();
        
        foreach($projectDefault->getHelpNotes() as $note) {
            $help_notes_default[$note->getHelp()->getId()] = $note->getValue();
            //$help_default_actions[$note->getHelp()->getId()] = $note->getAction();
            //$help_default_comments[$note->getHelp()->getId()] = $note->getComment();
        }
        
        return $help_notes_default;    
    }
    
    /**
     * Les criteres de poids >= 7 qui sont côtés Faible
     * @param type $project
     */
    public function getHighCriterias($project){
        $em = $this->em;
        $projectId = $project->getId();
         
        $notes = $em->getRepository('ArtoArchiBundle:Note')->findBy(
                array( 'project' => $projectId, 'value' => false));
        
        $results = array();
        
        foreach($notes as $note){
            $criteria = $note->getCriteria();
            if($criteria->getWeight() >= 7){
                array_push($results, $criteria);
            }
        }
        
        return $results;
    }
    
    public function saveCategory($project, $category, $request){
        $em = $this->em;
        
        $criterias = $category->getCriterias();
        $projectId = $project->getId();
        $projectDefault = $project->getDefault();
        $projectDefaultId = $projectDefault->getId();
        
        foreach($criterias as $criteria)
        {
            $criteriaId = $criteria->getId();
            $c = $request->get('criteria_'.$criteriaId, 0);
            $cd = $request->get('criteria_default_'.$criteriaId, 0);

            $note = $this->getNote($projectId, $criteriaId);
            $note_default = $this->getNote($projectDefaultId, $criteriaId);

            if ($note == null) {
                $note = new Note();
                $note->setProject($project);
                $note->setCriteria($criteria);
            }

            if ($note_default == null) {
                $note_default = new Note();
                $note_default->setProject($project->getDefault());
                $note_default->setCriteria($criteria);
            }

            $note->setValue($c);
            $note_default->setValue($cd);
            $em->persist($note);
            $em->persist($note_default);
            
            foreach($criteria->getHelps() as $help) {

                $h = $request->get('help_'.$help->getId());

                if ($h != null) {
                    $help_note = $em->getRepository('ArtoArchiBundle:HelpNote')->findOneBy(array('project' => $project->getId(), 'help' => $help->getId()));

                    if ($help_note == null) {
                        $help_note = new HelpNote();
                        $help_note->setProject($project);
                        $help_note->setHelp($help);
                    }

                    $help_note->setValue($h);
                    $em->persist($help_note);
                }

                $h_default = $request->get('help_default_'.$help->getId());

                if ($h_default != null) {
                    $help_default_note = $em->getRepository('ArtoArchiBundle:HelpNote')->findOneBy(array('project' => $project->getDefault()->getId(), 'help' => $help->getId()));

                    if ($help_default_note == null) {
                        $help_default_note = new HelpNote();
                        $help_default_note->setProject($project->getDefault());
                        $help_default_note->setHelp($help);
                    }

                    $help_default_note->setValue($h_default);
                    $em->persist($help_default_note);
                }
            }
        }
        
        $em->flush();
    }
    
    public function saveAction($project, $id, $action){
        $em = $this->em;
        
        $help_note = $em->getRepository('ArtoArchiBundle:HelpNote')->findOneBy(array('project' => $project, 'help' => $id));
        $project = $em->getRepository('ArtoArchiBundle:Project')->find($project);
        $help = $em->getRepository('ArtoArchiBundle:Help')->find($id);

        if ($help_note == null) {
            $help_note = new HelpNote();
            $help_note->setProject($project);
            $help_note->setHelp($help);
        }

        $help_note->setAction($action);

        $em->persist($help_note);
        $em->flush();
    }
    
    public function saveComment($project, $id, $comment, $commentDefault){
        $em = $this->em;
        
        $help_note = $em->getRepository('ArtoArchiBundle:HelpNote')->findOneBy(array('project' => $project, 'help' => $id));

        $project = $em->getRepository('ArtoArchiBundle:Project')->find($project);
        $help = $em->getRepository('ArtoArchiBundle:Help')->find($id);

        if ($help_note == null) {
            $help_note = new HelpNote();
            $help_note->setProject($project);
            $help_note->setHelp($help);
        }

        $help_note->setComment($comment);
        $help_note->setCommentDefault($commentDefault);

        $em->persist($help_note);
        $em->flush();
    }
    
    public function searchCriteria($key){
        $em = $this->em;
        
        $newSearchKey = '%'.htmlentities($key, ENT_NOQUOTES, "UTF-8").'%';

        $sql = "SELECT c FROM ArtoArchiBundle:Help c WHERE c.label LIKE :key";
        $query = $em->createQuery($sql)->setParameter('key', $newSearchKey);

        $results = $query->getResult();
        
        return $results;
    }
    
    public function savePlanAction($project, $request){
        $em = $this->em;
        
        
        $notes = $project->getHelpNotes();
        //$notes = $this->getNotes($project);
        
        foreach($notes as $note) {
            if($note->getAction() != null){
                
                $criteria = $note->getHelp()->getCriteria();
                
                $actionFound = $this->findActions($project, $criteria);
                
                if($actionFound == null){
                    $action = new Action();
                    
                    $piloteName = $request->get('pilote_new');
                    $piloteDate = $this->dateToSQL($request->get('pilote_date_new'));

                    $action->setProject($project);
                    $action->setCriteria($criteria);
                    $action->setPilote($piloteName);
                    $action->setPiloteDate($piloteDate);

                    $em->persist($action);     
                }else{
                    foreach($actionFound as $action){
                        $actionFoundId = $action->getId();
                    
                        $piloteName = $request->get('pilote_'.$actionFoundId);
                        $piloteDate = $this->dateToSQL($request->get('pilote_date_'.$actionFoundId));
                    
                        $action->setPilote($piloteName);
                        $action->setPiloteDate($piloteDate);
                    
                        $em->persist($action);
                    }
                    
                }
            }
        }  
        
        $em->flush();
    }
    
     public function checkPreuves($project){
        $em = $this->em;

        $proofs = array();

        $projectDefault = $project->getDefault();
        $projectDefaultId = $projectDefault->getId();
        $projectId = $project->getId();
        
        $helpNotes = $project->getHelpNotes();
        $helpNotesDefault = $projectDefault->getHelpNotes();
        
        $criterias = $this->getCriterias();
        
        foreach($criterias as $criteria){
            $proofs[$criteria->getId()] = true;
        }
        
        foreach($helpNotes as $note){
            $value = $note->getValue();
            $comment = $note->getComment();
            $commentDefault = $note->getCommentDefault();
            $help = $note->getHelp()->getId();
            
            if(count($helpNotesDefault) != 0){
                foreach($helpNotesDefault as $noteD) {
                     $helpD = $noteD->getHelp()->getId();
                     if($helpD == $help){
                         $valueD = $noteD->getValue();
                     }
                }
            }else{
                $valueD = 0;
            }
            
            if($value == 1 || $valueD == 1){
                if($comment == '' && $commentDefault == ''){
                    $help = $note->getHelp();
                    $criteria = $help->getCriteria();
                    $proofs[$criteria->getId()] = false;
                }   
            }
        }
        
        
        foreach($helpNotesDefault as $noteD){
            $valueD = $noteD->getValue();
            $helpD = $noteD->getHelp()->getId();
            
            foreach($helpNotes as $note){
                $help = $note->getHelp()->getId();
                if($help == $helpD){
                    $value = $note->getValue();
                    $comment = $note->getComment();
                    $commentDefault = $note->getCommentDefault();
                }
            }
            
            if($value == 1 || $valueD == 1){
                if($comment == '' && $commentDefault == ''){
                    $help = $note->getHelp();
                    $criteria = $help->getCriteria();
                    $proofs[$criteria->getId()] = false;
                }   
            }
        }
        
        

        return $proofs;
    }
    
}
