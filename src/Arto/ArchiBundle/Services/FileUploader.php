<?php

namespace Arto\ArchiBundle\Services;

use Arto\ArchiBundle\Entity\Project;
use Arto\ArchiBundle\Entity\Folder;
use Arto\ArchiBundle\Entity\Note;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FileUploader {
    
    public function uploadFile($project, $file){
        if (isset($file) && isset($file['tmp_name'])) {
            if($file['name'] != ''){
                move_uploaded_file($file['tmp_name'], __DIR__.'/../../../../web/uploads/archi/'.$file['name']);
                $project->setPath($file['name']);
            } 
        }
    }
    
}