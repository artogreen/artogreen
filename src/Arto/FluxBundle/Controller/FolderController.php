<?php

namespace Arto\FluxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Arto\FluxBundle\Entity\Folder;
use Arto\FluxBundle\Entity\Project;

use Symfony\Component\HttpFoundation\Response;

class FolderController extends Controller
{
    public function createFolderAction(){
        $user = $this->get('security.context')->getToken()->getUser();
        $userId = $user->getId();
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        $name = $request->get('name');
        $code = $request->get('code');
        
        $folder = new Folder();
        $folder->setUser($user);
        $folder->setName($name);
        $folder->setCode($code);
        $folder->setIsDeployed('non');
        
        $em->persist($folder);
        $em->flush();
       
        return $this->redirect($this->generateUrl('flux'));
    }
    
    public function deployFolderAction(){
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        
        $folderId = $request->get('folderId');
        
        $folder = $em->getRepository('ArtoFluxBundle:Folder')->find($folderId);
        
        if($folder->getIsDeployed() == 'non'){
            $folder->setIsDeployed('oui');
            $isDeployed = 'oui';
        }else{
            $folder->setIsDeployed('non');
            $isDeployed = 'non';
        }
        
        $result = array(
            'isDeployed' => $isDeployed
        );
        
        $em->persist($folder);
        $em->flush();
        
        $json = json_encode($result);
        $response = new Response($json);
        return $response;
    }
    
    public function hasCodeAction(){
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        
        $folderId = $request->get('folderId');
        $isFile = $request->get('isFile');
        
        $folder = $em->getRepository('ArtoFluxBundle:Folder')->find($folderId);
        
        if($folder->getCode() != ''){
            $hasCode = 'oui';
        }else{
            $hasCode = 'non';
        }
        
        if($folder->getIsDeployed() == 'non'){
            $isDeployed = 'non';
        }else{
            if($hasCode == 'oui'){
                $folder->setIsDeployed('non');
            }
            $isDeployed = 'oui';
        }
        
        if($isFile != 'oui'){
            $em->persist($folder);
            $em->flush();
        }
        
        $result = array(
            'hasCode' => $hasCode,
            'isDeployed' => $isDeployed
        );
        
        $json = json_encode($result);
        $response = new Response($json);
        return $response;
    }
    
    public function isEmptyAction(){
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        
        $folderId = $request->get('folderId');
        
        $folder = $em->getRepository('ArtoFluxBundle:Folder')->find($folderId);
        
        if($folder->isEmpty() == true){
            $isEmpty = 'oui';
        }else{
            $isEmpty = 'non';
        }
        
        $result = array(
            'isEmpty' => $isEmpty
        );
        
        $json = json_encode($result);
        $response = new Response($json);
        return $response;
    }
    
    public function checkCodeAction(){
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        
        $folderId = $request->get('folderId');
        $password = $request->get('password');
        
        $folder = $em->getRepository('ArtoFluxBundle:Folder')->find($folderId);
        
        if($folder->getCode() == $password){
            $isCorrect = 'oui';
        }else{
            $isCorrect = 'non';
        }
        
         $result = array(
            'isCorrect' => $isCorrect
        );
        
        $json = json_encode($result);
        $response = new Response($json);
        return $response;
    }
    
    public function deleteFolderAction(){
        $user = $this->get('security.context')->getToken()->getUser();

        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        
        $id = $request->get('folderId');

        $folder = $em->getRepository('ArtoFluxBundle:Folder')->find($id);

        if ($folder->getUser()->getId() == $user->getId()) {
            foreach($folder->getProjects() as $project){
                $project->setFolder(null);
                $em->persist($project);
            }
            $folder->removeAllProjects();
            $em->remove($folder);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('flux'));
    }
    
    public function setFolderAction(){
        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        
        $projectId = $request->get('projectId');
        $dossierId = $request->get('dossierId');
        
        $project = $em->getRepository('ArtoFluxBundle:Project')->find($projectId);
        
        if($dossierId == ''){
            $project->setFolder(null);
            $em->persist($project);
        }else{
            $dossier = $em->getRepository('ArtoFluxBundle:Folder')->find($dossierId);

            $dossier->addProject($project); 
            $project->setFolder($dossier);

            $em->persist($dossier);
            $em->persist($project);
        }
        $em->flush();
        
        return $this->redirect($this->generateUrl('flux'));
    }
    
     public function sortFolderAction(){
        $em = $this->getDoctrine()->getEntityManager();
        
        $user = $this->get('security.context')->getToken()->getUser();
        $userId = $user->getId();
        
        $request = $this->getRequest();
        
        $letter = $request->get('lettre');
        
        $folderLetters = array();
        
        if($letter != null){
           $searchTerm = $letter.'%';

            $sql = "SELECT c FROM ArtoFluxBundle:Folder c WHERE c.user = :user and c.name like :searchTerm ORDER BY c.name";
            $query = $em->createQuery($sql)->setParameters(
                    array('user' => $userId, 'searchTerm' => $searchTerm));
            $folders = $query->getResult();
            
            foreach($folders as $folder){
                $folderName = $folder->getName();
                $firstLetter = strtoupper(substr($folderName,0,1));

                if(!in_array($firstLetter, $folderLetters)){
                    array_push($folderLetters, $firstLetter);
                }
            }
            
            return $this->render('ArtoFluxBundle:Default:index.html.twig', array('user' => $userId, 'folders' => $folders, 'myLetter' => $letter, 'folderLetters' => $folderLetters));   
        }else{ 
            $projects = $em->getRepository('ArtoFluxBundle:Project')->findAllActive($user);
            
            //projectsWithoutFolder
            $projectsWF = $em->getRepository('ArtoFluxBundle:Project')->findBy(
                    array('greenUser' => $userId, 'folder' => null), array('name' => 'ASC'));

            $folders = $em->getRepository('ArtoFluxBundle:Folder')->findBy(
                    array('user' => $userId), array('name' => 'ASC'));
            
            foreach($folders as $folder){
                $folderName = $folder->getName();
                $firstLetter = strtoupper(substr($folderName,0,1));

                if(!in_array($firstLetter, $folderLetters)){
                    array_push($folderLetters, $firstLetter);
                }
            }

            return $this->render('ArtoFluxBundle:Default:index.html.twig', array('projects' => $projects, 'user' => $userId, 'folders' => $folders,'projectsWF'=> $projectsWF, 'myLetter'=> '', 'folderLetters' => $folderLetters));
        }  
    }
    
    public function renameFolderAction(){
        $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        
        $id = $request->get('id');
        $newName = $request->get('name');

        $folder = $em->getRepository('ArtoFluxBundle:Folder')->find($id);
        
        $folder->setName($newName);
        
        $em->persist($folder);
        $em->flush();
               
        return $this->redirect($this->generateUrl('flux'));  
    }
}
