<?php

namespace Arto\FluxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Arto\FluxBundle\Entity\Operation;

use Symfony\Component\HttpFoundation\Response;

class OperationController extends Controller
{

    public function saveAction($id)
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();
        $project = $em->getRepository('ArtoFluxBundle:Project')->find($id);
        $operations = $em->getRepository('ArtoFluxBundle:Operation')->findBy(array('project' => $project->getId()));

        foreach($operations as $operation) {

            $date = $request->get('operation_date_'.$operation->getId());
            $date = explode('/', $date);

            if (isset($date[2])) {
                $date = new \DateTime($date[2].'-'.$date[1].'-'.$date[0]);
                $operation->setDateAction($date);
                $operation->setDescription($request->get('operation_description_'.$operation->getId()));
                $em->persist($operation);
            }
        }

        if ($request->get('operation_date') != null && $request->get('operation_description') != null) {
            $operation = new Operation();
            $operation->setProject($project);
            $date = $request->get('operation_date');
            $date = explode('/', $date);
            $date = new \DateTime($date[2].'-'.$date[1].'-'.$date[0]);
            $operation->setDateAction($date);
            $operation->setDescription($request->get('operation_description'));
            $em->persist($operation);
        }
        $em->flush();

        return $this->redirect($this->generateUrl('flux_project_description', array('id' => $id)));
    }

}
