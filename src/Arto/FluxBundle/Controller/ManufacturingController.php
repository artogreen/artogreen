<?php

namespace Arto\FluxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Arto\FluxBundle\Entity\Project;
use Arto\FluxBundle\Entity\User;
use Arto\FluxBundle\Entity\ManufacturingPlastic;
use Arto\FluxBundle\Entity\ManufacturingMetallic;
use Arto\FluxBundle\Entity\ManufacturingPart;

use Arto\AcvBundle\Entity\Base;
use Arto\AcvBundle\Entity\Type;
use Arto\AcvBundle\Entity\Family;


class ManufacturingController extends Controller
{
    public function indexAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoFluxBundle:Project')->find($id);
        $categories = $em->getRepository('ArtoFluxBundle:ManufacturingCategory')->findAll();
        
        $materialsType = $em->getRepository('ArtoAcvBundle:Type')->findOneBy(
                array('name' => 'materiau'));
        $materialId = $materialsType->getId();
        
        $additivesType = $em->getRepository('ArtoAcvBundle:Type')->findOneBy(
                array('name' => 'additif'));
        $additiveId = $additivesType->getId();

        //Plastic
        //Normal plastic
        $normalPlasticFamily = $em->getRepository('ArtoAcvBundle:Family')->findOneBy(
                array('name' => 'plastique'));
        $normalPlasticId = $normalPlasticFamily->getId();
         
        //Textile plastic
        $textilePlasticFamily = $em->getRepository('ArtoAcvBundle:Family')->findOneBy(
                array('name' => 'textile plastique'));
        $textilePlasticId = $textilePlasticFamily->getId();
        
        //Bio plastic
        $bioPlasticFamily = $em->getRepository('ArtoAcvBundle:Family')->findOneBy(
                array('name' => 'bio-plastique'));
        $bioPlasticId = $bioPlasticFamily->getId();
        
        $sql = "SELECT c FROM ArtoAcvBundle:Base c WHERE c.type=:type AND (c.family=:plasticId OR c.family=:textilePlasticId OR c.family=:bioPlasticId) AND c.isVisible=:answerId ORDER BY c.name";
        $query = $em->createQuery($sql)->setParameters(
                array('type' => $materialId, 'plasticId' => $normalPlasticId, 'textilePlasticId' => $textilePlasticId, 'bioPlasticId' => $bioPlasticId, 'answerId' => 'oui'));
        $plasticMaterials = $query->getResult();
        
        //Additives
        //Plastic
        //IDs already found for previous requests
        
        //Autres
        $otherFamily = $em->getRepository('ArtoAcvBundle:Family')->findOneBy(
                array('name' => 'autres'));
        $otherId = $otherFamily->getId();
        
        $sql2 = "SELECT c FROM ArtoAcvBundle:Base c WHERE c.type=:type AND (c.family=:plasticId OR c.family=:textilePlasticId OR c.family=:bioPlasticId or c.family=:otherId) AND c.isVisible=:answerId ORDER BY c.name";
        $query2 = $em->createQuery($sql2)->setParameters(
                array('type' => $additiveId, 'plasticId' => $normalPlasticId, 'textilePlasticId' => $textilePlasticId, 'bioPlasticId' => $bioPlasticId, 'otherId' => $otherId, 'answerId' => 'oui'));
        $plasticAdditives = $query2->getResult();
        
        //Colorants
        $colorantFamily =  $em->getRepository('ArtoAcvBundle:Family')->findOneBy(
                array('name' => 'colorant'));
        $colorantId = $colorantFamily->getId();
        
        $sql3 = "SELECT c FROM ArtoAcvBundle:Base c WHERE c.type=:type AND c.family=:colorantId AND c.isVisible=:answerId ORDER BY c.name";
        $query3 = $em->createQuery($sql3)->setParameters(
                array('type' => $additiveId, 'colorantId' => $colorantId, 'answerId' => 'oui'));
        $plasticColorants = $query3->getResult();
        
        //Procédés finition
        $processesType = $em->getRepository('ArtoAcvBundle:Type')->findOneBy(
                array('name' => 'process'));
        $processesId = $processesType->getId();
        
        $finitionFamily = $em->getRepository('ArtoAcvBundle:Family')->findOneBy(
                array('name' => 'finition'));
        $finitionId = $finitionFamily->getId();
        
        $sql4 = "SELECT c FROM ArtoAcvBundle:Base c WHERE c.type=:type AND c.family=:normalPlasticId AND c.isVisible=:answerId ORDER BY c.name";
        $query4 = $em->createQuery($sql4)->setParameters(
                array('type' => $processesId, 'normalPlasticId' => $normalPlasticId, 'answerId' => 'oui'));
        $plasticProcesses = $query4->getResult();
        
        
        //Metallic
        //Materials
        $metallicFamily = $em->getRepository('ArtoAcvBundle:Family')->findOneBy(
                array('name' => 'métallique'));
        $metallicId = $metallicFamily->getId();
        
        $sql5 = "SELECT c FROM ArtoAcvBundle:Base c WHERE c.type=:type AND c.family=:metallicId AND c.isVisible=:answerId ORDER BY c.name";
        $query5 = $em->createQuery($sql5)->setParameters(
                array('type' => $materialId, 'metallicId' => $metallicId, 'answerId' => 'oui'));
        $metallicMaterials = $query5->getResult();
        
        $sql6 = "SELECT c FROM ArtoAcvBundle:Base c WHERE c.type=:type AND c.family=:metallicId AND c.isVisible=:answerId ORDER BY c.name";
        $query6 = $em->createQuery($sql6)->setParameters(
                array('type' => $processesId, 'metallicId' => $metallicId, 'answerId' => 'oui'));
        $metallicProcesses = $query6->getResult();
        
        $sql7 = "SELECT c FROM ArtoAcvBundle:Base c WHERE c.type=:type AND c.family=:finitionId AND c.isVisible=:answerId ORDER BY c.name";
        $query7 = $em->createQuery($sql7)->setParameters(
                array('type' => $processesId, 'finitionId' => $finitionId, 'answerId' => 'oui'));
        $metallicFinitionProcesses = $query7->getResult();
        
        //Existant dans la BDD
        $manufacturing_parts = $em->getRepository('ArtoFluxBundle:ManufacturingPart')->findBy(
                array('project' => $id));

        $parts = array();
        foreach ($manufacturing_parts as $part) {
            $category_id = $part->getCategory()->getId();
            if (!array_key_exists($category_id, $parts)) {
                $parts[$category_id] = array();
            }
            $parts[$category_id][] = $part;
        }
        
        $plasticParts = $em->getRepository('ArtoFluxBundle:ManufacturingPlastic')->findBy(
                array('project' => $id));

        $metallicParts = $em->getRepository('ArtoFluxBundle:ManufacturingMetallic')->findBy(
                array('project' => $id));

        // Calculate Global weight
        $weightManufacturing = 0;
        foreach($manufacturing_parts as $part) {
            $weightManufacturing += $part->getQuantity() * $part->getWeight();
        }

        foreach($plasticParts as $part) {
            $weightManufacturing += $part->getQuantity() * $part->getWeight();
        }

        foreach($metallicParts as $part) {
            $weightManufacturing += $part->getQuantity() * $part->getWeight();
        }

        return $this->render('ArtoFluxBundle:Default:manufacturing.html.twig', array(
            'project' => $project,
            'categories' => $categories,
            'plasticMaterials' => $plasticMaterials,
            'plasticAdditives' => $plasticAdditives,
            'plasticColorants' => $plasticColorants,
            'plasticProcesses' => $plasticProcesses,
            'metallicMaterials' => $metallicMaterials,
            'metallicProcesses' => $metallicProcesses,
            'metallicFinitionProcesses' => $metallicFinitionProcesses,
            'plasticParts' => $plasticParts,
            'metallicParts' => $metallicParts,
            'parts' => $parts,
            'weightManufacturing' => $weightManufacturing
        ));
    }

    public function generalSaveAction($project){
        $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoFluxBundle:Project')->find($project);

        $categories = $this->getRequest()->get('category');


        foreach($categories as $category_id) {
            $category = $em->getRepository('ArtoFluxBundle:ManufacturingCategory')->find($category_id);
            
            $categoryAnswer = $this->getRequest()->get('yesNo_'.$category_id);
            
            if($categoryAnswer != "no"){
                $parts = $this->getRequest()->get('part_'.$category_id);

                foreach($parts as $part) {
                    // We check if the qty of the part is not qty
                    if (isset($part['qty']) && $part['qty'] > 0) {

                        // Try Retreiving the object if the request id exist
                        if (isset($part['id']) && $part['id'] != null) {
                            $p = $em->getRepository('ArtoFluxBundle:ManufacturingPart')->find($part['id']);
                        } else {
                            $p = new ManufacturingPart();
                        }

                        $p->setProject($project);
                        $p->setCategory($category);

                        // Retreive type to set the part type FK
                        if($category->getId() != 5){
                            $type = $em->getRepository('ArtoFluxBundle:ManufacturingType')->find($part['type']);
                            $p->setType($type);
                        }    

                        $p->setQuantity($part['qty']);
                        $weight = $part['weight'];
                        $weight = str_replace(',','.',$weight);
                        $p->setWeight($weight);
                        $p->setInformations($part['info']);


                        $em->persist($p);
                    }
                }
            }
        }

        // We flush the all the modifications at once
        $em->flush();

        return $this->redirect($this->generateUrl('flux_manufacturing', array('id' => $project->getId())));
    }

     public function plasticSaveAction($project){
       $em = $this->getDoctrine()->getEntityManager();

       $project = $em->getRepository('ArtoFluxBundle:Project')->find($project);

       $plasticParts = $this->getRequest()->get('plastic');

       foreach($plasticParts as $plasticPart){
            // We check if the label of the part is not empty
            if (isset($plasticPart['label']) && $plasticPart['label'] != '') {
                // Try Retreiving the object if the request id exist
                if (isset($plasticPart['id']) && $plasticPart['id'] != null) {
                    $p = $em->getRepository('ArtoFluxBundle:ManufacturingPlastic')->find($plasticPart['id']);
                } else {
                    $p = new ManufacturingPlastic();
                }

                $p->setProject($project);
                $p->setLabel($plasticPart['label']);

                if ($plasticPart['base'] == "null") {
                    $p->setBase(null);
                    $p->setQuantity(null);
                    $p->setWeight(null);
                } else {
                    //Retrieve base to set the plasticPart base FK
                    $base = $em->getRepository('ArtoAcvBundle:Base')->find($plasticPart['base']);
                    $p->setBase($base);

                    $p->setQuantity($plasticPart['quantity']);
                    $weight = $plasticPart['weight'];
                    $weight = str_replace(',', '.', $weight);
                    $p->setWeight($weight);
                }

                if($plasticPart['charge'] == "null"){
                    $p->setCharge(null);
                    $p->setPercentCharge(null);
                }else{
                    //Retrieve charge to set the plasticPart charge FK
                    $charge = $em->getRepository('ArtoAcvBundle:Base')->find($plasticPart['charge']);
                    $p->setCharge($charge);

                    $percentCharge = $plasticPart['percentCharge'];
                    $percentCharge = str_replace(',', '.', $percentCharge);
                    $p->setPercentCharge($percentCharge);
                }


                if($plasticPart['additive'] == "null"){
                    $p->setAdditive(null);
                    $p->setPercentAdditive(null);
                }else{
                    //Retrieve additive to set the plasticPart additive FK
                    $additive = $em->getRepository('ArtoAcvBundle:Base')->find($plasticPart['additive']);
                    $p->setAdditive($additive);

                    $percentAdditive = $plasticPart['percentAdditive'];
                    $percentAdditive = str_replace(',', '.', $percentAdditive);
                    $p->setPercentAdditive($percentAdditive);
                }

                if($plasticPart['colorant'] == "null"){
                    $p->setColorant(null);
                    $p->setPercentColorant(null);
                }else{
                    //Retrieve colorant to set the plasticPart colorant FK
                    $colorant = $em->getRepository('ArtoAcvBundle:Base')->find($plasticPart['colorant']);
                    $p->setColorant($colorant);

                    $percentColorant = $plasticPart['percentColorant'];
                    $percentColorant = str_replace(',', '.', $percentColorant);
                    $p->setPercentColorant($percentColorant);  
                }
                
                if($plasticPart['process'] == "null"){
                    $p->setProcess(null);
                }else{
                    //Retrieve process to set the metallicPart process FK
                    $process = $em->getRepository('ArtoAcvBundle:Base')->find($plasticPart['process']);
                    $p->setProcess($process);
                }
                
                $em->persist($p);
            }    
        }

        // We flush the all the modifications at once
        $em->flush();

        return $this->redirect($this->generateUrl('flux_manufacturing', array('id' => $project->getId())));
    }
    
    public function plasticDeleteAction(){
        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        
        $plasticPartId = $request->get('id');
        
        $plasticPart = $em->getRepository('ArtoFluxBundle:ManufacturingPlastic')->findOneBy(
                array('id' => $plasticPartId));
        $project = $plasticPart->getProject();
        
        $em->remove($plasticPart);
        $em->flush();
        
        return $this->redirect($this->generateUrl('flux_manufacturing', array('id' => $project->getId())));      
    }

    public function metallicSaveAction($project){
       $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoFluxBundle:Project')->find($project);

        $metallicParts = $this->getRequest()->get('metallic');

        foreach ($metallicParts as $metallicPart) {
            // We check if the label of the part is not empty
            if (isset($metallicPart['label']) && $metallicPart['label'] != '') {
                // Try Retreiving the object if the request id exist
                if (isset($metallicPart['id']) && $metallicPart['id'] != null) {
                    $p = $em->getRepository('ArtoFluxBundle:ManufacturingMetallic')->find($metallicPart['id']);
                } else {
                    $p = new ManufacturingMetallic();
                }

                $p->setProject($project);
                $p->setLabel($metallicPart['label']);

                if($metallicPart['base'] == "null"){
                    $p->setBase(null);
                    $p->setQuantity(null);
                    $p->setWeight(null);
                }else{
                    //Retrieve base to set the metallicPart base FK
                    $base = $em->getRepository('ArtoAcvBundle:Base')->find($metallicPart['base']);
                    $p->setBase($base);

                    $p->setQuantity($metallicPart['quantity']);

                    $weight = $metallicPart['weight'];
                    $weight = str_replace(',', '.', $weight);
                    $p->setWeight($weight); 
                }
                
                if($metallicPart['process'] == "null"){
                    $p->setProcess(null);
                }else{
                    //Retrieve process to set the metallicPart process FK
                    $process = $em->getRepository('ArtoAcvBundle:Base')->find($metallicPart['process']);
                    $p->setProcess($process);
                }


                if($metallicPart['processFinition'] == "null"){
                    $p->setProcessFinition(null);
                    $p->setProcessSurface(null);
                }else{
                    //Retrieve process to set the metallicPart process FK
                    $process = $em->getRepository('ArtoAcvBundle:Base')->find($metallicPart['processFinition']);
                    $p->setProcessFinition($process);

                    $processSurface = $metallicPart['processSurface'];
                    $processSurface = str_replace(',', '.', $processSurface);
                    $p->setProcessSurface($processSurface); 
                }
                
                $em->persist($p);
            }
        }    
        // We flush the all the modifications at once
        $em->flush();

        return $this->redirect($this->generateUrl('flux_manufacturing', array('id' => $project->getId())));
    }
    
    public function metallicDeleteAction(){
        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        
        $metallicPartId = $request->get('id');
        
        $metallicPart = $em->getRepository('ArtoFluxBundle:ManufacturingMetallic')->findOneBy(
                array('id' => $metallicPartId));
        $project = $metallicPart->getProject();
        
        $em->remove($metallicPart);
        $em->flush();
        
        return $this->redirect($this->generateUrl('flux_manufacturing', array('id' => $project->getId())));      
    }
}