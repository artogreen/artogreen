<?php

namespace Arto\FluxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Arto\AcvBundle\Entity\Base;
use Arto\FluxBundle\Entity\Project;
use Arto\FluxBundle\Entity\Product;
use Arto\AcvBundle\Entity\Assembly;
use Arto\AcvBundle\Entity\Family;
use Arto\AcvBundle\Entity\Type;
use Arto\FluxBundle\Entity\Delivery;
use Arto\FluxBundle\Entity\DeliveryPart;

class DeliveryController extends Controller
{
    public function indexAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        
        $project = $em->getRepository('ArtoFluxBundle:Project')->find($id);
        $product = $em->getRepository('ArtoFluxBundle:Product')->findOneBy(
                array('project' => $project->getId()));

        $type = $em->getRepository('ArtoAcvBundle:Type')->findOneByName('process');
        $family = $em->getRepository('ArtoAcvBundle:Family')->findOneByName('énergie');

        $transport = $em->getRepository('ArtoAcvBundle:Family')->findOneByName('transport');
        $transports = $em->getRepository('ArtoAcvBundle:Base')->findByFamily($transport->getId());

        $parts = null;
        $delivery = $product->getDelivery();

        if ($delivery != null) {
            $parts = $delivery->getParts();
        } else {
            $delivery = new Delivery();
        }

        return $this->render('ArtoFluxBundle:Default:delivery.html.twig', array(
            'project' => $project,
            'product' => $product,
            'transports' => $transports,
            'parts' => $parts,
            'delivery' => $delivery
        ));
    }

    public function saveAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();

        $project = $em->getRepository('ArtoFluxBundle:Project')->find($id);
        $product = $em->getRepository('ArtoFluxBundle:Product')->findOneBy(
                array('project' => $project->getId()));

        $delivery = $product->getDelivery();

        if ($delivery == null) {
            $delivery = new Delivery();
            $delivery->setProduct($product);
        }

        $delivery->setDetailed($request->get('detailed'));
        $delivery->setMode($request->get('mode'));

        if ($delivery->getDetailed() == 1) {
            $parts = $request->get('part');
            foreach ($parts as $p) {

                if ($p['transport'] != '') {

                    if (!isset($p['id'])) {
                        $part = new DeliveryPart();
                        $part->setDelivery($delivery);
                    } else {
                        $part =  $em->getRepository('ArtoFluxBundle:DeliveryPart')->find($p['id']);
                    }

                    $transport = $em->getRepository('ArtoAcvBundle:Base')->find($p['transport']);
                    $part->setTransport($transport);
                    $part->setDistance($p['distance']);

                    $em->persist($part);
                }
            }
        } elseif ($delivery->getDetailed() == 2) {
            
            foreach ($delivery->getParts() as $part) {
                $em->remove($part);
            }
        }

        $em->persist($delivery);
        $em->flush();

        return $this->redirect($this->generateUrl('flux_delivery', array(
            'id' => $project->getId(),
        )));
    }

    public function partDeleteAction($id, $part)
    {
        $em = $this->getDoctrine()->getEntityManager();
        
        $project = $em->getRepository('ArtoFluxBundle:Project')->find($id);
      
        $p = $em->getRepository('ArtoFluxBundle:DeliveryPart')->findOneBy(
                array('id' => $part));
        $em->remove($p);

        $em->flush();

        return $this->redirect($this->generateUrl('flux_delivery', array(
            'id' => $project->getId(),
        )));
    }

}
