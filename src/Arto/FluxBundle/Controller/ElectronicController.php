<?php

namespace Arto\FluxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Arto\FluxBundle\Entity\Project;
use Arto\FluxBundle\Entity\Product;
use Arto\FluxBundle\Entity\User;
use Arto\FluxBundle\Entity\ManufacturingPlastic;
use Arto\FluxBundle\Entity\ManufacturingMetallic;
use Arto\FluxBundle\Entity\ManufacturingPart;
use Arto\FluxBundle\Entity\ElectronicPcb;
use Arto\FluxBundle\Entity\ElectronicPcbPart;
use Arto\FluxBundle\Entity\ElectronicOtherPart;
use Arto\FluxBundle\Entity\ElectronicType;
use Arto\FluxBundle\Entity\ElectronicComponent;
use Arto\FluxBundle\Entity\ElectronicComponentPart;

use Arto\AcvBundle\Entity\Base;
use Arto\AcvBundle\Entity\Type;
use Arto\AcvBundle\Entity\Family;


class ElectronicController extends Controller
{
    public function indexAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoFluxBundle:Project')->find($id);
        $product = $project->getProduct();
        
        $categories = $em->getRepository('ArtoFluxBundle:ManufacturingCategory')->findAll();
        
        $repositoryType = $em->getRepository('ArtoAcvBundle:Type');
        
        $query = $repositoryType->createQueryBuilder('p')
               ->where('p.name LIKE :word')
               ->setParameter('word', '%process%')
               ->getQuery();
        
        /*$processesType = $em->getRepository('ArtoAcvBundle:Type')->findOneBy(
                array('name' => 'process'));*/
        $processesId = $query->getResult();
        
        /*$componentsType = $em->getRepository('ArtoAcvBundle:Type')->findOneBy(
                array('name' => 'composant électronique'));*/
        $query = $repositoryType->createQueryBuilder('p')
               ->where('p.name LIKE :word')
               ->setParameter('word', '%composant%')
               ->getQuery();
        
        $componentsId = $query->getResult();
        
        $repositoryFamily = $em->getRepository('ArtoAcvBundle:Family');
        
        $query = $repositoryFamily->createQueryBuilder('p')
               ->where('p.name LIKE :word')
               ->setParameter('word', '%électronique%')
               ->getQuery();
        
        /*$electronicFamily = $em->getRepository('ArtoAcvBundle:Family')->findOneBy(
                array('name' => 'électronique (impact procédé compris)'));*/
        
        $electronicId = $query->getResult();
        
        
        $rsm = new ResultSetMappingBuilder($em);
        $rsm->addEntityResult('ArtoAcvBundle:Base', 'b');
        $rsm->addFieldResult('b', 'id', 'id');
        $rsm->addFieldResult('b', 'name', 'name');
        //$sqlPCBs = "SELECT c FROM ArtoAcvBundle:Base c WHERE c.type=:type AND c.family=:family and c.name LIKE BINARY :pcb AND c.isVisible=:answerId ORDER BY c.id";
        $sqlPCBs = "SELECT id,name FROM acv_base WHERE name LIKE BINARY :pcb AND isVisible=:answerId ORDER BY name";
        $queryPCBs = $em->createNativeQuery($sqlPCBs,$rsm);
        $queryPCBs->setParameters(
                array('pcb' => "%Carte%", 'answerId' => 'oui'));
       //$queryPCBs = $em->createQuery($sqlPCBs)->setParameters(
                //array('type' => $componentsId, 'family' => $electronicFamily, 'pcb' => "%Carte%", 'answerId' => 'oui'));
        
        $pcbTypes = $queryPCBs->getResult();
        
        $otherParts = $em->getRepository('ArtoFluxBundle:ElectronicOtherPart')->findBy(
                array('project' => $project->getId()));
        
        $sqlOthers = "SELECT c FROM ArtoAcvBundle:Base c WHERE c.type=:type AND c.family=:family and c.name NOT LIKE :pcb AND c.isVisible=:answerId ORDER BY c.name";
        $queryOthers = $em->createQuery($sqlOthers)->setParameters(
                array('type' => $componentsId, 'family' => $electronicId, 'pcb' => "%PCB%", 'answerId' => 'oui'));
        
        $others = $queryOthers->getResult();
        
        $pcbParts = $em->getRepository('ArtoFluxBundle:ElectronicPcbPart')->findBy(
                array('project' => $project->getId()));

        $componentsParts = $em->getRepository('ArtoFluxBundle:ElectronicComponentPart')->findBy(
                array('project' => $project->getId()));
             
        $sql = "SELECT c FROM ArtoAcvBundle:Base c WHERE c.type=:type AND c.family=:electronicId AND c.isVisible=:answerId ORDER BY c.name";
        $query = $em->createQuery($sql)->setParameters(
                array('type' => $processesId, 'electronicId' => $electronicId, 'answerId' => 'oui'));
        
        $processes = $query->getResult();
        
        $sqlFinitions = "SELECT c FROM ArtoAcvBundle:Base c WHERE c.type=:type AND c.family=:family and c.name LIKE :finition AND c.isVisible=:answerId ORDER BY c.name";
        $queryFinitions = $em->createQuery($sqlFinitions)->setParameters(
                array('type' => $processesId, 'family' => $electronicId, 'finition' => "%PWB (Finition%", 'answerId' => 'oui'));
        $finitionProcesses = $queryFinitions->getResult();
        
        $sqlSoudures = "SELECT c FROM ArtoAcvBundle:Base c WHERE c.type=:type AND c.family=:family and (c.name LIKE :soudure1 OR c.name LIKE :soudure2 OR c.name LIKE :soudure3) AND c.isVisible=:answerId ORDER BY c.name"; 
        $querySoudures = $em->createQuery($sqlSoudures)->setParameters(
                array('type' => $processesId, 'family' => $electronicId, 'soudure1' => "%PWB : Soudure%", 'soudure2' => "%PWB - Refusion%", 'soudure3' => "%PWB - Brasure%", 'answerId' => 'oui'));
        $soudureProcesses = $querySoudures->getResult();
        
        $typesBoitiers = $em->getRepository('ArtoFluxBundle:ElectronicType')->findBy(
                array('isVisible' => 'oui'), array('label' => 'ASC'));

        $composantsElectroniques = $em->getRepository('ArtoFluxBundle:ElectronicComponent')->findAll();

        // Calculate Global weight
        // $weightElectronic = 0;
        
        $this->recalculateWeightElectronicSummed($product);

        return $this->render('ArtoFluxBundle:Default:electronic.html.twig', array(
            'project' => $project,
            'categories' => $categories,
            'pcbTypes' => $pcbTypes,
            'processes' => $processes,
            'typesBoitiers' => $typesBoitiers,
            'composantsElectroniques' => $composantsElectroniques,
            'pcbParts' => $pcbParts,
            'componentsParts' => $componentsParts,
            'otherParts' => $otherParts,
            'others' => $others,
            'finitionProcesses' => $finitionProcesses,
            'soudureProcesses' => $soudureProcesses
        ));
    }

    public function pcbSaveAction($project)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoFluxBundle:Project')->find($project);
        
        $product = $project->getProduct();

        $PCBParts = $this->getRequest()->get('pcb');

        foreach($PCBParts as $PCBPart) {
            // We check if the surface of the part is not zero
            if(isset($PCBPart['label']) && $PCBPart['label'] != '' ){
                // Try Retreiving the object if the request id exist
                if(isset($PCBPart['id']) && $PCBPart['id'] != null ){
                    $p = $em->getRepository('ArtoFluxBundle:ElectronicPcbPart')->find($PCBPart['id']);
                }
                else{
                    $p = new ElectronicPcbPart();
                }
                
                $p->setProject($project);
                $p->setLabel($PCBPart['label']);
                
                
                if($PCBPart['typePCB'] == "null"){
                    $p->setPcb(null);
                    $p->setSurfacePCB(null);
                    $p->setWeight(null);
                }else{
                    //Retrieve base to set the plasticPart base FK
                    $pcb = $em->getRepository('ArtoAcvBundle:Base')->find($PCBPart['typePCB']);
                    $p->setPcb($pcb);
                    
                    //$multiplicateur = $pcb->getMultiplicateur();

                    //Replace , by .
                    $surfacePCB = $PCBPart['surface'];
                    $surfacePCB = str_replace(',','.',$surfacePCB);
                    $p->setSurfacePCB($surfacePCB);
                    
                    $weightPCB = $PCBPart['weight'];
                    $weightPCB = str_replace(',','.',$weightPCB);
                    $p->setWeight($weightPCB);
                }
                  
                $em->persist($p);
                
            }   
        }
        
        // We flush the all the modifications at once
        $em->flush();
        
        $this->recalculateWeightElectronicSummed($product);

        return $this->redirect($this->generateUrl('flux_electronic', array('id' => $project->getId())));
    }
    
    public function pcbDeleteAction(){
        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        
        $pcbPartId = $request->get('id');
        
        $pcbPart = $em->getRepository('ArtoFluxBundle:ElectronicPcbPart')->findOneBy(
                array('id' => $pcbPartId));
        $project = $pcbPart->getProject();
        
        $em->remove($pcbPart);
        $em->flush();
        
        return $this->redirect($this->generateUrl('flux_electronic', array('id' => $project->getId())));
    }
    
    public function othersAction(){
        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        
        $otherId = $request->get('id');
        
        $other = $em->getRepository('ArtoAcvBundle:Base')->findOneBy(
                array('id' => $otherId));
        
        $param1Unit = $other->getUnit1();
        $param2Unit = $other->getUnit2();
        $unitMass = $other->getUnitMass();
        
        $result = array(
            'param1Unit' => $param1Unit,
            'param2Unit' => $param2Unit,
            'unitMass' => $unitMass
        );
        
        $json = json_encode($result);
        $response = new Response($json);
        return $response;  
        
    }
    
    public function othersSaveAction($project){
        $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoFluxBundle:Project')->find($project);
        
        $othersParts = $this->getRequest()->get('others');
        
        foreach($othersParts as $othersPart){
            if($othersPart['param1Value'] != ""){
                 // Try Retreiving the object if the request id exist
                    if (isset($othersPart['id']) && $othersPart['id'] != null) {
                        $p = $em->getRepository('ArtoFluxBundle:ElectronicOtherPart')->find($othersPart['id']);
                    } else {
                        $p = new ElectronicOtherPart();
                    }
                    
                    $p->setProject($project);
                    
                    $part = $em->getRepository('ArtoAcvBundle:Base')->find($othersPart['type']);
                    $p->setPart($part);
                    
                    //Replace , by .
                    $param1Value = $othersPart['param1Value'];
                    $param1Value = str_replace(',','.',$param1Value);
                    $p->setParam1Value($param1Value);
                    $param1Unit = $part->getUnit1();
                    $p->setParam1Unit($param1Unit);
                    
                    $param2Unit = $part->getUnit2();
                    $p->setParam2Unit($param2Unit);
                    
                    if($param2Unit != ""){
                        //Replace , by .
                        $param2Value = $othersPart['param2Value'];
                        $param2Value = str_replace(',','.',$param2Value);
                        $p->setParam2Value($param2Value);    
                    }
                          
                    $unitMass = $part->getUnitMass();
                    $p->setUnitMass($unitMass);
                    
                    $em->persist($p);
            }
        }
        
        // We flush the all the modifications at once
        $em->flush();
        
        return $this->redirect($this->generateUrl('flux_electronic', array('id' => $project->getId())));
    }
    
    public function othersDeleteAction(){
        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        
        $othersPartId = $request->get('id');
        
        $othersPart = $em->getRepository('ArtoFluxBundle:ElectronicOtherPart')->findOneBy(
                array('id' => $othersPartId));
        $project = $othersPart->getProject();
        
        $em->remove($othersPart);
        $em->flush();
        
        return $this->redirect($this->generateUrl('flux_electronic', array('id' => $project->getId())));
    }
    
    public function pcbGetTooltipAction(){
        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        
        $pcbId = $request->get('id');
        
        $pcb = $em->getRepository('ArtoAcvBundle:Base')->findOneBy(
                array('id' => $pcbId));
        $tooltip = $pcb->getUnitMass();
        
        $json = json_encode($tooltip);
        $response = new Response($json);
        return $response;  
    }

    public function componentsSaveAction($project)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoFluxBundle:Project')->find($project);
        
        $product = $project->getProduct();
        
        $componentsParts = $this->getRequest()->get('components');

        foreach($componentsParts as $componentsPart) {
            if(isset($componentsPart['param1Value']) && $componentsPart['param1Value'] != '') {
                    // Try Retreiving the object if the request id exist
                    if (isset($componentsPart['id']) && $componentsPart['id'] != '') {
                        $p = $em->getRepository('ArtoFluxBundle:ElectronicComponentPart')->find($componentsPart['id']);
                    } else {
                        $p = new ElectronicComponentPart();
                    }
                    
                    $p->setProject($project);

                    //Retrieve Component
                    $component = $em->getRepository('ArtoFluxBundle:ElectronicComponent')->find($componentsPart['component']);

                    $p->setComponent($component);
                    
                    $param1Value = $componentsPart['param1Value'];
                    $param1Value = str_replace(',','.',$param1Value);
                    $p->setParam1Value($param1Value);
                    
                    if(isset($componentsPart['param2Value'])){
                        $param2Value = $componentsPart['param2Value'];
                        $param2Value = str_replace(',','.',$param2Value);
                        $p->setParam2Value($param2Value);
                    }
                    
                    $param1Unit = $component->getParam1();
                    $param2Unit = $component->getParam2();
                    
                    if(!isset($param2Value)) {
                        $param2Value = $component->getVal();
                    } 
                        
                    if($param1Unit == 'g'){
                        $p->setWeight($param1Value);
                    }
                    
                    if($param1Unit == 'UN'){
                        if($param2Unit == 'g'){
                            $p->setWeight($param1Value * $param2Value);
                        }else{
                            $code = $component->getCode();
                            $base = $em->getRepository('ArtoAcvBundle:Base')->findOneBy(array(
                                'code' => $code
                            ));
                            
                            $multiplicateur = $base->getMultiplicateur();
                            
                            if($param2Unit != ''){
                                $p->setWeight($param1Value * $param2Value * $multiplicateur);
                            }else{
                                $p->setWeight($param1Value * $multiplicateur);
                            }
                        }
                    }else{
                        if($param1Unit == 'g'){
                            $p->setWeight($param1Value);
                        }else{
                            $code = $component->getCode();
                            $base = $em->getRepository('ArtoAcvBundle:Base')->findOneBy(array(
                                'code' => $code
                            ));
                            
                            $multiplicateur = $base->getMultiplicateur();
                            
                            if($param2Unit != ''){
                                $p->setWeight($param1Value * $param2Value * $multiplicateur);  
                            }else{
                                $p->setWeight($param1Value * $multiplicateur);
                            }
                        }
                    }
                   
                    $em->persist($p);
            }
        }

        // We flush the all the modifications at once
        $em->flush();
        
        $this->recalculateWeightElectronicSummed($product);

        return $this->redirect($this->generateUrl('flux_electronic', array('id' => $project->getId())));
    }
    
    public function componentsDeleteAction(){
        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        
        $componentsPartId = $request->get('id');
        
        $componentsPart = $em->getRepository('ArtoFluxBundle:ElectronicComponentPart')->findOneBy(
                array('id' => $componentsPartId));
        $project = $componentsPart->getProject();
        
        $em->remove($componentsPart);
        $em->flush();
        
        return $this->redirect($this->generateUrl('flux_electronic', array('id' => $project->getId())));
    }
    
    public function fixsSaveAction($project)
    {
        echo("--fixsSaveAction--");
        $em = $this->getDoctrine()->getEntityManager();

        $project = $em->getRepository('ArtoFluxBundle:Project')->find($project);

        $fixs = $this->getRequest()->get('fixs');
        echo(print_r($fixs));

        foreach($fixs as $fix) {
                    // Try Retreiving the object if the request id exist
                    if (isset($fix['id']) && $fix['id'] != null) {
                        $p = $em->getRepository('ArtoFluxBundle:ElectronicFix')->find($fix['id']);
                    } else {
                        $p = new ElectronicFix();
                    }
                    
                    //echo(print_r($fix));
                    
                    $p->setProject($project);
                    
                    $p->setSpots($fix['spots']);
                    $p->setCmsSnpb($fix['cms_snpb']);
                    $p->setCmsOther($fix['cms_other']);
                    
                    $p->setRefusionSnpb($fix['refusion_snpb']);
                    $p->setRefusionSncu($fix['refusion_sncu']);
                    $p->setRefusionSnagcu($fix['refusion_snagcu']);
                    $p->setRefusionOther($fix['refusion_other']);
                    
                    $p->setVarnish($fix['varnish']);

                    $em->persist($p);    
        }

        // We flush the all the modifications at once
        $em->flush();

        return $this->redirect($this->generateUrl('flux_electronic', array('id' => $project->getId())));
    }

    public function componentsAction()
    {
        $request = $this->getRequest();

        $em = $this->getDoctrine()->getEntityManager();

        $type = $request->get('type');
        $id = $request->get('id');

        if ($id != null) {
            $part = $em->getRepository('ArtoFluxBundle:ElectronicComponentPart')->find($id);
            $id = $part->getComponent()->getId();
        }

        $components = $em->getRepository('ArtoFluxBundle:ElectronicComponent')->findBy(
                array('type' => $type),
                array('description' => "ASC"));

        return $this->render('ArtoFluxBundle:Default:composants.html.twig', array(
            'components' => $components,
            'id' => $id
        ));
    }

    public function componentAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $id = $this->getRequest()->get('id');

        $c = $em->getRepository('ArtoFluxBundle:ElectronicComponent')->findOneBy(
                array('id' => $id));
        
        $param1 = $c->getParam1();
        $param2 = "";
        $unitMass = $c->getUnitMass();
        $val = $c->getVal();
          
        if($c->getParam2()=="mm2"){
            $param2 = "mm<sup>2</sup>";
        }
        else if($c->getParam2()=="cm3"){
            $param2 = "cm<sup>3</sup>";
        }
        else{
            $param2 = $c->getParam2();
        }

        $result = array(
            'id' => $c->getId(),
            'param1' => $param1,
            'param2' => $param2,
            'unitMass' => $unitMass,
            'val' => $val
        );

        $json = json_encode($result);
        $response = new Response($json);
        return $response;
    }
    
    public function weightSaveAction($project){
        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        $weightElectronic = $request->get('product_electronic_weight');

        $project = $em->getRepository('ArtoFluxBundle:Project')->find($project);
        
        
        $product_id = $project->getProduct()->getId();
        $product = $em->getRepository('ArtoFluxBundle:Product')->find($product_id);
        
        $product->setWeightElectronic($weightElectronic);
        
        $em->persist($product);
        $em->flush();
        
        return $this->redirect($this->generateUrl('flux_electronic', array('id' => $project->getId())));
        
    }
    
    public function recalculateWeightElectronicSummed(Product $product){
        $em = $this->getDoctrine()->getEntityManager();
        
        $weight = 0;
        
        $productId = $product->getId();
        $project = $product->getProject();
        $projectId = $project->getId();
        
        $electronicPcbParts = $em->getRepository('ArtoFluxBundle:ElectronicPcbPart')->findBy(
                array('project' => $projectId));
        
        foreach($electronicPcbParts as $electronicPcbPart ){
            $partWeight = $electronicPcbPart->getWeight();
            $weight = $weight + $partWeight;
        }
        
        $electronicComponentParts = $em->getRepository('ArtoFluxBundle:ElectronicComponentPart')->findBy(
                array('project' => $projectId));
        
        foreach($electronicComponentParts as $electronicComponentPart ){
            $partWeight = $electronicComponentPart->getWeight();
            $weight = $weight + $partWeight;
        }
        
        $product->setWeightElectronicSummed($weight);
        $em->persist($product);
        $em->flush();
    }
}