<?php

namespace Arto\FluxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Arto\GreenBundle\Controller\TokenAuthenticatedController;
use Arto\FluxBundle\Entity\Project;
use Arto\FluxBundle\Entity\Folder;
use Arto\FluxBundle\Entity\Product;
use Arto\FluxBundle\Entity\ProductPart;
use Arto\FluxBundle\Entity\User;
use Arto\FluxBundle\Entity\ManufacturingPlastic;
use Arto\FluxBundle\Entity\ManufacturingMetallic;
use Arto\FluxBundle\Entity\ManufacturingPart;
use Arto\FluxBundle\Entity\ElectronicComponent;
use Arto\FluxBundle\Entity\ElectronicType;
use Arto\FluxBundle\Entity\ElectronicComponentPart;

use Arto\AcvBundle\Entity\Base;
use Arto\AcvBundle\Entity\Type;
use Arto\AcvBundle\Entity\Family;
//use Arto\AcvBundle\Entity\Project;
//use Arto\AcvBundle\Entity\Product;
use Arto\AcvBundle\Entity\Assembly;
use Arto\AcvBundle\Entity\Part;
use DateTime;

class DefaultController extends Controller implements TokenAuthenticatedController
{
    private function dateToSQL($date) {
        $returnDate = null;

        $s=substr($date, 6, 4)
            ."-".substr($date, 3, 2)
            ."-".substr($date, 0, 2);

        try {
           $returnDate = new \DateTime($s);
        } catch (\Exception $e) {
            // Nothing to do
        }

        return $returnDate;
    }

    public function generalAction()
    {
        $siteArtogreen = $this->getRequest()->getScheme() . '://' . $this->getRequest()->getHttpHost() . $this->getRequest()->getBasePath();
        return $this->render('ArtoFluxBundle:Default:general.html.twig', array('site' => $siteArtogreen));
    }

    public function termsAction()
    {
        return $this->render('ArtoFluxBundle:Default:terms.html.twig');
    }

    public function indexAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $userId = $user->getId();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        $folderLetters = array();

        $projects = $em->getRepository('ArtoFluxBundle:Project')->findAllActive($user);
        
        //projectsWithoutFolder
        $projectsWF = $em->getRepository('ArtoFluxBundle:Project')->findBy(
                array('greenUser' => $userId, 'folder' => null), array('name' => 'ASC'));
        
        $folders = $em->getRepository('ArtoFluxBundle:Folder')->findBy(
                array('user' => $userId), array('name' => 'ASC'));
        
        foreach($folders as $folder){
            $folderName = $folder->getName();
            $firstLetter = strtoupper(substr($folderName,0,1));
            
            if(!in_array($firstLetter, $folderLetters)){
                array_push($folderLetters, $firstLetter);
            }
        }

        return $this->render('ArtoFluxBundle:Default:index.html.twig', array('projects' => $projects, 'user' => $userId, 'folders' => $folders,'projectsWF'=> $projectsWF, 'myLetter'=> '', 'folderLetters' => $folderLetters));
    }

    public function createAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();

        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        $dossierId = $request->get('dossier');

        $project = new Project();
        $project->setGreenUser($user);
        $project->setName($request->get('name'));
        $project->setNumero(0);

        $product = new Product();
        $product->setProject($project);
        $product->setName($request->get('name'));
        $product->setRef(0);
        
        if($dossierId == ''){
            $project->setFolder(null);
            $em->persist($project);
        }else{
            $dossier = $em->getRepository('ArtoFluxBundle:Folder')->find($dossierId);

            $dossier->addProject($project); 
            $project->setFolder($dossier);

            $em->persist($dossier);
            $em->persist($project);
        }

        $em->persist($project);
        $em->persist($product);
        $em->flush();

        return $this->redirect($this->generateUrl('flux_project_description', array('id' => $project->getId())));
    }

     public function deleteAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();

        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        $id = $request->get('id');
        $special = $request->get('special');

        $project = $em->getRepository('ArtoFluxBundle:Project')->find($id);
        
        $isCorrectUser = $project->getGreenUser()->getId() == $user->getId();
        $isSpecial = isset($special) && $special != null;

        if ($isCorrectUser || $isSpecial) {
            $em->remove($project);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('flux'));
    }
    
    public function renameAction(){
        $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        
        $id = $request->get('id');
        $newName = $request->get('name');

        $project = $em->getRepository('ArtoFluxBundle:Project')->find($id);
        
        $project->setName($newName);
        
        $em->persist($project);
        $em->flush();
               
        return $this->redirect($this->generateUrl('flux'));  
    }

    public function descriptionAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $request = $this->getRequest();
        $id = $request->get('id');

        $project = $em->getRepository('ArtoFluxBundle:Project')->find($id);
        $users = $em->getRepository('ArtoFluxBundle:User')->findBy(array('project' => $project->getId()));

        $product = $em->getRepository('ArtoFluxBundle:Product')->findOneBy(array('project' => $project->getId()));
        
        $operations = $em->getRepository('ArtoFluxBundle:Operation')->findBy(
                array('project' => $project->getId()));

        return $this->render('ArtoFluxBundle:Default:description.html.twig', array(
            'project' => $project,
            'users' => $users,
            'product' => $product,
            'operations' => $operations
        ));
    }

    public function descriptionSaveAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        $project_id = $request->get('project');
        $project = $em->getRepository('ArtoFluxBundle:Project')->find($project_id);

        $product_id = $project->getProduct()->getId();
        $product = $em->getRepository('ArtoFluxBundle:Product')->find($product_id);

        $product->setName($request->get('product_name'));
        $product->setRef($request->get('product_reference'));
        $product->setWeight($request->get('product_masse_totale'));
        $project->setPilote($request->get('pilote'));
        $project->setNumero($request->get('numero'));
        $project->setDescriptionuf($request->get('descriptionuf'));

        if ($request->get('pilote_date') != null) {
            $project->setPiloteDate($this->dateToSQL($request->get('pilote_date')));
        }

        $uploadedFile = $_FILES['file'];
        if (isset($uploadedFile['name'])) {
            if($uploadedFile['name'] != ''){
                move_uploaded_file($uploadedFile['tmp_name'], __DIR__.'/../../../../web/uploads/flux/'.$uploadedFile['name']);
                $project->setPath($uploadedFile['name']);
            }
        }

        $em->persist($project);
        $em->persist($product);
        $em->flush();

        return $this->redirect($this->generateUrl('flux_project_description', array(
            'id' => $project->getId())
        ));
    }
    
    public function reglementationSaveAction(){
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        $project_id = $request->get('project');
        $project = $em->getRepository('ArtoFluxBundle:Project')->find($project_id);

        $product_id = $project->getProduct()->getId();
        $product = $em->getRepository('ArtoFluxBundle:Product')->find($product_id);
        
        $product->setRohs($request->get('rohs'));
        $product->setReach($request->get('reach'));
        $product->setDeee($request->get('deee'));
        $product->setBattery($request->get('battery'));
        $product->setBatteryExtract($request->get('battery_extract'));
              
        $em->persist($project);
        $em->persist($product);
        $em->flush();

        return $this->redirect($this->generateUrl('flux_project_description', array(
            'id' => $project->getId())
        ));
    }
    
    public function normalisationSaveAction(){
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        $project_id = $request->get('project');
        $project = $em->getRepository('ArtoFluxBundle:Project')->find($project_id);

        $product_id = $project->getProduct()->getId();
        $product = $em->getRepository('ArtoFluxBundle:Product')->find($product_id);
        
        $product->setProductStandard($request->get('standard'));
        
        $em->persist($project);
        $em->persist($product);
        $em->flush();

        return $this->redirect($this->generateUrl('flux_project_description', array(
            'id' => $project->getId())
        ));
    }
    
    public function productSaveAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        $project = $request->get('project');
        $project = $em->getRepository('ArtoFluxBundle:Project')->find($project);

        $products = $em->getRepository('ArtoFluxBundle:Product')->findBy(array('project' => $project->getId()));
        $product = $products[0];

        $total_weight = 0;

        foreach($product->getParts() as $part) {
            $id = $part->getId();
            $part->setType($request->get('type_'.$id));
            $part->setRef($request->get('ref_'.$id));
            $part->setName($request->get('name_'.$id));
            $part->setQuantity($request->get('quantity_'.$id));
            //Replace , by .
            $weight = $request->get('weight_'.$id);
            $weight = str_replace(',','.',$weight);
            $part->setWeight($weight);
            $total_weight += ($part->getQuantity() * $part->getWeight());
            $em->persist($part);
        }

        if ($request->get('ref') != null && $request->get('name') != null && $request->get('weight') != null) {
            $part = new ProductPart();
            $part->setProduct($product);
            $part->setType($request->get('type'));
            $part->setRef($request->get('ref'));
            $part->setName($request->get('name'));
            $part->setQuantity($request->get('quantity'));
            //Replace , by .
            $weight = $request->get('weight');
            $weight = str_replace(',','.',$weight);
            $part->setWeight($weight);
            $total_weight += ($part->getQuantity() * $part->getWeight());
            $em->persist($part);
        }

        $product->setWeight($total_weight);
        $em->persist($product);
        $em->flush();

        return $this->redirect($this->generateUrl('flux_project_description', array('id' => $project->getId())));
    }

    public function userSaveAction($id)
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();
        $project = $em->getRepository('ArtoFluxBundle:Project')->find($id);
        $users = $em->getRepository('ArtoFluxBundle:User')->findBy(array('project' => $project->getId()));

        foreach($users as $user) {
            $user->setRole($request->get('user_role_'.$user->getId()));
            $user->setCompany($request->get('user_company_'.$user->getId()));
            $user->setFirstname($request->get('user_firstname_'.$user->getId()));
            $user->setLastname($request->get('user_lastname_'.$user->getId()));
            $user->setEmail($request->get('user_email_'.$user->getId()));
            $user->setTel($request->get('user_tel_'.$user->getId()));
            $em->persist($user);
        }

        if ($request->get('user_role') != null &&
            $request->get('user_company') != null &&
            $request->get('user_firstname') != null &&
            $request->get('user_lastname') != null) {

            $user = new User();
            $user->setProject($project);
            $user->setRole($request->get('user_role'));
            $user->setCompany($request->get('user_company'));
            $user->setFirstname($request->get('user_firstname'));
            $user->setLastname($request->get('user_lastname'));
            $user->setTel($request->get('user_tel'));
            $user->setEmail($request->get('user_email'));
            $em->persist($user);
        }
        $em->flush();

        return $this->redirect($this->generateUrl('flux_project_description', array('id' => $id)));
    }

    public function importAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $line = 2;
        if (($handle = fopen("/Users/nay/Clients/Artogreen/flux.csv", "r")) !== FALSE) {

            while (($row = fgetcsv($handle, 0, ";")) !== FALSE) {
                $type = $em->getRepository('ArtoFluxBundle:ElectronicType')->findOneByLabel(trim($row[0]));
                if ($type == null) {
                    $type = new ElectronicType();
                    $type->setLabel(trim($row[0]));
                    $em->persist($type);
                    $em->flush();
                }
            }

            fclose($handle);
        }

        if (($handle = fopen("/Users/nay/Clients/Artogreen/flux.csv", "r")) !== FALSE) {
            while (($row = fgetcsv($handle, 0, ";")) !== FALSE) {
                //$num = count($data);
                //echo "<p> $num champs à la ligne $row: <br /></p>\n";
                //$base = $em->getRepository('ArtoAcvBundle:Base')->findOneByCode(trim($row[3]));
                //if ($base == null) {
                    //$base = new Base();
                //}

                $c = new ElectronicComponent();

                $type = $em->getRepository('ArtoFluxBundle:ElectronicType')->findOneByLabel(trim($row[0]));
                $c->setType($type);

                $c->setDescription($row[1]);
                $c->setWeight($row[2]);
                $c->setSurface($row[3]);
                $c->setVal($row[4]);
                $c->setParam1($row[5]);
                $c->setParam2($row[6]);
                $c->setUnitMass($row[7]);
                $c->setCode($row[8]);

                $em->persist($c);
                $em->flush();

                $line++;
            }
            fclose($handle);
        }

        exit();
    }
    
    public function exportAction($id = null){
        $user = $this->get('security.context')->getToken()->getUser();
        $userId = $user->getId();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        $fluxUser = $em->getRepository('ArtoGreenBundle:User')->findOneBy(
                array('id' => $userId));
        
        $acvUserId = $fluxUser->getAcvUserId();
        
        $acvProjects = $em->getRepository('ArtoAcvBundle:Project')->findBy(
                array('greenUser' => $acvUserId, 'parent' => null), array('name' => 'ASC'));

        $project = $em->getRepository('ArtoFluxBundle:Project')->find($id);
        
        return $this->render('ArtoFluxBundle:Default:export.html.twig', array(
            'project' => $project, 'acvProjects' => $acvProjects));
    }
    
    public function createFluxAssembly($projectId, $acvProjectId){
        $em = $this->getDoctrine()->getEntityManager();
        
        $user = $this->get('security.context')->getToken()->getUser();
        $userId = $user->getId();
        
        $fluxUser = $em->getRepository('ArtoGreenBundle:User')->findOneBy(
                array('id' => $userId));
 
        $project = $em->getRepository('ArtoFluxBundle:Project')->find($projectId);
        
        $projectName = $project->getName();
        
        $product = $project->getProduct();
        $productRef = $product->getRef();
        
        $fluxUserName = $fluxUser->getFirstname();
        
        $projectArtoAcv = $em->getRepository('ArtoAcvBundle:Project')->findOneBy(
                array('id' => $acvProjectId));
        
        $productArtoAcv = $em->getRepository('ArtoAcvBundle:Product')->findOneBy(
                array('project' => $projectArtoAcv->getId()));
        
        if($productArtoAcv != null){
            $assembly = new Assembly();
            $assemblyName = 'ArtoFlux-'.$fluxUserName.'-'.$projectName.'-'.$productRef;
            $existingAssembly = $em->getRepository('ArtoAcvBundle:Assembly')->findOneBy(
                    array('product' => $productArtoAcv->getId(), 'name' => $assemblyName));
            
            if($existingAssembly != null){
                $em->remove($existingAssembly);
                $em->flush();
            }
            
            $assembly->setName($assemblyName);
            $assembly->setProduct($productArtoAcv);
            $assembly->setQuantity(1);
            
            $em->persist($assembly);
            $em->flush();
        }
    }
    
    public function exportPlasticParts($projectId, $assembly){
        $em = $this->getDoctrine()->getEntityManager();
        
        $plasticParts = $em->getRepository('ArtoFluxBundle:ManufacturingPlastic')->findBy(
                array('project' => $projectId));
        
        //Additifs - Colorants - Charges
        foreach ($plasticParts as $plasticPart) {
            $plasticPartName = $plasticPart->getLabel();
            $plasticPartQty = $plasticPart->getQuantity();
            $plasticPartWeight = $plasticPart->getWeight();
            $plasticPartBase = $plasticPart->getBase();
            $plasticPartProcess = $plasticPart->getProcess();

            $plasticPartCharge = $plasticPart->getCharge();
            $plasticPartPercentCharge = $plasticPart->getPercentCharge();
            $plasticPartAdditive = $plasticPart->getAdditive();
            $plasticPartPercentAdditive = $plasticPart->getPercentAdditive();
            $plasticPartColorant = $plasticPart->getColorant();
            $plasticPartPercentColorant = $plasticPart->getPercentColorant();
            
            $plasticPartPercentRemaining = 100 - ($plasticPartPercentCharge + $plasticPartPercentAdditive + $plasticPartPercentColorant);

            $acvMainPart = $em->getRepository("ArtoAcvBundle:Part")->findOneBy(
                    array('assembly' => $assembly->getId(), 'name' => $plasticPartName));

            if ($acvMainPart != null) {
                //La part
                $acvPart = new Part();
                $acvPart->setMaterial($plasticPartBase);
                $acvPart->setAssembly($assembly);
                $acvPart->setParent($acvMainPart);
                $acvPart->setParam1(($plasticPartWeight * $plasticPartPercentRemaining) / 100);
               
                if ($plasticPartProcess != null){
                    $acvPart->setProcess($plasticPartProcess);
                    $acvPart->setParam3($plasticPartWeight);
                }

                $em->persist($acvPart);
                $em->flush();

                //Les additifs / charges / colorants
                if ($plasticPartAdditive != null) {
                    $acvPart = new Part();
                    $acvPart->setMaterial($plasticPartAdditive);
                    $acvPart->setAssembly($assembly);
                    $acvPart->setParent($acvMainPart);
                    //$acvPart->setProcess($processInjectionThermoplastique);
                    $acvPart->setParam1(($plasticPartWeight * $plasticPartPercentAdditive) / 100);
                    //$acvPart->setParam3(($plasticPartWeight * $plasticPartPercentAdditive) / 100);

                    $em->persist($acvPart);
                    $em->flush();
                }

                if ($plasticPartCharge != null) {
                    $acvPart = new Part();
                    $acvPart->setMaterial($plasticPartCharge);
                    $acvPart->setAssembly($assembly);
                    $acvPart->setParent($acvMainPart);
                    //$acvPart->setProcess($processInjectionThermoplastique);
                    $acvPart->setParam1(($plasticPartWeight * $plasticPartPercentCharge) / 100);
                    //$acvPart->setParam3(($plasticPartWeight * $plasticPartPercentCharge) / 100);

                    $em->persist($acvPart);
                    $em->flush();
                }

                if ($plasticPartColorant != null) {
                    $acvPart = new Part();
                    $acvPart->setMaterial($plasticPartColorant);
                    $acvPart->setAssembly($assembly);
                    $acvPart->setParent($acvMainPart);
                    //$acvPart->setProcess($processInjectionThermoplastique);
                    $acvPart->setParam1(($plasticPartWeight * $plasticPartPercentColorant) / 100);
                    //$acvPart->setParam3(($plasticPartWeight * $plasticPartPercentColorant) / 100);

                    $em->persist($acvPart);
                    $em->flush();
                }
            } else {
                //Si elle existe pas on crée la Main Part
                $acvPart = new Part();

                $acvPart->setAssembly($assembly);
                $acvPart->setName($plasticPartName);
                $acvPart->setQuantity($plasticPartQty);

                $em->persist($acvPart);
                $em->flush();

                $acvMainPart = $em->getRepository("ArtoAcvBundle:Part")->findOneBy(
                        array('assembly' => $assembly->getId(), 'name' => $plasticPartName));

                //La part
                $acvPart = new Part();
                $acvPart->setMaterial($plasticPartBase);
                $acvPart->setAssembly($assembly);
                $acvPart->setParent($acvMainPart);
                $acvPart->setParam1(($plasticPartWeight * $plasticPartPercentRemaining) / 100);
                
                if ($plasticPartProcess != null){
                    $acvPart->setProcess($plasticPartProcess);
                    $acvPart->setParam3($plasticPartWeight);
                }

                $em->persist($acvPart);
                $em->flush();

                //Les additifs / charges / colorants
                if ($plasticPartAdditive != null) {
                    $acvPart = new Part();
                    $acvPart->setMaterial($plasticPartAdditive);
                    $acvPart->setAssembly($assembly);
                    $acvPart->setParent($acvMainPart);
                    //$acvPart->setProcess($processInjectionThermoplastique);
                    $acvPart->setParam1(($plasticPartWeight * $plasticPartPercentAdditive) / 100);
                    //$acvPart->setParam3(($plasticPartWeight * $plasticPartPercentAdditive) / 100);

                    $em->persist($acvPart);
                    $em->flush();
                }

                if ($plasticPartCharge != null) {
                    $acvPart = new Part();
                    $acvPart->setMaterial($plasticPartCharge);
                    $acvPart->setAssembly($assembly);
                    $acvPart->setParent($acvMainPart);
                    //$acvPart->setProcess($processInjectionThermoplastique);
                    $acvPart->setParam1(($plasticPartWeight * $plasticPartPercentCharge) / 100);
                    //$acvPart->setParam3(($plasticPartWeight * $plasticPartPercentCharge) / 100);

                    $em->persist($acvPart);
                    $em->flush();
                }

                if ($plasticPartColorant != null) {
                    $acvPart = new Part();
                    $acvPart->setMaterial($plasticPartColorant);
                    $acvPart->setAssembly($assembly);
                    $acvPart->setParent($acvMainPart);
                    //$acvPart->setProcess($processInjectionThermoplastique);
                    $acvPart->setParam1(($plasticPartWeight * $plasticPartPercentColorant) / 100);
                    //$acvPart->setParam3(($plasticPartWeight * $plasticPartPercentColorant) / 100);

                    $em->persist($acvPart);
                    $em->flush();
                }
            }
        }
    }
    
    public function exportMetallicParts($projectId, $assembly) {
        $em = $this->getDoctrine()->getEntityManager();

        $metallicParts = $em->getRepository('ArtoFluxBundle:ManufacturingMetallic')->findBy(
                array('project' => $projectId));

        foreach ($metallicParts as $metallicPart) {
            $metallicPartName = $metallicPart->getLabel();
            $metallicPartQty = $metallicPart->getQuantity();
            $metallicPartWeight = $metallicPart->getWeight();
            $metallicPartBase = $metallicPart->getBase();

            $metallicPartProcess = $metallicPart->getProcess();
            $metallicPartProcessFinition = $metallicPart->getProcessFinition();
            $metallicPartProcessSurface = $metallicPart->getProcessSurface();

            $acvMainPart = $em->getRepository("ArtoAcvBundle:Part")->findOneBy(
                    array('assembly' => $assembly->getId(), 'name' => $metallicPartName));

            if ($acvMainPart != null) {
                //La part
                $acvPart = new Part();
                $acvPart->setMaterial($metallicPartBase);
                $acvPart->setAssembly($assembly);
                $acvPart->setParent($acvMainPart);    
                $acvPart->setParam1($metallicPartWeight);
                
                if ($metallicPartProcess != null){
                    $acvPart->setProcess($metallicPartProcess);
                    
                    if ($metallicPartProcess->getUnit1() == 'g') {
                        $acvPart->setParam3($metallicPartWeight);
                    }
                    if ($metallicPartProcess->getUnit1() == 'cm2') {
                        $acvPart->setParam3($metallicPartProcessSurface);
                    }
                }
               

                $em->persist($acvPart);
                $em->flush();

                //Les procédés de finition
                if ($metallicPartProcessFinition != null) {
                    $acvPart = new Part();
                    $acvPart->setProcess($metallicPartProcessFinition);
                    $acvPart->setParam3($metallicPartProcessSurface);
                    $acvPart->setParent($acvMainPart);
                    $em->persist($acvPart);
                    $em->flush();
                }
            } else {
                //Si elle existe pas on crée la Main Part
                $acvPart = new Part();

                $acvPart->setAssembly($assembly);
                $acvPart->setName($metallicPartName);
                $acvPart->setQuantity($metallicPartQty);

                $em->persist($acvPart);
                $em->flush();

                $acvMainPart = $em->getRepository("ArtoAcvBundle:Part")->findOneBy(
                        array('assembly' => $assembly->getId(), 'name' => $metallicPartName));

                //La part
                $acvPart = new Part();
                $acvPart->setMaterial($metallicPartBase);
                $acvPart->setAssembly($assembly);
                $acvPart->setParent($acvMainPart);
                $acvPart->setParam1($metallicPartWeight);
                
                if ($metallicPartProcess != null){
                    $acvPart->setProcess($metallicPartProcess);
                    
                    if ($metallicPartProcess->getUnit1() == 'g') {
                        $acvPart->setParam3($metallicPartWeight);
                    }
                    if ($metallicPartProcess->getUnit1() == 'cm2') {
                        $acvPart->setParam3($metallicPartProcessSurface);
                    }
                }
               
                $em->persist($acvPart);
                $em->flush();

                //Les procédés de finition
                if ($metallicPartProcessFinition != null) {
                    $acvPart = new Part();
                    $acvPart->setProcess($metallicPartProcessFinition);
                    $acvPart->setParam3($metallicPartProcessSurface);
                    $acvPart->setParent($acvMainPart);
                    $em->persist($acvPart);
                    $em->flush();
                }
            }
        }
    }
    
    public function exportPcbParts($projectId, $assembly) {
        $em = $this->getDoctrine()->getEntityManager();
        
        $pcbParts = $em->getRepository('ArtoFluxBundle:ElectronicPcbPart')->findBy(
                array('project' => $projectId));
        
        foreach ($pcbParts as $pcbPart){
            $pcbPartName = $pcbPart->getLabel();
            $pcbPartType = $pcbPart->getPcb();
            $pcbPartSurface = $pcbPart->getSurfacePCB();
            $pcbPartWeight = $pcbPart->getWeight();
            /*$pcbPartSurfaceMask = $pcbPart->getSurfaceMask();
            $pcbPartSurfacePassivation = $pcbPart->getSurfacePassivation();
            $pcbPartProcessFinition = $pcbPart->getFinition();
            $pcbPartSurfaceFinition = $pcbPart->getSurfaceFinition();
            $pcbPartSurfaceCollage = $pcbPart->getSurfaceCollage();
            $pcbPartProcessSoudure = $pcbPart->getSoudure();
            $pcbPartNbPointsSoudure = $pcbPart->getNbPointsSoudure();
            
            $processPcbMask = $em->getRepository("ArtoAcvBundle:Base")->findOneBy(
                    array('code' => 'CODDE-0146'));
            
            $processPcbPassivation = $em->getRepository("ArtoAcvBundle:Base")->findOneBy(
                    array('code' => 'ECO-153-'));
            
            $processPcbCollage = $em->getRepository("ArtoAcvBundle:Base")->findOneBy(
                    array('code' => 'ECO-088-'));*/
            
            $acvMainPart = $em->getRepository("ArtoAcvBundle:Part")->findOneBy(
                    array('assembly' => $assembly->getId(), 'name' => $pcbPartName));
            
            if($acvMainPart != null){
                //La part
                $acvPart = new Part();
                $acvPart->setMaterial($pcbPartType);
                $acvPart->setAssembly($assembly);
                $acvPart->setParent($acvMainPart);    
                $acvPart->setParam1($pcbPartWeight);
                
                /*
                //Masque de soudure ?
                if($pcbPartSurfaceMask != 0){
                    $acvPart = new Part();
                    $acvPart->setProcess($processPcbMask);
                    $acvPart->setParam3($pcbPartSurfaceMask);
                    $acvPart->setParent($acvMainPart);
                    $em->persist($acvPart);
                    $em->flush();
                }
                
                //Passivation ?
                if($pcbPartSurfacePassivation != 0){
                    $acvPart = new Part();
                    $acvPart->setProcess($processPcbPassivation);
                    $acvPart->setParam3($pcbPartSurfacePassivation);
                    $acvPart->setParent($acvMainPart);
                    $em->persist($acvPart);
                    $em->flush();
                }
                
                //Finition ?
                if($pcbPartProcessFinition != null){
                    $acvPart = new Part();
                    $acvPart->setProcess($pcbPartProcessFinition);
                    $acvPart->setParam3($pcbPartSurfaceFinition);
                    $acvPart->setParent($acvMainPart);
                    $em->persist($acvPart);
                    $em->flush();
                }
                
                //Collage ?
                if($pcbPartSurfaceCollage != 0){
                    $acvPart = new Part();
                    $acvPart->setProcess($processPcbCollage);
                    $acvPart->setParam3($pcbPartSurfaceCollage);
                    $acvPart->setParent($acvMainPart);
                    $em->persist($acvPart);
                    $em->flush();
                }
                
                //Soudure ?
                if($pcbPartNbPointsSoudure != 0){
                    $acvPart = new Part();
                    $acvPart->setProcess($pcbPartProcessSoudure);
                    $acvPart->setParam3($pcbPartNbPointsSoudure);
                    $acvPart->setParent($acvMainPart);
                    $em->persist($acvPart);
                    $em->flush();
                }*/
            }else{
                //Si elle existe pas on crée la Main Part
                $acvPart = new Part();

                $acvPart->setAssembly($assembly);
                $acvPart->setName($pcbPartName);
                $acvPart->setQuantity(1);
                
                $em->persist($acvPart);
                $em->flush();
                
                $acvMainPart = $em->getRepository("ArtoAcvBundle:Part")->findOneBy(
                        array('assembly' => $assembly->getId(), 'name' => $pcbPartName));
                
                //La part
                $acvPart = new Part();
                $acvPart->setMaterial($pcbPartType);
                $acvPart->setAssembly($assembly);    
                $acvPart->setParam1($pcbPartWeight);
                $acvPart->setParent($acvMainPart);
                        
                $em->persist($acvPart);
                $em->flush();
                
                /*
                //Finition ?
                if($pcbPartProcessFinition != null){
                    $acvPart->setProcess($pcbPartProcessFinition);
                    $acvPart->setParam3($pcbPartSurfaceFinition);
                    $acvPart->setParent($acvMainPart);
                    $em->persist($acvPart);
                    $em->flush();
                }
                
                //Masque de soudure ?
                if($pcbPartSurfaceMask != 0){
                    $acvPart = new Part();
                    $acvPart->setProcess($processPcbMask);
                    $acvPart->setParam3($pcbPartSurfaceMask);
                    $acvPart->setParent($acvMainPart);
                    $em->persist($acvPart);
                    $em->flush();
                }
                
                //Passivation ?
                if($pcbPartSurfacePassivation != 0){
                    $acvPart = new Part();
                    $acvPart->setProcess($processPcbPassivation);
                    $acvPart->setParam3($pcbPartSurfacePassivation);
                    $acvPart->setParent($acvMainPart);
                    $em->persist($acvPart);
                    $em->flush();
                }
                
                //Collage ?
                if($pcbPartSurfaceCollage != 0){
                    $acvPart = new Part();
                    $acvPart->setProcess($processPcbCollage);
                    $acvPart->setParam3($pcbPartSurfaceCollage);
                    $acvPart->setParent($acvMainPart);
                    $em->persist($acvPart);
                    $em->flush();
                }
                
                //Soudure ?
                if($pcbPartNbPointsSoudure != 0){
                    $acvPart = new Part();
                    $acvPart->setProcess($pcbPartProcessSoudure);
                    $acvPart->setParam3($pcbPartNbPointsSoudure);
                    $acvPart->setParent($acvMainPart);
                    $em->persist($acvPart);
                    $em->flush();
                }*/
                
            }
            
        }   
    }
    
    public function exportElectronicParts($projectId, $assembly) {
        $em = $this->getDoctrine()->getEntityManager();

        $electronicParts = $em->getRepository('ArtoFluxBundle:ElectronicComponentPart')->findBy(
                array('project' => $projectId));

        foreach ($electronicParts as $electronicPart) {
            $electronicPartParam1Val = $electronicPart->getParam1Value();
            $electronicPartParam2Val = $electronicPart->getParam2Value();
            $electronicPartName = $electronicPart->getComponent()->getDescription();
            $electronicPartCode = $electronicPart->getComponent()->getCode();

            $electronicPartBase = $em->getRepository("ArtoAcvBundle:Base")->findOneBy(
                    array('code' => $electronicPartCode));

            $electronicPartBaseMultiplicateur = $electronicPartBase->getMultiplicateur();
            $electronicPartBaseValParam2ByUN = $electronicPartBase->getValParam2ByUN();

            $electronicPartParam1Unit = $electronicPart->getComponent()->getParam1();
            $electronicPartParam2Unit = $electronicPart->getComponent()->getParam2();

            $acvMainPart = $em->getRepository("ArtoAcvBundle:Part")->findOneBy(
                    array('assembly' => $assembly->getId(), 'name' => $electronicPartName));

            if ($acvMainPart != null) {
                //La part
                $acvPart = new Part();
                $acvPart->setMaterial($electronicPartBase);
                $acvPart->setAssembly($assembly);
                $acvPart->setParent($acvMainPart);

                //UN en Param1
                if ($electronicPartParam1Unit == 'UN') {
                    if ($electronicPartParam2Unit == 'g') {
                        $acvPart->setParam1($electronicPartParam2Val / $electronicPartBaseMultiplicateur);
                        $acvPart->setParam2($electronicPartBaseMultiplicateur);
                    }elseif ($electronicPartParam2Unit != '' && $electronicPartParam2Unit != 'g'){
                        $acvPart->setParam1($electronicPartParam2Val / $electronicPartBaseValParam2ByUN);
                        $acvPart->setParam2($electronicPartBaseValParam2ByUN);
                    }
                    else {
                        $acvPart->setParam1($electronicPartParam1Val);
                        $acvPart->setParam2(null);
                    }
                }
                // UN pas en Param1
                else {
                    //UN en Param2
                    if ($electronicPartParam2Unit == 'UN') {
                        if ($electronicPartParam1Unit == 'g') {
                            $acvPart->setParam2($electronicPartParam1Val / $electronicPartBaseMultiplicateur);
                            $acvPart->setParam1($electronicPartBaseMultiplicateur);
                        } else {
                            $acvPart->setParam2($electronicPartParam1Val / $electronicPartBaseValParam2ByUN);
                            $acvPart->setParam1($electronicPartBaseValParam2ByUN);
                        }
                    }
                    //Pas d'UN du tout donc g seulement
                    else {
                        $acvPart->setParam1($electronicPartParam1Val);
                        $acvPart->setParam2(null);
                    }
                }

                $em->persist($acvPart);
                $em->flush();
            } else {
                //Si elle existe pas on crée la Main Part    
                //UN en Param1
                if ($electronicPartParam1Unit == 'UN') {
                    $electronicPartQty = $electronicPart->getParam1Value();
                }
                // UN pas en Param1
                else {
                    //UN en Param2
                    if ($electronicPartParam2Unit == 'UN') {
                        $electronicPartQty = $electronicPart->getParam2Value();
                    }
                    //Pas d'UN du tout donc g seulement
                    else {
                        $electronicPartQty = 1;
                    }
                }

                $electronicPartName = $electronicPart->getComponent()->getDescription();

                $acvPart = new Part();

                $acvPart->setAssembly($assembly);
                $acvPart->setName($electronicPartName);
                $acvPart->setQuantity($electronicPartQty);

                $em->persist($acvPart);
                $em->flush();

                $acvMainPart = $em->getRepository("ArtoAcvBundle:Part")->findOneBy(
                        array('assembly' => $assembly->getId(), 'name' => $electronicPartName));

                //La part
                $acvPart = new Part();
                $acvPart->setMaterial($electronicPartBase);
                $acvPart->setAssembly($assembly);
                $acvPart->setParent($acvMainPart);

                $electronicPartParam1Unit = $electronicPart->getComponent()->getParam1();
                $electronicPartParam2Unit = $electronicPart->getComponent()->getParam2();

                //UN en Param1
                if ($electronicPartParam1Unit == 'UN') {
                    if ($electronicPartParam2Unit == 'g') {
                        $acvPart->setParam1($electronicPartParam2Val / $electronicPartBaseMultiplicateur);
                        $acvPart->setParam2($electronicPartBaseMultiplicateur);
                    } elseif ($electronicPartParam2Unit != '' && $electronicPartParam2Unit != 'g') {
                        $acvPart->setParam1($electronicPartParam2Val / $electronicPartBaseValParam2ByUN);
                        $acvPart->setParam2($electronicPartBaseValParam2ByUN);
                    } else{
                        $acvPart->setParam1($electronicPartParam1Val);
                        $acvPart->setParam2(null);
                    }
                }
                // UN pas en Param1
                else {
                    //UN en Param2
                    if ($electronicPartParam2Unit == 'UN') {
                        if ($electronicPartParam1Unit == 'g') {
                            $acvPart->setParam2($electronicPartParam1Val / $electronicPartBaseMultiplicateur);
                            $acvPart->setParam1($electronicPartBaseMultiplicateur);
                        } else {
                            $acvPart->setParam2($electronicPartParam1Val / $electronicPartBaseValParam2ByUN);
                            $acvPart->setParam1($electronicPartBaseValParam2ByUN);
                        }
                    }
                    //Pas d'UN du tout donc g seulement
                    else {
                        $acvPart->setParam1($electronicPartParam1Val);
                        $acvPart->setParam2(null);
                    }
                }

                $em->persist($acvPart);
                $em->flush();
            }
        }
    }

    public function exportToAcvAction(){
        $em = $this->getDoctrine()->getEntityManager();
        
        $request = $this->getRequest();
        
        //Retrieve acvProjects
        $user = $this->get('security.context')->getToken()->getUser();
        $userId = $user->getId();
        
        $fluxUser = $em->getRepository('ArtoGreenBundle:User')->findOneBy(
                array('id' => $userId));
        
        $fluxUserName = $fluxUser->getFirstname();
        
        
        $acvUserId = $fluxUser->getAcvUserId();
        
        $acvProjects = $em->getRepository('ArtoAcvBundle:Project')->findBy(
                array('greenUser' => $acvUserId, 'parent' => null), array('name' => 'ASC'));
  
        $projectId = $request->get('projectId');
        $acvProjectId = $request->get('acvProject');
        
        $this->createFluxAssembly($projectId, $acvProjectId);
        
        $project = $em->getRepository('ArtoFluxBundle:Project')->find($projectId);
        
        $product = $project->getProduct();
        $productRef = $product->getRef();
        
        $fluxProjectName = $project->getName();
        
        //$date = date('d/m/Y');
        $dateTime = new DateTime('NOW');
        
        $project->setExportedAt($dateTime);
        
        $em->persist($project);
        $em->flush();
        
        $projectArtoAcv = $em->getRepository('ArtoAcvBundle:Project')->findOneBy(
                array('id' => $acvProjectId));
      
        $productArtoAcv = $em->getRepository('ArtoAcvBundle:Product')->findOneBy(
                array('project' => $projectArtoAcv->getId()));
        
        $projectArtoAcvName = $projectArtoAcv->getName();
        
        if($productArtoAcv != null){
            $assemblyName = 'ArtoFlux-'.$fluxUserName.'-'.$fluxProjectName.'-'.$productRef;
            $assembly = $em->getRepository('ArtoAcvBundle:Assembly')->findOneBy(
                    array('product' => $productArtoAcv->getId(), 'name' => $assemblyName));
              
            $this->exportPlasticParts($projectId, $assembly);
            
            $this->exportMetallicParts($projectId, $assembly);
            
            $this->exportElectronicParts($projectId, $assembly);
            
            $this->exportPcbParts($projectId, $assembly);
             
        }
        
        return $this->render('ArtoFluxBundle:Default:export.html.twig', array(
            'project' => $project, 'acvProjects' => $acvProjects, 'exportOK' => 'OK', 'acvProjectId' => $acvProjectId, 'acvProjectName' => $projectArtoAcvName
        ));
    }
    
    public function exportToExcelAction(){
        
    }
}

