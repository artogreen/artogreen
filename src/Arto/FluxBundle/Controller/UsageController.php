<?php

namespace Arto\FluxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Arto\AcvBundle\Entity\Base;
use Arto\FluxBundle\Entity\Project;
use Arto\FluxBundle\Entity\Product;
use Arto\AcvBundle\Entity\Assembly;
use Arto\AcvBundle\Entity\Family;
use Arto\AcvBundle\Entity\Type;
use Arto\FluxBundle\Entity\Usage;
use Arto\FluxBundle\Entity\UsagePart;

class UsageController extends Controller
{
    public function indexAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $project = $em->getRepository('ArtoFluxBundle:Project')->find($id);
        echo($project->getId());
        $product = $em->getRepository('ArtoFluxBundle:Product')->findOneBy(
                array('project' => $project->getId()));

        $type = $em->getRepository('ArtoAcvBundle:Type')->findOneByName('process');
        $family = $em->getRepository('ArtoAcvBundle:Family')->findOneByName('énergie');

        $energies = $em->getRepository('ArtoAcvBundle:Base')->findBy(
            array('type' => $type->getId(),'family' => $family->getId()),
            array('name' => 'ASC')
        );

        $usage = $product->getUsage();

        $parts1 = array();
        $parts2 = array();

        if ($usage == null) {
            $usage = new Usage();
        } else {
            $parts1 = $em->getRepository('ArtoFluxBundle:UsagePart')->findBy(array(
                'category' => 1,
                'usage' => $product->getUsage()->getId()
            ));
            $parts2 = $em->getRepository('ArtoFluxBundle:UsagePart')->findBy(array(
                'category' => 2,
                'usage' => $product->getUsage()->getId()
            ));
        }

        return $this->render('ArtoFluxBundle:Default:usage.html.twig', array(
            'project' => $project,
            'product' => $product,
            'usage'   => $usage,
            'energies' => $energies,
            'parts1' => $parts1,
            'parts2' => $parts2,
        ));
    }

    public function partAction($id, $category, $key)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $part = $em->getRepository('ArtoFluxBundle:UsagePart')->find($id);

        $types = $em->getRepository('ArtoAcvBundle:Type')->findAll();
        $families = $em->getRepository('ArtoAcvBundle:Family')->findAll();

        $batteries = array();
        $bases = array();
        if ($category == 1) {
            $query = $em->createQuery('SELECT b FROM ArtoAcvBundle:Base b WHERE b.name LIKE :term ORDER BY b.name ASC')
                        ->setParameter('term', 'batterie%');
            $batteries = $query->getResult();
        } else {
            $batteries = $em->getRepository('ArtoAcvBundle:Base')->findBy(array(
            'type' => $part->getBase()->getType()->getId(),
            'family' => $part->getBase()->getFamily()->getId(),
            ));
        }

        return $this->render('ArtoFluxBundle:Default:usage_part.html.twig', array(
            'types' => $types,
            'families' => $families,
            'part' => $part,
            'key' => $key,
            'batteries' => $batteries,
            'category' => $category
        ));
    }

    public function partNewAction($category, $key)
    {
        $key = 1000;
        $em = $this->getDoctrine()->getEntityManager();

        $types = $em->getRepository('ArtoAcvBundle:Type')->findAll();

        $batteries = null;
        if ($category == 1) {
            $query = $em->createQuery('SELECT b FROM ArtoAcvBundle:Base b WHERE b.name LIKE :term ORDER BY b.name ASC')
                        ->setParameter('term', 'batterie%');
            $batteries = $query->getResult();
        }

        return $this->render('ArtoFluxBundle:Default:usage_part_new.html.twig', array(
            'types' => $types,
            'category' => $category,
            'batteries' => $batteries,
            'key' => $key
        ));
    }

    public function saveAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        
        $project = $em->getRepository('ArtoFluxBundle:Project')->find($id);

        //$product = $request->get('product');
        //$product = $em->getRepository('ArtoFluxBundle:Product')->find($id);
        $product = $em->getRepository('ArtoFluxBundle:Product')->findOneBy(
                array('project' => $project->getId()));

        $usage = $product->getUsage();

        if ($usage == null) {
            $usage = new Usage();
            $usage->setProduct($product);
        }

        $usage->setEnergyType($request->get('energy_type'));

        $usage->setOnKw($request->get('on_kw'));
        $usage->setOnPercent($request->get('on_percent'));

        $usage->setOffKw($request->get('off_kw'));
        $usage->setOffPercent($request->get('off_percent'));

        $usage->setStandByActiveKw($request->get('standby_active_kw'));
        $usage->setStandByActivePercent($request->get('standby_active_percent'));

        $usage->setStandByPassiveKw($request->get('standby_passive_kw'));
        $usage->setStandByPassivePercent($request->get('standby_passive_percent'));

        $usage->setLifetime($request->get('lifetime'));

        $usage->setContinuous($request->get('continuous'));

        $energy = $em->getRepository('ArtoAcvBundle:Base')->find($request->get('energy'));
        if ($energy != null) {
            $usage->setEnergy($energy);
        }

        $em->persist($usage);
        $em->flush();

        return $this->redirect($this->generateUrl('flux_usage', array(
            'id' => $project->getId()
        )));
    }

    public function partSaveAction($id, $category)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        
        $project = $em->getRepository('ArtoFluxBundle:Project')->find($id);
        echo($project->getId());
        $product = $em->getRepository('ArtoFluxBundle:Product')->findOneBy(
                array('project' => $project->getId()));

        $usage = $product->getUsage();

        $parts = $request->get('part');

        foreach($parts as $key => $part) {

            if ($key != 1000 && $part['base'] != null && $part['quantity'] != null && $part['param1Value'] != null) {
                if (isset($part['id'])) {
                    $p = $em->getRepository('ArtoFluxBundle:UsagePart')->find($part['id']);
                } else {
                    $p = new UsagePart();
                    $p->setUsage($usage);
                }

                $p->setCategory($category);
                $p->setQuantity($part['quantity']);
                
                //Replace , by .
                $val = $part['param1Value'];
                $val = str_replace(',','.',$val);
                $p->setVal($val);

                $base = $em->getRepository('ArtoAcvBundle:Base')->find($part['base']);
                if ($base != null) {
                    $p->setBase($base);
                }

                $em->persist($p);
            }

        }

        $em->flush();

        return $this->redirect($this->generateUrl('flux_usage', array(
            'id' => $project->getId()  
        )));
    }

    public function partDeleteAction($id, $part)
    {
        $em = $this->getDoctrine()->getEntityManager();
        
        $project = $em->getRepository('ArtoFluxBundle:Project')->find($id);
      
        $p = $em->getRepository('ArtoFluxBundle:UsagePart')->find($part);
        $em->remove($p);

        $em->flush();

        return $this->redirect($this->generateUrl('flux_usage', array(
            'id' => $project->getId(),
        )));
    }
}
