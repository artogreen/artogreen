<?php
namespace Arto\FluxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="flux_usage")
 */
class Usage
{
    const ENERGY_ACTIVE = 1;
    const ENERGY_PASSIVE = 2;
    const ENERGY_OTHER = 3;

    const RANGE_YEAR = "Y";
    const RANGE_DAY = "D";
    const RANGE_MIN = "M";

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="energy_type", type="integer", nullable=false)
     */
    private $energyType;

    /**
     * @ORM\Column(name="on_kw", type="decimal", nullable=true, scale=3)
     */
    private $onKw;

    /**
     * @ORM\Column(name="on_precent", type="decimal", nullable=false, scale=3)
     */
    private $onPercent;

    /**
     * @ORM\Column(name="off_kw", type="decimal", nullable=true, scale=3)
     */
    private $offKw;

    /**
     * @ORM\Column(name="off_percent", type="decimal", nullable=true, scale=3)
     */
    private $offPercent;

    /**
     * @ORM\Column(name="standby_active_kw", type="decimal", nullable=true, scale=3)
     */
    private $standbyActiveKw;

    /**
     * @ORM\Column(name="standby_active_percent", type="decimal", nullable=true, scale=3)
     */
    private $standbyActivePercent;

    /**
     * @ORM\Column(name="standby_passive_kw", type="decimal", nullable=true, scale=3)
     */
    private $standbyPassiveKw;

    /**
     * @ORM\Column(name="standby_passive_percent", type="decimal", nullable=true, scale=3)
     */
    private $standbyPassivePercent;

    /**
     * @ORM\Column(name="lifetime", type="integer", nullable=true)
     */
    private $lifetime;

    /**
     * @ORM\Column(name="continuous", type="integer", nullable=true)
     */
    private $continuous;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Arto\AcvBundle\Entity\Base")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="energy_id", referencedColumnName="id")
     * })
     */
    private $energy;

    /**
     * @ORM\OneToOne(targetEntity="Product", inversedBy="usage")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;

    /**
     * @ORM\OneToMany(targetEntity="UsagePart", mappedBy="usage", cascade={"remove"})
     */
    private $parts;


    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getName();
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set energyType
     *
     * @param integer $energyType
     */
    public function setEnergyType($energyType)
    {
        $this->energyType = $energyType;
    }

    /**
     * Get energyType
     *
     * @return integer
     */
    public function getEnergyType()
    {
        return $this->energyType;
    }

    /**
     * Set onKw
     *
     * @param integer $onKw
     */
    public function setOnKw($onKw)
    {
        $this->onKw = $onKw;
    }

    /**
     * Get onKw
     *
     * @return integer
     */
    public function getOnKw()
    {
        return $this->onKw;
    }

    /**
     * Set onPercent
     *
     * @param integer $onPercent
     */
    public function setOnPercent($onPercent)
    {
        $this->onPercent = $onPercent;
    }

    /**
     * Get onPercent
     *
     * @return integer
     */
    public function getOnPercent()
    {
        return $this->onPercent;
    }

    /**
     * Set offKw
     *
     * @param integer $offKw
     */
    public function setOffKw($offKw)
    {
        $this->offKw = $offKw;
    }

    /**
     * Get offKw
     *
     * @return integer
     */
    public function getOffKw()
    {
        return $this->offKw;
    }

    /**
     * Set offPercent
     *
     * @param integer $offPercent
     */
    public function setOffPercent($offPercent)
    {
        $this->offPercent = $offPercent;
    }

    /**
     * Get offPercent
     *
     * @return integer
     */
    public function getOffPercent()
    {
        return $this->offPercent;
    }

    /**
     * Set standbyActiveKw
     *
     * @param integer $standbyActiveKw
     */
    public function setStandbyActiveKw($standbyActiveKw)
    {
        $this->standbyActiveKw = $standbyActiveKw;
    }

    /**
     * Get standbyActiveKw
     *
     * @return integer
     */
    public function getStandbyActiveKw()
    {
        return $this->standbyActiveKw;
    }

    /**
     * Set standbyActivePercent
     *
     * @param integer $standbyActivePercent
     */
    public function setStandbyActivePercent($standbyActivePercent)
    {
        $this->standbyActivePercent = $standbyActivePercent;
    }

    /**
     * Get standbyActivePercent
     *
     * @return integer
     */
    public function getStandbyActivePercent()
    {
        return $this->standbyActivePercent;
    }

    /**
     * Set standbyPassiveKw
     *
     * @param integer $standbyPassiveKw
     */
    public function setStandbyPassiveKw($standbyPassiveKw)
    {
        $this->standbyPassiveKw = $standbyPassiveKw;
    }

    /**
     * Get standbyPassiveKw
     *
     * @return integer
     */
    public function getStandbyPassiveKw()
    {
        return $this->standbyPassiveKw;
    }

    /**
     * Set standbyPassivePercent
     *
     * @param integer $standbyPassivePercent
     */
    public function setStandbyPassivePercent($standbyPassivePercent)
    {
        $this->standbyPassivePercent = $standbyPassivePercent;
    }

    /**
     * Get standbyPassivePercent
     *
     * @return integer
     */
    public function getStandbyPassivePercent()
    {
        return $this->standbyPassivePercent;
    }

    /**
     * Set lifetime
     *
     * @param integer $lifetime
     */
    public function setLifetime($lifetime)
    {
        $this->lifetime = $lifetime;
    }

    /**
     * Get lifetime
     *
     * @return integer
     */
    public function getLifetime()
    {
        return $this->lifetime;
    }

    /**
     * Set continuous
     *
     * @param boolean $continuous
     */
    public function setContinuous($continuous)
    {
        $this->continuous = $continuous;
    }

    /**
     * Get continuous
     *
     * @return boolean
     */
    public function getContinuous()
    {
        return $this->continuous;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set energy
     *
     * @param Arto\AcvBundle\Entity\Base $energy
     */
    public function setEnergy(\Arto\AcvBundle\Entity\Base $energy)
    {
        $this->energy = $energy;
    }

    /**
     * Get energy
     *
     * @return Arto\AcvBundle\Entity\Base
     */
    public function getEnergy()
    {
        return $this->energy;
    }

    /**
     * Set product
     *
     * @param Product $product
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Add parts
     *
     * @param UsagePart $parts
     */
    public function addUsagePart(UsagePart $parts)
    {
        $this->parts[] = $parts;
    }

    /**
     * Get parts
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getParts()
    {
        return $this->parts;
    }
}