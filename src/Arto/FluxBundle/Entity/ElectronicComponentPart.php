<?php
namespace Arto\FluxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="flux_electronic_component_part")
 */
class ElectronicComponentPart
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="param1Value", type="decimal", scale=2, nullable=false)
     */
    private $param1Value;

     /**
     * @ORM\Column(name="param2Value", type="decimal", scale=2, nullable=true)
     */
    private $param2Value;
    
    /**
     * @ORM\Column(name="weight", type="decimal", scale=2, nullable=true)
     */
    private $weight;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="electronicComponentParts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;

    /**
     * @var ElectronicComponent
     *
     * @ORM\ManyToOne(targetEntity="ElectronicComponent", inversedBy="parts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="component_id", referencedColumnName="id")
     * })
     */
    private $component;

    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getId();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set param1Value
     *
     * @param integer $param1Value
     */
    public function setParam1Value($param1Value)
    {
        $this->param1Value = $param1Value;
    }

    /**
     * Get param1Value
     *
     * @return integer 
     */
    public function getParam1Value()
    {
        return $this->param1Value;
    }

    /**
     * Set param2Value
     *
     * @param integer $param2Value
     */
    public function setParam2Value($param2Value)
    {
        $this->param2Value = $param2Value;
    }

    /**
     * Get param2Value
     *
     * @return integer 
     */
    public function getParam2Value()
    {
        return $this->param2Value;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set project
     *
     * @param Arto\FluxBundle\Entity\Project $project
     */
    public function setProject(\Arto\FluxBundle\Entity\Project $project)
    {
        $this->project = $project;
    }

    /**
     * Get project
     *
     * @return Arto\FluxBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set component
     *
     * @param Arto\FluxBundle\Entity\ElectronicComponent $component
     */
    public function setComponent(\Arto\FluxBundle\Entity\ElectronicComponent $component)
    {
        $this->component = $component;
    }

    /**
     * Get component
     *
     * @return Arto\FluxBundle\Entity\ElectronicComponent 
     */
    public function getComponent()
    {
        return $this->component;
    }
    
    /**
     * Set weight
     *
     * @param decimal $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * Get weight
     *
     * @return decimal 
     */
    public function getWeight()
    {
        return $this->weight;
    }
}