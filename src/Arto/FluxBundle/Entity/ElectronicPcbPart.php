<?php
namespace Arto\FluxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="flux_electronic_pcb_part")
 */
class ElectronicPcbPart
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @ORM\Column(name="label", type="string", length=255, nullable=false)
     */
    private $label;
    
    /**
     * @ORM\Column(name="weight", type="decimal", scale=2, nullable=true)
     */
    private $weight;
    
    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="electronicPcbParts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;
    
    /**
     * PCB
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Arto\AcvBundle\Entity\Base")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pcb_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $pcb;
    
    /**
     * @ORM\Column(name="surfacePCB", type="decimal", nullable=true, scale=2)
     */
    private $surfacePCB;
    
    /**
     * Procédés de passivation
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Arto\AcvBundle\Entity\Base")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="passivation_id", referencedColumnName="id")
     * })
     */
    private $passivation;
    
    /**
     * @ORM\Column(name="surfacePassivation", type="decimal", nullable=true, scale=2)
     */
    private $surfacePassivation;
    
    /**
     * Procédés de vernis/dépôt masque de soudure
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Arto\AcvBundle\Entity\Base")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mask_id", referencedColumnName="id")
     * })
     */
    private $mask;
    
    /**
     * @ORM\Column(name="surfaceMask", type="decimal", nullable=true, scale=2)
     */
    private $surfaceMask;

    /**
     * Procédés de finition
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Arto\AcvBundle\Entity\Base")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="finition_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $finition;
    
    /**
     * @ORM\Column(name="surfaceFinition", type="decimal", nullable=true, scale=2)
     */
    private $surfaceFinition;
    
    /**
     * Procédés de collage
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Arto\AcvBundle\Entity\Base")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="collage_id", referencedColumnName="id")
     * })
     */
    private $collage;
    
    /**
     * @ORM\Column(name="surfaceCollage", type="decimal", nullable=true, scale=2)
     */
    private $surfaceCollage;
    
    /**
     * Procédés de soudure
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Arto\AcvBundle\Entity\Base")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="soudure_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $soudure;
    
    /**
     * @ORM\Column(name="nbPointsSoudure", type="integer", nullable=true, scale=2)
     */
    private $nbPointsSoudure;
     
    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getId();
    }

    

  

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set surfacePCB
     *
     * @param decimal $surfacePCB
     */
    public function setSurfacePCB($surfacePCB = null)
    {
        $this->surfacePCB = $surfacePCB;
    }

    /**
     * Get surfacePCB
     *
     * @return decimal 
     */
    public function getSurfacePCB()
    {
        return $this->surfacePCB;
    }

    /**
     * Set surfacePassivation
     *
     * @param decimal $surfacePassivation
     */
    public function setSurfacePassivation($surfacePassivation)
    {
        $this->surfacePassivation = $surfacePassivation;
    }

    /**
     * Get surfacePassivation
     *
     * @return decimal 
     */
    public function getSurfacePassivation()
    {
        return $this->surfacePassivation;
    }

    /**
     * Set surfaceMask
     *
     * @param decimal $surfaceMask
     */
    public function setSurfaceMask($surfaceMask)
    {
        $this->surfaceMask = $surfaceMask;
    }

    /**
     * Get surfaceMask
     *
     * @return decimal 
     */
    public function getSurfaceMask()
    {
        return $this->surfaceMask;
    }

    /**
     * Set surfaceFinition
     *
     * @param decimal $surfaceFinition
     */
    public function setSurfaceFinition($surfaceFinition = null)
    {
        $this->surfaceFinition = $surfaceFinition;
    }

    /**
     * Get surfaceFinition
     *
     * @return decimal 
     */
    public function getSurfaceFinition()
    {
        return $this->surfaceFinition;
    }

    /**
     * Set surfaceCollage
     *
     * @param decimal $surfaceCollage
     */
    public function setSurfaceCollage($surfaceCollage)
    {
        $this->surfaceCollage = $surfaceCollage;
    }

    /**
     * Get surfaceCollage
     *
     * @return decimal 
     */
    public function getSurfaceCollage()
    {
        return $this->surfaceCollage;
    }

    /**
     * Set nbPointsSoudure
     *
     * @param integer $nbPointsSoudure
     */
    public function setNbPointsSoudure($nbPointsSoudure = null)
    {
        $this->nbPointsSoudure = $nbPointsSoudure;
    }

    /**
     * Get nbPointsSoudure
     *
     * @return integer 
     */
    public function getNbPointsSoudure()
    {
        return $this->nbPointsSoudure;
    }

    /**
     * Set project
     *
     * @param Arto\FluxBundle\Entity\Project $project
     */
    public function setProject(\Arto\FluxBundle\Entity\Project $project)
    {
        $this->project = $project;
    }

    /**
     * Get project
     *
     * @return Arto\FluxBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set pcb
     *
     * @param Arto\AcvBundle\Entity\Base $pcb
     */
    public function setPcb(\Arto\AcvBundle\Entity\Base $pcb = null)
    {
        $this->pcb = $pcb;
    }

    /**
     * Get pcb
     *
     * @return Arto\AcvBundle\Entity\Base 
     */
    public function getPcb()
    {
        return $this->pcb;
    }

    /**
     * Set passivation
     *
     * @param Arto\AcvBundle\Entity\Base $passivation
     */
    public function setPassivation(\Arto\AcvBundle\Entity\Base $passivation)
    {
        $this->passivation = $passivation;
    }

    /**
     * Get passivation
     *
     * @return Arto\AcvBundle\Entity\Base 
     */
    public function getPassivation()
    {
        return $this->passivation;
    }

    /**
     * Set mask
     *
     * @param Arto\AcvBundle\Entity\Base $mask
     */
    public function setMask(\Arto\AcvBundle\Entity\Base $mask)
    {
        $this->mask = $mask;
    }

    /**
     * Get mask
     *
     * @return Arto\AcvBundle\Entity\Base 
     */
    public function getMask()
    {
        return $this->mask;
    }

    /**
     * Set finition
     *
     * @param Arto\AcvBundle\Entity\Base $finition
     */
    public function setFinition(\Arto\AcvBundle\Entity\Base $finition = null)
    {
        $this->finition = $finition;
    }

    /**
     * Get finition
     *
     * @return Arto\AcvBundle\Entity\Base 
     */
    public function getFinition()
    {
        return $this->finition;
    }

    /**
     * Set collage
     *
     * @param Arto\AcvBundle\Entity\Base $collage
     */
    public function setCollage(\Arto\AcvBundle\Entity\Base $collage)
    {
        $this->collage = $collage;
    }

    /**
     * Get collage
     *
     * @return Arto\AcvBundle\Entity\Base 
     */
    public function getCollage()
    {
        return $this->collage;
    }

    /**
     * Set soudure
     *
     * @param Arto\AcvBundle\Entity\Base $soudure
     */
    public function setSoudure(\Arto\AcvBundle\Entity\Base $soudure = null)
    {
        $this->soudure = $soudure;
    }

    /**
     * Get soudure
     *
     * @return Arto\AcvBundle\Entity\Base 
     */
    public function getSoudure()
    {
        return $this->soudure;
    }
    
    /**
     * Set weight
     *
     * @param decimal $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * Get weight
     *
     * @return decimal 
     */
    public function getWeight()
    {
        return $this->weight;
    }
}