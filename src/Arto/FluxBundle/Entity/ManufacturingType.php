<?php
namespace Arto\FluxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="flux_manufacturing_type")
 */
class ManufacturingType
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="label", type="string", length=50, nullable=false)
     */
    private $label;

    /**
     * @var ManufacturingCategory
     *
     * @ORM\ManyToOne(targetEntity="ManufacturingCategory", inversedBy="types")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;


    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getLabel();
    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set label
     *
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set category
     *
     * @param Arto\FluxBundle\Entity\ManufacturingCategory $category
     */
    public function setCategory(\Arto\FluxBundle\Entity\ManufacturingCategory $category)
    {
        $this->category = $category;
    }

    /**
     * Get category
     *
     * @return Arto\FluxBundle\Entity\ManufacturingCategory
     */
    public function getCategory()
    {
        return $this->category;
    }
}