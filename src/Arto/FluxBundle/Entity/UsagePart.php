<?php
namespace Arto\FluxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="flux_usage_part")
 */
class UsagePart
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="quantity", type="integer", nullable=false)
     */
    private $quantity;

    /**
     * @ORM\Column(name="val", type="decimal", nullable=false)
     */
    private $val;

    /**
     * @ORM\Column(name="category", type="integer", nullable=false)
     */
    private $category;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Arto\AcvBundle\Entity\Base")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="base_id", referencedColumnName="id")
     * })
     */
    private $base;

    /**
     * @var Usage
     *
     * @ORM\ManyToOne(targetEntity="Usage", inversedBy="parts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usage_id", referencedColumnName="id")
     * })
     */
    private $usage;


    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getName();
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set quantity
     *
     * @param integer $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set val
     *
     * @param decimal $val
     */
    public function setVal($val)
    {
        $this->val = $val;
    }

    /**
     * Get val
     *
     * @return decimal
     */
    public function getVal()
    {
        return $this->val;
    }

    /**
     * Set category
     *
     * @param integer $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * Get category
     *
     * @return integer
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set base
     *
     * @param Arto\AcvBundle\Entity\Base $base
     */
    public function setBase(\Arto\AcvBundle\Entity\Base $base)
    {
        $this->base = $base;
    }

    /**
     * Get base
     *
     * @return Arto\AcvBundle\Entity\Base
     */
    public function getBase()
    {
        return $this->base;
    }


    /**
     * Set usage
     *
     * @param Usage $usage
     */
    public function setUsage(Usage $usage)
    {
        $this->usage = $usage;
    }

    /**
     * Get usage
     *
     * @return Usage
     */
    public function getUsage()
    {
        return $this->usage;
    }
}