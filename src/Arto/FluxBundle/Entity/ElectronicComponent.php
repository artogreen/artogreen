<?php
namespace Arto\FluxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="flux_electronic_component")
 */
class ElectronicComponent
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @ORM\Column(name="val", type="decimal", scale=4, nullable=true)
     */
    private $val;

    /**
     * @ORM\Column(name="param1", type="string", length=5, nullable=true)
     */
    private $param1;

    /**
     * @ORM\Column(name="param2", type="string", length=5, nullable=true)
     */
    private $param2;

    /**
     * @ORM\Column(name="unitmass", type="string", length=100, nullable=false)
     */
    private $unitmass;

    /**
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;

    /**
     * @var ElectronicType
     *
     * @ORM\ManyToOne(targetEntity="ElectronicType", inversedBy="parts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     * })
     */
    private $type;


    public function __construct()
    {

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set weight
     *
     * @param decimal $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * Get weight
     *
     * @return decimal
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set surface
     *
     * @param decimal $surface
     */
    public function setSurface($surface)
    {
        $this->surface = $surface;
    }

    /**
     * Get surface
     *
     * @return decimal
     */
    public function getSurface()
    {
        return $this->surface;
    }

    /**
     * Set val
     *
     * @param decimal $val
     */
    public function setVal($val)
    {
        $this->val = $val;
    }

    /**
     * Get val
     *
     * @return decimal
     */
    public function getVal()
    {
        return $this->val;
    }

    /**
     * Set param1
     *
     * @param string $param1
     */
    public function setParam1($param1)
    {
        $this->param1 = $param1;
    }

    /**
     * Get param1
     *
     * @return string
     */
    public function getParam1()
    {
        return $this->param1;
    }

    /**
     * Set param2
     *
     * @param string $param2
     */
    public function setParam2($param2)
    {
        $this->param2 = $param2;
    }

    /**
     * Get param2
     *
     * @return string
     */
    public function getParam2()
    {
        return $this->param2;
    }

    /**
     * Set unitmass
     *
     * @param string $unitmass
     */
    public function setUnitmass($unitmass)
    {
        $this->unitmass = $unitmass;
    }

    /**
     * Get unitmass
     *
     * @return string
     */
    public function getUnitmass()
    {
        return $this->unitmass;
    }

    /**
     * Set code
     *
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set type
     *
     * @param Arto\FluxBundle\Entity\ElectronicType $type
     */
    public function setType(\Arto\FluxBundle\Entity\ElectronicType $type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return Arto\FluxBundle\Entity\ElectronicType
     */
    public function getType()
    {
        return $this->type;
    }
}