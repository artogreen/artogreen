<?php
namespace Arto\FluxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="flux_product")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="ref", type="string", length=100, nullable=true)
     */
    private $ref;

    /**
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(name="weight", type="decimal", nullable=true, scale=2)
     */
    private $weight;
    
    /**
     * @ORM\Column(name="weightElectronic", type="decimal", nullable=true, scale=2)
     */
    private $weightElectronic;
    
    /**
     * @ORM\Column(name="weightElectronicSummed", type="decimal", nullable=true, scale=2)
     */
    private $weightElectronicSummed;
    
     /**
     * Gamme de produit
     *
     * @ORM\Column(name="product_line", type="boolean", nullable=true)
     */
    private $productLine;

    /**
     * Unité fonctionnelle du produit
     *
     * @ORM\Column(name="functional_unit", type="text", nullable=true)
     */
    private $functionalUnit;


    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
    
    /**
     * ROHS
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $rohs;

    /**
     * ROHS, Lesquelles ?
     *
     * @ORM\Column(name="rohs_detail", type="boolean", type="string", length=255, nullable=true)
     */
    private $rohsDetail;

    /**
     * DEEE
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $deee;

    /**
     * Pile ou Accu ?
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $battery;

    /**
     * Pile extractible ?
     *
     * @ORM\Column(name="battery_extract", type="boolean", nullable=true)
     */
    private $batteryExtract;

    /**
     * REACH
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $reach;

    /**
     * REACH, Lesquelles ?
     *
     * @ORM\Column(name="reach_detail", type="boolean", type="string", length=255, nullable=true)
     */
    private $reachDetail;

    /**
     * Normalisation produit
     *
     * @ORM\Column(name="product_standard", type="boolean", nullable=true)
     */
    private $productStandard;

    /**
     * Normalisation produit, Laquelle ?
     *
     * @ORM\Column(name="product_standard_detail", type="boolean", type="string", length=255, nullable=true)
     */
    private $productStandardDetail;
    
    /**
     * @ORM\OneToMany(targetEntity="ProductPart", mappedBy="product", cascade={"remove","persist"})
     */
    private $parts;


    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;

  
    /**
     * @ORM\OneToOne(targetEntity="Usage", mappedBy="product", cascade={"remove"})
     */
    private $usage;

    /**
     * @ORM\OneToOne(targetEntity="Delivery", mappedBy="product", cascade={"remove"})
     */
    private $delivery;

    public function __construct()
    {
        $this->weight = 0;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ref
     *
     * @param string $ref
     */
    public function setRef($ref)
    {
        $this->ref = $ref;
    }

    /**
     * Get ref
     *
     * @return string 
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set weight
     *
     * @param decimal $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * Get weight
     *
     * @return decimal 
     */
    public function getWeight()
    {
        return $this->weight;
    }
    
    /**
     * Set weightElectronic
     * 
     * @param decimal weightElectronic
     */
    public function setWeightElectronic($weightElectronic)
    {
        $this->weightElectronic = $weightElectronic;
    }
    
    /**
     * Get weightElectronic
     * 
     * @return decimal
     */
    public function getWeightElectronic()
    {
        return $this->weightElectronic;
    }
    
    /**
     * Set weightElectronicSummed
     * 
     * @param decimal weightElectronicSummed
     */
    public function setWeightElectronicSummed($weightElectronicSummed)
    {
        $this->weightElectronicSummed = $weightElectronicSummed;
    }
    
    /**
     * Get weightElectronicSummed
     * 
     * @return decimal
     */
    public function getWeightElectronicSummed()
    {
        return $this->weightElectronicSummed;
    }

    /**
     * Set productLine
     *
     * @param boolean $productLine
     */
    public function setProductLine($productLine)
    {
        $this->productLine = $productLine;
    }

    /**
     * Get productLine
     *
     * @return boolean 
     */
    public function getProductLine()
    {
        return $this->productLine;
    }

    /**
     * Set functionalUnit
     *
     * @param text $functionalUnit
     */
    public function setFunctionalUnit($functionalUnit)
    {
        $this->functionalUnit = $functionalUnit;
    }

    /**
     * Get functionalUnit
     *
     * @return text 
     */
    public function getFunctionalUnit()
    {
        return $this->functionalUnit;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set rohs
     *
     * @param boolean $rohs
     */
    public function setRohs($rohs)
    {
        $this->rohs = $rohs;
    }

    /**
     * Get rohs
     *
     * @return boolean 
     */
    public function getRohs()
    {
        return $this->rohs;
    }

    /**
     * Set rohsDetail
     *
     * @param string $rohsDetail
     */
    public function setRohsDetail($rohsDetail)
    {
        $this->rohsDetail = $rohsDetail;
    }

    /**
     * Get rohsDetail
     *
     * @return string 
     */
    public function getRohsDetail()
    {
        return $this->rohsDetail;
    }

    /**
     * Set deee
     *
     * @param boolean $deee
     */
    public function setDeee($deee)
    {
        $this->deee = $deee;
    }

    /**
     * Get deee
     *
     * @return boolean 
     */
    public function getDeee()
    {
        return $this->deee;
    }

    /**
     * Set battery
     *
     * @param boolean $battery
     */
    public function setBattery($battery)
    {
        $this->battery = $battery;
    }

    /**
     * Get battery
     *
     * @return boolean 
     */
    public function getBattery()
    {
        return $this->battery;
    }

    /**
     * Set batteryExtract
     *
     * @param boolean $batteryExtract
     */
    public function setBatteryExtract($batteryExtract)
    {
        $this->batteryExtract = $batteryExtract;
    }

    /**
     * Get batteryExtract
     *
     * @return boolean 
     */
    public function getBatteryExtract()
    {
        return $this->batteryExtract;
    }

    /**
     * Set reach
     *
     * @param boolean $reach
     */
    public function setReach($reach)
    {
        $this->reach = $reach;
    }

    /**
     * Get reach
     *
     * @return boolean 
     */
    public function getReach()
    {
        return $this->reach;
    }

    /**
     * Set reachDetail
     *
     * @param string $reachDetail
     */
    public function setReachDetail($reachDetail)
    {
        $this->reachDetail = $reachDetail;
    }

    /**
     * Get reachDetail
     *
     * @return string 
     */
    public function getReachDetail()
    {
        return $this->reachDetail;
    }

    /**
     * Set productStandard
     *
     * @param boolean $productStandard
     */
    public function setProductStandard($productStandard)
    {
        $this->productStandard = $productStandard;
    }

    /**
     * Get productStandard
     *
     * @return boolean 
     */
    public function getProductStandard()
    {
        return $this->productStandard;
    }

    /**
     * Set productStandardDetail
     *
     * @param string $productStandardDetail
     */
    public function setProductStandardDetail($productStandardDetail)
    {
        $this->productStandardDetail = $productStandardDetail;
    }

    /**
     * Get productStandardDetail
     *
     * @return string 
     */
    public function getProductStandardDetail()
    {
        return $this->productStandardDetail;
    }

    /**
     * Set project
     *
     * @param Arto\FluxBundle\Entity\Project $project
     */
    public function setProject(\Arto\FluxBundle\Entity\Project $project)
    {
        $this->project = $project;
    }

    /**
     * Get project
     *
     * @return Arto\FluxBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set usage
     *
     * @param Arto\FluxBundle\Entity\Usage $usage
     */
    public function setUsage(\Arto\FluxBundle\Entity\Usage $usage)
    {
        $this->usage = $usage;
    }

    /**
     * Get usage
     *
     * @return Arto\FluxBundle\Entity\Usage 
     */
    public function getUsage()
    {
        return $this->usage;
    }

    /**
     * Set delivery
     *
     * @param Arto\FluxBundle\Entity\Delivery $delivery
     */
    public function setDelivery(\Arto\FluxBundle\Entity\Delivery $delivery)
    {
        $this->delivery = $delivery;
    }

    /**
     * Get delivery
     *
     * @return Arto\FluxBundle\Entity\Delivery 
     */
    public function getDelivery()
    {
        return $this->delivery;
    }

    /**
     * Add parts
     *
     * @param Arto\FluxBundle\Entity\ProductPart $parts
     */
    public function addProductPart(\Arto\FluxBundle\Entity\ProductPart $parts)
    {
        $this->parts[] = $parts;
    }

    /**
     * Get parts
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getParts()
    {
        return $this->parts;
    }
}