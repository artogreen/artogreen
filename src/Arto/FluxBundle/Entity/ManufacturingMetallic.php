<?php
namespace Arto\FluxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="flux_manufacturing_metallic")
 */
class ManufacturingMetallic
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="label", type="string", length=100, nullable=false)
     */
    private $label;

    /**
     * Valeur pour Poids/Volume/Longueur
     *
     * @ORM\Column(name="quantity", type="decimal", nullable=true, scale=2)
     */
    private $quantity;

    /**
     * Poids (g)
     *
     * @ORM\Column(name="weight", type="decimal", nullable=true, scale=2)
     */
    private $weight;

    /**
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * Surface globale du procédé de finition
     *
     * @ORM\Column(name="process_surface", type="integer", nullable=true)
     */
    private $processSurface;
    
    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="manufacturingMetallics")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;

    /**
     * Type de matière métallique
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Arto\AcvBundle\Entity\Base", inversedBy="metallic")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="base_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $base;

    /**
     * Procédés de fabrication
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Arto\AcvBundle\Entity\Base", inversedBy="metallic")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="process_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $process;
    
     /**
     * Procédés de finition
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Arto\AcvBundle\Entity\Base", inversedBy="metallic")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="processFinition_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $processFinition;


    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getLabel();
    }

   

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set quantity
     *
     * @param decimal $quantity
     */
    public function setQuantity($quantity = null)
    {
        $this->quantity = $quantity;
    }

    /**
     * Get quantity
     *
     * @return decimal 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set weight
     *
     * @param decimal $weight
     */
    public function setWeight($weight = null)
    {
        $this->weight = $weight;
    }

    /**
     * Get weight
     *
     * @return decimal 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set comment
     *
     * @param string $comment
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set processSurface
     *
     * @param integer $processSurface
     */
    public function setProcessSurface($processSurface = null)
    {
        $this->processSurface = $processSurface;
    }

    /**
     * Get processSurface
     *
     * @return integer 
     */
    public function getProcessSurface()
    {
        return $this->processSurface;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set project
     *
     * @param Arto\FluxBundle\Entity\Project $project
     */
    public function setProject(\Arto\FluxBundle\Entity\Project $project)
    {
        $this->project = $project;
    }

    /**
     * Get project
     *
     * @return Arto\FluxBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set base
     *
     * @param Arto\AcvBundle\Entity\Base $base
     */
    public function setBase(\Arto\AcvBundle\Entity\Base $base = null)
    {
        $this->base = $base;
    }

    /**
     * Get base
     *
     * @return Arto\AcvBundle\Entity\Base 
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * Set process
     *
     * @param Arto\AcvBundle\Entity\Base $process
     */
    public function setProcess(\Arto\AcvBundle\Entity\Base $process = null)
    {
        $this->process = $process;
    }

    /**
     * Get process
     *
     * @return Arto\AcvBundle\Entity\Base 
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * Set processFinition
     *
     * @param Arto\AcvBundle\Entity\Base $processFinition
     */
    public function setProcessFinition(\Arto\AcvBundle\Entity\Base $processFinition = null)
    {
        $this->processFinition = $processFinition;
    }

    /**
     * Get processFinition
     *
     * @return Arto\AcvBundle\Entity\Base 
     */
    public function getProcessFinition()
    {
        return $this->processFinition;
    }
}