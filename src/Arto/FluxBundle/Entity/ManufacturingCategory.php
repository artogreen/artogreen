<?php
namespace Arto\FluxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="flux_manufacturing_category")
 */
class ManufacturingCategory
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="label", type="string", length=50, nullable=false)
     */
    private $label;

    /**
     * label Volume (cm3) / Poids (g) / Surface (cm2) / Longueur (m)
     *
     * @ORM\Column(name="unit", type="string", length=20, nullable=false)
     */
    private $unit;

    /**
     * @ORM\OneToMany(targetEntity="ManufacturingPart", mappedBy="category", cascade={"remove"})
     */
    private $parts;

    /**
     * @ORM\OneToMany(targetEntity="ManufacturingType", mappedBy="category", cascade={"remove"})
     */
    private $types;


    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getLabel();
    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set label
     *
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set unit
     *
     * @param string $unit
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
    }

    /**
     * Get unit
     *
     * @return string 
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Add parts
     *
     * @param Arto\FluxBundle\Entity\ManufacturingPart $parts
     */
    public function addManufacturingPart(\Arto\FluxBundle\Entity\ManufacturingPart $parts)
    {
        $this->parts[] = $parts;
    }

    /**
     * Get parts
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getParts()
    {
        return $this->parts;
    }

    /**
     * Add types
     *
     * @param Arto\FluxBundle\Entity\ManufacturingType $types
     */
    public function addManufacturingType(\Arto\FluxBundle\Entity\ManufacturingType $types)
    {
        $this->types[] = $types;
    }

    /**
     * Get types
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getTypes()
    {
        return $this->types;
    }
}