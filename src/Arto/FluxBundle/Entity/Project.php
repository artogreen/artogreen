<?php
namespace Arto\FluxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Arto\FluxBundle\Repository\ProjectRepository")
 * @ORM\Table(name="flux_project")
 * @ORM\HasLifecycleCallbacks
 */
class Project
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="uniq", type="string", length=32, nullable=false)
     */
    private $uniq;

    /**
     * @ORM\Column(name="name", type="string", length=32, nullable=false)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(name="numero", type="integer", nullable=true)
     */
    private $numero;
    
    /**
     * @ORM\Column(name="descriptionuf", type="text", nullable=true)
     */
    private $descriptionuf;

    /**
     * @ORM\Column(name="pilote", type="string", length=255, nullable=true)
     */
    private $pilote;

    /**
     * @ORM\Column(name="pilote_date", type="date", nullable=true)
     */
    private $piloteDate;

    /**
     * @var string $path
     *
     * @ORM\Column(name="path", type="string", length=100, nullable=true)
     */
    private $path;

    /**
     * @Assert\File(maxSize="10000000")
     */
    public $file;

    /**
     * to get the image path (slug + extension)
     */
    private $image;

    /**
     * a property used temporarily while deleting
     */
    private $filenameForRemove;

    /**
     * @var string $path
     *
     * @ORM\Column(name="schema_path", type="string", length=100, nullable=true)
     */
    private $schemaPath;

    /**
     * @Assert\File(maxSize="10000000")
     */
    public $schemaFile;

    /**
     * to get the image path (slug + extension)
     */
    private $schemaImage;

    /**
     * a property used temporarily while deleting
     */
    private $schemaFilenameForRemove;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
    
    /**
     * @ORM\Column(name="exported_at", type="datetime", nullable=true)
     */
    private $exportedAt;
    
     /**
     * @var Folder
     *
     * @ORM\ManyToOne(targetEntity="Folder", inversedBy="projects")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="folder_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $folder;

    /**
     * @ORM\OneToOne(targetEntity="Product", mappedBy="project", cascade={"remove", "persist"})
     */
    private $product;
    
    /**
     * @ORM\OneToMany(targetEntity="ManufacturingPlastic", mappedBy="project", cascade={"remove", "persist"})
     */
    private $manufacturingPlastics;
    
    /**
     * @ORM\OneToMany(targetEntity="ManufacturingMetallic", mappedBy="project", cascade={"remove", "persist"})
     */
    private $manufacturingMetallics;
    
    /**
     * @ORM\OneToMany(targetEntity="ManufacturingPart", mappedBy="project", cascade={"remove", "persist"})
     */
    private $manufacturingParts;
    
    /**
     * @ORM\OneToMany(targetEntity="ElectronicOtherPart", mappedBy="project", cascade={"remove", "persist"})
     */
    private $electronicOtherParts;
    
    /**
     * @ORM\OneToMany(targetEntity="ElectronicPcbPart", mappedBy="project", cascade={"remove", "persist"})
     */
    private $electronicPcbParts;
    
    /**
     * @ORM\OneToMany(targetEntity="ElectronicComponentPart", mappedBy="project", cascade={"remove", "persist"})
     */
    private $electronicComponentParts;
    
    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="project", cascade={"remove", "persist"})
     */
    private $users;

    /**
     * @var GreenUser
     *
     * @ORM\ManyToOne(targetEntity="Arto\GreenBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="green_user_id", referencedColumnName="id")
     * })
     */
    private $greenUser;


    public function __construct()
    {
        $this->uniq = md5( uniqid('artoproduct', true) );
        $this->active = true;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Methods for upload
     */

    public function getAbsolutePath($image)
    {
        return null === $image ? null : $this->getUploadRootDir().'/'.$image;
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/flux/project';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            $this->path = $this->file->guessExtension();
        }

        if (null !== $this->schemaFile) {
            $this->path = $this->file->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        } else {
            $this->file->move($this->getUploadRootDir(), $this->getImage());
            unset($this->file);
        }

        if (null === $this->schemaFile) {
            return;
        } else {
            $this->file->move($this->getUploadRootDir(), $this->getSchemaImage());
            unset($this->schemaFile);
        }
    }

    /**
     * @ORM\PreRemove()
     */
    public function storeFilenameForRemove()
    {
        $this->filenameForRemove = $this->getAbsolutePath($this->image);
        $this->schemaFilenameForRemove = $this->getAbsolutePath($this->schemaImage);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($this->filenameForRemove) {
            unlink($this->filenameForRemove);
        }

        if ($this->schemaFilenameForRemove) {
            unlink($this->schemaFilenameForRemove);
        }
    }


    /**

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uniq
     *
     * @param string $uniq
     */
    public function setUniq($uniq)
    {
        $this->uniq = $uniq;
    }

    /**
     * Get uniq
     *
     * @return string
     */
    public function getUniq()
    {
        return $this->uniq;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set pilote
     *
     * @param string $pilote
     */
    public function setPilote($pilote)
    {
        $this->pilote = $pilote;
    }

    /**
     * Get pilote
     *
     * @return string
     */
    public function getPilote()
    {
        return $this->pilote;
    }

    /**
     * Set piloteDate
     *
     * @param date $piloteDate
     */
    public function setPiloteDate($piloteDate)
    {
        $this->piloteDate = $piloteDate;
    }

    /**
     * Get piloteDate
     *
     * @return date
     */
    public function getPiloteDate()
    {
        return $this->piloteDate;
    }

    /**
     * Set path
     *
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set schemaPath
     *
     * @param string $schemaPath
     */
    public function setSchemaPath($schemaPath)
    {
        $this->schemaPath = $schemaPath;
    }

    /**
     * Get schemaPath
     *
     * @return string
     */
    public function getSchemaPath()
    {
        return $this->schemaPath;
    }

    /**
     * Set active
     *
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    
     /**
     * Set exportedAt
     *
     * @param datetime $exportedAt
     */
    public function setExportedAt($exportedAt)
    {
        $this->exportedAt = $exportedAt;
    }

    /**
     * Get exportedAt
     *
     * @return datetime
     */
    public function getExportedAt()
    {
        return $this->exportedAt;
    }

    /**
     * Set product
     *
     * @param Arto\FluxBundle\Entity\Product $product
     */
    public function setProduct(\Arto\FluxBundle\Entity\Product $product)
    {
        $this->product = $product;
    }

    /**
     * Get product
     *
     * @return Arto\FluxBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set greenUser
     *
     * @param Arto\GreenBundle\Entity\User $greenUser
     */
    public function setGreenUser(\Arto\GreenBundle\Entity\User $greenUser)
    {
        $this->greenUser = $greenUser;
    }

    /**
     * Get greenUser
     *
     * @return Arto\GreenBundle\Entity\User 
     */
    public function getGreenUser()
    {
        return $this->greenUser;
    }

    /**
     * Add manufacturingPlastics
     *
     * @param Arto\FluxBundle\Entity\ManufacturingPlastic $manufacturingPlastics
     */
    public function addManufacturingPlastic(\Arto\FluxBundle\Entity\ManufacturingPlastic $manufacturingPlastics)
    {
        $this->manufacturingPlastics[] = $manufacturingPlastics;
    }

    /**
     * Get manufacturingPlastics
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getManufacturingPlastics()
    {
        return $this->manufacturingPlastics;
    }

    /**
     * Add manufacturingMetallics
     *
     * @param Arto\FluxBundle\Entity\ManufacturingMetallic $manufacturingMetallics
     */
    public function addManufacturingMetallic(\Arto\FluxBundle\Entity\ManufacturingMetallic $manufacturingMetallics)
    {
        $this->manufacturingMetallics[] = $manufacturingMetallics;
    }

    /**
     * Get manufacturingMetallics
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getManufacturingMetallics()
    {
        return $this->manufacturingMetallics;
    }

    /**
     * Add manufacturingParts
     *
     * @param Arto\FluxBundle\Entity\ManufacturingPart $manufacturingParts
     */
    public function addManufacturingPart(\Arto\FluxBundle\Entity\ManufacturingPart $manufacturingParts)
    {
        $this->manufacturingParts[] = $manufacturingParts;
    }

    /**
     * Get manufacturingParts
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getManufacturingParts()
    {
        return $this->manufacturingParts;
    }

    /**
     * Add users
     *
     * @param Arto\FluxBundle\Entity\User $users
     */
    public function addUser(\Arto\FluxBundle\Entity\User $users)
    {
        $this->users[] = $users;
    }

    /**
     * Get users
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set descriptionuf
     *
     * @param string $descriptionuf
     */
    public function setDescriptionuf($descriptionuf)
    {
        $this->descriptionuf = $descriptionuf;
    }

    /**
     * Get descriptionuf
     *
     * @return string 
     */
    public function getDescriptionuf()
    {
        return $this->descriptionuf;
    }

    /**
     * Add electronicOtherParts
     *
     * @param Arto\FluxBundle\Entity\ElectronicOtherPart $electronicOtherParts
     */
    public function addElectronicOtherPart(\Arto\FluxBundle\Entity\ElectronicOtherPart $electronicOtherParts)
    {
        $this->electronicOtherParts[] = $electronicOtherParts;
    }

    /**
     * Get electronicOtherParts
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getElectronicOtherParts()
    {
        return $this->electronicOtherParts;
    }

    /**
     * Add electronicPcbParts
     *
     * @param Arto\FluxBundle\Entity\ElectronicPcbPart $electronicPcbParts
     */
    public function addElectronicPcbPart(\Arto\FluxBundle\Entity\ElectronicPcbPart $electronicPcbParts)
    {
        $this->electronicPcbParts[] = $electronicPcbParts;
    }

    /**
     * Get electronicPcbParts
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getElectronicPcbParts()
    {
        return $this->electronicPcbParts;
    }
    
    /**
     * Add electronicPcbParts
     *
     * @param Arto\FluxBundle\Entity\ElectronicComponentPart $electronicComponentParts
     */
    public function addElectronicComponentPart(\Arto\FluxBundle\Entity\ElectronicComponentPart $electronicComponentParts)
    {
        $this->electronicComponentParts[] = $electronicComponentParts;
    }

    /**
     * Get electronicComponentParts
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getElectronicComponentParts()
    {
        return $this->electronicComponentParts;
    }

    /**
     * Set folder
     *
     * @param Arto\FluxBundle\Entity\Folder $folder
     */
    public function setFolder(\Arto\FluxBundle\Entity\Folder $folder = null)
    {
        $this->folder = $folder;
    }

    /**
     * Get folder
     *
     * @return Arto\FluxBundle\Entity\Folder 
     */
    public function getFolder()
    {
        return $this->folder;
    }
}