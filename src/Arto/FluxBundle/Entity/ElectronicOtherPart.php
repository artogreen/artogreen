<?php
namespace Arto\FluxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="flux_electronic_other_part")
 */
class ElectronicOtherPart
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="electronicOtherParts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;
    
    /**
     * part
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Arto\AcvBundle\Entity\Base")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="part_id", referencedColumnName="id")
     * })
     */
    private $part;
    
    
    /**
     * @ORM\Column(name="param1Unit", type="string", nullable=false)
     */
    private $param1Unit;
    
    /**
     * @ORM\Column(name="param1Value", type="decimal", nullable=false, scale=2)
     */
    private $param1Value;
    
    /**
     * @ORM\Column(name="param2Unit", type="string", nullable=true)
     */
    private $param2Unit;
    
    /**
     * @ORM\Column(name="param2Value", type="decimal", nullable=true, scale=2)
     */
    private $param2Value;
    
    /**
     * @ORM\Column(name="unitMass", type="string", nullable=true)
     */
    private $unitMass;
    
    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getId();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set param1Unit
     *
     * @param string $param1Unit
     */
    public function setParam1Unit($param1Unit)
    {
        $this->param1Unit = $param1Unit;
    }

    /**
     * Get param1Unit
     *
     * @return string 
     */
    public function getParam1Unit()
    {
        return $this->param1Unit;
    }

    /**
     * Set param1Value
     *
     * @param decimal $param1Value
     */
    public function setParam1Value($param1Value)
    {
        $this->param1Value = $param1Value;
    }

    /**
     * Get param1Value
     *
     * @return decimal 
     */
    public function getParam1Value()
    {
        return $this->param1Value;
    }

    /**
     * Set param2Unit
     *
     * @param string $param2Unit
     */
    public function setParam2Unit($param2Unit)
    {
        $this->param2Unit = $param2Unit;
    }

    /**
     * Get param2Unit
     *
     * @return string 
     */
    public function getParam2Unit()
    {
        return $this->param2Unit;
    }

    /**
     * Set param2Value
     *
     * @param decimal $param2Value
     */
    public function setParam2Value($param2Value)
    {
        $this->param2Value = $param2Value;
    }

    /**
     * Get param2Value
     *
     * @return decimal 
     */
    public function getParam2Value()
    {
        return $this->param2Value;
    }

    /**
     * Set unitMass
     *
     * @param string $unitMass
     */
    public function setUnitMass($unitMass)
    {
        $this->unitMass = $unitMass;
    }

    /**
     * Get unitMass
     *
     * @return string 
     */
    public function getUnitMass()
    {
        return $this->unitMass;
    }

    /**
     * Set project
     *
     * @param Arto\FluxBundle\Entity\Project $project
     */
    public function setProject(\Arto\FluxBundle\Entity\Project $project)
    {
        $this->project = $project;
    }

    /**
     * Get project
     *
     * @return Arto\FluxBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set part
     *
     * @param Arto\AcvBundle\Entity\Base $part
     */
    public function setPart(\Arto\AcvBundle\Entity\Base $part)
    {
        $this->part = $part;
    }

    /**
     * Get part
     *
     * @return Arto\AcvBundle\Entity\Base 
     */
    public function getPart()
    {
        return $this->part;
    }
}