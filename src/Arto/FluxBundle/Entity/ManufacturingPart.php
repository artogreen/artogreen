<?php
namespace Arto\FluxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="flux_manufacturing_part")
 */
class ManufacturingPart
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Quantité
     *
     * @ORM\Column(name="quantity", type="integer", nullable=false, scale=2)
     */
    private $quantity;
    
    /**
     * Valeur pour Poids/Volume/Longueur
     *
     * @ORM\Column(name="weight", type="decimal", nullable=false, scale=2)
     */
    private $weight;

    /**
     * @ORM\Column(name="informations", type="string", length=255, nullable=false)
     */
    private $informations;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="manufacturingParts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;

    /**
     * @var ManufacturingCategory
     *
     * @ORM\ManyToOne(targetEntity="ManufacturingCategory", inversedBy="parts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var ManufacturingType
     *
     * @ORM\ManyToOne(targetEntity="ManufacturingType", inversedBy="parts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     * })
     */
    private $type;

    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getLastname();
    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param decimal $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * Get quantity
     *
     * @return decimal
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set informations
     *
     * @param string $informations
     */
    public function setInformations($informations)
    {
        $this->informations = $informations;
    }

    /**
     * Get informations
     *
     * @return string
     */
    public function getInformations()
    {
        return $this->informations;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set project
     *
     * @param Arto\FluxBundle\Entity\Project $project
     */
    public function setProject(\Arto\FluxBundle\Entity\Project $project)
    {
        $this->project = $project;
    }

    /**
     * Get project
     *
     * @return Arto\FluxBundle\Entity\Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set type
     *
     * @param Arto\FluxBundle\Entity\ManufacturingType $type
     */
    public function setType(\Arto\FluxBundle\Entity\ManufacturingType $type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return Arto\FluxBundle\Entity\ManufacturingType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set category
     *
     * @param Arto\FluxBundle\Entity\ManufacturingCategory $category
     */
    public function setCategory(\Arto\FluxBundle\Entity\ManufacturingCategory $category)
    {
        $this->category = $category;
    }

    /**
     * Get category
     *
     * @return Arto\FluxBundle\Entity\ManufacturingCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set weight
     *
     * @param decimal $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * Get weight
     *
     * @return decimal 
     */
    public function getWeight()
    {
        return $this->weight;
    }
}