<?php
namespace Arto\FluxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="flux_manufacturing_plastic")
 */
class ManufacturingPlastic
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="label", type="string", length=100, nullable=false)
     */
    private $label;

    /**
     * Valeur pour Poids/Volume/Longueur
     *
     * @ORM\Column(name="quantity", type="decimal", nullable=true, scale=2)
     */
    private $quantity;

    /**
     * Poids (g)
     *
     * @ORM\Column(name="weight", type="decimal", nullable=true, scale=2)
     */
    private $weight;

    /**
     * @ORM\Column(name="informations", type="string", length=255, nullable=true)
     */
    private $informations;

    /**
     * Surface globale du procédé de finition
     *
     * @ORM\Column(name="process_surface", type="integer", nullable=true)
     */
    private $processSurface;

    /**
     * Epaisseur (µm) du procédé de finition
     *
     * @ORM\Column(name="process_thickness", type="integer", nullable=true)
     */
    private $processThickness;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="manufacturingPlastics")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;

    /**
     * Type de matière plastique
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Arto\AcvBundle\Entity\Base", inversedBy="plastic")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="base_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $base;

    /**
     * dont les Charges
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Arto\AcvBundle\Entity\Base", inversedBy="plastic")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="charge_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $charge;
    
    /**
     * % de Charges
     *
     * @ORM\Column(name="percent_charges", type="integer", nullable=true)
     */
    private $percentCharge;

    /**
     * dont les Additifs
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Arto\AcvBundle\Entity\Base", inversedBy="plastic")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="additive_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $additive;
    
    /**
     * % d'Additifs
     *
     * @ORM\Column(name="percent_additives", type="integer", nullable=true)
     */
    private $percentAdditive;

    /**
     * dont les Colorants
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Arto\AcvBundle\Entity\Base", inversedBy="plastic")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="colorant_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $colorant;
    
    /**
     * % de Colorants
     *
     * @ORM\Column(name="percent_colorants", type="integer", nullable=true)
     */
    private $percentColorant;

    /**
     * Procédés de fabrication
     * @var Base
     *
     * @ORM\ManyToOne(targetEntity="Arto\AcvBundle\Entity\Base", inversedBy="plastic")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="process_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $process;


    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getLabel();
    }

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set quantity
     *
     * @param decimal $quantity
     */
    public function setQuantity($quantity = null)
    {
        $this->quantity = $quantity;
    }

    /**
     * Get quantity
     *
     * @return decimal 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set weight
     *
     * @param decimal $weight
     */
    public function setWeight($weight = null)
    {
        $this->weight = $weight;
    }

    /**
     * Get weight
     *
     * @return decimal 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set informations
     *
     * @param string $informations
     */
    public function setInformations($informations = null)
    {
        $this->informations = $informations;
    }

    /**
     * Get informations
     *
     * @return string 
     */
    public function getInformations()
    {
        return $this->informations;
    }

    /**
     * Set processSurface
     *
     * @param integer $processSurface
     */
    public function setProcessSurface($processSurface = null)
    {
        $this->processSurface = $processSurface;
    }

    /**
     * Get processSurface
     *
     * @return integer 
     */
    public function getProcessSurface()
    {
        return $this->processSurface;
    }

    /**
     * Set processThickness
     *
     * @param integer $processThickness
     */
    public function setProcessThickness($processThickness = null)
    {
        $this->processThickness = $processThickness;
    }

    /**
     * Get processThickness
     *
     * @return integer 
     */
    public function getProcessThickness()
    {
        return $this->processThickness;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set percentCharge
     *
     * @param integer $percentCharge
     */
    public function setPercentCharge($percentCharge = null)
    {
        $this->percentCharge = $percentCharge;
    }

    /**
     * Get percentCharge
     *
     * @return integer 
     */
    public function getPercentCharge()
    {
        return $this->percentCharge;
    }

    /**
     * Set percentAdditive
     *
     * @param integer $percentAdditive
     */
    public function setPercentAdditive($percentAdditive = null)
    {
        $this->percentAdditive = $percentAdditive;
    }

    /**
     * Get percentAdditive
     *
     * @return integer 
     */
    public function getPercentAdditive()
    {
        return $this->percentAdditive;
    }

    /**
     * Set percentColorant
     *
     * @param integer $percentColorant
     */
    public function setPercentColorant($percentColorant = null)
    {
        $this->percentColorant = $percentColorant;
    }

    /**
     * Get percentColorant
     *
     * @return integer 
     */
    public function getPercentColorant()
    {
        return $this->percentColorant;
    }

    /**
     * Set project
     *
     * @param Arto\FluxBundle\Entity\Project $project
     */
    public function setProject(\Arto\FluxBundle\Entity\Project $project)
    {
        $this->project = $project;
    }

    /**
     * Get project
     *
     * @return Arto\FluxBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set base
     *
     * @param Arto\AcvBundle\Entity\Base $base
     */
    public function setBase(\Arto\AcvBundle\Entity\Base $base = null)
    {
        $this->base = $base;
    }

    /**
     * Get base
     *
     * @return Arto\AcvBundle\Entity\Base 
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * Set charge
     *
     * @param Arto\AcvBundle\Entity\Base $charge
     */
    public function setCharge(\Arto\AcvBundle\Entity\Base $charge = null)
    {
        $this->charge = $charge;
    }

    /**
     * Get charge
     *
     * @return Arto\AcvBundle\Entity\Base 
     */
    public function getCharge()
    {
        return $this->charge;
    }

    /**
     * Set additive
     *
     * @param Arto\AcvBundle\Entity\Base $additive
     */
    public function setAdditive(\Arto\AcvBundle\Entity\Base $additive = null)
    {
        $this->additive = $additive;
    }

    /**
     * Get additive
     *
     * @return Arto\AcvBundle\Entity\Base 
     */
    public function getAdditive()
    {
        return $this->additive;
    }

    /**
     * Set colorant
     *
     * @param Arto\AcvBundle\Entity\Base $colorant
     */
    public function setColorant(\Arto\AcvBundle\Entity\Base $colorant = null)
    {
        $this->colorant = $colorant;
    }

    /**
     * Get colorant
     *
     * @return Arto\AcvBundle\Entity\Base 
     */
    public function getColorant()
    {
        return $this->colorant;
    }

    /**
     * Set process
     *
     * @param Arto\AcvBundle\Entity\Base $process
     */
    public function setProcess(\Arto\AcvBundle\Entity\Base $process = null)
    {
        $this->process = $process;
    }

    /**
     * Get process
     *
     * @return Arto\AcvBundle\Entity\Base 
     */
    public function getProcess()
    {
        return $this->process;
    }
}