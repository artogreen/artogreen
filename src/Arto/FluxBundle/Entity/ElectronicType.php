<?php
namespace Arto\FluxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="flux_electronic_type")
 */
class ElectronicType
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="label", type="string", length=255, nullable=false)
     */
    private $label;
    
    /**
     * @ORM\Column(name="isVisible", type="string", length=6, nullable=true)
     */
    private $isVisible;



    public function __construct()
    {

    }

    public function __toString()
    {
        return $this->getLabel();
    }

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set label
     *
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }
    
    /**
     * Set isVisible
     *
     * @param string $isVisible
     */
    public function setIsVisible($isVisible)
    {
        $this->isVisible = $isVisible;
    }

    /**
     * Get isVisible
     *
     * @return string 
     */
    public function getIsVisible()
    {
        return $this->isVisible;
    }
}