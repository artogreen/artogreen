-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Dim 03 Février 2013 à 19:40
-- Version du serveur: 5.5.24-log
-- Version de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `artogreen`
--

-- --------------------------------------------------------

--
-- Structure de la table `product_process_icpe`
--

CREATE TABLE IF NOT EXISTS `product_process_icpe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `unit` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `product_process_icpe`
--

INSERT INTO `product_process_icpe` (`id`, `name`, `unit`, `created_at`, `updated_at`) VALUES
(1, 'Investissement nouvelle machine pour la transformation matières plastiques,caoutchouc, résines, adhésifs', 'T/j', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Investissement nouvelle machine pour le travail mécanique des métaux et alliages', 'kW', '2012-10-25 00:00:00', '2012-10-25 00:00:00'),
(3, 'Investissement de cuve de nettoyage, dégraissage, décapage, polissage,... des métaux et matières plastiques', 'm<sup>3</sup>', '2012-10-25 00:00:00', '2012-10-25 00:00:00'),
(4, 'Investissement nouvelle machine ou équipement (refroidissement, combustion,…)', '', '2012-10-25 00:00:00', '2012-10-25 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `product_stockage_icpe`
--

CREATE TABLE IF NOT EXISTS `product_stockage_icpe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `unit` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `product_stockage_icpe`
--

INSERT INTO `product_stockage_icpe` (`id`, `name`, `unit`, `created_at`, `updated_at`) VALUES
(1, 'Matières premières plastiques', 'm<sup>3</sup>/an', '2012-10-25 00:00:00', '2012-10-25 00:00:00'),
(2, 'Sous-ensembles plastiques', 'm<sup>3</sup>/an', '2012-10-25 00:00:00', '2012-10-25 00:00:00'),
(3, 'Produits finis plastiques', 'm<sup>3</sup>/an', '2012-10-25 00:00:00', '2012-10-25 00:00:00'),
(4, 'Produit à usage réglementé tels que isocyanate, solvant, hydrocarbures, gaz, …', 'Kg/an', '2012-10-25 00:00:00', '2012-10-25 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
