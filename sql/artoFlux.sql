-- Adminer 3.6.1 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `flux_manufacturing_category`;
CREATE TABLE `flux_manufacturing_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) NOT NULL,
  `unit` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `flux_manufacturing_category` (`id`, `label`, `unit`) VALUES
(1,	'Batteries',	'Poids (g)'),
(2,	'Ampoules',	'Volume (cm3)'),
(3,	'Ecran LCD',	'Poids (g)'),
(4,	'Câbles',	'Longueur (m)'),
(5,	'Prises',	'Poids (g)'),
(6,	'Panneaux photovoltaïques',	'Surface (cm3)');

DROP TABLE IF EXISTS `flux_manufacturing_metallic`;
CREATE TABLE `flux_manufacturing_metallic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `base_id` int(11) DEFAULT NULL,
  `colorant_id` int(11) DEFAULT NULL,
  `label` varchar(100) NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `weight` int(11) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `process_surface` int(11) NOT NULL,
  `process_thickness` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_50F5F485166D1F9C` (`project_id`),
  KEY `IDX_50F5F4856967DF41` (`base_id`),
  KEY `IDX_50F5F485D2C121B7` (`colorant_id`),
  CONSTRAINT `FK_50F5F485D2C121B7` FOREIGN KEY (`colorant_id`) REFERENCES `acv_base` (`id`),
  CONSTRAINT `FK_50F5F485166D1F9C` FOREIGN KEY (`project_id`) REFERENCES `flux_project` (`id`),
  CONSTRAINT `FK_50F5F4856967DF41` FOREIGN KEY (`base_id`) REFERENCES `acv_base` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `flux_manufacturing_part`;
CREATE TABLE `flux_manufacturing_part` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `exist` tinyint(1) NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `informations` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B2C34BA1166D1F9C` (`project_id`),
  KEY `IDX_B2C34BA112469DE2` (`category_id`),
  CONSTRAINT `FK_B2C34BA112469DE2` FOREIGN KEY (`category_id`) REFERENCES `flux_manufacturing_category` (`id`),
  CONSTRAINT `FK_B2C34BA1166D1F9C` FOREIGN KEY (`project_id`) REFERENCES `flux_project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `flux_manufacturing_plastic`;
CREATE TABLE `flux_manufacturing_plastic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `base_id` int(11) DEFAULT NULL,
  `charge_id` int(11) DEFAULT NULL,
  `additive_id` int(11) DEFAULT NULL,
  `colorant_id` int(11) DEFAULT NULL,
  `label` varchar(100) NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `weight` int(11) NOT NULL,
  `informations` varchar(255) DEFAULT NULL,
  `process_surface` int(11) NOT NULL,
  `process_thickness` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `process_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_36E0D86166D1F9C` (`project_id`),
  KEY `IDX_36E0D866967DF41` (`base_id`),
  KEY `IDX_36E0D8655284914` (`charge_id`),
  KEY `IDX_36E0D86384C93F0` (`additive_id`),
  KEY `IDX_36E0D86D2C121B7` (`colorant_id`),
  KEY `IDX_36E0D867EC2F574` (`process_id`),
  CONSTRAINT `FK_36E0D867EC2F574` FOREIGN KEY (`process_id`) REFERENCES `acv_base` (`id`),
  CONSTRAINT `FK_36E0D86166D1F9C` FOREIGN KEY (`project_id`) REFERENCES `flux_project` (`id`),
  CONSTRAINT `FK_36E0D86384C93F0` FOREIGN KEY (`additive_id`) REFERENCES `acv_base` (`id`),
  CONSTRAINT `FK_36E0D8655284914` FOREIGN KEY (`charge_id`) REFERENCES `acv_base` (`id`),
  CONSTRAINT `FK_36E0D866967DF41` FOREIGN KEY (`base_id`) REFERENCES `acv_base` (`id`),
  CONSTRAINT `FK_36E0D86D2C121B7` FOREIGN KEY (`colorant_id`) REFERENCES `acv_base` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `flux_manufacturing_type`;
CREATE TABLE `flux_manufacturing_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `label` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_77126C4E12469DE2` (`category_id`),
  CONSTRAINT `FK_77126C4E12469DE2` FOREIGN KEY (`category_id`) REFERENCES `flux_manufacturing_category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `flux_manufacturing_type` (`id`, `category_id`, `label`) VALUES
(1,	1,	'Plomb acide'),
(2,	1,	'LiSoClO2'),
(3,	1,	'Ni-Cd'),
(4,	1,	'NiMh'),
(5,	1,	'Alcaline'),
(6,	1,	'Autre'),
(7,	2,	'Tube fluorescent'),
(8,	2,	'Ampoule à incandescence'),
(9,	2,	'Ampoule halogène'),
(10,	2,	'Autre');

DROP TABLE IF EXISTS `flux_project`;
CREATE TABLE `flux_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniq` varchar(32) NOT NULL,
  `name` varchar(32) NOT NULL,
  `numero` int(11) DEFAULT NULL,
  `reference` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `product_line` tinyint(1) DEFAULT NULL,
  `functional_unit` longtext,
  `pilote` varchar(255) DEFAULT NULL,
  `pilote_date` date DEFAULT NULL,
  `path` varchar(100) DEFAULT NULL,
  `schema_path` varchar(100) DEFAULT NULL,
  `rohs` tinyint(1) DEFAULT NULL,
  `rohs_detail` varchar(255) DEFAULT NULL,
  `deee` tinyint(1) DEFAULT NULL,
  `battery` tinyint(1) DEFAULT NULL,
  `battery_extract` tinyint(1) DEFAULT NULL,
  `reach` tinyint(1) DEFAULT NULL,
  `reach_detail` varchar(255) DEFAULT NULL,
  `product_standard` tinyint(1) DEFAULT NULL,
  `product_standard_detail` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `flux_project` (`id`, `uniq`, `name`, `numero`, `reference`, `weight`, `product_line`, `functional_unit`, `pilote`, `pilote_date`, `path`, `schema_path`, `rohs`, `rohs_detail`, `deee`, `battery`, `battery_extract`, `reach`, `reach_detail`, `product_standard`, `product_standard_detail`, `active`, `created_at`, `updated_at`) VALUES
(1,	'a60b50e2f779c8e41745400541488440',	'test',	0,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	'2012-11-16 10:42:46',	'2012-11-16 10:42:46');

DROP TABLE IF EXISTS `flux_user`;
CREATE TABLE `flux_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `role` varchar(255) NOT NULL,
  `company` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `tel` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_181781B166D1F9C` (`project_id`),
  CONSTRAINT `FK_181781B166D1F9C` FOREIGN KEY (`project_id`) REFERENCES `flux_project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2012-11-16 10:43:00
