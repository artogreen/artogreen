-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mer 07 Novembre 2012 à 09:23
-- Version du serveur: 5.5.24-log
-- Version de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `artogreen`
--

-- --------------------------------------------------------

--
-- Structure de la table `product_activite`
--

CREATE TABLE IF NOT EXISTS `product_activite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `situation_administrative_id` int(11) DEFAULT NULL,
  `situation_reelle_id` int(11) DEFAULT NULL,
  `rubrique_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `conformite_id` int(11) DEFAULT NULL,
  `commentaire` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_73C53F828147D86B` (`situation_administrative_id`),
  KEY `IDX_73C53F82141C302F` (`situation_reelle_id`),
  KEY `IDX_73C53F823BD38833` (`rubrique_id`),
  KEY `IDX_73C53F82166D1F9C` (`project_id`),
  KEY `IDX_73C53F8294F3C51` (`conformite_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `product_activite`
--

INSERT INTO `product_activite` (`id`, `situation_administrative_id`, `situation_reelle_id`, `rubrique_id`, `name`, `created_at`, `updated_at`, `project_id`, `conformite_id`, `commentaire`) VALUES
(1, 1, 1, 28, 'Activité 1', '2012-10-29 00:00:00', '0000-00-00 00:00:00', 1, 1, 'Bla bla bla !');

-- --------------------------------------------------------

--
-- Structure de la table `product_conformite`
--

CREATE TABLE IF NOT EXISTS `product_conformite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `product_conformite`
--

INSERT INTO `product_conformite` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'NC', '2012-10-29 00:00:00', '2012-10-29 00:00:00'),
(2, 'C', '2012-10-29 00:00:00', '2012-10-29 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `product_evaluationicpe`
--

CREATE TABLE IF NOT EXISTS `product_evaluationicpe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `nom_correspondant` varchar(255) NOT NULL,
  `date_reponse` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `commentaire` varchar(255) NOT NULL,
  `rubriqueEvaluation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7502D6451D5F791B` (`rubriqueEvaluation_id`),
  KEY `IDX_7502D645166D1F9C` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `product_evaluationicpe`
--

INSERT INTO `product_evaluationicpe` (`id`, `project_id`, `nom_correspondant`, `date_reponse`, `created_at`, `updated_at`, `commentaire`, `rubriqueEvaluation_id`) VALUES
(1, 1, 'Antoine', '2012-11-08 00:00:00', '2012-11-05 00:00:00', '2012-11-05 00:00:00', 'No comment', 1);

-- --------------------------------------------------------

--
-- Structure de la table `product_family`
--

CREATE TABLE IF NOT EXISTS `product_family` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `process_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `impact` longtext,
  `usage_terms` longtext,
  `rank1` varchar(20) NOT NULL,
  `rank2` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C79A60FF7EC2F574` (`process_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=143 ;

--
-- Contenu de la table `product_family`
--

INSERT INTO `product_family` (`id`, `process_id`, `name`, `impact`, `usage_terms`, `rank1`, `rank2`, `created_at`, `updated_at`) VALUES
(1, 2, 'Argenture', 'La solution est basée sur la substance dangereuse bien connue de cyanure (toxicité inhérente). Les cyanures peuvent causer à des problèmes dans le traitement des effluents et ceci interfère l''oxydation du cyanure et la séparation par la précipitation du métal', 'Les besoins : \n  - extraction de vapeur \n - les eaux usées devraient être traitées avec une étape d''oxydation de cyanure', 'Non Acceptable', 'Acceptable', '2012-10-19 11:48:51', '2012-10-19 11:48:51'),
(2, 3, 'Manuel/semi-manuel', 'Aucun impact', 'Aucun impact', 'Propre', 'Propre', '2012-10-19 11:48:51', '2012-10-19 11:48:51'),
(3, 4, 'Atomisation  fab Ag', 'Consommation d''énergie importante (1200°C pour la fusion en métal) et poussière métall dans l''eau', 'L''eau en cirsuit fermé avec filtration\n(traiter l''eau comme un DIS pourrait être une solution mais chère à long terme)', 'Non Acceptable', 'Acceptable', '2012-10-19 11:48:51', '2012-10-19 11:48:51'),
(4, 5, 'Bobinage', 'Pas d''impact sur l''environnement', 'Aucun impact', 'Propre', 'Propre', '2012-10-19 11:48:52', '2012-10-19 11:48:52'),
(5, 6, 'Brasage manuel (chalumeau)', 'Émission de COV (alcool d''isopropyle), impact sur l''homme', '- Aspiration et filtration\n - EPI pour le personnel', 'Non Acceptable', 'Acceptable', '2012-10-19 11:48:52', '2012-10-19 11:48:52'),
(6, 2, 'Cadmiage', 'Cadmium interdit par la ROHS (hormis dans les contacts électriques)', 'Impact non acceptable', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:48:52', '2012-10-19 11:48:52'),
(7, 2, 'Chromage Cr 3', 'Cr (III) est converti en chrome (metal)', 'Emissions dans l''air : aspiration \n Ne pas utiliser le Cr 6', 'Acceptable', 'Acceptable', '2012-10-19 11:48:52', '2012-10-19 11:48:52'),
(8, 2, 'Chromage Cr 6', 'La présence du chrome 6 dans les produits est interdit par la ROHS (allergénique, cancérogène, persistant et toxique pour l''environnement)', 'Cancérigène car présence de chrome 6\nEmissions dans l''air : aspiration', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:48:52', '2012-10-19 11:48:52'),
(9, 7, 'Clinchage Acier', 'Aucun impact', 'Aucun impact', 'Propre', 'Propre', '2012-10-19 11:48:52', '2012-10-19 11:48:52'),
(10, 7, 'Clinchage Al', 'Aucun impact', 'Aucun impact', 'Propre', 'Propre', '2012-10-19 11:48:52', '2012-10-19 11:48:52'),
(11, 8, 'Collage', 'Emissions de COV', 'Aspiration et filtration', 'Acceptable', 'Propre', '2012-10-19 11:48:52', '2012-10-19 11:48:52'),
(12, 4, 'Contactage Ag', 'Consommation d''énergie', 'Aucun impact', 'Acceptable', 'Acceptable', '2012-10-19 11:48:52', '2012-10-19 11:48:52'),
(13, 28, 'Hydraulique (PET)', 'Thermoplastique sans substance halogéne\n - émissions dues à l''ouverture de moule\n - Évaporation hydraulique d''huile : circuit (poussière dans l'' air, fuites…), consommation pétrolière', 'Utiliser un autre procédé :  injection électrique', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:48:52', '2012-10-19 11:48:52'),
(14, 28, 'Hydraulique (polyurethane)', 'Thermoplatique sans substance halogéne, mais contient des isocyanates (asthme, allergéniques…)\n - Emissions lors de l''ouverture du moule \n - Évaporation hydraulique d''huile : circuit (poussière dans l'' air, fuites…), consommation pétrolière', 'Utiliser un autre procédé :  injection électrique', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:48:52', '2012-10-19 11:48:52'),
(15, 28, 'Electrique (ABS)', 'Émissions de styrène lors de l''ouverture du moule : cancérigène potentiel et toxique pour l''environnement mais non persistant', '- Aspiration des émissions de styrène\n- Substituer par autre thermoplastique si possible', 'Non Acceptable', 'Acceptable', '2012-10-19 11:48:53', '2012-10-19 11:48:53'),
(16, 28, 'Electrique (PET)', 'Thermoplastique sans substance halogène\n - émissions dues à l''ouverture de moule', '- Aspiration \n- Rassembler l''excès de la graisse et la réutiliser', 'Acceptable', 'Acceptable', '2012-10-19 11:48:53', '2012-10-19 11:48:53'),
(17, 28, 'Electrique (phtalamide)', 'Thermoplastique sans substance halogène, mais contient la toxicité légère des isocyanates (asthme, allergies…)\n - émissions dues à l''ouverture de moule', '- Aspiration\n- Rassembler l''excès de la graisse et la réutiliser', 'Acceptable', 'Acceptable', '2012-10-19 11:48:53', '2012-10-19 11:48:53'),
(18, 28, 'Electrique (polyamide)', 'Thermoplastique sans substances halogènes\n - émissions dues à l''ouverture de moule', '- Aspiration\n- Rassembler l''excès de la graisse et la réutiliser', 'Acceptable', 'Acceptable', '2012-10-19 11:48:53', '2012-10-19 11:48:53'),
(19, 28, 'Electrique (polycarbonate)', 'Thermoplastique sans substances halogènes\n - émissions dues à l''ouverture de moule', '- Aspiration\n- Rassembler l''excès de la graisse et la réutiliser', 'Acceptable', 'Acceptable', '2012-10-19 11:48:53', '2012-10-19 11:48:53'),
(20, 28, 'Electrique (polyethylene)', 'Thermoplastique sans substance halogène\n - émissions dues à l''ouverture de moule', '- Aspiration\n- Rassembler l''excès de la graisse et la réutiliser', 'Acceptable', 'Acceptable', '2012-10-19 11:48:53', '2012-10-19 11:48:53'),
(21, 28, 'Electrique (polystryrene)', 'Emissions de styrène\n - émissions dues à l''ouverture de moule', '- Aspiration des émissions de styrène', 'Non Acceptable', 'Acceptable', '2012-10-19 11:48:53', '2012-10-19 11:48:53'),
(22, 28, 'Electrique (polyurethane)', 'Thermoplastique sans substance halogène\n - émissions dues à l''ouverture de moule', '- Aspiration \n- Rassembler l''excès de la graisse et la réutiliser', 'Acceptable', 'Acceptable', '2012-10-19 11:48:53', '2012-10-19 11:48:53'),
(23, 28, 'Electrique (PVC)', 'Pendant le moulage du PVC, création de l''acide chlorhydrique => impact pour l''environnement et l''homme', 'Employez un autre thermoplastique ou employez le processus de l''extrusion avec le PVC', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:48:53', '2012-10-19 11:48:53'),
(24, 28, 'Electrique PPOM', 'Thermoplastique sans substances halogènes\n - émissions dues à l''ouverture de moule', '- Aspiration\n- Rassembler l''excès de la graisse et la réutiliser', 'Acceptable', 'Acceptable', '2012-10-19 11:48:53', '2012-10-19 11:48:53'),
(25, 28, 'Hydraulique (ABS)', 'Émissions de styrène lors de l''ouverture de moule (cancérigène potentiel et toxique pour l''environnement mais non persistant\nÉvaporation hydraulique d''huile  => circuit (poussière dans l'' air, fuites…), consommation pétrolière', 'Contient du styrène\nSubstituer par autre thermoplastique si possible', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:48:53', '2012-10-19 11:48:53'),
(26, 28, 'Hydraulique (phtalamide)', 'Thermoplastique sans substance halogène\n - émissions dues à l''ouverture de moule\n - Évaporation hydraulique d''huile : circuit (poussière dans l'' air, fuites…), consommation pétrolière', 'Utiliser un autre procédé :  injection électrique', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:48:54', '2012-10-19 11:48:54'),
(27, 28, 'Hydraulique (polyamide)', 'Thermoplatique sans substance halogène\n - émissions dues à l''ouverture de moule\n - Évaporation hydraulique d''huile : circuit (poussière dans l'' air, fuites…), consommation pétrolière', 'Utiliser un autre procédé :  injection électrique', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:48:54', '2012-10-19 11:48:54'),
(28, 28, 'Hydraulique (polycarbonate)', 'Thermoplastique sans substance halogène\n - émissions dues à l''ouverture de moule\n - Évaporation hydraulique d''huile : circuit (poussière dans l'' air, fuites…), consommation pétrolière', 'Utiliser un autre procédé :  injection électrique', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:48:54', '2012-10-19 11:48:54'),
(29, 28, 'Hydraulique (polyethylene)', 'Thermoplastique sans substance halogène\n - émissions dues à l''ouverture de moule\n - Évaporation hydraulique d''huile : circuit (poussière dans l'' air, fuites…), consommation pétrolière', 'Utiliser un autre procédé :  injection électrique', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:48:54', '2012-10-19 11:48:54'),
(30, 28, 'Hydraulique (polystryrene)', 'Emissions de styrène\n - émissions dues à l''ouverture de moule\n - Évaporation hydraulique d''huile : circuit (poussière dans l'' air, fuites…), consommation pétrolière', 'contient le styrène : dangereux', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:48:54', '2012-10-19 11:48:54'),
(31, 28, 'Hydraulique (PVC)', 'Pendant le moulage du PVC, création de l''acide chloridrique => impact pour l''environnement et l''homme\n - Emissions dangereuses\n - Évaporation hydraulique d''huile : de circuit (poussière dans l''air, fuites…), consommation d''huile', 'Employez un autre thermoplastique ou employez le processus de l''extrusion avec le PVC', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:48:54', '2012-10-19 11:48:54'),
(32, 28, 'Hydraulique PPOM', 'Thermoplastique sans substance halogène\n - moins d''émissions \n - Évaporation hydraulique d''huile : circuit (poussière dans l'' air, fuites…), consommation pétrolière', 'Utiliser un autre procédé :  injection électrique', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:48:54', '2012-10-19 11:48:54'),
(33, 28, 'Polyester (BMC/SMC)', 'Toxicité avec émission de styrène\nEmissions de COV\nchutes de pièces => épuisement de ressource, ne peuvent pas être réutilisation \ndifficulté pour trouver une manière de valorisation des rebuts', 'Changer le moulage par compression pour un moulage par injection (moins d''émissions) \n- Aspiration et de filtration => réduction des COV et la toxicité pour l''homme\n-  Réduction des émissions COV => aspiration et filtration', 'Non Acceptable', 'Acceptable', '2012-10-19 11:48:54', '2012-10-19 11:48:54'),
(34, 4, 'Contact AgSnO2', 'Emissions de poussières métalliques : toxicité pour l''environnement et l''homme (inhalation)\nConsommation d''énergie importante', '- Aspiration, filtration et collecte du déchets métallique', 'Non Acceptable', 'Acceptable', '2012-10-19 11:48:54', '2012-10-19 11:48:54'),
(35, 2, 'Cuivrage', 'Nécessite beaucoup d''énergie\nEmissions dans l''air et dans l''eau', 'Aucun impact', 'Acceptable', 'Acceptable', '2012-10-19 11:48:55', '2012-10-19 11:48:55'),
(36, 10, 'Découpage Laser', 'Émissions dans l''air : vapeurs et projection de poudre en métal,… toxicité pour l''environnement et l''homme (inhalation)', 'Aspiration et filtration\nLa poussière en métal doit être stockée dans un tonneau pour éviter la dispersion', 'Acceptable', 'Propre', '2012-10-19 11:48:55', '2012-10-19 11:48:55'),
(37, 11, 'Plasma (torche)', 'Emissions de fumées toxiques pour l''homme', 'depend de l''épaisseur de tôle\nAspiration et filtres', 'Non Acceptable', 'Acceptable', '2012-10-19 11:48:55', '2012-10-19 11:48:55'),
(38, 11, 'Huile insoluble et chlore', 'Émissions dans l''air : projection de vapeurs d''huile, pulvérisation d''huile (inhalation des composés chlorés)\népuisement important de ressources', 'Impact non acceptable', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:48:55', '2012-10-19 11:48:55'),
(39, 11, 'Micro pulverisation (sans chlore)', 'Huile sans chlore plus favorable à l''environnement : réutilisation du métal possible\nLa micro-pulvérisation consomme moins de ressources et moins d''huile sur lesz pièces\nLe dégraissage est plus facile mais : \n - émissions dans l''air : projection de vapeurs d''huile\n - traitement d''huile', 'Réutilisation de l''huile en circuit fermé afin de diminuer la consommation de matières premières\nCollecter les déchets de découpage (installation magnétique)', 'Acceptable', 'Propre', '2012-10-19 11:48:55', '2012-10-19 11:48:55'),
(40, 11, 'Sans chlore', 'Huile plus favorable à l''environnement : sans chlore, réutilisation du métal possible mais : \n - émissions d''air : projection de vapeurs d''huile', 'Réutilisation de l''huile en circuit fermé afin de diminuer la consommation de matières premières\nCollecter les déchets de découpage (installation magnétique)', 'Acceptable', 'Propre', '2012-10-19 11:48:55', '2012-10-19 11:48:55'),
(41, 11, 'Sans huile', 'Aucun impact sur l''environnement  : processus propre', 'Aucun impact', 'Propre', 'Propre', '2012-10-19 11:48:55', '2012-10-19 11:48:55'),
(42, 12, 'Base', 'Utilisation de préparations de nature basique pourrait être un problème, le pH est modifié => dangerosité pour la flore aquatique et faune \nUtilisation de l''acide => eutrophisation de l''eau \nEn ce qui concerne l''homme, l'' acide est très irritable', 'L''eau en circuit fermé \nMesure du pH avant de rejeter', 'Acceptable', 'Propre', '2012-10-19 11:48:55', '2012-10-19 11:48:55'),
(43, 12, 'Soude', 'L''utilisation de carbonate de sodium => la modification du pH => détérioration de la la faune et flore', 'Placer le bain sur un bac de rétention afin d''éviter l''infiltration des substances dans le sol \nÉliminer les bains usés par une entreprise spécialisée', 'Non Acceptable', 'Acceptable', '2012-10-19 11:48:55', '2012-10-19 11:48:55'),
(44, 12, 'Solvant chloré', 'Les émissions de COV au chlore => appauvrissement de l''ozone dans la stratosphère => dangerosité pour la santé humaine (rayons UV) \nContaminiation sol avec des solvants chlorés pendant la manutention\nLes produits chlorés sont des produits toxiques pour l''homme', 'La surveillance des moyens: \n  - cabine fermée et étanche à l''air => moins d''émissions dans l''air \n  - aspiration des COV et filtration avec du charbon actif pour le traitement des émissions \n - La manutention des solvants chlorés doit être faite par un personnel formé avec une consigne sécurité et environnement \n  - L''huile usée (avec chlore) doit être collectée et stockée avant le traitement par une entreprise spécialisée (déchets dangereux)\nNe pas investir dans cette technique pour les nouvelles lignes de fabrication => dans le futur, l''utilisation de solvants chlorés sera interdit et le prix d''achat de ces produits augmentera', 'Non Acceptable', 'Acceptable', '2012-10-19 11:48:55', '2012-10-19 11:48:55'),
(45, 12, 'Solvant chloré Classe A', 'Emissions COV\nDéchets dangereux', 'Aspiration des COV et fitration par charbon actif pour traitement des émissions', 'Non Acceptable', 'Acceptable', '2012-10-19 11:48:56', '2012-10-19 11:48:55'),
(46, 12, 'White Spirit', 'Toxique pour l''homme (inhalation)', 'Besoin de porter des équipements de protection individuels', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:48:56', '2012-10-19 11:48:56'),
(47, 12, 'Phosphorique acide/base', 'Le carbonate de sodium (soude) est responsable de l''acidification en milieu aqueux (à cause du changement de pH) \nL''acide phosphorique => eutrophisation du milieu aquatique \nEn ce qui concerne la toxicité pour l''homme, l''acide phosphorique peut produire des irritations, mais moins qu''avec l''acide chlorhydrique', 'La préparation base et l''acide dans l''eau doit être traitée comme des déchets dangereux (entreprise spécialisée) \nEn ce qui concerne le stockage de l''eau usée => éviter l''infiltration des substances dans le sol \nMoyens de surveillance pour vérifier le pH', 'Non Acceptable', 'Acceptable', '2012-10-19 11:48:56', '2012-10-19 11:48:56'),
(48, 13, 'Dépose joint Polyuréthane', 'Émissions de COV\nImpact sur l''homme (Isocyanate)', '- Aspiration et filtration\n - EPI pour le personnel', 'Non Acceptable', 'Acceptable', '2012-10-19 11:48:56', '2012-10-19 11:48:56'),
(49, 14, 'Estampage', 'Non efficace\n=> cadence de production faible', 'Aspiration et filtration des poussières', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:48:56', '2012-10-19 11:48:56'),
(50, 14, 'Estampage à chaud', 'Le film en polyester où se trouve l''encre durcit est non recyclable. Le traitement de ces déchets pose problème: déchets certainement incinérés. Conséquences sur les émissions dans l''air et la consommation de ressources même si ils sont valorisés en externe', 'A utiliser pour marquage en petite quantité', 'Non Acceptable', 'Acceptable', '2012-10-19 11:48:56', '2012-10-19 11:48:56'),
(51, 2, 'Etamage', 'Nécessite bcp d''énergie\nEmissions dans l''air et dans l''eau', 'Aucun impact', 'Non Acceptable', 'Acceptable', '2012-10-19 11:48:56', '2012-10-19 11:48:56'),
(52, 9, 'Extrusion PVC', 'Pas d''émission dangereuse', '- Aspiration', 'Acceptable', 'Propre', '2012-10-19 11:48:56', '2012-10-19 11:48:56'),
(53, 4, 'Fab contact AgSnO2', 'Les émissions métalliques => poudre du métal : toxicité pour l''environnement et l''homme (inhalation)\nconsommation importante d''énergie', 'Aspiration, filtration et collecte de la poussière en métal', 'Non Acceptable', 'Acceptable', '2012-10-19 11:48:56', '2012-10-19 11:48:56'),
(54, 15, 'Filage', 'Pas d''impact sur l''environnement', 'Aucun impact', 'Propre', 'Propre', '2012-10-19 11:48:57', '2012-10-19 11:48:57'),
(55, 16, 'Filtration COV traitement charbon actif', 'Procédé efficace seulement si les filtres sont changés régulièrement', 'Aucun impact', 'Propre', 'Propre', '2012-10-19 11:48:57', '2012-10-19 11:48:57'),
(56, 17, 'Frittage poudres métalliques', 'Forte consommation d''énergie', 'Aucun impact', 'Acceptable', 'Propre', '2012-10-19 11:48:57', '2012-10-19 11:48:57'),
(57, 2, 'Galvanisation à chaud', 'Consommation d''énergie', 'Aucun impact', 'Acceptable', 'Propre', '2012-10-19 11:48:57', '2012-10-19 11:48:57'),
(58, 14, 'Gravure par moulage', 'Gravure au moulage ne produit pas d''effet sur l''environnement et sur l''homme, car intégrée dans le moulage.', '"Propre" concerne le marquage du numéro de série pour le moulage des pièces. \nBesoin d''un investissement des moules: chaque référence spécifique a besoin d''un moule', 'Propre', 'Propre', '2012-10-19 11:48:57', '2012-10-19 11:48:57'),
(59, 2, 'Grenaillage, \nsablage', 'dégagements de poussières', 'cabine fermée \nfiltration des poussières', 'Propre', 'Propre', '2012-10-19 11:48:57', '2012-10-19 11:48:57'),
(60, 18, 'Imprégnation (en autoclave)', 'Emissions de COV \n - fuites de résine à l''autoclave', '-Employez un vernis sans substance RoHS \n=> enceinte fermée et hermétique : moins d''émission dans l''air \n-Filtres dans les évents\n-Développement d''une technologie qui permettra d''éviter la sur-remplissage et par conséquent, diminue la consommation de résine et les fuites', 'Non Acceptable', 'Acceptable', '2012-10-19 11:48:57', '2012-10-19 11:48:57'),
(61, 16, 'Incinération COV', '- Importante consommation d''énergie', 'Aucun impact', 'Acceptable', 'Acceptable', '2012-10-19 11:48:57', '2012-10-19 11:48:57'),
(62, 29, 'Retardateur halogéné', 'Les Bromes (PCB - Polybromobiphenyl) et PBDE (Polybromodiphenylether), composés de chlore et fluore sont interdits par RoHS \nLa substance HBCD sont interdits par REACH\nUtliser un autre retardateur de flamme', 'Impact non acceptable', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:48:57', '2012-10-19 11:48:57'),
(63, 29, 'Retardateur non halogèné', 'Ces composés ne sont pas de substances toxiques', 'Aucun impact', 'Propre', 'Propre', '2012-10-19 11:48:57', '2012-10-19 11:48:57'),
(64, 29, 'Electrique (ABS)', 'Moins d''émission de styrène avec l''injection mais le styrène est encore dangereux pour l''homme (cancérigènes)', '- Aspiration\n- Rassembler l''excès de la graisse et la réutiliser', 'Non Acceptable', 'Acceptable', '2012-10-19 11:48:57', '2012-10-19 11:48:57'),
(65, 29, 'Electrique (PET)', 'Thermoplastique sans substance halogénique\n - moins d''émissions', '- Aspiration\n- Rassembler l''excès de la graisse et la réutiliser', 'Acceptable', 'Propre', '2012-10-19 11:48:58', '2012-10-19 11:48:58'),
(66, 29, 'Electrique (phtalamide)', 'Thermoplatique sans substance halogène, mais contient la toxicité légère des isocyanates (asthme, allergies…)\n - moins d''émissions', '- Rassembler l''excès de la graisse et la réutiliser', 'Acceptable', 'Propre', '2012-10-19 11:48:58', '2012-10-19 11:48:58'),
(67, 29, 'Electrique (polyamide)', 'Thermoplatique sans substance halogène, mais contient la toxicité légère des isocyanates (asthme, allergies…)\n - moins d''émissions', '- Aspiration\n- Rassembler l''excès de la graisse et la réutiliser', 'Acceptable', 'Propre', '2012-10-19 11:48:58', '2012-10-19 11:48:58'),
(68, 29, 'Electrique (polycarbonate)', 'Thermoplastique sans substances halogènes\n - moins d''émissions', '- Aspiration\n- Rassembler l''excès de la graisse et la réutiliser', 'Acceptable', 'Propre', '2012-10-19 11:48:58', '2012-10-19 11:48:58'),
(69, 29, 'Electrique (polyethylene)', 'Thermoplastique sans substance halogéne\n -moins d'' émissions', '- Aspiration\n- Rassembler l''excès de la graisse et la réutiliser', 'Acceptable', 'Propre', '2012-10-19 11:48:58', '2012-10-19 11:48:58'),
(70, 29, 'Electrique (polystryrene)', 'Moins d''émissions de styrène', '- Aspiration\n- Rassembler l''excès de la graisse et la réutiliser\n- Contient le styrène : dangereux', 'Non Acceptable', 'Acceptable', '2012-10-19 11:48:58', '2012-10-19 11:48:58'),
(71, 29, 'Electrique (PVC)', 'Pendant le moulage du PVC, création de l''acide chlorhydrique => impact pour l''environnement et l''homme\n - moins d''émissions avec l''injection mais elle est encore dangereuse', 'Employez un autre thermoplastique ou employez le processus de l''extrusion avec le PVC', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:48:58', '2012-10-19 11:48:58'),
(72, 29, 'Electrique (polyurethane)', 'Thermoplatique sans substance halogéne, mais contient des isocyanates (asthme, allergéniques…)\n  - moins d''émissions', '- Aspiration  \n- Rassembler l''excès de la graisse et la réutiliser', 'Acceptable', 'Propre', '2012-10-19 11:48:58', '2012-10-19 11:48:58'),
(73, 29, 'Electrique PPOM ', 'Thermoplastique sans substances halogéniques\n - moins d''émissions', '- Aspiration\n- Rassembler l''excès de la graisse et la réutiliser', 'Acceptable', 'Propre', '2012-10-19 11:48:58', '2012-10-19 11:48:58'),
(74, 29, 'Hydraulique (ABS)', 'Moins d''émission de styrène avec l''inhjection mais le styrène est encore dangereux pour l''homme (cancérigènes) \névaporation hydraulique d''huile : circuit (poussière dans l'' air, fuites…), consommation pétrolière', '- Aspiration\n- Substituer par autre thermoplastique si possible', 'Non Acceptable', 'Acceptable', '2012-10-19 11:48:58', '2012-10-19 11:48:58'),
(75, 29, 'Hydraulique (PET)', 'Thermoplastique sans substance halogène\n - moins d''émissions \n - Évaporation hydraulique d''huile : circuit (poussière dans l''air, fuites…), consommation pétrolière', '- Aspiration', 'Acceptable', 'Acceptable', '2012-10-19 11:48:58', '2012-10-19 11:48:58'),
(76, 29, 'Hydraulique (phtalamide)', 'Thermoplastique sans substance halogénique\n - moins d''émissions\n - Évaporation hydraulique d''huile : circuit (poussière dans l'' air, fuites…), consommation pétrolière', '- Aspiration\n- Rassembler l''excès de la graisse et la réutiliser', 'Acceptable', 'Acceptable', '2012-10-19 11:48:59', '2012-10-19 11:48:59'),
(77, 29, 'Hydraulique (polyamide)', 'Thermoplastique sans substance halogène\n - moins d''émissions', '- Aspiration', 'Acceptable', 'Acceptable', '2012-10-19 11:48:59', '2012-10-19 11:48:59'),
(78, 29, 'Hydraulique (polycarbonate)', 'Thermoplastique sans substance halogène\n - moins d''émissions \n - Évaporation hydraulique d''huile : circuit (poussière dans l'' air, fuites…), consommation pétrolière', '- Aspiration', 'Acceptable', 'Acceptable', '2012-10-19 11:48:59', '2012-10-19 11:48:59'),
(79, 29, 'Hydraulique (polyethylene)', 'Thermoplastique sans substance halogène\n - moins d''émissions \n - Évaporation hydraulique d''huile : circuit (poussière dans l'' air, fuites…), consommation pétrolière', '- Aspiration', 'Acceptable', 'Acceptable', '2012-10-19 11:48:59', '2012-10-19 11:48:59'),
(80, 29, 'Hydraulique (polystryrene)', 'Moins d''émissions de styrène\n - émissions dues à l''ouverture de moule\n - Évaporation hydraulique d''huile : circuit (poussière dans l'' air, fuites…), consommation pétrolière', '- Aspiration\n- contient le styrène : dangereux', 'Non Acceptable', 'Acceptable', '2012-10-19 11:48:59', '2012-10-19 11:48:59'),
(81, 29, 'Hydraulique (polyurethane)', 'Thermoplatique sans substance halogéne, mais contient des isocyanates (asthme, allergéniques…)\n  - moins d''émissions \n - Évaporation hydraulique d''huile : circuit (poussière dans l'' air, fuites…), consommation pétrolière', '- Aspiration  \n- Rassembler l''excès de la graisse et la réutiliser', 'Acceptable', 'Acceptable', '2012-10-19 11:48:59', '2012-10-19 11:48:59'),
(82, 29, 'Hydraulique (PVC)', 'Pendant le moulage du PVC, création de l''acide chloridrique => impact pour l''environnement et l''homme\n - moins d''émissions avec l''injection mais elle est encore dangereuse\n - Évaporation hydraulique d''huile : de circuit (poussière dans l''air, fuites…), consommation d''huile', 'Employez un autre thermoplastique ou employez le processus de l''extrusion avec le PVC', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:48:59', '2012-10-19 11:48:59'),
(83, 29, 'Hydraulique PPOM', 'Thermoplastique sans substance halogénique\n - moins d''émissions \n - Évaporation hydraulique d''huile : circuit (poussière dans l'' air, fuites…), consommation pétrolière', '- Aspiration', 'Acceptable', 'Acceptable', '2012-10-19 11:48:59', '2012-10-19 11:48:59'),
(84, 29, 'Polyester (BMC/SMC)', 'Toxicité avec émission de styrène\nLes émissions de COV (moins que le moulage par compression)\nchutes de pièces => épuisement de ressource, ne peuvent pas être réutilisation \nDifficulté pour trouver une manière de valorisation les rebuts', 'Réduction des émissions COV => aspiration et filtration', 'Non Acceptable', 'Acceptable', '2012-10-19 11:48:59', '2012-10-19 11:48:59'),
(85, 29, 'Résine Epoxy', 'Les impacts sur l''environnement : \n  - L''utilisation des agents siliconés (interdit par REACH) \n  - Enfouissement des déchets', 'Il n''y a pas d''autre technique disponible dans cette voie, le moulage de résine époxy est considéré comme la "meilleure technologie disponible" (MTD), mais, il peut être utilisé dans les conditions: \n  - Mise en place de moyens d''aspiration et de filtration \n  - Utiliser les agents conformes', 'Non Acceptable', 'Acceptable', '2012-10-19 11:49:00', '2012-10-19 11:49:00'),
(86, 29, 'Retardateur phosphoré', 'Toxique pour l''environnement aquatique\nIrritant pour l''homme', '- Aspiration \n- Évitez les émissios dans l''eau ==> filtration et traitement', 'Non Acceptable', 'Acceptable', '2012-10-19 11:49:00', '2012-10-19 11:49:00'),
(87, 19, 'Injection Zamak/refroidissement air', 'Composés métalliques émis dans l''air \n  - L''utilisation des agents tq l''essence de pétrole', 'Aspiration des poussières avec un extracteur pour récupérer les particules qui sont émis dans l''air \nRecycler ou réutiliser les émissions et les poussières recueillies => Recyclage \nAspiration et de filtration des COV avec du charbon actif ==> traitement', 'Acceptable', 'Propre', '2012-10-19 11:49:00', '2012-10-19 11:49:00'),
(88, 19, 'Injection Zamak/refroidissement eau', 'Epuisement des ressources => chutes et les déchets ne peuvent pas être réutilisés dans le processus \nEmissions de métaux dans l''eau et l''air', 'Impact non acceptable', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:49:00', '2012-10-19 11:49:00'),
(89, 20, 'Installation utilisant du HCFC', 'Même si les HCFC sont moins dangereux pour l''environnement que le CFC, ils contribuent à : \n - réchauffement global (potentiel d''impact de réchauffement global)\n - l''appauvrissement de la couche d''ozone dans l''atmosphère', 'Impact non acceptable', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:49:00', '2012-10-19 11:49:00'),
(90, 20, 'Installation utilisant du HFC', 'Pas de chlore. Ainsi HFC contribuent « seulement » au réchauffement global comparé au HCFC et au CFC', 'les recommandations au sujet du réfrigérant sont \nR134a, R35, R152a, R407C, R410A, R417A', 'Non Acceptable', 'Propre', '2012-10-19 11:49:00', '2012-10-19 11:49:00'),
(91, 20, 'Installation utilisant le CFC', 'Très dangereux pour l''environnement (potentiel d''impact de réchauffement global) en raison de la présence du chlore, il contribue à l''appauvrissement de la couche d''ozone dans l''atmosphère', 'Pour la destruction du système de ventilation, réutiliser ou traiter le réfrigérant en centre agréé', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:49:00', '2012-10-19 11:49:00'),
(92, 21, 'Marquage encre', 'Aucun impact', '- Aspiration et filtration avec charbon actif\n - EPI pour le personnel', 'Non Acceptable', 'Acceptable', '2012-10-19 11:49:00', '2012-10-19 11:49:00'),
(93, 14, 'Marquage jet d''encre', 'Emissions de COV => la formation d''ozone photochimique (dans la troposphère) \nCadence de production + faible de la technique jet d''encre par rapport à la tampographie', 'A utiliser seulement pour la couleur de marquage dans les conditions suivantes: \n  - L''utilisation d''encre, conformément à REACH \n  - Biodegredable  \n- Ne pas utiliser pour le marquage noir => utiliser la technique laser pour ce type de marquage (+ économique)\n  - bain d''ence fermé => moins d''émissions', 'Non Acceptable', 'Acceptable', '2012-10-19 11:49:00', '2012-10-19 11:49:00'),
(94, 14, 'Marquage Laser', 'Procédé efficace et propre', 'Meilleure technique disponible concernant le marquage noir', 'Propre', 'Propre', '2012-10-19 11:49:00', '2012-10-19 11:49:00'),
(95, 14, 'Marquage par micro impacts', '', 'Aspiration et filtration des poussières', 'Propre', 'Propre', '2012-10-19 11:49:01', '2012-10-19 11:49:01'),
(96, 9, 'Moulage lubrifiant Si', 'Interdire les lubrifiants de moule au silicone (le silicone liquide libérant les particules volatiles)', 'L''utilisation du silicone utilisant l''agent est autorisée pour le moule de pièce en époxy ==> aucune alternative', 'Non Acceptable', 'Acceptable', '2012-10-19 11:49:01', '2012-10-19 11:49:01'),
(97, 2, 'Nickelage', 'Nécessite bcp d''énergie\nEmissions dans l''air et dans l''eau', 'Aucun impact', 'Acceptable', 'Acceptable', '2012-10-19 11:49:01', '2012-10-19 11:49:01'),
(98, 22, 'Peinture avec rideau d''eau', 'Des composés dangereux sont rejetés dans l''eau \nEmissions de COV', 'Réduire les émissions dans l''eau en circuit fermé de l''eau\nPrévenir les déchets dangereux (traitement par une industrie spécialisée)\nAspiration et filtration', 'Non Acceptable', 'Acceptable', '2012-10-19 11:49:01', '2012-10-19 11:49:01'),
(99, 22, 'Peinture avec solvant', 'Emissions de COV', 'Aspiration et filtration des COV avec traitement de charbon actif  \nEviter les émissions de la peinture dans le sol et permettre la collecte des effluents\nTraitement des déchets dangereuxde par entreprise spécialisée  \nVérifiez que le liquide avec de la peinture solvant est en conformité avec la REACH (Voir la fiche FDS)', 'Non Acceptable', 'Acceptable', '2012-10-19 11:49:01', '2012-10-19 11:49:01'),
(100, 22, 'Peinture hydrosoluble', 'Emissions de COV', 'Aspiration afin d''extraire les émissions COV\n Eviter les émissions de la peinture dans le sol et permettre la collecte de l''effluent', 'Acceptable', 'Propre', '2012-10-19 11:49:01', '2012-10-19 11:49:01'),
(101, 22, 'Peinture hydrosoluble par électrophorèse', 'Utilisation de peinture hydrosoluble : moins d''émissions de COV \nconsommation d''énergie importante', 'Filtration en continu par ultrafiltration : moins de consommations de ressources\nAspiration et filtration\nIncinération des COV et récupération de l''énergie', 'Acceptable', 'Propre', '2012-10-19 11:49:02', '2012-10-19 11:49:02'),
(102, 2, 'Passivation Cr 3', 'Emissions dans l''eau \nEmissions dans l''air', 'Traitement des effluents : \n - station d''épuration', 'Non Acceptable', 'Acceptable', '2012-10-19 11:49:02', '2012-10-19 11:49:02'),
(103, 2, 'Passivation  Cr 6', 'L''utilisation du chrome 6 est interdit par ROHS => pour l''homme et l''environnement ==> dangereux (allergènes, persistantes et toxiques pour l''environnement) \nEmissions dans l''eau \nEmissions dans l''air', 'Traitement des effluents : \n - station d''épuration\n - Traitement du Chrome 6 et plan de recherche de substitution du Cr6 en Cr3', 'Non Acceptable', 'Acceptable', '2012-10-19 11:49:02', '2012-10-19 11:49:02'),
(104, 22, 'Poudrage (avec RoHS et REACH)', 'Substances interdites\nEmissions volatiles de poudre importantes (70% de la poudre n''est pas projetée sur la pièce) : \n          - Danger pour l'' homme (inhalation) \n          - Toxicité pour l''environnement : infiltration dans le sol (contamination de l''eau souterraine) \nPerte de poudre importante', 'Changer le type de peinture en poudre (voir avec les fournisseurs)', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:49:03', '2012-10-19 11:49:03'),
(105, 22, 'Poudrage (sans RoHS et REACH)', 'Emissions volatiles de la peinture de poudre (limité en raison de la caractéristique électrostatique)\n=>Danger limité pour l''homme (inhalation)', 'Aspirez et collecter la poudre afin de réduire les émissions dans l''air et le sol par filtration\nLa cabine en dépression afin de récupérer la poudre puis traiter comme déchets dangereux', 'Acceptable', 'Propre', '2012-10-19 11:49:03', '2012-10-19 11:49:03'),
(106, 4, 'Process chimique AgSnO2 ', 'Émission dans l''air et l''eau de substances dangereuses (produit chimique)', 'Impact non acceptable', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:49:03', '2012-10-19 11:49:03'),
(107, 23, 'Rivetage Acier', 'Aucun impact', 'Aucun impact', 'Propre', 'Propre', '2012-10-19 11:49:03', '2012-10-19 11:49:03'),
(108, 24, 'Rivetage Alu', 'Aucun impact', 'Aucun impact', 'Propre', 'Propre', '2012-10-19 11:49:03', '2012-10-19 11:49:03'),
(109, 25, 'Sertissage', 'Aucun impact', 'Aucun impact', 'Propre', 'Propre', '2012-10-19 11:49:03', '2012-10-19 11:49:03'),
(110, 23, 'Arc sous gaz avec fil électrode', 'Les fumées sont provoquées par le métal supplémentaire. Avec cette technique les émissions sont importantes', 'Ne pas employer le cadmium, le plomb, le chrome VI et l''oxyde de nickel comme métal supplémentaire \nLes besoins : \n - Aspiration et filtration\n - EPI pour le personnel', 'Non Acceptable', 'Acceptable', '2012-10-19 11:49:03', '2012-10-19 11:49:03'),
(111, 23, 'Manuel (chalumeau)', 'Émissions de COV (alcool d''isopropyle)', '- Aspiration et filtration\n - EPI pour le personnel', 'Non Acceptable', 'Acceptable', '2012-10-19 11:49:03', '2012-10-19 11:49:03'),
(112, 23, 'Par points (soudage/résistance)', 'Deux électrodes sont simultanément utilisées pour maintenir les pièces ensemble et pour passer le courant par les pièces.\nPoussières et fumées', '- Aspiration/poste', 'Propre', 'Propre', '2012-10-19 11:49:03', '2012-10-19 11:49:03'),
(113, 6, 'Soudure à la vague', 'Aspirations des fumées', 'Installation :\n- Filtration d''aspiration des COV \n- Cabine fermée et hermétique\n- Substances RoHS et REACH interdites', 'Acceptable', 'Propre', '2012-10-19 11:49:04', '2012-10-19 11:49:04'),
(114, 23, 'Arc élec/utilisation de poudre', 'Ce processus automatisé ne génère pas d''impact sur l''homme mais quelques impacts sur l''environnement', 'N''employez pas le cadmium, le plomb, le chrome VI et l''oxyde de nickel comme le métal supplémentaire. \nLes besoins : \n - Aspiration et filtration (épurateur humide)', 'Non Acceptable', 'Acceptable', '2012-10-19 11:49:04', '2012-10-19 11:49:04'),
(115, 6, 'Soudure auto composants', 'Installation équipée d''une hotte au dessus du poste de collage', 'Aspiration et filtration des COV', 'Acceptable', 'Propre', '2012-10-19 11:49:04', '2012-10-19 11:49:04'),
(116, 23, 'Outillage manuel', 'Aucune fumée de matériaux n''est provoquée par la fusion des pièces', '- Aspiration/poste', 'Acceptable', 'Propre', '2012-10-19 11:49:04', '2012-10-19 11:49:04'),
(117, 23, 'MAG', 'Consommation énergie', '- Aspiration et filtration\n - EPI pour le personnel', 'Acceptable', 'Propre', '2012-10-19 11:49:04', '2012-10-19 11:49:04'),
(118, 23, 'Laser', 'Aucune fumée n''est provoquée par la fusion des pièces', '- Aspiration/poste', 'Propre', 'Propre', '2012-10-19 11:49:04', '2012-10-19 11:49:04'),
(119, 23, 'MIG (sans métal)', 'Poussières et fumées provoquées entre autre par le métal d''apport', 'Ne pas employer le cadmium, le plomb, le chrome VI et l''oxyde de nickel comme métal d''apport\nLes besoins : \n - Aspiration/poste \n - EPI pour le personnel', 'Acceptable', 'Propre', '2012-10-19 11:49:04', '2012-10-19 11:49:04'),
(120, 23, 'Par faisceau d''électrons', 'Pas d'' impact sur l''environnement et l''homme en raison de l''installation très technique (clôturée et hermétique) . Aucun matériel supplémentaire', 'haut investissement', 'Propre', 'Propre', '2012-10-19 11:49:04', '2012-10-19 11:49:04'),
(121, 23, 'Par friction', 'Aucune fumée de matériaux n''est provoquée par la fusion des pièces', '- Aspiration/poste', 'Propre', 'Propre', '2012-10-19 11:49:04', '2012-10-19 11:49:04'),
(122, 23, 'Par point  Acier', 'Consommation énergie', '- Aspiration et filtration\n - EPI pour le personnel', 'Acceptable', 'Propre', '2012-10-19 11:49:05', '2012-10-19 11:49:05'),
(123, 23, 'Par point  Alu', 'Consommation énergie', '- Aspiration et filtration\n - EPI pour le personnel', 'Acceptable', 'Propre', '2012-10-19 11:49:05', '2012-10-19 11:49:05'),
(124, 23, 'Ultrasons', 'Aucune fumée de matériaux n''est provoquée par la fusion des pièces', 'Ne l''utiliser que pour la fusion de matériaux plastiques (emballage)\n - Aspiration/poste', 'Propre', 'Propre', '2012-10-19 11:49:05', '2012-10-19 11:49:05'),
(125, 23, 'Plasma', 'Poussières et fumées', '- Aspiration/poste', 'Propre', 'Propre', '2012-10-19 11:49:05', '2012-10-19 11:49:05'),
(126, 23, 'TIG/électrode tungstène', 'Poussières et fumées', 'Aspiration/poste \nAspiration lors du meulage de l''électrode\nEPI pour le personnel', 'Acceptable', 'Propre', '2012-10-19 11:49:05', '2012-10-19 11:49:05'),
(127, 14, 'Tampographie', 'Emissions de COV => formation d''ozone photochimique + toxicité pour l''homme', 'Ce marquage est recommandé pour le marquage de couleur (si demandé par clients) \nConditions d''utilisation : \n  - utilisation d''encre, conformément à REACH \npour le marquage noir mieux vaut utiliser la technique laser pour ce type de marquage (+ économique)\n  - solvant biodegredable (si possible)\n  - Encrier fermé => moins d''émissions \n  - Traitement des déchets', 'Non Acceptable', 'Acceptable', '2012-10-19 11:49:05', '2012-10-19 11:49:05'),
(128, 12, 'Acide chlorhydrique /base', 'Utilisation de préparations de nature basique pourrait être un problème, le pH est modifié => dangerosité pour la flore aquatique et faune \nUtilisation de l''acide chlorhydrique => eutrophisation de l''eau \nEn ce qui concerne l''homme, l'' acide chlorhydrique est très irritable', 'Ne pas utiliser l''acide chlorhydrique => remplacement par l''acide phosphorique', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:49:05', '2012-10-19 11:49:05'),
(129, 12, 'Bain solvant chloré', 'Emissions COV => participation à la création d''ozone dans la troposphère', 'Choisir un solvant organique présentant moins d'' impacts sur l''environnement et l''homme (nature des COV) \nInstaller une aspiration et une filtration \nPlacer le bain sur un bac de rétention afin d''éviter l''infiltration des substances dangereuses dans le sol', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:49:05', '2012-10-19 11:49:05'),
(130, 12, 'Fontaine à bactéries', 'Propreté et efficace \nMais pas toujours adapté pour toutes les applications (par exemple une application adaptée est le lavage des outillages)', 'Aucun impact', 'Propre', 'Propre', '2012-10-19 11:49:06', '2012-10-19 11:49:06'),
(131, 12, 'Fontaine solvant chloré', 'Emissions COV => participation à la création d''ozone dans la troposphère \nSubstances toxiques pour l'' homme', 'Quand il est possible (parfois le bain est mobile), mettre une aspiration et de filtration des COV avec du charbon actif traitement', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:49:06', '2012-10-19 11:49:06'),
(132, 2, 'Zamak passivation Cr 6', 'L''utilisation du chrome 6 est interdit par ROHS => pour l''homme et l''environnement ==> dangereux (allergènes, persistantes et toxiques pour l''environnement)', 'Ne pas utiliser le chrome VI => remplacement par chrome III', 'Non Acceptable', 'Acceptable', '2012-10-19 11:49:06', '2012-10-19 11:49:06'),
(133, 2, 'Zamak passivation (Cr)', 'Les principaux problèmes sont les suivants : \n  - Rinçage avant le placage => utilisation de carbonate de sodium, phospophate => toxiques pour l''environnement \n  - Les émissions dans l''air => irritation sur l''homme et dangerosité pour les écosystèmes', 'Conditions : \n  - L''eau (de rinçage) de traitement par une société spécialisée (DIS) ou à l''intérieur de l''unité de fabrication (station de traitement de l''eau) \n  - Les émissions dans l''air : aspiration et de lavage de gaz d''usinage ou évaporateur', 'Acceptable', 'Propre', '2012-10-19 11:49:06', '2012-10-19 11:49:06'),
(134, 16, 'Traitement physico-chimique', 'Aucun impact', 'Aucun impact', 'Propre', 'Propre', '2012-10-19 11:49:06', '2012-10-19 11:49:06'),
(135, 21, 'Transfert thermique', 'Le film en polyester où se trouve l''encre durcit est non recyclable. Le traitement de ces déchets pose problème: déchet incinérés. Conséquences sur les emissions dans l''atmosphere', 'Impact non acceptable', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:49:06', '2012-10-19 11:49:06'),
(136, 26, 'Trempe thermique', 'Forte consommation d''énergie', 'Aucun impact', 'Acceptable', 'Acceptable', '2012-10-19 11:49:06', '2012-10-19 11:49:06'),
(137, 2, 'Zingage Cr 3', 'Dégagement de poussières de zinc considéré comme dangereux pour l''environnement et l''homme (inhalation)\nLa poussière de zinc (effets de pesanteur) sur le sol \n     - infiltration et pénétration dans le sol et l''eau souterraine \n     - infiltration dans l''eau de surface \ntraces de cadmium\népuisement de ressource (minerai)', 'Afin d''éviter les émissions de la poudre de zinc dans le sol (sur les postes de travail de projection de zinc) et permettre le rassemblement de l''effluent\n - Aspirer et collecter les poussières sur sol', 'Non Acceptable', 'Acceptable', '2012-10-19 11:49:07', '2012-10-19 11:49:07'),
(138, 27, 'Huile insoluble et chlore', 'Émissions dans l''air : projection de vapeurs d''huile, pulvérisation d''huile (inhalation des composés chlorés)\r\népuisement important de ressources', 'Impact non acceptable', 'Non Acceptable', 'Non Acceptable', '2012-10-19 11:48:55', '2012-10-19 11:48:55'),
(140, 27, 'Micro pulvérisation (sans chlore)', 'Huile sans chlore plus favorable à l''environnement : réutilisation du métal possible\r\nLa micro-pulvérisation consomme moins de ressources et moins d''huile sur lesz pièces\r\nLe dégraissage est plus facile mais : \r\n - émissions dans l''air : projection de vapeurs d''huile\r\n - traitement d''huile', 'Réutilisation de l''huile en circuit fermé afin de diminuer la consommation de matières premières\r\nCollecter les déchets de découpage (installation magnétique)', 'Acceptable', 'Propre', '2012-10-19 11:48:55', '2012-10-19 11:48:55'),
(141, 27, 'Sans huile', 'Aucun impact sur l''environnement  : processus propre', 'Aucun impact', 'Propre', 'Propre', '2012-10-19 11:48:55', '2012-10-19 11:48:55'),
(142, 27, 'Sans chlore', 'Huile plus favorable à l''environnement : sans chlore, réutilisation du métal possible mais : \r\n - émissions d''air : projection de vapeurs d''huile', 'Réutilisation de l''huile en circuit fermé afin de diminuer la consommation de matières premières\r\nCollecter les déchets de découpage (installation magnétique)', 'Acceptable', 'Propre', '2012-10-19 11:48:55', '2012-10-19 11:48:55');

-- --------------------------------------------------------

--
-- Structure de la table `product_impact_icpe`
--

CREATE TABLE IF NOT EXISTS `product_impact_icpe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `processicpe_id` int(11) DEFAULT NULL,
  `answer` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `curr_qty` varchar(255) DEFAULT NULL,
  `new_qty` varchar(255) DEFAULT NULL,
  `supp_qty` varchar(255) DEFAULT NULL,
  `stockageicpe_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_AA3C0B1A166D1F9C` (`project_id`),
  KEY `IDX_AA3C0B1A52D72944` (`processicpe_id`),
  KEY `IDX_AA3C0B1A54C38D61` (`stockageicpe_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=64 ;

--
-- Contenu de la table `product_impact_icpe`
--

INSERT INTO `product_impact_icpe` (`id`, `project_id`, `processicpe_id`, `answer`, `created_at`, `updated_at`, `curr_qty`, `new_qty`, `supp_qty`, `stockageicpe_id`) VALUES
(33, 1, 1, 1, '2012-10-26 19:24:09', '2012-10-26 19:24:09', '12', '323', '2321', NULL),
(34, 1, 2, 0, '2012-10-26 19:24:09', '2012-10-26 19:24:09', '3233', '3323', '2424', NULL),
(35, 1, 3, 0, '2012-10-26 19:24:09', '2012-10-26 19:24:09', '31331', '333', '2442', NULL),
(36, 1, 4, 1, '2012-10-26 19:24:09', '2012-10-26 19:24:09', '4242', '433', '663223', NULL),
(37, 1, NULL, 0, '2012-10-26 19:24:09', '2012-10-26 19:24:09', '23', '33', '234', 2),
(38, 1, NULL, 0, '2012-10-26 19:24:09', '2012-10-26 19:24:09', '442', '323', '5', 3),
(39, 1, NULL, 0, '2012-10-26 19:24:09', '2012-10-26 19:24:09', '434', '54', '46', 4),
(40, 1, NULL, 1, '2012-10-26 19:24:09', '2012-10-26 19:24:09', '324', '68', '64', 5),
(63, 1, NULL, 1, '2012-10-26 20:17:09', '2012-10-26 20:17:09', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `product_part`
--

CREATE TABLE IF NOT EXISTS `product_part` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `family_id` int(11) DEFAULT NULL,
  `name` varchar(32) NOT NULL,
  `sitename` varchar(50) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C4E75267166D1F9C` (`project_id`),
  KEY `IDX_C4E75267C35E566A` (`family_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `product_part`
--

INSERT INTO `product_part` (`id`, `project_id`, `family_id`, `name`, `sitename`, `country`, `created_at`, `updated_at`) VALUES
(1, 1, 102, 'Pièce 1', 'Legrand Malaunay', 'France', '2012-10-22 00:00:00', '2012-10-23 18:41:38'),
(2, 1, 140, 'PIèce 2', 'Legrand Malaunay', 'France', '2012-10-23 14:11:12', '2012-10-26 16:42:28');

-- --------------------------------------------------------

--
-- Structure de la table `product_process`
--

CREATE TABLE IF NOT EXISTS `product_process` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Contenu de la table `product_process`
--

INSERT INTO `product_process` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'Traitement de surface', '2012-10-19 11:42:04', '2012-10-19 11:42:04'),
(3, 'Assemblage', '2012-10-19 11:42:04', '2012-10-19 11:42:04'),
(4, 'Contact électrique', '2012-10-19 11:42:04', '2012-10-19 11:42:04'),
(5, 'Bobinage', '2012-10-19 11:42:04', '2012-10-19 11:42:04'),
(6, 'Brasage', '2012-10-19 11:42:04', '2012-10-19 11:42:04'),
(7, 'Clinchage', '2012-10-19 11:42:04', '2012-10-19 11:42:04'),
(8, 'Collage', '2012-10-19 11:42:04', '2012-10-19 11:42:04'),
(9, 'Moulage', '2012-10-19 11:42:04', '2012-10-19 11:42:04'),
(10, 'Découpe', '2012-10-19 11:42:04', '2012-10-19 11:42:04'),
(11, 'Découpe, pliage', '2012-10-19 11:42:05', '2012-10-19 11:42:05'),
(12, 'Dégraissage', '2012-10-19 11:42:05', '2012-10-19 11:42:05'),
(13, 'Dépose joint', '2012-10-19 11:42:05', '2012-10-19 11:42:05'),
(14, 'Marquage', '2012-10-19 11:42:05', '2012-10-19 11:42:05'),
(15, 'Filage', '2012-10-19 11:42:05', '2012-10-19 11:42:05'),
(16, 'Traitement effluents', '2012-10-19 11:42:05', '2012-10-19 11:42:05'),
(17, 'Frittage', '2012-10-19 11:42:05', '2012-10-19 11:42:05'),
(18, 'Imprégnation', '2012-10-19 11:42:05', '2012-10-19 11:42:05'),
(19, 'Injection Zamac', '2012-10-19 11:42:05', '2012-10-19 11:42:05'),
(20, 'Refroidissement', '2012-10-19 11:42:05', '2012-10-19 11:42:05'),
(21, 'Identification', '2012-10-19 11:42:05', '2012-10-19 11:42:05'),
(22, 'Peinture', '2012-10-19 11:42:05', '2012-10-19 11:42:05'),
(23, 'Soudage', '2012-10-19 11:42:06', '2012-10-19 11:42:06'),
(24, 'Rivetage', '2012-10-19 11:42:06', '2012-10-19 11:42:06'),
(25, 'Sertissage', '2012-10-19 11:42:06', '2012-10-19 11:42:06'),
(26, 'Trempe thermique', '2012-10-19 11:42:06', '2012-10-19 11:42:06'),
(27, 'Décolletage', '2012-10-23 00:00:00', '2012-10-23 00:00:00'),
(28, 'Moulage/Compression', '2012-10-23 00:00:00', '2012-10-23 00:00:00'),
(29, 'Moulage/Injection', '2012-10-23 00:00:00', '2012-10-23 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `product_process_icpe`
--

CREATE TABLE IF NOT EXISTS `product_process_icpe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `unit` varchar(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `product_process_icpe`
--

INSERT INTO `product_process_icpe` (`id`, `name`, `unit`, `created_at`, `updated_at`) VALUES
(1, 'Investissement nouvelle machine pour la<br>  transformation matières plastiques,<br>caoutchouc, résines, adhésifs', 'T/j', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Investissement nouvelle machine pour le<br> travail mécanique des métaux et alliages', 'kW', '2012-10-25 00:00:00', '2012-10-25 00:00:00'),
(3, 'Investissement de cuve de nettoyage,<br> dégraissage, décapage, polissage,... des<br> métaux et matières plastiques', 'm3', '2012-10-25 00:00:00', '2012-10-25 00:00:00'),
(4, 'Investissement nouvelle machine ou<br> équipement (refroidissement, combustion,…)', '', '2012-10-25 00:00:00', '2012-10-25 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `product_project`
--

CREATE TABLE IF NOT EXISTS `product_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniq` varchar(32) NOT NULL,
  `name` varchar(32) NOT NULL,
  `pilote` varchar(255) DEFAULT NULL,
  `pilote_date` date DEFAULT NULL,
  `path` varchar(100) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `reference` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `product_project`
--

INSERT INTO `product_project` (`id`, `uniq`, `name`, `pilote`, `pilote_date`, `path`, `active`, `created_at`, `updated_at`, `reference`, `numero`) VALUES
(1, 'f3fce0bb1842eb4c575bb588389737e2', 'Projet test', 'Antoine', '2012-10-19', '', 1, '2012-10-19 10:16:24', '2012-10-22 09:48:49', 0, 1),
(2, 'd52e5938b955391a86559d897890c6b5', 'ffd', 'Laura', '2012-10-19', '', 1, '2012-10-28 10:40:03', '2012-10-28 10:40:03', 0, 2),
(3, '296f3ca0943f302d351cd7baa8d4a7cf', 'ffd', NULL, NULL, NULL, 1, '2012-10-28 10:40:04', '2012-10-28 10:40:04', 0, 3),
(4, '45558fe2357306bff72d5b08da68aadb', 'laura', NULL, NULL, NULL, 1, '2012-10-29 15:14:02', '2012-10-29 15:14:02', 0, 4),
(5, '1ef21bc46c891c9e3287c2ecf4503eb2', 'Projet 1', NULL, NULL, NULL, 1, '2012-11-06 13:02:55', '2012-11-06 13:02:55', 0, 0),
(6, '5ca0e2fc955738549e6f76c75737fc5c', 'Projet 1', NULL, NULL, NULL, 1, '2012-11-06 13:23:31', '2012-11-06 13:23:31', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `product_regime`
--

CREATE TABLE IF NOT EXISTS `product_regime` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `product_regime`
--

INSERT INTO `product_regime` (`id`, `name`, `created_at`, `updated_at`) VALUES
(3, 'AS', '2012-10-29 00:00:00', '2012-10-29 00:00:00'),
(4, 'A', '2012-10-29 00:00:00', '2012-10-29 00:00:00'),
(5, 'DC', '2012-10-29 00:00:00', '2012-10-29 00:00:00'),
(6, 'D', '2012-10-29 00:00:00', '2012-10-29 00:00:00'),
(7, 'E', '2012-10-29 00:00:00', '2012-10-29 00:00:00'),
(8, 'Non classé', '2012-10-29 00:00:00', '2012-10-29 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `product_rubriques`
--

CREATE TABLE IF NOT EXISTS `product_rubriques` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `activites` longtext NOT NULL,
  `seuil_declaration` longtext,
  `seuil_enregistrement` longtext,
  `seuil_autorisation` longtext,
  `seuil_autorisation_servitudes` longtext,
  `unit` longtext,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `seuil_declaration_controle` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=77 ;

--
-- Contenu de la table `product_rubriques`
--

INSERT INTO `product_rubriques` (`id`, `name`, `activites`, `seuil_declaration`, `seuil_enregistrement`, `seuil_autorisation`, `seuil_autorisation_servitudes`, `unit`, `created_at`, `updated_at`, `seuil_declaration_controle`) VALUES
(1, '1131-1', 'Emploi stokage toxique-substances et préparations solides', '5', '', '50', '200', 't', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(2, '1131-2', 'Emploi stokage toxique-substances et préparations liquides', '1', '', '10', '200', 't', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(3, '1131-3', 'Emploi stokage toxique-gaz ou gaz liquéfiés', '0,4', '', '4', '200', 't', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(4, '1136 A-1', 'Ammoniac (emploi ou stockage de l'') - stokage en récipients de capacité unitaire sup à 50kg', '', '', '0,15', '200', 't', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(5, '1136 A-2', 'Ammoniac (emploi ou stockage de l'') - stokage en récipients de capacité unitaire inf à 50kg', '', '', '5', '200', 't', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0,15'),
(6, '1136 B', 'Ammoniac (emploi ou stockage de l'') - emploi', '', '', '1,5', '200', 't', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0,15'),
(7, '1158', 'fabrication, emploi stockage MDI', '', '', '20', '', 't', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2'),
(8, '1175', 'Organohalogénés (emploi ou stockage de liquides)', '200', '', '1500', '', 'L', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(9, '1180-1', 'polychlorobiphényl, polychloroterphényl', '30', '', '', '', 'L', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(10, '1185-1', 'chlorofluorocarbures, halons ou autres hydrocarbures halogénés- conditionnement de fluides tq fab de mousses,…', '80', '', '800', '', 'l', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(11, '1185-2', 'chlorofluorocarbures, halons ou autres hydrocarbures halogénés- composants et appareils clos en exploitation', '200', '', '', '', 'kg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(12, '1185-3', 'chlorofluorocarbures, halons ou autres hydrocarbures halogénés- régénération et recyclage des fluides des halons, sur site de traitement', '', '', '0,01', '', 'L', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(13, '1200-1', 'fabrication de comburants', '', '', '200', '200', 't', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(14, '1200-2', 'Emploi ou stockage de comburants', '2', '', '50', '200', 't', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(15, '1220', 'emploi stockage oxygène', '2', '', '200', '2000', 't', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(16, '1411-2', 'Gazomètres et réservoirs de gaz comprimés renfermant des gaz inflammables', '1', '', '10', '200', 't', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(17, '1412', 'stockage reservoir manufacturés de gaz inflammable liquéfiés', '', '', '50', '200', 't', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '6'),
(18, '1414-1', 'Gaz inflammables liquéfiés (installation de remplissage ou de distribution de)- installations de remplissage de bouteilles ou conteneurs', '', '', '', '0,01', 'L', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(19, '1414-2', 'Gaz inflammables liquéfiés (installation de remplissage ou de distribution de)- installations de chargt ou déchargt désservant un dépôt de gaz infl soumis à Autorisation', '', '', '', '0,01', 'L', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(20, '1414-3', 'Gaz inflammables liquéfiés (installation de remplissage ou de distribution de)- installations de remplissage de réservoirs alimentant des moteurs …', '0,01', '', '', '', 'L', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(21, '1416', 'Hydrogène (stockage ou emploi de l'')', '0,1', '', '1', '50', 't', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(22, '1418', 'stockage ou emploi d''acétylène', '0,1', '', '1', '50', 't', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(23, '1419 A', 'Oxyde d''éthylène ou de propylène (fabrication, stockage ou emploi de l'') - fabrication', '', '', '50', '50', 't', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(24, '1419 B', 'Oxyde d''éthylène ou de propylène (fabrication, stockage ou emploi de l'') - stockage ou emploi', '0,5', '', '5', '50', 't', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(25, '1432-2', 'stockage reservoir manufacturés de liquide inflammable', '10', '', '100', '', 'm3', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(26, '1433 A', 'Installations de mélange ou d''emploi de liquides inflammables - installations de simple mélange à froid', '', '', '50', '', 't', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '5'),
(27, '1433 B', 'Installations de mélange ou d''emploi de liquides inflammables - autres installations ', '', '', '10', '', 't', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1'),
(28, '1510', 'Stockage de matières, produits ou substances combustibles dans des entrepôts couverts', '', '50000', '300000', '', 'm3', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '5000'),
(29, '1530', 'Dépôts de papiers, cartons ou matériaux combustibles analogues', '1000', '20000', '50000', '', 'm3', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(30, '1532', 'Dépôt de bois sec ou matériaux combustibles analogues', '1000', '', '20000', '', 'm3', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(31, '1611', 'Emploi ou stockage d''acides', '50', '', '250', '', 't', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(32, '1630 A', 'Fabrication industrielle de lessives de soude ou de potasse caustique', '', '', '0,01', '', 't', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(33, '1630 B', 'Emploi ou stockage de lessives de soude ou de potasse caustique', '100', '', '250', '', 't', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(34, '2410', 'Ateliers ou l''on travaille le bois ou matériaux combustibles analogues', '50', '', '200', '', 'kW', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(35, '2450-1', 'Imprimeries ou ateliers de reproduction graphique sur tout support tel que métal, papier, carton (fléxogravure) - Offset utilisant des rotatives à séchage thermique', '', '', '0,01', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(36, '2450-2', 'Imprimeries ou ateliers de reproduction graphique sur tout support tel que métal, papier, carton (fléxogravure)', '50', '', '200', '', 'kg/J', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(37, '2450-3', 'Imprimeries ou ateliers de reproduction graphique sur tout support tel que métal, papier, carton', '100', '', '400', '', 'kg/J', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(38, '2552', 'Fonderie (fabrication de produits moulés) de métaux et alliages non-ferreux', '', '', '2', '', 't/J', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0,1'),
(39, '2560', 'Travail mécanique des métaux et alliages', '', '750', '', '', 'kW', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '50'),
(40, '2561', 'Trempé recuit, revenu des métaux et alliages', '', '', '', '', 'kW', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0,01'),
(41, '2564-1', 'Nettoyage, dégraissage, décapage de surfaces utilisant des liquides organohalogénés ou des solvants organiques', '', '', '1500', '', 'L', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(42, '2564-2', 'Nettoyage, dégraissage, décapage de surfaces utilisant des liquides organohalogénés ou des solvants organiques', '', '', '', '', 'L', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '200'),
(43, '2564-3', 'Nettoyage, dégraissage, décapage de surfaces utilisant des liquides organohalogénés ou des solvants organiques', '', '', '', '', 'L', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '20'),
(44, '2565-2', 'Revêtement métallique ou traitement de surfaces par voie électrolytique ou chimique  (liquide sans cd)', '', '', '1500', '', 'L', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '200'),
(45, '2565-3', 'Revêtement métallique ou traitement de surfaces par voie électrolytique ou chimique  (liquide sans cd)- traitement en phase gazeuse ou autres traitements sans cadmium', '', '', '', '', 'L', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0,01'),
(46, '2565-4', 'Revêtement métallique ou traitement de surfaces par voie électrolytique ou chimique (vibro-abrasion)', '', '', '', '', 'L', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '200'),
(47, '2575', 'Emploi de matières abrasives', '20', '', '', '', 'kW', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(48, '2640', 'Colorants et pigments organiques, minéraux et naturels (fabrication industrielle, emploi de)', '0,2', '', '2', '', 't/J', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(49, '2661-1', 'Transformation de polymères (injection extrusion)', '1', '', '10', '', 't/J', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(50, '2661-2', 'Transformation de polymères (mécanique)', '2', '', '20', '', 't/J', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(51, '2662', 'Stockage de polymères', '100', '1000', '40000', '', 'm3', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(52, '2663-1', 'Stockage de pneumatiques et produits composés d’au moins 50 % de polymères (alvéolaires)', '200', '2000', '45000', '', 'm3', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(65, '2663-2', 'Stockage de pneumatiques et produits composés d’au moins 50 % de polymères', '1000', '10000', '80000', '', 'm3', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(66, '2910-A', 'Installations de combustion', '', '', '20', '', 'MW', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2'),
(67, '2915-1', 'Chauffage utilisant des fluides caloporteurs TU > TE', '100', '', '1000', '', 'L', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(68, '2915-2', 'Chauffage utilisant des fluides caloporteurs TU < TE', '250', '', '', '', 'L', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(69, '2920', 'Installation de compression', '', '', '10', '', 'MW', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(70, '2921-1', 'Refroidissement par dispersion d’eau dans un flux d’air (fermé)', '2000', '', '2000', '', 'kW', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(71, '2921-2', 'Refroidissement par dispersion d’eau dans un flux d’air (ouvert)', '0,01', '', '', '', 'kW', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(72, '2925', 'Charge d''accumulateurs', '50', '', '', '', 'kW', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(73, '2930', 'Ateliers de réparation et d''entretien de véhicules et engins à moteur,', '', '', '5000', '', 'm²', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2000'),
(74, '2940-1', 'Application, cuisson, séchage de vernis, peinture, apprêt, colle, enduit, etc.Liquide au trempé', '', '', '1000', '', 'L', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '100'),
(75, '2940-2', 'Application, cuisson, séchage de vernis, peinture, apprêt, colle, enduit, etc.Liquide autre que trempé', '', '', '100', '', 'kg/J', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '10'),
(76, '2940-3', 'Application, cuisson, séchage de vernis, peinture, apprêt, colle, enduit, etc.Poudre', '', '', '200', '', 'kg/J', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '20');

-- --------------------------------------------------------

--
-- Structure de la table `product_rubriques_evaluation`
--

CREATE TABLE IF NOT EXISTS `product_rubriques_evaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rubrique_id` int(11) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `commentaire` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `evaluationIcpe_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6EE455CC3BD38833` (`rubrique_id`),
  KEY `IDX_6EE455CC9DF9BB88` (`evaluationIcpe_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `product_rubriques_evaluation`
--

INSERT INTO `product_rubriques_evaluation` (`id`, `rubrique_id`, `value`, `commentaire`, `created_at`, `updated_at`, `evaluationIcpe_id`) VALUES
(1, 10, '240', 'Test', '2012-11-05 00:00:00', '2012-11-05 00:00:00', 1);

-- --------------------------------------------------------

--
-- Structure de la table `product_situation_administrative`
--

CREATE TABLE IF NOT EXISTS `product_situation_administrative` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `regime_id` int(11) DEFAULT NULL,
  `volume_activity` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_92A7098335E7D534` (`regime_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `product_situation_administrative`
--

INSERT INTO `product_situation_administrative` (`id`, `regime_id`, `volume_activity`, `created_at`, `updated_at`) VALUES
(1, 3, '300', '2012-10-29 00:00:00', '2012-10-29 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `product_situation_reelle`
--

CREATE TABLE IF NOT EXISTS `product_situation_reelle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `regime_id` int(11) DEFAULT NULL,
  `volume_activity` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_198812F735E7D534` (`regime_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `product_situation_reelle`
--

INSERT INTO `product_situation_reelle` (`id`, `regime_id`, `volume_activity`, `created_at`, `updated_at`) VALUES
(1, 8, '500', '2012-10-29 00:00:00', '2012-10-29 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `product_stockage_icpe`
--

CREATE TABLE IF NOT EXISTS `product_stockage_icpe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `unit` varchar(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `product_stockage_icpe`
--

INSERT INTO `product_stockage_icpe` (`id`, `name`, `unit`, `created_at`, `updated_at`) VALUES
(1, 'Matières plastiques :', '', '2012-10-25 00:00:00', '2012-10-25 00:00:00'),
(2, 'Matières premières', 'm3/an', '2012-10-25 00:00:00', '2012-10-25 00:00:00'),
(3, 'Sous-ensembles', 'm3/an', '2012-10-25 00:00:00', '2012-10-25 00:00:00'),
(4, 'Produits finis', 'm3/an', '2012-10-25 00:00:00', '2012-10-25 00:00:00'),
(5, 'Produit à usage réglementé tels que<br> Isocyanate, solvant, hydrocarbures, gaz, …', 'Kg/an', '2012-10-25 00:00:00', '2012-10-25 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `product_user`
--

CREATE TABLE IF NOT EXISTS `product_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `role` varchar(255) NOT NULL,
  `company` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `tel` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7BF4E8166D1F9C` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `product_activite`
--
ALTER TABLE `product_activite`
  ADD CONSTRAINT `FK_73C53F82141C302F` FOREIGN KEY (`situation_reelle_id`) REFERENCES `product_situation_reelle` (`id`),
  ADD CONSTRAINT `FK_73C53F82166D1F9C` FOREIGN KEY (`project_id`) REFERENCES `product_project` (`id`),
  ADD CONSTRAINT `FK_73C53F823BD38833` FOREIGN KEY (`rubrique_id`) REFERENCES `product_rubriques` (`id`),
  ADD CONSTRAINT `FK_73C53F828147D86B` FOREIGN KEY (`situation_administrative_id`) REFERENCES `product_situation_administrative` (`id`),
  ADD CONSTRAINT `FK_73C53F8294F3C51` FOREIGN KEY (`conformite_id`) REFERENCES `product_conformite` (`id`);

--
-- Contraintes pour la table `product_evaluationicpe`
--
ALTER TABLE `product_evaluationicpe`
  ADD CONSTRAINT `FK_7502D645166D1F9C` FOREIGN KEY (`project_id`) REFERENCES `product_project` (`id`),
  ADD CONSTRAINT `FK_7502D6451D5F791B` FOREIGN KEY (`rubriqueEvaluation_id`) REFERENCES `product_rubriques_evaluation` (`id`);

--
-- Contraintes pour la table `product_family`
--
ALTER TABLE `product_family`
  ADD CONSTRAINT `FK_C79A60FF7EC2F574` FOREIGN KEY (`process_id`) REFERENCES `product_process` (`id`);

--
-- Contraintes pour la table `product_impact_icpe`
--
ALTER TABLE `product_impact_icpe`
  ADD CONSTRAINT `FK_AA3C0B1A166D1F9C` FOREIGN KEY (`project_id`) REFERENCES `product_project` (`id`),
  ADD CONSTRAINT `FK_AA3C0B1A52D72944` FOREIGN KEY (`processicpe_id`) REFERENCES `product_process_icpe` (`id`),
  ADD CONSTRAINT `FK_AA3C0B1A54C38D61` FOREIGN KEY (`stockageicpe_id`) REFERENCES `product_stockage_icpe` (`id`);

--
-- Contraintes pour la table `product_part`
--
ALTER TABLE `product_part`
  ADD CONSTRAINT `FK_C4E75267166D1F9C` FOREIGN KEY (`project_id`) REFERENCES `product_project` (`id`),
  ADD CONSTRAINT `FK_C4E75267C35E566A` FOREIGN KEY (`family_id`) REFERENCES `product_family` (`id`);

--
-- Contraintes pour la table `product_rubriques_evaluation`
--
ALTER TABLE `product_rubriques_evaluation`
  ADD CONSTRAINT `FK_6EE455CC3BD38833` FOREIGN KEY (`rubrique_id`) REFERENCES `product_rubriques` (`id`),
  ADD CONSTRAINT `FK_6EE455CC9DF9BB88` FOREIGN KEY (`evaluationIcpe_id`) REFERENCES `product_evaluationicpe` (`id`);

--
-- Contraintes pour la table `product_situation_administrative`
--
ALTER TABLE `product_situation_administrative`
  ADD CONSTRAINT `FK_92A7098335E7D534` FOREIGN KEY (`regime_id`) REFERENCES `product_regime` (`id`);

--
-- Contraintes pour la table `product_situation_reelle`
--
ALTER TABLE `product_situation_reelle`
  ADD CONSTRAINT `FK_198812F735E7D534` FOREIGN KEY (`regime_id`) REFERENCES `product_regime` (`id`);

--
-- Contraintes pour la table `product_user`
--
ALTER TABLE `product_user`
  ADD CONSTRAINT `FK_7BF4E8166D1F9C` FOREIGN KEY (`project_id`) REFERENCES `product_project` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
