$(function(){
    $(document).tooltip({
        selector:'[data-toggle="tooltip"]',
        placement:'top'
    });

	$('div.expandable p').expander({
		expandEffect: 'fadeIn',
		expandSpeed: 500,
		collapseEffect: 'fadeOut',
		collapseSpeed: 200,
	    slicePoint:       300,  // default is 100
	    expandPrefix:     ' ', // default is '... '
	    expandText:       'Lire la suite -> ', // default is 'read more'
	    userCollapseText: '<'  // default is 'read less'
	});

    $('#project-create').click(function () {
		$('#project-name').fadeIn();
	});
        
    $('#folder-create').click(function () {
		$('#folder-name').fadeIn();
	});
        
    $('#project-import-nomenclature').click(function () {
                $('#dialog-importExcel').dialog('open');
        });    

    if ($('.iphone :checkbox').length > 0) {
        $('.iphone :checkbox').iphoneStyle({checkedLabel: 'Fort', uncheckedLabel: 'Faible', resizeContainer: false, resizeHandle: false});
    }

    /*
    $('#project-validate').click(function () {
        var url = $('#project-form').attr('action');
		$.post(url, $("#project-form").serialize(), function(data) {
			$('#project-name').fadeOut('fast');
		});
	});
    */

   $('.date-pick').datePicker();

    $( ".slider" ).click(function () {
        console.log("Slider click !");
        var val = $(this).parent().find('.help-note').val();

        var slider_prop_main = $(this).parent().parent().parent().find('.slider-prop-main');
        var slider_prop_default = $(this).parent().parent().parent().find('.slider-prop-default');
        
        $(this).removeClass('red green blue');
        val = parseFloat(val);

        if (val == 1) {
            $(this).addClass('red');
            $(this).html('Faible');
            val = 0;
        } else {
            if (val == 0.5) {
                $(this).addClass('blue');
                $(this).html('Fort');
                val = 1;
            }
            if (val == 0) {
                $(this).addClass('green');
                $(this).html('Moyen');
                val = 0.5;
            }
            if (isNaN(val)) {
                $(this).addClass('red');
                $(this).html('Faible');
                val = 0;
            }
        }
        $(this).parent().find('.help-note').val( val );

        updateHelpProp(slider_prop_main, 'main');
        updateHelpProp(slider_prop_default, 'default');
	});

    $('.slider-prop-main').each(function(index) {
        updateHelpProp($(this), 'main');
    });
    $('.slider-prop-default').each(function(index) {
        updateHelpProp($(this), 'default');
    });
    //$('body').disableSelection();



    $( ".dialog" ).dialog({
			autoOpen: false,
			height: 500,
			width: 800,
			modal: true,
			buttons: {
                'Valider': function() {
                    var form = $( this ).find( "form" );
					var url = form.attr('action');
                    $.post(url, form.serialize());
                    $( this ).dialog( "close" );
                    btn_current.attr('src', '/css/skin/famfamfam/accept.png');
				},
				'Annuler': function() {
					$( this ).dialog( "close" );
				}
			},
			close: function() {

			}
    });

    $( ".dialog2" ).dialog({
			autoOpen: false,
			height: 520,
			width: 900,
			modal: true,
			buttons: {
				'Ok': function() {
					$( this ).dialog( "close" );
				}
			}
    });

    $('#energy').click(function () {
        if($(this).is(':checked')){
            $('#energy-container').slideDown();
        } else {
            $('#energy-container').hide();
        }
	});

    $('.delete').click(function() {
        return window.confirm(this.title || 'Etes vous sûr de vouloir supprimer');
    });
});

function showHelp(elt, id) {
    if ($('#help-'+id).is(':visible')) {
        $('#help-'+id).hide();
        $(elt).html('Afficher&nbsp;l\'aide');
    } else {
        $('#help-'+id).fadeIn();
        $(elt).html('Masquer&nbsp;l\'aide');
    }
    return false;
}

function navigateTo(elt) {
    var url = $(elt).val();
    document.location.href=url;
}

function updateHelpProp(prop, type) {
    var tot = 0;
    var i = 0;

    notes = prop.parent().parent().parent().find('.help-note-'+type);
    prop.removeClass('red green blue');

    notes.each(function(index) {
        var val = parseFloat($(this).val());
        if (val >= 0) {
            tot = tot + val;
            i++;
        }
    });
    
    console.log("Total : "+tot);

    var total = tot/i;

    if (total <= 0.5) {
        prop.addClass('red');
        prop.html('Faible');
    }

    if (total > 0.5) {
        prop.addClass('blue');
        prop.html('Fort');
    }
}