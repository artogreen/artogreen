$(function(){
    $("nav .product").click(function(event) {
        event.preventDefault();
        $('.subnav-home').hide();
        $('.subnav-product').hide();
        $('.subnav-service').hide();
        $('.active-nav a').animate({opacity: 0}, 1);
        $('.subnav-product').slideToggle(function () {
              $('.active-nav a.product').animate({opacity: 1});
        });
        return false;
    });

    $("nav .service").click(function(event) {
        event.preventDefault();
        $('.subnav-home').hide();
        $('.subnav-product').hide();
        $('.subnav-service').hide();
        $('.active-nav a').animate({opacity: 0}, 1);
        $('.subnav-service').slideToggle(function () {
              $('.active-nav a.service').animate({opacity: 1});
        });
        return false;
    });

    $("nav .home").click(function(event) {
        event.preventDefault();
        $('.subnav-home').hide();
        $('.subnav-product').hide();
        $('.subnav-service').hide();
        $('.active-nav a').animate({opacity: 0}, 1);
        $('.subnav-home').slideToggle(function () {
              $('.active-nav a.home').animate({opacity: 1});
        });
        return false;
    });

    $('.carousel').carousel({
        interval: 5000
    });

    $('.date-pick').datePicker();
});

function showProduct() {
	$("nav .product").click();
	var pos = $('nav').offset().top + 150;
	$('html, body').animate({scrollTop: pos}, 500);
}