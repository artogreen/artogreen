$(function(){
    $("#calculator button").click(function() {
        $input = $("#calculator .input");
        $result = $("#calculator .result");
        try {
            $result.val(eval($input.val()));
        } catch (e) {
            $result.val('erreur');
        }
    });
});

function roundNumber(rnum, rlength) { // Arguments: number to round, number of decimal places
    var newnumber = Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);
    return parseFloat(newnumber); // Output the result to the form field (change for your purposes)
}